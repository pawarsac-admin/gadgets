<?php
	//error_reporting(E_ALL);
	/*
	//CLEANUP
	//TRUNCATE TABLE `PRODUCT_MASTER`
	//TRUNCATE TABLE `PRODUCT_NAME_INFO`
	//TRUNCATE TABLE `PRODUCT_FEATURE`
	//TRUNCATE TABLE `PRICE_VARIANT_VALUES`

	TRUNCATE TABLE `PRODUCT_MASTER`;TRUNCATE TABLE `PRODUCT_NAME_INFO`;TRUNCATE TABLE `PRODUCT_FEATURE`;TRUNCATE TABLE `PRICE_VARIANT_VALUES`


	*/
	set_time_limit (0);

	$BASEPATH	= (defined('BASEPATH'))? BASEPATH : '';   

	require_once($BASEPATH.'include/config.php');
	require_once(CLASSPATH.'DbConn.php');
	require_once(CLASSPATH.'DbOp.php');

	define("SPECIAL_CHAR_PATTERN",'[^ a-zA-Z0-9,\.\/\':;"\[\]{}`~@#\$%^&*()_+-=<>\?!]');

	$pattern='/'.SPECIAL_CHAR_PATTERN.'/';

	$dbconn			= new DbConn;
	$DbOperation	= new DbOperation;
	$category_id	= 1;// Top Rated Phones
	$country_id=$state_id=$city_id=1;


	if (!defined('FILE_PATH')) {
		define('FILE_PATH','./upload/');
	}

	if(!empty($_FILES["filename"]["name"])){
		$filename	= $_FILES["filename"]["name"];
	} else {
		$filename	= $_REQUEST['filename'];
	}

	if(empty($filename)) {
		echo "<b>Usage :</b> " . $_SERVER["PHP_SELF"]  . '?filename=file.xls&debug=1'. "<br/><br/>";	
		die("No filename specified.");
	}

	if($_REQUEST['debug']==1) echo "\n FILE PATH : " , FILE_PATH .$filename;

	if( ! file_exists( FILE_PATH .$filename)){
		die("File of name <b>$filename</b> not exists.");
	}

	if($_REQUEST['debug']==1) echo "\nBRANDS : ";
	//brand_id 	category_id 	brand_name 	brand_image 	brand_research_image 	short_desc 	status
	$sql		= "SELECT * FROM BRAND_MASTER";
	$data		= $DbOperation->select($sql);
 	//print_r($data);
	foreach($data as $key => $val){
		$arrBrands[$val['brand_id']]= strtolower($val['brand_name']);
	}
	if($_REQUEST['debug']==1) print_r($arrBrands);
	unset($data);
	

	if($_REQUEST['debug']==1) echo  "\nMODELS : ";
	//product_name_id 	product_info_name 	category_id 	brand_id 	tags 	abstract 	product_name_desc 	media_id 	video_path 	img_media_id 	image_path 	status
	$sql		= "SELECT * FROM PRODUCT_NAME_INFO";
	$data		= $DbOperation->select($sql);
 	//print_r($data);
	foreach($data as $key => $val){
		$arrModels[$val['brand_id']][$val['product_name_id']]= strtolower($val['product_info_name']);
	}
	if($_REQUEST['debug']==1) print_r($arrModels);
	unset($data);

	//// READ EXCEL SHEET


	require "excel/reader.php";

	// initialize reader object
	$excel = new Spreadsheet_Excel_Reader();

	// read spreadsheet data
	$excel->read( FILE_PATH .$filename);//Exact_20_09.xls

	// iterate over spreadsheet cells and print as HTML table
	$x=6;
	$tabledata .= "<table border='1'>";
	while($x <= $excel->sheets[0]['numRows']) {

		$brand	= isset($excel->sheets[0]['cells'][$x][1]) ? $excel->sheets[0]['cells'][$x][1] : '';
		$brand	= trim($brand);
		$sBrand	= strtolower($brand);

		$model 	= isset($excel->sheets[0]['cells'][$x][2]) ? $excel->sheets[0]['cells'][$x][2] : '';
		$model	= trim($model);
		$sModel	= strtolower($model);
		
		$variant= isset($excel->sheets[0]['cells'][$x][3]) ? $excel->sheets[0]['cells'][$x][3] : '';
		$variant= trim($variant);
		$sVariant= strtolower($variant);
		
		$id				= isset($excel->sheets[0]['cells'][$x][4]) ? $excel->sheets[0]['cells'][$x][4] : '';
		$price			= isset($excel->sheets[0]['cells'][$x][5]) ? $excel->sheets[0]['cells'][$x][5] : '';
		$availibility	= trim($excel->sheets[0]['cells'][$x][6]);
		$description	= trim($excel->sheets[0]['cells'][$x][7]);

		if(!empty($brand)){
			$brand	= preg_replace($pattern, '', $brand);
			$sBrand	= preg_replace($pattern, '', $sBrand);
		}

		if(!empty($model)){
			$model	= preg_replace($pattern, '', $model);
			$sModel	= preg_replace($pattern, '', $sModel);
		}

		if(!empty($variant)){
			$variant	= preg_replace($pattern, '', $variant);
			$sVariant	= preg_replace($pattern, '', $sVariant);
		}
		
		if(!empty($description)){
			$description	= preg_replace($pattern, '', $description);
			$description	= addslashes($description);
		}
		
		if(!empty($availibility)){
			$availibility	= preg_replace($pattern, '', $availibility);
		}

		if(!empty($price)){
			$price  = str_ireplace(array('rs','.'),'', $price);
			$price  = trim($price);
		}

		unset($arrFeatures);

		$tabledata .= "\t<tr>\n";

		$y=7;
		while($y <= $excel->sheets[0]['numCols']) {
			$cell = isset($excel->sheets[0]['cells'][$x][$y]) ? $excel->sheets[0]['cells'][$x][$y] : '';
			$tabledata .= "\t\t<td>$cell</td>\n";

			if(!empty($cell)){
				$cell	= preg_replace($pattern, '', $cell);
			}

			$arrFeatures[$excel->sheets[0]['cells'][4][$y]] = $cell;
			$y++;
		}
		$tabledata .= "\t</tr>\n";
		$x++;
		//continue;
	        if($_REQUEST['debug']==1) echo  "<br>Brand : ", $sBrand;
	        if($_REQUEST['debug']==1) echo  "<br>Model : ", $sModel;
	        if($_REQUEST['debug']==1) echo  "<br>Variant : ", $sVariant;
        	if($_REQUEST['debug']==1) echo  "<br>id : ", $id;
	        if($_REQUEST['debug']==1) echo  "<br>Price : ", $price;

		if(empty($model) ){
			echo "\n<br/>No Model Name. Continuing.....";
			continue;
		}

		if($_REQUEST['debug']==1) print_r($arrFeatures);
		//continue;
		/// BRAND PROCESSING
	    if( in_array( $sBrand,$arrBrands ) ){
        	        $brand_id       = array_search( $sBrand, $arrBrands );
 		} else {
        	        $sql    = "INSERT INTO BRAND_MASTER(brand_id,category_id,brand_name,brand_image,brand_research_image,short_desc,status,position,create_date,update_date) VALUE ('',$category_id,'$brand','$brand_image','$brand_research_image','$short_desc',1,999,now(),now())";
                	if($_REQUEST['debug']==1) echo "\n<br/>NEW BRAND SQL : ", $sql;
                	$brand_id               = $DbOperation->insert($sql);
                	$arrBrands[$brand_id]   = $sBrand;
        }

		/// MODEL PROCESSING
		if( in_array( $sModel,$arrModels[$brand_id] ) ){
				$model_id       = array_search( $smodel, $arrModels[$brand_id] );
		} else {
				$sql    = "INSERT INTO PRODUCT_NAME_INFO(product_name_id,product_info_name,category_id,brand_id,tags,abstract,product_name_desc,media_id,video_path,img_media_id,image_path,	status,create_date,update_date) VALUE ('','$model',$category_id,$brand_id,'$tags','$abstract','$product_name_desc','$media_id','$video_path','$img_media_id','$image_path', 1,now(),now())";
				if($_REQUEST['debug']==1) echo "\n<br/>NEW MODEL SQL : ", $sql;
				$model_id            		   = $DbOperation->insert($sql);
				$arrModels[$brand_id][$model_id]   = $sModel;
		}

		$sql	= "SELECT product_id,count(product_id) as cnt FROM PRODUCT_MASTER WHERE brand_id='$brand_id' AND product_name='$model' AND VARIANT='$variant'";
 		if($_REQUEST['debug']==1) echo "\n<br/> FIND EXISTING:", $sql;
		unset($op);
		$op		= $DbOperation->select($sql);
		if($_REQUEST['debug']==1) print_r($op);
        $cnt	= $op[0]['cnt'];

		// PRODUCT [VARIANT] PROCESSING
		 if( $cnt <= 0 ){
              $sql    = "INSERT INTO PRODUCT_MASTER(product_id,category_id,brand_id,uid,product_name,variant,product_desc,media_id,video_path,img_media_id,image_path,status,availibility,create_date, update_date) VALUE ('', $category_id,$brand_id,'$uid','$model','$variant','$description','$media_id','$video_path','$img_media_id','$image_path', 1,'$availibility',now(),now())";
              if($_REQUEST['debug']==1) echo "\n<br/>NEW PRODUCT SQL : ", $sql;
              $product_id	= $DbOperation->insert($sql);
			if($product_id){
				if($_REQUEST['debug']==1) echo "\n<br/>NEW PRODUCT ADDED : $model $variant";
				// FEATUREs [VARIANT] PROCESSING
		        	if( count($arrFeatures) > 0 ){
					foreach($arrFeatures as $fkey => $fvalue ){
						
                        			$sql    = "INSERT INTO PRODUCT_FEATURE(product_feature_id,product_id,feature_id,feature_value,create_date) VALUE ('', $product_id,$fkey,'$fvalue', now())";
                        			if($_REQUEST['debug']==1) echo "\n<br/>-FEATURE SQL : ", $sql;
                        			$feature_id     = $DbOperation->insert($sql);
					}
                        		if($product_id){
                                		if($_REQUEST['debug']==1) echo "\n<br/>--NEW FEATURE ADDED for $model $variant";
                        		}
                		}else{
                        		if($_REQUEST['debug']==1) echo "\n<br/>NO FEATURES AVAILABLE FOR : $model $variant";
                		}
			}
       }else{
			$product_id    = $op[0]['product_id'];
			if($_REQUEST['debug']==1) echo "<br/>PRODUCT ALREADY EXISTS : $product_id $model $variant";
			if(is_numeric($product_id) && $product_id > 0 ) {
				if($_REQUEST['debug']==1) echo "\n<br/>  UPDATING.................";
			}else{
				if($_REQUEST['debug']==1) echo "\n<br/>  Check Product_id : $product_id .It's Invalid. Continuing to next record............";
				continue;	
			}

			$extraparam		= '';

			if(!empty($media_id)){
				$extraparam	.= ", media_id='$media_id'";
			}
			if(!empty($video_path)){
				$extraparam	.= ", video_path='$video_path'";
			}
			if(!empty($img_media_id)){
				$extraparam	.= ", img_media_id='$img_media_id'";
			}
			if(!empty($image_path)){
				$extraparam	.= ", image_path='$image_path'";
			}

			$sql    = "UPDATE PRODUCT_MASTER SET brand_id = $brand_id,product_name='$model',variant='$variant',product_desc='$description', availibility='$availibility', update_date=now() $extraparam WHERE product_id=$product_id";
                        if($_REQUEST['debug']==1) echo "\n<br/>PRODUCT UPDATE SQL : ", $sql;
                        $op     = $DbOperation->update($sql);
                        if($product_id){
                                if($_REQUEST['debug']==1) echo "\n<br/>PRODUCT UPDATED : $product_id $model $variant";
                                // FEATUREs [VARIANT] PROCESSING
                                if( count($arrFeatures) > 0 ){
                                        foreach($arrFeatures as $fkey => $fvalue ){
											if(empty($fkey)) continue;

											$sql    = "SELECT count(1) as cnt1 FROM PRODUCT_FEATURE WHERE product_id = $product_id AND feature_id= $fkey";
											
											if($_REQUEST['debug']==1) echo "\n<br/>- FEATURE SQL : ", $sql;

											$op		= $DbOperation->select($sql);
											if($_REQUEST['debug']==1) print_r($op);
											$cnt1	= $op[0]['cnt1'];


											if( $cnt1 >= 1 ) {
												$sql    = "UPDATE PRODUCT_FEATURE set feature_value= '$fvalue'  WHERE product_id = $product_id AND feature_id= $fkey ";
                                                if($_REQUEST['debug']==1) echo "\n<br/>-FEATURE UPDATE SQL : ", $sql;
                                                $feature_id     = $DbOperation->update($sql);
                                                if($_REQUEST['debug']==1) echo "\n<br/>-- FEATURE UPDATED? : $feature_id";
                                            } else {    
												$sql    = "INSERT INTO PRODUCT_FEATURE(product_feature_id,product_id,feature_id,feature_value,create_date) VALUE ('', $product_id,$fkey,'$fvalue', now())";
												if($_REQUEST['debug']==1) echo "\n<br/>-FEATURE INSERT SQL : ", $sql;
												$feature_id     = $DbOperation->insert($sql);
												if($_REQUEST['debug']==1) echo "\n<br/>-- FEATURE Id : $feature_id";
											}

						
                                        }
                                        if($product_id){
                                                if($_REQUEST['debug']==1) echo "\n<br/>-- FEATURE UPDATED for $product_id $model $variant";
                                        }
								}else{
										if($_REQUEST['debug']==1) echo "\n<br/>NO FEATURES AVAILABLE FOR : $product_id $model $variant";
								}
                        }
				} 

			// PRICE PROCESSING START
			if( $product_id ){

				$sql    = "SELECT count(1) as cnt2 FROM PRICE_VARIANT_VALUES WHERE product_id = $product_id ";
				if($_REQUEST['debug']==1) echo "\n<br/>- PRICE SQL : ", $sql;
				$op		= $DbOperation->select($sql);
				if($_REQUEST['debug']==1) print_r($op);
				$cnt2	= $op[0]['cnt2'];
				
				
				if( $cnt2 >= 1 ) {

					$sql    = "UPDATE PRICE_VARIANT_VALUES SET brand_id='$brand_id',product_id='$product_id',country_id='$country_id',state_id='$state_id',city_id='$city_id',variant_value='$price',update_date=now() WHERE product_id=$product_id ";
					if($_REQUEST['debug']==1) echo "<br/>-UPDATE PRICE SQL : ", $sql;
					$price_id     = $DbOperation->update($sql);
					if($price_id){
						if($_REQUEST['debug']==1) echo "\n<br/>--PRICE UPDATED for $product_id $model $variant";
					}

				} else {

					$sql    = "INSERT INTO PRICE_VARIANT_VALUES( price_variant,category_id,brand_id,product_id,country_id,state_id,city_id,variant_value,variant_id,default_city,status,create_date,update_date) VALUE ('', $category_id,$brand_id,$product_id,$country_id,$state_id,$city_id,'$price',1,1,1,now(), now())";
					if($_REQUEST['debug']==1) echo "\n<br/>-PRICE SQL : ", $sql;
					$price_id     = $DbOperation->insert($sql);
					if($price_id){
						if($_REQUEST['debug']==1) echo "\n<br/>--PRICE ADDED for $model $variant";
					}
				}
			} // PRICE PROCESSING END 

	}
	$tabledata .= "</table>";
	if($_REQUEST['debug']==1) echo $tabledata;
?>