<?php
// Include PEAR::Spreadsheet_Excel_Writer
require "Writer.php";
 
// Create an instance
$xls =& new Spreadsheet_Excel_Writer();
 
// Send HTTP headers to tell the browser what's coming
$fn = $xls->save("test.xls");

 
// Add a worksheet to the file, returning an object to add data to
$sheet =& $xls->addWorksheet('Success');
 
// Write some numbers
for ( $i=0;$i<11;$i++ ) {
 // Use PHP's decbin() function to convert integer to binary
	 for($j=0;$j<14;$j++){
		$k = $i * $j;
		$sheet->write($i,$j,$k);
	 }
}

// Finish the spreadsheet, dumping it to the browser
$xls->close();
 
?>