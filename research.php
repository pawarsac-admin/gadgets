<?php
	require_once('./include/config.php');
	require_once(CLASSPATH.'DbConn.php');
	require_once(CLASSPATH.'brand.class.php');
	require_once(CLASSPATH.'category.class.php');
	require_once(CLASSPATH.'pivot.class.php');
	require_once(CLASSPATH.'feature.class.php');
	require_once(CLASSPATH.'videos.class.php');

	$dbconn = new DbConn;
	$brand = new BrandManagement;
	$category = new CategoryManagement;
	$pivot = new PivotManagement;
	$feature = new FeatureManagement;
	$videoGallery = new videos();


//	print_r($_REQUEST);

	$category_id = $_REQUEST['catid'] ? $_REQUEST['catid'] : SITE_CATEGORY_ID;
	$tab_id = $_REQUEST['fid'] ? $_REQUEST['fid'] : 1;
	$brandids = $_REQUEST['brandids'];
	$featureids = $_REQUEST['featureids'];
	$productids = $_REQUEST['productids'];
	$startlimit = $_REQUEST['startlimit'];
	$endlimit = $_REQUEST['cnt'];
	$variant_id = "1"; // important for price search.ie. ex-showroom price.
/*
	$mn_price = $_REQUEST['mnprice'] ? $_REQUEST['mnprice'] : '1';
	$mn_price_unit = $_REQUEST['mnpriceunit'] ? $_REQUEST['mnpriceunit'] : 'lakh';

	$mx_price = $_REQUEST['mxprice'] ? $_REQUEST['mxprice'] : '22.0';
	$mx_price_unit = $_REQUEST['mxpriceunit'] ? $_REQUEST['mxpriceunit'] : 'crore';

	$seopriceArr[] = $mn_price." ".$mn_price_unit;
	$seopriceArr[] = $mx_price." ".$mx_price_unit;

	if(strtolower($mn_price_unit) == 'crore'){
		$mn_mult = 10000000;
	}else{
		$mn_mult = 100000;
	}
	if(strtolower($mx_price_unit) == 'crore'){
		$mx_mult = 10000000;
	}else{
		$mx_mult = 100000;
	}

	$startprice = $mn_price*$mn_mult;
	$endprice = $mx_price*$mx_mult;

*/
	require_once('./product_search_min_max_price.php');

	$unsel_all_brand = $_REQUEST['unsel_all_brand'];
	$selectedbrandArr = (!$unsel_all_brand) ? $_REQUEST['branddetails'] : '';
	$seoUrlArr[] = WEB_URL.SEO_CAR_FINDER;
	if(!empty($startprice)){
		$priceArr[] = $startprice;
	}
	if(!empty($endprice)){
		$priceArr[] = $endprice;
	}
	if(sizeof($priceArr) > 0){
		$seoUrlArr[] = implode("-",$priceArr);
	}
	if(sizeof($selectedbrandArr) > 0){		
		foreach($selectedbrandArr as $brand_id){			
			$result = $brand->arrGetBrandDetails($brand_id,$category_id);
			$seoUrlArr[] = $result[0]['brand_name'];
			$seoBrandArr[] = rawurlencode($result[0]['brand_name']);
			unset($result);
		}
	}
	
	if(sizeof($seoBrandArr) > 0){
		$seotitleArr[] = implode(",",$seoBrandArr);
	}

	$selectedfeatureArr = $_REQUEST['featuredetails'];	
	if(sizeof($selectedfeatureArr) > 0){		
		foreach($selectedfeatureArr as $feature_id){			
			$result = $feature->arrGetFeatureDetails($feature_id,$category_id);
			$seoUrlArr[] = $result[0]['feature_name'];
			$seoFeatureArr[] = rawurlencode($result[0]['feature_name']);
			unset($result);
		}
	}
	
	if(sizeof($seoFeatureArr) > 0){
		$seotitleArr[] = 'Cars with '.implode(",",$seoFeatureArr);
	}
	
	$seo_url = implode("/",$seoUrlArr);
	
	unset($seoUrlArr[0]);
	//unset($seoUrlArr[1]);
	$seo_js = implode(",",$seoUrlArr);
	
	$seotitle = implode(" ",$seotitleArr);
	unset($seotitleArr);
	if(!empty($seotitle)){
		$seotitleArr[] = "Compare ".$seotitle;
	}
	if(sizeof($seotitleArr) > 0){		
		$seotitleArr[] = "Price Rs.".implode(" - ",$seopriceArr);
		$seotitleArr[] = "on ".SEO_DOMAIN;
	}else{
		$seotitleArr[] = "Mobile Finder - Price Rs.".implode(" - ",$seopriceArr);
		$seotitleArr[] = "on ".SEO_DOMAIN;
	}
	
	$seo_title = "Mobile Finder - On Mobiles India | Search Cars, New Mobiles by Brands, Price, Body Style, Mobile Features on ".SEO_DOMAIN;

	$seo_desc = "Search cars, new mobiles by selecting the searching criteria - Mobile brand, mobile prices in India, body style, mobile features, fuel type, transmission and seating capacity using Mobile Finder of ".SEO_DOMAIN;
	$seo_tags = "car finder, find cars, search car, search mobiles by brand, search mobiles by style, search mobiles by features, find mobiles by brand, find mobiles by style, carfinder, carfinder of OnCars, mobile finder at ".SEO_DOMAIN.".india, find mobiles on OnCars india";
	
	$breadcrumb = CATEGORY_HOME."New Mobile Search";

	$sortproductBY = $_REQUEST['sortproduct'] ? $_REQUEST['sortproduct'] : 1;
	$sortproductxml = "<SELECTED_SORT_PRODUCT_BY><![CDATA[$sortproductBY]]></SELECTED_SORT_PRODUCT_BY>";

	//$selecteditemArr[] = Array();
	
	if(!empty($category_id)){
		$result = $brand->arrGetBrandDetails("",$category_id,1);
	}
	$cnt = sizeof($result);
	$xml = "<BRAND_MASTER>";
	$xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";	
	
	$selectedIndex = "0";
	$isBrandSelected = "0"; //used toggle all brands checkbox.
	for($i=0;$i<$cnt;$i++){
		$brand_id = $result[$i]['brand_id'];
		if(in_array($brand_id,$selectedbrandArr)){
			$result[$i]['selected_brand_id'] = $brand_id;
			$selecteditemArr[$selectedIndex]['selected_id'] = $brand_id;
			$selecteditemArr[$selectedIndex]['selected_type'] = 'checkbox_brand_id_'.$brand_id;
			$selecteditemArr[$selectedIndex]['selected_name'] =$result[$i]['brand_name'];
			$selectedIndex++;
			$isBrandSelected++;
			
		}
		$status = $result[$i]['status'];
		$categoryid = $result[$i]['category_id'];
		if(!empty($categoryid)){
			$category_result = $category->arrGetCategoryDetails($categoryid);
		}
		$category_name = $category_result[0]['category_name'];
		$result[$i]['js_category_name'] = $category_name;
		$result[$i]['category_name'] = html_entity_decode($category_name,ENT_QUOTES,'UTF-8');
		$result[$i]['brand_status'] = ($status == 1) ? 'Active' : 'InActive';
		$result[$i]['create_date'] = date('d-m-Y',strtotime($result[$i]['create_date']));
		$result[$i]['js_brand_name'] = $result[$i]['brand_name'];
		$result[$i]['brand_name'] = html_entity_decode($result[$i]['brand_name'],ENT_QUOTES,'UTF-8');
		$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
		$xml .= "<BRAND_MASTER_DATA>";
		foreach($result[$i] as $k=>$v){
			$xml .= "<$k><![CDATA[$v]]></$k>";
		}
		$xml .= "</BRAND_MASTER_DATA>";
	}
	$xml .= "</BRAND_MASTER>";
	if(!empty($category_id)){
		unset($result);
		$result = $pivot->arrPivotSubGroupDetails("",$category_id,1);
	}
	$cnt = sizeof($result);
	
	for($i=0;$i<$cnt;$i++){
		$plusminusimgstatus = 0;
		$status = $result[$i]['status'];
		$sub_group_id = $result[$i]['sub_group_id'];
		$categoryid = $result[$i]['category_id'];
		$sub_group_name = $result[$i]['sub_group_name'];
		
		if(!empty($categoryid)){
            $category_name = $category_result[0]['category_name'];
            $result[$i]['js_category_name'] = $category_name;
			$result[$i]['category_name'] = html_entity_decode($category_name,ENT_QUOTES,'UTF-8');
			$pivot_result = $pivot->arrGetPivotDetails("",$category_id,"","1",$sub_group_id);
			$pivotCnt = sizeof($pivot_result);
			for($j=0;$j<$pivotCnt;$j++){
				$pivot_display_id = $pivot_result[$j]['pivot_display_id'];
				if(!empty($pivot_display_id)){
					$pivot_display_type_result = $pivot->arrPivotDisplayDetails($pivot_display_id,"1");
					$pivot_display_type = $pivot_display_type_result[0]['pivot_display_name'];
				}else{
					$pivot_display_type = "checkbox";
				}
				$pivot_group = $pivot_result[$j]['pivot_group'];
				$main_pivot_group = $sub_group_name ;

				$status = $pivot_result[$j]['status'];

				$categoryid = $pivot_result[$j]['category_id'];
				$feature_id = $pivot_result[$j]['feature_id'];
				
				if(!empty($feature_id)){
					$feature_result = $feature->arrGetFeatureDetails($feature_id,$categoryid,"","","1");
					$feature_name = $feature_result[0]['feature_name'];
					$feature_img_path = $feature_result[0]['feature_img_path'];
				}

				if(in_array($feature_id,$selectedfeatureArr)){
					$pivot_result[$j]['selected_feature_id'] = $feature_id;
					$selecteditemArr[$selectedIndex]['selected_id'] = $feature_id;
					$selecteditemArr[$selectedIndex]['selected_type'] = 'checkbox_feature_id_'.$feature_id;
					$selecteditemArr[$selectedIndex]['selected_name'] = $feature_name;
					$selectedIndex++;
					$plusminusimgstatus++;
				}

				$pivot_result[$j]['feature_name'] = $feature_name;
				$pivot_result[$j]['feature_img_path'] = !empty($feature_img_path) ? IMAGE_URL.$feature_img_path : '';
				$pivot_result[$j]['pivot_display_type'] = $pivot_display_type;
				
				$pivotresult[$sub_group_id][$pivot_group][] = $pivot_result[$j];
				$result[$i]['plus_minus_img_status'] = $plusminusimgstatus;
				foreach($result[$i] as $k=> $v){
					$pivotresult[$sub_group_id][$k] = $v;
				}
			}			
		}
	
	}
	$groupnodexml .= "<PIVOT_MASTER>";
	if($pivotresult){
		foreach($pivotresult as $maingroupkey => $maingroupval){
			if(is_array($maingroupval)){
				
				$groupnodexml .= "<PIVOT_MASTER_DATA>";
				foreach($maingroupval as $subgroupkey=>$subgroupval){
					
					if(is_array($subgroupval)){
						$groupnodexml .= "<SUB_PIVOT_MASTER>";
						 foreach($subgroupval as $key => $featuredata){
							if(is_array($featuredata)){
								$groupnodexml .= "<SUB_PIVOT_MASTER_DATA>";
								$featuredata = array_change_key_case($featuredata,CASE_UPPER);
								foreach($featuredata as $featurekey => $featureval){
									$groupnodexml .= "<$featurekey><![CDATA[$featureval]]></$featurekey>";
								}	
								$groupnodexml .= "</SUB_PIVOT_MASTER_DATA>";
							}else{
								$key = strtoupper($key);
								$groupnodexml .= "<$key><![CDATA[$featuredata]]></$key>";
							}
						}
						$groupnodexml .= "</SUB_PIVOT_MASTER>";
					}else{
						$subgroupkey = strtoupper($subgroupkey);
						$groupnodexml .= "<$subgroupkey><![CDATA[$subgroupval]]></$subgroupkey>";
					}


				}
				$groupnodexml .= "</PIVOT_MASTER_DATA>";
			}
		}
	}	
	$groupnodexml .= "</PIVOT_MASTER>";
	
	$selectedBoxCnt = sizeof($selecteditemArr);
	$selectitemxml = "<SELECTED_ITEM_MASTER>";
	$selectitemxml .= "<COUNT><![CDATA[$selectedBoxCnt]]></COUNT>";
	for($i=0;$i<$selectedBoxCnt;$i++){
		$selectitemxml .= "<SELECTED_ITEM_MASTER_DATA>";
		foreach($selecteditemArr[$i] as $k=>$v){
			$selectitemxml .= "<".strtoupper($k)."><![CDATA[$v]]></".strtoupper($k).">";
		}
		$selectitemxml .= "</SELECTED_ITEM_MASTER_DATA>";
	}
	$selectitemxml .= "</SELECTED_ITEM_MASTER>";
	 
	$config_details = get_config_details();
	$strXML = "<XML>";
	$strXML .= "<MAX_PRICE><![CDATA[$mx_price]]></MAX_PRICE>";
	$strXML .= "<MAX_PRICE_UNIT><![CDATA[$mx_price_unit]]></MAX_PRICE_UNIT>";
	$strXML .= "<MIN_PRICE><![CDATA[$mn_price]]></MIN_PRICE>";
	$strXML .= "<MIN_PRICE_UNIT><![CDATA[$mn_price_unit]]></MIN_PRICE_UNIT>";	
	$strXML .= "<SEO_CAR_FINDER><![CDATA[".SEO_CAR_FINDER."]]></SEO_CAR_FINDER>";
	$strXML .= "<SEO_JS><![CDATA[$seo_js]]></SEO_JS>";
	$strXML .= "<SEO_TITLE><![CDATA[$seo_title]]></SEO_TITLE>";
	$strXML .= "<BREAD_CRUMB><![CDATA[$breadcrumb]]></BREAD_CRUMB>";
	$strXML .= "<SEO_DESC><![CDATA[$seo_desc]]></SEO_DESC>";
	$strXML .= "<SEO_TAGS><![CDATA[$seo_tags]]></SEO_TAGS>";
	$strXML .= "<SEO_URL><![CDATA[$seo_url]]></SEO_URL>";
	$strXML .= "<STARTLIMIT><![CDATA[".$offset."]]></STARTLIMIT>";
	$strXML .= "<PAGE_OFFSET><![CDATA[".OFFSET."]]></PAGE_OFFSET>";
	$strXML .= "<CNT><![CDATA[$numpages]]></CNT>";
	$strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$category_id]]></SELECTED_CATEGORY_ID>"; 
	$strXML .= "<SELECTED_BRAND_ID><![CDATA[$isBrandSelected]]></SELECTED_BRAND_ID>";
	$strXML .= "<SELECTEDTABID><![CDATA[$tab_id]]></SELECTEDTABID>";

	$strXML .= "<SEO_PRICE_STR><![CDATA[".implode("-",array($startprice,$endprice))."]]></SEO_PRICE_STR>";
	$strXML .= $config_details;
	$strXML .= $xml;
	$strXML .= $groupnodexml;
	$strXML .= $selbrandxml;
	$strXML .= $selfeaturexml;
	$strXML .= $sortproductxml;
	$strXML .= $selectitemxml;
	$strXML .= "<PAGER><![CDATA[$pageNavStr]]></PAGER>";
	$strXML .= "<OC_ROS_BOTTOM_NORTH_728x90><![CDATA[OC_ROS_Bottom_North_728x90]]></OC_ROS_BOTTOM_NORTH_728x90>";
	$strXML .= "<OC_ROS_TOP_RHS_LREC_300x250_1><![CDATA[OC_ROS_Top_RHS_Lrec_300x250_1]]></OC_ROS_TOP_RHS_LREC_300x250_1>";
	 $strXML .= "<PAGE_NAME>".$_SERVER['SCRIPT_URI']."</PAGE_NAME>";
	$most_recent_one_result_list = $videoGallery->getarrMostRecentVideosHeaderTabLink($category_id,"0","1",$array_result);
	$strXML.="<MOST_RECENT_VIDEO_HEADER_LINK><![CDATA[".$most_recent_one_result_list."]]></MOST_RECENT_VIDEO_HEADER_LINK>";
	$strXML .= "</XML>";
	if($_REQUEST['debug']==1){ header('Content-type: text/xml');echo $strXML;exit;}
	$doc = new DOMDocument();
	$doc->loadXML($strXML);
	$doc->saveXML();
	$xslt = new xsltProcessor;
	$xsl = DOMDocument::load('xsl/research.xsl');
	$xslt->importStylesheet($xsl);
	print $xslt->transformToXML($doc);
?>
