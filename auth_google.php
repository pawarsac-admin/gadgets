<?php
//ini_set("display_errors",true);
//error_reporting(E_ALL);
require_once("include/config.php");
require_once(CLASSPATH."/authentication.class.php");
require_once(CLASSPATH."/utility.class.php");
require_once(CLASSPATH.'xmlparser.class.php');
////require_once(CLASSPATH."/user_details.class.php");
////$oUserDetails = new user_details;
$oXmlparser		= new XMLParser;
$oAuth =new authentication();
$sAction	 = $_REQUEST['action'];
$sAuthToken	 = $_REQUEST['oauth_token'];
$sRedirectUrl	= $_REQUEST['redirect_url'];
//$iSid			= $_REQUEST['service_id'];

$iFlag=0;
require 'openID/openid.php';

try {
	$openid = new LightOpenID;				
		//if(!$openid->mode){ 
		if($_REQUEST['login']==1 && !$openid->mode){
			$openid->identity = 'https://www.google.com/accounts/o8/id';
			$openid->required = array('birthDate','person/gender','contact/postalCode/home','namePerson/first', 'namePerson/last', 'contact/email','contact/country/home'); 

			setcookie("redirect_url", $sRedirectUrl, $sExpTime, "/",DOMAIN);

			//$openid->optional = array('birthDate','person/gender','contact/postalCode/home');
			//die($openid->authUrl());
			header('Location: ' . $openid->authUrl());
			
		}else{
			$iFlag =1;
			$userAttributes = $openid->getAttributes();
			$firstName		= $userAttributes['namePerson/first'];  
			$lastName		= $userAttributes['namePerson/last'];  
			$userEmail		= $userAttributes['contact/email'];  
			$birthDate		= $userAttributes['birthDate'];  
			$gender			= $userAttributes['person/gender'];  
			$gender			= ($gender=='F') ? 'F':'M';
			$postalCode		= $userAttributes['contact/postalCode/home'];
			$country		= $userAttributes['contact/country/home']; 
		
			$iSid			= SERVICEID;
			$openid->validate();
			$iUid			= $openid->identity;

			$sRedirectUrl = $_COOKIE['redirect_url'];//$sRedirectUrl;
			//echo $sRedirectUrl."TEST---";

			// default gender set is M as Gender is not getting using apen id
			$sContent= "action=register&mode=google&redirect_url=$sRedirectUrl&service_id=$iSid&id=$iUid&email=$userEmail&first_name=$firstName&last_name=$lastName&sex=$gender";
			//error_log("Content:".$sContent);
			
			$sResultXML =utility::curlaccess($sContent,AUTH_API_ACTION_URL);
			$oXmlparser->XMLParse($sResultXML);
			$aResultXML =$oXmlparser->getOutput();

			$iUid	= $aResultXML['response']['uid'];
			$sFname	= $aResultXML['response']['fname'];
			$sLname	= $aResultXML['response']['lname'];
			$sEmail	= $aResultXML['response']['email'];
			$sUrl	= $aResultXML['response']['url'];


			$sSessionId = authentication::create_session(array('email'=>$sEmail, 'uid'=>$iUid));

			setcookie("email", $sEmail, $sExpTime, "/",DOMAIN);
			setcookie("session_id", $sSessionId, $sExpTime, "/",DOMAIN);
			setcookie("uid", $iUid,$sExpTime,"/",DOMAIN);
			setcookie("fname", $sFname,$sExpTime,"/",DOMAIN);
			setcookie("lname", $sLname,$sExpTime,"/",DOMAIN); 

			
			setcookie("redirect_url", '', 0, "/",DOMAIN);
		}
		
} catch(ErrorException $e) {
	echo $e->getMessage();
}


if($iFlag==1){?>
<script langauge="javascript">			
window.opener.location.href=<?="'$sRedirectUrl'"?>;					
window.close();
</script>
<?}
?>
