<?php
	//error_reporting(E_ALL);
	require_once('include/config.php');
	require_once(CLASSPATH.'DbConn.php');
	require_once(CLASSPATH.'DbOp.php');

	$dbconn		= new DbConn;
	$DbOperation	= new DbOperation;
	$category_id	= 1;// Top Rated Phones

	if($_REQUEST['debug']==1) echo "\nFEATURE GROUP : ";
	$sql		= "SELECT * FROM MAIN_FEATURE_GROUP";
	$data		= $DbOperation->select($sql);
 	//print_r($data);
	foreach($data as $key => $val){
		$arrFeatGroup[$val['group_id']]= $val['main_group_name'];
	}
	if($_REQUEST['debug']==1) print_r($arrFeatGroup);
	unset($data);


	if($_REQUEST['debug']==1) echo  "\nFEATURE SUB GROUP : ";
	//sub_group_id 	category_id 	main_group_id 	sub_group_name 	sub_group_position 	status 	create_date
	$sql		= "SELECT * FROM FEATURE_SUB_GROUP";
	$data		= $DbOperation->select($sql);
 	//print_r($data);
	foreach($data as $key => $val){
		$arrFeatSubGroup[$val['main_group_id']][$val['sub_group_id']]= $val['sub_group_name'];
	}
	if($_REQUEST['debug']==1) print_r($arrFeatSubGroup);
	unset($data);



	if($_REQUEST['debug']==1)  echo "\nFEATURE : ";  
	//Full Texts 	feature_id 	main_feature_group 	feature_name 	category_id 	feature_description 	feature_group 	feature_img_path 	unit_id 	status
        $sql            = "SELECT * FROM FEATURE_MASTER";
        $data           = $DbOperation->select($sql);
        //print_r($data);
	$featCount	= 0;
        foreach($data as $key => $val){
                $arrFeat[$val['main_feature_group']][$val['feature_group']][$val['feature_id']]= $val['feature_name'];
                $arrFeatures[$val['feature_id']]= $val['feature_name'];
		$featCount++;
        }
        if($_REQUEST['debug']==1) print_r($arrFeat);
        unset($data);


	if($_REQUEST['debug']==1) echo "Features Count : ", $featCount;
	if($_REQUEST['debug']==1) die;

require "excel/Writer.php";

// Create an instance
$xls =& new Spreadsheet_Excel_Writer();

// Send HTTP headers to tell the browser what's coming
$fn = $xls->save("specification.xls");


// Add a worksheet to the file, returning an object to add data to
$sheet =& $xls->addWorksheet('Success');

$sheet->write( 1, 0, "Feature Group" );
$sheet->write( 2, 0, "Feature Subgroup" );
$sheet->write( 3, 0, "Feature Id" );
$sheet->write( 4, 0, "Brand" );
$sheet->write( 4, 1, "Model" );
$sheet->write( 4, 2, "Variant" );
$sheet->write( 4, 3, "ID" );
$sheet->write( 4, 4, "Price" );

// Write some numbers
$column	= 5;

foreach ( $arrFeatGroup as $featureGroupId => $featureGroup  ) {
	foreach ( $arrFeatSubGroup[$featureGroupId] as $featureSubGroupID => $featureSubGroup ) {
		foreach ( $arrFeat[$featureGroupId][$featureSubGroupID] as $featureId => $feature ) {
      			$sheet->write( 1, $column, $featureGroup );
      			$sheet->write( 2, $column, $featureSubGroup );
      			$sheet->write( 3, $column, $featureId );
      			$sheet->write( 4, $column, $feature );
			$column++;
		}
	}
}

// Finish the spreadsheet, dumping it to the browser
$xls->close();

?>
