<?php
	// include
	require_once('./include/config.php');
	require_once(CLASSPATH.'DbConn.php');
	require_once(CLASSPATH.'DbConn.php');
	require_once(CLASSPATH.'brand.class.php');
	require_once(CLASSPATH.'category.class.php');
	require_once(CLASSPATH.'product.class.php');
	require_once(CLASSPATH.'article.class.php');
	require_once(CLASSPATH.'reviews.class.php');
	require_once(CLASSPATH.'pager.class.php');
	require_once(CLASSPATH.'user.class.php');
	require_once(CLASSPATH.'user_review.class.php');
	require_once(CLASSPATH.'videos.class.php');
	require_once(CLASSPATH.'report.class.php');
	require_once(CLASSPATH."campus_discussion.class.php");

	// object declaration
	$dbconn = new DbConn;
	$oBrand = new BrandManagement;
	$category = new CategoryManagement;
	$oProduct = new ProductManagement;
	$oArticle = new article;
	$oReview = new reviews;	
	$videoGallery = new videos();
	$obj_user = new user;
	$userreview = new USERREVIEW;
	$report = new report;
	$oCampusDiscussion =  new campus_discussion();


	//print "<pre>"; print_r($_REQUEST);

	// param
	$category_id = $_REQUEST['catid'] ? $_REQUEST['catid'] : SITE_CATEGORY_ID;
	$curpage = $_REQUEST['page'] ? $_REQUEST['page'] : 1 ;

	$BrandID = $_REQUEST['Brand'] ? $_REQUEST['Brand'] : "" ;
	$ModelId = $_REQUEST['Model'] ? $_REQUEST['Model'] : "" ;
	
	//SEO details	
	$seoArr[0] = "Car Reviews - On Mobiles India";
	$seoArr[1] = "Get Latest Mobile Reviews, Auto Reviews for New Mobiles from Experts & Users on ".SEO_DOMAIN;
	
	$seo_desc = "New mobile reviews, automobile reviews, get reviews of latest mobiles from experts as well as mobile owners. Write your own reviews on mobiles and get commented by other users on on ".SEO_DOMAIN;
	$seo_keywords = "car reviews, new mobile reviews, auto reviews, automobile reviews, latest mobile reviews, mobile experts review, mobile owner review, write mobile reviews, get mobile reviews";
	
	$seo_title = implode(" | ",$seoArr);

	$request_uri = $_SERVER['REQUEST_URI'];
	if(strpos($request_uri,"page=")){
		$_REQUEST['page'] = str_replace(array("/Car-User-Reviews?page="),"",$_SERVER['REQUEST_URI']);
	}
	$curpage = $_REQUEST['page'] ? $_REQUEST['page'] : 1 ;
	$limit = 5;
	$oPager = new Pager();
	$start = $oPager->findStart($limit);
	
	$count = $userreview->getDistinctModelUserReviewsCount("","","","","",$BrandID,$category_id,$ModelId,"","1");
	$pages = $oPager->findPages($count, $limit);
	$siteUrl = WEB_URL.str_replace('/','',$_SERVER['PHP_SELF']);	
	if(!empty($curpage)){
		$sPagingXml = $oPager->pageNumNextPrev($curpage, $pages, $siteUrl, $link_type);
	}
	$type=$_REQUEST['type'];
	if($type=='user_reviews'){
		$resultRev=$userreview->getDistinctModelUserReviews("","","","","",$BrandID,$category_id,$ModelId,"","1",$start,$limit,"order by PR.`product_info_name`");
		//print"</pre>";print_r($resultRev);print"</pre>";
		/**********start view count from API*******************/
		$cnt = sizeof($resultRev);
        	$aViewCntUrl = array();
	        $aEncViewCntUrl = array();

	        for($k=0;$k<$cnt;$k++){
        	        $seoTitleArr =""; $seoTitleArr1 ="";
			$product_info_name=$resultRev[$k]['product_info_name'];
                        $product_name_id=$resultRev[$k]['product_info_id'];
                        $product_brand_id=$resultRev[$k]['brand_id'];
			if(!empty($product_brand_id)){
                        	$brand_result = $oBrand->arrGetBrandDetails($product_brand_id,$category_id);
                                $seo_brand_name = $brand_result[0]['brand_name'];
                        }
                        //user review list
	                $result = $userreview->arrGetUserReviewDetails("","","","","","",$category_id,$product_name_id,"","1",0,1,'order by create_date desc');


	                $seo_brand_name = html_entity_decode($seo_brand_name,ENT_QUOTES,'UTF-8');
	                $seo_brand_name = removeSlashes($seo_brand_name);
	                $seo_brand_name = seo_title_replace($seo_brand_name);

	                $product_info_name = html_entity_decode($product_info_name,ENT_QUOTES,'UTF-8');
	                $product_info_name = removeSlashes($product_info_name);
	                $product_info_name = seo_title_replace($product_info_name);

			$res_cnt = sizeof($result);
			for($i=0;$i<$res_cnt;$i++){
				$user_review_id = $result[$i]['user_review_id'];
				$brand_id = $result[$i]['brand_id'];
	                        $model_id = $result[$i]['product_info_id'];
	                        $product_id = $result[$i]['product_id'];
	                        $category_id = $result[$i]['category_id'];
	                        $create_date = $result[$i]['create_date'];
	                        $result[$i]['create_date'] = date('d/m/Y',strtotime($create_date));
	                        if(!empty($brand_id)){
	                                $brand_result = $oBrand->arrGetBrandDetails($brand_id,$category_id);
	                                $brand_name = $brand_result[0]['brand_name'];
	                                $modelArr[] = $brand_name;
	                        }
	                        $result[$i]['brand_name'] = $brand_name;
	                        if(!empty($model_id)){
	                                $product_result = $oProduct->arrGetProductNameInfo($model_id);
	                                $model_name = $product_result[0]['product_info_name'];
	                                $modelArr[] = $model_name;
	                        }
	                        $result[$i]['model_name'] = $model_name;
	                        $result[$i]['brand_model_name'] = implode(" ",$modelArr);
	                        if(!empty($product_id)){
	                                $product_result = $oProduct->arrGetProductDetails($product_id);
	                                $variant = $product_result[0]['variant'];
	                                $modelArr[] = $variant_name;
	                        }
	                        $result[$i]['brand_model_variant_name'] = implode(" ",$modelArr);
	                        unset($modelArr);
	                        $result[$i]['variant'] = $variant;
				$brand_name = html_entity_decode($brand_name,ENT_QUOTES,'UTF-8');
	                        $brand_name = removeSlashes($brand_name);
	                        $brand_name = seo_title_replace($brand_name);
	                        $model = html_entity_decode($model_name,ENT_QUOTES,'UTF-8');
	                        $model = removeSlashes($model_name);
	                        $model = seo_title_replace($model_name);
	                        $variant = html_entity_decode($variant,ENT_QUOTES,'UTF-8');
	                        $variant = removeSlashes($variant);
	                        $variant = seo_title_replace($variant);
	                        //seo user review
	                        unset($seoTitleArr);
	                        $seoTitleArr[] = SEO_WEB_URL;
	                        $seoTitleArr[] = $brand_name."-cars";
	                        $seoTitleArr[] = $brand_name."-".$model;;
	                        $seoTitleArr[] = "reviews-ratings";
	                        $seoTitleArr[] = SEO_CARS_MODEL_FULLREVIEWS;
	                        $seoTitleArr[] = $model;
	                        $seoTitleArr[] = $model_id;
	                        $seoTitleArr[] = $user_review_id;
	                        $seo_review_url = implode("/",$seoTitleArr);	
				$aViewCntUrl[$seo_review_url] = $user_review_id;
			        $aEncViewCntUrl[$seo_review_url] = $user_review_id;

					$aMBList[] = $user_review_id;
			}

        	}
        	//print "<pre>"; print_r($aViewCntUrl);
	        //print "<pre>";print_r($aEncViewCntUrl);

        	$aViewCnt=Array();
	        $aViewCnt = $report->getPageViews($aViewCntUrl,$aEncViewCntUrl);

			$aComParameters = array(); $aMBData = array();
			$aComParameters = Array("title"=>implode(",",$aMBList),"cid"=>USER_REVIEW_MODEL_CATEGORY_ID,"sid"=>SERVICEID);
			$aMBData = $oCampusDiscussion->getMulThreadParentReplyCnt($aComParameters);

        	//print "<pre>"; print_r($aViewCnt);
	        //die();
		/**********End view count from API*******************/

		$resultCnt=sizeof($resultRev);
		$xml .= "<MODEL_USER_REVIEW_MASTER>";
		for($ii=0;$ii<$resultCnt;$ii++){
			$product_info_name=$resultRev[$ii]['product_info_name'];
			$product_name_id=$resultRev[$ii]['product_info_id'];
			$product_brand_id=$resultRev[$ii]['brand_id'];
			if(!empty($product_brand_id)){
					$brand_result = $oBrand->arrGetBrandDetails($product_brand_id,$category_id);
					$seo_brand_name = $brand_result[0]['brand_name'];				
					//$modelArr[] = $brand_name;
				}
				//user review list
				$result = $userreview->arrGetUserReviewDetails("","","","","","",$category_id,$product_name_id,"","1",0,1,'order by create_date desc');
			
				$cnt = sizeof($result);
				$xml .="<MODEL_USER_REVIEW_DATA>";
				$xml .= "<MODEL_NAME><![CDATA[".$product_info_name."]]></MODEL_NAME>";
				$xml .= "<BRAND_NAME><![CDATA[".$seo_brand_name."]]></BRAND_NAME>";
				$xml .= "<DISP_NAME><![CDATA[".$seo_brand_name." ".$product_info_name."]]></DISP_NAME>";

				$seo_brand_name = html_entity_decode($seo_brand_name,ENT_QUOTES,'UTF-8');
				$seo_brand_name = removeSlashes($seo_brand_name);
				$seo_brand_name = seo_title_replace($seo_brand_name);
			
				$product_info_name = html_entity_decode($product_info_name,ENT_QUOTES,'UTF-8');
				$product_info_name = removeSlashes($product_info_name);
				$product_info_name = seo_title_replace($product_info_name);
			
				//seo user review
				unset($seoTitleArr);
				$seoTitleArr[] = SEO_WEB_URL;
				$seoTitleArr[] = $seo_brand_name."-cars";
				$seoTitleArr[] = $seo_brand_name."-".$product_info_name;;
				$seoTitleArr[] = "reviews-ratings";
				$seoTitleArr[] = SEO_CARS_MODEL_REVIEWS; 
				$seoTitleArr[] = $product_info_name;
				$seoTitleArr[] = $product_name_id;
				$review_seo_url = implode("/",$seoTitleArr);
				$xml .= "<REVIEW_SEO_URL><![CDATA[".$review_seo_url."]]></REVIEW_SEO_URL>";

				$xml .= "<USER_REVIEW_MASTER>";
				$xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
				for($i=0;$i<$cnt;$i++){
					$user_review_id = $result[$i]['user_review_id'];
					//echo "USER_REVIEW--".$user_review_id;
					
					$SERVICEID = SERVICEID;
					$USER_REVIEW_MODEL_CATEGORY_ID = USER_REVIEW_MODEL_CATEGORY_ID;
					$comment_count = $aMBData['data'][$user_review_id][$USER_REVIEW_MODEL_CATEGORY_ID][$SERVICEID];
					if(!empty($comment_count) || $comment_count!=0){
						$result[$i]['comment_count'] = $comment_count;
					}

					if(isset($aViewCnt) && $aViewCnt[$user_review_id]!=''){
						$result[$i]['views_count'] = $aViewCnt[$user_review_id];
					}

					$brand_id = $result[$i]['brand_id'];
					$model_id = $result[$i]['product_info_id'];
					$product_id = $result[$i]['product_id'];
					$category_id = $result[$i]['category_id'];
					$create_date = $result[$i]['create_date'];
					$result[$i]['create_date'] = date('d/m/Y',strtotime($create_date));
					if(!empty($brand_id)){
						$brand_result = $oBrand->arrGetBrandDetails($brand_id,$category_id);
						$brand_name = $brand_result[0]['brand_name'];				
						$modelArr[] = $brand_name;
					}
					$result[$i]['brand_name'] = $brand_name;
					if(!empty($model_id)){
						$product_result = $oProduct->arrGetProductNameInfo($model_id);
						//$product_info_id = $product_result[0]['product_info_id'];
						$model_name = $product_result[0]['product_info_name'];
						$modelArr[] = $model_name;
					}			
					$result[$i]['model_name'] = $model_name;
					$result[$i]['brand_model_name'] = implode(" ",$modelArr);
					if(!empty($product_id)){
						$product_result = $oProduct->arrGetProductDetails($product_id);
						$variant = $product_result[0]['variant'];
						$modelArr[] = $variant_name;
					}
					$result[$i]['brand_model_variant_name'] = implode(" ",$modelArr);
					unset($modelArr);
					$result[$i]['variant'] = $variant;
					$xml .= "<USER_REVIEW_MASTER_DATA>";
					$ratingresult = $userreview->arrGetUserQnA('','',$user_review_id,"1");
					$ratingcnt = sizeof($ratingresult);
					//print "<pre>"; print_r($ratingresult);
					$xml .= "<USER_RATING_MASTER>";
					for($rating=0;$rating<$ratingcnt;$rating++){
						$que_id = $ratingresult[$rating]['que_id'];
						$que_result = $userreview->arrGetQuestions($que_id);				
						$ratingresult[$rating]['quename'] = $que_result[0]['quename'];
						$answer = $ratingresult[$rating]['answer'];
						$ansArr = explode(",",$answer);
						$gradeCnt = $ratingresult[$rating]['grade'];
						$html = "";
						for($grade=1;$grade<=5;$grade++){
							if($grade <= $gradeCnt){
								$html .= '<img src="'.IMAGE_URL.'spacer.gif" class="starOn"/>';
							}else{
								$html .= '<img src="'.IMAGE_URL.'spacer.gif" class="starOff"/>';
							}
						}
						$ratingresult[$rating]['grade'] = $html;				
						$xml .= "<USER_RATING_MASTER_DATA>";
						$ratingresult[$rating] = array_change_key_case($ratingresult[$rating],CASE_UPPER);
						foreach($ratingresult[$rating] as $k=>$v){
							$xml .= "<$k><![CDATA[$v]]></$k>";
						}
						$xml .= "</USER_RATING_MASTER_DATA>";
					}
					$xml .= "</USER_RATING_MASTER>";
					$reviewresult = $userreview->arrGetUserQnA('','',$user_review_id,"0","1","0","1"); // for comment
					$reviewcnt = sizeof($reviewresult);
					$xml .= "<USER_REVIEW_COMMENT_ANSWER_MASTER>";
					for($review=0;$review<$reviewcnt;$review++){
						$que_id = $reviewresult[$review]['que_id'];
						$answer = $reviewresult[$review]['answer'];
						$answer = removeSlashes($answer);                
						$answer = html_entity_decode($answer,ENT_QUOTES,'UTF-8');
						if(strlen($answer)>100){ $answer = getCompactString($answer, 95).' ...'; }
							$reviewresult[$review]['answer'] = $answer;
							$que_result = $userreview->arrGetQuestions($que_id);			
							$reviewresult[$review]['quename'] = $que_result[0]['quename'];
							$reviewresult[$review] = array_change_key_case($reviewresult[$review],CASE_UPPER);
							$xml .= "<USER_REVIEW_COMMENT_ANSWER_MASTER_DATA>";
							foreach($reviewresult[$review] as $k=>$v){
								$xml .= "<$k><![CDATA[$v]]></$k>";
							}
							$xml .= "</USER_REVIEW_COMMENT_ANSWER_MASTER_DATA>";
						}
						$xml .= "</USER_REVIEW_COMMENT_ANSWER_MASTER>";
						$brand_name = html_entity_decode($brand_name,ENT_QUOTES,'UTF-8');
						$brand_name = removeSlashes($brand_name);
						$brand_name = seo_title_replace($brand_name);
						$model = html_entity_decode($model_name,ENT_QUOTES,'UTF-8');
						$model = removeSlashes($model_name);
						$model = seo_title_replace($model_name);
						$variant = html_entity_decode($variant,ENT_QUOTES,'UTF-8');
						$variant = removeSlashes($variant);
						$variant = seo_title_replace($variant);
						//seo user review
						unset($seoTitleArr);
						$seoTitleArr[] = SEO_WEB_URL;
						$seoTitleArr[] = $brand_name."-cars";
						$seoTitleArr[] = $brand_name."-".$model;;
						$seoTitleArr[] = "reviews-ratings";
						$seoTitleArr[] = SEO_CARS_MODEL_FULLREVIEWS; 
						$seoTitleArr[] = $model;
						$seoTitleArr[] = $model_id;
						$seoTitleArr[] = $user_review_id;
						$result[$i]['user_review_seo_url'] = implode("/",$seoTitleArr);
						$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
						foreach($result[$i] as $k=>$v){
							$xml .= "<$k><![CDATA[$v]]></$k>";
						}
						$xml .= "</USER_REVIEW_MASTER_DATA>";
					}
					$xml .= "</USER_REVIEW_MASTER>";
					//used to check admin rating.
					$result = $userreview->arrGetAdminOverallGrade($category_id,$brand_id,'0',$model_id);
					$cnt = sizeof($result);
					$overallcnt = 0;
					$overallavg = round($result[0]['overallgrade']);
					if($cnt <= 0){
						$result = $userreview->arrGetOverallGrade($category_id,$brand_id,'0',$model_id);
						$overallcnt = $result[0]['totaloverallcnt'];
						$overallavg = round($result[0]['overallavg']);
					}
					$html = "";
					for($grade=1;$grade<=5;$grade++){
						if($grade <= $overallavg){
							$html .= '<img src="'.IMAGE_URL.'spacer.gif" class="vsblStr mlr1"/>';
						}else{
							$html .= '<img src="'.IMAGE_URL.'spacer.gif" class="dsblStr mlr1"/>';
						}
					}
					$xml .= "<OVERALL_AVG_HTML><![CDATA[$html]]></OVERALL_AVG_HTML>";
					$xml .= "<OVERALL_AVG_CNT><![CDATA[$overallavg]]></OVERALL_AVG_CNT>";
					$xml .= "<OVERALL_TOTAL_CNT><![CDATA[$overallcnt]]></OVERALL_TOTAL_CNT>";
					$xml .= "</MODEL_USER_REVIEW_DATA>";
				}
		$xml .= "</MODEL_USER_REVIEW_MASTER>";
				
	}

	$result = $oBrand->arrGetBrandDetails("",$category_id,1);
	$cnt = sizeof($result);
	$xml1 = "<BRAND_MASTER>";
	$xml1 .= "<COUNT><![CDATA[$cnt]]></COUNT>";	

	$selectedIndex = "0";
	$isBrandSelected = "0"; //used toggle all brands checkbox.
	for($i=0;$i<$cnt;$i++){
		$brand_id = $result[$i]['brand_id'];
		$status = $result[$i]['status'];
		$categoryid = $result[$i]['category_id'];
		if(!empty($categoryid)){
		$category_result = $category->arrGetCategoryDetails($categoryid);
		}
		$category_name = $category_result[0]['category_name'];
		$result[$i]['js_category_name'] = $category_name;
		$result[$i]['category_name'] = html_entity_decode($category_name,ENT_QUOTES,'UTF-8');
		$result[$i]['brand_status'] = ($status == 1) ? 'Active' : 'InActive';
		$result[$i]['create_date'] = date('d-m-Y',strtotime($result[$i]['create_date']));
		$result[$i]['js_brand_name'] = $result[$i]['brand_name'];
		$result[$i]['brand_name'] = html_entity_decode($result[$i]['brand_name'],ENT_QUOTES,'UTF-8');
		$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
		$xml1 .= "<BRAND_MASTER_DATA>";
		foreach($result[$i] as $k=>$v){
			$xml1 .= "<$k><![CDATA[$v]]></$k>";
		}
		$xml1 .= "</BRAND_MASTER_DATA>";
	}
	$xml1 .= "</BRAND_MASTER>";

$breadcrumb = CATEGORY_HOME." User Reviews";
$config_details = get_config_details();

	$strXML .= "<XML>";
	$strXML .= "<TYPE><![CDATA[$type]]></TYPE>";
	$strXML .= "<BREAD_CRUMB><![CDATA[$breadcrumb]]></BREAD_CRUMB>";
	$strXML .= "<SEO_WEB_URL><![CDATA[".SEO_WEB_URL."]]></SEO_WEB_URL>";
	$strXML .= "<SEO_TITLE><![CDATA[$seo_title]]></SEO_TITLE>";
	$strXML .= "<SEO_TAGS><![CDATA[$seo_keywords]]></SEO_TAGS>";
	$strXML .= "<SEO_DESC><![CDATA[$seo_desc]]></SEO_DESC>";
	$strXML .= "<MODEL_BRAND_NAME><![CDATA[$sModelBrandName]]></MODEL_BRAND_NAME>";
	$strXML .= "<MODEL_NAME><![CDATA[$product_info_name]]></MODEL_NAME>";
	$strXML .= "<STARTLIMIT><![CDATA[$startlimit]]></STARTLIMIT>";
	$strXML .= "<CNT><![CDATA[$limitcnt]]></CNT>";
	$strXML .= "<CURRENT_PAGE><![CDATA[$curpage]]></CURRENT_PAGE>";
	$strXML .= "<SELECTEDTABID><![CDATA[$tab_id]]></SELECTEDTABID>";
	$strXML .= $config_details;
	$strXML .= $sFeaturedReviewsDetXml;
	$strXML .= $sMoreReviewsDetXml;
	$strXML .= $xml;
	$strXML .= $xml1;
	$strXML .=  "<PAGING><![CDATA[$sPagingXml]]></PAGING>";
	$strXML .=  "<BRANDID><![CDATA[$BrandID]]></BRANDID>";
	$strXML .=  "<MODELID><![CDATA[$ModelId]]></MODELID>";
	
	$strXML .= "<OC_ROS_BOTTOM_NORTH_728x90><![CDATA[OC_ROS_Bottom_North_728x90]]></OC_ROS_BOTTOM_NORTH_728x90>";
	$strXML .= "<OC_ROS_TOP_RHS_LREC_300x250_1><![CDATA[OC_ROS_Top_RHS_Lrec_300x250_1]]></OC_ROS_TOP_RHS_LREC_300x250_1>";
	
	$strXML .= "<PAGE_NAME>".$_SERVER['SCRIPT_URI']."</PAGE_NAME>";
	$most_recent_one_result_list = $videoGallery->getarrMostRecentVideosHeaderTabLink($category_id,"0","1",$array_result);
	$strXML.="<MOST_RECENT_VIDEO_HEADER_LINK><![CDATA[".$most_recent_one_result_list."]]></MOST_RECENT_VIDEO_HEADER_LINK>";
	$strXML .= "</XML>";

	if($_GET['debug']==1) { header('Content-type: text/xml');echo $strXML;exit; }
	$doc = new DOMDocument();
	$doc->loadXML($strXML);
	$doc->saveXML();

	$xslt = new xsltProcessor;
	$xsl = DOMDocument::load('xsl/user_reviews_list.xsl');

	$xslt->importStylesheet($xsl);
	print $xslt->transformToXML($doc);
?>
