<?php
	require_once('./include/config.php');
	require_once(CLASSPATH.'DbConn.php');
	require_once(CLASSPATH.'videos.class.php');
	require_once(CLASSPATH.'reviews.class.php');
	require_once(CLASSPATH.'article.class.php');
	require_once(CLASSPATH.'Utility.php');


	$dbconn = new DbConn;
	$videoGallery = new videos();
	$reviews = new reviews(); 
	$article = new article(); 
	$url = $_REQUEST['url'];
	$category_id = $_REQUEST['catid'] ? $_REQUEST['catid'] : SITE_CATEGORY_ID;
	$video_id=$_REQUEST['vid'];
	$group_id=$_REQUEST['group_id'];
	$type_id=$_REQUEST['type_id'];
	$product_id=$_REQUEST['product_id'];
	$product_info_id=$_REQUEST['product_info_id'];
	$brand_id=$_REQUEST['brand_id'];
	$status=$_REQUEST['status'];
	$startlimit=$_REQUEST['startlimit'];
	$cnt=$_REQUEST['cnt'];
	$orderby=$_REQUEST['orderby'];
	$type = $_REQUEST['type'];
	$tbl_type = $_REQUEST['tbl_type'];
	$selected_video_id = $video_id;
	$table_name="VIDEO_GALLERY";
	$queryparam = Array();
	$urlArr[] = SEO_WEB_URL;
	switch($url){
		case 'Car-Videos-Photos':
			$queryparam[] = $videoGallery->getarrMostRecentVideosHeaderTabLink($category_id,"0","1",$array_result);
			$queryparam[] = 'utm';
			$redirectUrl = implode("?",$queryparam);
			break;
		case 'Car-Video-Most-Popular':
			if($tbl_type == 1){
                        	$result = $videoGallery->getVideosDetails($video_id,"","","","",$category_id);
				$t_id = $result[0]["type_id"];
				if($t_id == "3"){
					 $urlArr[] = SEO_CAR_VIDEOS_AUTO_PORN;	
				}elseif($t_id == "4"){
					$urlArr[] = SEO_CAR_VIDEOS_INTERNATIONAL;
				}
	                }else if($tbl_type == 2){
        	                $result = $reviews->arrGetReviewsVideoDetails($video_id,"","","",$category_id);
				$urlArr[] = SEO_CAR_VIDEOS_REVIEW;
	                }else if($tbl_type == 3){
        	                $result = $article->arrGetArticleVideoDetails($video_id,"","","",$category_id);
				$urlArr[] = SEO_CAR_VIDEOS_MAINTAINANCE;
	                }else if($tbl_type == 4){
        	                $result = $article->arrGetNewsVideoDetails($video_id,"","","",$category_id);
				$urlArr[] = SEO_CAR_VIDEOS_NEWS;
			}
			if(!empty($result[0]['title'])){
				$urlArr[] = str_replace(array(" ","/"),"-",$result[0]['title']);
			}
			if(!empty($result[0]['video_id'])){
				$urlArr[] = $result[0]['video_id'];	
			}
			$redirectUrl = implode("/",$urlArr);
			break;
		case 'Car-Video-Reviews':
			 $result = $reviews->arrGetReviewsVideoDetails($video_id,"","","",$category_id);
			 $urlArr[] = SEO_CAR_VIDEOS_REVIEW;
			 if(!empty($result[0]['title'])){
				 $urlArr[] = str_replace(array(" ","/"),"-",$result[0]['title']);
			 }
			 if(!empty($result[0]['video_id'])){
				 $urlArr[] = $result[0]['video_id'];
			 }
			 $redirectUrl = implode("/",$urlArr);
			break;
		case 'Car-Video-First-Drive':
			$result = $videoGallery->getVideosDetails($video_id,"","","","",$category_id);
			$urlArr[] = SEO_CAR_VIDEOS_AUTO_PORN;
			if(!empty($result[0]['title'])){
				$urlArr[] = str_replace(array(" ","/"),"-",$result[0]['title']);
			}
			if(!empty($result[0]['video_id'])){
				$urlArr[] = $result[0]['video_id'];
			}
			$redirectUrl = implode("/",$urlArr);
			break;
		case 'Car-Video-International':
			$result = $videoGallery->getVideosDetails($video_id,"","","","",$category_id);
			$urlArr[] = SEO_CAR_VIDEOS_INTERNATIONAL;
			if(!empty($result[0]['title'])){
				$urlArr[] = str_replace(array(" ","/"),"-",$result[0]['title']);
			}
			if(!empty($result[0]['video_id'])){
	            $urlArr[] = $result[0]['video_id'];
			}
            $redirectUrl = implode("/",$urlArr);
			break;
		case 'Car-Video-Others':
                        if($type == "1"){
								$result = $article->arrGetArticleVideoDetails($video_id,"","","",$category_id);
                                $urlArr[] = SEO_CAR_VIDEOS_MAINTAINANCE;
                        }elseif($type == "2"){
								$result = $article->arrGetNewsVideoDetails($video_id,"","","",$category_id);
                                $urlArr[] = SEO_CAR_VIDEOS_NEWS;
                        }
						if(!empty($result[0]['title'])){
	                        $urlArr[] = str_replace(array(" ","/"),"-",$result[0]['title']);
						}
						if(!empty($result[0]['video_id'])){
	                        $urlArr[] = $result[0]['video_id'];
						}
                        $redirectUrl = implode("/",$urlArr);
			break;
		case 'Car-Video-Featured':
			if($tbl_type == 1){
						$result = $videoGallery->getVideosDetails($video_id,"","","","",$category_id);
						$t_id = $result[0]["type_id"];
						if($t_id == "3"){
								 $urlArr[] = SEO_CAR_VIDEOS_AUTO_PORN;
						}elseif($t_id == "4"){
								$urlArr[] = SEO_CAR_VIDEOS_INTERNATIONAL;
						}
                        }else if($tbl_type == 2){
                                $result = $reviews->arrGetReviewsVideoDetails($video_id,"","","",$category_id);
                                $urlArr[] = SEO_CAR_VIDEOS_REVIEW;
                        }else if($tbl_type == 3){
                                $result = $article->arrGetArticleVideoDetails($video_id,"","","",$category_id);
                                $urlArr[] = SEO_CAR_VIDEOS_MAINTAINANCE;
                        }else if($tbl_type == 4){
                                $result = $article->arrGetNewsVideoDetails($video_id,"","","",$category_id);
                                $urlArr[] = SEO_CAR_VIDEOS_NEWS;
                        }
						if(!empty($result[0]['title'])){
	                        $urlArr[] = str_replace(array(" ","/"),"-",$result[0]['title']);
						}
						if(!empty($result[0]['video_id'])){
	                        $urlArr[] = $result[0]['video_id'];
						}
                        $redirectUrl = implode("/",$urlArr);
			break;
		default:
			$queryparam[] = $videoGallery->getarrMostRecentVideosHeaderTabLink($category_id,"0","1",$array_result);
			$queryparam[] = 'utm';
			$redirectUrl = implode("?",$queryparam);
                        break;
	}
//	echo $redirectUrl;print_r($result);exit;
	header("Location: $redirectUrl");
	exit;
?>