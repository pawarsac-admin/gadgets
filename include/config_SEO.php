<?php
	/** @defgroup include Include
	*  This is the Include  group
	*  @{
	*/
	/**
	* @version 1.0
	* @created 23-11-2011
	*/

	define("SEO_WEB_TITLE","Mobile Buying Guide India");
	define("SEO_PHONE_FINDER_URL", WEB_URL . "Mobile-finder");
	define("SEO_COMPARE_PHONES_URL", WEB_URL . "Compare-mobiles");
	define("SEO_PHONE_LIST_URL", WEB_URL . "Mobile-finder/Brand-all/Price-all");
	define("SEO_PHONE_FINDER_RESULT_URL", WEB_URL . "Mobile-finder/Brand-all/Price-all");

	// SEO HOME PAGE
	define('SEO_HOME_TITLE','Mobile Price in India | Mobile Reviews & Comparison | Mobile Finder | Mobile Buying Guide at ' . SEO_DOMAIN );

	define('SEO_HOME_META_DESC','Mobile Buying Guide - Compare mobile phones with their specification, reviews, and price in India at ' . SEO_DOMAIN .' before buying a best mobile which suits your criteria.');

	define('SEO_HOME_KEWWORDS','Mobile phones, handset, cell phone, mobile guide, mobile models, mobile specification, compare mobiles, Mobile buying guide, Mobile Prices in India, Mobile Reviews, mobile comparison, mobile finder');

	// SEO COMPARE PHONES PAGE
	define('SEO_COMPARE_PHONES_PAGE_TITLE','Compare Mobile Phones Online in India - Choose Best Mobile Handset by Comparing Price, Brands, Reviews & Features at '. SEO_DOMAIN);

	define('SEO_COMPARE_PHONES_PAGE_META_DESC','Compare Mobile Phones Online in India - Choose best mobile handset by comparing price, brands, reviews & rating, features, specifications & all other required details at '. SEO_DOMAIN);

	define('SEO_COMPARE_PHONES_PAGE_KEWWORDS','Compare Mobiles India, compare mobiles, compare cell phones, Compare phones, Compare mobile handset,  , mobile Brands, mobile features, mobile specification, mobile prices, mobile handset model,  mobile price in India, devise type, weight');


	// SEO COMPARE PHONES PAGE for prodcuts
	define('SEO_COMPARE_PHONES_PAGE_TITLE_1','Compare PRODUCT_LIST - Compare Mobile Phones at '. SEO_DOMAIN);

	define('SEO_COMPARE_PHONES_PAGE_META_DESC_1','Compare PRODUCT_LIST - Choose Best Mobile Handset by Comparing Price, Brands, specifications, Features, reviews & ratings at '. SEO_DOMAIN);

	define('SEO_COMPARE_PHONES_PAGE_KEWWORDS_1','PRODUCT_LIST, Compare Mobiles India, compare mobiles, compare cell phones, Compare phones, Compare mobile handset,  , mobile Brands, mobile features, mobile specification, mobile prices');


	// SEO PHONE FINDER PAGE
	define('SEO_PHONE_FINDER_PAGE_TITLE','Mobile Phone Finder in India - Find Mobile Handset by Mobile Brands, Features, Specification & Price in India at '. SEO_DOMAIN);

	define('SEO_PHONE_FINDER_PAGE_META_DESC','Mobile Finder in India - Find best mobile handset by mobile brands, features, Specification, Price in India, devise type, weight, mobile OS, No. of SIM & form factor at ' . SEO_DOMAIN);

	define('SEO_PHONE_FINDER_PAGE_KEWWORDS','Mobile Brands, mobile features, mobile specification, mobile prices, mobile handset model,  mobile price in India, devise type, weight, mobile OS, No. of SIM, form factor');

	// SEO RESULT PAGE

	define('SEO_TITLE','<Brand> <Mobile-Type> <OS> Mobile Phones India - Get Details of <Brand> <Mobile-Type> <OS> Phones with Price Range <Price-Range> at ' . SEO_DOMAIN);

	define('SEO_DESC','<Brand> <Mobile-Type> <OS> Mobile Phones India - Get details of all <Brand> <Mobile-Type> <OS> phones with price Range <Price-Range> in India, features & reviews at ' . SEO_DOMAIN);

	// Mobile OS Only
	define('SEO_TAGS_OS','Mobile phones, <OS> mobiles, <OS> handsets, <OS> phones India, <OS> mobile phones in India, <OS> phones, <OS> phone price, mobile price in India');

	// Mobile Brand Only
	define('SEO_TAGS_BRAND','Mobile phones, <Brand> mobiles, <Brand> handsets, <Brand> phones India, <Brand> mobile phones in India, <Brand> phones, <Brand> phone price, mobile price in India');

	// Mobile Type Only
	define('SEO_TAGS_TYPE','Mobile phones, <Mobile-Type> mobiles, <Mobile-Type> handsets, <Mobile-Type> phones India, <Mobile-Type> mobile phones in India, <Mobile-Type> phones, <Mobile-Type> phone price, mobile price in India');

	// Mobile Brand & TYPE addition
	define('SEO_TAGS_BRAND_TYPE','<Brand> <Mobile-Type> phones, <Brand> <Mobile-Type> mobiles, <Brand> <Mobile-Type> handsets, <Brand> <Mobile-Type> phones India, <Brand> <Mobile-Type> mobile price in India');

	define("SEO_CAR_FINDER","Mobile-finder");
	define("SEO_COMPARE_URL","Compare-mobiles");
	define("SEO_USER_REVIEWS","Reviews/Write-User-Reviews");
?>
