<?php
	/** @defgroup include Include
	*  This is the Include  group
	*  @{
	*/
	/**
	* @version 1.0
	* @created 13-07-2011
	*/
        error_reporting(0);
	ini_set("display_errors", 0);
	ini_set("display_errors","Off");
	
        define("DB_HOST", "172.16.1.142");
        //define("DB_NAME", "mobile_buying_guide");
        //define("DB_USER", "mbuying_user");
        define("DB_NAME", "gadgets");
        define("DB_USER", "gadgetuser");
        define("DB_PASSWORD", "MBGuid3s@!");

	define("VERSION","1.14");

	define("COUNTRYID","1");
	define("SERVICEID","2");
	define("SERVICE","mobile");
	define("DOMAIN",".india.com");
	define("SERVICE_NAME","mobile");
	define("SITE_CATEGORY_ID","1");

        define("BASEPATH","/var/www/projects/gadgets.india.com/");
	define("ADMINPATH", BASEPATH . "admin/");
	define("CLASSPATH", BASEPATH . "classes/");
	define("UPLOAD_TMP_PATH", BASEPATH . "tmp/");
	define("TMP_UPLOAD_PATH", BASEPATH . "tmp/");
	define("UPLOAD_CLIENT_PATH", BASEPATH . "client/");

	define("SEO_DOMAIN","gadgets.india.com");
	define("SEO_WEB_URL","http://gadgets.india.com");

	define("WEB_URL", SEO_WEB_URL . "/");
	define("SITE_PATH", WEB_URL );
	define("JS_URL", WEB_URL . "js/");
	define("CSS_URL", WEB_URL . "css/");
	define("IMAGE_URL", WEB_URL . "images/");
	define("PLAYER_URL", WEB_URL . "player/");
	define("PLAYER_JS_URL", WEB_URL . "player/js/");
	define("ADMIN_WEB_URL", WEB_URL . "admin/");
	define("ADMIN_JS_URL", WEB_URL . "admin/js/");
	define("ADMIN_CSS_URL", WEB_URL . "admin/css/");
	define("ADMIN_IMAGE_URL", WEB_URL . "admin/images/");
	define("CATEGORY_HOME","<a href='".WEB_URL."'>Home</a> : ");

	define("CENTRAL_IMAGE_URL","http://media.ind.sh/");
	define("CENTRAL_MEDIA_URL","http://media.ind.sh/");
	define("IMAGE_SEARCH_URL","http://media.ind.sh/");
	define("UPLOAD_SEARCH_URL","http://media.ind.sh/apiserver.php");
	define("IMAGE_READER_FILE","http://media.ind.sh/apiserver.php");
	define("ORIGIN_CENTRAL_SERVER","http://media.ind.sh/");
	define("CENTRAL_API_SERVER","http://media.ind.sh/apiserver.php");
	define("UPLOAD_BASE_URL","http://media.ind.sh/upload.php");

	define("BREAD_CRUMB_STR","&nbsp;->&nbsp;");

	define("IMG_MAIN_SIZE","480X360");
	define("IMG_THUMB_SIZE","81X70");

	define("ON_RAOD_PRICE_TITLE","On Road Price");
	define("EX_SHOWROOM_STR","Ex-Showroom");

	$aSocialCategory=Array("1"=>"Question","2"=>"GENERAL","3"=>"QAnswer","4"=>"MB","5"=>"Campus report","6"=>"Campus","7"=>"SharedNote","8"=>"slideshow",'9'=>'article','10'=>'news');

	$aModuleImageResize = array("90X120","120X160");

	$aColorArray=array("0"=>array("color_code"=>"1","color_name"=>"Red"),"1"=>array("color_code"=>"2","color_name"=>"Yellow"),"2"=>array("color_code"=>"3","color_name"=>"Aqua"),"3"=>array("color_code"=>"4","color_name"=>"White"),"4"=>array("color_code"=>"5","color_name"=>"Blue"),"5"=>array("color_code"=>"6","color_name"=>"Green"));

	//Email invitation hosts
	$aEmailServices=array("1"=>"gmail","2"=>"yahoo","3"=>"hotmail","4"=>"rediff","5"=>"aol");

	//Social network service codes
	$aSocialNetwork=Array("1"=>"FACEBOOK","2"=>"TWITTER","3"=>"goggle buzz","4"=>"Yahoo buzz");
	//use this for MB categories  also
	$aSocialCategory=Array("1"=>"Question","2"=>"GENERAL","3"=>"QAnswer","4"=>"MB","5"=>"Campus report","6"=>"Campus","7"=>"SharedNote","8"=>"slideshow",'9'=>'article','10'=>'news');

	//social sharing urls.
	define('JQUERY_TOOL','http://cdn.jquerytools.org/1.2.5/jquery.tools.min.js');
	define('FB_JS_URL','http://static.ak.fbcdn.net/connect.php/js/FB.Share');
	define('TWITTER_JS_URL','http://platform.twitter.com/widgets.js');

	//feature yes/no images.
	define("FEATURE_YES_IMAGE_URL",IMAGE_URL."yes.gif");
	define("FEATURE_NO_IMAGE_URL",IMAGE_URL."no.gif");

	define("FRONT_PERPAGE","5");

	define("MAILERPATH",CLASSPATH.'phpmailer/');
	define("MAILER_HOST_ADDRESS","localhost");
	define("MAILER_USER_NAME","");
	define("MAILER_PWD","");
	define("MODULE_REPLY_TO","");

	//start code to add google add server key.Added by rajesh on dated 26-02-2011.
	define("GOOGLE_AD_API_KEY","ca-pub-6717584324019958");
	define("GOOGLE_AD_SERVICE_JS","http://partner.googleadservices.com/gampad/google_service.js");

	require_once(CLASSPATH.'DbOp.php');
	require_once(CLASSPATH.'Utility.php');
	//start code for video GA on dated 08-03-2011.
	define("VIDEO_GOOGLE_ANALYTICS","UA-21665814-1");

	define("ARTICLE_CATEGORYID","1");
	define("NEWS_CATEGORYID","2");
	define("VIDEO_CATEGORYID","3");
	define("SLIDESHOW_CATEGORYID","4");
	define("ONCARS_REVIEW_CATEGORYID","5");
	define("USER_REVIEW_MODEL_CATEGORY_ID","6");
	define("USER_REVIEW_VARIANT_CATEGORY_ID","7");
	define("VIDEO_REVIEW_CATEGORYID","8");
	define("VIDEO_ARTICLE_CATEGORYID","9");
	define("VIDEO_NEWS_CATEGORYID","10");

	define("VIEW_TRACKER_API_PATH", "http://social-dev.int.india.com/view.php");

	require_once(BASEPATH."include/expertratingArr.php");

	define("MEMCACHE_MASTER_KEY", SERVICE_NAME );
	require_once(CLASSPATH."memcache.class.php");

	define("EXCEL_DOWNLOAD_PATH", WEB_URL."format/");

	require_once(BASEPATH."include/config_SEO.php");
	

	define('MIN_PRICE',0);
	define('MAX_PRICE',100000);

	define('NO_PRODUCT_MESSAGE', "No products found for this criteria.");
        require_once(BASEPATH."include/config_VERSION.php");
        if(empty($_SERVER['SCRIPT_URI'])) $_SERVER['SCRIPT_URI']  = $_SERVER['REQUEST_URI'];
?>
