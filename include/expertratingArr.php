<?php
	$ratingAlgoArr['Is it a car?'] = array('expertrating-half-star','expertrating-disable-star','expertrating-disable-star','expertrating-disable-star','expertrating-disable-star');
	
	$ratingAlgoArr['Bad!'] = array('expertrating-full-star','expertrating-disable-star','expertrating-disable-star','expertrating-disable-star','expertrating-disable-star');

	$ratingAlgoArr['Stay away!'] = array('expertrating-full-star','expertrating-half-star','expertrating-disable-star','expertrating-disable-star','expertrating-disable-star');

	$ratingAlgoArr['Mediocre'] = array('expertrating-full-star','expertrating-full-star','expertrating-disable-star','expertrating-disable-star','expertrating-disable-star');

	$ratingAlgoArr['Does the job'] = array('expertrating-full-star','expertrating-full-star','expertrating-half-star','expertrating-disable-star','expertrating-disable-star');

	$ratingAlgoArr['Now we&#039;re talking!'] = array('expertrating-full-star','expertrating-full-star','expertrating-full-star','expertrating-disable-star','expertrating-disable-star');

	$ratingAlgoArr['Left us impressed'] = array('expertrating-full-star','expertrating-full-star','expertrating-full-star','expertrating-half-star','expertrating-disable-star');
	
	$ratingAlgoArr['Bang for the buck'] = array('expertrating-full-star','expertrating-full-star','expertrating-full-star','expertrating-full-star','expertrating-disable-star');
	
	$ratingAlgoArr['Almost the best'] = array('expertrating-full-star','expertrating-full-star','expertrating-full-star','expertrating-full-star','expertrating-half-star');
	
	$ratingAlgoArr['Best in class'] = array('expertrating-full-star','expertrating-full-star','expertrating-full-star','expertrating-full-star','expertrating-full-star');

	$rangeArr['Is it a car?'] = array('1','1.9');
	$rangeArr['Bad!'] = array('2','2.9');
	$rangeArr['Stay away!'] = array('3','3.9');
	$rangeArr['Mediocre'] = array('4','4.9');
	$rangeArr['Does the job'] = array('5','5.9');
	$rangeArr['Now we&#039;re talking!'] = array('6','6.9');
	$rangeArr['Left us impressed'] = array('7','7.9');
	$rangeArr['Bang for the buck'] = array('8','8.9');
	$rangeArr['Almost the best'] = array('9','9.5');
	$rangeArr['Best in class'] = array('9.6','10');
?>
