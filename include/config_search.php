<?php
	/** @defgroup include Include
	*  This is the Include  group
	*  @{
	*/
	/**
	* @version 1.0
	* @created 23-09-2011
	*/
	///// SEARCH DATA

	define('INPUTMECH',97);
	$arrInputMech[INPUTMECH][1]	= 'AlphaNumeric';
	$arrInputMech[INPUTMECH][2]	= 'QWERTY';
	$arrInputMech[INPUTMECH][3]	= 'Touchpad';
	$arrInputMech[INPUTMECH][4]	= 'AlphaNum & QWERTY';
	$arrInputMech[INPUTMECH][5]	= 'Touchpad & AlphaNum';
	$arrInputMech[INPUTMECH][6]	= 'Touchpad & QWERTY';
	$arrInputMech[INPUTMECH][7]	= 'AlphaNum & QWERTY & Touchpad';

	define('DUALSIM',112);
	$arrDualSIM[DUALSIM][1]	= 'YES';
	$arrDualSIM[DUALSIM][2]	= 'NO';

	define('WIFI',94);
	$arrWIFI[WIFI][1]	= 'YES';
	$arrWIFI[WIFI][2]	= 'NO';

	define('GPS',114);
	$arrGPS[GPS][1]	= 'YES';
	$arrGPS[GPS][2]	= 'NO';

	define('CAMRESOLUTION',110);
	$arrCamResolution[CAMRESOLUTION][0]	= '0';
	$arrCamResolution[CAMRESOLUTION][1]	= '0.1-1.9';
	$arrCamResolution[CAMRESOLUTION][2]	= '2';
	$arrCamResolution[CAMRESOLUTION][3]	= '3-4';
	$arrCamResolution[CAMRESOLUTION][5]	= '5';
	$arrCamResolution[CAMRESOLUTION][8]	= '8';
	$arrCamResolution[CAMRESOLUTION][12]= '12';


	define('OS',106);
	$arrOS[OS][1]	= 'Android';
	$arrOS[OS][2]	= 'iOS';
	$arrOS[OS][3]	= 'Symbian';
	$arrOS[OS][4]	= 'Windows Phone';
	$arrOS[OS][5]	= 'Blackberry';
	$arrOS[OS][6]	= 'Bada';
	$arrOS[OS][7]	= 'Others';

	define('RAM',111);
	$arrRAM[RAM][1]	= 256;
	$arrRAM[RAM][2]	= 512;
	$arrRAM[RAM][3]	= 1024;
	$arrRAM[RAM][4]	= 2048;
	$arrRAM[RAM][5]	= 4096;
	$arrRAM[RAM][6]	= 8192;

	define('WEIGHT',34);
	$arrWeight[WEIGHT][1]	= '0-100';
	$arrWeight[WEIGHT][2]	= '101-150';
	$arrWeight[WEIGHT][3]	= '151-1000';

	define('LENGTH',30);
	$arrLength[LENGTH][1]	= '0-50';
	$arrLength[LENGTH][2]	= '51-100';
	$arrLength[LENGTH][3]	= '101-150';

	define('BREADTH',31);
	$arrBreadth[BREADTH][1]	= '0-30';
	$arrBreadth[BREADTH][2]	= '31-60';
	$arrBreadth[BREADTH][3]	= '61-100';

	define('THICK',32);
	$arrThickness[THICK][1]	= '0-10';
	$arrThickness[THICK][2]	= '11-20';
	$arrThickness[THICK][3]	= '21-30';

	define('PHONETYPE',79);
	$arrPhoneType[PHONETYPE][1]	= 'Smart Phone';
	$arrPhoneType[PHONETYPE][2]	= 'Feature Phone';
	$arrPhoneType[PHONETYPE][3]	= 'Basic Phone';
	$arrPhoneType[PHONETYPE][4]	= 'Tablet';

	define('FORMFACTOR',86);
	$arrFormFactor[FORMFACTOR][1]	= 'Bar';
	$arrFormFactor[FORMFACTOR][3]	= 'Flip';
	$arrFormFactor[FORMFACTOR][4]	= 'Slide';
	$arrFormFactor[FORMFACTOR][5]	= 'Tablet';
	$arrFormFactor[FORMFACTOR][7]	= 'Folder';
	$arrFormFactor[FORMFACTOR][8]	= 'Swivel';
	$arrFormFactor[FORMFACTOR][2]	= 'Candy Bar';
	$arrFormFactor[FORMFACTOR][6]	= 'QWERTY Slider';
	$arrFormFactor[FORMFACTOR][9]	= 'Mixed';

	define('NET3G',6);
	$arr3GNetwork[NET3G][1]	= 'CDMA';
	$arr3GNetwork[NET3G][2]	= 'UMTS';
	$arr3GNetwork[NET3G][3]	= 'HSDPA';

	define('NET2G',109);
	$arr2GNetwork[NET2G][1]	= 'GSM';
	$arr2GNetwork[NET2G][2]	= 'CDMA';
	$arr2GNetwork[NET2G][3]	= 'GSM & CDMA';

	define('ANOUNCEDIN',9998);
	$arrAnnounced[ANOUNCEDIN][2007]	= 2007;
	$arrAnnounced[ANOUNCEDIN][2008]	= 2008;
	$arrAnnounced[ANOUNCEDIN][2009]	= 2009;
	$arrAnnounced[ANOUNCEDIN][2010]	= 2010;
	$arrAnnounced[ANOUNCEDIN][2011]	= 2011;

	define('AVAILABLEIN',9999);
	$arrAvaliable[AVAILABLEIN]['Available']	= 'Available In India';
	$arrAvaliable[AVAILABLEIN]['Abroad']	= 'Available Abroad';
	$arrAvaliable[AVAILABLEIN]['coming']	= 'Coming soon';
	$arrAvaliable[AVAILABLEIN]['Obselete']	= 'Obselete';
?>