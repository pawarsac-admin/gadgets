<?php	
	require_once('./include/config.php');
	require_once(CLASSPATH.'DbConn.php');
	require_once(CLASSPATH.'pivot.class.php');
	require_once(CLASSPATH.'feature.class.php');
	require_once(CLASSPATH.'category.class.php');
	require_once(CLASSPATH.'product.class.php');
	require_once(CLASSPATH.'brand.class.php');
	require_once(CLASSPATH.'citystate.class.php');
	require_once(CLASSPATH.'price.class.php');
	require_once(CLASSPATH.'compare.class.php');
	require_once(CLASSPATH.'Utility.php');
	require_once(CLASSPATH.'user_review.class.php');
	require_once(CLASSPATH.'videos.class.php');
	
	$dbconn			= new DbConn;
	$pivot			= new PivotManagement;
	$feature		= new FeatureManagement;
	$category		= new CategoryManagement;
	$product		= new ProductManagement;
	$brand			= new BrandManagement;
	$citystate		= new citystate;
	$price			= new price;
	$compare		= new compare;
	$userreview		= new USERREVIEW;
	$videoGallery	= new videos();

	//print_r($_REQUEST);
	//print_r($_SERVER);
	// Redirection code old URLs

	if(!isset($_REQUEST['catid'])){
		$URI	= $_SERVER['REQUEST_URI'];
		$arrURI	= explode('/',$URI);
		//print_r($arrURI);
		$sProductIds	= $arrURI[3];
		if(!empty($sProductIds)){
			$condition	= " product_id in ( $sProductIds ) ";
			$arrData	= $product->arrGetProducts( $condition, 'product_id', 'ASC' );
			foreach($arrData as $key => $value) {
				$arrPData[$value['product_id']]		= trim($value['product_full_name']);
			}
			//print_r($arrPData);
			$arrTemp1 = explode(',',$sProductIds);
			//print_r($arrTemp1);
			foreach($arrTemp1 as $key => $val){
				$arrFinal[] = $arrPData[$val];
			}
			//print_r($arrFinal);
			$sPFinal		= implode('-Vs-',$arrFinal );
			$sPFinal		= str_replace(' ','-',$sPFinal );
			$redirectUrl	= WEB_URL . 'Compare-mobiles/' . $sPFinal ."/1/$sProductIds/1"; 
			//echo "<br/>$redirectUrl";
			header('Location: ' . $redirectUrl );
			exit();
		}
	}
	//die;

	$category_id	= $_REQUEST['catid'] ? $_REQUEST['catid'] : SITE_CATEGORY_ID;
	$tab_id			= $_REQUEST['fid'] ? $_REQUEST['fid'] : 1;
	$productids		= $_REQUEST['productids'];
	$md5ProductIds	= md5($productids);
	$cookieProductIds = $_REQUEST['scomp'];
	
	$uid = $_REQUEST['uid'] ? $_REQUEST['uid'] : 0;

	if(!empty($productids)){
		$productArr		= explode(",",$productids);
	}
	$compareProductCnt = 0;
	$compareProductCnt = sizeof($productArr);
	if($compareProductCnt > 4){
	//	echo "Compare set is more than 4.";
		//exit;
	}

	if($compareProductCnt < 4){
		$pushCompareEmptyItem = 4 - $compareProductCnt;
		for($i=0;$i<$pushCompareEmptyItem;$i++){
			array_push($productArr,"x");
		}
	}

	
	function GetSeoVariantPageUrl($brand_name,$product_name,$variant,$product_id){
			//set seo url for product variant page.
			$variantnameSeoArr[] = SEO_WEB_URL;	
			$variantnameSeoArr[] = 'Mobile';	
			/*
			if(!empty($brand_name)){
				$variantnameSeoArr[] = seo_title_replace($brand_name).'-phones';
			}
			*/
			if(!empty($brand_name) && !empty($product_name)){
				$variantnameSeoArr[] = toAscii (seo_title_replace($brand_name).'-'.seo_title_replace($product_name));
			}
			if(!empty($variant)){
				$variantnameSeoArr[] = seo_title_replace($variant);
			}
			//$variantnameSeoArr[] = 'Overviews';
			$variantnameSeoArr[] = $product_id;
			$seo_url = implode("/",$variantnameSeoArr);
			return $seo_url;
	}

	
	$iflag=0;
	$productxml = "<PRODUCT_EX_SHOW_ROOM_PRICE>";
	$productxml .= "<COUNT><![CDATA[".sizeof($productArr)."]]></COUNT>";

	foreach($productArr as $key => $productid){
		unset($productNameArr);
		unset($brand_result);
		unset($result);
		$seo_variant_page_url = "";
		if(!empty($productid)){			
			$insert_param = array("product_id"=>$productid,"uid"=>$uid);
			if($md5ProductIds != $cookieProductIds){
				//used to save compare only 1 time for a single user.
				setcookie('scomp',$md5ProductIds);
				$save_compare_id = $compare->saveComparision($insert_param);
			}
			$result = $product->arrGetProductDetails($productid,$category_id,"",'1',"","","1","","","1");
			$product_id = $result[0]['product_id'];

			$video_path = str_replace(array(CENTRAL_IMAGE_URL,CENTRAL_MEDIA_URL),"",$result[0]['video_path']);
			if(!empty($video_path)){
				$result[0]['video_path'] = CENTRAL_MEDIA_URL.$video_path;			
			}
			$image_path = str_replace(array(CENTRAL_IMAGE_URL,CENTRAL_MEDIA_URL),"",$result[0]['image_path']);
			if(!empty($image_path)){
				$image_path = resizeImagePath($image_path,"120X160",$aModuleImageResize);
				$result[0]['image_path'] = CENTRAL_IMAGE_URL.$image_path;
			}

			$exshowroomprice = $result[0]['variant_value'];
			$exshowroomprice = $exshowroomprice ? 'Rs.'.priceFormat($exshowroomprice) : '';
			
			if(is_numeric($productid)) {
				$brand_result = $brand->arrGetBrandDetails($result[0]['brand_id']);
				$brand_name = trim($brand_result[0]['brand_name']);
			}else{
				$brand_name = '';
			}
		
			if(!empty($brand_name)){
				$productNameArr[] = $brand_name;
			}
			$product_name = trim($result[0]['product_name']);
			$productNameArr[] = $product_name;

			$variant = trim($result[0]['variant']);
			//print_r($result);
			$productNameArr[] = $variant;
			//print_r($productNameArr);
			//set seo url for product variant page.
			$seo_variant_page_url = GetSeoVariantPageUrl($brand_name,$product_name,$variant,$product_id);
			
			unset($subtitle);
			$subtitle[]	= $brand_name;
			$subtitle[]	= $product_name;
			$subtitle[]	= $variant;
			$sProduct	= implode(" ",$subtitle);
			$sProduct	= trim($sProduct);
			if($sProduct) {
				$arrProducts[]	=	$strsubtitle[]	= $sProduct;
			}
		
			//start code added by rajesh on dated 02-06-2011 for expert rating and graph.
			$rating_brand_id = $result[0]['brand_id'];
			$product_info_name = $result[0]['product_name'];

			$productresult = $product->arrGetProductNameInfo("",$category_id,$rating_brand_id,$product_info_name);
			$product_name_id = $productresult[0]['product_name_id'];
			
			$expertresult = $userreview->arrGetAdminExpertGrade($category_id,'','',$product_name_id);

			$design_rating = $expertresult[0]['design_rating'];
			$performance_rating = $expertresult[0]['performance_rating'];
			$user_rating = $expertresult[0]['user_rating'];
			
			$design_rating_proportion = ($design_rating*100)/10;
			$performance_rating_proportion = ($performance_rating*100)/10;
			$user_rating_proportion = ($user_rating*100)/10;

			$overallgrade = $expertresult[0]['overallgrade'];

			$rating_algo_key = "";
			foreach($rangeArr as $ratingkey => $range){
				if($overallgrade >= $range[0] && $overallgrade <= $range[1]){
					$rating_algo_key = $ratingkey;
					break;
				}
			}
			
			$expertratinghtml = '';
			$temprating = 1;
			$ratepos = $key+1;
			//PRINT_R($ratingAlgoArr);EXIT;
			if(sizeof($ratingAlgoArr[$rating_algo_key]) > 0){
				foreach($ratingAlgoArr[$rating_algo_key] as $classname){
					$expertratinghtml .= "<img class=\"$classname\" src=\"".IMAGE_URL."spacer.gif\" id=\"experts-img_".($ratepos)."_".$temprating."\" onmouseover=\"showratingToolTip('$ratepos','$temprating','1');\" onmouseout=\"hideratingToolTip('$ratepos','$temprating','1');\">";
					$temprating++;
				}
			}else{
				for($i=1;$i<=5;$i++){
					$expertratinghtml .= "<img class=\"expertrating-disable-star\" src=\"".IMAGE_URL."spacer.gif\" id=\"experts-img_".($ratepos)."_".$i."\" onmouseover=\"showratingToolTip('$ratepos','$i','1');\" onmouseout=\"hideratingToolTip('$ratepos','$i','1');\">";
				}
			}		


			if(is_numeric($productid)) {} else {
				$overallgrade = $expertratinghtml = $rating_algo_key = $expertratinghtml = $design_rating_proportion = $design_rating = $performance_rating_proportion = $performance_rating = $user_rating_proportion =$user_rating = '';
			}
			
			$rating_algo_key = $rating_algo_key ? $rating_algo_key : 'Not Yet Rated';
			$expertratingxml .= "<STAR_EXPERT_OVERALL_RATING_STR><![CDATA[".round($overallgrade)."]]></STAR_EXPERT_OVERALL_RATING_STR>";	$expertratingxml .= "<STAR_EXPERT_OVERALL_RATING_TOOLTIP><![CDATA[".$rangeToolTipArr[$rating_algo_key]."]]></STAR_EXPERT_OVERALL_RATING_TOOLTIP>";
			$expertratingxml .= "<STAR_EXPERT_GRAPH_RATING_STR><![CDATA[$expertratinghtml]]></STAR_EXPERT_GRAPH_RATING_STR>";
			$expertratingxml .= "<STAR_EXPERT_GRAPH_RATING_MSG><![CDATA[$rating_algo_key]]></STAR_EXPERT_GRAPH_RATING_MSG>";
			//$expertratinghtml =  ($rating_algo_key != 'onestar' && $rating_algo_key != 'halfstar' && $rating_algo_key != '') ? $expertratinghtml."($rating_algo_key)" : $expertratinghtml;

			$expertratingxml .= "<STAR_EXPERT_RATING_STR><![CDATA[$expertratinghtml]]></STAR_EXPERT_RATING_STR>";
			
			$expertratingxml .= "<EXPERT_DESIGN_RATING_PROPORTION><![CDATA[$design_rating_proportion]]></EXPERT_DESIGN_RATING_PROPORTION>";
			$expertratingxml .= "<EXPERT_DESIGN_RATING><![CDATA[$design_rating]]></EXPERT_DESIGN_RATING>";
			$expertratingxml .= "<EXPERT_PERFORMANCE_RATING_PROPORTION><![CDATA[$performance_rating_proportion]]></EXPERT_PERFORMANCE_RATING_PROPORTION>";
			$expertratingxml .= "<EXPERT_PERFORMANCE_RATING><![CDATA[$performance_rating]]></EXPERT_PERFORMANCE_RATING>";
			$expertratingxml .= "<EXPERT_USER_RATING_PROPORTION><![CDATA[$user_rating_proportion]]></EXPERT_USER_RATING_PROPORTION>";
			$expertratingxml .= "<EXPERT_USER_RATING><![CDATA[$user_rating]]></EXPERT_USER_RATING>";			
			
		//end code added by rajesh on dated 02-06-2011 for expert rating and graph.

		//start  code added by rajesh on dated 02-06-2011 for user.
			unset($reviewsresult);unset($totalcnt);
			$reviewsresult = $userreview->arrGetAdminOverallGrade($category_id,$rating_brand_id,"",$product_name_id);
			
			$reviewscnt = sizeof($reviewsresult);
			$overallcnt = 0;
			$overallavg = round($reviewsresult[0]['overallgrade']);
			if($reviewscnt <= 0){
				
				$reviewsresult = $userreview->arrGetOverallGrade($category_id,$rating_brand_id,"",$product_name_id);		
				$overallavg = round($reviewsresult[0]['overallavg']);
				$overallcnt = $reviewsresult[0]['totaloverallcnt'] ? $reviewsresult[0]['totaloverallcnt'] : 0;
				$totalcnt = $userreview->arrGetUserReviewDetailsCount("","","","","",$rating_brand_id,$category_id,$product_name_id);			
			}

			$brandresult = $brand->arrGetBrandDetails($rating_brand_id);
			$product_brand_name = html_entity_decode($brandresult[0]['brand_name'],ENT_QUOTES,'UTF-8');
			$product_brand_name = removeSlashes($product_brand_name);
			$product_brand_name = seo_title_replace($product_brand_name);
			$product_link_name = $product_brand_name."-".$product_info_name;
			$product_link_name = html_entity_decode($product_link_name,ENT_QUOTES,'UTF-8');
			$product_link_name = removeSlashes($product_link_name);
			$product_link_name = seo_title_replace($product_link_name);
			unset($seoTitleArr);
			$seoTitleArr[] = SEO_WEB_URL;
			$seoTitleArr[] = $product_brand_name."-phones";
			$seoTitleArr[] = $product_link_name;
			$seoTitleArr[] = "reviews-ratings";
			$seoTitleArr[] = SEO_phones_MODEL_REVIEWS; 
			$seoTitleArr[] = $product_info_name;
			$seoTitleArr[] = $product_name_id;
			$seo_model_url = implode("/",$seoTitleArr);
			unset($seoTitleArr);

			$html = "";
			for($grade=1;$grade<=5;$grade++){
				if($grade <= $overallavg){
					$html .= '<img src="'.IMAGE_URL.'spacer.gif" class="vsblStr"'." id=\"user-img_".($ratepos)."_".$grade."\" onmouseover=\"showratingToolTip('$ratepos','$grade','2');\" onmouseout=\"hideratingToolTip('$ratepos','$grade','2');\"".'/>';
				}else{
					$html .= '<img src="'.IMAGE_URL.'spacer.gif" class="dsblStr"'." id=\"user-img_".($ratepos)."_".$grade."\" onmouseover=\"showratingToolTip('$ratepos','$grade','2');\" onmouseout=\"hideratingToolTip('$ratepos','$grade','2');\"".'/>';
				}
			}
			if($overallavg == 1){
				$gradeStr = "Poor";
			}else if($overallavg == 2){
				$gradeStr = "Fair";
			}else if($overallavg == 3){
				$gradeStr = "Average";
			}else if($overallavg == 4){
				$gradeStr = "Good";
			}else if($overallavg == 5){
				$gradeStr = "Excellent";
			}

			if(is_numeric($productid)) {} else {
				$html = $gradeStr = $overallavg = $overallcnt = $totalcnt = '';
			}
			
			$expertratingxml .= "<OVERALL_AVG_HTML><![CDATA[$html]]></OVERALL_AVG_HTML>";
			$expertratingxml .= "<OVERALL_AVG_HTML_MSG><![CDATA[$gradeStr]]></OVERALL_AVG_HTML_MSG>";
			$expertratingxml .= "<OVERALL_AVG_CNT><![CDATA[$overallavg]]></OVERALL_AVG_CNT>";
			$expertratingxml .= "<OVERALL_TOTAL_CNT><![CDATA[$overallcnt]]></OVERALL_TOTAL_CNT>";
			$expertratingxml .= "<OVERALL_CNT><![CDATA[$totalcnt]]></OVERALL_CNT>";
			$expertratingxml .= "<SEO_MODEL_RATING_PAGE_URL><![CDATA[$seo_model_url]]></SEO_MODEL_RATING_PAGE_URL>";
			$expertratingxml .= "<EXPERT_USER_RATING_PRODUCT_ID><![CDATA[$productid]]></EXPERT_USER_RATING_PRODUCT_ID>";
			unset($html);unset($seo_model_url);unset($gradeStr);unset($overallavg);unset($overallcnt);unset($totalcnt);
			//end code added by rajesh on dated 02-06-2011 for user.
		}
		
		
		$compare_product_name = implode(" ",$productNameArr);
		$compare_product_name = trim($compare_product_name);
		if(!empty($compare_product_name)){
			$seoCompareProductArr[] = $compare_product_name;
		} else {
			$seoCompareProductArr[] = '';
		}
		$compare_prdct_img = $result[0]['image_path'];

		$productxml .= "<PRODUCT_EX_SHOW_ROOM_PRICE_DATA>";		
		$productxml .= "<PRODUCT_EX_SHOW_ROOM_PRICE><![CDATA[".trim($exshowroomprice)."]]></PRODUCT_EX_SHOW_ROOM_PRICE>";
		$productxml .= "<EX_SHOW_ROOM_PRICE_PRODUCT_ID><![CDATA[$productid]]></EX_SHOW_ROOM_PRICE_PRODUCT_ID>";
		$productxml .= "<COMPARE_PRODUCT_ID><![CDATA[$productid]]></COMPARE_PRODUCT_ID>";	
		$productxml .= "<COMPARE_PRODUCT_NAME><![CDATA[$compare_product_name]]></COMPARE_PRODUCT_NAME>";
		$productxml .= "<COMPARE_PRODUCT_IMG><![CDATA[$compare_prdct_img]]></COMPARE_PRODUCT_IMG>";
		$productxml .= "<COMPARE_PRODUCT_VARIANT_PAGE_URL><![CDATA[$seo_variant_page_url]]></COMPARE_PRODUCT_VARIANT_PAGE_URL>";
		$productxml .= "</PRODUCT_EX_SHOW_ROOM_PRICE_DATA>";		
		if(!empty($expertratingxml)){
			$ratingxmlArr[$key][]		= $expertratingxml;
			$ratingxmlArr[$key]['PID']	= "<PID>" . $productid ."</PID>";
		}
		$productid				= "";
		$exshowroomprice		= "";
		$compare_product_name	= "";
		$compare_prdct_img		= "";
		$html					= "";
		$expertratingxml		= "";
		$iflag++;
	}
	$productxml .= "</PRODUCT_EX_SHOW_ROOM_PRICE>";

	//if($_GET['debug']==1) { header('Content-type: text/xml');echo $productxml;exit; }

	//start code for compare set
	$ratecnt = sizeof($ratingxmlArr);
	$ratexml = "<RATING_MASTER>";
	for($i=0;$i<$ratecnt;$i++){
		$ratexml .= "<RATING_MASTER_DATA>";
		foreach($ratingxmlArr[$i] as $k=>$v){
			$ratexml .= $v;
		}
		$ratexml .= "</RATING_MASTER_DATA>";
	}
	$ratexml .= "</RATING_MASTER>";
	//end code for compare set

	if(!empty($category_id)){
		//$startlimit = 0;$limitcnt=2;
		$result = $feature->arrGetFeatureMainGroupDetails("",$category_id,"",$startlimit,$limitcnt);
	}
	
	$cnt = sizeof($result);
	for($i=0;$i<$cnt;$i++){
		$status = $result[$i]['status'];
		$main_group_id = $result[$i]['group_id'];
		$categoryid = $result[$i]['category_id'];
		$main_feature_group_name = $result[$i]['main_group_name'];
		
		if(!empty($categoryid)){
			$category_name = $category_result[0]['category_name'];
			$result[$i]['js_category_name'] = $category_name;
			$result[$i]['category_name'] = html_entity_decode($category_name,ENT_QUOTES,'UTF-8');
			if(strtolower($main_feature_group_name) == 'overview'){
				$overview_result = $compare->arrGetCompareOverview("","","","",$category_id);	
				
				$overview_result_cnt = sizeof($overview_result);
				for($overview=0;$overview<$overview_result_cnt;$overview++){
					$unit_id = "";$feature_unit="";
					$feature_overview_id = $overview_result[$overview]['feature_id'];
					$feature_overview_result = $feature->arrGetFeatureDetails($feature_overview_id);
					$overview_result[$overview]['feature_name'] = $feature_overview_result[0]['feature_name'];
					$feature_description = $feature_overview_result[0]['feature_description'];
					$unit_id = $feature_overview_result[0]['unit_id'];
					if(!empty($unit_id)){
							$unit_result = $feature->arrFeatureUnitDetails($unit_id);
							$feature_unit = $unit_result[0]['unit_name'];
					}
					if($feature_description != ""){
						$feature_description = html_entity_decode($feature_description,ENT_QUOTES,'UTF-8');
						$feature_description = str_replace('&amp;amp;',"",$feature_description);
						$feature_description = str_replace('&#039;',"'",$feature_description);
						$feature_description = str_replace('#039;',"'",$feature_description);
					}
					$overview_result[$overview]['feature_description'] = $feature_description;
					foreach($productArr as $key => $product_id){
						if(!empty($product_id)){
							$product_result = $product->arrGetProductFeatureDetails("",$feature_overview_id,$product_id);						
							//$overview_result[$overview]['product_feature'][$product_id]['product_feature_value'] = $product_result[0]['feature_value'];
							$feature_value = $product_result[0]['feature_value'];
							if(strtolower($feature_value) == 'yes'){
								$feature_value = 'yes';
							}else if(strtolower($feature_value) == 'no'){
								$feature_value = 'no';
							}
							$overview_result[$overview]['product_feature'][][$product_id]['product_feature_value'] = 
							 ($feature_value!='-' && strtolower($feature_value)!='yes' && strtolower($feature_value)!='no')? implode(" ",array($feature_value,$feature_unit)) : $feature_value;

						}
					}
					$main_feature_group = $overview_result[$overview]['main_feature_group'];
					$feature_overview_result = $feature->arrGetFeatureMainGroupDetails($main_feature_group);
					$main_group_name = $feature_overview_result[0]['main_group_name'];
					$overviewresult[$main_feature_group][] = $overview_result[$overview];
					$overviewresult[$main_feature_group]['main_group_name'] = $main_group_name;
				}
			}
				$feature_result = $feature->arrGetFeatureDetails("",$category_id,$main_group_id,"","");
		
				$featureCnt = sizeof($feature_result);
				for($j=0;$j<$featureCnt;$j++){
					$feature_group = $feature_result[$j]['feature_group'];
					$feature_sub_group_array = $feature->arrFeatureSubGroupDetails($feature_group,"","");
					$sub_group_name = $feature_sub_group_array[0]['sub_group_name'];
					$main_feature_group = $feature_result[$j]['main_feature_group'];
					$status = $feature_result[$j]['status'];
					$categoryid = $feature_result[$j]['category_id'];
					$feature_id = $feature_result[$j]['feature_id'];

					$unit_id = $feature_result[$j]['unit_id'];
					if(!empty($unit_id)){
							$unit_result = $feature->arrFeatureUnitDetails($unit_id);
							$feature_unit = $unit_result[0]['unit_name'];
					}
					if(!empty($feature_id)){
							$pivot_result = $pivot->arrGetPivotDetails("",$categoryid,$feature_id,"");
							foreach($productArr as $key => $product_id){
								if(!empty($product_id)){
									
									//sac
									if( !is_numeric($product_id)) $feature_unit = '';
									
									$product_result = $product->arrGetProductFeatureDetails("",$feature_id,$product_id);
									$feature_value = $product_result[0]['feature_value'];
									if(strtolower($feature_value) == 'yes'){
										$feature_value = 'yes';
									}else if(strtolower($feature_value) == 'no'){
										$feature_value = 'no';
									}
									$feature_result[$j]['product_feature'][$key]['feature_product_id'] = $product_id;
									$feature_result[$j]['product_feature'][$key]['product_feature_id'] = $product_result[0]['feature_id'];
									$feature_result[$j]['product_feature'][$key]['product_feature_value'] = ($feature_value!='-' && strtolower($feature_value)!='yes' && strtolower($feature_value)!='no')? implode(" ",array($feature_value,$feature_unit)) : $feature_value;
								}
							}
					}
					$feature_unit = "";
					$pivot_feature_id = $pivot_result[0]['feature_id'];
					$feature_result[$j]['pivot_feature_id'] = $pivot_feature_id;
					$feature_result[$j]['js_feature_name'] = $feature_result[$j]['feature_name'];
					$feature_result[$j]['js_feature_group'] = $feature_result[$j]['feature_group'];
					$feature_result[$j]['js_feature_desc'] = $feature_result[$j]['feature_description'];

					$feature_result[$j]['js_feature_unit'] = $feature_unit;
					$feature_result[$j]['feature_status'] = ($status == 1) ? 'Active' : 'InActive';
					$feature_result[$j]['feature_unit'] = $feature_unit ? html_entity_decode($feature_unit,ENT_QUOTES,'UTF-8') : 'Nil';
					$feature_result[$j]['feature_group'] = $feature_result[$j]['feature_group'] ? html_entity_decode($feature_result[$j]['feature_group'],ENT_QUOTES,'UTF-8') : 'Nil';
					$feature_description = $feature_result[$j]['feature_description'];
					if($feature_description != ""){
						$feature_description = html_entity_decode($feature_description,ENT_QUOTES,'UTF-8');
						$feature_description = str_replace('&amp;amp;',"",$feature_description);
						$feature_description = str_replace('&#039;',"'",$feature_description);
						$feature_description = str_replace('#039;',"'",$feature_description);
					}
					$feature_result[$j]['feature_description'] = $feature_description ? html_entity_decode($feature_description,ENT_QUOTES,'UTF-8') : 'Nil';
					$feature_result[$j]['create_date'] = date('d-m-Y',strtotime($feature_result[$j]['create_date']));
					$feature_result[$j]['js_feature_name'] = $feature_result[$j]['feature_name'];
					$feature_result[$j]['feature_name'] = $feature_result[$j]['feature_name'] ? html_entity_decode($feature_result[$j]['feature_name'],ENT_QUOTES,'UTF-8') : 'Nil';	

					$featureresult[$main_group_id][$feature_group][] = $feature_result[$j];
					$featureresult[$main_group_id][$feature_group]['sub_group_name'] = $feature_sub_group_array[0]['sub_group_name'];
					$featureresult[$main_group_id][$feature_group]['sub_group_id'] = $feature_group;
					$featureresult[$main_group_id][$feature_group]['pivot_feature_id'] = $pivot_feature_id;
					$featureresult[$main_group_id][$feature_group]['feature_id'] = $feature_id;
					
					
				}
				foreach($result[$i] as $k=> $v){
					$featureresult[$main_group_id][$k] = $v;
				}
			
		}
	}
	function strGetCompareXML($featureresult,$rootnode="GROUP_MASTER"){
		$groupnodexml .= "<$rootnode>";
		if($featureresult){
			foreach($featureresult as $maingroupkey => $maingroupval){
				 
				if(is_array($maingroupval)){
					
					$groupnodexml .= "<GROUP_MASTER_DATA>";
					foreach($maingroupval as $subgroupkey=>$subgroupval){
						
						if(is_array($subgroupval)){
							$groupnodexml .= "<SUB_GROUP_MASTER>";
							 foreach($subgroupval as $key => $featuredata){
								if(is_array($featuredata)){
									$groupnodexml .= "<SUB_GROUP_MASTER_DATA>";
									$featuredata = array_change_key_case($featuredata,CASE_UPPER);
									foreach($featuredata as $featurekey => $featureval){
										if(is_array($featureval)){
											//print_r($featureval);exit;
											$groupnodexml .= "<PRODUCT_FEATURE_MASTER>";
											foreach($featureval as $productid=>$productfeatureArr){
												$groupnodexml .= "<PRODUCT_FEATURE_MASTER_DATA>";
												$productfeatureArr = array_change_key_case($productfeatureArr,CASE_UPPER);
												foreach($productfeatureArr as $product_feature_key => $product_feature_value){
													$groupnodexml .= "<$product_feature_key><![CDATA[$product_feature_value]]></$product_feature_key>";
												}											
												$groupnodexml .= "</PRODUCT_FEATURE_MASTER_DATA>";
											}
											$groupnodexml .= "</PRODUCT_FEATURE_MASTER>";
										 }else{
											$groupnodexml .= "<$featurekey><![CDATA[$featureval]]></$featurekey>";
										 }
									}	
									$groupnodexml .= "</SUB_GROUP_MASTER_DATA>";
								}else{
									$groupnodexml .= "<".strtoupper($key)."><![CDATA[$featuredata]]></".strtoupper($key).">";
								}
							}
							$groupnodexml .= "</SUB_GROUP_MASTER>";
						}else{
							 $groupnodexml .= "<".strtoupper($subgroupkey)."><![CDATA[$subgroupval]]></".strtoupper($subgroupkey).">";
						}


					}
					$groupnodexml .= "</GROUP_MASTER_DATA>";
				}
			}
		}	
		$groupnodexml .= "</$rootnode>";
		return $groupnodexml;
	}
	$xml .= strGetCompareXML($featureresult);
	$seo_title = implode(" Vs ", array_filter($seoCompareProductArr) );
	$display_heading = $seo_title;
	if(!empty($seo_title)){
		$breadcrumb = CATEGORY_HOME."$seo_title";
	}else{
		$breadcrumb = CATEGORY_HOME."Compare phones";
	}
	$seo_title = "Compare Phones - $seo_title On phones India | Phone Comparison by Price, Performance, Phone Features & Specification on ".SEO_DOMAIN;
	$seo_desc = "Compare phones on ".SEO_DOMAIN." by comparing their price, features, technical specification and performance. Select minimum two and maximum four phones by their brand and model name.";
	$seo_tags = "Phone compare, phones compare, auto compare, Phone comparison, compare Phone by price, compare Phone by features, compare Phone by brands, compare phones at ".SEO_DOMAIN.", compare phones at on phones buying guide";

	// BRAND MASTER
	 if(!empty($category_id)){
                $result = $brand->arrGetBrandDetails("",$category_id);
        }
        $cnt = sizeof($result);
        $xml .= "<BRAND_MASTER>";
        $xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
        for($i=0;$i<$cnt;$i++){
                        $status = $result[$i]['status'];
                        $categoryid = $result[$i]['category_id'];
                        if(!empty($categoryid)){
                                        $category_result = $category->arrGetCategoryDetails($categoryid);
                        }
                        $category_name = $category_result[0]['category_name'];
                        $result[$i]['js_category_name'] = $category_name;
                        $result[$i]['category_name'] = html_entity_decode($category_name,ENT_QUOTES,'UTF-8');
                        $result[$i]['brand_status'] = ($status == 1) ? 'Active' : 'InActive';
                        $result[$i]['create_date'] = date('d-m-Y',strtotime($result[$i]['create_date']));
                        $result[$i]['js_brand_name'] = $result[$i]['brand_name'];
                        $result[$i]['brand_name'] = html_entity_decode($result[$i]['brand_name'],ENT_QUOTES,'UTF-8');
                        $result[$i] = array_change_key_case($result[$i],CASE_UPPER);
                        $xml .= "<BRAND_MASTER_DATA>";
                        foreach($result[$i] as $k=>$v){
                                        $xml .= "<$k><![CDATA[$v]]></$k>";
                        }
                        $xml .= "</BRAND_MASTER_DATA>";
        }
        $brandxml .= "</BRAND_MASTER>";





	$config_details = get_config_details();
	$strXML = "<XML>";

	if(count($arrProducts)> 0) {
		$sAllProducts	= implode(' Vs ',$arrProducts);
		$strXML	.= "<SEO_TITLE><![CDATA[".str_replace('PRODUCT_LIST',$sAllProducts,SEO_COMPARE_PHONES_PAGE_TITLE_1)."]]></SEO_TITLE>";
		$strXML	.= "<SEO_DESC><![CDATA[".str_replace('PRODUCT_LIST',$sAllProducts,SEO_COMPARE_PHONES_PAGE_META_DESC_1)."]]></SEO_DESC>";
		$sAllProducts	= implode(', ',$arrProducts);
		$strXML	.= "<SEO_KEWWORDS><![CDATA[".str_replace('PRODUCT_LIST',$sAllProducts,SEO_COMPARE_PHONES_PAGE_KEWWORDS_1)."]]></SEO_KEWWORDS>";
	} else {
		$strXML	.= "<SEO_TITLE><![CDATA[".SEO_COMPARE_PHONES_PAGE_TITLE."]]></SEO_TITLE>";
		$strXML	.= "<SEO_DESC><![CDATA[".SEO_COMPARE_PHONES_PAGE_META_DESC."]]></SEO_DESC>";
		$strXML	.= "<SEO_KEWWORDS><![CDATA[".SEO_COMPARE_PHONES_PAGE_KEWWORDS."]]></SEO_KEWWORDS>";
	}
	
	$strXML .= "<IS_MOST_POPULAR_SET><![CDATA[$ismostpopular]]></IS_MOST_POPULAR_SET>";
	$strXML .= "<DISPLAY_HEADING><![CDATA[$display_heading]]></DISPLAY_HEADING>";
	$strXML .= "<NO_OF_COMPARISONS><![CDATA[$compareProductCnt]]></NO_OF_COMPARISONS>";
	$strXML .= "<SEO_TITLE><![CDATA[$seo_title]]></SEO_TITLE>";
	$strXML .= "<SUB_TOP_TITLE><![CDATA[ " . $strsubtitle[0] . "]]></SUB_TOP_TITLE>";
	$strXML .= "<BREAD_CRUMB><![CDATA[$breadcrumb]]></BREAD_CRUMB>";
	$strXML .= "<SEO_DESC><![CDATA[$seo_desc]]></SEO_DESC>";
	$strXML .= "<SEO_TAGS><![CDATA[$seo_tags]]></SEO_TAGS>";
	$strXML .= "<MSG><![CDATA[$msg]]></MSG>";
	$strXML .= "<STARTLIMIT><![CDATA[$startlimit]]></STARTLIMIT>";
	$strXML .= "<CNT><![CDATA[$limitcnt]]></CNT>";
	$strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$category_id]]></SELECTED_CATEGORY_ID>"; 
	$strXML .= "<SELECTEDTABID><![CDATA[$tab_id]]></SELECTEDTABID>";
	$strXML .= "<COMPARE_PRODUCT_SET><![CDATA[$productids]]></COMPARE_PRODUCT_SET>";
	$strXML .= "<TAB_QUERY_STR><![CDATA[$tabQueryStr]]></TAB_QUERY_STR>";
	$strXML .= "<SEO_COMPARE_URL><![CDATA[".SEO_COMPARE_URL."]]></SEO_COMPARE_URL>";
	$strXML .= "<FEATURE_YES_IMAGE_URL><![CDATA[".FEATURE_YES_IMAGE_URL."]]></FEATURE_YES_IMAGE_URL>";
	$strXML .= "<FEATURE_NO_IMAGE_URL><![CDATA[".FEATURE_NO_IMAGE_URL."]]></FEATURE_NO_IMAGE_URL>";
	$strXML .= $overviewxml;
	$strXML .= $config_details;
	$strXML .= $comparesetxml;
	$strXML .= $productxml;
	$strXML .= $xml;
	$strXML .= $brandxml;
	$strXML .= "<OC_ROS_BOTTOM_NORTH_728x90><![CDATA[OC_ROS_Bottom_North_728x90]]></OC_ROS_BOTTOM_NORTH_728x90>";
	$strXML .= "<OC_ROS_TOP_RHS_LREC_300x250_1><![CDATA[OC_ROS_Top_RHS_Lrec_300x250_1]]></OC_ROS_TOP_RHS_LREC_300x250_1>";
	$strXML .= "<PAGE_NAME>".$_SERVER['SCRIPT_URI']."</PAGE_NAME>";
	$strXML .= $ratexml;
	$most_recent_one_result_list = $videoGallery->getarrMostRecentVideosHeaderTabLink($category_id,"0","1",$array_result);
	$strXML.="<MOST_RECENT_VIDEO_HEADER_LINK><![CDATA[".$most_recent_one_result_list."]]></MOST_RECENT_VIDEO_HEADER_LINK>";
	$strXML	.= "<SELECTED_NAV_TAB>3</SELECTED_NAV_TAB>";
	$strXML .= "</XML>";
	$strXML = mb_convert_encoding($strXML, "UTF-8");
	if($_GET['debug']==1) { header('Content-type: text/xml');echo $strXML;exit; }
	$doc = new DOMDocument();
	$doc->loadXML($strXML);
	$doc->saveXML();
	$xslt = new xsltProcessor;
	$xsl = DOMDocument::load('xsl/mobile-compare.xsl');
	$xslt->importStylesheet($xsl);
	print $xslt->transformToXML($doc);
?>
