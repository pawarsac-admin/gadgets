<?php

$BASEPATH	= (defined('BASEPATH'))? BASEPATH : ''; 

require_once($BASEPATH.'include/config.php');
require_once(CLASSPATH.'DbConn.php');
require_once(CLASSPATH.'topstories.class.php');
require_once(CLASSPATH.'product.class.php');
require_once(CLASSPATH.'brand.class.php');

$dbconn		= new DbConn;
$oProduct	= new ProductManagement;
$oBrand		= new BrandManagement;


$productData	= $oProduct->arrGetProductDetails('','','',1,'','',1,'','',1,'ORDER BY PRODUCT_MASTER.brand_id,PRODUCT_MASTER.product_id DESC');
$brandData  = $oBrand->arrGetBrandDetailsInAssociativeForm();
//print_r($brandData);
//print_r($productData);

if($productData	){
	
	foreach( $productData as $key => $value ){
		$tStr 							.= "{ label:'" .  $brandData[$value['brand_id']] . " " . $value['product_name'] . "', ";
		$tStr 							.= " brand:'" .  $brandData[$value['brand_id']] . "' ,";
		$tStr 							.= " product_id:'" .  $value['product_id'] . "' }, ";
		$arrbrand[$value['brand_id']] 	 = $brandData[$value['brand_id']] ;
	}
	$tStr		= substr($tStr,0,-2);
	
	 foreach( $arrbrand as $key => $value ){
                $brandStr       .= "'".$value . "', " ;
        }
	$brandStr	= substr($brandStr,0,-2);
	//echo $tStr;
}

//$pStr	=  '$(function() { var productData = [' . $tStr . '];});';
$pStr	=  'var productData = [' . $tStr . '];';

//$bStr	=  '$(function() { var availableBrands = [' . $brandStr  . '];})';
$bStr	=  'var availableBrands = [' . $brandStr  . '];';
	


$filename = 'js/autocomplete1.js';
$somecontent = $pStr . "\n\n" .  $bStr . "\n" ;

    // In our example we're opening $filename in append mode.
    // The file pointer is at the bottom of the file hence
    // that's where $somecontent will go when we fwrite() it.
    if (!$handle = fopen($filename, 'w+')) {
         echo "Cannot open file ($filename)";
         exit;
    }

    // Write $somecontent to our opened file.
    if (fwrite($handle, $somecontent) === FALSE) {
        echo "Cannot write to file ($filename)";
        exit;
    }

	if($_REQUEST['debug']==1)
    echo "Success, wrote ($somecontent) to file ($filename)";

    fclose($handle);
	
	// generate version config_VERSION.php
   
	require_once($BASEPATH.'include/config_VERSION.php');
	$version_dfv	= VERSION_DYNAMIC + 1;
	$ver_fv			= '<?php define("VERSION_DYNAMIC","'.$version_dfv.'");?>';
	if($version_dfv){
		if(file_exists($BASEPATH.'include/config_VERSION.php')){
		   chmod($BASEPATH.'include/config_VERSION.php', 0777);    
		}	
		$fp			= fopen($BASEPATH.'include/config_VERSION.php','w');
		fwrite($fp,$ver_fv);
		fclose($fp);	
	}
?>