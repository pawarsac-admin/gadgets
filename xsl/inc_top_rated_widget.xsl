<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet  [
  <!ENTITY nbsp   "&#160;">
  <!ENTITY copy   "&#169;">
  <!ENTITY reg    "&#174;">
  <!ENTITY trade  "&#8482;">
  <!ENTITY mdash  "&#8212;">
  <!ENTITY ldquo  "&#8220;">
  <!ENTITY rdquo  "&#8221;">
  <!ENTITY pound  "&#163;">
  <!ENTITY yen    "&#165;">
  <!ENTITY euro   "&#8364;">
  <!ENTITY deg "&#176;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fb="http://www.facebook.com/2008/fbml" version="1.0">
  <xsl:output method="html" encoding="utf-8" indent="yes" doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"/>
  <xsl:template name="topRatedWidget">
   <div class="top-rate-sell">
                <div class="top-rate-tabs">
                  <ul>
                    <li>
                      <a id="topRated" href="javascript:undefined;" class="active" onclick="return false" >Top Rated Phones</a>
                    </li>
                    <!--li>
                      <a id="topSell" href="javascript:undefined;" onclick="return false" >Top Selling Phones</a>
                    </li-->
                    <div class="clear"></div>
                  </ul>
                  <div class="clear"></div>
                </div>
                <div class="clear"></div>
                <div id="topRatedContent">

				<xsl:for-each select="XML/TOP_RATED/STORIES/STORY" >
				  <div class="rate rateWidth{position()}">
							<a onclick="_gaq.push(['_trackEvent', 'Top Rated Phones', 'clicked', 'Phone Name']);"  href="{LINKURL}" ><xsl:value-of select="PRODUCT_NAME" /></a>
							<div class="clear"></div>
						  </div>
						  <div class="rateNo"><xsl:value-of select="position()" />
							<div class="clear"></div>
						  </div>
						  <div class="clear"></div>
				</xsl:for-each>

                 <div class="clear"></div>
                </div>
                <div class="clear"></div>
                <div id="topSellContent" style="display:none">
                  <div class="rate rateWidth1">
                    <a href="javascript:undefined;">Samsung Galaxy S-LCD</a>
                    <div class="clear"></div>
                  </div>
                  <div class="rateNo">1
                    <div class="clear"></div>
                  </div>
                  <div class="clear"></div>
                  <div class="rate rateWidth2">
                    <a href="javascript:undefined;">LG Optimus 2x (P990)</a>
                    <div class="clear"></div>
                  </div>
                  <div class="rateNo">2
                    <div class="clear"></div>
                  </div>
                  <div class="clear"></div>
                  <div class="rate rateWidth3">
                    <a href="javascript:undefined;">Samsung Galaxy S II</a>
                    <div class="clear"></div>
                  </div>
                  <div class="rateNo">3
                    <div class="clear"></div>
                  </div>
                  <div class="clear"></div>
                  <div class="rate rateWidth4">
                    <a href="javascript:undefined;">Olive Fluid V-W1</a>
                    <div class="clear"></div>
                  </div>
                  <div class="rateNo">4
                    <div class="clear"></div>
                  </div>
                  <div class="clear"></div>
                  <div class="rate rateWidth5">
                    <a href="javascript:undefined;">Nokia E7</a>
                    <div class="clear"></div>
                  </div>
                  <div class="rateNo">5
                    <div class="clear"></div>
                  </div>
                  <div class="clear"></div>
                </div>
                <div class="clear"></div>
                <div class="clear"></div>
              </div>
  </xsl:template>
</xsl:stylesheet>

