<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet  [
  <!ENTITY nbsp   "&#160;">
  <!ENTITY copy   "&#169;">
  <!ENTITY reg    "&#174;">
  <!ENTITY trade  "&#8482;">
  <!ENTITY mdash  "&#8212;">
  <!ENTITY ldquo  "&#8220;">
  <!ENTITY rdquo  "&#8221;"> 
  <!ENTITY pound  "&#163;">
  <!ENTITY yen    "&#165;">
  <!ENTITY euro   "&#8364;">
  <!ENTITY deg "&#176;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fb="http://www.facebook.com/2008/fbml" version="1.0">
  <xsl:output method="html" encoding="utf-8" indent="yes" doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"/>
  <xsl:include href="inc_header.xsl" />
  <xsl:include href="inc_footer.xsl" />
  <xsl:template match="/">
  <div>
	<xsl:for-each select="XML/BGR_FAVORITES/STORIES/STORY" >	
	  <div class="single-phone-model">
		<img src="{IMAGE_PATH}" alt="" />
		<div class="phone-showcase-img-info">
		  <span><a href="{LINKURL}"><xsl:value-of select="PRODUCT_NAME" /></a></span>
		</div>
	  </div>
	  <xsl:if test="position()=6">
	    <div class="clear"></div>
	    <div class="phone-showcase-seperator"></div>
	  </xsl:if>
	</xsl:for-each>	

	<div class="clear"></div>
	<div class="morelink">
	<a href="javascript:undefined;">More Phones...</a>
	</div>
  </div>
  </xsl:template>
</xsl:stylesheet>
