<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet  [
  <!ENTITY nbsp   "&#160;">
  <!ENTITY copy   "&#169;">
  <!ENTITY reg    "&#174;">
  <!ENTITY trade  "&#8482;">
  <!ENTITY mdash  "&#8212;">
  <!ENTITY ldquo  "&#8220;">
  <!ENTITY rdquo  "&#8221;"> 
  <!ENTITY pound  "&#163;">
  <!ENTITY yen    "&#165;">
  <!ENTITY euro   "&#8364;">
  <!ENTITY deg "&#176;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fb="http://www.facebook.com/2008/fbml" version="1.0">
  <xsl:output method="html" encoding="utf-8" indent="yes" doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"/>
  <xsl:include href="inc_header.xsl" />
  <xsl:include href="inc_footer.xsl" />
  <xsl:template match="/">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>
          <xsl:value-of select="/XML/SEO_TITLE" disable-output-escaping="yes"/>
        </title>
        <meta name="Keywords" content="{/XML/SEO_KEWWORDS}" />
        <meta name="Description" content="{/XML/SEO_DESC}" />
        <link href="{/XML/CSS_URL}style.css?version={/XML/VERSION}" rel="stylesheet" type="text/css" />
		<script src="{/XML/JS_URL}compare.js?version={/XML/VERSION}"></script>

        <script>
		var siteURL		= '<xsl:value-of select="/XML/WEB_URL" disable-output-escaping="yes"/>';
		var comparedIds = '<xsl:value-of select="/XML/COMPARE_PRODUCT_SET" disable-output-escaping="yes"/>';
		var compareCount	= comparedIds.split(',').length;
		var catid		= '<xsl:value-of select="/XML/SELECTED_CATEGORY_ID" disable-output-escaping="yes"/>';
		var currenttab	= '<xsl:value-of select="/XML/SELECTEDTABID" disable-output-escaping="yes"/>';
		var isFirst = false;
		var newCompareArr = Array();
        </script>
		<script type="text/javascript">
		// Google Analytics start
		  var _gaq = _gaq || [];
		  _gaq.push(['_setAccount', 'UA-27114870-1']);
		  _gaq.push(['_trackPageview']);

		  (function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();
		// Google Analytics end
		//<!-- Quantcast Tag part 1-->

		var _qevents = _qevents || [];

		(function() {
		var elem = document.createElement('script');
		elem.src = (document.location.protocol == "https:" ? "https://secure" : "http://edge") + ".quantserve.com/quant.js";
		elem.async = true;
		elem.type = "text/javascript";
		var scpt = document.getElementsByTagName('script')[0];
		scpt.parentNode.insertBefore(elem, scpt);
		})();

		_qevents.push({
		qacct:"p-31f3D02tYU8zY"
		});
		</script>
		<!-- Quantcast Tag part 1-->
      </head>

      <body>
<!-- Quantcast Tag part 2-->
<noscript>
<div style="display:none;">
<img src="//pixel.quantserve.com/pixel/p-31f3D02tYU8zY.gif" border="0" height="1" width="1" alt="Quantcast"/>
</div>
</noscript>
<!--Quantcast Tag part 2 -->
        <div id="wrapper">
          <!-- header start -->
          <xsl:call-template name="headDiv"/>
          <!-- header end -->
          <div class="clear"></div>
          <!-- body starts -->
          <div id="maincontent-wrapper">
            <section class="fullwidth">
            <h1 class="page-heading"><a href="{/XML/WEB_URL}" class="home spriteicon"></a> 
			<!--span> <a href="{/XML/WEB_URL}">Home</a> / <a href="javascript:undefined;">Compare Phones</a></span-->
			<span><xsl:value-of select="/XML/BREAD_CRUMB" disable-output-escaping="yes"/></span>

			</h1>
              <div class="mobile-compare-wrap">
				<div style="float:right"></div>

              <h2>Compare Phones</h2>
                <!--<div class="mobile-compare-tabwrap">
                  <a href="javascript:undefined;" id="compare-phones" class="active">
                    <span>Compare phones</span>
                  </a>
                  <a href="javascript:undefined;" id="search-phones">
                    <span>Search phones</span>
                  </a>
                </div> -->
                <xsl:choose>
                  <xsl:when test="count(/XML/PRODUCT_EX_SHOW_ROOM_PRICE/PRODUCT_EX_SHOW_ROOM_PRICE_DATA)&lt;=0">
					<div class="mobile-compare-table-wrap">
                      <table width="100%" border="1" cellspacing="0" cellpadding="0">
                        <tr class="titlebg">
							<td class="last">
							<div class="proname">Product name</div>
							</td>
							<td class=""></td>
							<td class=""></td>
							<td class=""></td>
							<td class="last"></td>
                        </tr>

						<tr>
                          <td>
                            <div class=""></div>
                          </td>


							<td class="">
							<div class="prod-select-wrap" id="sel_div_1" >
								<select  name="Brand_1" id="Brand_1" onchange="getModelByBrand('1','get_model_detail.php','Model_1','')">
								  <option value="">--Select Brand--</option>
								  <xsl:for-each select="/XML/BRAND_MASTER/BRAND_MASTER_DATA">
									<option value="{BRAND_ID}">
									  <xsl:value-of select="BRAND_NAME" disable-output-escaping="yes"/>
									</option>
								  </xsl:for-each>
								</select>
								<select name="Model_1" id="Model_1" onchange="getVariantByBrandModel('1','get_variant_detail.php','ModelVariant_1','')">
								  <option value="">--Select Model--</option>
								</select>
								
								<div class="clear"></div>
							</div>
							</td>


							<td class="">
							<div class="prod-select-wrap" id="sel_div_2" >
								<select  name="Brand_2" id="Brand_2" onchange="getModelByBrand('2','get_model_detail.php','Model_2','')">
								  <option value="">--Select Brand--</option>
								  <xsl:for-each select="/XML/BRAND_MASTER/BRAND_MASTER_DATA">
									<option value="{BRAND_ID}">
									  <xsl:value-of select="BRAND_NAME" disable-output-escaping="yes"/>
									</option>
								  </xsl:for-each>
								</select>
								<select name="Model_2" id="Model_2" onchange="getVariantByBrandModel('2','get_variant_detail.php','ModelVariant_2','')">
								  <option value="">--Select Model--</option>
								</select>
								<div class="clear"></div>
							</div>
							</td>


							<td class="">
							<div class="prod-select-wrap" id="sel_div_3" >
								<select  name="Brand_3" id="Brand_3" onchange="getModelByBrand('3','get_model_detail.php','Model_3','')">
								  <option value="">--Select Brand--</option>
								  <xsl:for-each select="/XML/BRAND_MASTER/BRAND_MASTER_DATA">
									<option value="{BRAND_ID}">
									  <xsl:value-of select="BRAND_NAME" disable-output-escaping="yes"/>
									</option>
								  </xsl:for-each>
								</select>
								<select name="Model_3" id="Model_3" onchange="getVariantByBrandModel('3','get_variant_detail.php','ModelVariant_3','')">
								  <option value="">--Select Model--</option>
								</select>
								<div class="clear"></div>
							</div>
							</td>


							<td class="last">
							<div class="prod-select-wrap" id="sel_div_4" >
								<select  name="Brand_4" id="Brand_4" onchange="getModelByBrand('4','get_model_detail.php','Model_4','')">
								  <option value="">--Select Brand--</option>
								  <xsl:for-each select="/XML/BRAND_MASTER/BRAND_MASTER_DATA">
									<option value="{BRAND_ID}">
									  <xsl:value-of select="BRAND_NAME" disable-output-escaping="yes"/>
									</option>
								  </xsl:for-each>
								</select>
								<select name="Model_4" id="Model_4" onchange="getVariantByBrandModel('4','get_variant_detail.php','ModelVariant_4','')">
								  <option value="">--Select Model--</option>
								</select>
								<div class="clear"></div>
							</div>
							</td>
					 
                        </tr>

						<tr>
						<td colspan="5" align="center"> <a href="javascript:undefined;" class="compare-Horizontal spriteicon" onclick="_gaq.push(['_trackEvent', 'Compare Phones', 'clicked', 'Compare  Button']);compareIt1();" id="compare-btn"></a>  </td>
						</tr>

						</table>
					</div>
                 </xsl:when>
                  <xsl:otherwise>
                    <div class="mobile-compare-table-wrap">
                      <table width="100%" border="1" cellspacing="0" cellpadding="0">
                        <tr class="titlebg">
                          <td class="last">
                            <div class="proname">Product name</div>
                          </td>
                          <xsl:for-each select="/XML/PRODUCT_EX_SHOW_ROOM_PRICE/PRODUCT_EX_SHOW_ROOM_PRICE_DATA">
                            <xsl:variable name="pricelastclass">
                              <xsl:if test="position()=last()">last</xsl:if>
                            </xsl:variable>
                            <td class="{$pricelastclass}">
                              <div class="titlebg">
                               <a href="{COMPARE_PRODUCT_VARIANT_PAGE_URL}">
                                 <xsl:value-of select="COMPARE_PRODUCT_NAME" disable-output-escaping="yes"/>
                               </a>
                              </div>
                            </td>
                          </xsl:for-each>
                        </tr>
                        <tr>
                          <td>
                            <div class=""></div>
                          </td>
                          <xsl:for-each select="/XML/PRODUCT_EX_SHOW_ROOM_PRICE/PRODUCT_EX_SHOW_ROOM_PRICE_DATA">
                            <xsl:variable name="pricelastclass">
                              <xsl:if test="position()=last()">last</xsl:if>
                            </xsl:variable>
                            <td class="{$pricelastclass}">
                            
							<xsl:choose>							
							<xsl:when test="COMPARE_PRODUCT_NAME!=''">
							     
								 <div class="prod-img">
                                 <a href="{COMPARE_PRODUCT_VARIANT_PAGE_URL}">
                                   
									<xsl:choose>
									<xsl:when test="COMPARE_PRODUCT_IMG=''">
										<img src="/images/preset1.gif" alt="{COMPARE_PRODUCT_NAME}" title="{COMPARE_PRODUCT_NAME}" />
									</xsl:when>
									<xsl:otherwise>
										<img src="{COMPARE_PRODUCT_IMG}" alt="{COMPARE_PRODUCT_NAME}" title="{COMPARE_PRODUCT_NAME}"/>
									</xsl:otherwise>
									</xsl:choose>

                                 </a>
                              </div>
					  
							<input type="hidden" id="Model_{position()}" value="{EX_SHOW_ROOM_PRICE_PRODUCT_ID}" />
							<input type="hidden" id="Brand_{position()}" value="{EX_SHOW_ROOM_PRICE_PRODUCT_ID}" />
							<input type="hidden" id="Model_name_{position()}" value="{COMPARE_PRODUCT_NAME}" />

							</xsl:when>
							<xsl:otherwise>
							<div class="prod-select-wrap" id="sel_div_{position()}" >

								<select  name="Brand_{position()}" id="Brand_{position()}" class="w100pc mt5" onchange="getModelByBrand('{position()}','get_model_detail.php','Model_{position()}','')">
                                  <option value="">--Select Brand--</option>
                                  <xsl:for-each select="/XML/BRAND_MASTER/BRAND_MASTER_DATA">
                                    <option value="{BRAND_ID}">
                                      <xsl:value-of select="BRAND_NAME" disable-output-escaping="yes"/>
                                    </option>
                                  </xsl:for-each>
                                </select>

                                
								<select name="Model_{position()}" id="Model_{position()}" class="w100pc mt5" onchange="getVariantByBrandModel('{position()}','get_variant_detail.php','ModelVariant_{position()}','')">
                                  <option value="">--Select Model--</option>
                                </select>

                                <div class="clear"></div>
                  			</div>
							</xsl:otherwise>
							</xsl:choose>
                            </td>
                          </xsl:for-each>
                        </tr>
						<tr>
						<td colspan="5" align="center"> <a href="javascript:undefined;" class="compare-Horizontal spriteicon" onclick="_gaq.push(['_trackEvent', 'Compare Phones', 'clicked', 'Compare  Button']);compareIt1();" id="compare-btn"></a>  </td>
						</tr>
                        <tr class="titlebgray-bg">
                          <td class="firstcol">Price*</td>
                          <td>&nbsp;</td>
                          <td>&nbsp;</td>
                          <td>&nbsp;</td>
                          <td class="last">&nbsp;</td>
                        </tr>
                        <tr>
                          <td class="firstcol">&nbsp;</td>
                          <xsl:for-each select="/XML/PRODUCT_EX_SHOW_ROOM_PRICE/PRODUCT_EX_SHOW_ROOM_PRICE_DATA">
                            <xsl:variable name="pricelastclass">
                              <xsl:if test="position()=last()">last</xsl:if>
                            </xsl:variable>
                            <td class="{$pricelastclass}">
                              <xsl:choose>
                                <xsl:when test="PRODUCT_EX_SHOW_ROOM_PRICE!=''">
                                  <xsl:value-of select="PRODUCT_EX_SHOW_ROOM_PRICE" disable-output-escaping="yes"/>
                                </xsl:when>
                                <xsl:otherwise>
                                   <span class="noprice"> Not Available</span>
                                </xsl:otherwise>
                              </xsl:choose>
                            </td>
                          </xsl:for-each>
                        </tr>
                        <tr class="titlebgray-bg">
                          <td class="firstcol">Ratings</td>
                          <td>&nbsp;</td>
                          <td>&nbsp;</td>
                          <td>&nbsp;</td>
                          <td class="last">&nbsp;</td>
                        </tr>
                        <tr>
                          <td class="firstcol">BGR Ratings</td>
                          <xsl:for-each select="/XML/RATING_MASTER/RATING_MASTER_DATA">
                            <xsl:variable name="pricelastclass">
                              <xsl:if test="position()=last()">last</xsl:if>
                            </xsl:variable>
                            <td class="{$pricelastclass}">
                              <xsl:choose>
                                <xsl:when test="STAR_EXPERT_GRAPH_RATING_MSG!='Not Yet Rated'">
									<xsl:value-of select="STAR_EXPERT_OVERALL_RATING_STR" disable-output-escaping="yes"/>/5
                                </xsl:when>
                                <xsl:otherwise>
								<xsl:if test="PID &gt;=0">
									This Phone has not yet been Reviewed by Expert Panel of mobile buying guide.
							  </xsl:if>
                                </xsl:otherwise>
                              </xsl:choose>
                            </td>
                          </xsl:for-each>
                        </tr>
                        <xsl:for-each select="/XML/GROUP_MASTER/GROUP_MASTER_DATA">
                          <xsl:for-each select="SUB_GROUP_MASTER">
                            <xsl:if test="PIVOT_FEATURE_ID!=FEATURE_ID">
                              <tr class="titlebgray-bg">
                                <td class="firstcol">
                                  <xsl:value-of select="SUB_GROUP_NAME" disable-output-escaping="yes"/>
                                </td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td class="last">&nbsp;</td>
                              </tr>
                              <xsl:for-each select="SUB_GROUP_MASTER_DATA">
                                <tr>
                                  <td class="firstcol">
                                    <xsl:value-of select="FEATURE_NAME" disable-output-escaping="yes"/>
                                  </td>
                                  <xsl:for-each select="PRODUCT_FEATURE_MASTER/PRODUCT_FEATURE_MASTER_DATA">
                                    <xsl:variable name="featurelastclass">
                                      <xsl:if test="position()=last()">last</xsl:if>
                                    </xsl:variable>
                                    <td class="{$featurelastclass}">
                                      <xsl:choose>
                                        <xsl:when test="PRODUCT_FEATURE_VALUE='yes'">
                                          <img src="{/XML/FEATURE_YES_IMAGE_URL}" alt="{PRODUCT_FEATURE_VALUE}" title="{PRODUCT_FEATURE_VALUE}"/>
                                        </xsl:when>
                                        <xsl:otherwise>
                                          <xsl:choose>
                                            <xsl:when test="PRODUCT_FEATURE_VALUE='no'">
                                              <img src="{/XML/FEATURE_NO_IMAGE_URL}" alt="{PRODUCT_FEATURE_VALUE}" title="{PRODUCT_FEATURE_VALUE}"/>
                                            </xsl:when>
                                            <xsl:otherwise>
                                              <xsl:value-of select="PRODUCT_FEATURE_VALUE" disable-output-escaping="yes"/>
                                            </xsl:otherwise>
                                          </xsl:choose>
                                        </xsl:otherwise>
                                      </xsl:choose>
                                    </td>
                                  </xsl:for-each>
                                </tr>
                              </xsl:for-each>
                            </xsl:if>
                          </xsl:for-each>
                        </xsl:for-each>
                      </table>
                    </div>
                  </xsl:otherwise>
                </xsl:choose>
              </div>
              <div class="clear"></div>
			  <div class="searchLinkBottom">
				<xsl:value-of select="/XML/SEARCHLINK" disable-output-escaping="yes" />
			  </div>
			  <div class="clear"></div>

            </section>
            <div class="clear"></div>
          </div>
          <!-- body end -->
          <div class="clear"></div>
          <!--prefooter ad module starts-->
          <div class="prefooter-ad">
            <!--JavaScript Tag with group ID // Tag for network 1191: India.com // Website: Buying Guide // Page: Compare Phones // Placement: Buying_Guide_Compare-Phone_Bottom_728x90 (3483227) // created at: Oct 31, 2011 8:34:33 AM-->
			<script language="javascript">
			<![CDATA[
			if (window.adgroupid == undefined) {
				window.adgroupid = Math.round(Math.random() * 1000);
			}
			document.write('<scr'+'ipt language="javascript1.1" src="http://jt.india.com/addyn/3.0/1191/3483227/0/225/ADTECH;cookie=info;loc=100;target=_blank;key=key1+key2+key3+key4;grp='+window.adgroupid+';misc='+new Date().getTime()+'"></scri'+'pt>');
			]]>
			</script><noscript><a href="http://jt.india.com/adlink/3.0/1191/3483227/0/225/ADTECH;cookie=info;loc=300;key=key1+key2+key3+key4" target="_blank"><img src="http://jt.india.com/adserv/3.0/1191/3483227/0/225/ADTECH;cookie=info;loc=300;key=key1+key2+key3+key4" border="0" width="728" height="90" /></a></noscript>
			<!-- End of JavaScript Tag -->
          </div>
          <!--prefooter ad module starts-->
          <div class="clear"></div>
          <!-- footer starts -->
          <xsl:call-template name="footerDiv"/>
          <!-- footer end -->
          <div class="clear"></div>
        </div>
		<script>
		/// Clear compare cookies
//var cookie_pid		= 'ck_pid'; 
//var cookie_pname	= 'ck_pname';
	del_cookie(cookie_pid);
	del_cookie(cookie_pname);
	if(compareCount >= 4 ){
		$('#compare-btn').css('display',"none");
	}
		</script>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
