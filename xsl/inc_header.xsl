<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet  [
  <!ENTITY nbsp   "&#160;">
  <!ENTITY copy   "&#169;">
  <!ENTITY reg    "&#174;">
  <!ENTITY trade  "&#8482;">
  <!ENTITY mdash  "&#8212;">
  <!ENTITY ldquo  "&#8220;">
  <!ENTITY rdquo  "&#8221;">
  <!ENTITY pound  "&#163;">
  <!ENTITY yen    "&#165;">
  <!ENTITY euro   "&#8364;">
  <!ENTITY deg "&#176;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fb="http://www.facebook.com/2008/fbml" version="1.0">
  <xsl:output method="html" encoding="utf-8" indent="yes" doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"/>
  <xsl:template name="headDiv">
    <!-- header starts -->
	<!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
	<!--<script type="text/javascript" src="https://getfirebug.com/firebug-lite.js"></script> -->
	<!--<script language="JavaScript" src="{/XML/JS_URL}jquery.min.js?version={/XML/VERSION}" type="text/javascript"></script> -->
	<script language="JavaScript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.3/jquery.min.js" type="text/javascript"></script>
	<script language="JavaScript" src="{/XML/JS_URL}jquery.ui.core.js?version={/XML/VERSION}" type="text/javascript"></script>
	<script src="{/XML/JS_URL}jquery.ui.widget.js?version={/XML/VERSION}"></script>
	<script src="{/XML/JS_URL}jquery.ui.mouse.js?version={/XML/VERSION}"></script>
	<script language="JavaScript" src="{/XML/JS_URL}jquery.ui.position.js?version={/XML/VERSION}" type="text/javascript"></script>
	<script language="JavaScript" src="{/XML/JS_URL}jquery.ui.autocomplete.js?version={/XML/VERSION}" type="text/javascript"></script>
	<script language="JavaScript" src="{/XML/JS_URL}jquery.ui.slider.js?version={/XML/VERSION}" type="text/javascript"></script>
	<script src="{/XML/JS_URL}jquery.corners.js?version={/XML/VERSION}"></script>
	<script src="{/XML/JS_URL}basic.js?version={/XML/VERSION}"></script>
	<script src="{/XML/JS_URL}autocomplete1.js?version={/XML/VERSION}"></script>
	<link href="https://plus.google.com/105142581880497566028" rel="publisher" />
<!-- Begin comScore Tag -->
<script>
<![CDATA[
  var _comscore = _comscore || [];
  _comscore.push({ c1: "2", c2: "9254297" });
  (function() {
    var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;
    s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";
    el.parentNode.insertBefore(s, el);
  })();
]]>
</script>
<noscript>
  <img src="http://b.scorecardresearch.com/p?c1=2&amp;c2=9254297&amp;cv=2.0&amp;cj=1" />
</noscript>
<!-- End comScore Tag -->

<script>
	var web_url = '<xsl:value-of select="/XML/WEB_URL" disable-output-escaping="yes"/>';
	var nav_tab = '<xsl:value-of select="/XML/SELECTED_NAV_TAB" disable-output-escaping="yes"/>';

	<xsl:text disable-output-escaping="yes">
	<![CDATA[
	// iepp v2.1pre @jon_neal & @aFarkas github.com/aFarkas/iepp
	// html5shiv @rem remysharp.com/html5-enabling-script
	// Dual licensed under the MIT or GPL Version 2 licenses
	/*@cc_on(function(a,b){function r(a){var b=-1;while(++b<f)a.createElement(e[b])}if(!window.attachEvent||!b.createStyleSheet||!function(){var a=document.createElement("div");return a.innerHTML="<elem></elem>",a.childNodes.length!==1}())return;a.iepp=a.iepp||{};var c=a.iepp,d=c.html5elements||"abbr|article|aside|audio|canvas|datalist|details|figcaption|figure|footer|header|hgroup|mark|meter|nav|output|progress|section|subline|summary|time|video",e=d.split("|"),f=e.length,g=new RegExp("(^|\\s)("+d+")","gi"),h=new RegExp("<(/*)("+d+")","gi"),i=/^\s*[\{\}]\s*$/,j=new RegExp("(^|[^\\n]*?\\s)("+d+")([^\\n]*)({[\\n\\w\\W]*?})","gi"),k=b.createDocumentFragment(),l=b.documentElement,m=b.getElementsByTagName("script")[0].parentNode,n=b.createElement("body"),o=b.createElement("style"),p=/print|all/,q;c.getCSS=function(a,b){try{if(a+""===undefined)return""}catch(d){return""}var e=-1,f=a.length,g,h=[];while(++e<f){g=a[e];if(g.disabled)continue;b=g.media||b,p.test(b)&&h.push(c.getCSS(g.imports,b),g.cssText),b="all"}return h.join("")},c.parseCSS=function(a){var b=[],c;while((c=j.exec(a))!=null)b.push(((i.exec(c[1])?"\n":c[1])+c[2]+c[3]).replace(g,"$1.iepp-$2")+c[4]);return b.join("\n")},c.writeHTML=function(){var a=-1;q=q||b.body;while(++a<f){var c=b.getElementsByTagName(e[a]),d=c.length,g=-1;while(++g<d)c[g].className.indexOf("iepp-")<0&&(c[g].className+=" iepp-"+e[a])}k.appendChild(q),l.appendChild(n),n.className=q.className,n.id=q.id,n.innerHTML=q.innerHTML.replace(h,"<$1font")},c._beforePrint=function(){if(c.disablePP)return;o.styleSheet.cssText=c.parseCSS(c.getCSS(b.styleSheets,"all")),c.writeHTML()},c.restoreHTML=function(){if(c.disablePP)return;n.swapNode(q)},c._afterPrint=function(){c.restoreHTML(),o.styleSheet.cssText=""},r(b),r(k);if(c.disablePP)return;m.insertBefore(o,m.firstChild),o.media="print",o.className="iepp-printshim",a.attachEvent("onbeforeprint",c._beforePrint),a.attachEvent("onafterprint",c._afterPrint)})(this,document)@*/
		var smartphoneOS = ['Android', 'iOS', 'Symbian', 'Windows Phone', 'Blackberry','Bada'];
	]]>
	</xsl:text>
</script>

	<div id="icomribbon" class="ribbonWrapper"></div>
	<script type="text/javascript" src="http://ribbon.india.com/js/ribbon.js"></script>

	<div id="header-outer">
      <header>
        <a href="{/XML/WEB_URL}" class="logo spriteicon">buying guide</a>
        <div class="main-search">
          <div class="tab">
            <a href="javascript:switchSearch('1');" id="searchtab1" class="active">phone name</a>
            <a href="javascript:switchSearch('2');" id="searchtab2">brand</a>
            <!--a href="javascript:undefined;" id="searchtab3">deals</a-->
            <a href="javascript:switchSearch('3');" id="searchtab4">smartphone os</a>
          </div>
          <div class="searchboxwrap">
            <input type="text" class="searchbox spriteicon" id="tags" />
            <button type="button" id= "tagsbutton" class="searchbtn spriteicon" onclick="redirectToDetailPage();">Search</button>
          </div>
        </div>
		<div class="poweredbybgr"><span>Powered by</span><a href="http://www.bgr.in" target="_blank" class="spriteicon"></a></div>
        <div class="clear"></div>
      </header>
      <div class="clear"></div>
    </div>
    <!-- header end -->
    <div class="clear"></div>
    <!-- nav starts -->
    <div id="nav-outer">
      <nav>
		<ul>
			<li>
				<a id="nav_1" href="{/XML/WEB_URL}">home</a>
			</li>
			<li>
				<a id="nav_2" href="{/XML/SEO_PHONE_FINDER_URL}">phone finder</a>
				<!--a href="{/XML/WEB_URL}phone-finder.html">phone finder</a-->
			</li>
			<li>
				<a id="nav_3" href="{/XML/SEO_COMPARE_PHONES_URL}">compare phones</a>
			</li>
			<li>
				<a id="nav_4" href="{/XML/SEO_PHONE_LIST_URL}">phone list</a>
			</li>
			<li class="last">
				<a id="nav_5" href="http://www.bgr.in" target="_blank" >News &#38; Reviews</a>
			</li>
			<!--li class="last">
				<a href="http://www.bgr.in" target="_blank" class="nav-bgrlink spriteicon"></a>
			</li-->
			<div class="clear"></div>
		</ul>
        <div class="sharelink">
          <span>Follow Us</span>
          <a href="http://www.facebook.com/pages/Indiacom-Mobile/288535974500901" target="_blank" class="fb spriteicon"></a>
          <a href="http://www.twitter.com/IndiaComMobile" target="_blank" class="twitter spriteicon"></a>
          <!--a href="javascript:undefined;" class="rss spriteicon"></a-->
		  <a href="https://plus.google.com/105142581880497566028?prsrc=3" style="text-decoration: none;" target="_blank"><img src="https://ssl.gstatic.com/images/icons/gplus-16.png" width="16" height="16" style="border: 0;"/></a>
        </div>
        <div class="clear"></div>
      </nav>
      <div class="clear"></div>
    </div>

	<script>
	var productname =  productid = brandname = osname = searchType = '' ;
	<![CDATA[
	$.widget( "custom.catcomplete", $.ui.autocomplete, {
		_renderMenu: function( ul, items ) {
			var self = this,
				currentCategory = "";
			$.each( items, function( index, item ) {
				if ( item.brand != currentCategory ) {
					ul.append( "<li class='ui-autocomplete-category'>" + item.brand + "</li>" );
					currentCategory = item.brand;
				}
				self._renderItem( ul, item );
			});
		}
	});

	function redirectToDetailPage(){
		if(searchType=='1'){
			if(productname != '' &&  productid != ''){
				productname1		=  productname.replace(/[^a-zA-Z0-9]+/g,'-');
				turl	= web_url + "Mobile/"+ productname1 +"/Overviews/" + productid ;
				location.href = turl;
			}
		} else if(searchType=='2'){
			if ( brandname != '' ){
				brandname	= brandname.replace(/\s/g,'_');
				turl	= web_url + "Mobile-finder/Brand-"+ brandname  ;
				location.href = turl;
			}
		} else if(searchType=='3'){
			if ( osname != '' ){
				osname	= osname.replace(/\s/g,'_');
				turl	= web_url + "Mobile-finder/OS-"+ osname  ;
				location.href = turl;
			}
		} else {
			alert("Where to go...?");
		}
	}

	function switchSearch(n){
		searchType = n;
		$('.ui-autocomplete').remove();
		$("#tags").val("");
		$("#tags").autocomplete("destroy");
		switch(n){
			case '1':   $(function() {$("#tags").catcomplete({
							source: productData,
							select: function(event, ui) { productname = ui.item.label; productid= ui.item.product_id;redirectToDetailPage();}
						});});
						break;
			case '2':	$(function() {$("#tags").autocomplete({  source: availableBrands,
						select: function(event, ui) { brandname = ui.item.value; redirectToDetailPage();}
						});});
						break;
			case '3':	$(function() {$("#tags").autocomplete({  source: smartphoneOS,
						select: function(event, ui) { osname = ui.item.value; redirectToDetailPage(); }
						});});
						break;

			default	:  ;//alert("sac");
		}
	}
	switchSearch('1');

	$("#nav_"+nav_tab).addClass('active');

	$("#tags").keyup(function(event){
	  if(event.keyCode == 13){
		redirectToDetailPage();
	  }
	});

	]]>
	</script>
    <!-- nav end -->
  </xsl:template>
</xsl:stylesheet>