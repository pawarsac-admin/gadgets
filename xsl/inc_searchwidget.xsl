<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet  [
  <!ENTITY nbsp   "&#160;">
  <!ENTITY copy   "&#169;">
  <!ENTITY reg    "&#174;">
  <!ENTITY trade  "&#8482;">
  <!ENTITY mdash  "&#8212;">
  <!ENTITY ldquo  "&#8220;">
  <!ENTITY rdquo  "&#8221;">
  <!ENTITY pound  "&#163;">
  <!ENTITY yen    "&#165;">
  <!ENTITY euro   "&#8364;">
  <!ENTITY deg "&#176;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fb="http://www.facebook.com/2008/fbml" version="1.0">
  <xsl:output method="html" encoding="utf-8" indent="yes" doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"/>
  <xsl:template name="searchWidget">
<script>
	var mnprice = '<xsl:value-of select="/XML/MIN_PRICE" disable-output-escaping="yes"/>';
	var mxprice = '<xsl:value-of select="/XML/MAX_PRICE" disable-output-escaping="yes"/>';
</script>
   <form method="post" action="{/XML/SEO_PHONE_FINDER_RESULT_URL}" name="researchfrm" id="researchfrm">
					<input type="hidden" id="mxprice" name="mxprice" value="{/XML/MAX_PRICE}" />
					<input type="hidden" id="mnprice" name="mnprice" value="{/XML/MIN_PRICE}" />
					<input type="hidden" id="debug" name="debug" value="" />
                <div class="phone-listing-main">
                  <div class="phone-listing-brands">
                   <ul>
                      <li>
                        <xsl:choose>
                          <xsl:when test="/XML/BRAND_MASTER/BRANDSELECTSTATUSFLAG=0">
                            <input type="checkbox"  class="case1" value="all" name="sel_all_brand" id="sel_all_brand" checked="yes" onclick="_gaq.push(['_trackEvent', 'Search Phone', 'clicked', 'Brand Selection']);" />
                          </xsl:when>
                          <xsl:otherwise>
                            <input type="checkbox"  class="case1" value="all" name="unsel_all_brand" id="sel_all_brand" onclick="_gaq.push(['_trackEvent', 'Search Phone', 'clicked', 'Brand Selection']);"/>
                          </xsl:otherwise>
                        </xsl:choose>
                                All Brands
                      </li>
                      <xsl:for-each select="/XML/BRAND_MASTER/BRAND_MASTER_DATA">
					  <xsl:if test="position() &lt; 7" >
                        <li>
                          <xsl:choose>
                            <xsl:when test="SELECT_STATUS=1">
                              <input type="checkbox" class="case1" value="{BRAND_NAME_VALUE}" name="branddetails[]" id="checkbox_brand_id_{BRAND_ID}" checked="yes" onclick="_gaq.push(['_trackEvent', 'Search Phone', 'clicked', 'Brand Selection']);" />
                            </xsl:when>
                            <xsl:otherwise>
                              <input type="checkbox" class="case1" value="{BRAND_NAME_VALUE}" name="branddetails[]" id="checkbox_brand_id_{BRAND_ID}" onclick="_gaq.push(['_trackEvent', 'Search Phone', 'clicked', 'Brand Selection']);" />
                            </xsl:otherwise>
                          </xsl:choose>
                          <xsl:value-of select="BRAND_NAME" disable-output-escaping="yes"/>
                        </li>
						</xsl:if>
                      </xsl:for-each>
                    </ul>
                    <a href="javascript:undefined;" class="spriteicon morebrands" id="morebrandslink"></a>
                  </div>
				 
				 <div style="display: none;" id="listing-more" class="phone-listing-brands allbrands">
                    <ul>
					<xsl:for-each select="/XML/BRAND_MASTER/BRAND_MASTER_DATA">
					  <xsl:if test="position() &gt; 6" >
                        <li>
                          <xsl:choose>
                            <xsl:when test="SELECT_STATUS=1">
                              <input type="checkbox" class="case1" value="{BRAND_NAME_VALUE}" name="branddetails[]" id="checkbox_brand_id_{BRAND_ID}" checked="yes" onclick="_gaq.push(['_trackEvent', 'Search Phone', 'clicked', 'Brand Selection']);" />
                            </xsl:when>
                            <xsl:otherwise>
                              <input type="checkbox" class="case1" value="{BRAND_NAME_VALUE}" name="branddetails[]" id="checkbox_brand_id_{BRAND_ID}" onclick="_gaq.push(['_trackEvent', 'Search Phone', 'clicked', 'Brand Selection']);" />
                            </xsl:otherwise>
                          </xsl:choose>
                          <xsl:value-of select="BRAND_NAME" disable-output-escaping="yes"/>
                        </li>
						</xsl:if>
                      </xsl:for-each>
                    </ul>
                 </div>
                 <div class="phone-listing-filter">
                    <div class="filter-lhs" style="display:inline-block; width:375px; background:#fff; height:50px; float:left;">
                      <p>
                        <label>By phone type</label>
                        <span>
                          <select name="featuredetails" id="featuredetails"  onclick="_gaq.push(['_trackEvent', 'Search Phone', 'clicked', 'Phone Type']);" >
								<option value="0">--Select phone type--</option>
								<xsl:for-each select="/XML/PHONE_TYPE_MASTER/PHONE_TYPE">
									  <xsl:choose>
										<xsl:when test="SELECT_STATUS=1">
										  <option value="{NAME_VALUE}" selected="selected">
											 <xsl:value-of select="NAME" disable-output-escaping="yes"/>
										  </option>
										</xsl:when>
										<xsl:otherwise>
										  <option value="{NAME_VALUE}">
											 <xsl:value-of select="NAME" disable-output-escaping="yes"/>
										  </option>
										</xsl:otherwise>
									  </xsl:choose>
								  </xsl:for-each>
                          </select>
                        </span>
                      </p>
                      <p>
                        <label>Price range</label>
                        <!--slider starts-->
                      </p>
                      <div class="demo">
                        <div id="slider-range"></div>
                        <p class="price-range">
                          <input type="text" id="amount" style="border:0; color:#f6931f; font-weight:normal; font-size:11px; color:#000; width:230px; text-align:center;" />
                        </p>
                      </div>
                      <!--slider ends-->
                    </div>
			<div class="list-phones-wrap">
			<a href="#" class="list-phones-btn spriteicon" onclick="_gaq.push(['_trackEvent', 'Search Phone', 'clicked', 'List Phones - Button']);setTimeout('submitNewSearch()',250);" ></a><br/>
			<a href="{/XML/WEB_URL}Mobile-finder" class="more-opt" style="">More Options &gt;&gt;</a>
			</div>
                  </div>
                </div>
				</form>
<script>
	$(function() {
		$( "#slider-range" ).slider({
			range: true,
			min: 0,
			max: 100000,
			step: 100,
			values: [ mnprice, mxprice ],
			slide: function( event, ui ) {
				$( "#amount" ).val( "Rs." + ui.values[ 0 ] + " - Rs." + ui.values[ 1 ] );
				$('#mnprice').val( ui.values[ 0 ] );
				$('#mxprice').val( ui.values[ 1 ] );
				_gaq.push(['_trackEvent', 'Search Phone', 'clicked', 'Price Range']); 
			}
		});
		$( "#amount" ).val( "Rs." + mnprice + " - Rs." + mxprice );
		$('#mnprice').val( mnprice );
		$('#mxprice').val ( mxprice);
	});

	$('#morebrandslink').click(function () {
	if ($("#listing-more").is(":hidden")) {
	  $("#listing-more").slideDown("slow");
	} else {
	  $("#listing-more").slideUp("slow");
	}
	$(this).toggleClass("lessbrand");
	});


	$(function(){
     // add multiple select / deselect functionality
    $("#sel_all_brand").click(function () {
          $('.case1').attr('checked', this.checked);
    });
 
    // if all checkbox are selected, check the selectall checkbox and viceversa
    $(".case1").click(function(){
 
        if($(".case1").length == $(".case1:checked").length) {
            $("#sel_all_brand").attr("checked", "checked");
        } else {
            $("#sel_all_brand").removeAttr("checked");
        }
 
    });
	});


	function submitNewSearch(){
		var qstring = '';

		var brands	= getBrandValues();
		if( brands != '')	{
			qstring +=	'/Brand-' + brands;
		} else {
			qstring +=	'/Brand-all';
		}

		if($('#featuredetails').prop('selectedIndex')){
			var phoneTypeValue	= $("#featuredetails").val();
			qstring +=	'/PhoneType-' + phoneTypeValue;
		}

		qstring +=	'/Price-' + $('#mnprice').val() + '-' +$('#mxprice').val() ;
		
		location.href	= web_url + 'Mobile-finder' + qstring ;

	}

	function getBrandValues()
	{
		brands	= ($("input[name=branddetails[]]:checked").map
		(function () {return this.value;}).get().join("-") );
		return brands;
	}

</script>
  </xsl:template>
</xsl:stylesheet>