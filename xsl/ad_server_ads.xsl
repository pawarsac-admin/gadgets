<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet  [
  <!ENTITY nbsp   "&#160;">
  <!ENTITY copy   "&#169;">
  <!ENTITY reg    "&#174;">
  <!ENTITY trade  "&#8482;">
  <!ENTITY mdash  "&#8212;">
  <!ENTITY ldquo  "&#8220;">
  <!ENTITY rdquo  "&#8221;"> 
  <!ENTITY pound  "&#163;">
  <!ENTITY yen    "&#165;">
  <!ENTITY euro   "&#8364;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" encoding="utf-8"/>

  <xsl:template name="AdServerDiv">
  <xsl:param name="addType"/>  
	<!--<script type='text/javascript'>
		GS_googleAddAdSenseService('<xsl:value-of select="/XML/GOOGLE_AD_API_KEY" disable-output-escaping="yes"/>');
		GS_googleEnableAllServices();
	</script>
	<script type='text/javascript'>
		GA_googleAddSlot('<xsl:value-of select="/XML/GOOGLE_AD_API_KEY" disable-output-escaping="yes"/>',
		'<xsl:value-of select="$addType" disable-output-escaping="yes"/>');
	</script>
	<script type='text/javascript'>
		GA_googleFetchAds();
	</script>-->
	<!-- OC_ROS_Bottom_North_728x90 -->
	<!--<script type='text/javascript'>
		GA_googleFillSlot('<xsl:value-of select="$addType" disable-output-escaping="yes"/>');
	</script>-->

        <!-- Start OC_Home_Right_Top_Lrec_300x250 -->
    <xsl:if test="$addType ='OC_Home_Top_RHS_Lrec_300x250_1'">
	<script language="javascript">
<xsl:text disable-output-escaping="yes">
<![CDATA[
if (window.adgroupid == undefined) {
	window.adgroupid = Math.round(Math.random() * 1000);
}
document.write('<scr'+'ipt language="javascript1.1" src="http://adserver.adtech.de/addyn/3.0/1191/3149430/0/170/ADTECH;loc=100;target=_blank;key=key1+key2+key3+key4;grp='+window.adgroupid+';misc='+new Date().getTime()+'"></scri'+'pt>');
]]>
</xsl:text>
</script><noscript><a href="http://adserver.adtech.de/adlink/3.0/1191/3149430/0/170/ADTECH;loc=300;key=key1+key2+key3+key4" target="_blank"><img src="http://adserver.adtech.de/adserv/3.0/1191/3149430/0/170/ADTECH;loc=300;key=key1+key2+key3+key4" border="0" width="300" height="250"/></a></noscript>
   </xsl:if>
<!-- End OC_Home_Right_Top_Lrec_300x250 -->


 <!-- Start OC_Home_Right_Middle_Lrec_300x250 -->
   <xsl:if test="$addType ='OC_Home_RHS_Bottom_Lrec_300x250_2'">
  	<script language="javascript">
<xsl:text disable-output-escaping="yes">
<![CDATA[
if (window.adgroupid == undefined) {
	window.adgroupid = Math.round(Math.random() * 1000);
}
document.write('<scr'+'ipt language="javascript1.1" src="http://adserver.adtech.de/addyn/3.0/1191/3149429/0/170/ADTECH;loc=100;target=_blank;key=key1+key2+key3+key4;grp='+window.adgroupid+';misc='+new Date().getTime()+'"></scri'+'pt>');
]]>
</xsl:text>
</script><noscript><a href="http://adserver.adtech.de/adlink/3.0/1191/3149429/0/170/ADTECH;loc=300;key=key1+key2+key3+key4" target="_blank"><img src="http://adserver.adtech.de/adserv/3.0/1191/3149429/0/170/ADTECH;loc=300;key=key1+key2+key3+key4" border="0" width="300" height="250"/></a></noscript>
   </xsl:if>
<!-- End OC_Home_Right_Middle_Lrec_300x250 -->

<!-- Start OC_Home_Bottom_North_728x90 -->
   <xsl:if test="$addType ='OC_HOME_BOTTOM_NORTH_728x90'">
  	<script type="text/javascript">
	 var sFrameUrl=siteURL+"xsl/OC_Home_Bottom_North_728x90.html";
	 function resizer(ht){
              	i = document.getElementById('boardiframe');
	 }
         sUrl=encodeURIComponent(window.location.href);
         if ((navigator.appName.indexOf("Microsoft") != -1) || (navigator.userAgent.toLowerCase().indexOf('firefox'))){
          document.write('<IFRAME id="boardiframe" name="boardiframe" scrolling="no" height = "90px" width="728px" frameborder="0" src="'+sFrameUrl+'" ></IFRAME>');
         }
	</script>
   </xsl:if>
<!-- End OC_Home_Bottom_North_728x90 -->

<!-- Start OC_ROS_Right_Top_Lrec_300x250 -->
   <xsl:if test="$addType ='OC_ROS_Top_RHS_Lrec_300x250_1'">
	<script language="javascript">
	<xsl:text disable-output-escaping="yes">
	<![CDATA[
	if (window.adgroupid == undefined) {
	window.adgroupid = Math.round(Math.random() * 1000);
	}
	document.write('<scr'+'ipt language="javascript1.1" src="http://adserver.adtech.de/addyn/3.0/1191/3149432/0/170/ADTECH;loc=100;target=_blank;key=key1+key2+key3+key4;grp='+window.adgroupid+';misc='+new Date().getTime()+'"></scri'+'pt>');
	]]>
	</xsl:text>
	</script>
	<noscript><a href="http://adserver.adtech.de/adlink/3.0/1191/3149432/0/170/ADTECH;loc=300;key=key1+key2+key3+key4" target="_blank"><img src="http://adserver.adtech.de/adserv/3.0/1191/3149432/0/170/ADTECH;loc=300;key=key1+key2+key3+key4" border="0" width="300" height="250"/></a></noscript>
   </xsl:if>
<!-- End OC_ROS_Right_Top_Lrec_300x250 -->

<!-- Start OC_ROS_Bottom_North_728x90 -->
   <xsl:if test="$addType ='OC_ROS_Bottom_North_728x90'">
  	<script type="text/javascript">
         var sFrameUrl=siteURL+"xsl/OC_ROS_Bottom_North_728x90.html";
	function resizer(ht){
              	i = document.getElementById('boardiframe');
	 }
         sUrl=encodeURIComponent(window.location.href);
         if ((navigator.appName.indexOf("Microsoft") != -1) || (navigator.userAgent.toLowerCase().indexOf('firefox'))){
          document.write('<IFRAME id="boardiframe" name="boardiframe" scrolling="no" height = "90px" width="728px" frameborder="0" src="'+sFrameUrl+'" ></IFRAME>');
         }
	</script>
   </xsl:if>
<!-- End OC_ROS_Bottom_North_728x90 -->

<!-- Start OC_ROS_Right_Top_300x115 -->
   <xsl:if test="$addType ='OC_ROS_Right_Top_300x115'">
	<!--JavaScript Tag with group ID // Tag for network 1191: India.com // Website: Oncar.in // Page: ROS // Placement: OC_ROS_Right_Top_300x115 (3207577) // created at: Jun 3, 2011 10:29:58 AM-->
	<script language="javascript">
	<xsl:text disable-output-escaping="yes">
	<![CDATA[
	if (window.adgroupid == undefined) {
	window.adgroupid = Math.round(Math.random() * 1000);
	}
	document.write('<scr'+'ipt language="javascript1.1" src="http://adserver.adtech.de/addyn/3.0/1191/3207577/0/2049/ADTECH;loc=100;target=_blank;key=key1+key2+key3+key4;grp='+window.adgroupid+';misc='+new Date().getTime()+'"></scri'+'pt>');
	]]>
	</xsl:text>
	</script><noscript><a href="http://adserver.adtech.de/adlink/3.0/1191/3207577/0/2049/ADTECH;loc=300;key=key1+key2+key3+key4" target="_blank"><img src="http://adserver.adtech.de/adserv/3.0/1191/3207577/0/2049/ADTECH;loc=300;key=key1+key2+key3+key4" border="0" width="300" height="115"/></a></noscript>
	<!-- End of JavaScript Tag -->
   </xsl:if>
<!-- End OC_ROS_Right_Top_300x115 -->

<!-- Start OC_ROS_Middle_HB_468x60 -->
   <xsl:if test="$addType ='OC_ROS_Middle_HB_468x60'">
	<!--JavaScript Tag with group ID // Tag for network 1191: India.com // Website: Oncar.in // Page: ROS // Placement:  OC_ROS_Middle_HB_468x60 (3205588) // created at: Jun 3, 2011 10:29:58 AM-->
	<script language="javascript">
	<xsl:text disable-output-escaping="yes">
	<![CDATA[
	if (window.adgroupid == undefined) {
		window.adgroupid = Math.round(Math.random() * 1000);
	}
	document.write('<scr'+'ipt language="javascript1.1" src="http://adserver.adtech.de/addyn/3.0/1191/3205588/0/1/ADTECH;loc=100;target=_blank;key=key1+key2+key3+key4;grp='+window.adgroupid+';misc='+new Date().getTime()+'"></scri'+'pt>');
	]]>
	</xsl:text>
	</script><noscript><a href="http://adserver.adtech.de/adlink/3.0/1191/3205588/0/1/ADTECH;loc=300;key=key1+key2+key3+key4" target="_blank"><img src="http://adserver.adtech.de/adserv/3.0/1191/3205588/0/1/ADTECH;loc=300;key=key1+key2+key3+key4" border="0" width="468" height="60"/></a></noscript>
	<!-- End of JavaScript Tag -->
   </xsl:if>
<!-- End OC_ROS_Middle_HB_468x60 -->



<!-- Start OC_ROS_Middle_HOME_468x60 -->
 <xsl:if test="$addType ='OC_ROS_Middle_HOME_468x60'">
	<script language="javascript">
	<xsl:text disable-output-escaping="yes">
	<![CDATA[
	if (window.adgroupid == undefined) {
	window.adgroupid = Math.round(Math.random() * 1000);
	}
	document.write('<scr'+'ipt language="javascript1.1" src="http://adserver.adtech.de/addyn/3.0/1191/3214088/0/1/ADTECH;loc=100;target=_blank;grp='+window.adgroupid+';misc='+new Date().getTime()+'"></scri'+'pt>');
	]]>
	</xsl:text>
	</script>
	<noscript>
	<a href="http://adserver.adtech.de/adlink/3.0/1191/3214088/0/1/ADTECH;loc=300" target="_blank">
	<img src="http://adserver.adtech.de/adserv/3.0/1191/3214088/0/1/ADTECH;loc=300"
	border="0" width="468" height="60"/></a></noscript>
    </xsl:if>
<!-- END OC_ROS_Middle_HOME_468x60 -->


<!-- Start OC_AllCarsList_Right_Top_Lrec_300x250 -->
 <xsl:if test="$addType ='OC_AllCarsList_Right_Top_Lrec_300x250'">
	<script language="javascript">
	<xsl:text disable-output-escaping="yes">
	<![CDATA[
	if (window.adgroupid == undefined) {
	window.adgroupid = Math.round(Math.random() * 1000);
	}
	document.write('<scr'+'ipt language="javascript1.1" src="http://adserver.adtech.de/addyn/3.0/1191/3226249/0/170/ADTECH;loc=100;target=_blank;key=key1+key2+key3+key4;grp='+window.adgroupid+';misc='+new Date().getTime()+'"></scri'+'pt>');
	]]>
	</xsl:text>
	</script>
	<noscript>
        <a href="http://adserver.adtech.de/adlink/3.0/1191/3226249/0/170/ADTECH;loc=300;key=key1+key2+key3+key4" target="_blank">
        <img src="http://adserver.adtech.de/adserv/3.0/1191/3226249/0/170/ADTECH;loc=300;key=key1+key2+key3+key4" border="0"
         width="300" height="250"/></a></noscript>

    </xsl:if>
<!-- END OC_AllCarsList_Right_Top_Lrec_300x250 -->



<!-- Start OC_PHOTOGALLERY_BOTTOM_NORTH_728X90 -->
 <xsl:if test="$addType ='OC_PHOTOGALLERY_BOTTOM_NORTH_728X90'">
	<script language="javascript">
	<xsl:text disable-output-escaping="yes">
	<![CDATA[
	if (window.adgroupid == undefined) {
	window.adgroupid = Math.round(Math.random() * 1000);
	}
	document.write('<scr'+'ipt language="javascript1.1" src="http://adserver.adtech.de/addyn/3.0/1191/3228075/0/225/ADTECH;loc=100;target=_blank;key=key1+key2+key3+key4;grp='+window.adgroupid+';misc='+new Date().getTime()+'"></scri'+'pt>');
	]]>
	</xsl:text>
	</script>
	<noscript>
	<a href="http://adserver.adtech.de/adlink/3.0/1191/3228075/0/225/ADTECH;loc=300;key=key1+key2+key3+key4" target="_blank">
	<img src="http://adserver.adtech.de/adserv/3.0/1191/3228075/0/225/ADTECH;loc=300;key=key1+key2+key3+key4" border="0" width="728" height="90"/>
	</a></noscript>
    </xsl:if>
<!-- END OC_PHOTOGALLERY_BOTTOM_NORTH_728X90 -->


<!-- Start OC_PHOTOGALLERY_RIGHT_TOP_LREC_300X250 -->
 <xsl:if test="$addType ='OC_PHOTOGALLERY_RIGHT_TOP_LREC_300X250'">
	<script language="javascript">
	<xsl:text disable-output-escaping="yes">
	<![CDATA[
	if (window.adgroupid == undefined) {
	window.adgroupid = Math.round(Math.random() * 1000);
	}
	document.write('<scr'+'ipt language="javascript1.1" src="http://adserver.adtech.de/addyn/3.0/1191/3228074/0/170/ADTECH;loc=100;target=_blank;key=key1+key2+key3+key4;grp='+window.adgroupid+';misc='+new Date().getTime()+'"></scri'+'pt>');
	]]>
	</xsl:text>
	</script>
     <noscript>
     <a href="http://adserver.adtech.de/adlink/3.0/1191/3228074/0/170/ADTECH;loc=300;key=key1+key2+key3+key4" target="_blank">
     <img src="http://adserver.adtech.de/adserv/3.0/1191/3228074/0/170/ADTECH;loc=300;key=key1+key2+key3+key4" border="0" 
      width="300" height="250"/></a></noscript>
    </xsl:if>
<!-- END OC_PHOTOGALLERY_RIGHT_TOP_LREC_300X250 -->


<!-- Start OC_RPHOTOGALLERY_MIDDLE_HB_468X60 -->
 <xsl:if test="$addType ='OC_RPHOTOGALLERY_MIDDLE_HB_468X60'">
	<script language="javascript">
	<xsl:text disable-output-escaping="yes">
	<![CDATA[
	if (window.adgroupid == undefined) {
	window.adgroupid = Math.round(Math.random() * 1000);
	}
	document.write('<scr'+'ipt language="javascript1.1" src="http://adserver.adtech.de/addyn/3.0/1191/3228076/0/1/ADTECH;loc=100;target=_blank;key=key1+key2+key3+key4;grp='+window.adgroupid+';misc='+new Date().getTime()+'"></scri'+'pt>');
	]]>
	</xsl:text>
	</script>
	<noscript>
	<a href="http://adserver.adtech.de/adlink/3.0/1191/3228076/0/1/ADTECH;loc=300;key=key1+key2+key3+key4" target="_blank"><img src="http://adserver.adtech.de/adserv/3.0/1191/3228076/0/1/ADTECH;loc=300;key=key1+key2+key3+key4" border="0" width="468" height="60"/>
	</a></noscript>
    </xsl:if>
<!-- END OC_RPHOTOGALLERY_MIDDLE_HB_468X60 -->

<!-- Start OC_ROS_Middle_620x80 (3238708) -->
 <xsl:if test="$addType ='OC_ROS_Middle_620x80'">
        <script language="javascript">
        <xsl:text disable-output-escaping="yes">
        <![CDATA[
	if (window.adgroupid == undefined) {
		window.adgroupid = Math.round(Math.random() * 1000);
	}
	document.write('<scr'+'ipt language="javascript1.1" src="https://secserv.adtech.de/addyn/3.0/1191/3238708/0/563/ADTECH;loc=100;target=_blank;grp='+window.adgroupid+';misc='+new Date().getTime()+'"></scri'+'pt>');
        ]]>
        </xsl:text>
        </script>
        <noscript>
	<a href="https://secserv.adtech.de/adlink/3.0/1191/3238708/0/563/ADTECH;loc=300" target="_blank"><img src="https://secserv.adtech.de/adserv/3.0/1191/3238708/0/563/ADTECH;loc=300" border="0" width="620" height="80" />
        </a></noscript>
    </xsl:if>
<!-- END OC_ROS_Middle_620x80 (3238708) -->

<!-- OC_ROS_Right_Bottom_Lrec_300x250_2 (3239236) -->
 <xsl:if test="$addType ='OC_ROS_Right_Bottom_Lrec_300x250_2'">
        <script language="javascript">
        <xsl:text disable-output-escaping="yes">
        <![CDATA[
        if (window.adgroupid == undefined) {
                window.adgroupid = Math.round(Math.random() * 1000);
        }
        document.write('<scr'+'ipt language="javascript1.1" src="http://adserver.adtech.de/addyn/3.0/1191/3239236/0/170/ADTECH;loc=100;target=_blank;key=key1+key2+key3+key4;grp='+window.adgroupid+';misc='+new Date().getTime()+'"></scri'+'pt>');
        ]]>
        </xsl:text>
        </script>
        <noscript>
        <a href="http://adserver.adtech.de/adlink/3.0/1191/3239236/0/170/ADTECH;loc=300;key=key1+key2+key3+key4" target="_blank"><img src="http://adserver.adtech.de/adserv/3.0/1191/3239236/0/170/ADTECH;loc=300;key=key1+key2+key3+key4" border="0" width="300" height="250" />
        </a></noscript>
    </xsl:if> 
<!-- OC_ROS_Right_Bottom_Lrec_300x250_2 (3239236) -->

  </xsl:template>
</xsl:stylesheet>
