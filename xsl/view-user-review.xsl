<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet  [
  <!ENTITY nbsp   "&#160;">
  <!ENTITY copy   "&#169;">
  <!ENTITY reg    "&#174;">
  <!ENTITY trade  "&#8482;">
  <!ENTITY mdash  "&#8212;">
  <!ENTITY ldquo  "&#8220;">
  <!ENTITY rdquo  "&#8221;">
  <!ENTITY pound  "&#163;">
  <!ENTITY yen    "&#165;">
  <!ENTITY euro   "&#8364;">
  <!ENTITY deg "&#176;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fb="http://www.facebook.com/2008/fbml" version="1.0">
  <xsl:output method="html" encoding="utf-8" indent="yes" doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"/>
  <xsl:include href="inc_header.xsl" />
  <xsl:include href="inc_footer.xsl" />
  <xsl:include href="inc_searchwidget.xsl" />
  <xsl:include href="inc_newsletter_widget.xsl" />
  <xsl:include href="inc_top_rated_widget.xsl" />
  <xsl:template match="/">
    <html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Buying Guides</title>
<link href="{/XML/CSS_URL}style.css?version={/XML/VERSION}" rel="stylesheet" type="text/css" />
<!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--<script type="text/javascript" src="https://getfirebug.com/firebug-lite.js"></script> -->
<script>
<xsl:text disable-output-escaping="yes">
<![CDATA[

		// iepp v2.1pre @jon_neal & @aFarkas github.com/aFarkas/iepp
// html5shiv @rem remysharp.com/html5-enabling-script
// Dual licensed under the MIT or GPL Version 2 licenses
/*@cc_on(function(a,b){function r(a){var b=-1;while(++b<f)a.createElement(e[b])}if(!window.attachEvent||!b.createStyleSheet||!function(){var a=document.createElement("div");return a.innerHTML="<elem></elem>",a.childNodes.length!==1}())return;a.iepp=a.iepp||{};var c=a.iepp,d=c.html5elements||"abbr|article|aside|audio|canvas|datalist|details|figcaption|figure|footer|header|hgroup|mark|meter|nav|output|progress|section|subline|summary|time|video",e=d.split("|"),f=e.length,g=new RegExp("(^|\\s)("+d+")","gi"),h=new RegExp("<(/*)("+d+")","gi"),i=/^\s*[\{\}]\s*$/,j=new RegExp("(^|[^\\n]*?\\s)("+d+")([^\\n]*)({[\\n\\w\\W]*?})","gi"),k=b.createDocumentFragment(),l=b.documentElement,m=b.getElementsByTagName("script")[0].parentNode,n=b.createElement("body"),o=b.createElement("style"),p=/print|all/,q;c.getCSS=function(a,b){try{if(a+""===undefined)return""}catch(d){return""}var e=-1,f=a.length,g,h=[];while(++e<f){g=a[e];if(g.disabled)continue;b=g.media||b,p.test(b)&&h.push(c.getCSS(g.imports,b),g.cssText),b="all"}return h.join("")},c.parseCSS=function(a){var b=[],c;while((c=j.exec(a))!=null)b.push(((i.exec(c[1])?"\n":c[1])+c[2]+c[3]).replace(g,"$1.iepp-$2")+c[4]);return b.join("\n")},c.writeHTML=function(){var a=-1;q=q||b.body;while(++a<f){var c=b.getElementsByTagName(e[a]),d=c.length,g=-1;while(++g<d)c[g].className.indexOf("iepp-")<0&&(c[g].className+=" iepp-"+e[a])}k.appendChild(q),l.appendChild(n),n.className=q.className,n.id=q.id,n.innerHTML=q.innerHTML.replace(h,"<$1font")},c._beforePrint=function(){if(c.disablePP)return;o.styleSheet.cssText=c.parseCSS(c.getCSS(b.styleSheets,"all")),c.writeHTML()},c.restoreHTML=function(){if(c.disablePP)return;n.swapNode(q)},c._afterPrint=function(){c.restoreHTML(),o.styleSheet.cssText=""},r(b),r(k);if(c.disablePP)return;m.insertBefore(o,m.firstChild),o.media="print",o.className="iepp-printshim",a.attachEvent("onbeforeprint",c._beforePrint),a.attachEvent("onafterprint",c._afterPrint)})(this,document)@*/

]]>
</xsl:text>
</script>
</head>

<body>
<div id="wrapper">
	<!-- header start -->
          <xsl:call-template name="headDiv"/>
          <!-- header end -->
    <div class="clear"></div>
   	<!-- body starts -->
    <div id="maincontent-wrapper">
        	<section>
				<div class="write-user-review">
                    <!--Product features intro-->
                    <span class="section"><!--
                    <h2><xsl:value-of select="/XML/USER_REVIEW_MASTER/USER_REVIEW_MASTER_DATA/BRAND_MODEL_VARIANT_NAME" disable-output-escaping="yes"/> Features</h2>
                    <p><xsl:value-of select="/XML/USER_REVIEW_MASTER/USER_REVIEW_MASTER_DATA/PRODUCT_DESC" disable-output-escaping="yes"/> <a href="javascript:undefined;">Read More</a></p>
                    <div class="clear"></div>-->
                    </span>
                    <!--How to rate your mobile details-->
                    <span class="section">
                    <h2><xsl:value-of select="/XML/USER_REVIEW_MASTER/USER_REVIEW_MASTER_DATA/BRAND_MODEL_VARIANT_NAME" disable-output-escaping="yes"/> User Ratings</h2>
<div style="background:#FCFFF6;">
		<xsl:for-each select="/XML/USER_REVIEW_ANSWER_MASTER/USER_REVIEW_ANSWER_MASTER_DATA">
                    <div class="rating-component-single view-rating-component-single">
                        <div class="review-section-head">
                            <h2><xsl:value-of select="QUENAME" disable-output-escaping="yes"/></h2>
                        </div>
			<xsl:for-each select="QUE_ANSWER_MASTER/QUE_ANSWER_MASTER_DATA">
				<div>
				<span class="label"><xsl:value-of select="ANS" disable-output-escaping="yes"/><span class="required">*</span> :</span>
				<!--rating Slider1-->
				<span> <xsl:value-of select="SELECTED_ANSWER" disable-output-escaping="yes"/>/10</span>
				</div>
			</xsl:for-each>
                    </div>
			<xsl:if test="position()mod2=0"><div style="clear:both; padding:0; border-bottom:1px solid #CCCCCC;:"></div></xsl:if>
		</xsl:for-each>
		<div class="clear"></div>
</div><div class="clear"></div>
                    </span>              </div>
            </section>
            <aside>
				<!-- Top newsletter Widget start -->
				<xsl:call-template name="newsletterWidget"/>
			  <!-- Top newsletter Widget end -->
  
              <div class="clear"></div>
              <!-- Top Rated Widget start -->
				<xsl:call-template name="topRatedWidget"/>
			  <!-- Top Rated Widget end -->
              <div class="clear"></div>
            </aside>
		<div class="clear"></div>
      </div>
    <!-- body end -->
    <div class="clear"></div>
    <!--prefooter ad module starts-->
    <div class="prefooter-ad">
    	<img src="images/temp-botmad.gif" border="0" />    </div>
    <!--prefooter ad module starts-->
    <div class="clear"></div>
    <!-- footer starts -->
          <xsl:call-template name="footerDiv"/>
          <!-- footer end -->
    <div class="clear"></div>
</div>
</body>
</html>
</xsl:template>
</xsl:stylesheet>
