<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet  [
  <!ENTITY nbsp   "&#160;">
  <!ENTITY copy   "&#169;">
  <!ENTITY reg    "&#174;">
  <!ENTITY trade  "&#8482;">
  <!ENTITY mdash  "&#8212;">
  <!ENTITY ldquo  "&#8220;">
  <!ENTITY rdquo  "&#8221;">
  <!ENTITY pound  "&#163;">
  <!ENTITY yen    "&#165;">
  <!ENTITY euro   "&#8364;">
  <!ENTITY deg "&#176;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fb="http://www.facebook.com/2008/fbml" version="1.0">
  <xsl:output method="html" encoding="utf-8" indent="yes" doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"/>
  <xsl:include href="inc_header.xsl" />
  <xsl:include href="inc_footer.xsl" />
  <xsl:include href="inc_searchwidget.xsl" />
  <xsl:include href="inc_newsletter_widget.xsl" />
  <xsl:include href="inc_top_rated_widget.xsl" />
  <xsl:template match="/">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><xsl:value-of select="XML/SEO_TITLE" disable-output-esacaping="yes"/></title>
		<meta Name="description" Content="{XML/SEO_DESC}" /> 
		<meta Name="keywords" Content="{XML/SEO_KEYWORDS}" />
        <link href="{/XML/CSS_URL}style.css?version={/XML/VERSION}" rel="stylesheet" type="text/css" />
        <script>
          var selectedTab = '<xsl:value-of select="XML/SELECTEDTABID" disable-output-esacaping="yes"/>';
         </script>
		<script type="text/javascript">
		// Google Analytics start
		  var _gaq = _gaq || [];
		  _gaq.push(['_setAccount', 'UA-27114870-1']);
		  _gaq.push(['_trackPageview']);

		  (function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();
		// Google Analytics end
		//<!-- Quantcast Tag part 1-->

		var _qevents = _qevents || [];

		(function() {
		var elem = document.createElement('script');
		elem.src = (document.location.protocol == "https:" ? "https://secure" : "http://edge") + ".quantserve.com/quant.js";
		elem.async = true;
		elem.type = "text/javascript";
		var scpt = document.getElementsByTagName('script')[0];
		scpt.parentNode.insertBefore(elem, scpt);
		})();

		_qevents.push({
		qacct:"p-31f3D02tYU8zY"
		});
		</script>
		<!-- Quantcast Tag part 1-->
      </head>

      <body>
<!-- Quantcast Tag part 2-->
<noscript>
<div style="display:none;">
<img src="//pixel.quantserve.com/pixel/p-31f3D02tYU8zY.gif" border="0" height="1" width="1" alt="Quantcast"/>
</div>
</noscript>
<!--Quantcast Tag part 2 -->
        <div id="wrapper">
          <!-- header start -->
          <xsl:call-template name="headDiv"/>
          <!-- header end -->
          <div class="clear"></div>
          <!-- body starts -->
          <div id="maincontent-wrapper">
            <section>
            <h1 class="page-heading"><a href="{/XML/WEB_URL}" class="home spriteicon"></a> <span> <a href="{/XML/WEB_URL}">Home</a> / <a href="#">Phones Details: <xsl:value-of select="XML/PRODUCT_DETAIL/PRODUCT_DETAIL_DATA/DISPLAY_PRODUCT_NAME"/></a></span></h1>
            <div class="model-details">
            	<h1><xsl:value-of select="XML/PRODUCT_DETAIL/PRODUCT_DETAIL_DATA/DISPLAY_PRODUCT_NAME"/></h1>

		<div class="sharelinks">
			<!-- FB Like Button -->  
			<span class="flike">
			<div id="fb-root"></div>
			<script>(function(d, s, id) {
			  var js, fjs = d.getElementsByTagName(s)[0];
			  if (d.getElementById(id)) return;
			  js = d.createElement(s); js.id = id;
			  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
			  fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));</script>

			<div class="fb-like" data-href="{/XML/PAGE_NAME}" data-send="false" data-layout="button_count" data-width="50" data-show-faces="false"></div>
			</span>
			<span class="tweet">
			<!-- Tweet Button --> 
			<a href="https://twitter.com/share?url={/XML/PAGE_NAME}" class="twitter-share-button" data-related="Indiacom-Mobile" data-lang="en" data-count="none">Tweet</a>

			<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
			</span>
			<span class="gplus">

			<!-- Place this tag where you want the +1 button to render. -->
			<div class="g-plusone" data-size="medium" data-annotation="none"></div>

			<!-- Place this tag after the last +1 button tag. -->
			<script type="text/javascript">
			$(document).ready(function () {
				var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
				po.src = 'https://apis.google.com/js/plusone.js';
				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			});
			</script>
			</span>
			<div class="clear"></div>
		</div>

                <div class="model-overview">

				<xsl:choose>
				<xsl:when test="XML/PRODUCT_DETAIL/PRODUCT_DETAIL_DATA/IMAGE_PATH=''">
					<img src="/images/preset1.gif" alt="{XML/PRODUCT_DETAIL/PRODUCT_DETAIL_DATA/DISPLAY_PRODUCT_NAME}" title="{XML/PRODUCT_DETAIL/PRODUCT_DETAIL_DATA/DISPLAY_PRODUCT_NAME}" />
				</xsl:when>
				<xsl:otherwise>
					<img src="{XML/PRODUCT_DETAIL/PRODUCT_DETAIL_DATA/IMAGE_PATH}" border="0" title="{XML/PRODUCT_DETAIL/PRODUCT_DETAIL_DATA/DISPLAY_PRODUCT_NAME}" alt="{XML/PRODUCT_DETAIL/PRODUCT_DETAIL_DATA/DISPLAY_PRODUCT_NAME}" />
				</xsl:otherwise>
				</xsl:choose>
				
				<div class="left">
                <span><xsl:value-of select="XML/PRODUCT_DESCRIPTION" disable-output-escaping="yes" /></span></div>
                <!--<div class="middle"><h3><span class="icon spriteicon"></span><span><a href="#">Do people like this?</a></span></h3>
                <p>User Ratings: 9.5/10</p>
                </div> -->
                <div class="right">
			<div class="model-price-wrap">
				<span>
				<xsl:choose>
				<xsl:when test="XML/OVERVIEW/TECH_SPEC_SHORT_DESC/AVG_EX_SHOWROOM_PRICE=0">
					<b>Price*:</b> Not Available
				</xsl:when>
				<xsl:otherwise>
					<h3>Price*: <xsl:value-of select="XML/OVERVIEW/TECH_SPEC_SHORT_DESC/AVG_EX_SHOWROOM_PRICE"/></h3>
				</xsl:otherwise>
				</xsl:choose>
				</span>
			</div>
			<xsl:if test="XML/AVG_EXPERT_RATING &gt; 0">			
			<div class="model-rating-wrap">
				<p class="bgrratings spriteicon"><a href="#" title="{XML/AVG_EXPERT_RATING}/5" class="spriteicon ratings rate{XML/AVG_EXPERT_RATING}" ></a></p>
			</div>
			<div class="clear"></div>
			</xsl:if>
			
			<xsl:if test="XML/PRODUCT_DETAIL/PRODUCT_DETAIL_DATA/AVAILIBILITY  != '' ">
			<div class="model-availability-wrap">
				<span class="availability-since"><strong>Availability</strong><br/><xsl:value-of select="XML/PRODUCT_DETAIL/PRODUCT_DETAIL_DATA/AVAILIBILITY" disable-output-escaping="yes" />  </span>
			</div>
			</xsl:if>
			
			<div class="model-availability-wrap">
				<span class="availability-since"><xsl:value-of select="XML/PRODUCT_DETAIL/PRODUCT_DETAIL_DATA/MESSAGE" disable-output-escaping="yes" />  </span>
			</div>
                </div>
                <div>
                </div>
                <div class="clear"></div>
                </div>
                
                <div class="features-main-wrap">
                	<div class="tabs-mainwrap">
                    	<ul>
							<xsl:for-each select="/XML/GROUP_MASTER/GROUP_MASTER_DATA">

                        	<!--li><a href="javascript:void(0);" id="featuredtab1" class="active">Functionality</a></li-->

							<xsl:if test="GROUP_ID=3">
								<li><a href="{SEO_URL}" id="featuredtab3">Functionality</a></li>
							</xsl:if>

							</xsl:for-each>

                            <!--li><a href="{/XML/USERREVIEW_SEO_URL}" id="featuredtab2">Ratings &amp; Reviews</a></li>
                            <li><a href="javascript:void(0);" id="featuredtab3">Ratings &amp; Reviews</a></li>
                            <li><a href="javascript:void(0);" id="featuredtab4">Photos &amp; Videos</a></li>
                            <li><a href="javascript:void(0);" id="featuredtab5">360 &deg;</a></li> -->
                        </ul>
                    </div>
                    <div class="tab-content-wrap" id="featuredcontent1">
                        	
                        
                        <div class="clear"></div>
                    </div>
                    <div class="tab-content-wrap hide show" id="featuredcontent2" >
                    	<h3 class="model-features">Review</h3>
						<table border="1">
						<tr>
						<td>&nbsp;</td>
						<!--<td><a href="{/XML/WEB_URL}write_user_review.php?pid={/XML/SELECTED_PRODUCT_ID}&amp;bid={/XML/SELECTED_BRAND_ID}">Write review</a></td>-->
						<td><a href="{/XML/WEB_URL}Mobile/review/write/{/XML/SELECTED_PRODUCT_ID}/{/XML/SELECTED_BRAND_ID}">Write review</a></td>
						</tr>
						<xsl:for-each select="/XML/USER_REVIEW_MASTER/USER_REVIEW_MASTER_DATA">
						<tr>
						<td colspan="2">My experience with my <xsl:value-of select="BRAND_MODEL_VARIANT_NAME" disable-output-escaping="yes"/></td>
						</tr>
						<tr>
						<td colspan="2">By <xsl:value-of select="USER_NAME" disable-output-escaping="yes"/>| Posted on <xsl:value-of select="CREATE_DATE" disable-output-escaping="yes"/></td>
						</tr>
						<tr>
						<td colspan="2">
							<xsl:for-each select="/XML/USER_REVIEW_ANSWER_MASTER/USER_REVIEW_ANSWER_MASTER_DATA">
									<xsl:if test="position()=1"><xsl:value-of select="QUENAME" disable-output-escaping="yes"/></xsl:if>
							</xsl:for-each>
							</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<!--<td><a href="{/XML/WEB_URL}view_user_full_review.php?user_review_id={USER_REVIEW_ID}&amp;pid={PRODUCT_ID}">Read more&gt;&gt;</a></td>-->
						<td><a href="{/XML/WEB_URL}Mobile/review/read/{PRODUCT_ID}/{USER_REVIEW_ID}">Read more&gt;&gt;</a></td>
						</tr>
						</xsl:for-each>
						</table>                  
                        
                        <div class="clear"></div>
                    </div>

					<div class="tab-content-wrap hide" id="featuredcontent3" >
                        
						
<!-- Column Left -->	                    
						<div class="featured-sec-wrapper-columns">
						<h3 class="model-features"><xsl:value-of select="XML/PRODUCT_DETAIL/PRODUCT_DETAIL_DATA/DISPLAY_PRODUCT_NAME"/> Features</h3>
						<xsl:for-each select="/XML/GROUP_MASTER/GROUP_MASTER_DATA">
							<xsl:if test="GROUP_ID=3">
							<xsl:for-each select="SUB_GROUP_MASTER">
							<xsl:if test="position()&lt;=7">
							<xsl:if test="PIVOT_FEATURE_ID!=FEATURE_ID and SUB_GROUP_ID!=29">
                            <div class="featured-sec-single">
								<h4><xsl:value-of select="SUB_GROUP_NAME" disable-output-escaping="yes"/></h4>
                                <ul>
								
								<xsl:for-each select="SUB_GROUP_MASTER_DATA">
                                <xsl:if test="SHOW_IF_EMPTY='1'">
									<li><p class="label"><xsl:value-of select="FEATURE_NAME" disable-output-esacaping="yes"/></p>
									<p>
									<xsl:choose>
									  <xsl:when test="FEATURE_VALUE='yes'">
										<img src="{/XML/FEATURE_YES_IMAGE_URL}" alt="{FEATURE_VALUE}" title="{FEATURE_VALUE}"/>
									  </xsl:when>
									  <xsl:otherwise>
										<xsl:choose>
										  <xsl:when test="FEATURE_VALUE='no'">
											<img src="{/XML/FEATURE_NO_IMAGE_URL}" alt="{FEATURE_VALUE}" title="{FEATURE_VALUE}"/>
										  </xsl:when>
										  <xsl:otherwise>
											<xsl:value-of select="FEATURE_VALUE" disable-output-esacaping="yes"/>
										  </xsl:otherwise>
										</xsl:choose>
									  </xsl:otherwise>
									</xsl:choose>
									</p></li>
								</xsl:if>
								</xsl:for-each>
								</ul>
							</div>
							</xsl:if>
							</xsl:if>
							</xsl:for-each>
							</xsl:if>
							</xsl:for-each>
						
						</div>
<!-- Column Left -->
<!-- Column Right -->
						<div class="featured-sec-wrapper-columns">
							<div class="imp-stats">
								<h3>Important Stats</h3>
								
								<xsl:for-each select="/XML/GROUP_MASTER/GROUP_MASTER_DATA">
								<xsl:if test="GROUP_ID=3">
								<xsl:for-each select="SUB_GROUP_MASTER">
								<xsl:if test="SUB_GROUP_ID=29">
	<xsl:for-each select="SUB_GROUP_MASTER_DATA">
								<div class="single-specs">
									<img src="{/XML/IMAGE_URL}{FEATURE_IMG_PATH}"  border="0"/><div><h3><xsl:value-of select="FEATURE_NAME" disable-output-esacaping="yes"/></h3><xsl:value-of select="FEATURE_VALUE" disable-output-esacaping="yes"/></div>
								</div>
	</xsl:for-each>
								</xsl:if>
								</xsl:for-each>
								</xsl:if>
								</xsl:for-each>

							</div>

							<xsl:for-each select="/XML/GROUP_MASTER/GROUP_MASTER_DATA">
							<xsl:if test="GROUP_ID=3">
							<xsl:for-each select="SUB_GROUP_MASTER">
							<xsl:if test="position()&gt;7">
							<xsl:if test="PIVOT_FEATURE_ID!=FEATURE_ID and SUB_GROUP_ID!=29">
                            <div class="featured-sec-single">
								<h4><xsl:value-of select="SUB_GROUP_NAME" disable-output-escaping="yes"/></h4>
                                <ul>
								
								<xsl:for-each select="SUB_GROUP_MASTER_DATA">
                                <xsl:if test="SHOW_IF_EMPTY='1'">
									<li><p class="label"><xsl:value-of select="FEATURE_NAME" disable-output-esacaping="yes"/></p>
									<p>
									<xsl:choose>
									  <xsl:when test="FEATURE_VALUE='yes'">
										<img src="{/XML/FEATURE_YES_IMAGE_URL}" alt="{FEATURE_VALUE}" title="{FEATURE_VALUE}"/>
									  </xsl:when>
									  <xsl:otherwise>
										<xsl:choose>
										  <xsl:when test="FEATURE_VALUE='no'">
											<img src="{/XML/FEATURE_NO_IMAGE_URL}" alt="{FEATURE_VALUE}" title="{FEATURE_VALUE}"/>
										  </xsl:when>
										  <xsl:otherwise>
											<xsl:value-of select="FEATURE_VALUE" disable-output-esacaping="yes"/>
										  </xsl:otherwise>
										</xsl:choose>
									  </xsl:otherwise>
									</xsl:choose>
									</p></li>
								</xsl:if>
								</xsl:for-each>
								</ul>
							</div>
							</xsl:if>
							</xsl:if>
							</xsl:for-each>
							</xsl:if>
							</xsl:for-each>
<!-- Column Right -->						
						</div>
						
						
						
						

							
						
						<div class="clear"></div>
                    </div>

					<div class="tab-content-wrap hide" id="featuredcontent4" >
                        <div class="clear"></div>
                    </div>

					<div class="tab-content-wrap hide" id="featuredcontent5" >
                        <div class="clear"></div>
                    </div>

                </div>
            </div>
            <input type="hidden" id="disp_container" value="1" />
            <div class="clear"></div>
            </section>
			
			<aside>

			<div class="RHSAd">
				<!--JavaScript Tag with group ID // Tag for network 1191: India.com // Website: Buying Guide // Page: Phone List // Placement: Buying_Guide_Phone_List_Right_Top_300x250 (3483230) // created at: Oct 31, 2011 8:34:33 AM-->
				<script language="javascript">
				<![CDATA[
				if (window.adgroupid == undefined) {
					window.adgroupid = Math.round(Math.random() * 1000);
				}
				document.write('<scr'+'ipt language="javascript1.1" src="http://jt.india.com/addyn/3.0/1191/3483230/0/170/ADTECH;cookie=info;loc=100;target=_blank;key=key1+key2+key3+key4;grp='+window.adgroupid+';misc='+new Date().getTime()+'"></scri'+'pt>');
				]]>
				</script><noscript><a href="http://jt.india.com/adlink/3.0/1191/3483230/0/170/ADTECH;cookie=info;loc=300;key=key1+key2+key3+key4" target="_blank"><img src="http://jt.india.com/adserv/3.0/1191/3483230/0/170/ADTECH;cookie=info;loc=300;key=key1+key2+key3+key4" border="0" width="300" height="250" /></a></noscript>
				<!-- End of JavaScript Tag -->
				</div>
				<div class="clear"></div>

              <!-- Top newsletter Widget start -->
				<xsl:call-template name="newsletterWidget"/>
			  <!-- Top newsletter Widget end -->
  
              <div class="clear"></div>
              <!-- Top Rated Widget start -->
				<xsl:call-template name="topRatedWidget"/>
			  <!-- Top Rated Widget end -->
              <div class="clear"></div>
            </aside>

            <div class="clear"></div>
          </div>
          <!-- body end -->
          <div class="clear"></div>
          <!--prefooter ad module starts-->
          <div class="prefooter-ad">
            <!--JavaScript Tag with group ID // Tag for network 1191: India.com // Website: Buying Guide // Page: Phone List // Placement: Buying_Guide_Phone_List_Bottom_728x90 (3483231) // created at: Oct 31, 2011 8:34:34 AM-->
			<script language="javascript">
			<![CDATA[
			if (window.adgroupid == undefined) {
				window.adgroupid = Math.round(Math.random() * 1000);
			}
			document.write('<scr'+'ipt language="javascript1.1" src="http://jt.india.com/addyn/3.0/1191/3483231/0/225/ADTECH;cookie=info;loc=100;target=_blank;key=key1+key2+key3+key4;grp='+window.adgroupid+';misc='+new Date().getTime()+'"></scri'+'pt>');
			]]>
			</script><noscript><a href="http://jt.india.com/adlink/3.0/1191/3483231/0/225/ADTECH;cookie=info;loc=300;key=key1+key2+key3+key4" target="_blank"><img src="http://jt.india.com/adserv/3.0/1191/3483231/0/225/ADTECH;cookie=info;loc=300;key=key1+key2+key3+key4" border="0" width="728" height="90" /></a></noscript>
			<!-- End of JavaScript Tag -->
          </div>
          <!--prefooter ad module starts-->
          <div class="clear"></div>
          <!-- footer starts -->
          <xsl:call-template name="footerDiv"/>
          <!-- footer end -->
          <div class="clear"></div>
        </div>
        <script>
          <xsl:text disable-output-escaping="yes">
            <![CDATA[
              if(selectedTab=='' || selectedTab == 'undefined'){
                selectedTab=3;
              }
              $('#featuredtab'+selectedTab).addClass("active");
              $('.tab-content-wrap').removeClass("show");
			  $('.tab-content-wrap').addClass("hide");
			  $('#featuredcontent'+selectedTab).css("display","block");
            ]]>
          </xsl:text>
        </script>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
