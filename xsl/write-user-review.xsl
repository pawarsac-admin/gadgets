<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet  [
  <!ENTITY nbsp   "&#160;">
  <!ENTITY copy   "&#169;">
  <!ENTITY reg    "&#174;">
  <!ENTITY trade  "&#8482;">
  <!ENTITY mdash  "&#8212;">
  <!ENTITY ldquo  "&#8220;">
  <!ENTITY rdquo  "&#8221;">
  <!ENTITY pound  "&#163;">
  <!ENTITY yen    "&#165;">
  <!ENTITY euro   "&#8364;">
  <!ENTITY deg "&#176;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fb="http://www.facebook.com/2008/fbml" version="1.0">
  <xsl:output method="html" encoding="utf-8" indent="yes" doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"/>
  <xsl:include href="inc_header.xsl" />
  <xsl:include href="inc_footer.xsl" />
  <xsl:include href="inc_newsletter_widget.xsl" />
  <xsl:include href="inc_top_rated_widget.xsl" />
  <xsl:template match="/">
    <html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Buying Guides</title>
<link href="{/XML/CSS_URL}style.css?version={/XML/VERSION}" rel="stylesheet" type="text/css" />
<script>
<xsl:text disable-output-escaping="yes">
<![CDATA[
	var ck_title = /^[ \!#\$%&\'\(\)\*\+,-\.\/0-9\:\?@\:A-Z\[\\\]a-z]{3,120}$/;
	var regxp_alphanumeric = /^([a-zA-Z0-9'-/_ ]+)$/;
	var regxp_alpha = /^([a-zA-Z]+)$/;
	var regxp_numeric = /^([0-9,]+)$/;
	var regxp_color = /^([ \!#\$%&\'\(\)\*\+,-\.\/0-9\:\?@\:A-Z\[\\\]a-z]+)$/;
	var regxp_city = /^([ \!#\$%&\'\(\)\*\+,-\.\/\:\?@\:A-Z\[\\\]a-z]+)$/;
   function validate(){
	if(document.getElementById('uname').value == ''){
		alert("Please enter your Name");
		document.getElementById('uname').focus();
		return false;
	}else{
		var uname = jQuery.trim(document.getElementById('uname').value);
		if(regxp_alphanumeric.test(uname)==false){
			alert( "Please enter a valid Name" );
			document.getElementById('uname').focus();
			return false;
		}
	}
	if(document.getElementById('usr_review_emailid').value == ''){
		alert("Please enter your email");
		document.getElementById('usr_review_emailid').focus();
		return false;
	}else if(document.getElementById('usr_review_emailid').value != ''){
		if(emailValidator(document.getElementById('usr_review_emailid').value)==false){
			alert("Please enter a valid email address");
			document.getElementById('usr_review_emailid').focus();
			return false;
		}
	}
	if(document.getElementById('location').value == ''){
		alert("Please enter your City");
		document.getElementById('location').focus();
		return false;
	}else{
		var city = document.getElementById('location').value;
		if(regxp_city.test(city)==false){
			alert( "Please enter a valid city name." );
			document.getElementById('location').focus();
			return false;
		}
	}

	//var productid = document.getElementById('ModelVariant').value;
	var productid = document.getElementById('Model').value;
	var brand = document.getElementById('Brand').value;

	if(!brand){
		alert("Please select Brand");
		return false;
	}
	if(!productid){
		//alert("Please select Product");
		//return false;
	}
	if(validateRating() == true){
		return true;
	}
	return false;
   }
	function validateRating(){
		var totalquestion = document.getElementById('totalquestion').value;
		for(i=1;i<=totalquestion;i++){
			var que_id = document.getElementById('que_id_'+i).value;
			var totalanscnt = document.getElementById('total_ans_ques_'+que_id).value;
			for(ans=1;ans<=totalanscnt;ans++){
				var ans_id = document.getElementById('ans_'+que_id+'_'+ans).value;
				var is_reviewed = document.getElementById('user_review_'+que_id+'_'+ans_id).value;
				if(is_reviewed == 0){
					var ansTitle = document.getElementById('ans_review_'+que_id+'_'+ans_id).value;
					alert("please rate "+ansTitle);
					return false;
				}
			}
		}
		return true;
	}
	function getModelByBrand(iModelId,surl,divname,param){
		var iBrndId=document.getElementById('Brand').value;
		if(iBrndId != ""){
			var str="action=model&brand_id="+iBrndId+"&product_name_id="+iModelId+"&Rand="+Math.random();
			var data = $.ajax({url: siteURL+surl,data: str,async: false}).responseText;
			$('#Model').html(data);
			document.getElementById('Model').disabled=false;
		}else{
			$('#Model').empty().append('<option value="">All Products</option>');
		}
	}
]]>
</xsl:text>
var siteURL = '<xsl:value-of select="/XML/WEB_URL" disable-output-escaping="yes"/>';
</script>
</head>

<body>
<div id="wrapper">
	<!-- header start -->
          <xsl:call-template name="headDiv"/>
          <!-- header end -->
    <div class="clear"></div>
   	<!-- body starts -->

    <div id="maincontent-wrapper">
	 <form name="add_usr_review" method="post" id="form1" action="{/XML/WEB_URL}write_user_review.php" onSubmit="return validate();">
	 <input type="hidden" name="add_review" value="1"/>
                  <input type="hidden" name="user_rev_url" value="{/XML/RETURN_REVIEW_URL}"/>
        	<section>
				<div class="write-user-review">
                    <!--Product features intro-->
                    <span class="section"> <!--
                    <h2>Mobile Features</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam nec magna turpis, tincidunt varius tortor. Duis sem libero, condimentum a vulputate nec, accumsan in nibh. Aliquam quis nisl vitae sapien lobortis hendrerit. Praesent eu urna ante. Nam diam turpis, ultrices ac rutrum vel, consequat a lacus. <a href="#">Read More</a></p>
                    <div class="clear"></div>-->
                    </span>
                    <!--Personal details-->
                    <span class="section">
                    <h2>Your Personal Details</h2>
                    <div>
                    	<span class="label">Name<span class="required">*</span> :</span><input name="uname" id="uname" type="text" />
                    </div>
                    <div>
                    	<span class="label">Email ID<span class="required">*</span> :</span><input name="usr_review_emailid" id="usr_review_emailid" type="text" />
                    </div>
                    <div>
                    	<span class="label">City<span class="required">*</span> :</span><input name="location" id="location" type="text" />
                    </div>
                    <div class="clear"></div>
                    </span>
                    <!--Products Brands details-->
					<xsl:choose>
					<xsl:when test="/XML/SELECTED_PRODUCT_ID='' and /XML/SELECTED_BRAND_ID=''">
                    <span class="section">
                    <h2>Brand Details</h2>
                    <div>
                    	<span class="label">Select Brand<span class="required">*</span> :</span>
						<select name="Brand" id="Brand" onchange="getModelByBrand('','get_model_detail.php','Model','')">
                            <option value="">All Brands</option>
                            <xsl:for-each select="/XML/BRAND_MASTER/BRAND_MASTER_DATA">
                              <xsl:choose>
                                <xsl:when test="BRAND_ID=/XML/BRAND_ID">
                                  <option value="{BRAND_ID}" selected='yes'> <xsl:value-of select="BRAND_NAME"/> </option>
                                </xsl:when>
                                <xsl:otherwise>
                                  <option value="{BRAND_ID}"> <xsl:value-of select="BRAND_NAME"/> </option>
                                </xsl:otherwise>
                              </xsl:choose>
                            </xsl:for-each>
                          </select>
				   </div>
				   <div>
						  <span class="label">Select Products<span class="required">*</span> :</span>
						  <select name="Model" id="Model">
                            <option value="">All Product</option>
                          </select>
					</div>
                    <div class="clear"></div>
                    </span>
					</xsl:when>
					<xsl:otherwise>

					<input type="hidden" name="Brand" id="Brand" value="{/XML/SELECTED_BRAND_ID}"/>
					<input type="hidden" name="Model" id="Model" value="{/XML/SELECTED_PRODUCT_ID}"/>
					</xsl:otherwise>
					</xsl:choose>


                    <!--How to rate your mobile details-->
					<input type="hidden" name="totalquestion" id="totalquestion" value="{/XML/QUESTIONAIRE_MASTER/COUNT}"/>
                    <span class="section">
                    <h2>How would you Rate the Mobile?</h2>
					 <xsl:for-each select="/XML/QUESTIONAIRE_MASTER/QUESTIONAIRE_MASTER_DATA">
					 <input type="hidden" name="que_id_{position()}" id="que_id_{position()}" value="{QUEID}"/>
                    <div class="rating-component-single">
                        <div class="review-section-head">
                            <h2><xsl:value-of select="QUENAME"/></h2>
                        </div>
						<input type="hidden" name="total_ans_ques_{QUEID}" id="total_ans_ques_{QUEID}" value="{QUESTIONAIRE_ANS_MASTER/ANS_COUNT}"/>
						 <xsl:choose>
                          <xsl:when test="QUESTIONAIRE_ANS_MASTER/ANS_COUNT&gt;0">
                            <xsl:for-each select="QUESTIONAIRE_ANS_MASTER/QUESTIONAIRE_ANS_MASTER_DATA">
                              <input type="hidden" name="ans_{QUEID}_{position()}" id="ans_{QUEID}_{position()}" value="{ANS_ID}"/>
                              <input type="hidden" name="user_review_{QUEID}_{ANS_ID}" id="user_review_{QUEID}_{ANS_ID}" value="0"/>
                              <input type="hidden" name="ans_review_{QUEID}_{ANS_ID}" id="ans_review_{QUEID}_{ANS_ID}" value="{ANS}"/>
							  <input type="hidden" name="clickdone_{QUEID}_{ANS_ID}" id="clickdone_{QUEID}_{ANS_ID}" />
								<div>
									<span class="label"><xsl:value-of select="ANS" disable-output-escaping="yes"/><span class="required">*</span> :</span>
									<!--rating Slider1-->
										<span class="user-write-rating demo">
										<div id="slider_{QUEID}_{ANS_ID}"></div><input type="text" id="amount_{QUEID}_{ANS_ID}" style="border:0; color:#f6931f; font-weight:normal; font-size:11px; color:#000; width:230px; text-align:center;" />
										</span>
								</div>
								<div class="clear"></div>
								</xsl:for-each>
								</xsl:when>
					      <xsl:otherwise>
					      </xsl:otherwise>
				      </xsl:choose>
                    </div>
</xsl:for-each>
					<div class="clear"></div>
					<input type="submit" value="submit" style="float:right;" class="submit-review" />
					<div class="clear"></div>

                    </span>              </div>
            </section>
			</form>
            <aside>
            
			<!-- Top newsletter Widget start -->
			<xsl:call-template name="newsletterWidget"/>
			<!-- Top newsletter Widget end -->

            <div class="clear"></div>
              <!-- Top Rated Widget start -->
				<xsl:call-template name="topRatedWidget"/>
			  <!-- Top Rated Widget end -->
			<div class="clear"></div>
            </aside>
		<div class="clear"></div>
      </div>
    <!-- body end -->

    <div class="clear"></div>
    <!--prefooter ad module starts-->
    <div class="prefooter-ad">
    	<img src="images/temp-botmad.gif" border="0" />    </div>
    <!--prefooter ad module starts-->
    <div class="clear"></div>
     <!-- footer starts -->
          <xsl:call-template name="footerDiv"/>
          <!-- footer end -->
    <div class="clear"></div>
</div>
<script>
 $(document).ready(function() {
	$(function() {
		<xsl:for-each select="/XML/QUESTIONAIRE_MASTER/QUESTIONAIRE_MASTER_DATA">
			<xsl:for-each select="QUESTIONAIRE_ANS_MASTER/QUESTIONAIRE_ANS_MASTER_DATA">
				var sliderid = "slider_"+<xsl:value-of select="QUEID" disable-output-escaping="yes"/>+"_"+<xsl:value-of select="ANS_ID" disable-output-escaping="yes"/>;
				var amtid = "amount_"+<xsl:value-of select="QUEID" disable-output-escaping="yes"/>+"_"+<xsl:value-of select="ANS_ID" disable-output-escaping="yes"/>;
			//	$( "#"+sliderid).slider();
			$("#slider_"+<xsl:value-of select="QUEID" disable-output-escaping="yes"/>+"_"+<xsl:value-of select="ANS_ID" disable-output-escaping="yes"/>).slider({
				range: "min",
				min: 0,
				max: 10,
				value: [ 5 ],
				slide: function( event, ui ) {
					$("#amount_"+<xsl:value-of select="QUEID" disable-output-escaping="yes"/>+"_"+<xsl:value-of select="ANS_ID" disable-output-escaping="yes"/>).val(+ ui.value+"/10");
					var str = $(this).attr("id");
					$("#"+str.replace("slider_","user_review_")).val(ui.value);
				}
			});
			$( "#amount_"+<xsl:value-of select="QUEID" disable-output-escaping="yes"/>+"_"+<xsl:value-of select="ANS_ID" disable-output-escaping="yes"/>).val('5/10');
			</xsl:for-each>
		</xsl:for-each>
	});
});
</script>
</body>
</html>
</xsl:template>
</xsl:stylesheet>
