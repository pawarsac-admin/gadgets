<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet  [
  <!ENTITY nbsp   "&#160;">
  <!ENTITY copy   "&#169;">
  <!ENTITY reg    "&#174;">
  <!ENTITY trade  "&#8482;">
  <!ENTITY mdash  "&#8212;">
  <!ENTITY ldquo  "&#8220;">
  <!ENTITY rdquo  "&#8221;">
  <!ENTITY pound  "&#163;">
  <!ENTITY yen    "&#165;">
  <!ENTITY euro   "&#8364;">
  <!ENTITY deg "&#176;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fb="http://www.facebook.com/2008/fbml" version="1.0">
  <xsl:output method="html" encoding="utf-8" indent="yes" doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"/>
  <xsl:include href="inc_header.xsl" />
  <xsl:include href="inc_footer.xsl" />
  <xsl:include href="inc_searchwidget.xsl" />
  <xsl:include href="inc_newsletter_widget.xsl" />
  <xsl:include href="inc_top_rated_widget.xsl" />
  <xsl:template match="/">
    <html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><xsl:value-of select="/XML/SEO_TITLE" /></title>
<meta name="Keywords" content="{/XML/SEO_TAGS}" />
<meta name="Description" content="{/XML/SEO_DESC}" />
<link href="{/XML/CSS_URL}style.css?version={/XML/VERSION}" rel="stylesheet" type="text/css" />
<!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--<script type="text/javascript" src="https://getfirebug.com/firebug-lite.js"></script> -->
<script>
	var comparedIds	= '';
	var catid		= '<xsl:value-of select="/XML/SELECTED_CATEGORY_ID" disable-output-escaping="yes"/>';
	var dTypeId		= '<xsl:value-of select="/XML/SELECTED_FEATURE_ID" disable-output-escaping="yes"/>';
	var currenttab	= '<xsl:value-of select="/XML/SELECTEDTABID" disable-output-escaping="yes"/>';
	var from_phone_finder	= '<xsl:value-of select="/XML/FROM_PHONE_FINDER" disable-output-escaping="yes"/>';
	var searchTab	= '<xsl:value-of select="/XML/SEARCHTYPE" disable-output-escaping="yes"/>';
<xsl:text disable-output-escaping="yes">
<![CDATA[

function sortBy(){

	var debug	= 0;

	var pageurl	= location.href;

	if(debug == 1) alert('pageurl before page set to 1 : : ' + pageurl);

	var re	= /page-(\d+)/

    while (re.test(pageurl)) {
        pageurl = pageurl.replace(re, "page-1");
		break;
	}

	if(debug==1) alert('pageurl after page set to 1 : ' + pageurl);

	var selectIndex	= $('#select_sort_by').prop('selectedIndex');

	if(debug==1) alert('selectIndex:'+selectIndex);
	switch(selectIndex){
		case 0 : tText	= 'price-asc'; break
		case 1 : tText	= 'price-desc'; break
		case 2 : tText	= 'name-asc'; break
		case 3 : tText	= 'name-desc'; break
	}
	tText = 'orderby-' + tText;

	if(debug==1) alert('NEW ORDER BY TEXT : '+tText);

	var indx	= pageurl.indexOf("orderby");

	if(debug==1) alert('orderby indx : '+indx);

	if( indx > 0 ){

		indxOfSlash	= pageurl.indexOf("/",indx );

		if(debug==1) alert('indx Of Slash : '+indxOfSlash);

		if( indxOfSlash > -1 ){
			limit = indxOfSlash;
		} else {
			limit = pageurl.length;
		}
		
		if(debug==1) alert(limit);

		orderByText	= pageurl.substring(indx,limit);
		if(debug==1) alert('Old orderByText : '+orderByText);

		pageurl		= pageurl.replace(orderByText, tText );
		if(debug==1) alert('New pageurl 2 : '+pageurl);

	} else {
		pageurl		+= '/' + tText;
		if(debug==1) alert('New pageurl 1 : '+pageurl);
	}
	location.href= pageurl;
}

]]>
</xsl:text>
</script>
		<script type="text/javascript">
		// Google Analytics start
		  var _gaq = _gaq || [];
		  _gaq.push(['_setAccount', 'UA-27114870-1']);
		  _gaq.push(['_trackPageview']);

		  (function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();
		// Google Analytics end
		//<!-- Quantcast Tag part 1-->

		var _qevents = _qevents || [];

		(function() {
		var elem = document.createElement('script');
		elem.src = (document.location.protocol == "https:" ? "https://secure" : "http://edge") + ".quantserve.com/quant.js";
		elem.async = true;
		elem.type = "text/javascript";
		var scpt = document.getElementsByTagName('script')[0];
		scpt.parentNode.insertBefore(elem, scpt);
		})();

		_qevents.push({
		qacct:"p-31f3D02tYU8zY"
		});
		</script>
		<!-- Quantcast Tag part 1-->
</head>

<body>
<!-- Quantcast Tag part 2-->
<noscript>
<div style="display:none;">
<img src="//pixel.quantserve.com/pixel/p-31f3D02tYU8zY.gif" border="0" height="1" width="1" alt="Quantcast"/>
</div>
</noscript>
<!--Quantcast Tag part 2 -->
<div id="wrapper">
	<!-- header start -->
          <xsl:call-template name="headDiv"/>
          <!-- header end -->
    <div class="clear"></div>
   	<!-- body starts -->
    <div id="maincontent-wrapper">
        	<section>
            <div class="phone-listing-wrap inactive">
	            <h2 class="spriteicon">Hot deals</h2>
				<!-- search widget start -->
					<xsl:call-template name="searchWidget"/>
				<!-- search widget end -->
            </div>
            <div class="clear"></div>
		
			<div id="floating-box" class='compare-box-wrap' style="display:none;"></div>
           
			<div class="clear"></div>
            <div id="body" class="search-result-compare">
            <div class="search-results-wrap">
            	<div class="search-results-header">
                	<div style="float:left; width: 560px;">
                    <span>Phone Listings : </span> 
					<xsl:choose>
                        <xsl:when test="/XML/SELECTED_ITEM_MASTER/SELECTED_ITEM_MASTER_DATA=''">
                           All Phones, All Brands, All Prices
                        </xsl:when>
                        <xsl:otherwise>
                          <xsl:value-of select="/XML/SELECTED_ITEM_MASTER/SELECTED_ITEM_MASTER_DATA" disable-output-escaping="yes"/>
                        </xsl:otherwise>
                      </xsl:choose> </div>

					<span class="sortby">sort by <select name="select_sort_by" id="select_sort_by" onchange="sortBy()">
                        <xsl:choose>
                          <xsl:when test="/XML/SORT_PRODUCT_BY=1">
                            <option value="1" selected="selected">Price (Low-High)</option>
                          </xsl:when>
                          <xsl:otherwise>
                            <option value="1">Price (Low-High)</option>
                          </xsl:otherwise>
                        </xsl:choose>
                        <xsl:choose>
                          <xsl:when test="/XML/SORT_PRODUCT_BY=2">
                            <option value="2" selected="selected">Price (High-Low)</option>
                          </xsl:when>
                          <xsl:otherwise>
                            <option value="2">Price (High-Low)</option>
                          </xsl:otherwise>
                        </xsl:choose>
                        <xsl:choose>
                          <xsl:when test="/XML/SORT_PRODUCT_BY=3">
                            <option value="3" selected="selected">Name (A-Z)</option>
                          </xsl:when>
                          <xsl:otherwise>
                            <option value="3">Name (A-Z)</option>
                          </xsl:otherwise>
                        </xsl:choose>
                        <xsl:choose>
                          <xsl:when test="/XML/SORT_PRODUCT_BY=4">
                            <option value="4" selected="selected">Name (Z-A)</option>
                          </xsl:when>
                          <xsl:otherwise>
                            <option value="4">Name (Z-A)</option>
                          </xsl:otherwise>
                        </xsl:choose>
                      </select></span>

                    <div class="clear"></div>
                </div>
                <div class="clear"></div>

				<div style="width:100%;text-align:center">
				<xsl:value-of select="/XML/NOPRODUCTS" disable-output-escaping="yes"/>
				</div>

                <!--single search results starts-->
                 <xsl:for-each select="/XML/PRODUCT_MASTER/PRODUCT_MASTER_DATA">
                    <div class="search-results-single-wrap">
                      <div class="search-results-single">
                        
						<a href="{SEO_URL}">
						<xsl:choose>
						<xsl:when test="IMAGE_PATH=''">
							<img src="/images/preset1.gif" alt="{DISPLAY_PRODUCT_NAME}" title="{DISPLAY_PRODUCT_NAME}" border="0" />
						</xsl:when>
						<xsl:otherwise>
							<img alt="{DISPLAY_PRODUCT_NAME}" title="{DISPLAY_PRODUCT_NAME}" src="{IMAGE_PATH}" border="0"/>
						</xsl:otherwise>
						</xsl:choose>
						</a>
                        
						<div class="single-details">
                          <h3>
                            <a href="{SEO_URL}">
                            <xsl:value-of select="DISPLAY_PRODUCT_NAME" disable-output-escaping="yes"/>
                            </a>
                          </h3>
                          <span>
                            <xsl:value-of select="PRODUCT_DESC" disable-output-escaping="yes"/>
                          </span>
                          <div class="clear"></div>
                          <a href="{SEO_URL}">read more...</a>
                        </div>
                        <div class="price-ratings">
                          <p>
                            <span class="price">Price*</span>
                            <span>
								<xsl:choose>
									<xsl:when test="EXSHOWROOMPRICE!=''">
									  <xsl:value-of select="EXSHOWROOMPRICE" disable-output-escaping="yes"/>/-
									</xsl:when>
									<xsl:otherwise>
									   <span class="noprice"> Not Available</span>
									</xsl:otherwise>
								  </xsl:choose>
							</span>
                          </p>
                          <xsl:if test="AVG_EXPERT_RATING != '' and AVG_EXPERT_RATING &gt; 0">
			  <p class="ratings">
                            <span>BGR Ratings</span>
                            <a href="javascript:undefined;" class="spriteicon ratepage{AVG_EXPERT_RATING}" title="{AVG_EXPERT_RATING}/5"></a>
                          </p>
			  </xsl:if>

                        </div>
                      </div>
                      <div class="checkboxwrap">
                        <input type="checkbox" value="{PRODUCT_ID}" name="compare_product_{position()}" id="compare_product_{PRODUCT_ID}_{PRODUCT_ID}" onclick="setCookieForCompare('{PRODUCT_ID}','{DISPLAY_PRODUCT_NAME}',1,0);"/>
                      </div>
                    </div>
					<xsl:for-each select="SIMILAR_PRODUCT_MASTER">
					<div class="search-results-single-wrap">
                      <div class="search-results-single">

						<a href="{SEO_URL}">
						<xsl:choose>
						<xsl:when test="IMAGE_PATH=''">
							<img src="/images/preset1.gif" alt="{DISPLAY_PRODUCT_NAME}" title="{DISPLAY_PRODUCT_NAME}" border="0" />
						</xsl:when>
						<xsl:otherwise>
							<img alt="{DISPLAY_PRODUCT_NAME}" title="{DISPLAY_PRODUCT_NAME}" src="{IMAGE_PATH}" border="0"/>
						</xsl:otherwise>
						</xsl:choose>
						</a>

						<div class="single-details">
                          <h3>
                            <a href="{SEO_URL}">
                            <xsl:value-of select="DISPLAY_PRODUCT_NAME" disable-output-escaping="yes"/>
                            </a>
                          </h3>
                          <span>
                            <xsl:value-of select="PRODUCT_DESC" disable-output-escaping="yes"/>
                          </span>
                          <div class="clear"></div>
                          <a href="{SEO_URL}">read more...</a>
                        </div>                        

						<div class="price-ratings">
                          <p>
                            <span class="price">Price*</span>
                            <span>
							<style>
							.noprice { color: #0A2A53; display: block;font: bold 9px Arial,Helvetica,sans-serif; }
							</style>
								<xsl:choose>
									<xsl:when test="EXSHOWROOMPRICE!=''">
									  <xsl:value-of select="EXSHOWROOMPRICE" disable-output-escaping="yes"/>/-
									</xsl:when>
									<xsl:otherwise>
									   <span class="noprice"> Not Available</span>
									</xsl:otherwise>
								  </xsl:choose>
							</span>
                          </p>
			  <xsl:if test="AVG_EXPERT_RATING != '' and AVG_EXPERT_RATING &gt; 0">
                          <p class="ratings">
                            <span>BGR Ratings</span>
                            <a href="javascript:undefined;" class="spriteicon ratepage{AVG_EXPERT_RATING}" title="{AVG_EXPERT_RATING}/5"></a>
                          </p>
			  </xsl:if>
                        </div>

                      </div>
                      <div class="checkboxwrap">
                        <input type="checkbox" value="{PRODUCT_ID}" name="compare_product_{position()}" id="compare_product_{PRODUCT_ID}_{PRODUCT_ID}" onclick="setCookieForCompare('{PRODUCT_ID}','{DISPLAY_PRODUCT_NAME}',1,0);"/>
                      </div>
                    </div>
					</xsl:for-each>
                  </xsl:for-each>
                <!--single search results ends-->
               <xsl:if test="count(/XML/PRODUCT_MASTER/PRODUCT_MASTER_DATA)&gt;0"> 
				<div class="paging-wrap">
					<xsl:value-of select="/XML/PAGER" disable-output-escaping="yes"/>
				</div>
				</xsl:if>
            </div>
            <xsl:if test="count(/XML/PRODUCT_MASTER/PRODUCT_MASTER_DATA)&gt;1">
				<a href="javascript:undefined;" class="compare spriteicon" onclick="_gaq.push(['_trackEvent', 'Compare Phones', 'clicked', 'Compare  Button']);compareIt();" id="compare-btn"></a>
			</xsl:if>
			</div>
            <div class="clear"></div>
            </section>
            <aside>


				<div class="RHSAd">
				<!--JavaScript Tag with group ID // Tag for network 1191: India.com // Website: Buying Guide // Page: Phone List // Placement: Buying_Guide_Phone_List_Right_Top_300x250 (3483230) // created at: Oct 31, 2011 8:34:33 AM-->
				<script language="javascript">
				<![CDATA[
				if (window.adgroupid == undefined) {
					window.adgroupid = Math.round(Math.random() * 1000);
				}
				document.write('<scr'+'ipt language="javascript1.1" src="http://jt.india.com/addyn/3.0/1191/3483230/0/170/ADTECH;cookie=info;loc=100;target=_blank;key=key1+key2+key3+key4;grp='+window.adgroupid+';misc='+new Date().getTime()+'"></scri'+'pt>');
				]]>
				</script><noscript><a href="http://jt.india.com/adlink/3.0/1191/3483230/0/170/ADTECH;cookie=info;loc=300;key=key1+key2+key3+key4" target="_blank"><img src="http://jt.india.com/adserv/3.0/1191/3483230/0/170/ADTECH;cookie=info;loc=300;key=key1+key2+key3+key4" border="0" width="300" height="250" /></a></noscript>
				<!-- End of JavaScript Tag -->
				</div>
				<div class="clear"></div>


			<!-- Top newsletter Widget start -->
			<xsl:call-template name="newsletterWidget"/>
			<!-- Top newsletter Widget end -->

            <div class="clear"></div>
              <!-- Top Rated Widget start -->
				<xsl:call-template name="topRatedWidget"/>
			  <!-- Top Rated Widget end -->
			<div class="clear"></div>
            </aside>
		<div class="clear"></div>
      </div>
    <!-- body end -->
    <div class="clear"></div>
    <!--prefooter ad module starts-->
    <div class="prefooter-ad">
		<!--JavaScript Tag with group ID // Tag for network 1191: India.com // Website: Buying Guide // Page: Phone List // Placement: Buying_Guide_Phone_List_Bottom_728x90 (3483231) // created at: Oct 31, 2011 8:34:34 AM-->
		<script language="javascript">
		<![CDATA[
		if (window.adgroupid == undefined) {
			window.adgroupid = Math.round(Math.random() * 1000);
		}
		document.write('<scr'+'ipt language="javascript1.1" src="http://jt.india.com/addyn/3.0/1191/3483231/0/225/ADTECH;cookie=info;loc=100;target=_blank;key=key1+key2+key3+key4;grp='+window.adgroupid+';misc='+new Date().getTime()+'"></scri'+'pt>');
		]]>
		</script><noscript><a href="http://jt.india.com/adlink/3.0/1191/3483231/0/225/ADTECH;cookie=info;loc=300;key=key1+key2+key3+key4" target="_blank"><img src="http://jt.india.com/adserv/3.0/1191/3483231/0/225/ADTECH;cookie=info;loc=300;key=key1+key2+key3+key4" border="0" width="728" height="90" /></a></noscript>
		<!-- End of JavaScript Tag -->
	</div>
    <!--prefooter ad module starts-->
    <div class="clear"></div>
    <!-- footer starts -->
          <xsl:call-template name="footerDiv"/>
          <!-- footer end -->
    <div class="clear"></div>
</div>

<div id="about_popup">
    <h1>Comapre</h1>
    <p id="op"></p>
</div>

<script>
var diff = 0 ;
<![CDATA[
var $scrollingDiv = $("#compare-btn");

var dist	= 210;
if(searchTab =='3')	dist -= 100; 
 
$(window).scroll(function(){            
    var y = $(this).scrollTop(),
        maxY = $('#pager_links').offset().top - dist,
        scrollHeight = $scrollingDiv.height();
	
	tVar	= $(window).scrollTop() - dist;

    if( y < maxY-scrollHeight ){
		t1Var	= parseInt($("#compare-btn").css('margin-top'));
			//alert($(window).scrollTop());
			if( $(window).scrollTop() <= 300 ) tVar = 5 ;
			$scrollingDiv
			.stop()
			.animate({"marginTop": ( tVar ) + "px"}, "slow" );        
    }    
});
]]>
</script>
<script src="{/XML/JS_URL}common.js?version={/XML/VERSION}"></script>
<script src="{/XML/JS_URL}compare.js?version={/XML/VERSION}"></script>
<script src="{/XML/JS_URL}jquery.cookie.js?version={/XML/VERSION}"></script>
<script>
<![CDATA[
//// compare popup 
	var marray	= init_array();	
	get_array(cookie_pid, marray);
	for (var i=1; i< next_entry(marray); i++) {
		pid = marray[i]; 
		$("#compare_product_"+pid+"_"+pid).attr('checked', true);
	}
	$(function() {
		if( marray.length > 1 ){
			showSelectedProducts();
		}
	});
]]>
</script>
<script type="text/javascript">
<![CDATA[ 
    //avoid conflict with other script
    //$j=jQuery.noConflict();
 
    $(document).ready(function($) {
 
	//this is the floating content
	var $floatingbox = $('#floating-box');
 
	if($('#body').length > 0){
 
	  var bodyY = parseInt($('#body').offset().top) - 20;
	  var originalX = $floatingbox.css('margin-left');
 
	  $(window).scroll(function () { 
 
	   var scrollY = $(window).scrollTop();
	   var isfixed = $floatingbox.css('position') == 'fixed';
 
	   if($floatingbox.length > 0){
 
	      //$floatingbox.html("srollY : " + scrollY + ", bodyY : " + bodyY + ", isfixed : " + isfixed);
 
	      if ( scrollY > bodyY && !isfixed ) {
			$floatingbox.stop().css({
			  position: 'fixed',
			  left: '51%',
			  top: 0,
			  marginLeft: -500
			});
		} else if ( scrollY < bodyY && isfixed ) {
		 	  $floatingbox.css({
			  position: 'relative',
			  left: 0,
			  top: 0,
			  marginLeft: originalX
		});
	     }		
	   }
       });
     }
  });
  ]]>
</script>

</body>
</html>
</xsl:template>
</xsl:stylesheet>
