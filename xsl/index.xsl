<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet  [
  <!ENTITY nbsp   "&#160;">
  <!ENTITY copy   "&#169;">
  <!ENTITY reg    "&#174;">
  <!ENTITY trade  "&#8482;">
  <!ENTITY mdash  "&#8212;">
  <!ENTITY ldquo  "&#8220;">
  <!ENTITY rdquo  "&#8221;">
  <!ENTITY pound  "&#163;">
  <!ENTITY yen    "&#165;">
  <!ENTITY euro   "&#8364;">
  <!ENTITY deg "&#176;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fb="http://www.facebook.com/2008/fbml" version="1.0">
  <xsl:output method="html" encoding="utf-8" indent="yes" doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"/>
  <xsl:include href="inc_header.xsl" />
  <xsl:include href="inc_footer.xsl" />
  <xsl:include href="inc_searchwidget.xsl" />
  <xsl:include href="inc_newsletter_widget.xsl" />
  <xsl:include href="inc_top_rated_widget.xsl" />
  <xsl:template match="/">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><xsl:value-of select="/XML/SEO_HOME_TITLE" /></title>
		  <meta name="Keywords" content="{/XML/SEO_HOME_KEWWORDS}" />
		  <meta name="Description" content="{/XML/SEO_HOME_META_DESC}" />
        <link href="{/XML/CSS_URL}style.css?version={/XML/VERSION}" rel="stylesheet" type="text/css" />
		<script type="text/javascript">
		// Google Analytics start
		  var _gaq = _gaq || [];
		  _gaq.push(['_setAccount', 'UA-27114870-1']);
		  _gaq.push(['_trackPageview']);

		  (function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();
		// Google Analytics end
		//<!-- Quantcast Tag part 1-->

		var _qevents = _qevents || [];

		(function() {
		var elem = document.createElement('script');
		elem.src = (document.location.protocol == "https:" ? "https://secure" : "http://edge") + ".quantserve.com/quant.js";
		elem.async = true;
		elem.type = "text/javascript";
		var scpt = document.getElementsByTagName('script')[0];
		scpt.parentNode.insertBefore(elem, scpt);
		})();

		_qevents.push({
		qacct:"p-31f3D02tYU8zY"
		});
		</script>
		<!-- Quantcast Tag part 1-->
      </head>
      <body>
<!-- Quantcast Tag part 2-->
<noscript>
<div style="display:none;">
<img src="//pixel.quantserve.com/pixel/p-31f3D02tYU8zY.gif" border="0" height="1" width="1" alt="Quantcast"/>
</div>
</noscript>
<!--Quantcast Tag part 2 -->
        <div id="wrapper">
          <!-- header start -->
          <xsl:call-template name="headDiv"/>
          <!-- header end -->
          <div class="clear"></div>
          <!-- body starts -->
          <div id="maincontent-wrapper">
            <section>
              <div class="phone-listing-wrap">
                <h2 class="spriteicon">Search Phone</h2>
				<!-- search widget start -->
					<xsl:call-template name="searchWidget"/>
				<!-- search widget end -->
              </div>
              <div class="clear"></div>

	      <div class="phone-showcase-wrap">
                <h2>Phones showcase</h2>

                <div class="phone-showcase-main">
                  <!--div class="showcasetabs-wrap">
                    <a href="javascript:void(0)" id="showcase1" class="selected">BGR Favorites</a>
                    <a href="javascript:void(0)" id="showcase2">New releases</a>
                    <a href="javascript:void(0)" id="showcase3">Featured</a>
                    <a href="javascript:void(0)" id="showcase4">Coming soon</a>

		    <div id="loader-div" class="loader-div hide"><img width="20" border="0" height="20" src="{/XML/IMAGE_URL}ajax-round-loader.gif" /></div>

                  </div-->
                  <div class="clear"></div>

                  <div class="showcasetabs-content" id="showcasecontent1">
			<xsl:for-each select="XML/BGR_FAVORITES/STORIES/STORY" >
			  <div class="single-phone-model">
				
				<a href="{LINKURL}" onclick="_gaq.push(['_trackEvent', 'Phone Showcase', 'clicked', 'Image Link']);" >
				<xsl:choose>
				<xsl:when test="IMAGE_PATH=''">
					<img src="/images/preset1.gif" alt="{PRODUCT_NAME}" title="{PRODUCT_NAME}" />
				</xsl:when>
				<xsl:otherwise>
					<img src="{IMAGE_PATH}" alt="{PRODUCT_NAME}" title="{PRODUCT_NAME}" />
				</xsl:otherwise>
				</xsl:choose>
				</a>
				
				<div class="phone-showcase-img-info">
				  <span><a href="{LINKURL}" onclick="_gaq.push(['_trackEvent', 'Phone Showcase', 'clicked', 'Phone Link']);" ><xsl:value-of select="PRODUCT_NAME" /></a></span>
						<xsl:choose>
						<xsl:when test="VARIANT_VALUE='' or VARIANT_VALUE=0" >
							<span class="noprice">Price: Not Available</span>
						</xsl:when>
						<xsl:otherwise>
							<span>Rs. <xsl:value-of select="VARIANT_VALUE" /></span>
						</xsl:otherwise>
						</xsl:choose>				  
				</div>
			  </div>
			  <xsl:if test="position()mod6=0">
			    <div class="clear"></div>
			  </xsl:if>
			</xsl:for-each>

			<div class="clear"></div>
			<div class="morelink">
			<!--a href="javascript:undefined;">More Phones...</a-->
			</div>
                  </div>

                  <div class="showcasetabs-content" id="showcasecontent2" style="display:none;">

		  </div>

		  <div class="showcasetabs-content" id="showcasecontent3" style="display:none;">

                  </div>

		  <div class="showcasetabs-content" id="showcasecontent4" style="display:none;">

		  </div>


		  <div class="clear"></div>
                </div>
                <div class="clear"></div>
              </div>
              <div class="clear"></div>
            </section>

			<aside>
				<div class="RHSAd">
				<!--JavaScript Tag with group ID // Tag for network 1191: India.com // Website: Buying Guide // Page: Home // Placement: Buying_Guide_Home_Right_Top_300x250 (3483226) // created at: Oct 31, 2011 8:34:33 AM-->
				<script language="javascript">
				<![CDATA[
				if (window.adgroupid == undefined) {
					window.adgroupid = Math.round(Math.random() * 1000);
				}
				document.write('<scr'+'ipt language="javascript1.1" src="http://jt.india.com/addyn/3.0/1191/3483226/0/170/ADTECH;cookie=info;loc=100;target=_blank;key=key1+key2+key3+key4;grp='+window.adgroupid+';misc='+new Date().getTime()+'"></scri'+'pt>');
				]]>
				</script><noscript><a href="http://jt.india.com/adlink/3.0/1191/3483226/0/170/ADTECH;cookie=info;loc=300;key=key1+key2+key3+key4" target="_blank"><img src="http://jt.india.com/adserv/3.0/1191/3483226/0/170/ADTECH;cookie=info;loc=300;key=key1+key2+key3+key4" border="0" width="300" height="250" /></a></noscript>
				<!-- End of JavaScript Tag -->
				</div>
				<div class="clear"></div>
              <!-- Top newsletter Widget start -->
				<xsl:call-template name="newsletterWidget"/>
			  <!-- Top newsletter Widget end -->
  
              <div class="clear"></div>
              <!-- Top Rated Widget start -->
				<xsl:call-template name="topRatedWidget"/>
			  <!-- Top Rated Widget end -->
              <div class="clear"></div>
            </aside>

            <div class="clear"></div>
          </div>
          <!-- body end -->
          <div class="clear"></div>
          <!--prefooter ad module starts-->
          <div class="prefooter-ad">
				<!--JavaScript Tag with group ID // Tag for network 1191: India.com // Website: Buying Guide // Page: Home // Placement: Buying_Guide_Home_Bottom_728x90 (3483225) // created at: Oct 31, 2011 8:34:33 AM-->
				<script language="javascript">
				<![CDATA[
				if (window.adgroupid == undefined) {
					window.adgroupid = Math.round(Math.random() * 1000);
				}
				document.write('<scr'+'ipt language="javascript1.1" src="http://jt.india.com/addyn/3.0/1191/3483225/0/225/ADTECH;cookie=info;loc=100;target=_blank;key=key1+key2+key3+key4;grp='+window.adgroupid+';misc='+new Date().getTime()+'"></scri'+'pt>');
				]]>
				</script><noscript><a href="http://jt.india.com/adlink/3.0/1191/3483225/0/225/ADTECH;cookie=info;loc=300;key=key1+key2+key3+key4" target="_blank"><img src="http://jt.india.com/adserv/3.0/1191/3483225/0/225/ADTECH;cookie=info;loc=300;key=key1+key2+key3+key4" border="0" width="728" height="90" /></a></noscript>
				<!-- End of JavaScript Tag -->        
          </div>
          <!--prefooter ad module starts-->
          <div class="clear"></div>
          <!-- footer starts -->
          <xsl:call-template name="footerDiv"/>
          <!-- footer end -->
          <div class="clear"></div>
        </div>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
