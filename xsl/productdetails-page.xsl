<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet  [
  <!ENTITY nbsp   "&#160;">
  <!ENTITY copy   "&#169;">
  <!ENTITY reg    "&#174;">
  <!ENTITY trade  "&#8482;">
  <!ENTITY mdash  "&#8212;">
  <!ENTITY ldquo  "&#8220;">
  <!ENTITY rdquo  "&#8221;">
  <!ENTITY pound  "&#163;">
  <!ENTITY yen    "&#165;">
  <!ENTITY euro   "&#8364;">
  <!ENTITY deg "&#176;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fb="http://www.facebook.com/2008/fbml" version="1.0">
  <xsl:output method="html" encoding="utf-8" indent="yes" doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"/>
  <xsl:include href="inc_header.xsl" />
  <xsl:include href="inc_footer.xsl" />
  <xsl:include href="inc_searchwidget.xsl" />
  <xsl:include href="inc_newsletter_widget.xsl" />
  <xsl:include href="inc_top_rated_widget.xsl" />
  <xsl:template match="/">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Buying Guides</title>
        <link href="{/XML/CSS_URL}style.css?version={/XML/VERSION}" rel="stylesheet" type="text/css" />
        <!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
        <!--<script type="text/javascript" src="https://getfirebug.com/firebug-lite.js"></script> -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
        <script src="{/XML/JS_URL}jquery.corners.js?version={/XML/VERSION}"></script>
        <!--script language="JavaScript" src="{/XML/JS_URL}jquery.min.js?version={/XML/VERSION}" type="text/javascript"></script-->
        <script language="JavaScript" src="{/XML/JS_URL}price-slider.js?version={/XML/VERSION}" type="text/javascript"></script>
        <script src="{/XML/JS_URL}jquery.ui.widget.js?version={/XML/VERSION}"></script>
        <script src="{/XML/JS_URL}jquery.ui.mouse.js?version={/XML/VERSION}"></script>

        <script>
          var selectedTab = '<xsl:value-of select="XML/SELECTEDTABID" disable-output-esacaping="yes"/>';
          <xsl:text disable-output-escaping="yes">
            <![CDATA[

		// iepp v2.1pre @jon_neal & @aFarkas github.com/aFarkas/iepp
// html5shiv @rem remysharp.com/html5-enabling-script
// Dual licensed under the MIT or GPL Version 2 licenses
/*@cc_on(function(a,b){function r(a){var b=-1;while(++b<f)a.createElement(e[b])}if(!window.attachEvent||!b.createStyleSheet||!function(){var a=document.createElement("div");return a.innerHTML="<elem></elem>",a.childNodes.length!==1}())return;a.iepp=a.iepp||{};var c=a.iepp,d=c.html5elements||"abbr|article|aside|audio|canvas|datalist|details|figcaption|figure|footer|header|hgroup|mark|meter|nav|output|progress|section|subline|summary|time|video",e=d.split("|"),f=e.length,g=new RegExp("(^|\\s)("+d+")","gi"),h=new RegExp("<(/*)("+d+")","gi"),i=/^\s*[\{\}]\s*$/,j=new RegExp("(^|[^\\n]*?\\s)("+d+")([^\\n]*)({[\\n\\w\\W]*?})","gi"),k=b.createDocumentFragment(),l=b.documentElement,m=b.getElementsByTagName("script")[0].parentNode,n=b.createElement("body"),o=b.createElement("style"),p=/print|all/,q;c.getCSS=function(a,b){try{if(a+""===undefined)return""}catch(d){return""}var e=-1,f=a.length,g,h=[];while(++e<f){g=a[e];if(g.disabled)continue;b=g.media||b,p.test(b)&&h.push(c.getCSS(g.imports,b),g.cssText),b="all"}return h.join("")},c.parseCSS=function(a){var b=[],c;while((c=j.exec(a))!=null)b.push(((i.exec(c[1])?"\n":c[1])+c[2]+c[3]).replace(g,"$1.iepp-$2")+c[4]);return b.join("\n")},c.writeHTML=function(){var a=-1;q=q||b.body;while(++a<f){var c=b.getElementsByTagName(e[a]),d=c.length,g=-1;while(++g<d)c[g].className.indexOf("iepp-")<0&&(c[g].className+=" iepp-"+e[a])}k.appendChild(q),l.appendChild(n),n.className=q.className,n.id=q.id,n.innerHTML=q.innerHTML.replace(h,"<$1font")},c._beforePrint=function(){if(c.disablePP)return;o.styleSheet.cssText=c.parseCSS(c.getCSS(b.styleSheets,"all")),c.writeHTML()},c.restoreHTML=function(){if(c.disablePP)return;n.swapNode(q)},c._afterPrint=function(){c.restoreHTML(),o.styleSheet.cssText=""},r(b),r(k);if(c.disablePP)return;m.insertBefore(o,m.firstChild),o.media="print",o.className="iepp-printshim",a.attachEvent("onbeforeprint",c._beforePrint),a.attachEvent("onafterprint",c._afterPrint)})(this,document)@*/



	$(function(){
	$("javascript:undefined;searchtab1,javascript:undefined;searchtab2,javascript:undefined;searchtab3,javascript:undefined;searchtab4").click(function() {
	 $(".tab a").removeClass("active");
     $(this).addClass("active");
	 });

	$("javascript:undefined;lang1,javascript:undefined;lang2,javascript:undefined;lang3,javascript:undefined;lang4").click(function() {
	 $(".header-right-wrap a").removeClass("active");
     $(this).addClass("active");
	 });

	$("javascript:undefined;showcase1,javascript:undefined;showcase2,javascript:undefined;showcase3,javascript:undefined;showcase4").click(function() {
	 $(".showcasetabs-wrap a").removeClass("selected");
     $(this).addClass("selected");
	 });

	$("javascript:undefined;showcase1").click(function() {
	 $("javascript:undefined;showcasecontent1").show();
	 $("javascript:undefined;showcasecontent2,javascript:undefined;showcasecontent3,javascript:undefined;showcasecontent4").hide();
	 });
	$("javascript:undefined;showcase2").click(function() {
	 $("javascript:undefined;showcasecontent2").show();
	 $("javascript:undefined;showcasecontent1,javascript:undefined;showcasecontent3,javascript:undefined;showcasecontent4").hide();
	 });
	 $("javascript:undefined;showcase3").click(function() {
	 $("javascript:undefined;showcasecontent3").show();
	 $("javascript:undefined;showcasecontent1,javascript:undefined;showcasecontent2,javascript:undefined;showcasecontent4").hide();
	 });
	 $("javascript:undefined;showcase4").click(function() {
	 $("javascript:undefined;showcasecontent4").show();
	 $("javascript:undefined;showcasecontent1,javascript:undefined;showcasecontent2,javascript:undefined;showcasecontent3").hide();
	 });

  $("javascript:undefined;topRated").click(function() {
     $("javascript:undefined;topRatedContent").show();
     $("javascript:undefined;topSellContent").hide();
	 $("javascript:undefined;topSell").removeClass("active");
	 $(this).addClass("active");
     });
    $("javascript:undefined;topSell").click(function() {
     $("javascript:undefined;topSellContent").show();
     $("javascript:undefined;topRatedContent").hide();
	 $("javascript:undefined;topRated").removeClass("active");
	 $(this).addClass("active");
     });


   });


	$(function() {
		$( "javascript:undefined;slider-range" ).slider({
			range: true,
			min: 0,
			max: 500,
			values: [ 20, 475 ],
			slide: function( event, ui ) {
				$( "javascript:undefined;amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
			}
		});
		$( "javascript:undefined;amount" ).val( "$" + $( "javascript:undefined;slider-range" ).slider( "values", 0 ) +
			" - $" + $( "javascript:undefined;slider-range" ).slider( "values", 1 ) );
	});
]]>
          </xsl:text>
        </script>
      </head>

      <body>
        <div id="wrapper">
          <!-- header start -->
          <xsl:call-template name="headDiv"/>
          <!-- header end -->
          <div class="clear"></div>
          <!-- body starts -->
          <div id="maincontent-wrapper">
            <section>
              <div class="model-details">
                <h1>
                  <xsl:value-of select="XML/PRODUCT_DETAIL/PRODUCT_DETAIL_DATA/DISPLAY_PRODUCT_NAME"/>
                </h1>
                <div class="model-overview">
                  <img src="{XML/PRODUCT_DETAIL/PRODUCT_DETAIL_DATA/IMAGE_PATH}" border="0" />
                  <div class="left">
                    <h3>
                      <xsl:value-of select="XML/PRODUCT_DETAIL/PRODUCT_DETAIL_DATA/DISPLAY_PRODUCT_NAME"/>
                    </h3>
                    <span>
                      <xsl:value-of select="XML/PRODUCT_DETAIL/PRODUCT_DETAIL_DATA/PRODUCT_DESC" disable-output-escaping="yes" />
                    </span>
                  </div>
                  <!--div class="middle">
                    <h3>
                      <span class="icon spriteicon"></span>
                      <span>
                        <a href="javascript:undefined;">Do people like this?</a>
                      </span>
                    </h3>
                    <p>User Ratings: 9.5/10</p>
                    <p class="bgrratings spriteicon">Ratings
                      <a href="javascript:undefined;" class="spriteicon ratings"></a>
                    </p>
                  </div-->
                  <div class="right">
                    <h3>Price:
                      <xsl:value-of select="XML/PRODUCT_DETAIL/PRODUCT_DETAIL_DATA/EXSHOWROOMPRICE"/>
                    </h3>
                    <!--a href="javascript:undefined;" class="gotodemobtn spriteicon"></a>
                    <a href="javascript:undefined;" class="buybtn spriteicon"></a-->
                  </div>
                  <div></div>
                  <div class="clear"></div>
                </div>
                <div class="features-main-wrap">
                  <div class="tabs-mainwrap">
                    <ul>
                      <xsl:for-each select="/XML/GROUP_MASTER/GROUP_MASTER_DATA">
                      <li>
                        <xsl:if test="GROUP_ID=1">
                        <a href="{SEO_URL}" id="featuredtab1">Functionality</a>
                        </xsl:if>
                      </li>
                      <li>
                          <xsl:if test="GROUP_ID=3">
                            <a href="{SEO_URL}" id="featuredtab3">Tech Specs</a>
                          </xsl:if>
                      </li>
                       </xsl:for-each>
                      <li>
                        <a href="{/XML/USERREVIEW_SEO_URL}" id="featuredtab2">Ratings &amp; Reviews</a>
                      </li>
                      <li>
                        <a href="{/XML/SEO_PHOTO_TAB_URL}" id="featuredtab4">Photos &amp; Videos</a>
                      </li>
                      <!--li>
                        <a href="javascript:void(0);" id="featuredtab5">360 &deg;</a>
                      </li-->
                    </ul>
                  </div>
                  <xsl:variable name="styletab1"><xsl:choose><xsl:when test="/XML/SELECTEDTABID=1 or /XML/SELECTEDTABID=''">display:block;</xsl:when><xsl:otherwise>display:none;</xsl:otherwise></xsl:choose></xsl:variable>
                  <div class="tab-content-wrap" id="featuredcontent1" style="{$styletab1}">
                    <div class="model-features">
                      <h3>
                        <xsl:value-of select="XML/PRODUCT_DETAIL/PRODUCT_DETAIL_DATA/DISPLAY_PRODUCT_NAME"/>
Features</h3>
                      <ul>
                        <xsl:for-each select="/XML/OVERVIEW/FEATURE_SPEC_SHORT_DESC/FEATURE_SPEC_DATA">
                          <li>
                            <p class="label">
                              <xsl:value-of select="FEATURE_TITLE" disable-output-escaping="yes"/>
                            </p>
                            <p>
                              <xsl:value-of select="FEATURE_VALUE" disable-output-escaping="yes"/>
                            </p>
                          </li>
                        </xsl:for-each>
                      </ul>
                    </div>
                    <div class="imp-stats">
                      <h3>Important Stats</h3>
                      <xsl:for-each select="/XML/OVERVIEW/TECH_SPEC_SHORT_DESC/TECH_SPEC_DATA">
                        <div class="single-specs">
                          <img src="{/XML/IMAGE_URL}{FEATURE_IMG_PATH}" border="0"/>
                          <div>
                            <h3>
                              <xsl:value-of select="FEATURE_TITLE" disable-output-escaping="yes"/>
                            </h3>
                            <xsl:value-of select="FEATURE_VALUE" disable-output-escaping="yes"/>
                          </div>
                        </div>
                      </xsl:for-each>
                    </div>
                    <div class="clear"></div>
                  </div>
                  <xsl:variable name="styletab2"><xsl:choose><xsl:when test="/XML/SELECTEDTABID=3">display:block;</xsl:when><xsl:otherwise>display:none;</xsl:otherwise></xsl:choose></xsl:variable>
                  <div class="tab-content-wrap" id="featuredcontent3" style="{$styletab2}">
                    <table width="100%" border="1" cellspacing="0" cellpadding="0">
                      <xsl:for-each select="/XML/GROUP_MASTER/GROUP_MASTER_DATA">
                        <xsl:if test="GROUP_ID=3">
                        <xsl:for-each select="SUB_GROUP_MASTER">
                          <xsl:if test="PIVOT_FEATURE_ID!=FEATURE_ID">
                            <tr class="titlebgray-bg">
                              <td class="firstcol">
                                <xsl:value-of select="SUB_GROUP_NAME" disable-output-escaping="yes"/>
                              </td>
                              <td>&nbsp;</td>
                            </tr>
                            <xsl:for-each select="SUB_GROUP_MASTER_DATA">
                              <tr>
                                <td class="firstcol">
                                  <xsl:value-of select="FEATURE_NAME" disable-output-esacaping="yes"/>
                                </td>
                                  <td>
                                    <xsl:choose>
                                      <xsl:when test="FEATURE_VALUE='yes'">
                                        <img src="{/XML/FEATURE_YES_IMAGE_URL}" alt="{FEATURE_VALUE}" title="{FEATURE_VALUE}"/>
                                      </xsl:when>
                                      <xsl:otherwise>
                                        <xsl:choose>
                                          <xsl:when test="FEATURE_VALUE='no'">
                                            <img src="{/XML/FEATURE_NO_IMAGE_URL}" alt="{FEATURE_VALUE}" title="{FEATURE_VALUE}"/>
                                          </xsl:when>
                                          <xsl:otherwise>
                                            <xsl:value-of select="FEATURE_VALUE" disable-output-esacaping="yes"/>
                                          </xsl:otherwise>
                                        </xsl:choose>
                                      </xsl:otherwise>
                                    </xsl:choose>
                                  </td>
                              </tr>
                            </xsl:for-each>
                          </xsl:if>
                        </xsl:for-each>
                        </xsl:if>
                      </xsl:for-each>
                    </table>
                    <div class="clear"></div>
                  </div>
				  <xsl:variable name="styletab3"><xsl:choose><xsl:when test="/XML/SELECTEDTABID=2">display:block;</xsl:when><xsl:otherwise>display:none;</xsl:otherwise></xsl:choose></xsl:variable>
                  <div class="tab-content-wrap" id="featuredcontent2" style="{$styletab3}">
						<table border="1">
  <tr>
    <td>&nbsp;</td>
    <td><a href="{/XML/WEB_URL}write_user_review.php?pid={/XML/SELECTED_PRODUCT_ID}&amp;bid={/XML/SELECTED_BRAND_ID}">Write review</a></td>
  </tr>
  <xsl:for-each select="/XML/USER_REVIEW_MASTER/USER_REVIEW_MASTER_DATA">
  <tr>
  <td colspan="2">My experience with my <xsl:value-of select="BRAND_MODEL_VARIANT_NAME" disable-output-escaping="yes"/></td>
  </tr>
  <tr>
    <td colspan="2">By <xsl:value-of select="USER_NAME" disable-output-escaping="yes"/>| Posted on <xsl:value-of select="CREATE_DATE" disable-output-escaping="yes"/></td>
  </tr>
  <tr>
    <td colspan="2">
	<xsl:for-each select="/XML/USER_REVIEW_ANSWER_MASTER/USER_REVIEW_ANSWER_MASTER_DATA">
		<xsl:if test="position()=1"><xsl:value-of select="QUENAME" disable-output-escaping="yes"/></xsl:if>
	</xsl:for-each>
	</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><a href="{/XML/WEB_URL}view_user_full_review.php?user_review_id={USER_REVIEW_ID}&amp;pid={PRODUCT_ID}">Read more&gt;&gt;</a></td>
  </tr>
    </xsl:for-each>
</table>
                  </div>
                  <div class="tab-content-wrap" id="featuredcontent4" style="display:none;">
                    <div class="model-features">
                      <h3>Nokia N9 Features</h3>
                      <ul>
                        <li>
                          <p class="label">Browser</p>
                          <p>WAP 2.0/xHTML, HTML, RSS feeds</p>
                        </li>
                        <li>
                          <p class="label">Radio</p>
                          <p>No</p>
                        </li>
                        <li>
                          <p class="label">Games</p>
                          <p>Angry Birds Magic (NFC), Galaxy on Fire 2, Real Golf 2011; downloadable</p>
                        </li>
                      </ul>
                    </div>
                    <div class="imp-stats">
                      <h3>Important Stats</h3>
                      <div class="single-specs">
                        <img src="{/XML/IMAGE_URL}icon-battery.gif"  border="0"/>
                        <div>
                          <h3>Battery Life</h3>
Up to 380 h (2G) / Up to 450 h (3G)</div>
                      </div>
                      <div class="single-specs">
                        <img src="{/XML/IMAGE_URL}icon-display.gif"  border="0"/>
                        <div>
                          <h3>Display</h3>
AMOLED capacitive touchscreen, 16M colors</div>
                      </div>
                      <div class="single-specs">
                        <img src="{/XML/IMAGE_URL}icon-size.gif"  border="0"/>
                        <div>
                          <h3>Size</h3>
116.5 x 61.2 x 12.1 mm, 76 cc</div>
                      </div>
                      <div class="single-specs">
                        <img src="{/XML/IMAGE_URL}icon-networkcapability.gif"  border="0"/>
                        <div>
                          <h3>Network Capability</h3>
GSM 3G</div>
                      </div>
                      <div class="single-specs">
                        <img src="{/XML/IMAGE_URL}icon-inputmechanism.gif"  border="0"/>
                        <div>
                          <h3>Input Mechanism</h3>
Multi touch</div>
                      </div>
                      <div class="single-specs">
                        <img src="{/XML/IMAGE_URL}icon-processor.gif"  border="0"/>
                        <div>
                          <h3>Processor</h3>
1GHz Cortex A8 CPU, PowerVR SG X530 GPU, TI OMAP 3630 chipset</div>
                      </div>
                      <div class="single-specs">
                        <img src="{/XML/IMAGE_URL}icon-camera.gif"  border="0"/>
                        <div>
                          <h3>Camera</h3>
8 MP, 3264x2448 pixels, Carl Zeiss optics, autofocus, dual LED flash</div>
                      </div>
                    </div>
                    <div class="clear"></div>
                  </div>
                  <div class="tab-content-wrap" id="featuredcontent5" style="display:none;">
                    <div class="model-features">
                      <h3>Nokia N9 Features</h3>
                      <ul>
                        <li>
                          <p class="label">OS</p>
                          <p>MeeGo OS, v1.2 Harmattan</p>
                        </li>
                        <li>
                          <p class="label">CPU</p>
                          <p>1GHz Cortex A8 CPU, PowerVR SGX530 GPU, TI OMAP 3630 chipset</p>
                        </li>
                        <li>
                          <p class="label">Radio</p>
                          <p>No</p>
                        </li>
                        <li>
                          <p class="label">Games</p>
                          <p>Angry Birds Magic (NFC), Galaxy on Fire 2, Real Golf 2011; downloadable</p>
                        </li>
                      </ul>
                    </div>
                    <div class="imp-stats">
                      <h3>Important Stats</h3>
                      <div class="single-specs">
                        <img src="{/XML/IMAGE_URL}icon-battery.gif"  border="0"/>
                        <div>
                          <h3>Battery Life</h3>
Up to 380 h (2G) / Up to 450 h (3G)</div>
                      </div>
                      <div class="single-specs">
                        <img src="{/XML/IMAGE_URL}icon-display.gif"  border="0"/>
                        <div>
                          <h3>Display</h3>
AMOLED capacitive touchscreen, 16M colors</div>
                      </div>
                      <div class="single-specs">
                        <img src="{/XML/IMAGE_URL}icon-size.gif"  border="0"/>
                        <div>
                          <h3>Size</h3>
116.5 x 61.2 x 12.1 mm, 76 cc</div>
                      </div>
                      <div class="single-specs">
                        <img src="{/XML/IMAGE_URL}icon-networkcapability.gif"  border="0"/>
                        <div>
                          <h3>Network Capability</h3>
GSM 3G</div>
                      </div>
                      <div class="single-specs">
                        <img src="{/XML/IMAGE_URL}icon-inputmechanism.gif"  border="0"/>
                        <div>
                          <h3>Input Mechanism</h3>
Multi touch</div>
                      </div>
                      <div class="single-specs">
                        <img src="{/XML/IMAGE_URL}icon-processor.gif"  border="0"/>
                        <div>
                          <h3>Processor</h3>
1GHz Cortex A8 CPU, PowerVR SG X530 GPU, TI OMAP 3630 chipset</div>
                      </div>
                      <div class="single-specs">
                        <img src="{/XML/IMAGE_URL}icon-camera.gif"  border="0"/>
                        <div>
                          <h3>Camera</h3>
8 MP, 3264x2448 pixels, Carl Zeiss optics, autofocus, dual LED flash</div>
                      </div>
                    </div>
                    <div class="clear"></div>
                  </div>
                </div>
              </div>
              <input type="hidden" id="disp_container" value="1" />
              <div class="clear"></div>
            </section>
			
			<aside>
              <!-- Top newsletter Widget start -->
				<xsl:call-template name="newsletterWidget"/>
			  <!-- Top newsletter Widget end -->
  
              <div class="clear"></div>
              <!-- Top Rated Widget start -->
				<xsl:call-template name="topRatedWidget"/>
			  <!-- Top Rated Widget end -->
              <div class="clear"></div>
            </aside>

            <div class="clear"></div>
          </div>
          <!-- body end -->
          <div class="clear"></div>
          <!--prefooter ad module starts-->
          <div class="prefooter-ad">
            <img src="{/XML/IMAGE_URL}temp-botmad.gif" border="0" />
          </div>
          <!--prefooter ad module starts-->
          <div class="clear"></div>
          <!-- footer starts -->
          <xsl:call-template name="footerDiv"/>
          <!-- footer end -->
          <div class="clear"></div>
        </div>
        <script>
          <xsl:text disable-output-escaping="yes">
            <![CDATA[
              if(selectedTab=='' || selectedTab == 'undefined'){
                selectedTab=1;
              }
              $('#featuredtab'+selectedTab).addClass("active");
            ]]>
          </xsl:text>
        </script>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
