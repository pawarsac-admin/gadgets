<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet  [
  <!ENTITY nbsp   "&#160;">
  <!ENTITY copy   "&#169;">
  <!ENTITY reg    "&#174;">
  <!ENTITY trade  "&#8482;">
  <!ENTITY mdash  "&#8212;">
  <!ENTITY ldquo  "&#8220;">
  <!ENTITY rdquo  "&#8221;">
  <!ENTITY pound  "&#163;">
  <!ENTITY yen    "&#165;">
  <!ENTITY euro   "&#8364;">
  <!ENTITY deg "&#176;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fb="http://www.facebook.com/2008/fbml" version="1.0">
  <xsl:output method="html" encoding="utf-8" indent="yes" doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"/>
  <xsl:include href="inc_header.xsl" />
  <xsl:include href="inc_footer.xsl" />
  <xsl:template match="/">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><xsl:value-of select="/XML/SEO_PHONE_FINDER_PAGE_TITLE" /></title>
		  <meta name="Keywords" content="{/XML/SEO_PHONE_FINDER_PAGE_KEWWORDS}" />
		  <meta name="Description" content="{/XML/SEO_PHONE_FINDER_PAGE_META_DESC}" />
		 <link href="{/XML/CSS_URL}style.css?version={/XML/VERSION}" rel="stylesheet" type="text/css" />
      </head>

      <body>
        <div id="wrapper">
          <!-- header start -->
          <xsl:call-template name="headDiv"/>
          <!-- header end -->
          <div class="clear"></div>
          <!-- body starts -->
          <div id="maincontent-wrapper">
 
			<section>
				<div class="page404">
					<h3>Page Not Found</h3>
						<p>The page you requested cannot be found. The page you are looking for might have been removed, had its name changed, or is temporarily unavailable.</p>
						<p>
						Please try the following:
						<ul>
							<li>If you typed the page address in the Address bar, make sure that it is spelled correctly.</li>
							<li>Open the <a href="{/XML/SEO_WEB_URL}" class="b"><xsl:value-of select="/XML/SEO_WEB_URL" /></a> home page and look for links to the information you want.</li>
							<li>Use the navigation bar to find the link you are looking for.</li>
							<!--li class="sf">Click the Back button to try another link.</li-->
						</ul>
						</p>
				</div>  
            </section>

            <div class="clear"></div>
          </div>
          <!-- body end -->
          <div class="clear"></div>
          <!--prefooter ad module starts-->
          <div class="prefooter-ad">
            <img src="{/XML/IMAGE_URL}temp-botmad.gif" border="0" />
          </div>
          <!--prefooter ad module starts-->
          <div class="clear"></div>
          <!-- footer starts -->
          <xsl:call-template name="footerDiv"/>
          <!-- footer end -->
          <div class="clear"></div>
        </div>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>