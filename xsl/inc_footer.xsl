<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet  [
  <!ENTITY nbsp   "&#160;">
  <!ENTITY copy   "&#169;">
  <!ENTITY reg    "&#174;">
  <!ENTITY trade  "&#8482;">
  <!ENTITY mdash  "&#8212;">
  <!ENTITY ldquo  "&#8220;">
  <!ENTITY rdquo  "&#8221;"> 
  <!ENTITY pound  "&#163;">
  <!ENTITY yen    "&#165;">
  <!ENTITY euro   "&#8364;">
  <!ENTITY deg "&#176;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fb="http://www.facebook.com/2008/fbml" version="1.0">
  <xsl:output method="html" encoding="utf-8" indent="yes" doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"/>
  <xsl:template name="footerDiv">
    <!-- footer starts -->
    <div id="footer-outer">
      <footer>
	   <span style="color:#aaa;">* Prices mentioned in this buying guide are indicative and have been sourced from multiple sources. Buyers are advised to check with their local retailer as prices are bound to vary between dealers and cities. We are not responsible for variations of any kind. Buyers are also advised to cross-check the specifications of products mentioned here.</span>
		<br/><br/> 
        © 2011 India.com. All Rights Reserved.
        <div class="clear"></div>
        <!--a href="javascript:undefined;">About Us</a>
        <a href="javascript:undefined;">Disclaimer</a>
        <a href="javascript:undefined;">Privacy Policy</a>
        <a href="javascript:undefined;">Customer Service</a>
        <a href="javascript:undefined;">Feedback</a>
        <a href="javascript:undefined;">Media</a>
        <a href="javascript:undefined;">Advertise With Us</a>
        <a href="javascript:undefined;">Reviews</a>
        <a href="javascript:undefined;" class="last">Contact Us</a-->
        <div class="clear"></div>
      </footer>
      <div class="clear"></div>
    </div>
    <!-- footer end -->
  </xsl:template>
</xsl:stylesheet>

