<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet  [
  <!ENTITY nbsp   "&#160;">
  <!ENTITY copy   "&#169;">
  <!ENTITY reg    "&#174;">
  <!ENTITY trade  "&#8482;">
  <!ENTITY mdash  "&#8212;">
  <!ENTITY ldquo  "&#8220;">
  <!ENTITY rdquo  "&#8221;">
  <!ENTITY pound  "&#163;">
  <!ENTITY yen    "&#165;">
  <!ENTITY euro   "&#8364;">
  <!ENTITY deg "&#176;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fb="http://www.facebook.com/2008/fbml" version="1.0">
  <xsl:output method="html" encoding="utf-8" indent="yes" doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"/>
  <xsl:include href="inc_header.xsl" />
  <xsl:include href="inc_footer.xsl" />
  <xsl:include href="inc_newsletter_widget.xsl" />
  <xsl:include href="inc_top_rated_widget.xsl" />
  <xsl:template match="/">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><xsl:value-of select="/XML/SEO_PHONE_FINDER_PAGE_TITLE" /></title>
		  <meta name="Keywords" content="{/XML/SEO_PHONE_FINDER_PAGE_KEWWORDS}" />
		  <meta name="Description" content="{/XML/SEO_PHONE_FINDER_PAGE_META_DESC}" />
       <link href="{/XML/CSS_URL}style.css?version={/XML/VERSION}" rel="stylesheet" type="text/css" />
		<script type="text/javascript">
		// Google Analytics start
		  var _gaq = _gaq || [];
		  _gaq.push(['_setAccount', 'UA-27114870-1']);
		  _gaq.push(['_trackPageview']);

		  (function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();
		// Google Analytics end
		//<!-- Quantcast Tag part 1-->

		var _qevents = _qevents || [];

		(function() {
		var elem = document.createElement('script');
		elem.src = (document.location.protocol == "https:" ? "https://secure" : "http://edge") + ".quantserve.com/quant.js";
		elem.async = true;
		elem.type = "text/javascript";
		var scpt = document.getElementsByTagName('script')[0];
		scpt.parentNode.insertBefore(elem, scpt);
		})();

		_qevents.push({
		qacct:"p-31f3D02tYU8zY"
		});
		</script>
		<!-- Quantcast Tag part 1-->
	  </head>
      <body>
<!-- Quantcast Tag part 2-->
<noscript>
<div style="display:none;">
<img src="//pixel.quantserve.com/pixel/p-31f3D02tYU8zY.gif" border="0" height="1" width="1" alt="Quantcast"/>
</div>
</noscript>
<!--Quantcast Tag part 2 -->
        <div id="wrapper">
          <!-- header start -->
          <xsl:call-template name="headDiv"/>
          <!-- header end -->
          <div class="clear"></div>
          <!-- body starts -->
          <div id="maincontent-wrapper">
            <section>
           <h1 class="page-heading"><a href="{/XML/WEB_URL}" class="home spriteicon"></a> <span> <a href="{XML/WEB_URL}">Home</a> / <a href="#">Phone Finder</a></span></h1>

              <div class="clear"></div>
            <div class="phone-listing-wrap">
                <div class="phone-listing-main">
                	<div class="heading">
					Phone Finder
                    </div>
					<form method="post" action="{/XML/SEO_PHONE_FINDER_RESULT_URL}" name="researchfrm" id="researchfrm">
					<input type="hidden" name="pfp" value="1"/>
					<input type="hidden" name="sortproduct" id="sortproduct"/>
					<input type="hidden" name="catid" id="catid" value="{/XML/SITE_CATEGORY_ID}"/>
					<input type="hidden" name="{/XML/PAGE_OFFSET}" id="pageoffset" value=""/>
					<input type="hidden" id="mxprice" name="mxprice" value="{/XML/MAX_PRICE}" />
					<input type="hidden" id="mnprice" name="mnprice" value="{/XML/MIN_PRICE}" />
					<input type="hidden" id="pageoffset" name="pageoffset" value="1" />

                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
						  <tr class="alt">
                            <td colspan="5" width="380">
                            <input name="networkTypeAll" type="checkbox" value="0" checked="checked" id='selectall1' class="case1" /><label class="sub-options">All</label>
                            

							<input name="networkType" type="checkbox" value="GSM" class="case1" onclick="_gaq.push(['_trackEvent', 'Phone Finder', 'clicked', 'Technology Selection']);" /><label class="sub-options">GSM</label>
                            <input name="networkType" type="checkbox" value="CDMA"  class="case1" onclick="_gaq.push(['_trackEvent', 'Phone Finder', 'clicked', 'Technology Selection']);" /><label class="sub-options">CDMA</label>


                            </td><td colspan="5">
                            <!--label>By device type</label><span><select name="" class="w250"><option>All Phones</option></select></span--></td>
                          </tr>

						  <tr><td colspan="10"> 
						  <label>Brand - </label>
						  <input name="brandAll" type="checkbox" value="" checked="checked" id='selectall2' class="case2" onclick="_gaq.push(['_trackEvent', 'Phone Finder', 'clicked', 'Brand Selection']);" /><label class="sub-options">All</label>
						  <xsl:for-each select="/XML/BRAND_MASTER/BRAND_MASTER_DATA">
						  <xsl:if test="position()mod7=0"><br/>&nbsp;&nbsp;&nbsp;&nbsp;</xsl:if>
                            <input type="checkbox" value="{BRAND_NAME}" name="branddetails" id="checkbox_brand_id_{BRAND_ID}" class="case2" onclick="_gaq.push(['_trackEvent', 'Phone Finder', 'clicked', 'Brand Selection']);" /><label class="sub-options"><xsl:value-of select="BRAND_NAME" disable-output-escaping="yes"/></label>
	                      </xsl:for-each></td>
						  </tr>

						<tr class="alt"><td colspan="10"> 
						  <label>Phone Type - </label>
							<input name="phoneTypeAll" type="checkbox" value="0" checked="checked" id='selectall3'/><label class="sub-options">All</label>
                            
							<input name="phoneType" type="checkbox" value="Tablet" class="case3" onclick="_gaq.push(['_trackEvent', 'Phone Finder', 'clicked', 'Phone Type']);" /><label class="sub-options">Tablet</label>
							<input name="phoneType" type="checkbox" value="Smart_Phone" class="case3" onclick="_gaq.push(['_trackEvent', 'Phone Finder', 'clicked', 'Phone Type']);" /><label class="sub-options">Smart Phone</label>
                            <input name="phoneType" type="checkbox" value="Feature_Phone" class="case3" onclick="_gaq.push(['_trackEvent', 'Phone Finder', 'clicked', 'Phone Type']);" /><label class="sub-options">Feature Phone</label>
							<input name="phoneType" type="checkbox" value="Basic_Phone" class="case3" onclick="_gaq.push(['_trackEvent', 'Phone Finder', 'clicked', 'Phone Type']);" /><label class="sub-options">Basic Phone</label>
							</td>
						  </tr>

						   <tr>
                            <td colspan="2">
                            <label>GPS - </label>
                            <input name="GPS" type="radio" value="Yes" onclick="_gaq.push(['_trackEvent', 'Phone Finder', 'clicked', 'GPS']);"  /><label class="sub-options">Yes</label>
                            <input name="GPS" type="radio" value="No" onclick="_gaq.push(['_trackEvent', 'Phone Finder', 'clicked', 'GPS']);"  /><label class="sub-options">No</label>
                            </td>

							<td colspan="2">
                            <label>Wi-Fi - </label>
                            <input name="WIFI" type="radio" value="Yes" onclick="_gaq.push(['_trackEvent', 'Phone Finder', 'clicked', 'WI-FI']);"  /><label class="sub-options">Yes</label>
                            <input name="WIFI" type="radio" value="No" onclick="_gaq.push(['_trackEvent', 'Phone Finder', 'clicked', 'WI-FI']);"  /><label class="sub-options">No</label>
                            </td>

                            <td colspan="3">
                            <label>Dual Sim - </label>
                            <input name="dualSIM" type="radio" value="Dual" onclick="_gaq.push(['_trackEvent', 'Phone Finder', 'clicked', 'Dual Sim']);"  /><label class="sub-options">yes</label>
                            <input name="dualSIM" type="radio" value="Single" onclick="_gaq.push(['_trackEvent', 'Phone Finder', 'clicked', 'Dual Sim']);"  /><label class="sub-options">no</label>
                            </td>

                          </tr>
                          <tr class="alt">
                            <td colspan="3">
                            <label>Camera Resolution - </label>
                            <select name="camResolution" id="camResolution" onclick="_gaq.push(['_trackEvent', 'Phone Finder', 'clicked', 'Camera Resolution']);">
								<option value="0">- Select -</option>
								<option value="No"> No Camera </option>
								<option value="0.1-1.9MP">Less than 2 MP</option>
								<option value="2MP">2 MP</option>
								<option value="3-4MP">3 MP</option>
								<option value="5MP">5 MP</option>
								<option value="8MP">8 MP</option>
								<option value="12MP"> 12MP</option>
							</select>
                            </td>

							<td colspan="7">
                            <label>Operating System - </label>
                            <select name="os" id="os" onclick="_gaq.push(['_trackEvent', 'Phone Finder', 'clicked', 'Operating System']);">
								<option value="0">- Select -</option>
								<option value="Android">Android</option>
								<option value="iOS">iOS</option>
								<option value="Symbian">Symbian</option>
								<option value="Windows_Phone">Windows Phone</option>
								<option value="Blackberry">Blackberry</option>
								<option value="Bada">Bada</option>
								<option value="Others">Others</option>
							</select>
                            </td>

                          </tr>

                          <tr>
                            <td colspan="10">
                            <div class="filter-lhs" style="display:inline-block; width:320px; background:#fff; height:40px; float:left;">
                        	<p><label>By price</label>
                            <!--slider starts-->
                            <div class="demo">
							<div id="slider-range"></div>
                            <p class="price-range">
                                <input type="text" id="amount" style="border:0; color:#f6931f; font-weight:normal; font-size:11px; color:#000; width:230px; text-align:center;" />
                            </p>
							</div>
					<script>
					var mnprice = '<xsl:value-of select="/XML/MIN_PRICE" disable-output-escaping="yes"/>';
					var mxprice = '<xsl:value-of select="/XML/MAX_PRICE" disable-output-escaping="yes"/>';

					$(function() {
						$( "#slider-range" ).slider({
							range: true,
							min: 0,
							max: 100000,
							step: 100,
							values: [ mnprice, mxprice ],
							slide: function( event, ui ) {
								$( "#amount" ).val( "Rs." + ui.values[ 0 ] + " - Rs." + ui.values[ 1 ] );
								document.getElementById('mnprice').value = ui.values[ 0 ] ;
								document.getElementById('mxprice').value = ui.values[ 1 ];
								_gaq.push(['_trackEvent', 'Phone Finder', 'clicked', 'Price Range']);
							}
						});
						$( "#amount" ).val( "Rs." + mnprice + " - Rs." + mxprice );
						document.getElementById('mnprice').value = mnprice ;
						document.getElementById('mxprice').value = mxprice ;
					});
					</script>
					<!--slider ends-->
                            </p>
                        </div>
				<label>Input Mechanism - </label>
                          <select name="inputMechanism" id="inputMechanism"><option value="0">- Select -</option>
						  <option value="AlphaNum">AlphaNumeric</option>
						  <option value="QWERTY">QWERTY</option>
						  <option value="Touchpad">Touchpad</option>
						  <!--option value="4">AlphaNum & QWERTY</option-->
						  <option value="Touchpad_and_AlphaNum">Touchpad &amp; AlphaNum</option>
						  <option value="Touchpad_and_QWERTY">Touchpad &amp; QWERTY</option>
						  <!--option value="7">AlphaNum & QWERTY & Touchpad</option-->
                          </select>
                        </td>
                          </tr>
				  <tr class="alt">
					<td colspan="10"><label>By Form Factor - </label>
					<input name="formFactor" type="checkbox" value="Bar" onclick="_gaq.push(['_trackEvent', 'Phone Finder', 'clicked', 'Form Factor']);" /><label class="sub-options">Bar</label>
					<input name="formFactor" type="checkbox" value="Flip" onclick="_gaq.push(['_trackEvent', 'Phone Finder', 'clicked', 'Form Factor']);"  /><label class="sub-options">Flip</label>
					<input name="formFactor" type="checkbox" value="Slider" onclick="_gaq.push(['_trackEvent', 'Phone Finder', 'clicked', 'Form Factor']);"  /><label class="sub-options">Slider</label>
					<input name="formFactor" type="checkbox" value="Swivel" onclick="_gaq.push(['_trackEvent', 'Phone Finder', 'clicked', 'Form Factor']);"  /><label class="sub-options">Swivel</label>
					<input name="formFactor" type="checkbox" value="Tablet" onclick="_gaq.push(['_trackEvent', 'Phone Finder', 'clicked', 'Form Factor']);"  /><label class="sub-options">Tablet</label>
					<input name="formFactor" type="checkbox" value="Folder" onclick="_gaq.push(['_trackEvent', 'Phone Finder', 'clicked', 'Form Factor']);"  /><label class="sub-options">Folder</label>
					<br/>
					<input name="formFactor" type="checkbox" value="Candy_Bar" onclick="_gaq.push(['_trackEvent', 'Phone Finder', 'clicked', 'Form Factor']);"  /><label class="sub-options">Candy Bar</label>
					<input name="formFactor" type="checkbox" value="QWERTY_Slider" onclick="_gaq.push(['_trackEvent', 'Phone Finder', 'clicked', 'Form Factor']);"  /><label class="sub-options">QWERTY Slider</label>
					<input name="formFactor" type="checkbox" value="Mixed" onclick="_gaq.push(['_trackEvent', 'Phone Finder', 'clicked', 'Form Factor']);"  /><label class="sub-options">Mixed</label>
					</td>
				  </tr>
				  <tr>
                            <td colspan="3"><label>Availability - </label>
                            <select name="availability" id="availability" class="w200"><option value="0">- Select -</option>
							<option value="Available_In_India">Available In India</option>
							<option value="Available_Abroad">Available Abroad</option>
							<option value="Coming_soon">Coming soon</option>
							<option value="Obselete">Obselete</option>
							
                            </select><div class="form-field-spacer"></div>
							</td>
                            <td colspan="7">
							<label>Announced - </label>
                            <select name="announced" id="announced" class="w200"><option value="0">- Select -</option>
							<option value="2007">2007</option>
							<option value="2008">2008</option>
							<option value="2009">2009</option>
							<option value="2010">2010</option>
							<option value="2011">2011</option>

                            </select>
                            </td>
                          </tr>
                          <tr class="alt">
                            <!--td width="80">
                            <label>Weight - </label>
                            </td>
                            <td colspan="2">
                            <select name="weight" id="weight">
							<option value="0">- Select -</option>
							<option value="0-100g">0-100 g</option>
							<option value="101-150g">101-150 g</option>
							<option value="150-1000g">150-1000 g</option>
                            </select>
                            </td-->
                            <td colspan="10">
                            <label>Ram - </label>
                            <select name="ram" id="ram">
							<option value="0">- Select -</option>
							<option value="256MB"> 256 MB &#38; more</option>
							<option value="512MB"> 512 MB &#38; more</option>
							<option value="1G"> 1 GB &#38; more</option>
                            </select>
                            </td>
						
                          </tr>
                          <!--tr>
                            <td colspan="5"><label>Thickness - </label>
                            <select name="depth" id="depth"><option value="0">- Select -</option>
							<option value="0-10mm">0-10 mm</option>
							<option value="1-20mm">11-20 mm</option>
							<option value="21-30mm">21-30 mm</option>
                            </select>
                            </td>
                            <td>
                            <label>Height - </label>
                            </td>
                            <td colspan="2">
                            <select name="height" id="height">
							<option value="0">- Select -</option>
							<option value="0-50mm">0-50 mm</option>
							<option value="51-100mm">51-100 mm</option>
							<option value="101-150mm">101-150 mm</option>
                            </select>
                            </td>
                            <td>
                            <label>Width - </label>
                            </td>
                            <td colspan="2">
                            <select name="width" id="width">
							<option value="0">- Select -</option>
							<option value="0-30mm">0-30 mm</option>
							<option value="31-60mm">31-60 mm</option>
							<option value="61-100mm">61-100 mm</option>
                            </select>
                            </td>
                          </tr-->

						  <tr>
                            <td align="right" colspan="10"><a onclick="_gaq.push(['_trackEvent', 'Phone Finder', 'clicked', 'List Phones - Button']);setTimeout('submitNewSearch()',500);" href="javascript:void(0);"   class="list-phones-btn spriteicon"></a></td>
                          </tr>
                        </table>
					</form>
                </div>
            </div>
            <div class="clear"></div>
            </section>

			<aside>

				<div class="RHSAd">
				<!--JavaScript Tag with group ID // Tag for network 1191: India.com // Website: Buying Guide // Page: Phone Finder // Placement: Buying_Guide_Phone_Finder_Right_Top_300x250 (3483229) // created at: Oct 31, 2011 8:34:33 AM-->
				<script language="javascript">
				<![CDATA[
				if (window.adgroupid == undefined) {
					window.adgroupid = Math.round(Math.random() * 1000);
				}
				document.write('<scr'+'ipt language="javascript1.1" src="http://jt.india.com/addyn/3.0/1191/3483229/0/170/ADTECH;cookie=info;loc=100;target=_blank;key=key1+key2+key3+key4;grp='+window.adgroupid+';misc='+new Date().getTime()+'"></scri'+'pt>');
				]]>
				</script><noscript><a href="http://jt.india.com/adlink/3.0/1191/3483229/0/170/ADTECH;cookie=info;loc=300;key=key1+key2+key3+key4" target="_blank"><img src="http://jt.india.com/adserv/3.0/1191/3483229/0/170/ADTECH;cookie=info;loc=300;key=key1+key2+key3+key4" border="0" width="300" height="250" /></a></noscript>
				<!-- End of JavaScript Tag -->
				</div>
				<div class="clear"></div>


              <!-- Top newsletter Widget start -->
				<xsl:call-template name="newsletterWidget"/>
			  <!-- Top newsletter Widget end -->
  
              <div class="clear"></div>
              <!-- Top Rated Widget start -->
				<xsl:call-template name="topRatedWidget"/>
			  <!-- Top Rated Widget end -->
              <div class="clear"></div>
            </aside>

            <div class="clear"></div>
          </div>
          <!-- body end -->
          <div class="clear"></div>
          <!--prefooter ad module starts-->
          <div class="prefooter-ad">
            <!--JavaScript Tag with group ID // Tag for network 1191: India.com // Website: Buying Guide // Page: Phone Finder // Placement: Buying_Guide_Phone_Finder_Bottom_728x90 (3483228) // created at: Oct 31, 2011 8:34:33 AM-->
			<script language="javascript">
			<![CDATA[
			if (window.adgroupid == undefined) {
				window.adgroupid = Math.round(Math.random() * 1000);
			}
			document.write('<scr'+'ipt language="javascript1.1" src="http://jt.india.com/addyn/3.0/1191/3483228/0/225/ADTECH;cookie=info;loc=100;target=_blank;key=key1+key2+key3+key4;grp='+window.adgroupid+';misc='+new Date().getTime()+'"></scri'+'pt>');
			]]>
			</script><noscript><a href="http://jt.india.com/adlink/3.0/1191/3483228/0/225/ADTECH;cookie=info;loc=300;key=key1+key2+key3+key4" target="_blank"><img src="http://jt.india.com/adserv/3.0/1191/3483228/0/225/ADTECH;cookie=info;loc=300;key=key1+key2+key3+key4" border="0" width="728" height="90" /></a></noscript>
			<!-- End of JavaScript Tag -->
          </div>
          <!--prefooter ad module starts-->
          <div class="clear"></div>
          <!-- footer starts -->
          <xsl:call-template name="footerDiv"/>
          <!-- footer end -->
          <div class="clear"></div>
        </div>
      </body>
<script language="javascript">
<![CDATA[
$(function(){
     // add multiple select / deselect functionality
    $("#selectall1").click(function () {
          $('.case1').attr('checked', this.checked);
    });
 
    // if all checkbox are selected, check the selectall checkbox and viceversa
    $(".case1").click(function(){
 
        if($(".case1").length == $(".case1:checked").length) {
            $("#selectall1").attr("checked", "checked");
        } else {
            $("#selectall1").removeAttr("checked");
        }
 
    });


	 // add multiple select / deselect functionality
    $("#selectall2").click(function () {
          $('.case2').attr('checked', this.checked);
    });
 
    // if all checkbox are selected, check the selectall checkbox and viceversa
    $(".case2").click(function(){
 
        if($(".case2").length == $(".case2:checked").length) {
            $("#selectall2").attr("checked", "checked");
        } else {
            $("#selectall2").removeAttr("checked");
        }
 
    });


	 // add multiple select / deselect functionality
    $("#selectall3").click(function () {
          $('.case3').attr('checked', this.checked);
    });
 
    // if all checkbox are selected, check the selectall checkbox and viceversa
    $(".case3").click(function(){
 
        if($(".case3").length == $(".case3:checked").length) {
            $("#selectall3").attr("checked", "checked");
        } else {
            $("#selectall3").removeAttr("checked");
        }
 
    });
});





	function submitNewSearch(){
		var qstring = '';


		var networkTypes	= getCheckBoxValues('networkType');
		if( networkTypes != '')	qstring +=	'/Technology-' + networkTypes;

		var brands	= getCheckBoxValues('branddetails');
		if( brands != '')	{
			qstring +=	'/Brand-' + brands;
		} else {
			qstring +=	'/Brand-all';
		}

		var phoneTypes	= getCheckBoxValues('phoneType');
		if( phoneTypes != '')	qstring +=	'/PhoneType-' + phoneTypes;

		var formFactors	= getCheckBoxValues('formFactor');
		if( formFactors != '')	qstring +=	'/FormFactor-' + formFactors;

		var sim = $("input[name=dualSIM]:checked").val();
		if( sim != '' && sim != undefined ){
			qstring +=	'/SIM-' + sim;
		} 

		if($('#inputMechanism').prop('selectedIndex')){
			var phoneTypeValue	= $("#inputMechanism").val();
			qstring +=	'/InputType-' + phoneTypeValue;
		}

		if($('#weight').prop('selectedIndex')){
			var weight	= $("#weight").val();
			qstring +=	'/Weight-' + weight;
		}

		if($('#width').prop('selectedIndex')){
			var width	= $("#width").val();
			qstring +=	'/Width-' + width;
		}

		if($('#height').prop('selectedIndex')){
			var height	= $("#height").val();
			qstring +=	'/Height-' + height;
		}

		if($('#depth').prop('selectedIndex')){
			var thickness	= $("#depth").val();
			qstring +=	'/Thickness-' + thickness;
		}

		if($('#os').prop('selectedIndex')){
			var os	= $("#os").val();
			qstring +=	'/OS-' + os;
		}

		if($('#ram').prop('selectedIndex')){
			var ram	= $("#ram").val();
			qstring +=	'/RAM-' + ram;
		}

		if($('#availability').prop('selectedIndex')){
			var availability	= $("#availability").val();
			qstring +=	'/Availability-' + availability;
		}

		if($('#announced').prop('selectedIndex')){
			var announced	= $("#announced").val();
			qstring +=	'/Announced-' + announced;
		}


		var GPS = $("input[name=GPS]:checked").val();
		if( GPS != '' && GPS != undefined ){
			qstring +=	'/GPS-' + GPS;
		} 

		var WIFI = $("input[name=WIFI]:checked").val();
		if( WIFI != '' && WIFI != undefined ){
			qstring +=	'/WIFI-' + WIFI;
		} 

		if($('#camResolution').prop('selectedIndex')){
			var camResolution	= $("#camResolution").val();
			qstring +=	'/Camera-' + camResolution;
		}

		qstring +=	'/Price-' + $('#mnprice').val() + '-' +$('#mxprice').val() ;
		qstring	= encodeURI(qstring);
		location.href	= web_url + 'Mobile-finder' + qstring ;
	}

	function getCheckBoxValues( elName )
	{
		brands	= ($("input[name="+elName+"]:checked").map
		(function () {return this.value;}).get().join("-") );
		return brands;
	}
]]>
</script>
    </html>
  </xsl:template>
</xsl:stylesheet>