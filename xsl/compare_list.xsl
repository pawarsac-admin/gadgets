<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet  [
  <!ENTITY nbsp   "&#160;">
  <!ENTITY copy   "&#169;">
  <!ENTITY reg    "&#174;">
  <!ENTITY trade  "&#8482;">
  <!ENTITY mdash  "&#8212;">
  <!ENTITY ldquo  "&#8220;">
  <!ENTITY rdquo  "&#8221;">
  <!ENTITY pound  "&#163;">
  <!ENTITY yen    "&#165;">
  <!ENTITY euro   "&#8364;">
  <!ENTITY deg "&#176;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fb="http://www.facebook.com/2008/fbml" version="1.0">
  <xsl:output method="html" encoding="utf-8" indent="yes" doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"/>
  <xsl:include href="inc_header.xsl" />
  <xsl:include href="inc_footer.xsl" />
  <xsl:include href="inc_newsletter_widget.xsl" />
  <xsl:include href="inc_top_rated_widget.xsl" />
  <xsl:template match="/">
    <html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><xsl:value-of select="/XML/SEO_TITLE" /></title>
<meta name="Keywords" content="{/XML/SEO_TAGS}" />
<meta name="Description" content="{/XML/SEO_DESC}" />
<link href="{/XML/CSS_URL}style.css?version={/XML/VERSION}" rel="stylesheet" type="text/css" />
<!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--<script type="text/javascript" src="https://getfirebug.com/firebug-lite.js"></script> -->
<script>
	var comparedIds	= '';
	var catid		= '<xsl:value-of select="/XML/SELECTED_CATEGORY_ID" disable-output-escaping="yes"/>';
</script>
		<script type="text/javascript">
		// Google Analytics start
		  var _gaq = _gaq || [];
		  _gaq.push(['_setAccount', 'UA-27114870-1']);
		  _gaq.push(['_trackPageview']);

		  (function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();
		// Google Analytics end
		//<!-- Quantcast Tag part 1-->

		var _qevents = _qevents || [];

		(function() {
		var elem = document.createElement('script');
		elem.src = (document.location.protocol == "https:" ? "https://secure" : "http://edge") + ".quantserve.com/quant.js";
		elem.async = true;
		elem.type = "text/javascript";
		var scpt = document.getElementsByTagName('script')[0];
		scpt.parentNode.insertBefore(elem, scpt);
		})();

		_qevents.push({
		qacct:"p-31f3D02tYU8zY"
		});
		</script>
		<!-- Quantcast Tag part 1-->
<style type="text/css">
	#pager_links{float:right; margin:5px 25px 0 0;}
    #pager_links a { text-decoration:none; color:#DF9802; padding:1px 4px 1px 4px; margin:2px; font:bold 12px arial;}
    #pager_links a:hover { text-decoration:underline; padding:1px 4px 1px 4px; margin:2px; }
    #current_page { padding:1px 4px 1px 4px; margin:2px; color:#666; font-weight:bold;}
</style>
</head>

<body>
<!-- Quantcast Tag part 2-->
<noscript>
<div style="display:none;">
<img src="//pixel.quantserve.com/pixel/p-31f3D02tYU8zY.gif" border="0" height="1" width="1" alt="Quantcast"/>
</div>
</noscript>
<!--Quantcast Tag part 2 -->

<div id="wrapper">
	<!-- header start -->
          <xsl:call-template name="headDiv"/>
          <!-- header end -->
    <div class="clear"></div>
   	<!-- body starts -->
    <div id="maincontent-wrapper">
        	<section>
            <div class="clear"></div>
            <div class="search-result-compare">
				<div class="search-results-wrap">
					<div class="search-results-header">
						<div style="float:left; width: 560px;">
							<span>Phone Listings : </span> 
						</div>
					</div>
					<br/><br/>
						<xsl:for-each select="/XML/RECORDS/RECORD">
							<xsl:value-of select="." disable-output-escaping="yes"/><br/><br/>
						</xsl:for-each>
					<div class="clear"></div>
				<xsl:value-of select="/XML/PAGER" disable-output-escaping="yes"/>
				</div>
			</div>
            <div class="clear"></div>
				
            </section>
            <aside>

				<div class="RHSAd">
				<!--JavaScript Tag with group ID // Tag for network 1191: India.com // Website: Buying Guide // Page: Phone List // Placement: Buying_Guide_Phone_List_Right_Top_300x250 (3483230) // created at: Oct 31, 2011 8:34:33 AM-->
				<script language="javascript">
				<![CDATA[
				if (window.adgroupid == undefined) {
					window.adgroupid = Math.round(Math.random() * 1000);
				}
				document.write('<scr'+'ipt language="javascript1.1" src="http://jt.india.com/addyn/3.0/1191/3483230/0/170/ADTECH;cookie=info;loc=100;target=_blank;key=key1+key2+key3+key4;grp='+window.adgroupid+';misc='+new Date().getTime()+'"></scri'+'pt>');
				]]>
				</script><noscript><a href="http://jt.india.com/adlink/3.0/1191/3483230/0/170/ADTECH;cookie=info;loc=300;key=key1+key2+key3+key4" target="_blank"><img src="http://jt.india.com/adserv/3.0/1191/3483230/0/170/ADTECH;cookie=info;loc=300;key=key1+key2+key3+key4" border="0" width="300" height="250" /></a></noscript>
				<!-- End of JavaScript Tag -->
				</div>
				<div class="clear"></div>


			<!-- Top newsletter Widget start -->
			<xsl:call-template name="newsletterWidget"/>
			<!-- Top newsletter Widget end -->

            <div class="clear"></div>
              <!-- Top Rated Widget start -->
				<xsl:call-template name="topRatedWidget"/>
			  <!-- Top Rated Widget end -->
			<div class="clear"></div>
            </aside>
		<div class="clear"></div>
      </div>
    <!-- body end -->
    <div class="clear"></div>
    <!--prefooter ad module starts-->
    <div class="prefooter-ad">
		<!--JavaScript Tag with group ID // Tag for network 1191: India.com // Website: Buying Guide // Page: Phone List // Placement: Buying_Guide_Phone_List_Bottom_728x90 (3483231) // created at: Oct 31, 2011 8:34:34 AM-->
		<script language="javascript">
		<![CDATA[
		if (window.adgroupid == undefined) {
			window.adgroupid = Math.round(Math.random() * 1000);
		}
		document.write('<scr'+'ipt language="javascript1.1" src="http://jt.india.com/addyn/3.0/1191/3483231/0/225/ADTECH;cookie=info;loc=100;target=_blank;key=key1+key2+key3+key4;grp='+window.adgroupid+';misc='+new Date().getTime()+'"></scri'+'pt>');
		]]>
		</script><noscript><a href="http://jt.india.com/adlink/3.0/1191/3483231/0/225/ADTECH;cookie=info;loc=300;key=key1+key2+key3+key4" target="_blank"><img src="http://jt.india.com/adserv/3.0/1191/3483231/0/225/ADTECH;cookie=info;loc=300;key=key1+key2+key3+key4" border="0" width="728" height="90" /></a></noscript>
		<!-- End of JavaScript Tag -->
	</div>
    <!--prefooter ad module starts-->
    <div class="clear"></div>
    <!-- footer starts -->
          <xsl:call-template name="footerDiv"/>
          <!-- footer end -->
    <div class="clear"></div>
</div>
<script>
var diff = 0 ;
<![CDATA[
var $scrollingDiv = $("#compare-btn");

var dist	= 210;
if(searchTab =='3')	dist -= 100; 
 
$(window).scroll(function(){            
    var y = $(this).scrollTop(),
        maxY = $('#footer-outer').offset().top - dist,
        scrollHeight = $scrollingDiv.height();
	
	tVar	= $(window).scrollTop() - dist;

    if( y < maxY-scrollHeight ){
		t1Var	= parseInt($("#compare-btn").css('margin-top'));
			//alert($(window).scrollTop());
			if( $(window).scrollTop() <= 300 ) tVar = 5 ;
			$scrollingDiv
			.stop()
			.animate({"marginTop": ( tVar ) + "px"}, "slow" );        
    }    
});
]]>
</script>
<script src="{/XML/JS_URL}common.js?version={/XML/VERSION}"></script>
</body>
</html>
</xsl:template>
</xsl:stylesheet>
