<?php
require_once('include/config.php');
require_once(CLASSPATH.'DbConn.php');
require_once(CLASSPATH.'DbOp.php');

$dbconn         = new DbConn;
$DbOperation    = new DbOperation;

$patterns		= '/[ ]+/';
$replacements	= '';

echo "\n<br/>SCRIPT START<br\>\n";
echo "\n<br/>/////////////////////////  UPDATING 2G NETWORK TYPE /////////////////////////////";

$feature_id	=  109;
$arr2GNetwork[$feature_id][1]	= 'GSM';
$arr2GNetwork[$feature_id][2]	= 'CDMA';
$arr2GNetwork[$feature_id][3]	= 'GSM & CDMA';

foreach($arr2GNetwork[$feature_id] as $key => $value ){
	$value	= str_replace('&', '%', $value);
	$value	= preg_replace($patterns, $replacements, $value);
	$sql	= "UPDATE PRODUCT_FEATURE SET feature_numeric_value=$key WHERE feature_id= $feature_id AND feature_value LIKE '%$value%'";
	echo "\n<br/>",$sql;
    $data           = $DbOperation->update($sql);
	echo "\n<br/>Rows Updated : " , $data;
}

echo "\n<br/>/////////////////////////  UPDATING 3G NETWORK TYPE /////////////////////////////";

$feature_id	=  6;
$arr3GNetwork[$feature_id][1]	= 'CDMA';
$arr3GNetwork[$feature_id][2]	= 'UMTS';
$arr3GNetwork[$feature_id][3]	= 'HSDPA';

foreach($arr3GNetwork[$feature_id] as $key => $value ){
	$value	= preg_replace($patterns, $replacements, $value);
	$sql	= "UPDATE PRODUCT_FEATURE SET feature_numeric_value=$key WHERE feature_id= $feature_id AND feature_value LIKE '%$value%'";
	echo "\n<br/>",$sql;
    $data           = $DbOperation->update($sql);
	echo "\n<br/>Rows Updated : " , $data;

}

echo "\n<br/>/////////////////////////  UPDATING DEVICE TYPE /////////////////////////////";

$feature_id	=  79;
$arrDeviceType[$feature_id][1]	= 'Bar';
$arrDeviceType[$feature_id][3]	= 'Flip';
$arrDeviceType[$feature_id][4]	= 'Slide';
$arrDeviceType[$feature_id][5]	= 'Tablet';
$arrDeviceType[$feature_id][7]	= 'Folder';
$arrDeviceType[$feature_id][8]	= 'Swivel';
$arrDeviceType[$feature_id][2]	= 'Candy Bar';
$arrDeviceType[$feature_id][6]	= 'QWERTY Slider';
$arrDeviceType[$feature_id][9]	= 'Mixed';

foreach($arrDeviceType[$feature_id] as $key => $value ){
	$sql	= "UPDATE PRODUCT_FEATURE SET feature_numeric_value=$key WHERE feature_id= $feature_id AND feature_value LIKE '%$value%'";
	echo "\n<br/>",$sql;
    $data           = $DbOperation->update($sql);
	echo "\n<br/>Rows Updated : " , $data;

}

echo "\n<br/>/////////////////////////  UPDATING INPUT MECHANISM /////////////////////////////";

$feature_id	=  97;
$arrInputMech[$feature_id][1]	= 'AlphaNum';
$arrInputMech[$feature_id][2]	= 'QWERTY';
$arrInputMech[$feature_id][3]	= 'Touchpad';
$arrInputMech[$feature_id][4]	= 'AlphaNum & QWERTY';
$arrInputMech[$feature_id][5]	= 'AlphaNum & Touchpad';
$arrInputMech[$feature_id][6]	= 'QWERTY & Touchpad';
$arrInputMech[$feature_id][7]	= 'AlphaNum & QWERTY & Touchpad';

foreach($arrInputMech[$feature_id] as $key => $value ){
	$sql	= "UPDATE PRODUCT_FEATURE SET feature_numeric_value=$key WHERE feature_id= $feature_id AND feature_value LIKE '%$value%'";
	echo "\n<br/>",$sql;
    $data           = $DbOperation->update($sql);
	echo "\n<br/>Rows Updated : " , $data;

}

echo "\n<br/>/////////////////////////  UPDATING DUAL SIM FEATURE /////////////////////////////";

$feature_id	=  112;
$arrDualSIM[$feature_id][1]	= 'YES';
$arrDualSIM[$feature_id][2]	= 'NO';


foreach($arrDualSIM[$feature_id] as $key => $value ){
	$sql	= "UPDATE PRODUCT_FEATURE SET feature_numeric_value=$key WHERE feature_id= $feature_id AND feature_value LIKE '%$value%'";
	echo "\n<br/>",$sql;
    $data           = $DbOperation->update($sql);
	echo "\n<br/>Rows Updated : " , $data;
}


echo "\n<br/>/////////////////////////  UPDATING OPERATING SYSTEM /////////////////////////////";

$feature_id	=  106;
$arrOS[$feature_id][1]	= 'Android';
$arrOS[$feature_id][2]	= 'iPhone';
$arrOS[$feature_id][3]	= 'Symbian';
$arrOS[$feature_id][4]	= 'Windows';
$arrOS[$feature_id][5]	= 'Blackberry';
$arrOS[$feature_id][6]	= 'Bada';
$arrOS[$feature_id][7]	= 'Others';


foreach($arrOS[$feature_id] as $key => $value ){
	$sql	= "UPDATE PRODUCT_FEATURE SET feature_numeric_value=$key WHERE feature_id= $feature_id AND feature_value LIKE '%$value%'";
	echo "\n<br/>",$sql;
    $data           = $DbOperation->update($sql);
	echo "\n<br/>Rows Updated : " , $data;
}

echo "\n<br/>/////////////////////////  UPDATING RAM /////////////////////////////";

$feature_id	=  111;
$arrRAM[$feature_id][1]	= 256;
$arrRAM[$feature_id][2]	= 512;
$arrRAM[$feature_id][3]	= 1024;

//arsort($arrRAM[$feature_id]);

foreach($arrRAM[$feature_id] as $key => $value ){
	$sql	= "UPDATE PRODUCT_FEATURE SET feature_numeric_value=$key WHERE feature_id = $feature_id AND feature_value >= $value";
	echo "\n<br/>",$sql;
    $data           = $DbOperation->update($sql);
	echo "\n<br/>Rows Updated : " , $data;
}

echo "\n<br/>/////////////////////////  UPDATING WEIGHT /////////////////////////////";

$feature_id	=  34;
$arrWeight[$feature_id][1]	= '0-100';
$arrWeight[$feature_id][2]	= '101-150';
$arrWeight[$feature_id][3]	= '151-1000';


foreach($arrWeight[$feature_id] as $key => $value ){
	$arrT	= explode('-',$value);
	$sql	= "UPDATE PRODUCT_FEATURE SET feature_numeric_value=$key WHERE feature_id= $feature_id AND feature_value BETWEEN $arrT[0] AND $arrT[1]";
	echo "\n<br/>",$sql;
    $data           = $DbOperation->update($sql);
	echo "\n<br/>Rows Updated : " , $data;
}


echo "\n<br/>/////////////////////////  UPDATING Length /////////////////////////////";

$feature_id	=  30;
$arrLength[$feature_id][1]	= '0-50';
$arrLength[$feature_id][2]	= '51-100';
$arrLength[$feature_id][3]	= '101-150';


foreach($arrLength[$feature_id] as $key => $value ){
	$arrT	= explode('-',$value);
	$sql	= "UPDATE PRODUCT_FEATURE SET feature_numeric_value=$key WHERE feature_id= $feature_id AND feature_value BETWEEN $arrT[0] AND $arrT[1]";
	echo "\n<br/>",$sql;
    $data           = $DbOperation->update($sql);
	echo "\n<br/>Rows Updated : " , $data;
}


echo "\n<br/>/////////////////////////  UPDATING Breadth /////////////////////////////";

$feature_id	=  31;
$arrBreadth[$feature_id][1]	= '0-30';
$arrBreadth[$feature_id][2]	= '31-60';
$arrBreadth[$feature_id][3]	= '61-100';


foreach($arrBreadth[$feature_id] as $key => $value ){
	$arrT	= explode('-',$value);
	$sql	= "UPDATE PRODUCT_FEATURE SET feature_numeric_value=$key WHERE feature_id= $feature_id AND feature_value BETWEEN $arrT[0] AND $arrT[1]";
	echo "\n<br/>",$sql;
    $data           = $DbOperation->update($sql);
	echo "\n<br/>Rows Updated : " , $data;
}

echo "\n<br/>/////////////////////////  UPDATING Thickness /////////////////////////////";

$feature_id	=  32;
$arrThickness[$feature_id][1]	= '0-5';
$arrThickness[$feature_id][2]	= '6-10';
$arrThickness[$feature_id][3]	= '11-20';


foreach($arrThickness[$feature_id] as $key => $value ){
	$arrT	= explode('-',$value);
	$sql	= "UPDATE PRODUCT_FEATURE SET feature_numeric_value=$key WHERE feature_id= $feature_id AND feature_value BETWEEN $arrT[0] AND $arrT[1]";
	echo "\n<br/>",$sql;
    $data           = $DbOperation->update($sql);
	echo "\n<br/>Rows Updated : " , $data;
}

echo "\n<br/>SCRIPT END<br\>\n";
?>