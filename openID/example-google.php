<?php
 #Logging in with Google accounts requires setting special identity, so this example shows how to do it.
require 'openid.php';
try {
    $openid = new LightOpenID;
    if(!$openid->mode) {
        if(isset($_GET['login'])) {
            $openid->identity = 'https://www.google.com/accounts/o8/id';
			$openid->required = array('birthDate','person/gender','contact/postalCode/home','namePerson/first', 'namePerson/last', 'contact/email','contact/country/home'); 
			
			//$openid->optional = array('birthDate','person/gender','contact/postalCode/home');
            header('Location: ' . $openid->authUrl());
        }
?>
<form action="?login" method="post">
    <button>Login with Google</button>
</form>
<?php
    } elseif($openid->mode == 'cancel') {
        echo 'User has canceled authentication!';
    } else {
        echo 'User ' . ($openid->validate() ? $openid->identity . ' has ' : 'has not ') . 'logged in.';
		  $_GET['login'] = true;  
       echo("openid ok<br/>");  
       $retVal = $openid->validate();  
       if($retVal){  
          $userAttributes = $openid->getAttributes();  
		  print_R($userAttributes);
          $firstName = $userAttributes['namePerson/first'];  
          $lastName = $userAttributes['namePerson/last'];  
          $userEmail = $userAttributes['contact/email'];  

		  $birthDate	= $userAttributes['birthDate'];  
          $gender		= $userAttributes['person/gender'];  
          $postalCode	= $userAttributes['contact/postalCode/home'];
		  $country		= $userAttributes['contact/country/home']; 
		  
		 


          echo 'Your name: '.$firstName.' '.$lastName.'<br />';  
          echo("Your email address is: ".$userEmail."<br/>");  

		   echo 'Birthday: '.$birthDate.'<br />';  
          echo("gender: ".$gender."<br/>");  

		   echo 'Your postalCode: '.$postalCode.'<br />';  
          echo("Your country: ".$country."<br/>");  
          echo("Is logged in? ".$_GET['login']."<br/>");  
          //echo '<pre>';  
          //print_r($openid);  
          //print_r($openid->getAttributes());  
          //echo '</pre>';  
       } else {  
          echo('Failed to login.');  
       }  
    }
} catch(ErrorException $e) {
    echo $e->getMessage();
}
