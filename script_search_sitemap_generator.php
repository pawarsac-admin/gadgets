<?php

// SEARCH sitemaps
	error_reporting(0 ); 
	$Technologis	= array ( '', 'GSM', 'CDMA');

	$Brands			= array( 'all', 'Apple', 'BlackBerry', 'Dell', 'HTC',  'LG', 'Micromax', 'Motorola', 'Nokia', 'Samsung', 'Sony_Ericsson', 'Spice');

	$PhoneTypes		= array( '', 'Smart_Phone','Feature_Phone','Basic_Phone' );

	$arrDualSIM		= array( '', 'Single','Dual');
	$InputTypes		= array( '', 'QWERTY','Touchpad');
	$arrOS			= array( '', 'Android', 'iOS','Symbian','Windows_Phone','Blackberry','Bada' );

	$arrGPS			= array( '', 'Yes');
	$arrWIFI		= array( '', 'Yes');

//	$Prices			= array ( '', '0-1000', '1000-5000', '4000-7000', '5000-8000', '8000-10000', '10000-15000', '15000-20000', '20000-30000', '30000-40000', '40000-50000'  );
	$Prices			= array ( '');

	$sitemap_base_path = 'http://gadgets.india.com/data/sitemap/xml/search/';
	//$sitemap_base_path = 'http://gadgets.india.com/data/sitemap/xml/search/';


//http://gadgets.india.com/Mobile-finder/Technology-GSM/Brand-all/PhoneType-Tablet/SIM-Single/InputType-Touchpad/OS-Windows_Phone/GPS-No/WIFI-No/Price-0-100000

	/*
	 * <sitemapindex>
	 * <sitemap>
	 *      <loc>http://www.oncars.in/data/sitemap/xml/8/118/compare_all_118.xml?v=1</loc>
	 * </sitemap>
	 * <sitemap>
	 *      <loc>http://www.oncars.in/data/sitemap/xml/9/119/compare_all_119.xml?v=1</loc>
	 * </sitemap>
	 */

	echo "\n*********************************** SCRIPT STARTED ***********************************\n";

	$i					= 1;
	$sitemap_ctr		= 1;
	$storePath			= './data/sitemap/xml/search/';
	$sitemapIdxFilePath = $storePath . 'sitemap_index.xml';

	$filename = 'sitemap'.$sitemap_ctr.'.xml';
	$filePath = $storePath . $filename ;
	$fh = fopen($filePath, 'w');
	fwrite($fh,"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
	fwrite($fh, "<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">\n");


	$fhidx = fopen( $sitemapIdxFilePath, 'w' );
	fwrite($fhidx,"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
	fwrite($fhidx, "<sitemapindex  xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">\n");
	fwrite($fhidx, "<sitemap><loc>".$sitemap_base_path.$filename."</loc></sitemap>\n");

        
        foreach ($Brands as $brand) {        
            $url = "";
            if ($brand != ""){
                    $url .= "/Brand-".$brand;
            }
            $stringData = "<url><loc>http://gadgets.india.com/Mobile-finder".$url."</loc></url>\n";
            fwrite($fh, $stringData);
            $i++;

        }
        
	foreach ($Technologis as $Technology) {
		echo "\nOn Technology: $Technology\n";
//		foreach ($Brands as $brand) {
//		echo "\nOn Brand: $brand\n";
			foreach ($PhoneTypes as $PhoneType) {
				foreach ($arrDualSIM as $DualSIM) {
					foreach ($InputTypes as $InputType) {
						foreach ($arrOS as $OS) {
							foreach ($arrGPS as $GPS) {
								foreach ($arrWIFI as $WIFI) {
									foreach ($Prices as $Price) {
										$url = "";
//										if ($brand != ""){
//											$url .= "/Brand-".$brand;
//										}
										if ($Technology != ""){
											$url .= "/Technology-".$Technology;
										}
										if ($PhoneType != ""){
											$url .= "/PhoneType-".$PhoneType;
										}
										if ($DualSIM != ""){
											$url .= "/SIM-".$DualSIM;
										}
										if ($InputType != ""){
											$url .= "/InputType-".$InputType;
										}
										if ($OS != ""){
											$url .= "/OS-".$OS;
										}
										if ($GPS != ""){
											$url .= "/GPS-".$GPS;
										}
										if ($WIFI != ""){
											$url .= "/WIFI-".$WIFI;
										}
										if ($Price != ""){
											$url .= "/Price-".$Price;
										}
										$stringData = "<url><loc>http://gadgets.india.com/Mobile-finder".$url."</loc></url>\n";
										fwrite($fh, $stringData);
										//echo "\n i : $i $stringData";
										$i++;
										if($i == 20000)
										{
											fwrite($fh, "</urlset>\n");
											fclose($fh);
											$i = 1;
											$sitemap_ctr++;
											$filename = "sitemap".$sitemap_ctr.".xml";
											$filePath = $storePath . $filename ;
											$fh = fopen( $filePath, 'w');
											fwrite($fh,"<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
											fwrite($fh, "<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">\n");
											fwrite($fhidx, "<sitemap><loc>".$sitemap_base_path .$filename . "</loc></sitemap>\n");
										}
									}
								}
							}
						}
					}
				}
			}
//		}
	}

	fwrite($fhidx, "</sitemapindex>\n");
	fclose($fhidx);
	fwrite($fh, "</urlset>\n");
	fclose($fh);
	echo "\n*********************************** SCRIPT FINISHED ***********************************\n\n";
?>