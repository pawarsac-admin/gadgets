<?php
//ini_set("display_errors",true);
//error_reporting(E_ALL);
require_once("include/config.php");
require_once(CLASSPATH.'DbConn.php');
require_once(CLASSPATH."authentication.class.php");
require_once(CLASSPATH."utility.class.php");
require_once(CLASSPATH.'xmlparser.class.php');
require_once(CLASSPATH."report.class.php");
//require_once(CLASSPATH."/user_details.class.php");
//$oUserDetails = new user_details;
$dbconn = new DbConn;
$oXmlparser		= new XMLParser;
$oAuth =new authentication();
$report = new report;

$sAction	 = $_REQUEST['action'];
$sAuthToken	 = $_REQUEST['oauth_token'];
$sRedirectUrl	= $_POST['redirect_url'];
$iSid			= $_POST['service_id'];
//error_log("Action:".$sAction);
//print_r($_REQUEST);

if(!empty($sAuthToken) && $sAction=='twitter'){
	require_once(TWITTER_PATH_PATH."callback.php");
	error_log(" after Action:".$sAction);
}


switch($sAction){		
	case 'chkSession' :
		$iRes=$oAuth->auth_checkSession($_COOKIE['email'], $_COOKIE['session_id'],SERVICEID,$_COOKIE['uid']);

		if($iRes==1){
			//check if the user has set his default college
			$aParameters=Array("user_id"=>$_COOKIE['uid'],"is_active"=>1);	
			$aUserColDet=$oUserDetails->fetchUserCollegeDetailsList($aParameters,$sOrderfield="college_id",$sOrder="ASC",$iStartlimit="",$iCnt="");
			if(!is_array($aUserColDet)){
				$iRes=2;
			}
		}
		print  $iRes;
	break;	
	case 'facebook' :
		$cookie = $oAuth->get_facebook_cookie(FACEBOOK_APP_ID, FACEBOOK_SECRET);
		if(is_array($cookie) && count($cookie)>0){
				$sAccessToken	= $cookie['access_token'];
				$user			= $oAuth->getFacebookUsersDetails($sAccessToken);
				$iUid			= $user->id;
				$sEmail			= $user->email;
				$sFname			= $user->first_name;
				$sLname			= $user->last_name;
				$sDob			= date('Y-m-d',strtotime($user->birthday));
				$sGender		= ($user->gender=='male') ? 'M' : 'F';
		}		
		$mode			= $_POST['mode'];
		
			
		$sContent= "action=register&mode=$mode&redirect_url=$sRedirectUrl&service_id=$iSid&id=$iUid&email=$sEmail&first_name=$sFname&last_name=$sLname&dob=$sDob&sex=$sGender";
		
		$sResultXML =utility::curlaccess($sContent,AUTH_API_ACTION_URL);
		$oXmlparser->XMLParse($sResultXML);
		$aResultXML =$oXmlparser->getOutput();

		$iUid	= $aResultXML['response']['uid'];
		$sFname	= $aResultXML['response']['fname'];
		$sLname	= $aResultXML['response']['lname'];
		$sEmail	= $aResultXML['response']['email'];
		$sRedUrl= $sRedirectUrl;

		$sSessionId = authentication::create_session(array('email'=>$sEmail, 'uid'=>$iUid));

		setcookie("email", $sEmail, $sExpTime, "/",DOMAIN);
		setcookie("session_id", $sSessionId, $sExpTime, "/",DOMAIN);
		setcookie("uid", $iUid,$sExpTime,"/",DOMAIN);
		setcookie("fname", $sFname,$sExpTime,"/",DOMAIN);
		setcookie("lname", $sLname,$sExpTime,"/",DOMAIN); 
		
		header("Location: $sRedUrl");
		
		break;

	case 'anonymous_user':

		$iCPage		= $_REQUEST['cpage'];
		$iTId		= $_REQUEST['tid'];
		$cid		= $_REQUEST['cid'];
		$id			= $_REQUEST['id'];
		$sRcontent	= urldecode($_REQUEST['rcontent']);
		$sSubject	= $_REQUEST['subject'];
		$iParentId	= $_REQUEST['pid'];
		$iServiceId	= SERVICEID;	
		// Anonymous user details
		$sName		= $_REQUEST['name'];
		$sEmail		= $_REQUEST['email'];
		$sCity		= $_REQUEST['city'];
		$sGender	= $_REQUEST['gender'];
		$sProfile	= "";

		$mode			= 'anonymous_user';

		$sContent= "action=register&mode=$mode&redirect_url=$sRedirectUrl&service_id=$iServiceId&id=$iUid&email=$sEmail&name=$sName&sex=$sGender&city=$sCity";
		//error_log("Content:".$sContent);



		$sResultXML =utility::curlaccess($sContent,AUTH_API_ACTION_URL);
		$oXmlparser->XMLParse($sResultXML);
		$aResultXML =$oXmlparser->getOutput();	

		if($aResultXML['response']['status']==-1){
			echo -1;
			die;
		}

		

		$iUid	= $aResultXML['response']['uid'];
		$sFname	= $aResultXML['response']['fname'];
		$sLname	= $aResultXML['response']['lname'];
		$sEmail	= $aResultXML['response']['email'];
		$sUrl	= $aResultXML['response']['url'];

		
		/**
		 * include utility class
		*/
		require_once(CLASSPATH."/campus_discussion.class.php");
		$oCampusDiscussion  = new campus_discussion();
		$oUtility			= new utility;

		//strip tags from reply content 
		if(strlen($sRcontent)>0){
			$sRcontent=trim(strip_tags($sRcontent, $sAllowedTags));
			$sRcontent=trim(str_replace("]]>","",$sRcontent));
			$sRcontent=trim($oUtility->closetags($sRcontent));
			$sRcontent=trim(nl2br($sRcontent));
		}

		$aParameters=Array("tid"=>$iTId,"uid"=>$iUid,"profile"=>$sProfile,"subject"=>$sSubject,"content"=>$sRcontent,"parentid"=>$iParentId);
		$aReplyRes=$oCampusDiscussion->addReply($aParameters);
		$iReplyId=-2;
		
		$array_param = Array("id"=>$id,"sid"=>$iServiceId,"cid"=>$cid,"iTId"=>$iTId);
		//print_r($array_param);
		$result = $report->UpdateCommentCount($array_param);
		
		$iReplyId=$aReplyRes['replyid'];
		echo $iReplyId;die;		
		break;
	case 'twitter':
		//print_R($content);die;
		$iUid			= $content->id;
		$sEmail			= "$iUid@india.com";
		$aName			= explode(' ',$content->name);
		$sFname			= $aName[0];
		$sLname			= $aName[1];
		$sGender		= '';
		$sPassword		= md5($iUid); 
		$mode			= 'twitter';

		$sContent= "action=register&mode=$mode&redirect_url=$sRedirectUrl&service_id=$iSid&id=$iUid&email=$sEmail&first_name=$sFname&last_name=$sLname&sex=$sGender";
		//error_log("Content:".$sContent);
		//print_r($sContent);die;


		$sResultXML =utility::curlaccess($sContent,AUTH_API_ACTION_URL);
		//print "<pre>" print_r($sResultXML);
		//error_log("sResultXML--:".$sResultXML);
		$oXmlparser->XMLParse($sResultXML);
		$aResultXML =$oXmlparser->getOutput();
		//die();
		$iUid	= $aResultXML['response']['uid'];
		$sFname	= $aResultXML['response']['fname'];
		$sLname	= $aResultXML['response']['lname'];
		$sEmail	= $aResultXML['response']['email'];
		//$sUrl	= $aResultXML['response']['url'];
		$sRedirectUrl= $_REQUEST['redirect_url'];


		$sSessionId = authentication::create_session(array('email'=>$sEmail, 'uid'=>$iUid));

		setcookie("email", $sEmail, $sExpTime, "/",DOMAIN);
		setcookie("session_id", $sSessionId, $sExpTime, "/",DOMAIN);
		setcookie("uid", $iUid,$sExpTime,"/",DOMAIN);
		setcookie("fname", $sFname,$sExpTime,"/",DOMAIN);
		setcookie("lname", $sLname,$sExpTime,"/",DOMAIN); 
		break;
		
		
}
?>
		<script langauge="javascript">			
			window.opener.location.href=<?="'$sRedirectUrl'"?>;					
			window.close();
			</script>
		<?
?>
