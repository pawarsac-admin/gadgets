<?php
		require_once('./include/config.php');
		require_once(CLASSPATH.'DbConn.php');
		require_once(CLASSPATH.'user_review.class.php');
		require_once(CLASSPATH.'brand.class.php');
		require_once(CLASSPATH.'category.class.php');
		require_once(CLASSPATH.'product.class.php');
		require_once(CLASSPATH.'topstories.class.php');

		$dbconn = new DbConn;
		$userreview = new USERREVIEW;
		$brand = new BrandManagement;
		$category = new CategoryManagement;
		$product = new ProductManagement;
		$oTopStories= new TopStories;

		$group_id	= 1;// Top Rated Phones	

		$category_id = $_REQUEST['catid'] ? $_REQUEST['catid'] : SITE_CATEGORY_ID;
		$totalquestion = $_REQUEST['totalquestion'];
		$uname = trim($_REQUEST['uname']);
		$brand_id = $_REQUEST['brand_id'];
		$model_id = $_REQUEST['model_id'];
		$product_id = $_REQUEST['product_id'];
		$user_review_id = $_REQUEST['user_review_id'];
		$startlmit = $_REQUEST['startlimit'];
		$cnt = $_REQUEST['cnt'];

		$result = $userreview->arrGetUserReviewDetails($user_review_id,"","","","",$brand_id,$category_id,$model_id,$product_id,"1",$startlimit,$cnt);
		$cnt = sizeof($result);
		if(!empty($cnt)){
		$xml = "<USER_REVIEW_MASTER>";
		$xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
		for($i=0;$i<$cnt;$i++){
			$brand_id = $result[$i]['brand_id'];
			$model_id = $result[$i]['product_info_id'];
			$product_id = $result[$i]['product_id'];
			$category_id = $result[$i]['category_id'];
			$create_date = $result[$i]['create_date'];
			$result[$i]['create_date'] = date('d/m/Y',strtotime($create_date));
			if(!empty($brand_id)){
				$brand_result = $brand->arrGetBrandDetails($brand_id,$category_id);
				$brand_name = $brand_result[0]['brand_name'];
				$modelArr[] = $brand_name;
			}
			$result[$i]['brand_name'] = $brand_name;
			if(!empty($model_id)){
				$product_result = $product->arrGetProductNameInfo($model_id);
				$model_name = $product_result[0]['product_info_name'];
				if(!empty($model_name)){
					$modelArr[] = $model_name;
				}
			}
			$result[$i]['model_name'] = $model_name;
			$result[$i]['brand_model_name'] = implode(" ",$modelArr);
			if(!empty($product_id)){
				$product_result = $product->arrGetProductDetails($product_id);
				$variant_name = $product_result[0]['variant'];
				$product_name = $product_result[0]['product_name'];
				$product_desc = $product_result[0]['product_desc'];
				$modelArr[] = $product_name;
				$modelArr[] = $variant_name;
			}
			$result[$i]['brand_model_variant_name'] = implode(" ",$modelArr);
			unset($modelArr);

			$result[$i]['variant'] = $variant;
			$result[$i]['product_desc'] = $product_desc;
			$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
			$xml .= "<USER_REVIEW_MASTER_DATA>";
			foreach($result[$i] as $k=>$v){
				$xml .= "<$k><![CDATA[$v]]></$k>";
			}
			$xml .= "</USER_REVIEW_MASTER_DATA>";

		}
		$xml .= "</USER_REVIEW_MASTER>";

		$result = $userreview->arrGetUserQnA('','',$user_review_id,"1");

		$cnt = sizeof($result);

		$xml .= "<USER_REVIEW_ANSWER_MASTER>";
		$xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
		for($i=0;$i<$cnt;$i++){
			unset($que_result);

			$que_id = $result[$i]['que_id'];
			$que_result = $userreview->arrGetQuestions($que_id);

			$result[$i]['quename'] = $que_result[0]['quename'];

			$answer = $result[$i]['answer'];
			$ansArr = explode(",",$answer);

			$ans_result = $userreview->arrGetQueAnswer("",$que_id);

			$anscnt = sizeof($ans_result);

			$xml .= "<USER_REVIEW_ANSWER_MASTER_DATA>";

			$xml .= "<QUE_ANSWER_MASTER>";
			$xml .= "<COUNT><![CDATA[$anscnt]]></COUNT>";

			if($anscnt > 0){
				for($ans=0;$ans<$anscnt;$ans++){
					$ans_id = trim($ans_result[$ans]['ans_id']);
					$html = "";
					$ansCnt = $ansArr[$ans];

					$ans_result[$ans]['selected_answer'] = $ansCnt;

					$ans_result[$ans] = array_change_key_case($ans_result[$ans],CASE_UPPER);
					$xml .= "<QUE_ANSWER_MASTER_DATA>";
					foreach($ans_result[$ans] as $key=>$val){
						$xml .= "<$key><![CDATA[$val]]></$key>";
					}
					$xml .= "</QUE_ANSWER_MASTER_DATA>";
				}
			}
			$xml .= "</QUE_ANSWER_MASTER>";

			$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
			foreach($result[$i] as $k=>$v){
				$xml .= "<$k><![CDATA[$v]]></$k>";
			}
			$xml .= "</USER_REVIEW_ANSWER_MASTER_DATA>";
		}
		$xml .= "</USER_REVIEW_ANSWER_MASTER>";

		$result = $userreview->arrGetUserQnA('','',$user_review_id,"0","1");
		$cnt = sizeof($result);
		$xml .= "<USER_REVIEW_COMMENT_ANSWER_MASTER>";
		for($i=0;$i<$cnt;$i++){
			$que_id = $result[$i]['que_id'];
			$que_result = $userreview->arrGetQuestions($que_id);
			$result[$i]['quename'] = $que_result[0]['quename'];

			$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
			$xml .= "<USER_REVIEW_COMMENT_ANSWER_MASTER_DATA>";
			foreach($result[$i] as $k=>$v){
				$xml .= "<$k><![CDATA[$v]]></$k>";
			}
			$xml .= "</USER_REVIEW_COMMENT_ANSWER_MASTER_DATA>";
		}
		$xml .= "</USER_REVIEW_COMMENT_ANSWER_MASTER>";

		//used to check admin rating.
		$result = $userreview->arrGetAdminOverallGrade($category_id,$brand_id,$product_id,$model_id);
		$cnt = sizeof($result);
		$overallcnt = 0;
		$overallavg = round($result[0]['overallgrade']);

		$cnt = 0;
		if($cnt <= 0){
			$result = $userreview->arrGetOverallGrade($category_id,$brand_id,$product_id,$model_id);
			$overallcnt = $result[0]['totaloverallcnt'];
			$overallavg = round($result[0]['overallavg']);
		}

		$html = "";
		for($grade=1;$grade<=5;$grade++){
			if($grade <= $overallavg){
				$html .= '<img class="vsblStr mlr1"/>';
			}else{
				$html .= '<img class="dsblStr mlr1"/>';
			}
		}
		$xml .= "<OVERALL_AVG_HTML><![CDATA[$html]]></OVERALL_AVG_HTML>";
		$xml .= "<OVERALL_AVG_CNT><![CDATA[$overallavg]]></OVERALL_AVG_CNT>";
		$xml .= "<OVERALL_TOTAL_CNT><![CDATA[$overallcnt]]></OVERALL_TOTAL_CNT>";
		}
		$config_details = get_config_details();

		$topStoriesXML	 = $oTopStories->getStoriesByGroupXML( $group_id , $dbconn );

        $strXML .= "<XML>";
        $strXML .= $config_details;
	$strXML .= "<MSG><![CDATA[$msg]]></MSG>";
        $strXML .= "<SEO_URL><![CDATA[$seo_url]]></SEO_URL>";
        $strXML .= "<SEO_WEB_URL><![CDATA[".SEO_WEB_URL."]]></SEO_WEB_URL>";
        $strXML .= "<SEO_TITLE><![CDATA[$seo_title]]></SEO_TITLE>";
        $strXML .= "<SEO_TAGS><![CDATA[$seo_keywords]]></SEO_TAGS>";
        $strXML .= "<SEO_DESC><![CDATA[$seo_desc]]></SEO_DESC>";
        $strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$category_id]]></SELECTED_CATEGORY_ID>";
	$strXML.= $xml;
	$strXML .= "<OC_ROS_BOTTOM_NORTH_728x90><![CDATA[1]]></OC_ROS_BOTTOM_NORTH_728x90>";
	$strXML .= "<OC_ROS_TOP_RHS_LREC_300x250_1><![CDATA[1]]></OC_ROS_TOP_RHS_LREC_300x250_1>";
	$strXML	.= "<TOP_RATED>".$topStoriesXML."</TOP_RATED>";
	$strXML.= "</XML>";

	$strXML = mb_convert_encoding($strXML, "UTF-8");

	if($_REQUEST['debug']==1){ header('Content-type: text/xml');echo $strXML;exit; }

        $doc = new DOMDocument();
        $doc->loadXML($strXML);
        $doc->saveXML();

	    $xslt = new xsltProcessor;
        $xsl = DOMDocument::load('xsl/view-user-review.xsl');
        $xslt->importStylesheet($xsl);
        print $xslt->transformToXML($doc);
?>
