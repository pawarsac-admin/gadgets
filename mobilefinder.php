<?php
	error_reporting(E_ALL);
	require_once('include/config.php');
	require_once(CLASSPATH.'DbConn.php');
	require_once(CLASSPATH.'topstories.class.php');
	require_once(CLASSPATH.'brand.class.php');
	require_once(CLASSPATH.'category.class.php');
	require_once(CLASSPATH.'pivot.class.php');
	require_once(CLASSPATH.'feature.class.php');
	require_once(CLASSPATH.'product.class.php');

	$dbconn			= new DbConn;
	$oTopStories	= new TopStories;
	$brand			= new BrandManagement;
	$category		= new CategoryManagement;
	$pivot			= new PivotManagement;
	$feature		= new FeatureManagement;
	$product		= new ProductManagement;
	$group_id		= 1;// Top Rated Phones	

	$category_id	= $_REQUEST['catid'] ? $_REQUEST['catid'] : SITE_CATEGORY_ID;
	// search widget start
		require_once(BASEPATH.'searchwidget.php');
	// search widget end

	$config_details	 = get_config_details();
	$topStoriesXML	 = $oTopStories->getStoriesByGroupXML( $group_id , $dbconn );
	$bgrFavoritesXML = $oTopStories->getStoriesByGroupXML( 2 , $dbconn ); // BGR Favorites
	$hotDealsXML	 = $oTopStories->getStoriesByGroupXML( 3 , $dbconn ); // Hot Deals

	$strXML			 = "<XML>";
	$strXML			.= $config_details;
	$strXML	.= "<SEO_PHONE_FINDER_PAGE_TITLE><![CDATA[".SEO_PHONE_FINDER_PAGE_TITLE."]]></SEO_PHONE_FINDER_PAGE_TITLE>";
	$strXML	.= "<SEO_PHONE_FINDER_PAGE_META_DESC><![CDATA[".SEO_PHONE_FINDER_PAGE_META_DESC."]]></SEO_PHONE_FINDER_PAGE_META_DESC>";
	$strXML	.= "<SEO_PHONE_FINDER_PAGE_KEWWORDS><![CDATA[".SEO_PHONE_FINDER_PAGE_KEWWORDS."]]></SEO_PHONE_FINDER_PAGE_KEWWORDS>";
	$strXML			.= $xml;
	$strXML			.= "<TOP_RATED>".$topStoriesXML."</TOP_RATED>";
	$strXML			.= "<HOT_DEALS>".$hotDealsXML."</HOT_DEALS>";
	$strXML			.= "<BGR_FAVORITES>".$bgrFavoritesXML."</BGR_FAVORITES>";
	$strXML			.= "<SELECTED_FEATURE_ID>1</SELECTED_FEATURE_ID>";
	$strXML			.= "<SELECTED_NAV_TAB>2</SELECTED_NAV_TAB>";
	$strXML			.= "</XML>";

	if( $_GET['debug'] == 1 ){
		header('content-type:text/xml');
		echo $strXML;
		die;
	}

	$doc = new DOMDocument();
	$doc->loadXML($strXML);
	$doc->saveXML();
	$xslt = new xsltProcessor;
	$xsl = DOMDocument::load('xsl/mobilefinder.xsl');
	$xslt->importStylesheet($xsl);
	print $xslt->transformToXML($doc);
?>
