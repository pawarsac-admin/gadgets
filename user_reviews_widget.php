<?php
	ini_set('display_errors',1); error_reporting('E_ALL');
	// include
	require_once('./include/config.php');
	require_once(CLASSPATH.'DbConn.php');
	require_once(CLASSPATH.'DbConn.php');
	require_once(CLASSPATH.'brand.class.php');
	require_once(CLASSPATH.'category.class.php');
	require_once(CLASSPATH.'product.class.php');
	require_once(CLASSPATH.'article.class.php');
	require_once(CLASSPATH.'reviews.class.php');
	require_once(CLASSPATH.'pager.class.php');
	require_once(CLASSPATH.'user.class.php');
	require_once(CLASSPATH.'user_review.class.php');
	// object declaration
	$dbconn = new DbConn;
	$oBrand = new BrandManagement;
	$category = new CategoryManagement;
	$oProduct = new ProductManagement;
	$oArticle = new article;
	$oReview = new reviews;	
	$obj_user = new user;
	$userreview = new USERREVIEW;

	// param
	$category_id = $_REQUEST['catid'] ? $_REQUEST['catid'] : SITE_CATEGORY_ID;
	$type=$_REQUEST['type'];
	$type='user_reviews';

	$model_arr = $userreview->arrGetUserReviewWidget("","","",$category_id,"1","0","4");
	$aproduct_name_ids="";
	for($i=0;$i<sizeof($model_arr);$i++){
		$prod_name_id=$model_arr[$i]["product_info_id"];	
		$aproduct_name_ids.=$prod_name_id.",";
	}
	
	/*$amodels=array("Figo","Swift","Alto","City","Beat","i10");

	for($i=0;$i<sizeof($amodels);$i++){
		$prod_info_name = $amodels[$i];
		$res = $oProduct->arrGetProductNameInfo("",$category_id,"",$prod_info_name,"1","","");
		$prod_name_id=$res["0"]["product_name_id"];
		$aproduct_name_ids.=$prod_name_id.",";
	}*/	
	$aproduct_name_ids = substr($aproduct_name_ids, 0, -1);

	if($type=='user_reviews'){
		
		$resRev=$userreview->getDistinctModelUserReviews("","","","","","",$category_id,$aproduct_name_ids,"","1",0,4);

		$aproduct_name_ids_arr = explode(",",$aproduct_name_ids);
		$resultRev = Array();
		for($i=0; $i<sizeof($aproduct_name_ids_arr);$i++){
			$aproduct_name_id = $aproduct_name_ids_arr[$i];
			for($j=0;$j<sizeof($resRev);$j++){
				if($aproduct_name_id == $resRev[$j]["product_info_id"]){
					array_push($resultRev , $resRev[$j]);
				}
			}	
		}
		$resultCnt=sizeof($resultRev);
		
		$xml .= "<MODEL_USER_REVIEW_MASTER>";
		for($ii=0;$ii<$resultCnt;$ii++){
			$product_info_name=$resultRev[$ii]['product_info_name'];

			$product_name_id=$resultRev[$ii]['product_info_id'];

			$res = $oProduct->arrGetProductNameInfo($product_name_id,$category_id,"","","1","","");
			$image_path=$res["0"]["image_path"];
			if(!empty($image_path)){
				$image_path = resizeImagePath($image_path,"87X65",$aModuleImageResize,$video_img_id);
				$image_path = $image_path ? CENTRAL_IMAGE_URL.$image_path : '';
			}
			$product_brand_id=$resultRev[$ii]['brand_id'];
			$result_count=$userreview->arrGetUserReviewDetailsCount("","","","","","",$category_id,$product_name_id);
			
				if(!empty($product_brand_id)){
					$brand_result = $oBrand->arrGetBrandDetails($product_brand_id,$category_id);
					$seo_brand_name = $brand_result[0]['brand_name'];				
					//$modelArr[] = $brand_name;
				}
				//user review list
				$result = $userreview->arrGetUserReviewDetails("","","","","","",$category_id,$product_name_id,"","1",0,1,'order by create_date desc');
			
				$cnt = sizeof($result);
				$xml .="<MODEL_USER_REVIEW_DATA>";
				$xml .= "<MODEL_NAME><![CDATA[".$product_info_name."]]></MODEL_NAME>";
				$xml .= "<IMAGE_PATH><![CDATA[".$image_path."]]></IMAGE_PATH>";
				$xml .= "<BRAND_NAME><![CDATA[".$seo_brand_name."]]></BRAND_NAME>";
				$xml .= "<DISP_NAME><![CDATA[".$seo_brand_name." ".$product_info_name."]]></DISP_NAME>";
				if(!empty($result_count)){
					//print_r($result_count); echo "<br>";
					$xml .= "<REVCOUNT><![CDATA[".$result_count."]]></REVCOUNT>";
				}

				$seo_brand_name = html_entity_decode($seo_brand_name,ENT_QUOTES);
				$seo_brand_name = removeSlashes($seo_brand_name);
				$seo_brand_name = seo_title_replace($seo_brand_name);
			
				$product_info_name = html_entity_decode($product_info_name,ENT_QUOTES);
				$product_info_name = removeSlashes($product_info_name);
				$product_info_name = seo_title_replace($product_info_name);
			
				//seo user review
				unset($seoTitleArr);
				$seoTitleArr[] = SEO_WEB_URL;
				$seoTitleArr[] = $seo_brand_name."-cars";
				$seoTitleArr[] = $seo_brand_name."-".$product_info_name;;
				$seoTitleArr[] = "reviews-ratings";
				$seoTitleArr[] = SEO_CARS_MODEL_REVIEWS; 
				$seoTitleArr[] = $product_info_name;
				$seoTitleArr[] = $product_name_id; 
				$review_seo_url = implode("/",$seoTitleArr);
				$xml .= "<REVIEW_SEO_URL><![CDATA[".$review_seo_url."]]></REVIEW_SEO_URL>";

				$xml .= "<USER_REVIEW_MASTER>";
				$xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
				for($i=0;$i<$cnt;$i++){
					$user_review_id = $result[$i]['user_review_id'];
					$brand_id = $result[$i]['brand_id'];
					$model_id = $result[$i]['product_info_id'];
					$product_id = $result[$i]['product_id'];
					$category_id = $result[$i]['category_id'];
					$create_date = $result[$i]['create_date'];
					$result[$i]['create_date'] = date('d/m/Y',strtotime($create_date));
					if(!empty($brand_id)){
						$brand_result = $oBrand->arrGetBrandDetails($brand_id,$category_id);
						$brand_name = $brand_result[0]['brand_name'];				
						$modelArr[] = $brand_name;
					}
					$result[$i]['brand_name'] = $brand_name;
					if(!empty($model_id)){
						$product_result = $oProduct->arrGetProductNameInfo($model_id);
						$model_name = $product_result[0]['product_info_name'];
						$modelArr[] = $model_name;
					}			
					$result[$i]['model_name'] = $model_name;
					$result[$i]['brand_model_name'] = implode(" ",$modelArr);
					if(!empty($product_id)){
						$product_result = $oProduct->arrGetProductDetails($product_id);
						$variant = $product_result[0]['variant'];
						$modelArr[] = $variant_name;
					}
					$result[$i]['brand_model_variant_name'] = implode(" ",$modelArr);
					unset($modelArr);
					$result[$i]['variant'] = $variant;
					$xml .= "<USER_REVIEW_MASTER_DATA>";
					$ratingresult = $userreview->arrGetUserQnA('','',$user_review_id,"1");
					$ratingcnt = sizeof($ratingresult);
					$xml .= "<USER_RATING_MASTER>";
					for($rating=0;$rating<$ratingcnt;$rating++){
						$que_id = $ratingresult[$rating]['que_id'];
						$que_result = $userreview->arrGetQuestions($que_id);				
						$ratingresult[$rating]['quename'] = $que_result[0]['quename'];
						$answer = $ratingresult[$rating]['answer'];
						$ansArr = explode(",",$answer);
						$gradeCnt = $ratingresult[$rating]['grade'];
						$html = "";
						for($grade=1;$grade<=5;$grade++){
							if($grade <= $gradeCnt){
								$html .= '<img src="'.IMAGE_URL.'spacer.gif" class="vsblStr"/>';
							}else{
								$html .= '<img src="'.IMAGE_URL.'spacer.gif" class="dsblStr"/>';
							}
						}
						$ratingresult[$rating]['grade'] = $html;				
						$xml .= "<USER_RATING_MASTER_DATA>";
						$ratingresult[$rating] = array_change_key_case($ratingresult[$rating],CASE_UPPER);
						foreach($ratingresult[$rating] as $k=>$v){
							$xml .= "<$k><![CDATA[$v]]></$k>";
						}
						$xml .= "</USER_RATING_MASTER_DATA>";
					}
					$xml .= "</USER_RATING_MASTER>";
					$reviewresult = $userreview->arrGetUserQnA('','',$user_review_id,"0","1","0","1"); // for comment
					$reviewcnt = sizeof($reviewresult);
					$xml .= "<USER_REVIEW_COMMENT_ANSWER_MASTER>";
					for($review=0;$review<$reviewcnt;$review++){
						$que_id = $reviewresult[$review]['que_id'];
						$answer = $reviewresult[$review]['answer'];
						$answer = removeSlashes($answer);                
						$answer = html_entity_decode($answer,ENT_QUOTES);
						if(strlen($answer)>100){ $answer = getCompactString($answer, 95).' ...'; }
							$reviewresult[$review]['answer'] = $answer;
							$que_result = $userreview->arrGetQuestions($que_id);			
							$reviewresult[$review]['quename'] = $que_result[0]['quename'];
							$reviewresult[$review] = array_change_key_case($reviewresult[$review],CASE_UPPER);
							$xml .= "<USER_REVIEW_COMMENT_ANSWER_MASTER_DATA>";
							foreach($reviewresult[$review] as $k=>$v){
								$xml .= "<$k><![CDATA[$v]]></$k>";
							}
							$xml .= "</USER_REVIEW_COMMENT_ANSWER_MASTER_DATA>";
						}
						$xml .= "</USER_REVIEW_COMMENT_ANSWER_MASTER>";
						$brand_name = html_entity_decode($brand_name,ENT_QUOTES);
						$brand_name = removeSlashes($brand_name);
						$brand_name = seo_title_replace($brand_name);
						$model = html_entity_decode($model_name,ENT_QUOTES);
						$model = removeSlashes($model_name);
						$model = seo_title_replace($model_name);
						$variant = html_entity_decode($variant,ENT_QUOTES);
						$variant = removeSlashes($variant);
						$variant = seo_title_replace($variant);
						//seo user review
						unset($seoTitleArr);
						$seoTitleArr[] = SEO_WEB_URL;
						$seoTitleArr[] = $brand_name."-cars";
						$seoTitleArr[] = $brand_name."-".$model;;
						$seoTitleArr[] = "reviews-ratings";
						$seoTitleArr[] = SEO_CARS_MODEL_FULLREVIEWS; 
						$seoTitleArr[] = $model;
						$seoTitleArr[] = $user_review_id;
						$result[$i]['user_review_seo_url'] = implode("/",$seoTitleArr);
						$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
						foreach($result[$i] as $k=>$v){
							$xml .= "<$k><![CDATA[$v]]></$k>";
						}
						$xml .= "</USER_REVIEW_MASTER_DATA>";
					}
					$xml .= "</USER_REVIEW_MASTER>";
					//used to check admin rating.
					$result = $userreview->arrGetAdminOverallGrade($category_id,$brand_id,'0',$model_id);
					$cnt = sizeof($result);
					$overallcnt = 0;
					$overallavg = round($result[0]['overallgrade']);
					if($cnt <= 0){
						$result = $userreview->arrGetOverallGrade($category_id,$brand_id,'0',$model_id);
						$overallcnt = $result[0]['totaloverallcnt'];
						$overallavg = round($result[0]['overallavg']);
					}
					$html = "";
					for($grade=1;$grade<=5;$grade++){
						if($grade <= $overallavg){
							$html .= '<img src="'.IMAGE_URL.'spacer.gif" class="vsblStr"/>';
						}else{
							$html .= '<img src="'.IMAGE_URL.'spacer.gif" class="dsblStr"/>';
						}
					}

					
					$xml .= "<OVERALL_AVG_HTML><![CDATA[$html]]></OVERALL_AVG_HTML>";
					$xml .= "<OVERALL_AVG_CNT><![CDATA[$overallavg]]></OVERALL_AVG_CNT>";
					$xml .= "<OVERALL_TOTAL_CNT><![CDATA[$overallcnt]]></OVERALL_TOTAL_CNT>";
					$xml .= "</MODEL_USER_REVIEW_DATA>";
				}
		$xml .= "</MODEL_USER_REVIEW_MASTER>";
				
	}
//die();
	$config_details = get_config_details();
	
	$strXML .= "<XML>";
	$strXML .= "<TYPE><![CDATA[$type]]></TYPE>";
	$strXML .= "<BREAD_CRUMB><![CDATA[$breadcrumb]]></BREAD_CRUMB>";
	$strXML .= "<SEO_WEB_URL><![CDATA[".SEO_WEB_URL."]]></SEO_WEB_URL>";
	$strXML .= "<SEO_TITLE><![CDATA[$seo_title]]></SEO_TITLE>";
	$strXML .= "<SEO_TAGS><![CDATA[$seo_keywords]]></SEO_TAGS>";
	$strXML .= "<SEO_DESC><![CDATA[$seo_desc]]></SEO_DESC>";
	$strXML .= "<MODEL_BRAND_NAME><![CDATA[$sModelBrandName]]></MODEL_BRAND_NAME>";
	$strXML .= "<MODEL_NAME><![CDATA[$product_info_name]]></MODEL_NAME>";
	$strXML .= "<STARTLIMIT><![CDATA[$startlimit]]></STARTLIMIT>";
	$strXML .= "<CNT><![CDATA[$limitcnt]]></CNT>";
	$strXML .= "<CURRENT_PAGE><![CDATA[$curpage]]></CURRENT_PAGE>";
	$strXML .= "<SELECTEDTABID><![CDATA[$tab_id]]></SELECTEDTABID>";
	$strXML .= $config_details;
	$strXML .= $sFeaturedReviewsDetXml;
	$strXML .= $sMoreReviewsDetXml;
	$strXML .= $xml;
	$strXML .= "</XML>";

	if($_GET['debug']==1) { header('Content-type: text/xml');echo $strXML;exit; }
	$doc = new DOMDocument();
	$doc->loadXML($strXML);
	$doc->saveXML();

	$xslt = new xsltProcessor;
	$xsl = DOMDocument::load('xsl/user_reviews_widget.xsl');

	$xslt->importStylesheet($xsl);
	print $xslt->transformToXML($doc);
?>
