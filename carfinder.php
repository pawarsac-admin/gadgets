<?php	
	require_once('./include/config.php');
	require_once(CLASSPATH.'DbConn.php');
	require_once(CLASSPATH.'brand.class.php');
	require_once(CLASSPATH.'category.class.php');
	require_once(CLASSPATH.'pivot.class.php');
	require_once(CLASSPATH.'feature.class.php');
	require_once(CLASSPATH.'product.class.php');
	require_once(CLASSPATH.'PageNavigator.php');
	require_once(CLASSPATH.'Utility.php');
	require_once(CLASSPATH.'user_review.class.php');
	require_once(CLASSPATH.'videos.class.php');


	$dbconn = new DbConn;
	$brand = new BrandManagement;
	$category = new CategoryManagement;
	$pivot = new PivotManagement;
	$feature = new FeatureManagement;
	$product = new ProductManagement;
	$userreview = new USERREVIEW;
	$videoGallery = new videos();

	//print "<pre>";print_r($_REQUEST); die();
	$category_id = $_REQUEST['catid'] ? $_REQUEST['catid'] : SITE_CATEGORY_ID;
	$tab_id = $_REQUEST['fid'] ? $_REQUEST['fid'] : 1;
	
	$startlimit = $_REQUEST['startlimit'];
	$endlimit = $_REQUEST['cnt'];
	$unsel_all_brand = $_REQUEST['unsel_all_brand'];
	$selectedbrandArr = (!$unsel_all_brand) ? $_REQUEST['branddetails'] : '';
	$variant_id = "1"; // important for price search.ie. ex-showroom price.
	
	$request_uri = $_SERVER['REQUEST_URI'];
	$pos = strpos($request_uri,'?');
	if($pos > 0){
		$request_uri = substr($request_uri,0,$pos);
	}
	
	if(empty($selectedbrandArr)){
		//for brand.
		$brand_exp = "/brand\-([^\/]+)/";
		if(preg_match_all($brand_exp,$request_uri,$matches,PREG_SET_ORDER)){
			$brandnamesArr = explode("+.",$matches[0][1]);
			foreach($brandnamesArr as $k=>$brand_name){
				$brand_name = trim($brand_name);

				$result = $brand->arrGetBrandDetails("",$category_id,"1","","",urldecode($brand_name));

				$brand_id = $result[0]['brand_id'];
				if(!empty($brand_id)){
					$selectedbrandArr[] = $result[0]['brand_id'];
				}
				unset($brand_id);unset($result);
			}
			
		}
	}
	
	//print_r($selectedbrandArr);
	$selectedfeatureArr = $_REQUEST['featuredetails'];	
	
	if(empty($selectedfeatureArr)){
		//for fuel pivot
		$fuel_exp = "/fuel\-([^\/]+)/";
		if(preg_match_all($fuel_exp,$request_uri,$matches,PREG_SET_ORDER)){
			$featurenamesArr = explode("+.",$matches[0][1]);
			foreach($featurenamesArr as $k=>$feature_name){		
				$feature_name = trim($feature_name);
				$result = $feature->arrGetPivotFeatureDetails(urldecode($feature_name),$category_id);
				$feature_id = $result[0]['feature_id'];
				if(!empty($feature_id)){
					$selectedfeatureArr[] = $result[0]['feature_id'];
				}
				unset($feature_id);unset($result);
			}	
			unset($featurenamesArr);
		}
		$impfeature_exp = "/impfeature\-([^\/]+)/";
		if(preg_match_all($impfeature_exp,$request_uri,$matches,PREG_SET_ORDER)){
			//for important feature pivot
			$featurenamesArr = explode("+.",$matches[0][1]);
			foreach($featurenamesArr as $k=>$feature_name){		
				$feature_name = trim($feature_name);
				$result = $feature->arrGetPivotFeatureDetails(urldecode($feature_name),$category_id);
				$feature_id = $result[0]['feature_id'];
				if(!empty($feature_id)){
					$selectedfeatureArr[] = $result[0]['feature_id'];
				}
				unset($feature_id);unset($result);
			}	
			unset($featurenamesArr);
		}
		
		$style_exp = "/style\-([^\/]+)/";
		//print "<pre>";print_r($request_uri);
		if(preg_match_all($style_exp,$request_uri,$matches,PREG_SET_ORDER)){
			//for body style pivot.
			$featurenamesArr = explode("+.",$matches[0][1]);
			foreach($featurenamesArr as $k=>$feature_name){		
				$feature_name = trim($feature_name);
				$result = $feature->arrGetPivotFeatureDetails(urldecode($feature_name),$category_id);
				$feature_id = $result[0]['feature_id'];
				if(!empty($feature_id)){
					$selectedfeatureArr[] = $result[0]['feature_id'];
				}
				unset($feature_id);unset($result);
			}	
			unset($featurenamesArr);
		}
		$transmission_exp = "/transmission\-([^\/]+)/";
		if(preg_match_all($transmission_exp,$request_uri,$matches,PREG_SET_ORDER)){
			//for transmission pivot.
			$featurenamesArr = explode("+.",$matches[0][1]);
			foreach($featurenamesArr as $k=>$feature_name){		
				$feature_name = trim($feature_name);
				$result = $feature->arrGetPivotFeatureDetails(urldecode($feature_name),$category_id);
				$feature_id = $result[0]['feature_id'];
				if(!empty($feature_id)){
					$selectedfeatureArr[] = $result[0]['feature_id'];
				}
				unset($feature_id);unset($result);
			}	
			unset($featurenamesArr);
		}
		$seating_exp = "/seating\-([^\/]+)/";
		if(preg_match_all($seating_exp,$request_uri,$matches,PREG_SET_ORDER)){
			//for seating capacity pivot.
			$featurenamesArr = explode("+.",$matches[0][1]);
			foreach($featurenamesArr as $k=>$feature_name){		
				$feature_name = trim($feature_name);
				$result = $feature->arrGetPivotFeatureDetails(urldecode($feature_name),$category_id);
				$feature_id = $result[0]['feature_id'];
				if(!empty($feature_id)){
					$selectedfeatureArr[] = $result[0]['feature_id'];
				}
				unset($feature_id);unset($result);
			}	
			unset($featurenamesArr);
		}
	}

//	print_r($selectedfeatureArr);

	if(sizeof($selectedfeatureArr) > 0){		
		foreach($selectedfeatureArr as $feature_id){
			if(!empty($feature_id)){
				$result = $feature->arrGetFeatureDetails($feature_id,$category_id);
				$seoUrlArr[] = $result[0]['feature_name'];
				$seoFeatureArr[] = rawurlencode($result[0]['feature_name']);
				unset($result);
			}
		}
	}
	

	$mn_price = $_REQUEST['mnprice'];
	$mn_price_unit = $_REQUEST['mnpriceunit'];

	$mx_price = $_REQUEST['mxprice'];
	$mx_price_unit = $_REQUEST['mxpriceunit'];

	if(empty($mn_price) && empty($mx_price)){
		//for price.
		$price_exp = "/price\-([^\/]+)/";
		if(preg_match_all($price_exp,$request_uri,$matches,PREG_SET_ORDER)){


			$priceArr = explode("-",$matches[0][1]);
			$request_mnprice = $priceArr[0];
			if(!empty($request_mnprice)){
				$minprice = $pricelistinLakhArr[$request_mnprice];
				if(!empty($minprice)){
					$_REQUEST['mnprice'] = $minprice;
					$_REQUEST['mnpriceunit'] = 'thousand';
				}else{
					$minprice = $pricelistinCroreArr[$request_mnprice];
					if(!empty($minprice)){
						$_REQUEST['mnprice'] = $minprice;
						$_REQUEST['mnpriceunit'] = 'lakh';
					}
				}				
			}

			$request_mx_price = $priceArr[1];
			if(!empty($request_mx_price)){
				$mx_price = $pricelistinLakhArr[$request_mx_price];
				if(!empty($mx_price)){
					$_REQUEST['mxprice'] = $mx_price;
					$_REQUEST['mxpriceunit'] = 'thousand';
				}else{
					$mx_price = $pricelistinCroreArr[$request_mx_price];
					if(!empty($mx_price)){
						$_REQUEST['mxprice'] = $mx_price;
						$_REQUEST['mxpriceunit'] = 'lakh';
					}
				}				
			}
		}
	}
	
	$mn_price = $_REQUEST['mnprice'] ? $_REQUEST['mnprice'] : '1';
	$mn_price_unit = $_REQUEST['mnpriceunit'] ? $_REQUEST['mnpriceunit'] : 'thousand';

	$mx_price = $_REQUEST['mxprice'] ? $_REQUEST['mxprice'] : '5';
	$mx_price_unit = $_REQUEST['mxpriceunit'] ? $_REQUEST['mxpriceunit'] : 'lakh';


	$seopriceArr[] = $mn_price." ".$mn_price_unit;
	$seopriceArr[] = $mx_price." ".$mx_price_unit;

	if(strtolower($mn_price_unit) == 'lakh'){
		$mn_mult = 100000;
	}else{
		$mn_mult = 1000;
	}
	if(strtolower($mx_price_unit) == 'lakh'){
		$mx_mult = 100000;
	}else{
		$mx_mult = 1000;
	}

	$startprice = $mn_price*$mn_mult;
	$endprice = $mx_price*$mx_mult;


	//require_once('./product_search_min_max_price.php');
	
	$seoUrlArr[] = WEB_URL.SEO_CAR_FINDER;
	if(!empty($startprice)){
		$priceArr[] = $startprice;
	}
	if(!empty($endprice)){
		$priceArr[] = $endprice;
	}
	if(sizeof($priceArr) > 0){
		$seoUrlArr[] = implode("-",$priceArr);
	}
	if(sizeof($selectedbrandArr) > 0){
		
		foreach($selectedbrandArr as $brand_id){
			if(!empty($brand_id)){
				$result = $brand->arrGetBrandDetails($brand_id,$category_id);
				$seoUrlArr[] = $result[0]['brand_name'];
				$seoBrandArr[] = rawurlencode($result[0]['brand_name']);
			}
			unset($result);

		}
	}
	
	if(sizeof($seoBrandArr) > 0){
		$seotitleArr[] = implode(",",$seoBrandArr);
	}
	
	if(sizeof($seoFeatureArr) > 0){
		$seotitleArr[] = 'Cars with '.implode(",",$seoFeatureArr);
	}
	
	$cnt_seo=count($seoUrlArr);

	$seo_url = implode("/",$seoUrlArr);
	
	unset($seoUrlArr[0]);
	
	$seo_js = implode(",",$seoUrlArr);
	
	$seotitle = implode(" ",$seotitleArr);
	unset($seotitleArr);
	if(!empty($seotitle)){
		$seotitleArr[] = "Compare ".$seotitle;
	}
	if(sizeof($seotitleArr) > 0){		
		$seotitleArr[] = "Price Rs.".implode(" - ",$seopriceArr);
		$seotitleArr[] = "on ".SEO_DOMAIN;
	}else{
		$seotitleArr[] = "Mobile Finder - Price Rs.".implode(" - ",$seopriceArr);
		$seotitleArr[] = "on ".SEO_DOMAIN;
	}
	
	if(sizeof($seotitleArr) > 0){
		$seo_title = implode(" | ",$seotitleArr);
	}else{
		$seo_title = "Mobile Finder - On Mobiles India | Search Cars, New Mobiles by Brands, Price, Body Style, Mobile Features on ".SEO_DOMAIN;
		
	}
	array_pop($seotitleArr);
	$sub_title = rawurldecode(implode(" ",str_replace(",",", ",$seotitleArr)));
	
	$seo_desc = "Search cars, new mobiles by selecting the searching criteria - Mobile brand, mobile prices in India, body style, mobile features, fuel type, transmission and seating capacity using Mobile Finder of ".SEO_DOMAIN;
	$seo_tags = "car finder, find cars, search car, search mobiles by brand, search mobiles by style, search mobiles by features, find mobiles by brand, find mobiles by style, carfinder, carfinder of OnCars, mobile finder at ".SEO_DOMAIN.".india, find mobiles on OnCars india";
	$breadcrumb = CATEGORY_HOME." All Cars";

	$sortproductBY = $_REQUEST['sortproduct'] ? $_REQUEST['sortproduct'] : 1;
	$sortproductxml = "<SELECTED_SORT_PRODUCT_BY><![CDATA[$sortproductBY]]></SELECTED_SORT_PRODUCT_BY>";

	//$selecteditemArr[] = Array();
	
	if(!empty($category_id)){
		$result = $brand->arrGetBrandDetails("",$category_id,1);
	}
	$cnt = sizeof($result);
	$xml .= "<SELECTED_BRAND_MASTER>";
	$xml .= "</SELECTED_BRAND_MASTER>";
	$xml .= "<BRAND_MASTER>";
	$xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";	
	
	$selectedIndex = "0";
	$isBrandSelected = "0"; //used toggle all brands checkbox.
	for($i=0;$i<$cnt;$i++){
		$brand_id = $result[$i]['brand_id'];
		$brand_name = $result[$i]['brand_name'];
		$short_desc = $result[$i]['short_desc'];
		if(!empty($short_desc)){
			$result[$i]['short_desc'] = html_entity_decode($short_desc,ENT_QUOTES,'UTF-8');
		}
		if(in_array($brand_id,$selectedbrandArr)){
			$result[$i]['selected_brand_id'] = $brand_id;
			$result[$i]['selected_brand_name'] = $brand_name;
			$selecteditemArr[$selectedIndex]['selected_id'] = $brand_id;
			$selecteditemArr[$selectedIndex]['is_brand_select'] = "1";
			$selecteditemArr[$selectedIndex]['selected_type'] = 'checkbox_brand_id_'.$brand_id;
			$selecteditemArr[$selectedIndex]['selected_name'] =$result[$i]['brand_name'];
			$selectedIndex++;
			$isBrandSelected++;
			
		}
		$status = $result[$i]['status'];
		$categoryid = $result[$i]['category_id'];
		if(!empty($categoryid)){
			$category_result = $category->arrGetCategoryDetails($categoryid);
		}
		$category_name = $category_result[0]['category_name'];
		$result[$i]['js_category_name'] = $category_name;
		$result[$i]['category_name'] = html_entity_decode($category_name,ENT_QUOTES,'UTF-8');
		$result[$i]['brand_status'] = ($status == 1) ? 'Active' : 'InActive';
		$result[$i]['create_date'] = date('d-m-Y',strtotime($result[$i]['create_date']));
		$result[$i]['js_brand_name'] = $result[$i]['brand_name'];
		$result[$i]['brand_name'] = html_entity_decode($result[$i]['brand_name'],ENT_QUOTES,'UTF-8');
		$result[$i] = array_change_key_case($result[$i],CASE_UPPER);

		$xml .= "<BRAND_MASTER_DATA>";
			unset($popular_brand);
			$popular_brand = $brand->arrGetPopularBrandDetails("", $brand_id, "", $categoryid, "1",0,2);		
			$popularbrandCnt = sizeof($popular_brand);
			$xml .= "<POPULAR_BRAND_MASTER>";
			$xml .= "<POPULAR_BRAND_COUNT><![CDATA[$popularbrandCnt]]></POPULAR_BRAND_COUNT>";
			for($popular=0;$popular<$popularbrandCnt;$popular++){
				unset($modelresult);unset($brandresult);unset($popular_brand_id);unset($popular_brand_category_id);unset($popular_model_id);
				
				$popular_brand_id = $popular_brand[$popular]['brand_id'];
				$popular_brand_category_id = $popular_brand[$popular]['category_id'];
				$popular_model_id = $popular_brand[$popular]['popular_model_id'];
				if(!empty($popular_brand_id) && !empty($popular_brand_category_id)){
					$brandresult = $brand->arrGetBrandDetails($popular_brand_id,$popular_brand_category_id);
				}
				$popular_brand_name = $brandresult[0]['brand_name'];
				if(!empty($popular_model_id) && !empty($popular_brand_category_id) && !empty($popular_brand_id)){
					$modelresult = $product->arrGetProductNameInfo($popular_model_id,$popular_brand_category_id,$popular_brand_id);				
				}
				$product_info_name = $modelresult[0]['product_info_name'];

				unset($modelnameSeoArr);
				if(!empty($popular_brand_name) && !empty($product_info_name)){					
					
					//get model name and seo url.
					$modelnameSeoArr[] = SEO_WEB_URL;
					$modelnameSeoArr[] = seo_title_replace($popular_brand_name).'-cars';
					
					$modelnameSeoArr[] = seo_title_replace($popular_brand_name).'-'.seo_title_replace($product_info_name);
					$modelnameSeoArr[] = 'Model';
					$modelnameSeoArr[] = seo_title_replace($product_info_name);			
					
					$modelnameSeoArr[] = $popular_model_id;
					$link_model_name = implode(" ",$modelnameArr);
					$seo_model_url =  implode("/",$modelnameSeoArr);
					$modelresult[0]['seo_model_url'] = $seo_model_url;
				}

				$popular_brand[$popular] = array_merge($brandresult[0],$modelresult[0]);
				
				$xml .= "<POPULAR_BRAND_MASTER_DATA>";
				$popular_brand[$popular] = array_change_key_case($popular_brand[$popular],CASE_UPPER);
				foreach($popular_brand[$popular] as $k=>$v){
					$xml .= "<$k><![CDATA[$v]]></$k>";
				}
				$xml .= "</POPULAR_BRAND_MASTER_DATA>";
			}
			$xml .= "</POPULAR_BRAND_MASTER>";

		foreach($result[$i] as $k=>$v){
			$xml .= "<$k><![CDATA[$v]]></$k>";
		}
		$xml .= "</BRAND_MASTER_DATA>";
	}
	$xml .= "</BRAND_MASTER>";
	
	if(!empty($category_id)){
		unset($result);
		$result = $pivot->arrPivotSubGroupDetails("",$category_id,1);
	}
	$cnt = sizeof($result);
	
	for($i=0;$i<$cnt;$i++){
		$plusminusimgstatus = 0;
		$status = $result[$i]['status'];
		$sub_group_id = $result[$i]['sub_group_id'];
		$categoryid = $result[$i]['category_id'];
		$sub_group_name = $result[$i]['sub_group_name'];
		
		if(!empty($categoryid)){
            $category_name = $category_result[0]['category_name'];
            $result[$i]['js_category_name'] = $category_name;
			$result[$i]['category_name'] = html_entity_decode($category_name,ENT_QUOTES,'UTF-8');
			$pivot_result = $pivot->arrGetPivotDetails("",$category_id,"","1",$sub_group_id);
			
			$pivotCnt = sizeof($pivot_result);
			for($j=0;$j<$pivotCnt;$j++){
				
				$pivot_display_id = $pivot_result[$j]['pivot_display_id'];
				if(!empty($pivot_display_id)){
					$pivot_display_type_result = $pivot->arrPivotDisplayDetails($pivot_display_id,"1");
					$pivot_display_type = $pivot_display_type_result[0]['pivot_display_name'];
				}else{
					$pivot_display_type = "checkbox";
				}
				$pivot_group = $pivot_result[$j]['pivot_group'];
				$main_pivot_group = $sub_group_name ;

				$status = $pivot_result[$j]['status'];
				$pivot_desc = $pivot_result[$j]['pivot_desc'];
				if(!empty($pivot_desc)){
					$pivot_result[$j]['pivot_desc'] = html_entity_decode($pivot_desc,ENT_QUOTES,'UTF-8');
				}

				$categoryid = $pivot_result[$j]['category_id'];
				$feature_id = $pivot_result[$j]['feature_id'];
				
				if(!empty($feature_id)){
					$feature_result = $feature->arrGetFeatureDetails($feature_id,$categoryid,"","","1");
					$feature_name = $feature_result[0]['feature_name'];
					$feature_description = $feature_result[0]['feature_description'];
				}

				if(in_array($feature_id,$selectedfeatureArr)){
					$pivot_result[$j]['selected_feature_id'] = $feature_id;
					$selecteditemArr[$selectedIndex]['selected_id'] = $feature_id;
					$selecteditemArr[$selectedIndex]['is_feature_select'] = "1";
					$selecteditemArr[$selectedIndex]['selected_type'] = 'checkbox_feature_id_'.$feature_id;
					$selecteditemArr[$selectedIndex]['selected_name'] = $feature_name;
					$selecteditemArr[$selectedIndex]['selected_feature_group'] = $sub_group_name;
					$selectedIndex++;
					$plusminusimgstatus++;
				}

				$pivot_result[$j]['feature_name'] = $feature_name;
				$pivot_result[$j]['feature_description'] = $feature_description;
				$pivot_result[$j]['pivot_display_type'] = $pivot_display_type;
				
				$pivotresult[$sub_group_id][$pivot_group][] = $pivot_result[$j];
				
				$result[$i]['plus_minus_img_status'] = $plusminusimgstatus;
				foreach($result[$i] as $k=> $v){
					$pivotresult[$sub_group_id][$k] = $v;
				}
				
			}			
		}
	
	}
	
	$groupnodexml .= "<PIVOT_MASTER>";
	if($pivotresult){
		foreach($pivotresult as $maingroupkey => $maingroupval){
			if(is_array($maingroupval)){
				$groupnodexml .= "<PIVOT_MASTER_DATA>";
				foreach($maingroupval as $subgroupkey=>$subgroupval){
					
					if(is_array($subgroupval)){
						$groupnodexml .= "<SUB_PIVOT_MASTER>";
						 foreach($subgroupval as $key => $featuredata){							 
							 $feature_id = $featuredata['feature_id'];
							 $popularfeatureresult = $feature->arrGetPopularFeatureCarDetails("","","","","","",$category_id,"1",0,2,"",$feature_id);
							 $popular_featureCnt = sizeof($popularfeatureresult);
							 $popularfeaturexml = "<POPULAR_FEATURE_MASTER>";
							 $popularfeaturexml .= "<POPULAR_FEATURE_COUNT><![CDATA[$popular_featureCnt]]></POPULAR_FEATURE_COUNT>";
							 for($i=0;$i<$popular_featureCnt;$i++){
								$popular_brand_id = $popularfeatureresult[$i]['brand_id'];
								$popular_brand_category_id = $popularfeatureresult[$i]['category_id'];
								$popular_model_id = $popularfeatureresult[$i]['model_id'];
								if(!empty($popular_brand_id) && !empty($popular_brand_category_id)){
									$brandresult = $brand->arrGetBrandDetails($popular_brand_id,$popular_brand_category_id);
								}
								$popular_brand_name = $brandresult[0]['brand_name'];
								if(!empty($popular_model_id) && !empty($popular_brand_category_id) && !empty($popular_brand_id)){
									$modelresult = $product->arrGetProductNameInfo($popular_model_id,$popular_brand_category_id,$popular_brand_id);				
								}
								$product_info_name = $modelresult[0]['product_info_name'];
								unset($modelnameSeoArr);
								if(!empty($popular_brand_name) && !empty($product_info_name)){					
									
									//get model name and seo url.
									$modelnameSeoArr[] = SEO_WEB_URL;
									$modelnameSeoArr[] = seo_title_replace($popular_brand_name).'-cars';
									
									$modelnameSeoArr[] = seo_title_replace($popular_brand_name).'-'.seo_title_replace($product_info_name);
									$modelnameSeoArr[] = 'Model';
									$modelnameSeoArr[] = seo_title_replace($product_info_name);			
									
									$modelnameSeoArr[] = $popular_model_id;
									$link_model_name = implode(" ",$modelnameArr);
									$seo_model_url =  implode("/",$modelnameSeoArr);
									$modelresult[0]['seo_model_url'] = $seo_model_url;
								}

								$popularfeatureresult[$i] = array_merge($brandresult[0],$modelresult[0]);
								$popularfeaturexml .= "<POPULAR_FEATURE_MASTER_DATA>";
								$popularfeatureresult[$i] = array_change_key_case($popularfeatureresult[$i],CASE_UPPER);
								foreach($popularfeatureresult[$i] as $k=>$v){
									$popularfeaturexml .= "<$k><![CDATA[$v]]></$k>";
								}
								$popularfeaturexml .= "</POPULAR_FEATURE_MASTER_DATA>";
							 }
							$popularfeaturexml .= "</POPULAR_FEATURE_MASTER>";
							if(is_array($featuredata)){
								$groupnodexml .= "<SUB_PIVOT_MASTER_DATA>";
								$groupnodexml .= $popularfeaturexml;
								$featuredata = array_change_key_case($featuredata,CASE_UPPER);
								foreach($featuredata as $featurekey => $featureval){
									$groupnodexml .= "<$featurekey><![CDATA[$featureval]]></$featurekey>";
								}	
								$groupnodexml .= "</SUB_PIVOT_MASTER_DATA>";
							}else{
								$key = strtoupper($key);
								$groupnodexml .= "<$key><![CDATA[$featuredata]]></$key>";
							}
						}
						$groupnodexml .= "</SUB_PIVOT_MASTER>";
					}else{
						$subgroupkey = strtoupper($subgroupkey);
						$groupnodexml .= "<$subgroupkey><![CDATA[$subgroupval]]></$subgroupkey>";
					}


				}
				$groupnodexml .= "</PIVOT_MASTER_DATA>";
			}
		}
	}	
	$groupnodexml .= "</PIVOT_MASTER>";
	
	// Start Pagination constants.
	define("PERPAGE", 10);
	define("OFFSET", "pageno");
	define("STARTPAGESHOWN",10);
	define("MAXPAGESHOWN",10);
	$offset = $_REQUEST[OFFSET] ? $_REQUEST[OFFSET] : 1;
	if(!isset($offset)){
		$totaloffset = 0;		
	}else{
		$totaloffset = $offset * PERPAGE;
		$totalrecords = $totaloffset;
	}
	$pagelimitArr = arrGetPageLimit($offset,PERPAGE);

	if(empty($startlimit)){
		$startlimit = $pagelimitArr['startlimit'];
	}
	if(empty($endlimit)){
		$endlimit = $pagelimitArr['recordperpage'];
	}   

	if(!empty($category_id)){		
		unset($result);		
		$multikey = "variant_value";
		switch($sortproductBY){
			case '1':	
				
				if(sizeof($selectedbrandArr) <= 0 && sizeof($selectedfeatureArr) <= 0){	
					
					$orderby=' order by PRICE_VARIANT_VALUES.variant_value asc '; 
					$count_result = $product->arrGetProductByPriceAscCount("",$category_id,"",'1',$startprice,$endprice,$variant_id,$startlimit,$endlimit,$orderby);

					$result = $product->arrGetProductByPriceAsc("",$category_id,"",'1',$startprice,$endprice,"1",$startlimit,$endlimit);
					//print "<pre>"; print_r($result); 
				}else{
					$count_result = $product->searchProductCount($category_id,$selectedbrandArr,"",$selectedfeatureArr,"1",$startprice,$endprice,$variant_id);
					$result = $product->searchProduct($category_id,$selectedbrandArr,"",$selectedfeatureArr,"1",$startprice,$endprice,$variant_id,$startlimit,$endlimit,"PRICE_VARIANT_VALUES.variant_value asc");
					
				}
				break;
			case '2':
				if(sizeof($selectedbrandArr) <= 0 && sizeof($selectedfeatureArr) <= 0){	
					$count_result = $product->arrGetProductByPriceAscCount("",$category_id,"","1",$startprice,$endprice,$variant_id);

					$result = $product->arrGetProductByPriceDesc("",$category_id,"",'1',$startprice,$endprice,"1",$startlimit,$endlimit);
				}else{
					$count_result = $product->searchProductCount($category_id,$selectedbrandArr,"",$selectedfeatureArr,"1",$startprice,$endprice,$variant_id);
					$result = $product->searchProduct($category_id,$selectedbrandArr,"",$selectedfeatureArr,"1",$startprice,$endprice,$variant_id,$startlimit,$endlimit,"PRICE_VARIANT_VALUES.variant_value desc");
				}
				break;
			case '3':
				
				if(sizeof($selectedbrandArr) <= 0 && sizeof($selectedfeatureArr) <= 0){
					$count_result = $product->arrGetProductByPriceAscCount("",$category_id,"","1",$startprice,$endprice,$variant_id);			
					$result = $product->arrGetProductByNameAsc("",$category_id,"",'1',$startprice,$endprice,"1",$startlimit,$endlimit);			
				}else{
					$count_result = $product->searchProductCount($category_id,$selectedbrandArr,"",$selectedfeatureArr,"1",$startprice,$endprice,$variant_id);
					$result = $product->searchProduct($category_id,$selectedbrandArr,"",$selectedfeatureArr,"1",$startprice,$endprice,$variant_id,$startlimit,$endlimit,"PRODUCT_MASTER.product_name asc");
				}
				break;
			case '4':	
				
				if(sizeof($selectedbrandArr) <= 0 && sizeof($selectedfeatureArr) <= 0){
					$count_result = $product->arrGetProductByPriceAscCount("",$category_id,"","1",$startprice,$endprice,$variant_id);
					$result = $product->arrGetProductByNameDesc("",$category_id,"",'1',$startprice,$endprice,"1",$startlimit,$endlimit);					
				}else{
					$count_result = $product->searchProductCount($category_id,$selectedbrandArr,"",$selectedfeatureArr,"1",$startprice,$endprice,$variant_id);
					$result = $product->searchProduct($category_id,$selectedbrandArr,"",$selectedfeatureArr,"1",$startprice,$endprice,$variant_id,$startlimit,$endlimit,"PRODUCT_MASTER.product_name desc");
				}
				break;
			default:
				
				if(sizeof($selectedbrandArr) <= 0 && sizeof($selectedfeatureArr) <= 0){		
					$count_result = $product->arrGetProductByPriceAscCount("",$category_id,"",'1',$startprice,$endprice,$variant_id);
					$result = $product->arrGetProductByPriceAsc("",$category_id,"",'1',$startprice,$endprice,$variant_id);
				}else{						
					$count_result = $product->searchProductCount($category_id,$selectedbrandArr,"",$selectedfeatureArr,"1",$startprice,$endprice,$variant_id);
					$result = $product->searchProduct($category_id,$selectedbrandArr,"",$selectedfeatureArr,"1",$startprice,$endprice,$variant_id,$startlimit,$endlimit,"PRICE_VARIANT_VALUES.variant_value asc");
				}
				break;
		}
	}
	
	
	$totalcount = $count_result[0]['cnt'] ?  $count_result[0]['cnt'] : 0;

	$pageurl = $_SERVER['SCRIPT_URI'];

	// End Pagination constants.

	 //start code for pagination.
	
	$queryStr = $_SERVER['QUERY_STRING'];

	if(!empty($offset)){
		$replaceOffset = OFFSET."=$offset";
		$queryStr = str_replace($replaceOffset,"",$queryStr);
		$queryStr = substr($queryStr,strpos($queryStr,'&')+1);
	}
//	echo $queryStr;exit;
	$pagename = $pageurl;
	if($queryStr){$queryStr = '&'.$queryStr;}	

	$numpages = ceil($totalcount/PERPAGE);
	if($numpages > 1){
		$nav = new PageNavigator($pagename, $totalcount, PERPAGE, $totaloffset,STARTPAGESHOWN,MAXPAGESHOWN,"$queryStr");
		$pageNavStr = $nav->getNavigator();
		$reg = '/<a href="(.*?)">/';						
		if(preg_match_all($reg,$pageNavStr,$matches,PREG_SET_ORDER)){
			$count = sizeof($matches);
			for($i=0;$i<$count;$i++){
				$searchArr[] = $matches[$i][0];
				
				$replaceStr = str_replace(array($pageurl.'?'.OFFSET.'='),'',$matches[$i][1]);//removed page and url info
				
				$replaceArr[] = "<a href=\"javascript:undefined;\" onclick=\"getAjaxData('$replaceStr');\" >";
			}
			$pageNavStr = str_replace($searchArr,$replaceArr,$pageNavStr);
		}
	}
    //end code for pagination.
	
	//print_r($result);exit;
	
	$cnt = sizeof($result);
	$productxml = "<PRODUCT_MASTER>";
	$productxml .= "<TOTAL_SEARCH_ITEM_FOUND><![CDATA[".$totalcount."]]></TOTAL_SEARCH_ITEM_FOUND>";
	$productxml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
	for($i=0;$i<$cnt;$i++){
		$productxml .= "<PRODUCT_MASTER_DATA>";
		$productCnt = sizeof($result[$i]);

		$brandCheck = 0;
		$link_model_name="";$seo_model_url="";unset($modelnameSeoArr);
		for($j=0;$j<$productCnt;$j++){
			unset($modelnameArr);unset($variantnameSeoArr);
			$product_id = $result[$i][$j]['product_id'];

			$brand_id = $result[$i][$j]['brand_id'];
			$category_id = $result[$i][$j]['category_id'];
			$product_name = trim($result[$i][$j]['product_name']);
			$variant = trim($result[$i][$j]['variant']);
			$short_desc = trim($result[$i][$j]['short_desc']);

			$brand_result = $brand->arrGetBrandDetails($brand_id,$category_id);
			$brand_name = trim($brand_result[0]['brand_name']);
			
			//set seo url for product variant page.
			$variantnameSeoArr[] = SEO_WEB_URL;		
			$variantnameSeoArr[] = seo_title_replace($brand_name).'-cars';
			$variantnameSeoArr[] = seo_title_replace($brand_name).'-'.seo_title_replace($product_name);
			$variantnameSeoArr[] = seo_title_replace($variant);
			$variantnameSeoArr[] = 'Overviews';
			$variantnameSeoArr[] = $product_id;
			
			if(!empty($brand_name)){					
				$modelnameArr[] = $brand_name;
			}
			if(!empty($product_name)){					
				$modelnameArr[] = $product_name;
			}
			if(empty($brandCheck)){
				//get model name and seo url.
				$modelnameSeoArr[] = SEO_WEB_URL;
				$modelnameSeoArr[] = seo_title_replace($brand_name).'-cars';
				
				$modelnameSeoArr[] = seo_title_replace($brand_name).'-'.seo_title_replace($product_name);
				$modelnameSeoArr[] = 'Model';
				$modelnameSeoArr[] = seo_title_replace($product_name);			
				$model_result=$product->arrGetProductNameInfo("",$category_id,"",$product_name);
				$model_id = $model_result[0]['product_name_id'];
				$modelnameSeoArr[] = $model_id;
				$link_model_name = implode(" ",$modelnameArr);
				$seo_model_url =  implode("/",$modelnameSeoArr);	
				$brandCheck = 1;
			}
			
			if(!empty($variant)){					
				$modelnameArr[] = $variant;
			}

			$product_video_path = $result[$i][$j]['video_path'];
			
			if(!empty($product_video_path)){
					$result[$i][$j]['video_path'] = CENTRAL_IMAGE_URL.str_replace(array(CENTRAL_IMAGE_URL),"",$product_video_path);
			}

			$image_path = $result[$i][$j]['image_path'];
			if(!empty($image_path)){
					$image_path = resizeImagePath($image_path,"160X120",$aModuleImageResize);
					$result[$i][$j]['image_path'] = CENTRAL_IMAGE_URL.str_replace(array(CENTRAL_IMAGE_URL),"",$image_path);
			}
						

			$result[$i][$j]['EXSHOWROOMPRICE_ORIGIONAL'] = $result[$i][$j]['variant_value'];
			$result[$i][$j]['EXSHOWROOMPRICE'] = $result[$i][$j]['variant_value'] ? 'Rs.'.priceFormat($result[$i][$j]['variant_value']) : ''; // use for price showing ie Rs-1,20,00.
			
			//echo $link_model_name."====".$result[$i][$j]['EXSHOWROOMPRICE']."<br>";
			
			$result[$i][$j]['DISPLAY_PRODUCT_NAME'] = implode(" ",$modelnameArr);
			$result[$i][$j]['SEO_URL'] = implode("/",$variantnameSeoArr);			
			$result[$i][$j] = array_change_key_case($result[$i][$j],CASE_UPPER);
			
			if($j > 0){
				$productxml .= "<SIMILAR_PRODUCT_MASTER>";
				foreach($result[$i][$j] as $k=>$v){
					$productxml .= "<$k><![CDATA[$v]]></$k>";
				}	
				$productxml .= "</SIMILAR_PRODUCT_MASTER>";
			}else{
				$rootxml = "";
				foreach($result[$i][$j] as $k=>$v){
					$rootxml .= "<$k><![CDATA[$v]]></$k>";
				}
			}
			
		}
		
		//start code to add reviews and ratings.
		$modelresult = $product->arrGetProductNameInfo("",$category_id,$brand_id,$product_name);
		$model_id = $modelresult[0]['product_name_id'];
		
		$reviewsresult = $userreview->arrGetAdminOverallGrade($category_id,$brand_id,'0',$model_id);
		$reviewscnt = sizeof($reviewsresult);
		$overallcnt = 0;
		$overallavg = round($reviewsresult[0]['overallgrade']);
		if($reviewscnt <= 0){
			$reviewsresult = $userreview->arrGetOverallGrade($category_id,$brand_id,'0',$model_id);			
			$overallavg = round($reviewsresult[0]['overallavg']);
			$overallcnt = $reviewsresult[0]['totaloverallcnt'] ? $reviewsresult[0]['totaloverallcnt'] : 0;
			$totalcnt = $userreview->arrGetUserReviewDetailsCount("","","","","",$brand_id,$category_id,$model_id);			
		}
		$html = "";
		for($grade=1;$grade<=5;$grade++){
			if($grade <= $overallavg){
				$html .= '<img src="'.IMAGE_URL.'spacer.gif" class="vsblStr"/>';
			}else{
				$html .= '<img src="'.IMAGE_URL.'spacer.gif" class="dsblStr"/>';
			}
		}
		$productxml .= "<OVERALL_AVG_HTML><![CDATA[$html]]></OVERALL_AVG_HTML>";
		$productxml .= "<OVERALL_AVG_CNT><![CDATA[$overallavg]]></OVERALL_AVG_CNT>";
		$productxml .= "<OVERALL_TOTAL_CNT><![CDATA[$overallcnt]]></OVERALL_TOTAL_CNT>";
		$productxml .= "<OVERALL_CNT><![CDATA[$totalcnt]]></OVERALL_CNT>";
		$productxml .= "<SEO_MODEL_RATING_PAGE_URL><![CDATA[".str_replace("Model","reviews-ratings/model-user-reviews",$seo_model_url)."#Overview]]></SEO_MODEL_RATING_PAGE_URL>";
		//end code to add reviews and ratings.


		$productxml .= $rootxml;
		$productxml .= "<LINK_PRODUCT_NAME><![CDATA[".$link_model_name."]]></LINK_PRODUCT_NAME>";
		$productxml .= "<SEO_MODEL_PAGE_URL><![CDATA[".$seo_model_url."]]></SEO_MODEL_PAGE_URL>";		
		$productxml .= "<SEO_MODEL_PHOTO_PAGE_URL><![CDATA[".str_replace(array("Model"),array("Model-Exterior"),$seo_model_url)."/1"."]]></SEO_MODEL_PHOTO_PAGE_URL>";
		$productxml .= "<SEO_MODEL_VIDEO_PAGE_URL><![CDATA[".str_replace(array("Model"),array("Model-Videos"),$seo_model_url)."/3"."]]></SEO_MODEL_VIDEO_PAGE_URL>";
		$productxml .= "<SIMILAR_PRODUCT_COUNT><![CDATA[$productCnt]]></SIMILAR_PRODUCT_COUNT>";
		$productxml .= "</PRODUCT_MASTER_DATA>";
	}
	$productxml .= "</PRODUCT_MASTER>";

	$selectedBoxCnt = sizeof($selecteditemArr);
	$selectitemxml = "<SELECTED_ITEM_MASTER>";
	$selectitemxml .= "<COUNT><![CDATA[$selectedBoxCnt]]></COUNT>";
	for($i=0;$i<$selectedBoxCnt;$i++){
		$selectitemxml .= "<SELECTED_ITEM_MASTER_DATA>";
		foreach($selecteditemArr[$i] as $k=>$v){
			$selectitemxml .= "<".strtoupper($k)."><![CDATA[$v]]></".strtoupper($k).">";
		}
		$selectitemxml .= "</SELECTED_ITEM_MASTER_DATA>";
	}
	$selectitemxml .= "</SELECTED_ITEM_MASTER>";
	 
	$config_details = get_config_details();
	$strXML = "<XML>";
	$strXML .= "<PAGE_NAME><![CDATA[".$_SERVER['SCRIPT_URI']."]]></PAGE_NAME>";
	$strXML .= "<MAX_PRICE><![CDATA[$mx_price]]></MAX_PRICE>";
	$strXML .= "<MAX_PRICE_UNIT><![CDATA[$mx_price_unit]]></MAX_PRICE_UNIT>";
	$strXML .= "<MIN_PRICE><![CDATA[$mn_price]]></MIN_PRICE>";
	$strXML .= "<MIN_PRICE_UNIT><![CDATA[$mn_price_unit]]></MIN_PRICE_UNIT>";		
	$strXML .= "<SEO_CARFINDER_COMPARE_URL><![CDATA[".WEB_URL.SEO_COMPARE_URL."]]></SEO_CARFINDER_COMPARE_URL>";
	$strXML .= "<SEO_CAR_FINDER><![CDATA[".SEO_CAR_FINDER."]]></SEO_CAR_FINDER>";
	$strXML .= "<SEO_JS><![CDATA[$seo_js]]></SEO_JS>";
	$strXML .= "<SEO_TITLE><![CDATA[$seo_title]]></SEO_TITLE>";
	$strXML .= "<SUB_TITLE><![CDATA[$sub_title]]></SUB_TITLE>";
	$strXML .= "<BREAD_CRUMB><![CDATA[$breadcrumb]]></BREAD_CRUMB>";
	$strXML .= "<SEO_DESC><![CDATA[$seo_desc]]></SEO_DESC>";
	$strXML .= "<SEO_TAGS><![CDATA[$seo_tags]]></SEO_TAGS>";
	$strXML .= "<SEO_URL><![CDATA[$seo_url]]></SEO_URL>";
	$strXML .= "<STARTLIMIT><![CDATA[".$offset."]]></STARTLIMIT>";
	$strXML .= "<PAGE_OFFSET><![CDATA[".OFFSET."]]></PAGE_OFFSET>";
	$strXML .= "<CNT><![CDATA[$numpages]]></CNT>";
	$strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$category_id]]></SELECTED_CATEGORY_ID>"; 
	$strXML .= "<SELECTED_BRAND_ID><![CDATA[$isBrandSelected]]></SELECTED_BRAND_ID>";
	$strXML .= "<SELECTEDTABID><![CDATA[$tab_id]]></SELECTEDTABID>";
	$strXML .= "<SEO_PRICE_STR><![CDATA[".implode("-",array($startprice,$endprice))."]]></SEO_PRICE_STR>";
	$strXML .= $config_details;
	$strXML .= $xml;
	$strXML .= $groupnodexml;
	$strXML .= $productxml;
	$strXML .= $selbrandxml;
	$strXML .= $selfeaturexml;
	$strXML .= $sortproductxml;
	$strXML .= $selectitemxml;
	$strXML .= "<PAGER><![CDATA[$pageNavStr]]></PAGER>";
	$strXML .= "<OC_ROS_BOTTOM_NORTH_728x90><![CDATA[OC_ROS_Bottom_North_728x90]]></OC_ROS_BOTTOM_NORTH_728x90>";
	$strXML .= "<OC_ROS_TOP_RHS_LREC_300x250_1><![CDATA[OC_ROS_Top_RHS_Lrec_300x250_1]]></OC_ROS_TOP_RHS_LREC_300x250_1>";
	$most_recent_one_result_list = $videoGallery->getarrMostRecentVideosHeaderTabLink($category_id,"0","1",$array_result);
	$strXML.="<MOST_RECENT_VIDEO_HEADER_LINK><![CDATA[".$most_recent_one_result_list."]]></MOST_RECENT_VIDEO_HEADER_LINK>";
	$strXML .= "</XML>";

	if($_REQUEST['debug']==1){ header('Content-type: text/xml');echo $strXML;exit;}
	$doc = new DOMDocument();
	$doc->loadXML($strXML);
	$doc->saveXML();
	$xslt = new xsltProcessor;
	$xsl = DOMDocument::load('xsl/carfinder_new.xsl');
	$xslt->importStylesheet($xsl);
	print $xslt->transformToXML($doc);
?>
