<?php
		require_once('./include/config.php');
		require_once(CLASSPATH.'DbConn.php');
		require_once(CLASSPATH.'user_review.class.php');
		require_once(CLASSPATH.'brand.class.php');
		require_once(CLASSPATH.'category.class.php');

		$dbconn = new DbConn;
		$userreview = new USERREVIEW;
		$brand = new BrandManagement;
		$category = new CategoryManagement;

		$category_id = $_REQUEST['catid'] ? $_REQUEST['catid'] : SITE_CATEGORY_ID;		
		$totalquestion = $_REQUEST['totalquestion'];
		$uname = trim($_REQUEST['uname']);
		if(!empty($uname)){
			
			$request_param['user_name'] = htmlentities($uname,ENT_QUOTES);

			$emailid = trim($_REQUEST['emailid']);
			$request_param['email'] = htmlentities($emailid,ENT_QUOTES);

			$location = trim($_REQUEST['location']);
			$request_param['location'] = htmlentities($location,ENT_QUOTES);

			$location = trim($_REQUEST['location']);
			$request_param['location'] = htmlentities($location,ENT_QUOTES);

			$brand_id = trim($_REQUEST['Brand']);
			$request_param['brand_id'] = htmlentities($brand_id,ENT_QUOTES);

			$product_info_id = trim($_REQUEST['Model']);
			$request_param['product_info_id'] = htmlentities($product_info_id,ENT_QUOTES);

			$product_id = trim($_REQUEST['ModelVariant']);
			$request_param['product_id'] = htmlentities($product_id,ENT_QUOTES);

			$running = trim($_REQUEST['running']);
			$request_param['running'] = htmlentities($running,ENT_QUOTES);

			$year = trim($_REQUEST['year']);
			$request_param['year_manufacture'] = htmlentities($year,ENT_QUOTES);

			$color = trim($_REQUEST['color']);
			$request_param['color'] = htmlentities($color,ENT_QUOTES);

			$request_param['uid'] = '0';
			$request_param['category_id'] = $category_id;

			$user_review_id = $userreview->intInsertUserReviewInfo($request_param);
			unset($request_param);
			$msg = "";
			if(!empty($user_review_id)){
				$msg = "Your Review has been successfully posted.It will go live after moderation.";
			}
			$request_param['user_review_id'] = htmlentities($user_review_id,ENT_QUOTES);

			for($i=1;$i<=$totalquestion;$i++){
				$que_id = $_REQUEST['que_id_'.$i];
				$request_param['que_id'] = htmlentities($que_id,ENT_QUOTES);

				$result = $userreview->arrGetQuestions($que_id);
				$formula = $result[0]['algorithm'];
				$totalanscnt = $_REQUEST['total_ans_ques_'.$que_id];
				for($ans=1;$ans<=$totalanscnt;$ans++){
					$ans_id = $_REQUEST['ans_'.$que_id.'_'.$ans];
					$usrReviewedArr['{ans'.$ans.'}'] = $_REQUEST['user_review_'.$que_id.'_'.$ans_id];				
				}
			    $grade = 0;
				if(sizeof($usrReviewedArr) > 0){                                                			   
				   $request_param['answer'] = implode(",",$usrReviewedArr);
				   $grade = strtr($formula,$usrReviewedArr);
				   $grade = round(parse_mathematical_string($grade));
				   $request_param['grade'] = $grade;
				   $request_param['is_rating'] = '1';
				   $request_param['is_comment_ans'] = '0';
				}else{
					$answer = trim($_REQUEST['comment_'.$que_id]);
					$request_param['answer'] = htmlentities($answer,ENT_QUOTES);
					$request_param['grade'] = $grade;
					$request_param['is_rating'] = '0';
					$request_param['is_comment_ans'] = '1';
				}
				unset($usrReviewedArr);
				$user_review_id = $userreview->intInsertUserReviewAnswer($request_param);			
			}
		}

		if(!empty($category_id)){
			$result = $brand->arrGetBrandDetails("",$category_id,1);
		}
		$cnt = sizeof($result);
		$xml = "<BRAND_MASTER>";
		$xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";			
		for($i=0;$i<$cnt;$i++){
			$brand_id = $result[$i]['brand_id'];
			
			$status = $result[$i]['status'];
			$categoryid = $result[$i]['category_id'];
			if(!empty($categoryid)){
				$category_result = $category->arrGetCategoryDetails($categoryid);
			}
			$category_name = $category_result[0]['category_name'];
			$result[$i]['js_category_name'] = $category_name;
			$result[$i]['category_name'] = html_entity_decode($category_name,ENT_QUOTES);
			$result[$i]['brand_status'] = ($status == 1) ? 'Active' : 'InActive';
			$result[$i]['create_date'] = date('d-m-Y',strtotime($result[$i]['create_date']));
			$result[$i]['js_brand_name'] = $result[$i]['brand_name'];
			$result[$i]['brand_name'] = html_entity_decode($result[$i]['brand_name'],ENT_QUOTES);
			$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
			$xml .= "<BRAND_MASTER_DATA>";
			foreach($result[$i] as $k=>$v){
				$xml .= "<$k><![CDATA[$v]]></$k>";
			}
			$xml .= "</BRAND_MASTER_DATA>";
		}
		$xml .= "</BRAND_MASTER>";        

        $result = $userreview->arrGetQuestions();		
        $cnt = sizeof($result);        
		$xml .= "<QUESTIONAIRE_MASTER>";
		$xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
		for($i=0;$i<$cnt;$i++){				
				$queid = $result[$i]['queid'];
				$ansresult = $userreview->arrGetQueAnswer("",$queid);
				$anscnt = 0;
				$anscnt = sizeof($ansresult);
				$xml .= "<QUESTIONAIRE_MASTER_DATA>";
					$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
					foreach($result[$i] as $k=>$v){
						$xml .= "<$k><![CDATA[$v]]></$k>";
					}
					$xml .= "<QUESTIONAIRE_ANS_MASTER>";
						$xml .= "<ANS_COUNT><![CDATA[$anscnt]]></ANS_COUNT>";
						for($ans=0;$ans<$anscnt;$ans++){	
							$ansresult[$ans] = array_change_key_case($ansresult[$ans],CASE_UPPER);
							$xml .= "<QUESTIONAIRE_ANS_MASTER_DATA>";
							foreach($ansresult[$ans] as $k=>$v){
								$xml .= "<$k><![CDATA[$v]]></$k>";
							}
							$xml .= "</QUESTIONAIRE_ANS_MASTER_DATA>";
						}
					$xml .= "</QUESTIONAIRE_ANS_MASTER>";					
				$xml .= "</QUESTIONAIRE_MASTER_DATA>";					
		}		
		$xml .= "</QUESTIONAIRE_MASTER>";

		$dateArr = arrGetDateMonthYearDate(); //Year
		$xml .= "<CURRENT_YEAR><![CDATA[".$dateArr['currentdata']['current_year']."]]></CURRENT_YEAR>";
		$xml .= "<YEAR_DATA>";
		foreach($dateArr['year'] as $year){
			$xml .= "<YEAR><![CDATA[$year]]></YEAR>";
		}
		$xml .= "</YEAR_DATA>";

		$config_details = get_config_details();

        $strXML .= "<XML>";
        $strXML .= $config_details;
		$strXML .= "<MSG><![CDATA[$msg]]></MSG>";
        $strXML .= "<SEO_URL><![CDATA[$seo_url]]></SEO_URL>";
        $strXML .= "<SEO_WEB_URL><![CDATA[".SEO_WEB_URL."]]></SEO_WEB_URL>";
        $strXML .= "<SEO_TITLE><![CDATA[$seo_title]]></SEO_TITLE>";
        $strXML .= "<SEO_TAGS><![CDATA[$seo_keywords]]></SEO_TAGS>";
        $strXML .= "<SEO_DESC><![CDATA[$seo_desc]]></SEO_DESC>";
        $strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$category_id]]></SELECTED_CATEGORY_ID>";        
	    $strXML.= $xml;
	    $strXML.= "</XML>";

		$strXML = mb_convert_encoding($strXML, "UTF-8");
        
		if($_REQUEST['debug']==1){ header('Content-type: text/xml');echo $strXML;exit; }

        $doc = new DOMDocument();
        $doc->loadXML($strXML);
        $doc->saveXML();
	
	$xslt = new xsltProcessor;
        $xsl = DOMDocument::load('xsl/full_review.xsl');
        $xslt->importStylesheet($xsl);
        print $xslt->transformToXML($doc);

?>
