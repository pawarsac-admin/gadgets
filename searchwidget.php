<?php
	require_once('include/config_search.php');
	//////// PRICE 
	if(empty($mnprice) && empty($mxprice)){
		$price_exp = "/price\-([^\/]+)/i";
		if(preg_match_all($price_exp,$request_uri,$matches,PREG_SET_ORDER)) {
			$priceArr			= explode("-",$matches[0][1]);
			$mnprice	= empty($priceArr[0])? MIN_PRICE : $priceArr[0];
			$mxprice	= empty($priceArr[1])? MAX_PRICE : $priceArr[1];
		} else {
			$mnprice	= MIN_PRICE;
			$mxprice	= MAX_PRICE;
		}
	}

	$xml .= "<MIN_PRICE><![CDATA[$mnprice]]></MIN_PRICE>";
	$xml .= "<MAX_PRICE><![CDATA[$mxprice]]></MAX_PRICE>";

	//////// BRAND
	if(empty($arrBrandsData)){
		if(!empty($category_id)){
			$arrBrandsDataWidget = $brand->arrGetBrandDetails("",$category_id,"1","","","","order by position asc");
		}
	} else {
		$arrBrandsDataWidget = $arrBrandsData;
	}

	$cnt	 = sizeof($arrBrandsDataWidget);
	$xml	.= "<BRAND_MASTER>";
	$xml	.= "<COUNT><![CDATA[$cnt]]></COUNT>";

	$selectedIndex			= 0;
	$brandSelectStatusFlag	= 0;

	for( $i=0; $i<$cnt; $i++ ){

		$brand_id		= $arrBrandsDataWidget[$i]['brand_id'];
		$brand_name		= $arrBrandsDataWidget[$i]['brand_name'];
		$short_desc		= $arrBrandsDataWidget[$i]['short_desc'];

		$arrBrandsDataWidget[$i]['brand_name_value']	= str_replace(' ',"_",$brand_name);
		
		if( in_array( $brand_id, $selectedBrandArr ) ){
			$arrBrandsDataWidget[$i]['select_status'] = 1 ;
			$brandSelectStatusFlag	= 1;
		} else {
			$arrBrandsDataWidget[$i]['select_status'] = 0 ;
		}

		$arrBrandsDataWidget[$i]['brand_name']	= html_entity_decode($arrBrandsDataWidget[$i]['brand_name'],ENT_QUOTES,'UTF-8');
		
		$arrBrandsDataWidget[$i]	= array_change_key_case($arrBrandsDataWidget[$i],CASE_UPPER);

		$xml	.= "<BRAND_MASTER_DATA>";
		foreach($arrBrandsDataWidget[$i] as $k=>$v){
			$xml	.= "<$k><![CDATA[$v]]></$k>";
		}
		$xml	.= "</BRAND_MASTER_DATA>";
	}
	
	
	$xml	.= "<BRANDSELECTSTATUSFLAG><![CDATA[$brandSelectStatusFlag]]></BRANDSELECTSTATUSFLAG>";
	$xml	.= "</BRAND_MASTER>";

	$xml	.= "<PHONE_TYPE_MASTER>";
	foreach ($arrPhoneType[PHONETYPE] as $key =>  $val ){
		$phoneTypeSelectStatus = 0;
		$xml	.= "<PHONE_TYPE>";
		$xml	.= "<ID><![CDATA[$key]]></ID>";
		$xml	.= "<NAME><![CDATA[$val]]></NAME>";
		$xml	.= "<NAME_VALUE><![CDATA[".str_replace(' ',"_",$val)."]]></NAME_VALUE>";
		if ( in_array( $key,  $arrPhoneTypeId)) $phoneTypeSelectStatus = 1;
		$xml	.= "<SELECT_STATUS><![CDATA[$phoneTypeSelectStatus]]></SELECT_STATUS>";	

		$xml	.= "</PHONE_TYPE>";
	}
	$xml	.= "</PHONE_TYPE_MASTER>";

?>