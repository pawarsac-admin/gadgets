<?php
 /**
 * Message board reply page
 * 
 * Action page 
 * @version V1.0
 * @package message board 
*/
/**
 * include the config
*/
//ini_set("display_errors",true);
//error_reporting(E_ALL);

require_once("./include/config.php");
require_once(CLASSPATH.'DbConn.php');
require_once(CLASSPATH."campus_discussion.class.php");
require_once(CLASSPATH."report.class.php");

$dbconn		=  new DbConn;
$oCampusDiscussion =new campus_discussion();
$report = new report;


$sAction=$_REQUEST['action'];
switch ($sAction){
	case 'reply':
		$iCPage=$_REQUEST['cpage'];
		$iTId=$_REQUEST['tid'];
		$id=$_REQUEST['id'];
		$cid=$_REQUEST['cid'];
		//$iTId=$_REQUEST['tid'];
		$sRcontent=urldecode($_REQUEST['rcontent']);
		$sSubject=$_REQUEST['subject'];
		$iParentId=$_REQUEST['pid'];
		$iUserId=$_COOKIE['uid'];	
		$iServiceId=$_REQUEST['sid'];	
		$iSessionId=$_COOKIE['session_id'];
		$sEmail=$_COOKIE['email'];
		$sProfile="";
		
		/**
		 * include utility class
		*/
		require_once(CLASSPATH.'utility.class.php');
		$oUtility = new utility;

		//strip tags from reply content 
		if(strlen($sRcontent)>0){
			$sRcontent=trim(strip_tags($sRcontent, $sAllowedTags));
			$sRcontent=trim(str_replace("]]>","",$sRcontent));
			$sRcontent=trim($oUtility->closetags($sRcontent));
		}

		//check if the user is logged in
		/**
		 * include auth class
		*/
		require_once(CLASSPATH.'authentication.class.php');
		$oAuth = new authentication();
		$iLoginStatus=0; 
		if(isset($iSessionId)){
			$iLoginStatus = $oAuth->auth_checkSession($sEmail,$iSessionId,$iServiceId,$iUserId);
		}
		if($iLoginStatus==1){
			if(strlen($sRcontent)>0){
				$aParameters=Array("tid"=>$iTId,"uid"=>$iUserId,"profile"=>$sProfile,"subject"=>$sSubject,"content"=>$sRcontent,"parentid"=>$iParentId);
				$aReplyRes=$oCampusDiscussion->addReply($aParameters);
				$iReplyId=-2;
				$iReplyId=$aReplyRes['replyid'];
				$array_param = Array("id"=>$id,"sid"=>$iServiceId,"cid"=>$cid,"iTId"=>$iTId);
				$result = $report->UpdateCommentCount($array_param);

				if($iReplyId !=-2){
					//reload the page 
					print 1; 
				}
			}else {
				print 3; 	
			}
		}else {
			print 4; 	
		}
	break;
	case 'markabuse':
		$iCPage=$_REQUEST['cpage'];
		$iTId=$_REQUEST['tid'];
		$iReplyId=$_REQUEST['iReplyId'];
		$sAbuseOpt=0;
		$iUserId=$_COOKIE['uid'];	
		$iServiceId=$_REQUEST['sid'];	
		$iSessionId=$_COOKIE['session_id'];
		$sEmail=$_COOKIE['email'];

		//check if the user is logged in
		/**
		 * include auth class
		*/
		require_once(CLASSPATH.'authentication.class.php');
		$oAuth = new authentication;

		$iLoginStatus=0; 
		if(isset($iSessionId)){
			$iLoginStatus = $oAuth->auth_checkSession($sEmail,$iSessionId,$iServiceId,$iUserId);
		}
		if($iLoginStatus==1){
			//save the abuse options in table reply_abuse
			//Mark reply as abuse
			$aParameters=Array("replyid"=>$iReplyId,"uid"=>$iUserId);
			$aRes=$oCampusDiscussion->abuseReply($aParameters);
			print 1;
			
		}else {
			print "4"; //error invalid session.
		}
	break;
}
?>
