<?php
        //ini_set("display_errors",1);
	require_once('./include/config.php');
	require_once(CLASSPATH.'DbConn.php');
	require_once(CLASSPATH.'brand.class.php');
	require_once(CLASSPATH.'category.class.php');
	require_once(CLASSPATH.'pivot.class.php');
	require_once(CLASSPATH.'feature.class.php');
	require_once(CLASSPATH.'product.class.php');
	require_once(CLASSPATH.'article.class.php');
	require_once(CLASSPATH.'citystate.class.php');
	require_once(CLASSPATH.'price.class.php');
	require_once(CLASSPATH.'wallpaper.class.php');
	require_once(CLASSPATH.'reviews.class.php');
	require_once(CLASSPATH."campus_discussion.class.php");
	require_once(CLASSPATH.'pager.class.php');	
	require_once(CLASSPATH.'user.class.php');
	require_once(CLASSPATH.'user_review.class.php');
	require_once(CLASSPATH.'videos.class.php');
	require_once(CLASSPATH.'report.class.php');
	require_once(CLASSPATH.'dealer.class.php');
	require_once(CLASSPATH.'Utility.php');
	
	
	$dbconn = new DbConn;
	$oBrand = new BrandManagement;
	$category = new CategoryManagement;
	$oPivot = new PivotManagement;
	$oFeature = new FeatureManagement;
	$oProduct = new ProductManagement;
	$oArticle = new article;
	$oCityState = new citystate;
	$oPrice = new price;
	$oWallpapers =new Wallpapers;
	$oReview =new reviews;
	$oCampusDiscussion	=  new campus_discussion();
	$ObjPager	=  new Pager();
	$obj_user = new user;
	$userreview = new USERREVIEW;
	$videoGallery = new videos();
	$report = new report;
	$oDealer = new Dealer;

	//print "<pre>"; print_r($_REQUEST); //die();


	setcookie ("reviewAdded", "", time() - 3600); //used to remove cookie of user review and rating section.
	if(isset($_REQUEST['tid'])) $iTId=$_REQUEST['tid'];
	$category_id = $_REQUEST['catid'] ? $_REQUEST['catid'] : SITE_CATEGORY_ID;
	//$product_name_id = strtolower($_REQUEST['pname']);
	//$productnameArr = explode("-",$product_info_name); //used to get product id from url rewrite.
	$product_name_id = $_REQUEST['pname'];	

	$res = $oProduct->arrGetProductNameInfo($product_name_id,$category_id,"","","1");
	if(is_array($res)){
		$product_info_name = $res[0]["product_info_name"];
	}
	$user_name="";
	$tab_id = $_REQUEST['fid'] ? $_REQUEST['fid'] :1;
	$revid = trim($_REQUEST['revid']);
	// reviews oncars,experts and user
        $reviewName = trim($_REQUEST['rev']);
	$rev_grp_id = trim($_REQUEST['grpid']);
	$revtype = $_REQUEST['revtype'];
	$user_review_id = $_REQUEST['userrevid'];

	//$hd_city_name = $_REQUEST['hd_city_name'];
	$selected_city_id = $_REQUEST["cookie_city_id"];
        $selected_city_name = $_REQUEST['hd_city_name'];
	//photo and videos
	$tab = $_REQUEST['tab'];
	$photo_tab_id = $_REQUEST['photo_tab_id'];
	
	if(!empty($product_name_id)){
		$aProductInfoDetails = $oProduct->arrGetProductNameInfo($product_name_id,$category_id,"","");
		$product_name_id = $aProductInfoDetails[0]['product_name_id'] ? $aProductInfoDetails[0]['product_name_id'] : $product_name_id;
		$rating_brand_id = $aProductInfoDetails[0]['brand_id'];		
	}else{
		header("Location:".WEB_URL."404.php");exit;
	}

	/*if(!empty($hd_city_name)){
		unset($result);
		$result = $oCityState->arrGetCityStateDetails($hd_city_name,"","","1"); 
		$selected_city_id = $result[0]["city_id"];
	}*/
	if(!empty($selected_city_name)){
                unset($result);
                $result = $oCityState->arrGetCityStateDetails($selected_city_name,"","","1");
                $selected_city_id = $result[0]["city_id"];
        }
        if(empty($selected_city_name) && !empty($selected_city_id)){
                unset($result);
                $result = $oCityState->arrGetCityDetails($selected_city_id,"","1","","");
                $selected_city_name = $result[0]['city_name'];
        }
	$sCityId = $selected_city_id;
	$sCityName = $selected_city_name;
	
	if(!empty($selected_city_id)){
		unset($pResult);
		$aPriceRange = Array();
		$pResult=$oProduct->arrGetProductByName($product_info_name);
        	//print"<pre>";print_r($pResult);print"</pre>";
	        if(is_array($pResult)){
        	        $iCnt=sizeof($pResult);
               		for($i=0;$i<$iCnt;$i++){
                        	$product_id=$pResult[$i]['product_id'];
          			$rPriceDetail = $oPrice->arrGetPriceDetails("1",$product_id,$category_id,"","",$selected_city_id,"1","","","");
                        	//print_r($rPriceDetail); //die();
                	        if(is_array($rPriceDetail)){
        	                        $sExShowRoomPrice=$rPriceDetail[0]['variant_value'];
        	                        $aPriceRange[]=$sExShowRoomPrice;
                        	}
			}
		}
		$cnt_aPriceRange = sizeof($aPriceRange);
		if($cnt_aPriceRange == 0){
			$selected_city_id = "";
			$selected_city_name = "";
		}
	}
	//start code added by rajesh on dated 02-06-2011 for expert rating and graph.
	$result = $userreview->arrGetAdminExpertGrade($category_id,'','',$product_name_id);

	$design_rating = $result[0]['design_rating'];
	$performance_rating = $result[0]['performance_rating'];
	$user_rating = $result[0]['user_rating'];
		
	$design_rating_proportion = ($design_rating*100)/10;
	$performance_rating_proportion = ($performance_rating*100)/10;
	$user_rating_proportion = ($user_rating*100)/10;

	$overallgrade = $result[0]['overallgrade'];

	$rating_algo_key = "";
	foreach($rangeArr as $key => $range){
		if($overallgrade >= $range[0] && $overallgrade <= $range[1]){
			$rating_algo_key = $key;
			break;
		}
	}
	
	$expertratinghtml = '';
	foreach($ratingAlgoArr[$rating_algo_key] as $classname){
		$expertratinghtml .= "<img class=\"$classname\" src=\"".IMAGE_URL."spacer.gif\">";
	}
	$rating_algo_key = $rating_algo_key ? $rating_algo_key : 'Not Yet Rated';

	$expertratingxml .= "<STAR_EXPERT_GRAPH_RATING_STR><![CDATA[$expertratinghtml]]></STAR_EXPERT_GRAPH_RATING_STR>";
	$expertratingxml .= "<STAR_EXPERT_GRAPH_RATING_MSG><![CDATA[$rating_algo_key]]></STAR_EXPERT_GRAPH_RATING_MSG>";
		
	//$expertratinghtml =  ($rating_algo_key != 'onestar' && $rating_algo_key != 'halfstar' && $rating_algo_key != '') ? $expertratinghtml."($rating_algo_key)" : $expertratinghtml;

	$expertratingxml .= "<STAR_EXPERT_RATING_STR><![CDATA[$expertratinghtml]]></STAR_EXPERT_RATING_STR>";
		
	$expertratingxml .= "<EXPERT_DESIGN_RATING_PROPORTION><![CDATA[$design_rating_proportion]]></EXPERT_DESIGN_RATING_PROPORTION>";
	$expertratingxml .= "<EXPERT_DESIGN_RATING><![CDATA[$design_rating]]></EXPERT_DESIGN_RATING>";
	$expertratingxml .= "<EXPERT_PERFORMANCE_RATING_PROPORTION><![CDATA[$performance_rating_proportion]]></EXPERT_PERFORMANCE_RATING_PROPORTION>";
	$expertratingxml .= "<EXPERT_PERFORMANCE_RATING><![CDATA[$performance_rating]]></EXPERT_PERFORMANCE_RATING>";
	$expertratingxml .= "<EXPERT_USER_RATING_PROPORTION><![CDATA[$user_rating_proportion]]></EXPERT_USER_RATING_PROPORTION>";
	$expertratingxml .= "<EXPERT_USER_RATING><![CDATA[$user_rating]]></EXPERT_USER_RATING>";
	
	//end code added by rajesh on dated 02-06-2011 for expert rating and graph.

	//start  code added by rajesh on dated 02-06-2011 for user.
		
		$reviewsresult = $userreview->arrGetAdminOverallGrade($category_id,$rating_brand_id,'0',$product_name_id);
		$reviewscnt = sizeof($reviewsresult);
		$overallcnt = 0;
		$overallavg = round($reviewsresult[0]['overallgrade']);
		if($reviewscnt <= 0){
			$reviewsresult = $userreview->arrGetOverallGrade($category_id,$rating_brand_id,'0',$product_name_id);			
			$overallavg = round($reviewsresult[0]['overallavg']);
			$overallcnt = $reviewsresult[0]['totaloverallcnt'] ? $reviewsresult[0]['totaloverallcnt'] : 0;
			$totalcnt = $userreview->arrGetUserReviewDetailsCount("","","","","",$rating_brand_id,$category_id,$product_name_id);			
		}

		$brandresult = $oBrand->arrGetBrandDetails($rating_brand_id);
		
		$product_brand_name = html_entity_decode($brandresult[0]['brand_name'],ENT_QUOTES,'UTF-8');
		$product_brand_name = removeSlashes($product_brand_name);
		$product_brand_name = seo_title_replace($product_brand_name);

		$product_link_name = $product_brand_name."-".$aProductInfoDetails[0]['product_info_name'];
		$product_link_name = html_entity_decode($product_link_name,ENT_QUOTES,'UTF-8');
		$product_link_name = removeSlashes($product_link_name);
		$product_link_name = seo_title_replace($product_link_name);
		unset($seoTitleArr);
		$seoTitleArr[] = SEO_WEB_URL;
		$seoTitleArr[] = $product_brand_name."-cars";
		$seoTitleArr[] = $product_link_name;
		$seoTitleArr[] = "reviews-ratings";
		$seoTitleArr[] = SEO_CARS_MODEL_REVIEWS; 
		$seoTitleArr[] = $aProductInfoDetails[0]['product_info_name'];
		$seoTitleArr[] = $product_name_id;
		$seo_model_url = implode("/",$seoTitleArr);
		unset($seoTitleArr);


		for($grade=1;$grade<=5;$grade++){
			if($grade <= $overallavg){
				$html .= '<img src="'.IMAGE_URL.'spacer.gif" class="vsblStr"/>';
			}else{
				$html .= '<img src="'.IMAGE_URL.'spacer.gif" class="dsblStr"/>';
			}
		}
		if($overallavg == 1){
			$gradeStr = "Poor";
		}else if($overallavg == 2){
			$gradeStr = "Fair";
		}else if($overallavg == 3){
			$gradeStr = "Average";
		}else if($overallavg == 4){
			$gradeStr = "Good";
		}else if($overallavg == 5){
			$gradeStr = "Excellent";
		}
		$expertratingxml .= "<OVERALL_AVG_HTML><![CDATA[$html]]></OVERALL_AVG_HTML>";
		$expertratingxml .= "<OVERALL_AVG_HTML_MSG><![CDATA[$gradeStr]]></OVERALL_AVG_HTML_MSG>";
		$expertratingxml .= "<OVERALL_AVG_CNT><![CDATA[$overallavg]]></OVERALL_AVG_CNT>";
		$expertratingxml .= "<OVERALL_TOTAL_CNT><![CDATA[$overallcnt]]></OVERALL_TOTAL_CNT>";
		$expertratingxml .= "<OVERALL_CNT><![CDATA[$totalcnt]]></OVERALL_CNT>";
		$expertratingxml .= "<SEO_MODEL_RATING_PAGE_URL><![CDATA[$seo_model_url]]></SEO_MODEL_RATING_PAGE_URL>";
	//end code added by rajesh on dated 02-06-2011 for user.

	$sproduct_name_id = $product_name_id;
	$search_product_info_name = $aProductInfoDetails[0]['product_info_name'];
	$product_info_name = $aProductInfoDetails[0]['product_info_name'];
	$product_info_brand_id = $aProductInfoDetails[0]['brand_id'];
	$media_path = $aProductInfoDetails[0]['image_path'];
	$seo_product_info_name = $product_info_name;
	$img_media_id = $aProductInfoDetails[0]['img_media_id'];
	if(!empty($media_path)){
		$media_path = resizeImagePath($media_path,"160X120",$aModuleImageResize,$img_media_id);
		$media_path = $media_path ? CENTRAL_IMAGE_URL.$media_path : '';
	}
	$aProductInfoDetails['0']['image_path'] = $media_path ;
	$brand_result=$oBrand->arrGetBrandDetails($product_info_brand_id,$category_id);
	$brand_name=$brand_result[0]['brand_name'];
	$seo_product_info_name=seo_title_replace($product_info_name);
	$sLinkProductName=$brand_name."-".$product_info_name;
	$product_brand_name = html_entity_decode($brand_name,ENT_QUOTES,'UTF-8');
	$product_brand_name = removeSlashes($product_brand_name);
	$product_brand_name = seo_title_replace($product_brand_name);
	$product_link_name = html_entity_decode($sLinkProductName,ENT_QUOTES,'UTF-8');
	$product_link_name = removeSlashes($product_link_name);
	$product_link_name = seo_title_replace($product_link_name);
	$aHostqstr=explode("/",$_SERVER['SCRIPT_URL']);
	$seo_title_part = $brand_name." ".$product_info_name;
	$reviewName = $reviewName ? $reviewName : $tab;
	//Reviews Group details
	$aReviewsGroupDetail = $oReview->arrGetReviewsGroupDetails("","",$category_ids="",1);
	if(is_array($aReviewsGroupDetail)){
		foreach($aReviewsGroupDetail as $igKey=>$aGrpValue){
			$aReviewsGroupData[$aGrpValue['group_id']]=$aGrpValue['group_name'];
		}
	}
	///echo "TIME 2====".date('l jS \of F Y h:i:s A')."<br>"; //die();
	$aDefaultProductReview = $oReview->getReviewsDetails("","","","0",$product_name_id,$category_id,"",1,"","","");
	$defRevId=$aDefaultProductReview[0]['review_id'];
	$defRevGrpId=$aDefaultProductReview[0]['group_id'];
	$mb_defRevId=$defRevId;
	
	if($reviewName=='oncars'){
		$revid = $revid ? $revid : $defRevId;
		if($revid){
			$defRevId=$revid;
			//	Item detail
			///$aReviewDetails = $oReview->getReviewsDetails($revid,"","","","",$category_id,"",1,"","","");
			
			$aReviewDetails = $oReview->getReviewsDetails($revid,"","",$product_id,"",$category_id,"",1,"","","");
			$productRevId = $aReviewDetails[0]['product_review_id'];
			$aGroupId = $aReviewDetails[0]['group_id'];
			$abstract = html_entity_decode($aReviewDetails[0]['abstract'],ENT_QUOTES,'UTF-8');
			//if(strlen($abstract)>200){ $abstract = getCompactString($abstract, 200).' ...'; }
			
			$aViewCntUrl = array(); $aEncViewCntUrl = array();
			$seoTitleArr =""; $seoTitleArr1 ="";

			$brand_id = $aReviewDetails[0]['brand_id'];
			$product_id = $aReviewDetails[0]['product_id'];
			$product_info_id = $aReviewDetails[0]['product_info_id'];

			$aBrandDetail=$oBrand->arrGetBrandDetails($brand_id,$category_id);
			$brand_name=$aBrandDetail[0]['brand_name'];
			$productNameInfo = $oProduct->arrGetProductNameInfo($product_info_id,$category_id,"","",1,"","");
			$model=$productNameInfo[0]['product_info_name'];

			$brand_name = html_entity_decode($brand_name,ENT_QUOTES,'UTF-8');
			$brand_name = removeSlashes($brand_name);
			$brand_name = seo_title_replace($brand_name);
		
			$model = html_entity_decode($model,ENT_QUOTES,'UTF-8');
			$model = removeSlashes($model);
			$model = seo_title_replace($model);
		
			unset($seoTitleArr);
			$seoTitleArr[] = SEO_WEB_URL;
			$seoTitleArr[] = $brand_name."-cars";
			$seoTitleArr[] = $brand_name."-".$model;
			$seoTitleArr[] = "reviews-ratings";
			$seoTitleArr[] = "on-cars-Model-reviews";
			$seoTitleArr[] = $model;
			$seoTitleArr[] = $product_info_id;
			$seoTitleArr[] = $aGroupId;
			$seoTitleArr[] = $revid;
			$seo_url = implode("/",$seoTitleArr);

			$views_page_name = $seo_url;

			$aViewCntUrl[$seo_url] = $revid;
			$aEncViewCntUrl[$seo_url] = $revid;
			
			//print "<pre>"; print_r($aEncViewCntUrl);	

			$aViewCnt=Array();
			$aViewCnt = $report->getPageViews($aViewCntUrl,$aEncViewCntUrl);
			
			//print "<pre>"; print_r($aViewCnt);
			//die();

			$uid=$aReviewDetails[0]['uid'];
			$editor_res = $obj_user->arrGetEditorDetails($uid,"","","");
			$editor_name = $editor_res[0]['editor_name'];
			$profile_image = $editor_res[0]['profile_img'];
			$designation = $editor_res[0]['designation'];
			$short_desc = $editor_res[0]['short_desc'];
			$srevDetailxml = "";
			
			$srevDetailxml .= "<VIEWS_COUNT><![CDATA[$aViewCnt[$revid]]]></VIEWS_COUNT>";
			$srevDetailxml .= "<ABSTRACT><![CDATA[$abstract]]></ABSTRACT>";
			$srevDetailxml .= "<EDITOR_NAME><![CDATA[$editor_name]]></EDITOR_NAME>";
			$srevDetailxml .= "<PROFILE_IMAGE><![CDATA[$profile_image]]></PROFILE_IMAGE>";
			$srevDetailxml .= "<DESIGNATION><![CDATA[$designation]]></DESIGNATION>";
			$srevDetailxml .= "<SHORT_DESC><![CDATA[$short_desc]]></SHORT_DESC>";
			//$srevDetailxml .= "<COMMENT_COUNT><![CDATA[$comment_count]]></COMMENT_COUNT>";
			//$srevDetailxml .= "<VIEWS_COUNT><![CDATA[$views_count]]></VIEWS_COUNT>";
			$aReviewGroupDetail = $oReview->arrGetReviewsGroupDetails($aReviewDetails[0]['group_id'],"",$category_id,"1");
			$aReviewDetails[0]['group_name'] = $aReviewGroupDetail[0]['group_name'];
			$aReviewDetails[0]['disp_name'] = $sLinkProductName;
                     $disp_title_name=$aReviewGroupDetail[0]['group_name']."".$aReviewDetails[0]['disp_name'];
			$iCount = sizeof($aReviewGroupDetail);
			$sReviewDetail=arraytoxml($aReviewDetails,"REVIEWS_ITEM_DETAIL_DATA");
			$sReviewDetail.="<REVIEWS_ITEM_DETAIL_COUNT>".$iCount."</REVIEWS_ITEM_DETAIL_COUNT>";
			$sReviewDetailXml ="<REVIEWS_ITEM_DETAIL>".$sReviewDetail."</REVIEWS_ITEM_DETAIL>";
			//

			if(is_array($aReviewDetails)){
				$product_review_id=$aReviewDetails[0]['product_review_id'];
				$result = $oReview->arrGetUploadMediaReviewsDetails($product_review_id);

				$cnt = sizeof($result);
				$strmediaxml ="<REVIEW_DETAIL>";
				$strmediaxml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
				for($i=0;$i<$cnt;$i++){
					$content=$result[$i]['content'];
					$result[$i]['content']=html_entity_decode($content,ENT_QUOTES,'UTF-8');
					$upload_media_id = $result[$i]['upload_media_id'];
					$is_media_process = $result[$i]['is_media_process'];
					$content_type = $result[$i]['content_type'];
					$media_id = $result[$i]['media_id'];
					$media_path = $result[$i]['media_path'];
					$video_img_path = $result[$i]['video_img_path'];
					$video_img_id = $result[$i]['video_img_id'];
					$v_id="";
					if($content_type == "1"){
						$v_id = $upload_media_id;
					}
					if(empty($is_media_process) && !empty($media_path) && $content_type != 2){
						$media_path = resizeImagePath($media_path,"555X312",$aModuleImageResize,$media_id);
						$media_path = $media_path ? CENTRAL_IMAGE_URL.$media_path : '';			
					}elseif(!empty($is_media_process) && !empty($media_path) && $content_type != 2){
						$media_path = $media_path ? CENTRAL_MEDIA_URL.$media_path : '';			
					}
					if(!empty($video_img_path)){
						$video_img_path = resizeImagePath($video_img_path,"555X416",$aModuleImageResize,$video_img_id);
						$thumb_video_img_path = resizeImagePath($video_img_path,"160X120",$aModuleImageResize,$video_img_id);
						$video_img_path = $video_img_path ? CENTRAL_IMAGE_URL.$video_img_path : '';						
						$thumb_video_img_path = $thumb_video_img_path ? CENTRAL_IMAGE_URL.$thumb_video_img_path : '';
						
					}
					if($i>0){
						//echo $media_path."DDDDDDDDDDD";
						if(!empty($media_path)){
							$new_media_path = resizeImagePath($media_path,"555X416",$aModuleImageResize,$media_id);
							$media_path_dopop = $new_media_path ? CENTRAL_IMAGE_URL.$new_media_path : '';
							$result[$i]['media_path_pop'] = $media_path_dopop;
							//echo $media_path_dopop."SASASASASASA";

						}
					}
					if(empty($is_media_process) && $content_type != 2){
						//start code to get processed audio/video.
						if($media_id!=0){
							$upload_result = arrUpdateAudioVideo($media_id,$upload_media_id,"UPLOAD_MEDIA_REVIEWS",$video_img_path);
							if(sizeof($upload_result) > 0){
								$media_path = CENTRAL_MEDIA_URL.$upload_result['media_path'];
								$uploaded_video_img_path = $upload_result['video_img_path'];
								if(!empty($uploaded_video_img_path)){
									$uploaded_video_img_path = resizeImagePath($uploaded_video_img_path,"555X312",$aModuleImageResize,$video_img_id);
								}
								$video_img_path = $uploaded_video_img_path ? CENTRAL_IMAGE_URL.$uploaded_video_img_path : '';
							}
						}
					}
					$videoArr = arrGetDifferentVideoResolution($media_path);
					$low_media_path = $videoArr['low_media_path'];	
					$normal_media_path = $videoArr['normal_media_path'];
					if(!empty($media_path)){
						$media_path = CENTRAL_MEDIA_URL.str_replace(array(CENTRAL_MEDIA_URL,CENTRAL_IMAGE_URL),"",$media_path);
					}
					if(!empty($low_media_path)){
						$low_media_path = CENTRAL_MEDIA_URL.str_replace(array(CENTRAL_MEDIA_URL,CENTRAL_IMAGE_URL),"",$low_media_path);
					}
					if(!empty($normal_media_path)){
						$normal_media_path = CENTRAL_MEDIA_URL.str_replace(array(CENTRAL_MEDIA_URL,CENTRAL_IMAGE_URL),"",$normal_media_path);
					}
					$result[$i]['media_path'] = $media_path;
					$result[$i]['low_media_path'] = $low_media_path;
					$result[$i]['normal_media_path'] = $normal_media_path;
					$result[$i]['video_img_path'] = $video_img_path;
					$result[$i]['thumb_video_img_path'] = $thumb_video_img_path;
					$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
					$strmediaxml .= "<REVIEW_DETAIL_DATA>";
					foreach($result[$i] as $k => $v){
						$strmediaxml .= "<$k><![CDATA[$v]]></$k>";
					}
					$lang_video_arr = Array();
					$lang_cnt = "0";
					if($v_id != ""){
						$lang_video_arr = $videoGallery->getLanguageVideosDetails($v_id,"3","","","","","",$category_id,"","1","","","");
						$lang_cnt = sizeof($lang_video_arr);
					}
			        //$lang_available = !empty($lang_cnt) ? 1 : 0;
				    $strmediaxml .= "<LANG_CNT><![CDATA[$lang_cnt]]></LANG_CNT>";					
					if(!empty($lang_cnt)){
						$language_video_id = $lang_video_arr[0]["language_video_id"];
						$lang_video_id = $lang_video_arr[0]["video_id"];
						$lang_video_type = $lang_video_arr[0]["video_type"];
						$language_id = $lang_video_arr[0]["language_id"];
						$lang_title = $lang_video_arr[0]["title"];
						$lang_tags = $lang_video_arr[0]["tags"];
						$lang_media_id = $lang_video_arr[0]["media_id"];
						$lang_media_path = $lang_video_arr[0]["media_path"];
						$lang_video_img_id = $lang_video_arr[0]["video_img_id"];
						$lang_video_img_path = $lang_video_arr[0]["video_img_path"];
						$lang_content_type = $lang_video_arr[0]["content_type"];
						$lang_is_media_process = $lang_video_arr[0]["is_media_process"];
						$lang_status = $lang_video_arr[0]["status"];
						$lang_is_media_process = $lang_video_arr[0]["is_media_process"];
						if(!empty($lang_is_media_process) && $lang_is_media_process!= '0'){
							$lang_available = 1;
						}else{
							$lang_available = 0;
							//start code to get processed audio/video.
							$upload_result = arrUpdateAudioVideo($lang_media_id,$lang_video_img_id,"LANGUAGE_VIDEO_GALLERY",$lang_video_img_path,$language_video_id);
							if(sizeof($upload_result) > 0){
								$lang_media_path = $upload_result['media_path'];
								$uploaded_lang_video_img_path = $upload_result['video_img_path'];
								$lang_video_img_path = $lang_video_img_path ? $lang_video_img_path : $uploaded_lang_video_img_path;
								$lang_available = 1;
							}
						}
						$lang_videoArr = arrGetDifferentVideoResolution($lang_media_path);
						$low_media_path = $lang_videoArr['low_media_path'];     
						$normal_media_path = $lang_videoArr['normal_media_path'];
						if($lang_content_type == 1){
							//for video.
							if(!empty($lang_video_img_path)){
								$lang_video_img_path = resizeImagePath($lang_video_img_path,"555X312",$aModuleImageResize,$lang_video_img_id);
							}
							$lang_video_arr[0]['media_path'] = $lang_media_path ? CENTRAL_MEDIA_URL.$lang_media_path : '';
							$lang_video_arr[0]['low_media_path'] = $low_media_path ? CENTRAL_MEDIA_URL.$low_media_path : '';
							$lang_video_arr[0]['normal_media_path'] = $normal_media_path ? CENTRAL_MEDIA_URL.$normal_media_path : '';
							$lang_video_arr[0]['video_img_path'] = $lang_video_img_path ? CENTRAL_IMAGE_URL.$lang_video_img_path : '';
						}
						$lang_video_arr[0] = array_change_key_case($lang_video_arr[0],CASE_UPPER);
						$strmediaxml .= "<LANGUAGE_VIDEO_DETAILS_DATA>";
						$strmediaxml .= "<IS_LANGUAGE_VID_AVAILABLE><![CDATA[$lang_available]]></IS_LANGUAGE_VID_AVAILABLE>";
						foreach($lang_video_arr[0] as $k=>$v){
							$strmediaxml .= "<$k><![CDATA[$v]]></$k>";
						}
						$strmediaxml .= "</LANGUAGE_VIDEO_DETAILS_DATA>";
                	}
					$strmediaxml .= "</REVIEW_DETAIL_DATA>";
				}
				$strmediaxml .="</REVIEW_DETAIL>";
			}
		}
		///echo "TIME 3====".date('l jS \of F Y h:i:s A')."<br>"; //die();
	}else if($reviewName=='experts'){
		unset($seoArr);
		$aReviewDetails = $oReview->getExpertReviewsDetails("","","","",$product_name_id,$category_id,"",1,"",""," order by R.create_date desc ");
		$sReviewDetail=arraytoxml($aReviewDetails,"REVIEW_DETAIL_DATA");
		$sReviewDetail.="<REVIEWS_DETAIL_COUNT>".$iCount."</REVIEWS_DETAIL_COUNT>";
		$sReviewDetailXml ="<REVIEW_DETAIL>".$sReviewDetail."</REVIEW_DETAIL>";
	}else if($reviewName=='userreview'){
		if($user_review_id!=''){
			//used to get full review.
			/*
			$aViewsParameters = Array("item_id"=>$user_review_id,"turl"=>$sRequestUrl,"cid"=>USER_REVIEW_MODEL_CATEGORY_ID,"sid"=>SERVICEID);
			$views_result = $report->intInsertUpdateViewsCount($aViewsParameters);
			$views_count = $views_result;
			*/

			$result = $userreview->arrGetUserReviewDetails($user_review_id,"","","","",$brand_id,$category_id,$product_name_id,$product_id,"1",$startlimit,$cnt);
			$cnt = sizeof($result);
			$xml = "<USER_REVIEW_MASTER>";
			$xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
			
			for($i=0;$i<$cnt;$i++){
				$user_review_id = $result[$i]['user_review_id'];
				
				

				//$result[$i]['views_count'] = ($views_count != '0') ? $views_count : '';

				$brand_id = $result[$i]['brand_id'];
				$model_id = $result[$i]['product_info_id'];
				$product_id = $result[$i]['product_id'];
				$category_id = $result[$i]['category_id'];
				$create_date = $result[$i]['create_date'];
				$user_name = $result[$i]['user_name'];
				$title = $result[$i]['title'];
                            $disp_title_name=$title;
				if($title != ""){
					$title = html_entity_decode($title,ENT_QUOTES,'UTF-8');
					$result[$i]['title'] = $title;
				}
				if($user_name != ""){
					$user_name = html_entity_decode($user_name,ENT_QUOTES,'UTF-8');
					$result[$i]['user_name'] = $user_name;
				}
				$result[$i]['create_date'] = date('d/m/Y',strtotime($create_date));
				if(!empty($brand_id)){
					$brand_result = $oBrand->arrGetBrandDetails($brand_id,$category_id);
					$brand_name = $brand_result[0]['brand_name'];				
					$modelArr[] = $brand_name;
				}
				$result[$i]['brand_name'] = $brand_name;
				if(!empty($model_id)){
					$product_result = $oProduct->arrGetProductNameInfo($product_name_id);
					$model_name = $product_result[0]['product_info_name'];
					$modelArr[] = $model_name;
				}			
				$result[$i]['model_name'] = $model_name;
				$result[$i]['brand_model_name'] = implode(" ",$modelArr);
				if(!empty($product_id)){
					$product_result = $oProduct->arrGetProductDetails($product_id);
					$variant = $product_result[0]['variant'];
					$modelArr[] = $variant_name;
				}
				$result[$i]['brand_model_variant_name'] = implode(" ",$modelArr);
				unset($modelArr);
				$result[$i]['variant'] = $variant;

				$product_model_name = html_entity_decode($model_name,ENT_QUOTES,'UTF-8');
				$product_model_name = removeSlashes($product_model_name);
				$product_model_name = seo_title_replace($product_model_name);
				
				unset($seoTitleArr);
				$seoTitleArr[] = SEO_WEB_URL;
				$seoTitleArr[] = $product_brand_name."-cars";
				$seoTitleArr[] = $product_link_name;
				$seoTitleArr[] = "reviews-ratings";
				$seoTitleArr[] = SEO_CARS_MODEL_FULLREVIEWS; 
				$seoTitleArr[] = $product_model_name;
				$seoTitleArr[] = $product_name_id;
				$seoTitleArr[] = $user_review_id;
				$seo_url = implode("/",$seoTitleArr);

				$views_page_name = $seo_url;

				unset($seoTitleArr);
				$seoTitleArr[] = SEO_WEB_URL;
				$seoTitleArr[] = $product_brand_name."-cars";
				$seoTitleArr[] = $product_link_name;
				$seoTitleArr[] = "reviews-ratings";
				$seoTitleArr[] = SEO_CARS_MODEL_FULLREVIEWS; 
				$seoTitleArr[] = $product_model_name;
				$seoTitleArr[] = $product_name_id;
				$seoTitleArr[] = $user_review_id;
				$seo_url1 = implode("/",$seoTitleArr);
				$aViewCntUrl[$seo_url] = $result[$i]['user_review_id'];
				$aEncViewCntUrl[$seo_url1] = $result[$i]['user_review_id'];
				
				$aViewCnt=Array();
				$aViewCnt = $report->getPageViews($aViewCntUrl,$aEncViewCntUrl);

				//print "<pre>"; print_r($aViewCnt);
				$views_count = $aViewCnt[$result[$i]['user_review_id']];
				$result[$i]['views_count'] = $aViewCnt[$result[$i]['user_review_id']];
				
				$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
				$xml .= "<USER_REVIEW_MASTER_DATA>";
				foreach($result[$i] as $k=>$v){
					$xml .= "<$k><![CDATA[$v]]></$k>";
				}
				$xml .= "</USER_REVIEW_MASTER_DATA>";
			}
			$xml .= "</USER_REVIEW_MASTER>";

			//print "<pre>"; print_r($aViewCnt); 

			//get user qna
			$result = $userreview->arrGetUserQnA('','',$user_review_id,"1");
			$cnt = sizeof($result);
			$xml .= "<USER_REVIEW_ANSWER_MASTER>";
			$xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
			for($i=0;$i<$cnt;$i++){
				unset($que_result);
				$que_id = $result[$i]['que_id'];
				$que_result = $userreview->arrGetQuestions($que_id);
				$result[$i]['quename'] = $que_result[0]['quename'];
				$answer = $result[$i]['answer'];
				$ansArr = explode(",",$answer);
				$gradeCnt = $result[$i]['grade'];
				//echo $gradeCnt." & ".$que_id."<br/>";
				$html = "";
				for($grade=1;$grade<=5;$grade++){
					if($grade <= $gradeCnt){
						$html .= '<img src="'.IMAGE_URL.'spacer.gif" class="vsblStr mlr1"/>';
					}else{
						$html .= '<img src="'.IMAGE_URL.'spacer.gif" class="dsblStr mlr1"/>';
					}
				}
				$result[$i]['grade'] = $html;
				$ans_result = $userreview->arrGetQueAnswer("",$que_id);
				$anscnt = sizeof($ans_result);		
				$xml .= "<USER_REVIEW_ANSWER_MASTER_DATA>";
				$xml .= "<QUE_ANSWER_MASTER>";
				$xml .= "<COUNT><![CDATA[$anscnt]]></COUNT>";
				if($anscnt > 0){
					for($ans=0;$ans<$anscnt;$ans++){
						$ans_id = trim($ans_result[$ans]['ans_id']);					
						$html = "";
						$ansCnt = $ansArr[$ans];
						for($grade=1;$grade<=5;$grade++){
							if($grade <= $ansCnt){
								$html .= '<img src="'.IMAGE_URL.'spacer.gif" class="starOn"/>';
							}else{
								$html .= '<img src="'.IMAGE_URL.'spacer.gif" class="starOff"/>';
							}
						}
						$ans_result[$ans]['selected_answer'] = $html;

						$ans_result[$ans] = array_change_key_case($ans_result[$ans],CASE_UPPER);
						$xml .= "<QUE_ANSWER_MASTER_DATA>";
						foreach($ans_result[$ans] as $key=>$val){
							$xml .= "<$key><![CDATA[$val]]></$key>";
						}
						$xml .= "</QUE_ANSWER_MASTER_DATA>";
					}
				}
				$xml .= "</QUE_ANSWER_MASTER>";

				$result[$i] = array_change_key_case($result[$i],CASE_UPPER);			
				foreach($result[$i] as $k=>$v){
					$xml .= "<$k><![CDATA[$v]]></$k>";
				}
				$xml .= "</USER_REVIEW_ANSWER_MASTER_DATA>";
			}
			$xml .= "</USER_REVIEW_ANSWER_MASTER>";

			$result = $userreview->arrGetUserQnA('','',$user_review_id,"0","1");
			$cnt = sizeof($result);
			$xml .= "<USER_REVIEW_COMMENT_ANSWER_MASTER>";
			for($i=0;$i<$cnt;$i++){
				$que_id = $result[$i]['que_id'];
				$que_result = $userreview->arrGetQuestions($que_id);			
				$result[$i]['quename'] = $que_result[0]['quename'];
				$result[$i]['answer'] = nl2br($result[$i]['answer']);

				$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
				$xml .= "<USER_REVIEW_COMMENT_ANSWER_MASTER_DATA>";
				foreach($result[$i] as $k=>$v){
					$xml .= "<$k><![CDATA[$v]]></$k>";
				}
				$xml .= "</USER_REVIEW_COMMENT_ANSWER_MASTER_DATA>";
			}
			$xml .= "</USER_REVIEW_COMMENT_ANSWER_MASTER>";
			//used to check admin rating.
			$result = $userreview->arrGetAdminOverallGrade($category_id,$brand_id,$product_id,$product_name_id);
			$cnt = sizeof($result);
			$overallcnt = 0;
			$overallavg = round($result[0]['overallgrade']);
			//$cnt = 0;
			if($cnt <= 0){
				$result = $userreview->arrGetOverallGrade($category_id,$brand_id,$product_id,$product_name_id);
				$overallcnt = $result[0]['totaloverallcnt'];
				$overallavg = round($result[0]['overallavg']);
			}
			
			$html = "";
			for($grade=1;$grade<=5;$grade++){
				if($grade <= $overallavg){
					$html .= '<img src="'.IMAGE_URL.'spacer.gif" class="vsblStr"/>';
				}else{
					$html .= '<img src="'.IMAGE_URL.'spacer.gif" class="dsblStr"/>';
				}
			}

			$resultoption = $userreview->GetUserReviewOptions("",$user_review_id,$category_id);
			$oCount=sizeof($resultoption);
			//print_r($resultoption);
			if($oCount>0){
				$like_yes = $resultoption[0]['like_yes'];
				$like_no = $resultoption[0]['like_no'];
				$optionhtml .= "<REVIEW_RATE_OPTION>";
				$optionhtml .= "<REVIEW_RATE_OPTION_YES>$like_yes</REVIEW_RATE_OPTION_YES>";
				$optionhtml .= "<REVIEW_RATE_OPTION_NO>$like_no</REVIEW_RATE_OPTION_NO>";
				$optionhtml .= "</REVIEW_RATE_OPTION>";
			}
			
		}else{
				//used to list model based user review.
				$product_model_name = html_entity_decode($product_info_name,ENT_QUOTES,'UTF-8');
				$product_model_name = removeSlashes($product_model_name);
				$product_model_name = seo_title_replace($product_model_name);
				
				//user review list
				$request_uri = $_SERVER['REQUEST_URI'];
				
				if(strpos($request_uri,"page=")){
					
				$_REQUEST['page'] = str_replace("/$product_brand_name-cars/$product_link_name/reviews-ratings/model-user-reviews/$product_model_name/$product_name_id?page=","",$_SERVER['REQUEST_URI']);
				}

				$curpage = $_REQUEST['page'] ? $_REQUEST['page'] : 1 ;	
				// paging code 
				 $request_uri = $_SERVER['REQUEST_URI'];
				 $chk_currk=explode('?page=',$request_uri);
				 $chk_currk_arr=$chk_currk[1];
				 if($chk_currk_arr){
				   $curpage=$chk_currk_arr;
				 }
				
				$limit = 10;
				$oPager = new Pager();
				$start1 = $oPager->findStart($limit);
				
				$count = $userreview->arrGetUserReviewDetailsCount("","","","","",$brand_id,$category_id,$product_name_id,$product_id,"1");
			
				$pages = $oPager->findPages($count, $limit);
				$siteUrl = substr(WEB_URL,0,-1).$_SERVER['PHP_SELF'];
				$sPagingXml = $oPager->pageNumNextPrev($curpage, $pages, $siteUrl, "");
				
				$result = $userreview->arrGetUserReviewDetails("","","","","",$brand_id,$category_id,$product_name_id,$product_id,"1",$start1,$limit);
				$cnt = sizeof($result);
				for($k=0;$k<$cnt;$k++){
					$user_review_id = $result[$k]['user_review_id'];
					$brand_id = $result[$k]['brand_id'];
					$model_id = $result[$k]['product_info_id'];
					$product_id = $result[$k]['product_id'];
					$category_id = $result[$k]['category_id'];
					if(!empty($brand_id)){
						$brand_result = $oBrand->arrGetBrandDetails($brand_id,$category_id);
						$brand_name = $brand_result[0]['brand_name'];				
						//$modelArr[] = $brand_name;
					}
					if(!empty($model_id)){
						$product_result = $oProduct->arrGetProductNameInfo($model_id);
						$model_name = $product_result[0]['product_info_name'];
						//$modelArr[] = $model_name;
					}			
					if(!empty($product_id)){
						$product_result = $oProduct->arrGetProductDetails($product_id);
						$variant = $product_result[0]['variant'];
						//$modelArr[] = $variant_name;
					}
					unset($modelArr);

					$product_model_name = html_entity_decode($model_name,ENT_QUOTES,'UTF-8');
					$product_model_name = removeSlashes($product_model_name);
					$product_model_name = seo_title_replace($product_model_name);	

					unset($seoTitleArr);
					$seoTitleArr[] = SEO_WEB_URL;
					$seoTitleArr[] = $product_brand_name."-cars";
					$seoTitleArr[] = $product_link_name;
					$seoTitleArr[] = "reviews-ratings";
					$seoTitleArr[] = SEO_CARS_MODEL_FULLREVIEWS; 
					$seoTitleArr[] = $product_model_name;
					$seoTitleArr[] = $model_id;
					$seoTitleArr[] = $user_review_id;
					$seo_url = implode("/",$seoTitleArr);

					unset($seoTitleArr);
					$seoTitleArr[] = SEO_WEB_URL;
					$seoTitleArr[] = $product_brand_name."-cars";
					$seoTitleArr[] = $product_link_name;
					$seoTitleArr[] = "reviews-ratings";
					$seoTitleArr[] = SEO_CARS_MODEL_FULLREVIEWS; 
					$seoTitleArr[] = $product_model_name;
					$seoTitleArr[] = $model_id;
					$seoTitleArr[] = $user_review_id;
					$seo_url1 = implode("/",$seoTitleArr);
					$aViewCntUrl[$seo_url] = $result[$k]['user_review_id'];
					$aEncViewCntUrl[$seo_url1] = $result[$k]['user_review_id'];

					$aMBList[] = $result[$k]['user_review_id'];

				}
				//get pageviews from API
				$aViewCnt=Array();
				$aViewCnt = $report->getPageViews($aViewCntUrl,$aEncViewCntUrl);

				$aComParameters = array(); $aMBData = array();
				$aComParameters = Array("title"=>implode(",",$aMBList),"cid"=>USER_REVIEW_MODEL_CATEGORY_ID,"sid"=>SERVICEID);
				$aMBData = $oCampusDiscussion->getMulThreadParentReplyCnt($aComParameters);

				$xml = "<USER_REVIEW_MASTER>";
				$xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
				for($i=0;$i<$cnt;$i++){
					$user_review_id = $result[$i]['user_review_id'];
					$SERVICEID = SERVICEID;
					$USER_REVIEW_MODEL_CATEGORY_ID = USER_REVIEW_MODEL_CATEGORY_ID;
					$comment_count = $aMBData['data'][$user_review_id][$USER_REVIEW_MODEL_CATEGORY_ID][$SERVICEID];
					if(!empty($comment_count) || $comment_count!=0){
						$result[$i]['comment_count'] = $comment_count;
					}
					if(isset($aViewCnt) && $aViewCnt[$user_review_id]!=''){
						$result[$i]['views_count'] = $aViewCnt[$user_review_id];
					}
					$brand_id = $result[$i]['brand_id'];
					$model_id = $result[$i]['product_info_id'];
					$product_id = $result[$i]['product_id'];
					$category_id = $result[$i]['category_id'];
					$create_date = $result[$i]['create_date'];
					$result[$i]['create_date'] = date('d/m/Y',strtotime($create_date));
					
					if(!empty($brand_id)){
						$brand_result = $oBrand->arrGetBrandDetails($brand_id,$category_id);
						$brand_name = $brand_result[0]['brand_name'];				
						$modelArr[] = $brand_name;
					}
					$result[$i]['brand_name'] = $brand_name;
					if(!empty($model_id)){
						$product_result = $oProduct->arrGetProductNameInfo($model_id);
						$model_name = $product_result[0]['product_info_name'];
						$modelArr[] = $model_name;
					}			
					$result[$i]['model_name'] = $model_name;
					$result[$i]['brand_model_name'] = implode(" ",$modelArr);
					if(!empty($product_id)){
						$product_result = $oProduct->arrGetProductDetails($product_id);
						$variant = $product_result[0]['variant'];
						$modelArr[] = $variant_name;
					}
					$result[$i]['brand_model_variant_name'] = implode(" ",$modelArr);
					unset($modelArr);
					
					$result[$i]['variant'] = $variant;
					$xml .= "<USER_REVIEW_MASTER_DATA>";

					$ratingresult = $userreview->arrGetUserQnA('','',$user_review_id,"1");
					$ratingcnt = sizeof($ratingresult);
					$xml .= "<USER_RATING_MASTER>";
					for($rating=0;$rating<$ratingcnt;$rating++){
						$que_id = $ratingresult[$rating]['que_id'];
						$que_result = $userreview->arrGetQuestions($que_id);				
						$ratingresult[$rating]['quename'] = $que_result[0]['quename'];
						$answer = $ratingresult[$rating]['answer'];
						$ansArr = explode(",",$answer);
						$gradeCnt = $ratingresult[$rating]['grade'];
						$html = "";
						for($grade=1;$grade<=5;$grade++){
							if($grade <= $gradeCnt){
								$html .= '<img src="'.IMAGE_URL.'spacer.gif" class="starOn"/>';
							}else{
								$html .= '<img src="'.IMAGE_URL.'spacer.gif" class="starOff"/>';
							}
						}
						$ratingresult[$rating]['grade'] = $html;				
						$xml .= "<USER_RATING_MASTER_DATA>";
						$ratingresult[$rating] = array_change_key_case($ratingresult[$rating],CASE_UPPER);
						foreach($ratingresult[$rating] as $k=>$v){
							$xml .= "<$k><![CDATA[$v]]></$k>";
						}
						$xml .= "</USER_RATING_MASTER_DATA>";
					}
					$xml .= "</USER_RATING_MASTER>";
					
					$reviewresult = $userreview->arrGetUserQnA('','',$user_review_id,"0","1","0","1"); // for comment
					$reviewcnt = sizeof($reviewresult);
					$xml .= "<USER_REVIEW_COMMENT_ANSWER_MASTER>";
					for($review=0;$review<$reviewcnt;$review++){
						$que_id = $reviewresult[$review]['que_id'];
						$answer = $reviewresult[$review]['answer'];
						$answer = removeSlashes($answer);                
						$answer = html_entity_decode($answer,ENT_QUOTES,'UTF-8');
						if(strlen($answer)>100){ $answer = getCompactString($answer, 95).' ...'; }
						$reviewresult[$review]['answer'] = $answer;
						$que_result = $userreview->arrGetQuestions($que_id);			
						$reviewresult[$review]['quename'] = $que_result[0]['quename'];

						$reviewresult[$review] = array_change_key_case($reviewresult[$review],CASE_UPPER);
						$xml .= "<USER_REVIEW_COMMENT_ANSWER_MASTER_DATA>";
						foreach($reviewresult[$review] as $k=>$v){
							$xml .= "<$k><![CDATA[$v]]></$k>";
						}
						$xml .= "</USER_REVIEW_COMMENT_ANSWER_MASTER_DATA>";
					}
					$xml .= "</USER_REVIEW_COMMENT_ANSWER_MASTER>";


				
					$product_model_name = html_entity_decode($model_name,ENT_QUOTES,'UTF-8');
					$product_model_name = removeSlashes($product_model_name);
					$product_model_name = seo_title_replace($product_model_name);	

					//seo user review
					unset($seoTitleArr);
					$seoTitleArr[] = SEO_WEB_URL;
					$seoTitleArr[] = $product_brand_name."-cars";
					$seoTitleArr[] = $product_link_name;
					$seoTitleArr[] = "reviews-ratings";
					$seoTitleArr[] = SEO_CARS_MODEL_FULLREVIEWS; 
					$seoTitleArr[] = $product_model_name;
					$seoTitleArr[] = $model_id;
					$seoTitleArr[] = $user_review_id;
					$result[$i]['user_review_seo_url'] = implode("/",$seoTitleArr);

					$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
					
					foreach($result[$i] as $k=>$v){
						$xml .= "<$k><![CDATA[$v]]></$k>";
					}
					$xml .= "</USER_REVIEW_MASTER_DATA>";

				}
				$xml .= "</USER_REVIEW_MASTER>";
				
				//used to check admin rating.
				$result = $userreview->arrGetAdminOverallGrade($category_id,$brand_id,'0',$model_id);
				$cnt = sizeof($result);
				$overallcnt = 0;
				$overallavg = round($result[0]['overallgrade']);
				
				if($cnt <= 0){
					$result = $userreview->arrGetOverallGrade($category_id,$rating_brand_id,'0',$product_name_id);
					$overallcnt = $result[0]['totaloverallcnt'];
					$overallavg = round($result[0]['overallavg']);
				}
				
				$html = "";
				for($grade=1;$grade<=5;$grade++){
					if($grade <= $overallavg){
						$html .= '<img src="'.IMAGE_URL.'spacer.gif" class="vsblStr"/>';
					}else{
						$html .= '<img src="'.IMAGE_URL.'spacer.gif" class="dsblStr"/>';
					}
				}
		}	
	}
	///echo "TIME 4====".date('l jS \of F Y h:i:s A')."<br>"; //die();
	$aReviewGroupData = $oReview->arrGetReviewsGroupDetails("","",$category_id,"1");
	$sReviewsGroupDet=arraytoxml($aReviewGroupData,"REVIEW_GROUP_DETAIL_DATA");
	$strRevGroupxml ="<REVIEW_GROUP_DETAIL>".$sReviewsGroupDet."</REVIEW_GROUP_DETAIL>";
			
	// group ddl
	
	$aReviewIds = $oReview->getReviewsDetails("","","","",$product_name_id,$category_id,"",1,"","","");
	$i=0;
	foreach($aReviewIds as $rikey=>$rivalue){
		if($rivalue['product_info_id']!='' && $rivalue['product_id']==0){
			$arrRevId[$i]['review_id'] = $rivalue['review_id'];
			$arrRevId[$i]['group_id'] = $rivalue['group_id'];
			$i++;
		}
	}
	//print "<pre>"; print_r($arrRevId);
	$sRevIdDet=arraytoxml($arrRevId,"REVIEW_ID_DATA");
	$sRevIdDetXML ="<REVIEW_ID_DETAIL>".$sRevIdDet."</REVIEW_ID_DETAIL>";
	
	$aBrandDetail=$oBrand->arrGetBrandDetails("",$category_id);
	$sBrandDataDet=arraytoxml($aBrandDetail,"BRAND_DETAIL_DATA");
	$sBrandDataDetXML ="<BRAND_DETAIL>".$sBrandDataDet."</BRAND_DETAIL>";
	if(is_array($aBrandDetail)){
		foreach($aBrandDetail as $ibKey=>$aBrandData){
			$aBrandDetailName[$aBrandData['brand_id']][]=$aBrandData['brand_name'];
		}
	}
	
	//product model detail
	$aProductDetail=$oProduct->arrGetProductByName($product_info_name);
	//print"<pre>";print_r($aProductDetail);print"</pre>";
	if(is_array($aProductDetail)){
		$iCnt=sizeof($aProductDetail);
		$sProductVersionDetXML = "<PRODUCT_VERSION_DETAIL>";
		
		for($i=0;$i<$iCnt;$i++){
			$version_brand_id=$aProductDetail[$i]['brand_id'];
			$aBrandDetail=$oBrand->arrGetBrandDetails($version_brand_id,$category_id);
			$brand_name=$aBrandDetail['0']['brand_name'];
			$product_name=$aProductDetail[$i]['product_name'];
			$product_id=$aProductDetail[$i]['product_id'];
			if(!empty($selected_city_id)){
				$price_result = $oPrice->arrGetPriceDetails(1,$product_id,$category_id,"","",$selected_city_id,"1","","","");
                        }else{
				$price_result = $oPrice->arrGetPriceDetails(1,$product_id,$category_id,"","","","1","","","1");
                        }
			//print "<pre>"; print_r($price_result);
			//echo "TEST END<br>";
			if($price_result[0]['variant_value']!=0){
			$product_ids[]=$aProductDetail[$i]['product_id'];
			$variant=$aProductDetail[$i]['variant'];
			$aProductDetail[$i]['brand_name']=$brand_name;
			$aOverview = $oFeature->arrGetSummary($category_id,$product_id,$type="array");
			//echo $aOverview;exit;
			//print "<pre>"; print_r($aOverview);

			foreach($aOverview as $key=>$val){
				if(!strpos($key,'Price') && !strpos($key,'Feature') ){
					unset($overviewArr);
					$css_pos = 1;
					//print_r($aOverview[$key]);
					//$aOverview[$key] = array_reverse($aOverview[$key]);
					//print_r($aOverview[$key]);EXIT;
					foreach($aOverview[$key] as $overviewtitle=>$overview_value){
						$techspecxml .= "<TECH_SPEC_DATA>";
						/*if(($overviewtitle!='main_group_name') && ($overviewtitle!='main_group_id')){
							//if ($css_pos % 2) { $css_style = 'fl'; }else{ $css_style = 'fl w70'; }
							if($css_pos == 1) { $css_style = 'fl'; }else{ $css_style = 'fl w70'; }
							$overviewArr[] = "<span class=\"b\">$overviewtitle".":&#160;"."</span>".implode(",&#160;",$overview_value);
							$css_pos++;
						}*/
						$techspecxml .= "<FEATURE_TITLE><![CDATA[$overviewtitle]]></FEATURE_TITLE>";
						$techspecxml .= "<FEATURE_VALUE><![CDATA[".implode(",&#160;",$overview_value)."]]></FEATURE_VALUE>";
						$techspecxml .= "</TECH_SPEC_DATA>";
					}
				}
				if(strpos($key,'Feature') ){
					unset($overviewArr);
					$css_pos = 1;
					//$aOverview[$key] = array_reverse($aOverview[$key]);
					//print_r($aOverview[$key]);
					foreach($aOverview[$key] as $overviewtitle=>$overview_value){
						$featurespecxml .= "<FEATURE_SPEC_DATA>";
						/*if(($overviewtitle!='main_group_name') && ($overviewtitle!='main_group_id')){
							//if ($css_pos % 2) { $css_style = 'fl'; }else{ $css_style = 'fl w70'; }
							if($css_pos == 1) { $css_style = 'fl'; }else{ $css_style = 'fl w70'; }
							$overviewArr[] = "<span class=\"b\">$overviewtitle".":&#160;"."</span>".implode(",&#160;",$overview_value);
							$css_pos++;
						}*/
						$featurespecxml .= "<FEATURE_TITLE><![CDATA[$overviewtitle]]></FEATURE_TITLE>";
						$featurespecxml .= "<FEATURE_VALUE><![CDATA[".implode(",&#160;",$overview_value)."]]></FEATURE_VALUE>";
						$featurespecxml .= "</FEATURE_SPEC_DATA>";
					}
				}
			}

			
			$aProductDetail[$i]['tech_spec_short_desc'] = $techspecxml;
			unset($techspecxml);
			$aProductDetail[$i]['feature_spec_short_desc'] = $featurespecxml;
			unset($featurespecxml);
			$overviewArr="";
			$media_path=$aProductDetail[$i]['image_path'];
			if(!empty($media_path)){
				//$media_path = resizeImagePath($media_path,"555X416",$aModuleImageResize);
				$media_path = CENTRAL_IMAGE_URL.$media_path;
			}
			$aProductDetail[$i]['image_path'] = $media_path ;
			
			$sim_brand_name = html_entity_decode($brand_name,ENT_QUOTES,'UTF-8');
			$sim_brand_name = removeSlashes($sim_brand_name);
			$sim_brand_name = seo_title_replace($sim_brand_name);
			
			$sim_link_name=	$sim_brand_name."-".$product_name;
			$sim_link_name = html_entity_decode($sim_link_name,ENT_QUOTES,'UTF-8');
			$sim_link_name = removeSlashes($sim_link_name);
			$sim_link_name = seo_title_replace($sim_link_name);

			$sim_variant_name = html_entity_decode($variant,ENT_QUOTES,'UTF-8');
			$sim_variant_name = removeSlashes($sim_variant_name);
			$sim_variant_name = seo_title_replace($sim_variant_name);
			
			unset($seoTitleArr);
			$seoTitleArr[] = SEO_WEB_URL;
			$seoTitleArr[] = trim($sim_brand_name)."-cars";
			$seoTitleArr[] = trim($sim_link_name);
			$seoTitleArr[] = trim($sim_variant_name);
			$seoTitleArr[] = SEO_GET_ON_ROAD_PRICE ; //"Get-On-Road-Price";
			$seoTitleArr[] = $product_id;
			$seo_url1 = implode("/",$seoTitleArr);
			$aProductDetail[$i]['seo_url'] = trim($seo_url1);

			if(!empty($selected_city_id)){
				$aPriceDetail = $oPrice->arrGetPriceDetails("1",$product_id,$category_id,"","",$selected_city_id,"1","","","");
			}else{
				$aPriceDetail = $oPrice->arrGetPriceDetails("1",$product_id,$category_id,"","","","1","","","1");
			}
			//print_r($aPriceDetail); //die();
			if(is_array($aPriceDetail)){
				$sExShowRoomPrice=$aPriceDetail[0]['variant_value'];
				$iCity_id=$aPriceDetail[0]['city_id'];
				$iCity_name = $aPriceDetail[0]['city_name'];
				$aProductDetail[$i]['EXSHOWROOM_ORGINAL'] = $sExShowRoomPrice;
				$aProductDetail[$i]['EXSHOWROOM'] = $sExShowRoomPrice ? priceFormat($sExShowRoomPrice) : '';
				$aPriceRange[]=$sExShowRoomPrice;
				$format_price = $sExShowRoomPrice ? priceFormat($sExShowRoomPrice) : '';
				$aproducts[$sExShowRoomPrice]=$aPriceDetail[0]['product_id'];
			}
			//echo $iCity_id."<br>";
			$result = $oFeature->arrGetFeatureMainGroupDetails("",$category_id,"",$startlimit,$limitcnt);
			$cnt=sizeof($result);
				$sSeoUrlReviewsDetXml = "<REVIEW_SEO_URL_DETAIL>";
				for($j=0;$j<$cnt;$j++){
					unset($seoTitleArr);
					$seoTitleArr[] = SEO_WEB_URL;
					$seoTitleArr[] = $sim_brand_name."-cars";
					$seoTitleArr[] = $sim_link_name;
					$seoTitleArr[] = $sim_variant_name;
					$group_id=$result[$j]['group_id'];
					if(!empty($group_id)){
						if($group_id==1){
							$seoTitleArr[] = 'Overviews';
						}
						if($group_id==2){
							$seoTitleArr[] = 'Features';
						}
						if($group_id==3){
							$seoTitleArr[] = 'Tech-specification';
						}
					}
					$seoTitleArr[] = $product_id;
					if($group_id!=1){
						$seoTitleArr[] = $group_id;
					}
					$seo_url = implode("/",$seoTitleArr);
					
					
					$main_group_name=$result[$j]['main_group_name'];
					if($main_group_name!='Overview'){
						$sSeoUrlReviewsDetXml .= "<REVIEW_SEO_URL_DATA>";
						$sSeoUrlReviewsDetXml .= "<MAIN_GROUP_NAME><![CDATA[$main_group_name]]></MAIN_GROUP_NAME>";
						$sSeoUrlReviewsDetXml .= "<SEO_URL><![CDATA[$seo_url]]></SEO_URL>";
						$sSeoUrlReviewsDetXml .= "</REVIEW_SEO_URL_DATA>";
					}
					if($main_group_name=='Overview'){
						$sSeoUrlReviewsDetXml .= "<OVREVIEW_SEO_URL_DATA>";
						$sSeoUrlReviewsDetXml .= "<MAIN_GROUP_NAME><![CDATA[$main_group_name]]></MAIN_GROUP_NAME>";
						$sSeoUrlReviewsDetXml .= "<SEO_URL><![CDATA[$seo_url]]></SEO_URL>";
						$sSeoUrlReviewsDetXml .= "</OVREVIEW_SEO_URL_DATA>";
					}
				}
				$sSeoUrlReviewsDetXml .= "</REVIEW_SEO_URL_DETAIL>";
				$aProductDetail[$i] = array_change_key_case($aProductDetail[$i],CASE_UPPER);
				$sProductVersionDetXML .= "<PRODUCT_VERSION_DETAIL_DATA>";
				foreach($aProductDetail[$i] as $k => $v){					
					$sProductVersionDetXML .= ($k == 'TECH_SPEC_SHORT_DESC' || $k == 'FEATURE_SPEC_SHORT_DESC') ? "<$k>$v</$k>" : "<$k><![CDATA[$v]]></$k>";
				}
				$sProductVersionDetXML .=$sSeoUrlReviewsDetXml;
				$sProductVersionDetXML .= "</PRODUCT_VERSION_DETAIL_DATA>";
				}
			}
		$sProductVersionDetXML .= "</PRODUCT_VERSION_DETAIL>";
	}


	//exit;
	sort($aPriceRange);
	$lowPrice=$aPriceRange[0];
	if(count($aPriceRange)>1){
		$highPrice=$aPriceRange[count($aPriceRange)-1];
	}

	$lowprice_product_id = $aproducts[$lowPrice];
	$lowPrice = priceFormat($lowPrice);
	$highPrice = priceFormat($highPrice);

	$aProductInfoDetails['0']['low_price']=$lowPrice;
	$aProductInfoDetails['0']['high_price']=$highPrice;
	
	$product_brand_name=str_replace("","-",$product_brand_name);
	$product_link_name=str_replace("","-",$product_link_name);
	$product_link_info_name=str_replace("","-",$product_info_name);

	
	$disp_title .=$product_brand_name." ".$product_link_info_name;

	unset($seoTitleArr);
	$seoTitleArr[] = SEO_WEB_URL;
	$seoTitleArr[] = trim($product_brand_name)."-cars";
	$seoTitleArr[] = trim($product_link_name);
	$seoTitleArr[] = SEO_GET_ON_ROAD_PRICE; //"Get-Car-Info";
	$seoTitleArr[] = trim($product_link_info_name);
	$seo_url = implode("/",$seoTitleArr);
	$aProductInfoDetails['0']['seo_url'] = trim($seo_url);

	$aProductInfoDetails['0']['link_product_name']=str_replace("-"," ",$product_link_name);
	$sProductInfoDet=arraytoxml($aProductInfoDetails,"PRODUCT_INFO_DETAIL_DATA");
	$sProductInfoDetXml ="<PRODUCT_INFO_DETAIL>".$sProductInfoDet."</PRODUCT_INFO_DETAIL>";
	
	
	$sOverviewXML = $oFeature->arrGetSummary($category_id,$product_id,$type="xml");
	$aParamaters=array("category_id"=>$category_id,"brand_id"=>$brand_id);
	
	$city_available=$oPrice->arrGetVariantValueDetail("","1",$product_ids,$category_id,"","","","1","","","");
	if(is_array($city_available)){
		foreach($city_available as $ikey=>$Value){
			$city_ids[]=$Value['city_id'];
		}
		//print_r($city_ids);
		$aCityDet = $oCityState->arrGetCityDetails($city_ids,$state_id,$startlimit,$limitcnt);
	}

	$sCityDet=arraytoxml($aCityDet,"CITY_DATA");
	$sCityDetXml ="<CITY_DETAIL>".$sCityDet."</CITY_DETAIL>";
	
	//top competitor section start//
	if(is_array($aBrandDetailName) && isset($aBrandDetailName[$product_info_brand_id])){
		$sModelBrandName=$aBrandDetailName[$product_info_brand_id][0];
	}
	//$aProductCompetitorDetail=$oProduct->arrGetProductCompetitorDetails("","",$product_info_brand_id,$category_id,"1",0,4);
	$aProductCompetitorDetail=$oProduct->arrGetProdCompetitorDetails("","0",$sproduct_name_id,'',$category_id,"1","0","3");
	//echo $sproduct_name_id."TEST<br>";
	//print"<pre>";print_r($aProductCompetitorDetail);print"</pre>";
	$j=0;
	if(is_array($aProductCompetitorDetail)){
		foreach($aProductCompetitorDetail as $iKey=>$aValue){
				
			$aproduct_info_ids=$aValue['product_info_ids'];
			$aProductInfoDetail=$oProduct->arrGetProductNameInfo($aproduct_info_ids,$category_id,"","","1","","");
			$aproduct_info_name=$aProductInfoDetail[0]['product_info_name'];
			//echo "TEST--";  print_r($aProductInfoDetail);
			//$aProductDataDetail[$iKey]=$aProductInfoDetail[0];
			
			$aProductName = $oProduct->arrGetProductByName($aproduct_info_name,"","","1","","");
			if(is_array($aProductName)){
				foreach($aProductName as $ilKey=>$pValue){
					$aproduct_ids[]=$pValue['product_id'];
					$aTopCompt[$pValue['product_id']][]=$pValue;
				}
			}
			$sProductIds=implode(",",$aproduct_ids);
			unset($aproduct_ids);
			if(!empty($selected_city_id)){
				$aPriceVariantData=$oPrice->arrGetVariantValueDetail("","1",$sProductIds,$category_id,"","",$selected_city_id,"1",$startlimit,$cnt,"");
                        }else{
				$aPriceVariantData=$oPrice->arrGetVariantValueDetail("","1",$sProductIds,$category_id,"","","","1",$startlimit,$cnt,"1");
                        }
			//print"<pre>";print_r($aPriceVariantData);print"</pre>";
			if(is_array($aPriceVariantData)){
				foreach($aPriceVariantData as $iprKey=>$aPriceVariantDet){
					$aProductPriceInfo[]=$aPriceVariantDet['variant_value'];
				}
				$aProductDataDetail[$j]=$aProductInfoDetail[0];
				$aProductDataDetail[$j]['price_val']=$aProductPriceInfo;
				$j++;
			}
			unset($aProductPriceInfo);
		}
	}	

	

	$sProductIds='';
	$aCompProductData=array();
//print"<pre>";print_r($aProductDataDetail);print"</pre>";

if(is_array($aProductDataDetail)){
	foreach($aProductDataDetail as $ipKey=>$aProductValueData){
		$iProductID=$aProductValueData['product_name_id'];
		$sProductName=$aProductValueData['product_info_name'];
		$icBrandId=$aProductValueData['brand_id'];
		//$variant=$aProductValueData['variant'];
		$aCompProductData[$ipKey]=$aProductValueData;			
		$sImagePath=$aProductValueData['image_path'];
		if(!empty($sImagePath)){
			$sImagePath = resizeImagePath($sImagePath,"87X65",$aModuleImageResize);
			$sImagePath = CENTRAL_IMAGE_URL.$sImagePath;
		}
		$aCompProductData[$ipKey]['image_path']=$sImagePath;
		if(is_array($aBrandDetailName) && isset($aBrandDetailName[$icBrandId])){
			$sBrandName=$aBrandDetailName[$icBrandId][0];
			$sDisplayName=$sBrandName." ".$sProductName;
			$aCompProductData[$ipKey]['display_name']=$sDisplayName;
		}
		$aProductPriceInfo=$aProductValueData['price_val'];
		if(is_array($aProductPriceInfo)){
			$aPriceVariantValue = $aProductPriceInfo;
			sort($aPriceVariantValue);
			$EndCountprice=(count($aPriceVariantValue)-1);
			if(count($aPriceVariantValue)>1){$endCount=$EndCountprice;}else{$endCount=1;}
			$aCompProductData[$ipKey]['low_price_range_orginal'] = $aPriceVariantValue[0];
			$aCompProductData[$ipKey]['low_price_range'] = $aPriceVariantValue[0] ? priceFormat($aPriceVariantValue[0]) : '';
			$aCompProductData[$ipKey]['high_price_range_orginal'] = $aPriceVariantValue[$endCount];
			$aCompProductData[$ipKey]['high_price_range'] = $aPriceVariantValue[$endCount] ? priceFormat($aPriceVariantValue[$endCount]) : '';
		}

		$sim_brand_name = html_entity_decode($sBrandName,ENT_QUOTES,'UTF-8');
		$sim_brand_name = removeSlashes($sim_brand_name);
		$sim_brand_name = seo_title_replace($sim_brand_name);

		$sim_link_name=$sim_brand_name."-".$sProductName;
		$sim_link_name = html_entity_decode($sim_link_name,ENT_QUOTES,'UTF-8');
		$sim_link_name = removeSlashes($sim_link_name);
		$sim_link_name = seo_title_replace($sim_link_name);
		unset($seoTitleArr);
		$seoTitleArr[] = SEO_WEB_URL;
		$seoTitleArr[] = trim($sim_brand_name)."-cars";
		$seoTitleArr[] = trim($sim_link_name);
		$seoTitleArr[] = 'Model';
		$seoTitleArr[] = trim($sProductName);
		$seoTitleArr[] = $iProductID;
		$seo_url = implode("/",$seoTitleArr);
		$aCompProductData[$ipKey]['seo_url'] = trim($seo_url);
		unset($seoTitleArr);
		$seoTitleArr[] = SEO_WEB_URL;
		$seoTitleArr[] = trim($sim_brand_name)."-cars";
		$seoTitleArr[] = trim($sim_link_name);
		$seoTitleArr[] = SEO_GET_ON_ROAD_PRICE ; //"Get-On-Road-Price";
		$seoTitleArr[] = trim($sProductName);
		$seo_url1 = implode("/",$seoTitleArr);
		$aCompProductData[$ipKey]['seo_url1'] = trim($seo_url1);
		$aCompProductData[$ipKey] = array_change_key_case($aCompProductData[$ipKey],CASE_UPPER);
		}
		//print_r($aCompProductData);
	$sComptProductDet=arraytoxml($aCompProductData,"COMPETITOR_PRODUCT_DETAIL_DATA");
	$sTopCopmetitorsListing ="<COMPETITOR_PRODUCT_DETAIL>".$sComptProductDet."</COMPETITOR_PRODUCT_DETAIL>";
}
	//top competitor section End//
	//die();
	
	$aWallpapers = $oWallpapers->arrGetWallpapersDetails("","",$product_name_id,$category_id,"",'1',0,4,"create_date");
	
	$wCnt=sizeof($aWallpapers);
	$sWallpapersDetXml .= "<WALLPAPER_DETAIL>";
	for($i=0;$i<$wCnt;$i++){
			$sImagePath=$aWallpapers[$i]['video_img_path'];
			
			/*if(!empty($sImagePath)){
				$sImagePath = CENTRAL_IMAGE_URL.$sImagePath;
			}*/
			$sImageMediaPath=$aWallpapers[$i]['media_path'];
			$media_id = $aWallpapers[$i]['media_id'];
			$video_img_id = $aWallpapers[$i]['video_img_id'];
			if(!empty($sImageMediaPath)){
				//$sImageMediaPath = resizeImagePath($sImageMediaPath,"160X120",$aModuleImageResize);
				$sImageMediaPath = CENTRAL_IMAGE_URL.str_replace(array(CENTRAL_MEDIA_URL,CENTRAL_IMAGE_URL),"",$sImageMediaPath);
			}

		        $aWallpapers[$i]['media_path'] = $sImageMediaPath;
			if($sImagePath != ""){				
				$sImagePath = resizeImagePath($sImagePath,"160X120",$aModuleImageResize,$media_id);
			}
			
			if(!empty($sImagePath)){
				$sImagePath = CENTRAL_MEDIA_URL.str_replace(array(CENTRAL_MEDIA_URL,CENTRAL_IMAGE_URL),"",$sImagePath);
			}

			$aWallpapers[$i]['video_img_path']=$sImagePath;
			$wallpaper_id = $aWallpapers[$i]['wallpaper_id'];

			unset($seoTitleArr);
                        $seoTitleArr[] = SEO_WEB_URL;
                        $seoTitleArr[] = SEO_CAR_WALLPAPER; //Car-Video-Featured
                        $seoTitleArr[] = trim(seo_title_replace($title));
                        $seoTitleArr[] = $wallpaper_id;
                        $seo_wallpaper_url = implode("/",$seoTitleArr);
			$aWallpapers[$i]['seo_wallpaper_url']=$seo_wallpaper_url;

			$aWallpapers[$i] = array_change_key_case($aWallpapers[$i],CASE_UPPER);

			$sWallpapersDetXml .= "<WALLPAPER_DETAIL_DATA>";
			foreach($aWallpapers[$i] as $k => $v){
				$sWallpapersDetXml .= "<$k><![CDATA[$v]]></$k>";
			}
			$sWallpapersDetXml .= "</WALLPAPER_DETAIL_DATA>";
	}
	$sWallpapersDetXml .= "</WALLPAPER_DETAIL>";
	

	$aProductNews = $oArticle->getNewsDetails("","","","",$product_name_id,$category_id,$brand_id,1,0,3," order by A.create_date desc");

	$newscnt=sizeof($aProductNews);
	$sArticleDetXml .= "<NEWS_MASTER>";
	$sArticleDetXml .= "<COUNT><![CDATA[$newscnt]]></COUNT>";
	for($k=0;$k<$newscnt;$k++){
		$title=$aProductNews[$k]['title'];
		$news_id=$aProductNews[$k]['article_id'];
		unset($seoTitleArr);
		$title1 = seo_title_replace($title);
		$seoTitleArr[] = WEB_URL.SEO_AUTO_NEWS_DETAIL;
		$seoTitleArr[] = $title1;
		$seoTitleArr[] = $news_id;
		$image_path = $aProductNews[$k]['image_path'];
		if(!empty($image_path)){
			$image_path = resizeImagePath($image_path,"87X65",$aModuleImageResize);			
		}
		$aProductNews[$k]['image_path'] = $image_path ? CENTRAL_IMAGE_URL.$image_path : IMAGE_URL.'no-image.png';
		$aProductNews[$k]['seo_url'] = implode("/",$seoTitleArr);
		$aProductNews[$k]['create_date'] = date('d M Y',strtotime($aProductNews[$k]['create_date']));
		$aProductNews[$k] = array_change_key_case($aProductNews[$k],CASE_UPPER);
		$sArticleDetXml .= "<NEWS_MASTER_DATA>";
		foreach($aProductNews[$k] as $key=>$val){
			$sArticleDetXml .= "<$key><![CDATA[".html_entity_decode($val,ENT_QUOTES,'UTF-8')."]]></$key>";
		}
		$sArticleDetXml .= "</NEWS_MASTER_DATA>";
	}
	$sArticleDetXml .= "</NEWS_MASTER>";

	//echo "TIME 7====".date('l jS \of F Y h:i:s A')."<br>"; //die();
	
	$aReviewsProduct = $oReview->getReviewsDetails("","","","",$product_name_id,$category_id,$brand_id,"1","0","3"," order by R.create_date desc");
	//print_r($aReviewsProduct);
	$reviewscnt=sizeof($aReviewsProduct);
	$sArticleDetXml .= "<REVIEWS_MASTER>";
	$sArticleDetXml .= "<COUNT><![CDATA[$reviewscnt]]></COUNT>";

	for($k=0;$k<$reviewscnt;$k++){
		$product_review_id = $aReviewsProduct[$k]['product_review_id'];
		$group_id = $aReviewsProduct[$k]['group_id'];
		$aReviewsProduct[$k]['content_type'] = 0;
		if($group_id == 1){
			$aReviewsProduct[$k]['content_type'] = 1;
			$review_vid_result = $oReview->arrGetUploadMediaReviewsDetails($product_review_id,1);
			//echo "DSDSDSD===".print_r($review_vid_result);
			$upload_media_id = $review_vid_result[0]['upload_media_id'];
			$media_id = $review_vid_result[0]['media_id'];
			$video_img_id = $review_vid_result[0]['video_img_id'];
			$media_path = $review_vid_result[0]['media_path'] ? CENTRAL_MEDIA_URL.$review_vid_result[0]['media_path'] : '';	
			$video_img_path = $review_vid_result[0]['video_img_path'] ? CENTRAL_IMAGE_URL.$review_vid_result[0]['video_img_path'] : '';
			$is_media_process = $review_vid_result[0]['is_media_process'];
			if(empty($is_media_process)){
				$upload_result = arrUpdateAudioVideo($media_id,$upload_media_id,"UPLOAD_MEDIA_REVIEWS",$video_img_path);
				if(sizeof($upload_result) > 0){
					$media_path = CENTRAL_MEDIA_URL.$upload_result['media_path'];
					$uploaded_video_img_path = $upload_result['video_img_path'];
					if(!empty($uploaded_video_img_path)){
						$uploaded_video_img_path = resizeImagePath($uploaded_video_img_path,"555X312",$aModuleImageResize,$video_img_id);
					}
					$video_img_path = $uploaded_video_img_path ? CENTRAL_IMAGE_URL.$uploaded_video_img_path : "";
				}
			}
			$videoArr = arrGetDifferentVideoResolution($media_path);
			$low_media_path = $videoArr['low_media_path'];	
			$normal_media_path = $videoArr['normal_media_path'];
			$aReviewsProduct[$k]['low_media_path'] = $low_media_path;
			$aReviewsProduct[$k]['normal_media_path'] = $normal_media_path;
			$aReviewsProduct[$k]['video_img_path'] = $video_img_path;
			$aReviewsProduct[$k]['media_path'] = $media_path;	
			if(!empty($upload_media_id)){
				$lang_video_arr = $videoGallery->getLanguageVideosDetails($upload_media_id,"3","","","","","",$category_id,"","1","","","");
				
				$lang_cnt = sizeof($lang_video_arr);
				$strmediaxml .= "<LANG_CNT><![CDATA[$lang_cnt]]></LANG_CNT>";					
				if(!empty($lang_cnt)){
					$language_video_id = $lang_video_arr[0]["language_video_id"];
					$lang_video_id = $lang_video_arr[0]["video_id"];
					$lang_video_type = $lang_video_arr[0]["video_type"];
					$language_id = $lang_video_arr[0]["language_id"];
					$lang_title = $lang_video_arr[0]["title"];
					$lang_tags = $lang_video_arr[0]["tags"];
					$lang_media_id = $lang_video_arr[0]["media_id"];
					$lang_media_path = $lang_video_arr[0]["media_path"];
					$lang_video_img_id = $lang_video_arr[0]["video_img_id"];
					$lang_video_img_path = $lang_video_arr[0]["video_img_path"];
					$lang_content_type = $lang_video_arr[0]["content_type"];
					$lang_is_media_process = $lang_video_arr[0]["is_media_process"];
					$lang_status = $lang_video_arr[0]["status"];
					$lang_is_media_process = $lang_video_arr[0]["is_media_process"];
					if(!empty($lang_is_media_process) && $lang_is_media_process!= '0'){
						$lang_available = 1;
					}else{
						$lang_available = 0;
						//start code to get processed audio/video.
						//////////////$upload_result = arrUpdateAudioVideo($lang_media_id,$lang_video_img_id,"LANGUAGE_VIDEO_GALLERY",$lang_video_img_path,$language_video_id);
						if(sizeof($upload_result) > 0){
							$lang_media_path = $upload_result['media_path'];
							$uploaded_lang_video_img_path = $upload_result['video_img_path'];
							$lang_video_img_path = $lang_video_img_path ? $lang_video_img_path : $uploaded_lang_video_img_path;
							$lang_available = 1;
						}
					}
					$lang_videoArr = arrGetDifferentVideoResolution($lang_media_path);
					$low_media_path = $lang_videoArr['low_media_path'];     
					$normal_media_path = $lang_videoArr['normal_media_path'];
					if($lang_content_type == 1){
						//for video.
						if(!empty($lang_video_img_path)){
							$lang_video_img_path = resizeImagePath($lang_video_img_path,"555X312",$aModuleImageResize,$lang_video_img_id);
						}
						$lang_video_arr[0]['media_path'] = $lang_media_path ? CENTRAL_MEDIA_URL.$lang_media_path : '';
						$lang_video_arr[0]['low_media_path'] = $low_media_path ? CENTRAL_MEDIA_URL.$low_media_path : '';
						$lang_video_arr[0]['normal_media_path'] = $normal_media_path ? CENTRAL_MEDIA_URL.$normal_media_path : '';
						$lang_video_arr[0]['video_img_path'] = $lang_video_img_path ? CENTRAL_IMAGE_URL.$lang_video_img_path : '';
					}
					$lang_video_arr[0] = array_change_key_case($lang_video_arr[0],CASE_UPPER);
					$strmediaxml .= "<LANGUAGE_VIDEO_DETAILS_DATA>";
					$strmediaxml .= "<IS_LANGUAGE_VID_AVAILABLE><![CDATA[$lang_available]]></IS_LANGUAGE_VID_AVAILABLE>";
					foreach($lang_video_arr[0] as $langkey=>$langval){
						$strmediaxml .= "<$langkey><![CDATA[$langval]]></$langkey>";
					}
					$strmediaxml .= "</LANGUAGE_VIDEO_DETAILS_DATA>";
				}
			}
		}
		$aReviewsProduct[$k]['is_media_process'] = $is_media_process ? $is_media_process : 0;
		$title = $aReviewsProduct[$k]['title'];
		$news_id = $aReviewsProduct[$k]['article_id'];
		unset($seoTitleArr);
		$review_brand_id = $aReviewsProduct[$k]['brand_id'];
		//$product_id = $aReviewsProduct[$k]['product_id'];
		
		$product_info_id = $aReviewsProduct[$k]['product_info_id'];

		$aBrandDetail = $oBrand->arrGetBrandDetails($review_brand_id,$category_id);
		$review_brand_name = $aBrandDetail[0]['brand_name'];
		$productNameInfo = $oProduct->arrGetProductNameInfo($product_info_id,$category_id,"","",1,"","");
		$review_model = $productNameInfo[0]['product_info_name'];

		$review_brand_name = html_entity_decode($review_brand_name,ENT_QUOTES,'UTF-8');
		$review_brand_name = removeSlashes($review_brand_name);
		$review_brand_name = seo_title_replace($review_brand_name);
	
		$review_model = html_entity_decode($review_model,ENT_QUOTES,'UTF-8');
		$review_model = removeSlashes($review_model);
		$review_model = seo_title_replace($review_model);
	
		unset($seoTitleArr);
		$seoTitleArr[] = SEO_WEB_URL;
		$seoTitleArr[] = $review_brand_name."-cars";
		$seoTitleArr[] = $review_brand_name."-".$review_model;
		$seoTitleArr[] = "reviews-ratings";
		$seoTitleArr[] = "on-cars-Model-reviews";
		$seoTitleArr[] = $review_model;
		$seoTitleArr[] = $product_info_id;
		$seoTitleArr[] = $aReviewsProduct[$k]['group_id'];
		$seoTitleArr[] = $aReviewsProduct[$k]['review_id'];
		$seo_url = implode("/",$seoTitleArr);
		$image_path = $aReviewsProduct[$k]['image_path'];
		if(!empty($image_path)){
			$image_path = resizeImagePath($image_path,"87X65",$aModuleImageResize);			
		}
		$aReviewsProduct[$k]['image_path'] = $image_path ? CENTRAL_IMAGE_URL.$image_path : IMAGE_URL.'no-image.png';
		$aReviewsProduct[$k]['seo_url'] = implode("/",$seoTitleArr);
		$aReviewsProduct[$k]['create_date'] = date('d M Y',strtotime($aReviewsProduct[$k]['create_date']));
		$aReviewsProduct[$k] = array_change_key_case($aReviewsProduct[$k],CASE_UPPER);
		$sArticleDetXml .= "<REVIEWS_MASTER_DATA>";
		foreach($aReviewsProduct[$k] as $key=>$val){
			$sArticleDetXml .= "<$key><![CDATA[".html_entity_decode($val,ENT_QUOTES,'UTF-8')."]]></$key>";
		}
		//start code to add language video.
			$sArticleDetXml .= $strmediaxml;
		//end code to add language video.
		$sArticleDetXml .= "</REVIEWS_MASTER_DATA>";
	}
	$sArticleDetXml .= "</REVIEWS_MASTER>";

	//echo "TIME 8====".date('l jS \of F Y h:i:s A')."<br>"; die();
	
	$product_brand_name=str_replace(" ","-",$product_brand_name);
	$product_link_name=str_replace(" ","-",$product_link_name);
	$product_link_info_name=str_replace(" ","-",$product_info_name);
	
	unset($seoTitleArr);
	if(!empty($product_link_info_name)){
		$seoTitleArr[] = SEO_WEB_URL;
		$seoTitleArr[] = $product_brand_name."-cars";
		$seoTitleArr[] = $product_link_name;
		$seoTitleArr[] = "reviews-ratings";
		$seoTitleArr[] = "Meta-Model-reviews";
		$seoTitleArr[] = $product_link_info_name;
		$seoTitleArr[] = $product_name_id;
		$expert_seo_url = implode("/",$seoTitleArr)."/";
	}	
	unset($seoTitleArr);
	$seoTitleArr[] = SEO_WEB_URL;
	$seoTitleArr[] = $product_brand_name."-cars";
	$seoTitleArr[] = $product_link_name;
	$seoTitleArr[] = "reviews-ratings";
	$seoTitleArr[] = "on-cars-Model-reviews";
	$seoTitleArr[] = $product_link_info_name;
	$seoTitleArr[] = $product_name_id;
	$seoTitleArr[] = $defRevGrpId ? $defRevGrpId : '0';
	$seoTitleArr[] = $defRevId ? $defRevId : '0';
	$oncars_seo_url = implode("/",$seoTitleArr);

	
	unset($seoTitleArr);
	$seoTitleArr[] = SEO_WEB_URL;
	$seoTitleArr[] = $product_brand_name."-cars";
	$seoTitleArr[] = $product_link_name;
	$seoTitleArr[] = 'Model';
	$seoTitleArr[] = $product_link_info_name;
	$seoTitleArr[] = $product_name_id;
	$seo_url = implode("/",$seoTitleArr);
	
	unset($seoTitleArr);
	$seoTitleArr[] = SEO_WEB_URL;
	$seoTitleArr[] = $product_brand_name."-cars";
	$seoTitleArr[] = $product_link_name;
	$seoTitleArr[] = "reviews-ratings";
	$seoTitleArr[] = "on-cars-Model-reviews";
	$seoTitleArr[] = $product_link_info_name;
	$seoTitleArr[] = $product_name_id;
	$oncars_group_seo_url = implode("/",$seoTitleArr);

	unset($seoTitleArr);
	$seoTitleArr[] = SEO_WEB_URL;
	$seoTitleArr[] = $product_brand_name."-cars";
	$seoTitleArr[] = $product_link_name;
	$seoTitleArr[] = "reviews-ratings";
	$seoTitleArr[] = SEO_CARS_MODEL_REVIEWS; 
	$seoTitleArr[] = $product_link_info_name;
	$seoTitleArr[] = $product_name_id;
	$userreview_seo_url = implode("/",$seoTitleArr);


	//photos and videos section tab
	$photos_videos_tab_result = $oWallpapers->arrGetPhotosVideosTab();
	$tcnt = sizeof($photos_videos_tab_result);
	$sPhotoVideoDetXML = "<PRODUCT_PHOTO_DETAIL>";
	for($i=0;$i<$tcnt;$i++){
		$tab_group_id = $photos_videos_tab_result[$i]['group_id'];
		$tab_group_name = $photos_videos_tab_result[$i]['group_name'];
		unset($seoTitleArr);
		$product_info_name = seo_title_replace($product_info_name);
		$seoTitleArr[] = SEO_WEB_URL;
		$seoTitleArr[] = $product_brand_name."-cars";
		$seoTitleArr[] = $product_link_name;
		$seoTitleArr[] = "Model-".$tab_group_name ; //"interior"
		$seoTitleArr[] = $product_info_name;
		$seoTitleArr[] = $product_name_id;
		$seoTitleArr[] = $tab_group_id ; //"interior"
		$photos_videos_tab_result[$i]['seo_model_photo_tab'] = implode("/",$seoTitleArr);
		$photos_videos_tab_result[$i] = array_change_key_case($photos_videos_tab_result[$i],CASE_UPPER);
		$sPhotoVideoDetXML .= "<PRODUCT_PHOTO_DETAIL_DATA>";
		foreach($photos_videos_tab_result[$i] as $k => $v){
			$sPhotoVideoDetXML .= "<$k><![CDATA[$v]]></$k>";
		}
		$sPhotoVideoDetXML .= "</PRODUCT_PHOTO_DETAIL_DATA>";
	}
	$sPhotoVideoDetXML .= "</PRODUCT_PHOTO_DETAIL>";

	if($tab == 'photos_videos'){
		//Interior section in photos and videos tab
		if($photo_tab_id=='1'){
			$result = $oWallpapers->arrSlideShowDetails("","1","",$product_name_id,"",$category_id,"","1");
			$cnt = sizeof($result);
			$sPhotoDetXML .= "<PRODUCT_SLIDE_PHOTO_DETAIL>";
			$sPhotoDetXML .= "<COUNT>$cnt</COUNT>";
			for($i=0;$i<$cnt;$i++){
				$abstract = $result[$i]["abstract"];
				if(!empty($abstract)){
					$result[$i]['abstract'] = html_entity_decode($abstract,ENT_QUOTES,'UTF-8');
				}else{
					$result[$i]['abstract'] = $abstract;
				}
				$title = $result[$i]["title"];
				if(!empty($title)){
					$result[$i]['title'] = html_entity_decode($title,ENT_QUOTES,'UTF-8');
				}
				$media_id = $result[$i]["media_id"];
		                $media_path = $result[$i]["media_path"];
				if(!empty($media_path)){
					$media_path = resizeImagePath($media_path,"555X416",$aModuleImageResize,$media_id);
					$result[$i]['media_path'] = $media_path ? CENTRAL_IMAGE_URL.$media_path : '';
				}
		                $video_img_id = $result[$i]["video_img_id"];
                		$video_img_path = $result[$i]["video_img_path"];
				if(!empty($video_img_path)){
					$video_img_path =  resizeImagePath($video_img_path,"555X416",$aModuleImageResize, $video_img_id);
					$result[$i]['video_img_path'] = $video_img_path ? CENTRAL_IMAGE_URL.$video_img_path : '';
					$thumb_video_img_path = str_replace("555x416","87x65",$video_img_path);
					$thumb_video_img_path =  resizeImagePath($thumb_video_img_path,"87X65",$aModuleImageResize, $video_img_id);
					$result[$i]['thumb_video_img_path'] = $thumb_video_img_path ? CENTRAL_IMAGE_URL.$thumb_video_img_path : '';
					
				}
		                $meta_description = $result[$i]["meta_description"];
				if(!empty($meta_description)){
					$result[$i]['meta_description'] = html_entity_decode($meta_description,ENT_QUOTES,'UTF-8');
				}
				$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
				$sPhotoDetXML .= "<PRODUCT_SLIDE_PHOTO_DETAIL_DATA>";
				foreach($result[$i] as $k => $v){
					$sPhotoDetXML .= "<$k><![CDATA[$v]]></$k>";
				}
				$sPhotoDetXML .= "</PRODUCT_SLIDE_PHOTO_DETAIL_DATA>";
			}
			$sPhotoDetXML .= "</PRODUCT_SLIDE_PHOTO_DETAIL>";
		}

		//exterior slide show section in photos and videos tab
		if($photo_tab_id=='2'){
			$result=$oWallpapers->arrSlideShowDetails("","2","",$product_name_id,"",$category_id,"","1");
			$cnt=sizeof($result);
			$sPhotoDetXML .= "<PRODUCT_SLIDE_PHOTO_DETAIL>";
			$sPhotoDetXML .= "<COUNT>$cnt</COUNT>";
			for($i=0;$i<$cnt;$i++){
				$abstract = $result[$i]["abstract"];
				if(!empty($abstract)){
					$result[$i]['abstract'] = html_entity_decode($abstract,ENT_QUOTES,'UTF-8');
				}else{
					$result[$i]['abstract'] = $abstract;
				}
				$title = $result[$i]["title"];
				if(!empty($title)){
					$result[$i]['title'] = html_entity_decode($title,ENT_QUOTES,'UTF-8');
				}
				$media_id = $result[$i]["media_id"];
		        $media_path = $result[$i]["media_path"];
				if(!empty($media_path)){	
					$media_path = resizeImagePath($media_path,"555X416",$aModuleImageResize,$media_id);
					$result[$i]['media_path'] = $media_path ? CENTRAL_IMAGE_URL.$media_path : '';
				}
                		$video_img_id = $result[$i]["video_img_id"];
		                $video_img_path = $result[$i]["video_img_path"];
				if(!empty($video_img_path)){
					$video_img_path =  resizeImagePath($video_img_path,"555X416",$aModuleImageResize,$video_img_id);
					$result[$i]['video_img_path'] = $video_img_path ? CENTRAL_IMAGE_URL.$video_img_path : '';
					$thumb_video_img_path = str_replace("555x416","87x65",$video_img_path);
					$thumb_video_img_path =  resizeImagePath($thumb_video_img_path,"87X65",$aModuleImageResize, $video_img_id);
					$result[$i]['thumb_video_img_path'] = $thumb_video_img_path ? CENTRAL_IMAGE_URL.$thumb_video_img_path : '';
				}
                		$meta_description = $result[$i]["meta_description"];
				if(!empty($meta_description)){
					$result[$i]['meta_description'] = html_entity_decode($meta_description,ENT_QUOTES,'UTF-8');
				}
				$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
				$sPhotoDetXML .= "<PRODUCT_SLIDE_PHOTO_DETAIL_DATA>";
				foreach($result[$i] as $k => $v){
					$sPhotoDetXML .= "<$k><![CDATA[$v]]></$k>";
				}
				$sPhotoDetXML .= "</PRODUCT_SLIDE_PHOTO_DETAIL_DATA>";
			}
			$sPhotoDetXML .= "</PRODUCT_SLIDE_PHOTO_DETAIL>";
		}

		//videos section in photos and videos tab
		if($photo_tab_id=='3'){
			$result=$videoGallery->getVideosDetails("","","","",$product_name_id,$category_id,"","1","0","3","order by V.create_date desc");
                        $result=$videoGallery->arrGenerate($result,"");
                        $res_size = sizeof($result);
                        if(($res_size >= 0) && ($res_size < 3)){
                                $cnt_limit = 3-$res_size;
                                $result_list=$oReview->arrGetReviewsVideoDetails("","","",$product_name_id,$category_id,"","1","0",$cnt_limit);
                                $result_list=$videoGallery->arrGenerate($result_list,"2");
                                $result = array_merge($result,$result_list);
                        }
                        //print"<pre>";print_r($result);print"</pre>";
			$cnt = sizeof($result);	
			$sPhotoDetXML = "<PRODUCT_SLIDE_PHOTO_DETAIL>";
			$sPhotoDetXML .= "<COUNT><![CDATA[$cnt]]></COUNT>";
			for($i=0;$i<$cnt;$i++){
				$video_id = $result[$i]["video_id"];
				$media_id = $result[$i]["media_id"];
				$media_path = $result[$i]["media_path"];
				$video_img_id = $result[$i]["video_img_id"];
				$video_img_path = $result[$i]["video_img_path"];
				$content_type = $result[$i]["content_type"];
				$is_media_proces = $result[$i]["is_media_process"];
				$status = $result[$i]["status"];
				
				/*
				if(empty($is_media_process) && !empty($media_path) && $content_type != 2){
					$media_path = CENTRAL_IMAGE_URL.$media_path;
				}elseif(!empty($is_media_process) && !empty($media_path) && $content_type != 2){
					$media_path = CENTRAL_MEDIA_URL.$media_path;
				}
				*/
				if(!empty($video_img_path)){					
					$video_img_path = resizeImagePath($video_img_path,"555X312",$aModuleImageResize,$video_img_id);
					$video_img_path = CENTRAL_IMAGE_URL.$video_img_path;
					$embed_video_img_path = $video_img_path;
				}
				

				if(empty($is_media_process) && $content_type != 2){
					//start code to get processed audio/video.
					//$upload_result = arrUpdateAudioVideo($media_id,$video_img_id,"VIDEO_GALLERY",$video_img_path);
					$table_name="";
                                	if($result[$i]["type"] ==  2){
                                        	$table_name="UPLOAD_MEDIA_REVIEWS";
	                                }else{
        	                                $table_name="VIDEO_GALLERY";
                	                }
                        	        $upload_result = arrUpdateAudioVideo($media_id,$video_id,$table_name,$video_img_path,"");

					if(sizeof($upload_result) > 0){
						$media_path = $upload_result['media_path'];
						$uploaded_video_img_path = $upload_result['video_img_path'];
						if(!empty($uploaded_video_img_path)){
							$uploaded_video_img_path =  resizeImagePath($uploaded_video_img_path,"555X312",$aModuleImageResize,$video_img_id);
						}
						$video_img_path = $video_img_path ? $video_img_path : $uploaded_video_img_path;
					}
				}
				$videoArr = arrGetDifferentVideoResolution($media_path);
				$low_media_path = $videoArr['low_media_path'];	
				$normal_media_path = $videoArr['normal_media_path'];

				if(!empty($media_path)){
					$media_path = CENTRAL_MEDIA_URL.str_replace(array(CENTRAL_MEDIA_URL,CENTRAL_IMAGE_URL),'',$media_path);
				}
				if(!empty($low_media_path)){
					$low_media_path = CENTRAL_MEDIA_URL.str_replace(array(CENTRAL_MEDIA_URL,CENTRAL_IMAGE_URL),'',$low_media_path);
				}
				if(!empty($normal_media_path)){
					$normal_media_path = CENTRAL_MEDIA_URL.str_replace(array(CENTRAL_MEDIA_URL,CENTRAL_IMAGE_URL),'',$normal_media_path);
				}
				if(!empty($video_img_path)){
					$video_img_path = CENTRAL_IMAGE_URL.str_replace(array(CENTRAL_MEDIA_URL,CENTRAL_IMAGE_URL),'',$video_img_path);
				}

				if($content_type == 1){
					//for video.
					$result[$i]['media_path'] = $media_path;
					$result[$i]['low_media_path'] = $low_media_path;
					$result[$i]['normal_media_path'] = $normal_media_path;
					$result[$i]['video_img_path'] = $video_img_path;
					$emb_media_path=$media_path;
				}else if($content_type == 2){
					//for image
					$result[$i]['media_path'] = $media_path;
					$emb_media_path = $media_path;
					//$result[$i]['video_img_path'] = $video_img_path;
				}else if($content_type == 3){
					//for audio
					$result[$i]['media_path'] = $media_path;
					$result[$i]['low_media_path'] = $low_media_path;
					$result[$i]['normal_media_path'] = $normal_media_path;
					$result[$i]['video_img_path'] = $video_img_path;
					$emb_media_path=$media_path;
				}
				$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
				$sPhotoDetXML .= "<PRODUCT_SLIDE_PHOTO_DETAIL_DATA>";
				foreach($result[$i] as $k=>$v){
					$sPhotoDetXML .= "<$k><![CDATA[$v]]></$k>";
				}
				$sPhotoDetXML .= "</PRODUCT_SLIDE_PHOTO_DETAIL_DATA>";
			}
			$sPhotoDetXML .= "</PRODUCT_SLIDE_PHOTO_DETAIL>";
		}
	}


unset($seoArr);
$reviewName = $reviewName ? $reviewName : $tab;
	switch($reviewName){
		case 'oncars':
			$seoArr[] = $seo_title_part ." Reviews & Ratings";
			$seoArr[] = 'On mobiles Reviews';
			$seoArr[] = $seo_title_part." cars";
			$seo_desc = $seo_title_part.' Reviews & Ratings  - OnCars Reviews. Get all the latest '.$seo_title_part.' mobile reviews and ratings at '.SEO_DOMAIN.'.';
			//SEO details	
			$seoKeyArr[] = $brand_name." reviews";
			$seoKeyArr[] = $seo_product_info_name;
			$seoKeyArr[] = $brand_name." ".$seo_product_info_name." reviews" ;
			$seoKeyArr[] = $seo_product_info_name." wallpapers ";
			$seoKeyArr[] = $seo_product_info_name." price ";
			$seoKeyArr[] = $seo_product_info_name." photos ";
			$seoKeyArr[] = $seo_product_info_name." reviews ";
		break;
		case 'experts':
			$seoArr[] = $seo_title_part ." Reviews & Ratings";
			$seoArr[] = 'Meta Reviews';
			$seoArr[] = $seo_title_part." cars";
			$seo_desc = $seo_title_part.' Reviews & Ratings  - Meta Reviews. Get all the latest '.$seo_title_part.' mobile reviews and ratings at '.SEO_DOMAIN.'.';
			//SEO details	
			$seoKeyArr[] = $brand_name." reviews";
			$seoKeyArr[] = $seo_product_info_name;
			$seoKeyArr[] = $brand_name." ".$seo_product_info_name." reviews" ;
			$seoKeyArr[] = $seo_product_info_name." wallpapers ";
			$seoKeyArr[] = $seo_product_info_name." price ";
			$seoKeyArr[] = $seo_product_info_name." photos ";
			$seoKeyArr[] = $seo_product_info_name." reviews ";
		break;
		case 'userreview':
			$seoArr[] = $seo_title_part ." Reviews & Ratings";
			if($revtype=='fullreview'){
				$seoArr[] = 'User Review By '.$user_name;
			}else{
				$seoArr[] = 'User Reviews';
			}
			$seoArr[] = $seo_title_part." cars";
			$seo_desc = $seo_title_part.' Reviews & Ratings  - User Reviews. Get all the latest '.$seo_title_part.' mobile reviews and ratings at '.SEO_DOMAIN.'.';
			//SEO details	
			$seoKeyArr[] = $brand_name." user reviews";
			$seoKeyArr[] = $seo_product_info_name;
			$seoKeyArr[] = $brand_name." ".$seo_product_info_name." reviews" ;
			$seoKeyArr[] = $seo_product_info_name." wallpapers ";
			$seoKeyArr[] = $seo_product_info_name." price ";
			$seoKeyArr[] = $seo_product_info_name." photos ";
			$seoKeyArr[] = $seo_product_info_name." reviews ";
		break;
		case 'photos_videos':
			if($photo_tab_id == '1'){
				$seoArr[] = $seo_title_part ." Photos & Videos";
				$seoArr[] = 'Exterior Photos';
				$seoArr[] = $seo_title_part." mobiles ";
				$seo_desc = $seo_title_part.' Photos & Videos  - Exterior Photos. Get all the latest Exterior Photos of '.$seo_title_part.' at '.SEO_DOMAIN.'.';
				//SEO details	
				$seoKeyArr[] = $brand_name." photos";
				$seoKeyArr[] = $seo_product_info_name;
				$seoKeyArr[] = $brand_name." ".$seo_product_info_name." Exterior photos" ;
				$seoKeyArr[] = $seo_product_info_name." wallpapers ";
				$seoKeyArr[] = $seo_product_info_name." price ";
				$seoKeyArr[] = $seo_product_info_name." photos ";
				$seoKeyArr[] = $seo_product_info_name." reviews ";
			}else if($photo_tab_id == '2'){
				$seoArr[] = $seo_title_part ." Photos & Videos";
				$seoArr[] = 'Interior Photos';
				$seoArr[] = $seo_title_part." mobiles ";

				$seo_desc = $seo_title_part.' Photos & Videos  - Interior Photos. Get all the latest Interior Photos of '.$seo_title_part.' at '.SEO_DOMAIN.'.';

				//SEO details	
				$seoKeyArr[] = $brand_name." photos";
				$seoKeyArr[] = $seo_product_info_name;
				$seoKeyArr[] = $brand_name." ".$seo_product_info_name." Interior photos" ;
				$seoKeyArr[] = $seo_product_info_name." wallpapers ";
				$seoKeyArr[] = $seo_product_info_name." price ";
				$seoKeyArr[] = $seo_product_info_name." photos ";
				$seoKeyArr[] = $seo_product_info_name." reviews ";
			}else{
				$seoArr[] = $seo_title_part ." Videos";
				$seoArr[] = $seo_title_part." mobiles ";
				$seo_desc = $seo_title_part.' Videos . Get all the latest Videos of '.$seo_title_part.' at '.SEO_DOMAIN.'.';
				//SEO details	
				$seoKeyArr[] = $brand_name." Videos";
				$seoKeyArr[] = $seo_product_info_name;
				$seoKeyArr[] = $brand_name." ".$seo_product_info_name." Videos" ;
				$seoKeyArr[] = $seo_product_info_name." wallpapers ";
				$seoKeyArr[] = $seo_product_info_name." price ";
				$seoKeyArr[] = $seo_product_info_name." photos ";
				$seoKeyArr[] = $seo_product_info_name." reviews ";
			}
		break;
		default:
			$seoArr[] = $seo_title_part ." Overview & Details";
			$seoArr[] = $seo_title_part ." cars";
			$seo_desc = $seo_title_part.' Overview & Details. Get all the latest '.$seo_title_part.' mobile reviews and ratings, Mobile news, available versions, on road price, technical specifications, features, colors, photos and videos on '.SEO_DOMAIN;
			//SEO details	
			$seoKeyArr[] = $brand_name." ".$seo_product_info_name;
			$seoKeyArr[] = $brand_name;
			$seoKeyArr[] = $seo_product_info_name;
			$seoKeyArr[] = $brand_name." ".$seo_product_info_name." overview" ;
			$seoKeyArr[] = $seo_product_info_name." wallpaper ";
			$seoKeyArr[] = $seo_product_info_name." price ";
			$seoKeyArr[] = $seo_product_info_name." photos ";
			$seoKeyArr[] = $seo_product_info_name." reviews ";
		break;
	}
	$seoArr[] = SEO_DOMAIN;
	$seo_title = implode(" | ",$seoArr);
	$seo_keywords = strtolower(implode(" , ",$seoKeyArr));


	unset($seoTitleArr);
	$product_info_name = seo_title_replace($product_info_name);
	$seoTitleArr[] = SEO_WEB_URL;
	$seoTitleArr[] = $product_brand_name."-cars";
	$seoTitleArr[] = $product_link_name;
	////$seoTitleArr[] = "Model-Car-Photos-videos"; //"interior"
	$seoTitleArr[] = "Model-Exterior"; //"Exterior"
	$seoTitleArr[] = $product_info_name;
	$seoTitleArr[] = $product_name_id;
	$seoTitleArr[] = "1"; //"interior"
	$seo_photo_tab_url= implode("/",$seoTitleArr);
	unset($seoTitleArr);
	$product_info_name = seo_title_replace($product_info_name);
	$seoTitleArr[] = SEO_WEB_URL;
	$seoTitleArr[] = $product_brand_name."-cars";
	$seoTitleArr[] = $product_link_name;
	$seoTitleArr[] = "Model-Videos"; //"interior"
	$seoTitleArr[] = $product_info_name;
	$seoTitleArr[] = $product_name_id;
	$seoTitleArr[] = "3"; //"interior"
	$seo_video_tab_url= implode("/",$seoTitleArr);
	
	//get the message board data 
	if($reviewName==='oncars'){
		$rev_category_id = ONCARS_REVIEW_CATEGORYID;
		//$comment_result = $report->arrGetOnCarsReviewCommentCount($revid);
		$iRecCnt = $comment_result[0]['comment_count'];
		$iTId = $comment_result[0]['comment_board_id'];
		//if(empty($iRecCnt)){

			//$sRequestUrl = WEB_URL.substr($_SERVER['REQUEST_URI'],1);
             unset($aRequestUrl);
			$aRequestUrl[] = SEO_WEB_URL;
			$aRequestUrl[] = $brand_name."-cars";
			$aRequestUrl[] = $brand_name."-".$model;
			$aRequestUrl[] = "reviews-ratings";
			$aRequestUrl[] = "on-cars-Model-reviews";
			$aRequestUrl[] = $model;
			$aRequestUrl[] = $product_info_id;
			$aRequestUrl[] = $aGroupId;
			$aRequestUrl[] = $revid;
			$sRequestUrl = implode("/",$aRequestUrl);
			//print "<pre>";print_r($sRequestUrl);

			$aParameters = Array("title"=>$revid,"turl"=>$sRequestUrl,"cid"=>ONCARS_REVIEW_CATEGORYID,"sid"=>SERVICEID);
			$comment_result = $report->intInsertUpdateCommentCount($aParameters);
			//print "<pre>"; print_r($comment_result ); 
			$iRecCnt = $comment_result['comment_count'];
			$iTId = $comment_result['iTId'];
		//}
		if($iRecCnt > 0){
			//get the reply list
			$iRecordPerPage=10;
			//get the reply count 
			$aParameters=Array("title"=>$revid,"cid"=>ONCARS_REVIEW_CATEGORYID,"sid"=>SERVICEID);
			$aMBReplyCnt=$oCampusDiscussion->getMBDetails($aParameters);
			$iRecCnt=$aMBReplyCnt['reply_cnt'];

			//echo $iRecCnt."COUNT";
			unset($seoTitleArr);
			$seoTitleArr[] = SEO_WEB_URL;
			$seoTitleArr[] = $product_brand_name."-cars";
			$seoTitleArr[] = $product_link_name;
			$seoTitleArr[] = "reviews-ratings";
			if($reviewName==='oncars'){
				$seoTitleArr[] = "on-cars-Model-reviews";
				$seoTitleArr[] = $product_link_info_name;
				$seoTitleArr[] = $product_name_id;
				$seoTitleArr[] = $rev_grp_id;
				$seoTitleArr[] = $revid;
				$page_url = implode("/",$seoTitleArr);
			}
			if($reviewName==='experts'){
				$seoTitleArr[] = "Meta-Model-reviews";
				$seoTitleArr[] = $product_link_info_name;
				$seoTitleArr[] = $product_name_id;
				$page_url = implode("/",$seoTitleArr)."/";
			}
			if($iRecCnt!=0){
				if($_REQUEST['pagination']==1){
					$sTemplate="xsl/mboard_pagination.xsl";
				}
				//$page= 1;
				if(isset($_REQUEST['page'])){
					$page= $_REQUEST['page'];
				}
				$page_url=$page_url."&pagination=1&tid=".$iTId;
				$start  = $ObjPager->findStart($iRecordPerPage);
				$jsparams=$start.",".$iRecordPerPage.",".$page_url.",qcontainer,revid=$revid&tid=$iTId";
				$pages= $ObjPager->findPages($iRecCnt,$iRecordPerPage);
				if($pages > 1 ){
					$pagelist= $ObjPager->jsPageNumNextPrev($page,$pages,"mBPagination",$jsparams,"text");
					$nodesPaging .= "<Pages><![CDATA[".$pagelist."]]></Pages>";
					$nodesPaging .= "<Page><![CDATA[".$page."]]></Page>";
					$nodesPaging .= "<Perpage><![CDATA[".$perpage."]]></Perpage>";
				}
				$sReplyXml=$oCampusDiscussion->getReply(array("tid"=>$iTId,"rowcnt"=>$iRecordPerPage,"start"=>$start));
				$pos = strpos($sReplyXml, "<response>");
				$sReplyXml=substr($sReplyXml,$pos);
			}		
		}
		if($iRecCnt!=0){
			if($_REQUEST['pagination']==1){
				$sTemplate="xsl/mboard_pagination.xsl";
			}
		}
	}else if($reviewName==='userreview'){
		$rev_category_id = USER_REVIEW_MODEL_CATEGORY_ID;
		//$comment_result = $report->arrGetUserReviewCommentCount($user_review_id);
		$iRecCnt = $comment_result[0]['comment_count'];
		$iTId = $comment_result[0]['comment_board_id'];
		//if(empty($iRecCnt)){
			//$sRequestUrl = WEB_URL.substr($_SERVER['REQUEST_URI'],1);
            unset($aRequestUrl);
			$aRequestUrl[] = SEO_WEB_URL;
			$aRequestUrl[] = $product_brand_name."-cars";
			$aRequestUrl[] = $product_link_name;
			$aRequestUrl[] = "reviews-ratings";
			$aRequestUrl[] = "model-user-full-reviews";
			$aRequestUrl[] = $product_link_info_name;
			$aRequestUrl[] = $product_name_id;
			$aRequestUrl[] = $user_review_id;
           
		    $sRequestUrl = implode("/",$aRequestUrl);
	        //print "<pre>";print_r($sRequestUrl);

			$aParameters = Array("title"=>$user_review_id,"turl"=>$sRequestUrl,"cid"=>USER_REVIEW_MODEL_CATEGORY_ID,"sid"=>SERVICEID);
			$comment_result = $report->intInsertUpdateCommentCount($aParameters);
			$iRecCnt = $comment_result['comment_count'];
			$iTId = $comment_result['iTId'];
		//}
		if($iRecCnt > 0){
			//get the reply list
			$iRecordPerPage=10;
			//get the reply count 
			$aParameters=Array("title"=>$user_review_id,"cid"=>USER_REVIEW_MODEL_CATEGORY_ID,"sid"=>SERVICEID);
			$aMBReplyCnt=$oCampusDiscussion->getMBDetails($aParameters);
			$iRecCnt=$aMBReplyCnt['reply_cnt'];
			unset($seoTitleArr);
			$seoTitleArr[] = SEO_WEB_URL;
			$seoTitleArr[] = $product_brand_name."-cars";
			$seoTitleArr[] = $product_link_name;
			$seoTitleArr[] = "reviews-ratings";
			$seoTitleArr[] = "model-user-full-reviews";
			$seoTitleArr[] = $product_link_info_name;
			$seoTitleArr[] = $product_name_id;
			$seoTitleArr[] = $user_review_id;
			$page_url = implode("/",$seoTitleArr);
			if($iRecCnt!=0){
				if($_REQUEST['pagination']==1){
					$sTemplate="xsl/mboard_pagination.xsl";
				}
				//$page= 1;
				if(isset($_REQUEST['page'])){
					$page= $_REQUEST['page'];
				}
				$page_url=$page_url."&pagination=1&tid=".$iTId;
				$start  = $ObjPager->findStart($iRecordPerPage);
				$jsparams=$start.",".$iRecordPerPage.",".$page_url.",qcontainer,revid=$revid&tid=$iTId";
				$pages= $ObjPager->findPages($iRecCnt,$iRecordPerPage);
				if($pages > 1 ){
					$pagelist= $ObjPager->jsPageNumNextPrev($page,$pages,"mBPagination",$jsparams,"text");
					$nodesPaging .= "<Pages><![CDATA[".$pagelist."]]></Pages>";
					$nodesPaging .= "<Page><![CDATA[".$page."]]></Page>";
					$nodesPaging .= "<Perpage><![CDATA[".$perpage."]]></Perpage>";
				}
				$sReplyXml=$oCampusDiscussion->getReply(array("tid"=>$iTId,"rowcnt"=>$iRecordPerPage,"start"=>$start));
				$pos = strpos($sReplyXml, "<response>");
				$sReplyXml=substr($sReplyXml,$pos);
			}		
		}
		if($iRecCnt!=0){
			if($_REQUEST['pagination']==1){
				$sTemplate="xsl/mboard_pagination.xsl";
			}
		}
	}
	$xml .= "<OVERALL_AVG_HTML><![CDATA[$html]]></OVERALL_AVG_HTML>";
	$xml .= "<OVERALL_AVG_CNT><![CDATA[$overallavg]]></OVERALL_AVG_CNT>";
	$xml .= "<OVERALL_TOTAL_CNT><![CDATA[$overallcnt]]></OVERALL_TOTAL_CNT>";
	$xml .= $optionhtml;

	if(!empty($category_id)){
		$result = $oBrand->arrGetBrandDetails("",$category_id,1);
	}
	$cnt = sizeof($result);
	$xml .= "<BRAND_MASTER>";
	$xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";	
	
	$selectedIndex = "0";
	$isBrandSelected = "0"; //used toggle all brands checkbox.
	for($i=0;$i<$cnt;$i++){
		$brand_id = $result[$i]['brand_id'];
		if(in_array($brand_id,$selectedbrandArr)){
			$result[$i]['selected_brand_id'] = $brand_id;
			$selecteditemArr[$selectedIndex]['selected_id'] = $brand_id;
			$selecteditemArr[$selectedIndex]['selected_type'] = 'checkbox_brand_id_'.$brand_id;
			$selecteditemArr[$selectedIndex]['selected_name'] =$result[$i]['brand_name'];
			$selectedIndex++;
			$isBrandSelected++;
			
		}
		$status = $result[$i]['status'];
		$categoryid = $result[$i]['category_id'];
		if(!empty($categoryid)){
			$category_result = $category->arrGetCategoryDetails($categoryid);
		}
		$category_name = $category_result[0]['category_name'];
		$result[$i]['js_category_name'] = $category_name;
		$result[$i]['category_name'] = html_entity_decode($category_name,ENT_QUOTES,'UTF-8');
		$result[$i]['brand_status'] = ($status == 1) ? 'Active' : 'InActive';
		$result[$i]['create_date'] = date('d-m-Y',strtotime($result[$i]['create_date']));
		$result[$i]['js_brand_name'] = $result[$i]['brand_name'];
		$result[$i]['brand_name'] = html_entity_decode($result[$i]['brand_name'],ENT_QUOTES,'UTF-8');
		$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
		$xml .= "<BRAND_MASTER_DATA>";
		foreach($result[$i] as $k=>$v){
			$xml .= "<$k><![CDATA[$v]]></$k>";
		}
		$xml .= "</BRAND_MASTER_DATA>";
	}
	$xml .= "</BRAND_MASTER>";

	$breadcrumb = CATEGORY_HOME."<a href='".WEB_URL.SEO_CAR_RESEARCH."'>New Mobile Search</a> : $sModelBrandName $search_product_info_name";
	$config_details = get_config_details();
	$page_name=$_SERVER['SCRIPT_URI'];

     	// this title is for views title only 
	if($disp_title_name==''){
	   $disp_title_name="My experience with my".$brand_name."".$model_name;
	   $views_title = rawurlencode($disp_title_name);
	 }else {
	   $views_title = rawurlencode($disp_title_name);
	 }
	$strXML .= "<XML>";
	$strXML .= "<PAGE_NAME><![CDATA[$page_name]]></PAGE_NAME>";
	$strXML .= "<VIEW_DISP_TITLE><![CDATA[".$views_title."]]></VIEW_DISP_TITLE>";
	$strXML .= "<SEO_CAR_FINDER><![CDATA[".SEO_CAR_FINDER."]]></SEO_CAR_FINDER>";
	$strXML .= "<BREAD_CRUMB><![CDATA[$breadcrumb]]></BREAD_CRUMB>";
	$strXML .= "<SEO_URL><![CDATA[$seo_url]]></SEO_URL>";
	$strXML .= "<SEO_WEB_URL><![CDATA[".SEO_WEB_URL."]]></SEO_WEB_URL>";
	$strXML .= "<SEO_AUTO_NEWS><![CDATA[".SEO_AUTO_NEWS."]]></SEO_AUTO_NEWS>";
	$strXML .= "<ONCARS_SEO_URL><![CDATA[$oncars_seo_url]]></ONCARS_SEO_URL>";
	$strXML .= "<EXPERTS_SEO_URL><![CDATA[$expert_seo_url]]></EXPERTS_SEO_URL>";
	$strXML .= "<SEO_TITLE><![CDATA[$seo_title]]></SEO_TITLE>";
	$strXML .= "<SEO_TAGS><![CDATA[$seo_keywords]]></SEO_TAGS>";
	$strXML .= "<SEO_DESC><![CDATA[$seo_desc]]></SEO_DESC>";
	$strXML .= "<MODEL_BRAND_NAME><![CDATA[$sModelBrandName]]></MODEL_BRAND_NAME>";
	$strXML .= "<MODEL_NAME><![CDATA[$search_product_info_name]]></MODEL_NAME>";
	$strXML.= $xml;
	$strXML .= "<STARTLIMIT><![CDATA[$startlimit]]></STARTLIMIT>";
	$strXML .= "<CNT><![CDATA[$limitcnt]]></CNT>";
	$strXML .= "<SELECTEDTABID><![CDATA[$tab_id]]></SELECTEDTABID>";
	$strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$category_id]]></SELECTED_CATEGORY_ID>";
	$strXML .= "<SELECTED_CITY_ID><![CDATA[$iCity_id]]></SELECTED_CITY_ID>";
	$strXML .= "<PHOTO_TAB_ID><![CDATA[$photo_tab_id]]></PHOTO_TAB_ID>";
	$strXML .= "<SEO_PHOTO_TAB_URL><![CDATA[$seo_photo_tab_url]]></SEO_PHOTO_TAB_URL>";
	$strXML .= "<SEO_VIDEO_TAB_URL><![CDATA[$seo_video_tab_url]]></SEO_VIDEO_TAB_URL>";
	$strXML .= $config_details;
	$strXML .= $sProductDetXml;
	$strXML .= $srevDetailxml;
	$strXML .= $sReviewDetailXml;
	$strXML .= $groupnodexml;
	$strXML .= $sArticleDetXml;
	$strXML .= $sCityDetXml;
	$strXML .= $strmediaxml;
	$strXML .= $sTopCopmetitorsListing;
	$strXML .= $sWallpapersDetXml;
	$strXML .= $sProductInfoDetXml;
	$strXML .= $sProductVersionDetXML;
	$strXML .= $sBrandDataDetXML;
	$strXML .= $sReviewDetailsDetXML;
	$strXML .= $sProductRevImageDetXML;
	$strXML .= $sReviewsGroupDetailXML;
	$strXML .= $sRevIdDetXML;
	$strXML .= $strRevGroupxml;
	$strXML .= $sPhotoDetXML;
	$strXML .= $sPhotoVideoDetXML;	
	$strXML .= "<OVERVIEW>$sOverviewXML</OVERVIEW>";
	$strXML .= "<PRODSELCITY>$prodCityId</PRODSELCITY>";
	$strXML .= "<PRODSELGROUP>$rev_grp_id</PRODSELGROUP>";
	$strXML .= "<DEF_REVIEW_ID>$defRevId</DEF_REVIEW_ID>";
	$strXML .= "<ONCARS_GROUP_SEO_URL><![CDATA[$oncars_group_seo_url]]></ONCARS_GROUP_SEO_URL>";
	$strXML .= "<USERREVIEW_SEO_URL><![CDATA[$userreview_seo_url]]></USERREVIEW_SEO_URL>";

	if($reviewName==='oncars'){
		$strXML .= "<REVIEW_NAME>$reviewName</REVIEW_NAME>";
		
		$xsl = DOMDocument::load('xsl/model_rating_review.xsl');
	}else if($reviewName==='experts'){
		$strXML .= "<REVIEW_NAME>$reviewName</REVIEW_NAME>";
		$xsl = DOMDocument::load('xsl/model_expert_review.xsl');
	}else if($reviewName==='userreview'){
		
		//views updating section
		$strXML .= "<REVIEW_NAME>$reviewName</REVIEW_NAME>";
		if($revtype=='fullreview'){
			
			$xsl = DOMDocument::load('xsl/user_full_review.xsl');
		}else{
			
			$xsl = DOMDocument::load('xsl/model_page_user_review.xsl');
		}
	}else{
		if($tab=='photos_videos'){
			$xsl = DOMDocument::load('xsl/model_photos_videos.xsl');
		}else{
			$xsl = DOMDocument::load('xsl/model_page.xsl');
		}
	}
	$sCityId = !empty($sCityId) ? $sCityId : $iCity_id; 
	if(!empty($sCityId)){
		setcookie ('cookie_city_id', $sCityId,time()+3600*24, '/', DOMAIN);
	}

	$curr_city_name = !empty($selected_city_name) ? $selected_city_name : $iCity_name; 

	$dealer_flag = 0;
	if(!empty($product_info_brand_id) && !empty($iCity_id)){
		$dealer_results = $oDealer->arrGetLMSDealerDetails("","","",$iCity_id,$category_id,$product_info_brand_id,"1");
		$dcnt = sizeof($dealer_results);
		if($dcnt > 0){
			$dealer_flag = 1;
		}else{
			$rCityRes = $oCityState->arrGetRelatedCityDetails("",$selected_city_id,"",$category_id,"1");
                        if(is_array($rCityRes)){
				$cnt = sizeof($rCityRes);
				$related_city_ids = "";
				for($i=0;$i<$cnt;$i++){
	                                $related_city_ids .= $rCityRes[$i]['related_city_id'].",";
				}
				$related_city_ids = substr($related_city_ids,0,-1);
				$dResults = $oDealer->arrGetLMSDealerDetails("","","",$related_city_ids,$category_id,$product_info_brand_id,"1");
				if(is_array($dResults)){
					$related_city_id = $dResults[0]['city_id'];
					$dealer_flag = 1;
				}else{
					$related_city_id = "";
				}
                        }
		}
	}

	unset($dealerQuoteUrl);
	$dealerQuoteUrl[] = SEO_WEB_URL;
	$dealerQuoteUrl[] = GET_DEALER_QUOTE;
	$dealerQuoteUrl[] = $product_info_brand_id;
	$dealerQuoteUrl[] = $product_name_id;
	$dealerQuoteUrl[] = $lowprice_product_id;
	$dealerQuoteUrl[] = $related_city_id;
	$dealer_quote_url = implode("/",$dealerQuoteUrl);

	unset($bookTestDriveUrl);
        $bookTestDriveUrl[] = SEO_WEB_URL;
        $bookTestDriveUrl[] = BOOK_TEST_DRIVE;
        $bookTestDriveUrl[] = $product_info_brand_id;
        $bookTestDriveUrl[] = $product_name_id;
        $bookTestDriveUrl[] = $lowprice_product_id;
        $bookTestDriveUrl[] = $related_city_id;
        $book_test_drive_url = implode("/",$bookTestDriveUrl);

	unset($compareCarsUrl);
        $compareCarsUrl[] = WEB_URL."Compare-cars";
        $compareCarsUrl[] = 1;
        $compareCarsUrl[] = $lowprice_product_id;
        $compareCarsUrl[] = "1#pg"; 
        $compareCarsUrl = implode("/",$compareCarsUrl);
	
	$strXML.="<MBREPLYLIST>";
	$strXML.=$sReplyXml.$nodesPaging;
	$strXML.="<MBREPLYCOUNT><![CDATA[".$iRecCnt."]]></MBREPLYCOUNT>";
	$strXML.="<MBTID><![CDATA[".$iTId."]]></MBTID>";
	$strXML.="<CPAGE><![CDATA[".$page."]]></CPAGE>";
	$strXML.="<SERVICEID><![CDATA[".SERVICEID."]]></SERVICEID>";
	$strXML.="<CATEGORY><![CDATA[".$rev_category_id."]]></CATEGORY>";
	$strXML.="</MBREPLYLIST>";
	$strXML.="<DEALER_QUOTE_URL><![CDATA[$dealer_quote_url]]></DEALER_QUOTE_URL>";
	$strXML.="<BOOK_TEST_DRIVE_URL><![CDATA[$book_test_drive_url]]></BOOK_TEST_DRIVE_URL>";
	$strXML.="<COMPARE_CARS_URL><![CDATA[$compareCarsUrl]]></COMPARE_CARS_URL>";
	$strXML.="<CURR_CITY><![CDATA[".$curr_city_name."]]></CURR_CITY>";
	$strXML.="<DEALER_FLAG><![CDATA[".$dealer_flag."]]></DEALER_FLAG>";
	$strXML.="<RELATED_CITY_ID><![CDATA[".$related_city_id."]]></RELATED_CITY_ID>";
	$strXML .=  "<PAGING><![CDATA[$sPagingXml]]></PAGING>";
	$strXML .=  "<CATEGORY_ID><![CDATA[$category_id]]></CATEGORY_ID>";
	$strXML .=  "<BRAND_ID><![CDATA[$product_info_brand_id]]></BRAND_ID>";
	$strXML .=  "<PRODUCT_NAME_ID><![CDATA[$product_name_id]]></PRODUCT_NAME_ID>";
	//$strXML .=  "<PRODUCT_ID><![CDATA[$rev_product_id]]></PRODUCT_ID>";
	$strXML .=  "<RETURN_REVIEW_URL><![CDATA[".$_SERVER['SCRIPT_URI']."]]></RETURN_REVIEW_URL>";
	$strXML .=  "<USER_REVIEW_ID><![CDATA[$user_review_id]]></USER_REVIEW_ID>";
	$strXML .= "<OC_ROS_BOTTOM_NORTH_728x90><![CDATA[OC_ROS_Bottom_North_728x90]]></OC_ROS_BOTTOM_NORTH_728x90>";
	$strXML .= "<OC_ROS_TOP_RHS_LREC_300x250_1><![CDATA[OC_ROS_Top_RHS_Lrec_300x250_1]]></OC_ROS_TOP_RHS_LREC_300x250_1>";
	
	$strXML .= "<COMMENT_COUNT><![CDATA[".$iRecCnt."]]></COMMENT_COUNT>";
	$strXML .= "<VIEWS_COUNT><![CDATA[$views_count]]></VIEWS_COUNT>";
	$strXML .= "<VIEWS_PAGE_NAME><![CDATA[".$views_page_name."]]></VIEWS_PAGE_NAME>";
	$strXML .= $expertratingxml;
	$most_recent_one_result_list = $videoGallery->getarrMostRecentVideosHeaderTabLink($category_id,"0","1",$array_result);
	$strXML.="<MOST_RECENT_VIDEO_HEADER_LINK><![CDATA[".$most_recent_one_result_list."]]></MOST_RECENT_VIDEO_HEADER_LINK>";
	$strXML .= "</XML>";

	$strXML = mb_convert_encoding($strXML, "UTF-8");
	if($_REQUEST['debug']==1){ header('Content-type: text/xml');echo $strXML;exit; }
	$doc = new DOMDocument();
	$doc->loadXML($strXML);
	$doc->saveXML();

	$xslt = new xsltProcessor;
	$xslt->importStylesheet($xsl);
	print $xslt->transformToXML($doc);
?>
