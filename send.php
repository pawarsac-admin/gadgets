<?php
	require_once('./include/config.php');
	require_once(CLASSPATH.'mail.class.php');

	$mail = new mailer('share');
	//http://www.beta.oncars.in/send.php?from_name=Manish&from_email=system@indiancolleges.com&to_name=Rajesh&to_email=rajesh@corp.india.com&msg=hi%20my%20first%20mail&subject=hello%20Everyone

	$url = WEB_URL;
	$from_email=$_REQUEST['from_email'];
	$from_name=$_REQUEST['from_name'];
	$to_email=$_REQUEST['to_email'];
	$to_name=$_REQUEST['to_name'];
	$msg = nl2br($_REQUEST['msg']);
	$subject = htmlentities($_REQUEST['subject'],ENT_QUOTES);
	
	$request_array['emails'] =  $to_email;
	$request_array['message'] = htmlentities($msg,ENT_QUOTES);
	$request_array['subject'] = $subject;
	$request_array['from_email'] = ONCARS_SHARING_SERVICE_EMAIL;
	$request_array['from_name'] = ONCARS_SHARING_SERVICE_EMAIL_NAME;
	$request_array['url'] = $url;
	$request_array['reply-to'] = ONCARS_NO_REPLY;

	$isSend = $mail->boolSendShare($request_array);

	if($isSend == 1){
		echo "yes";
	}else{
		echo "no";
	}
?>