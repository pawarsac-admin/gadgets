<?php
	require_once('./include/config.php');
	require_once('./include/config_search.php');
	require_once(CLASSPATH.'DbConn.php');
	require_once(CLASSPATH.'brand.class.php');
	require_once(CLASSPATH.'category.class.php');
	require_once(CLASSPATH.'pivot.class.php');
	require_once(CLASSPATH.'feature.class.php');
	require_once(CLASSPATH.'product.class.php');
	require_once(CLASSPATH.'PageNavigator.php');
	require_once(CLASSPATH.'Utility.php');
	require_once(CLASSPATH.'user_review.class.php');
	require_once(CLASSPATH.'topstories.class.php');
	require_once(CLASSPATH.'kgPager.class.php');

	$dbconn			= new DbConn;
	$brand			= new BrandManagement;
	$category		= new CategoryManagement;
	$pivot			= new PivotManagement;
	$feature		= new FeatureManagement;
	$product		= new ProductManagement;
	$userreview		= new USERREVIEW;
	$oTopStories	= new TopStories;

	define("PERPAGE", 10);

	$tab_id			= 1;
	$group_id		= 1;				// Top Rated Phones	
	$category_id	= SITE_CATEGORY_ID;
	$variant_id		= "1";				// important for price search.ie. ex-showroom price.

	$debug			= (empty($_REQUEST['debug']))? '' : $_REQUEST['debug'] ; 

	$startlimit		= $_REQUEST['startlimit'];
	$endlimit		= $_REQUEST['cnt'];
	$request_uri	= $_SERVER['REQUEST_URI'];
	$parameters		= $_REQUEST['param'];
	$arrParams		= explode('/',$parameters);

	function trim_and_lowercase_value(&$value) { 
		$value = strtolower(trim($value));
		$value = str_replace('_',' ',$value);
		return $value;
	}

	function display_data( $text, $a, $b, $c){
		global $debug;
		if( $debug == 1 ){
			echo "<br/><b>$text</b> : ";
			print_r($a);
			print_r($c);
			print_r($b);
		}
	}

	$arrParams	= array_map('trim_and_lowercase_value',$arrParams);

	/**************************************************/
	// Brands
	$arrBrandsData		= $brand->arrGetBrandDetails("",$category_id,"1","","","","order by position asc");
	foreach( $arrBrandsData as $brandData ){
		$arrBrands[$brandData['brand_id']]	= $brandData['brand_name'];
	}

	/**************************************************/
	if($debug ==1 ){
		echo "<br/> _REQUEST: ";
		print_r($_REQUEST);
		echo "<br/> ALL PARAMS: ";
		print_r($arrParams);
	}

	$product_ids		= array(); 

	$arrFlag['brand']			= false;
	$arrFlag['os']				= false;
	$arrFlag['price']			= true;
	$arrFlag['netowrkType']		= false;
	$arrFlag['inputType']		= false;
	$arrFlag['phoneType']		= false;
	$arrFlag['formFactor']		= false;
	$arrFlag['dualSIM']			= false;
	$arrFlag['availability']	= false;
	$arrFlag['announced']		= false;
	$arrFlag['ram']				= false;

	$arrFlag['weight']			= false;
	$arrFlag['height']			= false;
	$arrFlag['width']			= false;
	$arrFlag['depth']			= false;

	$arrFlag['GPS']				= false;
	$arrFlag['WIFI']			= false;
	$arrFlag['camera']	= false;

	
	// Default Initialization
	$mnprice					= MIN_PRICE;
	$mxprice					= MAX_PRICE;
	$sortProductBy				= 1;
	$pagenum					= 1;

	//////// SEO
	function cleanup( $str ){

		$what   = array("<Brand>", "<Mobile-Type>","<Price-Range>","<OS>");
		$by		= '';
		$newstr = str_ireplace($what, $by, $str);
		return $newstr;
	}

	$SEO_TITLE					= SEO_TITLE;
	$SEO_DESC					= SEO_DESC;
	$SEO_TAGS_BRAND_TYPE		= SEO_TAGS_BRAND_TYPE;


	foreach( $arrParams as $parameter ) {
		$arrParam	= explode( '-', $parameter );
		$count		= count($arrParam);
		$param		= $arrParam[0];
		unset($id);
		switch($param){

			case 'brand'	:  
								$arrBrandsTemp	= array_map('trim_and_lowercase_value',$arrBrands);
								for( $i = 1 ; $i < $count ; $i++ ) {
									
									if($arrParam[$i]=='all') { $arrBrandTerms[]='All' ; continue;}

									$arrFlag['brand']	= true;
									if( in_array($arrParam[$i],$arrBrandsTemp) ) {
										$id					= array_search( $arrParam[$i], $arrBrandsTemp );
										$selectedBrandArr[] = $id;
										$arrBrandTerms[]	= $arrBrands[$id];
									}
								}

								$strBrand		= implode(', ',$arrBrandTerms) ;
								$displayTerms[] = "Brands: " . $strBrand;
								$SEO_TITLE		= str_replace('<Brand>', $strBrand, $SEO_TITLE);
								$SEO_DESC		= str_replace('<Brand>', $strBrand, $SEO_DESC);
								$SEO_TAGS_BRAND	= SEO_TAGS_BRAND;
								$SEO_TAGS_BRAND	= str_replace('<Brand>', $strBrand, $SEO_TAGS_BRAND);
								$SEO_TAGS_BRAND_TYPE = str_replace('<Brand>', $strBrand, $SEO_TAGS_BRAND_TYPE);

								display_data('BRANDS',$arrBrandsTemp,$selectedBrandArr,$arrBrandTerms);
								break;

			case 'os'		:	// OS
								$SEO_TAGS_OS	= SEO_TAGS_OS;
								$arrOSTemp		= array_map('trim_and_lowercase_value',$arrOS[OS]);
								$count	= count($arrParam);
								for( $i = 1 ; $i < $count ; $i++ ) {
									if( in_array($arrParam[$i],$arrOSTemp ) ) {
										$id					= array_search( $arrParam[$i], $arrOSTemp );
										$selectedOSArr[]	= $id;
										$arrFlag['os']		= true;
										$arrOSTerms[]		= $arrOS[OS][$id];
									}
								}
								if($arrFlag['os']){
									$feature_id			= OS;
									$arrOSPID			= $product->arrGetProductIdsOfFeature( $feature_id, $selectedOSArr , '' );
									$displayTerms[] = "OS: ".implode(', ',$arrOSTerms) ;
									$strOS			= implode(', ',$arrOSTerms) ;
									$SEO_TITLE		= str_replace('<OS>', $strOS, $SEO_TITLE);
									$SEO_DESC		= str_replace('<OS>', $strOS, $SEO_DESC);
									$SEO_TAGS_OS	= str_replace('<OS>', $strOS, $SEO_TAGS_OS);

								}
								display_data('OS',$arrOSTemp,$selectedOSArr,$arrOSTerms);
								break;

			case 'price'	:	if($arrParam[1]=='all')	$arrParam[1] ='';

								$mnprice			= (empty($arrParam[1]))? MIN_PRICE : $arrParam[1];
								$mxprice			= (empty($arrParam[2]))? MAX_PRICE : $arrParam[2];

								$mnprice			= str_ireplace('k','000',$mnprice);
								$mnprice			= str_ireplace('l','00000',$mnprice);
								$mxprice			= str_ireplace('k','000',$mxprice);
								$mxprice			= str_ireplace('k','00000',$mxprice);

								$arrFlag['price']	= true;
								break;

			case 'technology':	
								// Networktype
								$arrNetworkTypeTemp	= array_map('trim_and_lowercase_value',$arr2GNetwork[NET2G]);

								$count	= count($arrParam);
								for( $i = 1 ; $i < $count ; $i++ ) {
									if( in_array($arrParam[$i],$arrNetworkTypeTemp ) ) {
										$id						= array_search( $arrParam[$i], $arrNetworkTypeTemp );
										$arrNetworkTypeId[]		= $id;
										$arrFlag['netowrkType']	= true;
										$arrNetworkTypeTerms[]	= $arr2GNetwork[NET2G][$id];
									}
								}
								if($arrFlag['netowrkType']){
									$feature_id					=  NET2G;
									$arrNetTypePID				= $product->arrGetProductIdsOfFeature( $feature_id, $arrNetworkTypeId , 'OR' );
								
									$strTechnology		= implode(', ',$arrNetworkTypeTerms) ;
									$displayTerms[]		= "Network technology: ".$strTechnology ;
									$SEO_TITLE			= str_ireplace('<Mobile-Type>', $strTechnology, $SEO_TITLE);
									$SEO_DESC			= str_ireplace('<Mobile-Type>', $strTechnology, $SEO_DESC);
									$SEO_TAGS_TYPE		= SEO_TAGS_TYPE;
									$SEO_TAGS_TYPE		= str_replace('<Mobile-Type>', $strTechnology, $SEO_TAGS_TYPE);
									$SEO_TAGS_BRAND_TYPE= str_replace('<Mobile-Type>', $strTechnology, $SEO_TAGS_BRAND_TYPE);
									

								}
								display_data('NETWORK TYPE',$arrNetworkTypeTemp,$arrNetworkTypeId,$arrNetworkTypeTerms);
								break;


			case 'phonetype':	
								// phonetype
								$arrPhoneTypeTemp	= array_map('trim_and_lowercase_value',$arrPhoneType[PHONETYPE]);

								$count	= count($arrParam);
								for( $i = 1 ; $i < $count ; $i++ ) {
									if( in_array($arrParam[$i],$arrPhoneTypeTemp ) ) {
										$id						= array_search( $arrParam[$i], $arrPhoneTypeTemp );
										$arrPhoneTypeId[]		= $id;
										$arrFlag['phoneType']	= true;
										$arrPhoneTypeTerms[]	= $arrPhoneType[PHONETYPE][$id];
									}
								}
								if($arrFlag['phoneType']){
									$feature_id					=  PHONETYPE;
									$arrPhoneTypePID			= $product->arrGetProductIdsOfFeature( $feature_id, $arrPhoneTypeId , 'OR' );
									$displayTerms[] = "Phone type: ".implode(', ',$arrPhoneTypeTerms) ;
								}
								display_data('PHONE TYPE',$arrPhoneTypeTemp,$arrPhoneTypeId,$arrPhoneTypeTerms);
								break;

			case 'formfactor':	
								// formfactor
								$arrFormFactorTemp	= array_map('trim_and_lowercase_value',$arrFormFactor[FORMFACTOR]);

								$count	= count($arrParam);
								for( $i = 1 ; $i < $count ; $i++ ) {
									if( in_array($arrParam[$i],$arrFormFactorTemp ) ) {
										$id						= array_search( $arrParam[$i], $arrFormFactorTemp );
										$arrFormFactorId[]		= $id;
										$arrFlag['formFactor']	= true;
										$arrFormFactorTerms[]	= $arrFormFactor[FORMFACTOR][$id];
									}
								}
								if($arrFlag['formFactor']){
									$feature_id		=  FORMFACTOR;
									$arrDTypePID	= $product->arrGetProductIdsOfFeature( $feature_id, $arrFormFactorId , 'OR' );
									$displayTerms[] = "Form Factor: ".implode(', ',$arrFormFactorTerms) ;
								}
								display_data('FORM FACTOR',$arrFormFactorTemp,$arrFormFactorId,$arrFormFactorTerms);
								break;

			case 'inputtype':	
								// inputtype
								$arrInputTypeTemp	= array_map('trim_and_lowercase_value',$arrInputMech[INPUTMECH]);
								//print_r($arrParam);
								//echo "sac3:",$arrParam[1];
								$arrParam[1]	= str_replace(' and ',' & ',$arrParam[1]);
								//echo "sac4:",$arrParam[1];
								$count	= count($arrParam);
								for( $i = 1 ; $i < $count ; $i++ ) {
									if( in_array($arrParam[$i],$arrInputTypeTemp ) ) {
										$id						= array_search( $arrParam[$i], $arrInputTypeTemp );
										$arrInputTypeId[]		= $id;
										$arrFlag['inputType']	= true;
										$arrInputTypeTerms[]	= $arrInputMech[INPUTMECH][$id];
									}
								}
								if($arrFlag['inputType']){
									$feature_id		=  INPUTMECH;
									$arrInputPID	= $product->arrGetProductIdsOfFeature( $feature_id, $arrInputTypeId , 'AND' );
									$displayTerms[] = "Input Type: ".implode(', ',$arrInputTypeTerms) ;
								}
								display_data('INPUT TYPE',$arrInputTypeTemp,$arrInputTypeId,$arrInputTypeTerms);
								break;

			case 'sim':	
								// sim
								$arrSimTemp	= array_map('trim_and_lowercase_value',$arrDualSIM[DUALSIM]);
								$arrFlag['dualSIM']		= true;
								if( $arrParam[1] == 'double' OR $arrParam[1] == 'dual' OR $arrParam[1]=='2' ) {
										$dualSIM	= 1;
										$arrSIMTerms[]	= "Dual SIM";
								} else {
										$dualSIM	= 2;
										$arrSIMTerms[]	= "Single SIM";
								}
								$feature_id		=  DUALSIM;
								$arrdualSIMPID	= $product->arrGetProductIdsOfFeature( DUALSIM, $dualSIM , '' );
								$displayTerms[] = "SIM: ".implode(', ',$arrSIMTerms) ;
								display_data('SIM',$arrSimTemp,$dualSIM,$arrSIMTerms);
								break;

			case 'ram':	
								// RAM
								$arrRAMTemp	= $arrRAM[RAM];
								$count	= count($arrParam);
								
								$ramVal			= (int) $arrParam[1];

								if ( stripos($arrParam[1], 'm') !== false) {
									$ramVal		= $ramVal ;
								}
								if ( stripos($arrParam[1], 'g') !== false) {
									$ramVal		= $ramVal * 1024 ;
								}

								if( in_array( $ramVal, $arrRAMTemp ) ) {
									
									$id				= array_search( $ramVal, $arrRAMTemp );
									$arrRAMId[]		= $id;
									$arrFlag['ram']	= true;
									$arrRAMTerms[]	= $arrParam[1];
								}

								if($arrFlag['ram']){
									$feature_id		=  RAM;
									$arrRAMPID		= $product->arrGetProductIdsOfFeature( $feature_id, $arrRAMId , '' );
									$displayTerms[] = "RAM: ".implode(', ',$arrRAMTerms) ;
								}
								display_data('RAM',$arrRAMTemp,$arrRAMId,$arrRAMTerms);
								break;


			case 'availability':
				
					// availability
					$arrAvaliableTemp			= array_map('trim_and_lowercase_value',$arrAvaliable[AVAILABLEIN]);
					$count	= count($arrParam);
					for( $i = 1 ; $i < $count ; $i++ ) {
							$arrTemp[]		= $arrParam[$i];
					}
					
					$availability			= implode(' ',$arrTemp);

					if( in_array($availability,$arrAvaliableTemp ) ) {
							$id							= array_search( $availability, $arrAvaliableTemp );
							$arrFlag['availability']	= true;
							$arrAvaliableTerms[]		= $arrAvaliable[AVAILABLEIN][$id];
					}
					if($arrFlag['availability']){
						$arrAvailPID	= $product->arrGetProductIdsOfInfo($availability);
					}
					$displayTerms[] = "Available in: ".implode(', ',$arrAvaliableTerms) ;

					display_data('AVAILIBILITY',$arrAvaliableTemp,$availability,$arrAvaliableTerms);
					break;

			case 'announced':
				
					// announced
					$arrAnnouncedTemp		= $arrAnnounced[ANOUNCEDIN];
					if( in_array($arrParam[1],$arrAnnouncedTemp ) ) {
							$id						= array_search( $arrParam[1], $arrAnnouncedTemp );
							$announced				= $arrParam[1];
							$arrFlag['announced']	= true;
							$arrAnnouncedTerms[]	= $arrAnnounced[ANOUNCEDIN][$id];
					
					}
					if($arrFlag['announced']){
						$arrInfoPID						= $product->arrGetProductIdsOfInfo($announced);
					}
					$displayTerms[] = "Announced in: ".implode(', ',$arrAnnouncedTerms) ;

					display_data('ANNOUNCED IN',$arrAnnouncedTemp,$announced,$arrAnnouncedTerms);
					break;

			case 'weight':
					$weight1[]			= (int) $arrParam[1];
					$weight1[]			= (int) $arrParam[2];
					$weight				= implode('-',$weight1);

					if( in_array($weight,$arrWeight[WEIGHT] ) ) {
							$id					= array_search( $weight, $arrWeight[WEIGHT] );
							$weighID			= $id;
							$arrFlag['weight']	= true;
							$arrWeighTerms[]	= $arrWeight[WEIGHT][$id];
					
					}
					if($arrFlag['weight']){
						$feature_id				= WEIGHT;//34;
						$arrWeightPID			= $product->arrGetProductIdsOfFeature( $feature_id, $weighID , '' );
						$displayTerms[] = "Weight : ".implode('-',$arrWeighTerms) ;
						display_data('WEIGHT',$arrWeight[WEIGHT],$weight,$arrWeighTerms);
					}
					break;

			case 'width': // LENGTH
					$width1[]			= (int) $arrParam[1];
					$width1[]			= (int) $arrParam[2];
					$width				= implode('-',$width1);

					if( in_array($width,$arrLength[LENGTH] ) ) {
							$id					= array_search( $width, $arrLength[LENGTH] );
							$widthID			= $id;
							$arrFlag['width']	= true;
							$arrwidthTerms[]	= $arrLength[LENGTH][$id];
					
					}
					if($arrFlag['width']){
						$feature_id				= LENGTH;//34;
						$arrWidhtPID			= $product->arrGetProductIdsOfFeature( $feature_id, $widthID , '' );
						$displayTerms[] = "Widht : ".implode('-',$arrwidthTerms) ;
						display_data('WIDTH',$arrLength[LENGTH],$width,$arrwidthTerms);
					}
					break;

			case 'height': // BREADTH
					$height1[]			= (int) $arrParam[1];
					$height1[]			= (int) $arrParam[2];
					$height				= implode('-',$height1);

					if( in_array($height,$arrBreadth[BREADTH] ) ) {
							$id					= array_search( $height, $arrBreadth[BREADTH] );
							$heightID			= $id;
							$arrFlag['height']	= true;
							$arrHeightTerms[]	= $arrBreadth[BREADTH][$id];
					
					}
					if($arrFlag['height']){
						$feature_id				= BREADTH;//34;
						$arrWeightPID			= $product->arrGetProductIdsOfFeature( $feature_id, $heightID , '' );
						$displayTerms[] = "Height : ".implode('-',$arrHeightTerms) ;
						display_data('HEIGHT',$arrBreadth[BREADTH],$height,$arrHeightTerms);
					}
					break;

			case 'thickness': 
					$thick1[]			= (int) $arrParam[1];
					$thick1[]			= (int) $arrParam[2];
					$thick				= implode('-',$thick1);

					if( in_array($thick,$arrThickness[THICK] ) ) {
							$id					= array_search( $thick, $arrThickness[THICK] );
							$thickID			= $id;
							$arrFlag['depth']	= true;
							$arrThicknessTerms[]	= $arrThickness[THICK][$id];
					
					}
					if($arrFlag['depth']){
						$feature_id				= THICK;//34;
						$arrDepthPID			= $product->arrGetProductIdsOfFeature( $feature_id, $thickID , '' );
						$displayTerms[] = "Thickness : ".implode('-',$arrThicknessTerms) ;
						display_data('THICKNESS',$arrThickness[THICK],$thick,$arrThicknessTerms);
					}
					break;

			case 'gps':	
					// gps
					$arrGPSTemp	= array_map('trim_and_lowercase_value',$arrGPS[GPS]);
					$arrFlag['GPS']		= true;
					if( $arrParam[1] == '1' OR $arrParam[1] == 'yes') {
							$GPS			= 1;
							$arrGPSTerms[]	= "GPS - Yes";
					} else {
							$GPS			= 2;
							$arrGPSTerms[]	= "GPS - No";
					}
					$feature_id		=  GPS;
					$arrGPSPID		= $product->arrGetProductIdsOfFeature( GPS, $GPS , '' );
					$displayTerms[] = "GPS : ".implode(', ',$arrGPSTerms) ;
					display_data('GPS',$arrGPSTemp,$GPS,$arrGPSTerms);
					break;

			case 'wifi':	
					// gps
					$arrWIFITemp		= array_map('trim_and_lowercase_value',$arrWIFI[WIFI]);
					$arrFlag['WIFI']	= true;
					if( $arrParam[1] == '1' OR $arrParam[1] == 'yes') {
							$WIFI			= 1;
							$arrWIFITerms[]	= "WIFI - Yes";
					} else {
							$WIFI			= 2;
							$arrWIFITerms[]	= "WIFI - No";
					}
					$feature_id		=  WIFI;
					$arrWIFIPID		= $product->arrGetProductIdsOfFeature( WIFI, $WIFI , '' );
					$displayTerms[] = "WIFI : ".implode(', ',$arrWIFITerms) ;
					display_data('WIFI',$arrWIFITemp,$WIFI,$arrWIFITerms);
					break;

			case 'camera':
				//print_r($arrParam);

					$r1[]			= (float) $arrParam[1];
					if(count($arrParam) > 2 ){
						$r1[]			= (float) $arrParam[2];
						$resolution		= implode('-',$r1);
					} else {
						$resolution		= $r1[0];
					}
					
					//echo '$resolution:',$resolution;
					if( in_array($resolution,$arrCamResolution[CAMRESOLUTION] ) ) {
							$id						= array_search( $resolution, $arrCamResolution[CAMRESOLUTION] );
							$rID					= $id;
							$arrFlag['camera']		= true;
							$arrCamResolutionTerms[]	= ($arrCamResolution[CAMRESOLUTION][$id])?
								 $arrCamResolution[CAMRESOLUTION][$id] : 'No';					
					}
					if($arrFlag['camera']){
						$feature_id				= CAMRESOLUTION;//34;
						$arrCamResolutionPID	= $product->arrGetProductIdsOfFeature( $feature_id, $rID , '' );
						$displayTerms[] = "Camera : ".implode('-',$arrCamResolutionTerms) . "MP";
						display_data('Camera Resolution',$arrCamResolution[CAMRESOLUTION],$rID,$arrCamResolutionTerms);
					}
					break;

			case 'orderby':
					$orderByTerm	= (isset($arrParam[1]))? strtolower($arrParam[1]) : 'price';
					$orderBy		= (isset($arrParam[2]))? strtolower($arrParam[2]) : 'asc';
					
					if($orderByTerm == 'price' ){
						$sortProductBy	= ($orderBy=='desc')? 2 : 1 ;
					} else if ($orderByTerm == 'name' ){
						$sortProductBy	= ($orderBy=='desc')? 4 : 3 ;
					}
					break;

			case 'page':
					$pagenum		= (isset($arrParam[1]))? $arrParam[1] : 1;
					break;

		}
	}

	/* Get selected brand products in given price range */
	if($arrFlag['brand']){
		if(is_array($selectedBrandArr)){
			$brandids		= implode(',',$selectedBrandArr);
		} else {
			$brandids		= $selectedBrandArr;
		}
		if(empty($brandids))	$brandids = 9999;
	}

	$arrPricePID	= $product->arrGetProductIdsOfPriceRange($mnprice,$mxprice,$brandids);
	
	$displayTerms[] = "Price from Rs. ".$mnprice." to Rs. " . $mxprice ;
	$strDisplayTerm	= implode(', ', $displayTerms);

	// SEO
	if(!empty($SEO_TAGS_BRAND)){
		$ARR_SEO_TAGS[]	= $SEO_TAGS_BRAND ;
	}

	if(!empty($SEO_TAGS_TYPE)){
		$ARR_SEO_TAGS[]	= $SEO_TAGS_TYPE;
	}

	if($arrFlag['brand'] && $arrFlag['netowrkType'] ){
		$ARR_SEO_TAGS[]	= $SEO_TAGS_BRAND_TYPE;
	}

	if($arrFlag['os']){
		$ARR_SEO_TAGS[]	= $SEO_TAGS_OS;
	}

	$SEO_TAGS		= implode(',',$ARR_SEO_TAGS);
	
	$strPrice		= ($mnprice/1000) ."K to " . ($mxprice/1000) ."K";
	$SEO_TITLE		= str_ireplace('<Price-Range>', $strPrice, $SEO_TITLE);
	$SEO_DESC		= str_ireplace('<Price-Range>', $strPrice, $SEO_DESC) . ' | ' . $strDisplayTerm ;

	if($debug==1){
		echo "<br/><br/>". $strDisplayTerm;
		echo "<br/><b>mnprice : </b>", $mnprice , '<b> mxprice : </b>', $mxprice;
		echo "<br/><b>ORDER BY : </b>", $orderByTerm ." ".$orderBy , '<b> PAGE : </b>', $pagenum;
		echo "<br/><br/>Products in Price Range for mentioned brands:";
		print_r($arrPricePID);
	}

	if(1){
		//////////////////////////  INTERSECT all arrays
		$arrFinal = $arrPricePID ;
		foreach($arrFlag as $key => $value ){
			if($value){
				switch($key){
					case 'netowrkType'	: $arrFinal = array_intersect( $arrFinal, $arrNetTypePID );
										  break;
					case 'inputType'	: $arrFinal = array_intersect( $arrFinal, $arrInputPID );
										  break;
					case 'formFactor'	: $arrFinal = array_intersect( $arrFinal, $arrDTypePID );
										  break;
					case 'phoneType'	: $arrFinal = array_intersect( $arrFinal, $arrPhoneTypePID );
										  break;
					case 'dualSIM'		: $arrFinal = array_intersect( $arrFinal, $arrdualSIMPID ); 
										  break;
					case 'GPS'			: $arrFinal = array_intersect( $arrFinal, $arrGPSPID ); 
										  break;
					case 'WIFI'			: $arrFinal = array_intersect( $arrFinal, $arrWIFIPID ); 
										  break;
					case 'weight'		: $arrFinal = array_intersect( $arrFinal, $arrWeightPID ); 
										  break;
					case 'width'		: $arrFinal = array_intersect( $arrFinal, $arrWidhtPID ); 
										  break;
					case 'height'		: $arrFinal = array_intersect( $arrFinal, $arrHeightPID ); 
										  break;
					case 'depth'		: $arrFinal = array_intersect( $arrFinal, $arrDepthPID ); 
										  break;
					case 'os'			: $arrFinal = array_intersect( $arrFinal, $arrOSPID ); 
										  break;
					case 'ram'			: $arrFinal = array_intersect( $arrFinal, $arrRAMPID ); 
										  break;
					case 'announced'	: $arrFinal = array_intersect( $arrFinal, $arrInfoPID ); 
										  break;
					case 'availability'	: $arrFinal = array_intersect( $arrFinal, $arrAvailPID ); 
										  break;
					case 'camera': $arrFinal = array_intersect( $arrFinal, $arrCamResolutionPID ); 
										  break;
					default				:  break;
 				}
			}
		}
		$product_ids	= $arrFinal;
	} /// End search


/////////////////////////////
	//print_r($selecteditemArr1);

	require_once(BASEPATH.'searchwidget.php');
	// search widget end

	// Start Pagination constants.
	define("PERPAGE",10);
	
	if(!is_numeric($startlimit)){
		$startlimit		= ($pagenum - 1) * PERPAGE ;
	}
	if(empty($endlimit)){
		$endlimit		= PERPAGE;
	}

	//echo "<br>SORT BY :" , $sortProductBy ;
	if(!empty($category_id)){
		unset($result);
		if( count($product_ids) == 0  ){ $noProductXML ="<NOPRODUCTS>".NO_PRODUCT_MESSAGE."</NOPRODUCTS>"; } else {
			switch($sortProductBy){
				case '1':
						$orderby=' order by PRICE_VARIANT_VALUES.variant_value asc ';
						$count_result = $product->arrGetProductByPriceAscCount($product_ids,$category_id,"",'1',$mnprice,$mxprice,$variant_id,$startlimit,$endlimit,$orderby);

						$result = $product->arrGetProductByPriceAsc($product_ids,$category_id,"",'1',$mnprice,$mxprice,"1",$startlimit,$endlimit);
					break;
				case '2':
						$count_result = $product->arrGetProductByPriceAscCount($product_ids,$category_id,"","1",$mnprice,$mxprice,$variant_id);

						$result = $product->arrGetProductByPriceDesc($product_ids,$category_id,"",'1',$mnprice,$mxprice,"1",$startlimit,$endlimit);
					break;
				case '3':
						$count_result = $product->arrGetProductByPriceAscCount($product_ids,$category_id,"","1",$mnprice,$mxprice,$variant_id);
						$result = $product->arrGetProductByNameAsc($product_ids,$category_id,"",'1',$mnprice,$mxprice,"1",$startlimit,$endlimit);
					break;
				case '4':
						$count_result = $product->arrGetProductByPriceAscCount($product_ids,$category_id,"","1",$mnprice,$mxprice,$variant_id);
						$result = $product->arrGetProductByNameDesc($product_ids,$category_id,"",'1',$mnprice,$mxprice,"1",$startlimit,$endlimit);
					break;
				default:break;
			}// switch end
		} // product_ids to get
	} // category condition end

	if($debug==1){
		echo "<br/><b>Total Products to list by FILTER: </b>"; print_r($product_ids);
		echo "<br/><b>Total record count by QUERY:</b> "; print_r($count_result);
		//echo "<br/><b>Total records  by QUERY:</b> "; print_r($result);
	}

//Pagination START
$totalCount		= $count_result[0]['cnt'] ?  $count_result[0]['cnt'] : 0;
$pageurl		= $_SERVER['SCRIPT_URI'];
$queryStr		= $_SERVER['QUERY_STRING'];

if( strpos($pageurl, 'page-') )  {
	$pagename		= strstr( $pageurl, '/page-', true );
} else {
	$pagename		= $pageurl ;
}

$total_records		= $totalCount;	
$scroll_page		= 5; 
$per_page			= PERPAGE; 
$current_page		= $pagenum;
$pager_url			= $pagename. '/page-';
$inactive_page_tag	= 'id="current_page"';
$previous_page_text = '&lt;';
$next_page_text		= '&gt;';
$first_page_text	= '&lt;&lt;';
$last_page_text		= '&gt;&gt;';

$kgPagerOBJ = & new kgPager();
$kgPagerOBJ -> pager_set($pager_url, $total_records, $scroll_page, $per_page, $current_page, $inactive_page_tag, $previous_page_text, $next_page_text, $first_page_text, $last_page_text);

$strPage  = '<p id="pager_links">';
$strPage .= $kgPagerOBJ -> first_page;
$strPage .= $kgPagerOBJ -> previous_page;
$strPage .= $kgPagerOBJ -> page_links;
$strPage .= $kgPagerOBJ -> next_page;
$strPage .= $kgPagerOBJ -> last_page;
$strPage .= '</p>';
$pageNavStr	= $strPage;

	$cnt = sizeof($result);
	$productxml = "<PRODUCT_MASTER>";
	$productxml .= "<TOTAL_SEARCH_ITEM_FOUND><![CDATA[".$totalCount."]]></TOTAL_SEARCH_ITEM_FOUND>";
	$productxml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
	for($i=0;$i<$cnt;$i++){
		$productxml .= "<PRODUCT_MASTER_DATA>";
		$productCnt = sizeof($result[$i]);
		$brandCheck = 0;
		$link_model_name="";$seo_model_url="";unset($modelnameSeoArr);
		for($j=0;$j<$productCnt;$j++){
			unset($modelnameArr);unset($variantnameSeoArr);
			$product_id = $result[$i][$j]['product_id'];
			$brand_id = $result[$i][$j]['brand_id'];
			$category_id = $result[$i][$j]['category_id'];
			$product_name = trim($result[$i][$j]['product_name']);
			$variant = trim($result[$i][$j]['variant']);
			$short_desc = trim($result[$i][$j]['short_desc']);

			$brand_result = $brand->arrGetBrandDetails($brand_id,$category_id);
			$brand_name = trim($brand_result[0]['brand_name']);

			//set seo url for product variant page.
			$variantnameSeoArr[] = SEO_WEB_URL;
			//$variantnameSeoArr[] = seo_title_replace($brand_name).'-mobiles';
			$variantnameSeoArr[] = 'Mobile';
			$variantnameSeoArr[] =  toAscii (seo_title_replace($brand_name).'-'.seo_title_replace($product_name));
			//$variantnameSeoArr[] = seo_title_replace($variant);
			//$variantnameSeoArr[] = 'Overviews';
			$variantnameSeoArr[] = $product_id;

			if(!empty($brand_name)){
				$modelnameArr[] = $brand_name;
			}
			if(!empty($product_name)){
				$modelnameArr[] = $product_name;
			}
			if(empty($brandCheck)){
				//get model name and seo url.
				$modelnameSeoArr[] = SEO_WEB_URL;
				$modelnameSeoArr[] = seo_title_replace($brand_name).'-cars';

				$modelnameSeoArr[] = seo_title_replace($brand_name).'-'.seo_title_replace($product_name);
				$modelnameSeoArr[] = 'Model';
				$modelnameSeoArr[] = seo_title_replace($product_name);
				$model_result=$product->arrGetProductNameInfo("",$category_id,"",$product_name);
				$model_id = $model_result[0]['product_name_id'];
				$modelnameSeoArr[] = $model_id;
				$link_model_name = implode(" ",$modelnameArr);
				$seo_model_url =  implode("/",$modelnameSeoArr);
				$brandCheck = 1;
			}
			if(!empty($variant)){
				$modelnameArr[] = $variant;
			}
			$video_path = $result[$i][$j]['video_path'];
			if(!empty($video_path)){
					$result[$i][$j] = CENTRAL_IMAGE_URL.str_replace(array(CENTRAL_IMAGE_URL),"",$video_path);
			}
			$image_path = $result[$i][$j]['image_path'];
			if(!empty($image_path)){
					$image_path = resizeImagePath($image_path,"90X120",$aModuleImageResize);
					$result[$i][$j]['image_path'] = CENTRAL_IMAGE_URL.str_replace(array(CENTRAL_IMAGE_URL),"",$image_path);
			}

			$result[$i][$j]['EXSHOWROOMPRICE_ORIGIONAL'] = trim($result[$i][$j]['variant_value']);

			$result[$i][$j]['EXSHOWROOMPRICE'] = $result[$i][$j]['variant_value'] ? 'Rs.'.priceFormat($result[$i][$j]['variant_value']) : ''; // use for price showing ie Rs-1,20,00.

			$result[$i][$j]['DISPLAY_PRODUCT_NAME'] = implode(" ",$modelnameArr);
			$result[$i][$j]['brand_name'] = $brand_name;
			$result[$i][$j]['SEO_URL'] = implode("/",$variantnameSeoArr);
			$result[$i][$j] = array_change_key_case($result[$i][$j],CASE_UPPER);

			if($j > 0){
				$productxml .= "<SIMILAR_PRODUCT_MASTER>";
				foreach($result[$i][$j] as $k=>$v){
					$productxml .= "<$k><![CDATA[$v]]></$k>";
				}
				$productxml .= "</SIMILAR_PRODUCT_MASTER>";
			}else{
				$rootxml = "";
				foreach($result[$i][$j] as $k=>$v){
					$rootxml .= "<$k><![CDATA[$v]]></$k>";
				}
			}

		}

		//start code to add reviews and ratings.
		$modelresult = $product->arrGetProductNameInfo("",$category_id,$brand_id,$product_name);
		$model_id = $modelresult[0]['product_name_id'];

		$reviewsresult = $userreview->arrGetAdminOverallGrade($category_id,$brand_id,'0',$model_id);


		$rs				= $userreview->arrGetAdminExpertGrade($category_id,'','',$product_id);
		$overallgrade	= $rs[0]['overallgrade'];
		$overallgrade	=  (empty($overallgrade))?	0 :(int) $overallgrade ;
				
		$reviewscnt = sizeof($reviewsresult);
		$overallcnt = 0;
		$overallavg = round($reviewsresult[0]['overallgrade']);
		if($reviewscnt <= 0){
			$reviewsresult = $userreview->arrGetOverallGrade($category_id,$brand_id,'0',$model_id);
			$overallavg = round($reviewsresult[0]['overallavg']);
			$overallcnt = $reviewsresult[0]['totaloverallcnt'] ? $reviewsresult[0]['totaloverallcnt'] : 0;
			$totalcnt = $userreview->arrGetUserReviewDetailsCount("","","","","",$brand_id,$category_id,$model_id);
		}
		$html = "";
		for($grade=1;$grade<=5;$grade++){
			if($grade <= $overallavg){
				$html .= '<img src="'.IMAGE_URL.'spacer.gif" class="vsblStr"/>';
			}else{
				$html .= '<img src="'.IMAGE_URL.'spacer.gif" class="dsblStr"/>';
			}
		}
		$productxml .= "<AVG_EXPERT_RATING><![CDATA[$overallgrade]]></AVG_EXPERT_RATING>";
		$productxml .= "<OVERALL_AVG_HTML><![CDATA[$html]]></OVERALL_AVG_HTML>";
		$productxml .= "<OVERALL_AVG_CNT><![CDATA[$overallavg]]></OVERALL_AVG_CNT>";
		$productxml .= "<OVERALL_TOTAL_CNT><![CDATA[$overallcnt]]></OVERALL_TOTAL_CNT>";
		$productxml .= "<OVERALL_CNT><![CDATA[$totalcnt]]></OVERALL_CNT>";
		$productxml .= "<SEO_MODEL_RATING_PAGE_URL><![CDATA[".str_replace("Model","reviews-ratings/model-user-reviews",$seo_model_url)."#Overview]]></SEO_MODEL_RATING_PAGE_URL>";
		//end code to add reviews and ratings.


		$productxml .= $rootxml;
		$productxml .= "<LINK_PRODUCT_NAME><![CDATA[".$link_model_name."]]></LINK_PRODUCT_NAME>";
		$productxml .= "<SEO_MODEL_PAGE_URL><![CDATA[".$seo_model_url."]]></SEO_MODEL_PAGE_URL>";
		$productxml .= "<SEO_MODEL_PHOTO_PAGE_URL><![CDATA[".str_replace(array("Model"),array("Model-Exterior"),$seo_model_url)."/1"."]]></SEO_MODEL_PHOTO_PAGE_URL>";
		$productxml .= "<SEO_MODEL_VIDEO_PAGE_URL><![CDATA[".str_replace(array("Model"),array("Model-Videos"),$seo_model_url)."/3"."]]></SEO_MODEL_VIDEO_PAGE_URL>";
		$productxml .= "<SIMILAR_PRODUCT_COUNT><![CDATA[$productCnt]]></SIMILAR_PRODUCT_COUNT>";
		$productxml .= "</PRODUCT_MASTER_DATA>";
	}
	$productxml .= "</PRODUCT_MASTER>";



	$selectitemxml = "<SELECTED_ITEM_MASTER>";
	$selectitemxml .= "<SELECTED_ITEM_MASTER_DATA><![CDATA[";
	$selectitemxml .=   $strDisplayTerm ;
	$selectitemxml .= "]]></SELECTED_ITEM_MASTER_DATA>";
	$selectitemxml .= "</SELECTED_ITEM_MASTER>";

	//echo $selectitemxml;

	$topStoriesXML	 = $oTopStories->getStoriesByGroupXML( $group_id , $dbconn );

	$config_details = get_config_details();
	$strXML = "<XML>";
	$strXML .= "<PAGE_NAME><![CDATA[".$_SERVER['SCRIPT_URI']."]]></PAGE_NAME>";
	$strXML .= "<SEO_JS><![CDATA[$seo_js]]></SEO_JS>";
	$strXML .= "<SEO_TITLE><![CDATA[".cleanup($SEO_TITLE)."]]></SEO_TITLE>";
	$strXML .= "<SEO_DESC><![CDATA[".cleanup($SEO_DESC)."]]></SEO_DESC>";
	$strXML .= "<SEO_TAGS><![CDATA[".cleanup($SEO_TAGS)."]]></SEO_TAGS>";
	$strXML .= "<SEO_URL><![CDATA[$seo_url]]></SEO_URL>";
	$strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$category_id]]></SELECTED_CATEGORY_ID>";
	$strXML .= "<SELECTED_BRAND_ID><![CDATA[$isBrandSelected]]></SELECTED_BRAND_ID>";
	$strXML .= "<SELECTEDTABID><![CDATA[$tab_id]]></SELECTEDTABID>";
	$strXML .= "<SORT_PRODUCT_BY><![CDATA[$sortProductBy]]></SORT_PRODUCT_BY>";
	$strXML .= "<SEO_PRICE_STR><![CDATA[".implode("-",array($mnprice,$mxprice))."]]></SEO_PRICE_STR>";
	$strXML .= $config_details;
	$strXML .= $xml;
	$strXML .= $groupnodexml;
	$strXML .= $productxml;
	$strXML .= $selbrandxml;
	$strXML .= $selfeaturexml;
	$strXML .= $sortproductxml;
	$strXML .= $selectitemxml;
	$strXML	.= "<TOP_RATED>".$topStoriesXML."</TOP_RATED>";
	$strXML .= "<PAGER><![CDATA[$pageNavStr]]></PAGER>";
	$strXML .= "<DEFAULT_SEARCH><![CDATA[$default_search]]></DEFAULT_SEARCH>";
	$strXML .= "<FROM_PHONE_FINDER><![CDATA[$from_phone_finder]]></FROM_PHONE_FINDER>";
	$strXML .= "<SELECTED_FEATURE_ID><![CDATA[$selectedfeatureid]]></SELECTED_FEATURE_ID>";
	$strXML .= "<SEARCHTYPE><![CDATA[$searchType]]></SEARCHTYPE>";
	$strXML .= $searchXML1;
	$strXML	.= "<SELECTED_NAV_TAB>4</SELECTED_NAV_TAB>";
	$strXML	.= $noProductXML;
	$strXML .= "</XML>";

	if($_REQUEST['debug']==2){ header('Content-type: text/xml');echo $strXML;exit;}
	$doc = new DOMDocument();
	$doc->loadXML($strXML);
	$doc->saveXML();
	$xslt = new xsltProcessor;
	$xsl = DOMDocument::load('xsl/results-page.xsl');
	$xslt->importStylesheet($xsl);
	print $xslt->transformToXML($doc);
?>