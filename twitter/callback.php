<?php
/**
 * @file
 * Take the user when they return from Twitter. Get access tokens.
 * Verify credentials and redirect to based on response from Twitter.
 */

/* Start session and load lib */
//session_start();
require_once('twitteroauth/twitteroauth.php');
require_once('config.php');
/* If the oauth_token is old redirect to the connect page. */
	error_log($_COOKIE['tt'].'=='.$_REQUEST['oauth_token']);
if (isset($_REQUEST['oauth_token']) && $_COOKIE['tt'] !== $_REQUEST['oauth_token']) {
  $_COOKIE['oauth_status'] = 'oldtoken';
  header('Location: ./clearsessions.php');
}

/* Create TwitteroAuth object with app key/secret and token key/secret from default phase */
$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $_COOKIE['tt'], $_COOKIE['tsk']);

/* Request access tokens from twitter */
$access_token = $connection->getAccessToken($_REQUEST['oauth_verifier']);

/* Save the access tokens. Normally these would be saved in a database for future use. */
$_COOKIE['access_token'] = $access_token;

//error_log("<br/>connnection code ".$connection->http_code."==".$_COOKIE['tt'].'=='.$_REQUEST['oauth_token']);
/* If HTTP response is 200 continue otherwise send to connect page to retry */
if (200 == $connection->http_code) {
	/* The user has been verified and the access tokens can be saved for future use */
	setcookie("status", 'verified', $sExpTime, "/",DOMAIN);
	//$sAction			  = 'register';
	/* If method is set change API call made. Test is called by default. */
	$content = $connection->get('account/verify_credentials');
	error_log(" in if condition:".$_REQUEST['oauth_token']);

	/* Remove no longer needed request tokens */
} else {
  /* Save HTTP status for error dialog on connnect page.*/
  header('Location: ./clearsessions.php');
}
