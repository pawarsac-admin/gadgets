<?php

/* Start session and load library. */
//session_start();
require_once("../include/config.php");
require_once('twitteroauth/twitteroauth.php');
require_once('config.php');

/* Build TwitterOAuth object with client credentials. */
$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET);
/* Get temporary credentials. */
$sRedirectUrl	= $_REQUEST['redirect_url'];
$iServiceId		= $_REQUEST['service_id'];
$sCallbackUrl	= OAUTH_CALLBACK."?service_id=$iServiceId&redirect_url=$sRedirectUrl&action=twitter";
$request_token = $connection->getRequestToken($sCallbackUrl);
/* Save temporary credentials to session. */

///$_SESSION['oauth_token'] = $token = $request_token['oauth_token'];
///$_SESSION['oauth_token_secret'] = $request_token['oauth_token_secret'];
//$_COOKIE['oauth_token'] = $token = $request_token['oauth_token'];
//$_COOKIE['oauth_token_secret'] = $request_token['oauth_token_secret'];
$token = $request_token['oauth_token'];
$sExpTime=-1;
//token - tt ,token secret tsk
setcookie("tt", $request_token['oauth_token'], $sExpTime, "/",DOMAIN);
setcookie("tsk", $request_token['oauth_token_secret'], $sExpTime, "/",DOMAIN);
/* If last connection failed don't display authorization link. */
switch ($connection->http_code) {
  case 200:
    /* Build authorize URL and redirect user to Twitter. */
    $url = $connection->getAuthorizeURL($token);
    header('Location: ' . $url); 
    break;
  default:
    /* Show notification if something went wrong. */
    echo 'Could not connect to Twitter. Refresh the page or try again later.';
}
