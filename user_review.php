<?php
		require_once('./include/config.php');
		require_once(CLASSPATH.'DbConn.php');
		require_once(CLASSPATH.'user_review.class.php');
		require_once(CLASSPATH.'brand.class.php');
		require_once(CLASSPATH.'category.class.php');
		require_once(CLASSPATH.'product.class.php');
		require_once(CLASSPATH.'videos.class.php');

		$dbconn = new DbConn;
		$userreview = new USERREVIEW;
		$brand = new BrandManagement;
		$category = new CategoryManagement;
		$product = new ProductManagement;
		$videoGallery = new videos();
		//print"<pre>";print_r($_REQUEST);print"</pre>";

		$category_id = $_REQUEST['catid'] ? $_REQUEST['catid'] : SITE_CATEGORY_ID;		
		$totalquestion = $_REQUEST['totalquestion'];
		$uname = trim($_REQUEST['uname']);
		$title = trim($_REQUEST['title']);
		$add_review = $_REQUEST['add_review'];
		$uid = $_REQUEST['uid'] ? $_REQUEST['uid'] : '0';
				
		$rev_brand_id = $_REQUEST['brand_id'];
		$rev_product_id = $_REQUEST['product_id'];
		$rev_product_name_id = $_REQUEST['product_info_id'];
		$rev_product_review_url = $_REQUEST['user_rev_url'];
		$rev_user_review_id = $_REQUEST['user_rev_id'];
		$is_review_added = $_REQUEST['reviewAdded'];

		

		if(!empty($uname) && !empty($add_review)){
			$request_param['title'] = htmlentities($title,ENT_QUOTES);

			$request_param['user_name'] = htmlentities($uname,ENT_QUOTES);

			$emailid = trim($_REQUEST['usr_review_emailid']);
			$request_param['email'] = htmlentities($emailid,ENT_QUOTES);

			$location = trim($_REQUEST['location']);
			$request_param['location'] = htmlentities($location,ENT_QUOTES);

			$location = trim($_REQUEST['location']);
			$request_param['location'] = htmlentities($location,ENT_QUOTES);

			$brand_id = trim($_REQUEST['Brand']);
			$request_param['brand_id'] = htmlentities($brand_id,ENT_QUOTES);

			$product_info_id = trim($_REQUEST['Model']);
			$request_param['product_info_id'] = htmlentities($product_info_id,ENT_QUOTES);

			$product_id = trim($_REQUEST['ModelVariant']);
			$request_param['product_id'] = htmlentities($product_id,ENT_QUOTES);

			$running = trim($_REQUEST['running']);
			$request_param['running'] = htmlentities($running,ENT_QUOTES);

			$year = trim($_REQUEST['year']);
			$request_param['year_manufacture'] = htmlentities($year,ENT_QUOTES);

			$color = trim($_REQUEST['color']);
			$request_param['color'] = htmlentities($color,ENT_QUOTES);

			$request_param['uid'] = '0';
			$request_param['category_id'] = $category_id;

			if($product_info_id != $is_review_added){
				$user_review_id = $userreview->intInsertUserReviewInfo($request_param);
				setcookie("reviewAdded",$product_info_id);
				if(!empty($uname)){
					$emailid = trim($_REQUEST['usr_review_emailid']);
					setcookie('rev_username',$uname, time()+3600*24,'/',$domain);
					setcookie('rev_emailid',$emailid, time()+3600*24,'/',$domain);
				}
			}

		
			unset($request_param);
			$msg = "";
			if(!empty($user_review_id)){
				$msg = "Your Review has been successfully posted.It will go live after moderation.<br /><br />";
				if(empty($rev_product_review_url)){
					$rev_product_review_url = WEB_URL;
				}
				$msg .= "<input type='button' value='Go Back' onclick='getbacktopage()' class='subBtn' />";

			}
			
			$request_param['user_review_id'] = htmlentities($user_review_id,ENT_QUOTES);

			for($i=1;$i<=$totalquestion;$i++){
				$que_id = $_REQUEST['que_id_'.$i];
				$request_param['que_id'] = htmlentities($que_id,ENT_QUOTES);

				$result = $userreview->arrGetQuestions($que_id);
				$formula = $result[0]['algorithm'];
				$totalanscnt = $_REQUEST['total_ans_ques_'.$que_id];
				for($ans=1;$ans<=$totalanscnt;$ans++){
					$ans_id = $_REQUEST['ans_'.$que_id.'_'.$ans];
					$usrReviewedArr['{ans'.$ans.'}'] = $_REQUEST['user_review_'.$que_id.'_'.$ans_id];				
				}
			    $grade = 0;
				if(sizeof($usrReviewedArr) > 0){                                                			   
				   $request_param['answer'] = implode(",",$usrReviewedArr);
				   $grade = strtr($formula,$usrReviewedArr);
				   $grade = round(parse_mathematical_string($grade));
				   $gradeArr[] = $grade."*{overallans".$i."}";
				   $request_param['grade'] = $grade;
				   $request_param['is_rating'] = '1';
				   $request_param['is_comment_ans'] = '0';
				}else{
					$answer = trim($_REQUEST['comment_'.$que_id]);
					$request_param['answer'] = htmlentities($answer,ENT_QUOTES);
					$request_param['grade'] = $grade;
					$request_param['is_rating'] = '0';
					$request_param['is_comment_ans'] = '1';
				}
				unset($usrReviewedArr);
				$user_review_ans_id = $userreview->intInsertUserReviewAnswer($request_param);			
			}
			//start code to calculate over all grade and insert into the db.
			$overall_formulaArr = array("{overallans1}"=>"2","{overallans2}"=>"4","{overallans3}"=>"2","{overallans4}"=>"3");
			$gradeformulaStr = implode('+',$gradeArr);
			$overallformulaStr = implode('+',$overall_formulaArr);
			$overall_formula = '(('.$gradeformulaStr.')/('.$overallformulaStr.'))';
			$overallgrade = strtr($overall_formula,$overall_formulaArr);
			$overallgrade = round(parse_mathematical_string($overallgrade));
			$overall_param = array('uid'=>$uid,'overallgrade'=>$overallgrade,'user_review_id'=>$user_review_id);
			$overall_id = $userreview->intInsertOverallRating($overall_param);
		}

		if(!empty($category_id)){
			$result = $brand->arrGetBrandDetails("",$category_id,1);
		}
		$cnt = sizeof($result);
		$xml = "<BRAND_MASTER>";
		$xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";			
		for($i=0;$i<$cnt;$i++){
			$brand_id = $result[$i]['brand_id'];
			
			$status = $result[$i]['status'];
			$categoryid = $result[$i]['category_id'];
			if(!empty($categoryid)){
				$category_result = $category->arrGetCategoryDetails($categoryid);
			}
			$category_name = $category_result[0]['category_name'];
			$result[$i]['js_category_name'] = $category_name;
			$result[$i]['category_name'] = html_entity_decode($category_name,ENT_QUOTES,'UTF-8');
			$result[$i]['brand_status'] = ($status == 1) ? 'Active' : 'InActive';
			$result[$i]['create_date'] = date('d-m-Y',strtotime($result[$i]['create_date']));
			$result[$i]['js_brand_name'] = $result[$i]['brand_name'];
			$result[$i]['brand_name'] = html_entity_decode($result[$i]['brand_name'],ENT_QUOTES,'UTF-8');
			$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
			$xml .= "<BRAND_MASTER_DATA>";
			foreach($result[$i] as $k=>$v){
				$xml .= "<$k><![CDATA[$v]]></$k>";
			}
			$xml .= "</BRAND_MASTER_DATA>";
		}
		$xml .= "</BRAND_MASTER>";        

        $result = $userreview->arrGetQuestions();		
        $cnt = sizeof($result);        
		$xml .= "<QUESTIONAIRE_MASTER>";
		$xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
		for($i=0;$i<$cnt;$i++){				
				$queid = $result[$i]['queid'];
				$ansresult = $userreview->arrGetQueAnswer("",$queid);
				$anscnt = 0;
				$anscnt = sizeof($ansresult);
				$xml .= "<QUESTIONAIRE_MASTER_DATA>";
					$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
					foreach($result[$i] as $k=>$v){
						$xml .= "<$k><![CDATA[$v]]></$k>";
					}
					$xml .= "<QUESTIONAIRE_ANS_MASTER>";
						$xml .= "<ANS_COUNT><![CDATA[$anscnt]]></ANS_COUNT>";
						for($ans=0;$ans<$anscnt;$ans++){	
							$ansresult[$ans] = array_change_key_case($ansresult[$ans],CASE_UPPER);
							$xml .= "<QUESTIONAIRE_ANS_MASTER_DATA>";
							foreach($ansresult[$ans] as $k=>$v){
								$xml .= "<$k><![CDATA[$v]]></$k>";
							}
							$xml .= "</QUESTIONAIRE_ANS_MASTER_DATA>";
						}
					$xml .= "</QUESTIONAIRE_ANS_MASTER>";					
				$xml .= "</QUESTIONAIRE_MASTER_DATA>";					
		}		
		$xml .= "</QUESTIONAIRE_MASTER>";

		$dateArr = arrGetDateMonthYearDate(); //Year
		$xml .= "<CURRENT_YEAR><![CDATA[".$dateArr['currentdata']['current_year']."]]></CURRENT_YEAR>";
		$xml .= "<YEAR_DATA>";
		foreach($dateArr['year'] as $year){
			$xml .= "<YEAR><![CDATA[$year]]></YEAR>";
		}
		$xml .= "</YEAR_DATA>";



		if(!empty($rev_brand_id)){
			$brand_result = $brand->arrGetBrandDetails($rev_brand_id,$category_id);
			$brand_name = $brand_result[0]['brand_name'];
		}
		if(!empty($rev_product_name_id)){
			$model_result = $product->arrGetProductNameInfo($rev_product_name_id,$category_id);
			$model_name = $model_result[0]['product_info_name'];
		}
		if(!empty($rev_product_id)){
			$product_result = $product->arrGetProductDetails($rev_product_id,$category_id);
			$product_variant = $product_result[0]['variant'];
		}

		//echo $brand_name ,$model_name ,$product_name;
		if(!empty($brand_name)){
			$name_arr[] = $brand_name;
		}
		if(!empty($model_name)){
			$name_arr[] = $model_name;
		}
		if(!empty($product_variant)){
			$name_arr[] = $product_variant;
		}
		if(!empty($name_arr)){
			$name_str = implode(" ",$name_arr);
			$seo_sub_title = "for $name_str";
			$seo_start_title = " Write A Review";
		}else{$seo_start_title =" Write a User Reviews";}


		if(!empty($_REQUEST['fname']) && !empty($_REQUEST['lname'])){
			$user_name = trim($_REQUEST['fname'])." ".trim($_REQUEST['lname']);	
		}else if(!empty($_REQUEST['rev_username'])){
			$user_name = trim($_REQUEST['rev_username']);	
		}
		
		if(!empty($_REQUEST['email'])){
			$email_id = trim($_REQUEST['email']);
		}else if(!empty($_REQUEST['rev_emailid'])){
			$email_id = trim($_REQUEST['rev_emailid']);
		}



		$seo_title=" $seo_start_title - On Mobiles India | Write Mobile Reviews  $seo_sub_title , Auto Reviews for New Mobiles as Experts or Users on ".SEO_DOMAIN;
		$seo_desc="Write User mobile reviews, automobile reviews as experts or as mobile owners. Write your own reviews on mobiles and get commented by other users on ".SEO_DOMAIN;
		$seo_keywords="write mobile reviews, User reviews, mobile reviews, new mobile reviews, auto reviews, automobile reviews, latest mobile reviews, mobile experts review, mobile owner review, write mobile reviews, get mobile reviews";

		$breadcrumb = CATEGORY_HOME." Write Review";

		$config_details = get_config_details();

       		$strXML .= "<XML>";
		$strXML .= "<BREAD_CRUMB><![CDATA[$breadcrumb]]></BREAD_CRUMB>";
	        $strXML .= $config_details;
		$strXML .= "<MSG><![CDATA[$msg]]></MSG>";
        	$strXML .= "<SEO_URL><![CDATA[$seo_url]]></SEO_URL>";
	        $strXML .= "<SEO_WEB_URL><![CDATA[".SEO_WEB_URL."]]></SEO_WEB_URL>";
        	$strXML .= "<SEO_TITLE><![CDATA[$seo_title]]></SEO_TITLE>";
	        $strXML .= "<SEO_TAGS><![CDATA[$seo_keywords]]></SEO_TAGS>";
        	$strXML .= "<SEO_DESC><![CDATA[$seo_desc]]></SEO_DESC>";
		$strXML .= "<SUB_TITLE><![CDATA[$name_str]]></SUB_TITLE>";
	        $strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$category_id]]></SELECTED_CATEGORY_ID>"; 
		$strXML.= $xml;
		
		$strXML .=  "<CATEGORY_ID><![CDATA[$rev_category_id]]></CATEGORY_ID>";
		$strXML .=  "<BRAND_ID><![CDATA[$rev_brand_id]]></BRAND_ID>";
		$strXML .=  "<PRODUCT_NAME_ID><![CDATA[$rev_product_name_id]]></PRODUCT_NAME_ID>";
		$strXML .=  "<PRODUCT_ID><![CDATA[$rev_product_id]]></PRODUCT_ID>";
		$strXML .=  "<RETURN_REVIEW_URL><![CDATA[$rev_product_review_url]]></RETURN_REVIEW_URL>";
		$strXML .=  "<USER_REVIEW_ID><![CDATA[$rev_user_review_id]]></USER_REVIEW_ID>";

		$strXML .= "<OC_ROS_BOTTOM_NORTH_728x90><![CDATA[OC_ROS_Bottom_North_728x90]]></OC_ROS_BOTTOM_NORTH_728x90>";
		$strXML .= "<OC_ROS_TOP_RHS_LREC_300x250_1><![CDATA[OC_ROS_Top_RHS_Lrec_300x250_1]]></OC_ROS_TOP_RHS_LREC_300x250_1>";


		$strXML .=  "<USERNAME><![CDATA[$user_name]]></USERNAME>";
		$strXML .=  "<EMAILID><![CDATA[$email_id]]></EMAILID>";
		$strXML .= "<PAGE_NAME>".$_SERVER['SCRIPT_URI']."</PAGE_NAME>";

		$most_recent_one_result_list = $videoGallery->getarrMostRecentVideosHeaderTabLink($category_id,"0","1",$array_result);
		$strXML.="<MOST_RECENT_VIDEO_HEADER_LINK><![CDATA[".$most_recent_one_result_list."]]></MOST_RECENT_VIDEO_HEADER_LINK>";
		$strXML.= "</XML>";

		$strXML = mb_convert_encoding($strXML, "UTF-8");
        
		if($_REQUEST['debug']==1){ header('Content-type: text/xml');echo $strXML;exit; }

        $doc = new DOMDocument();
        $doc->loadXML($strXML);
        $doc->saveXML();
	
	$xslt = new xsltProcessor;
        $xsl = DOMDocument::load('xsl/user_review.xsl');
        $xslt->importStylesheet($xsl);
        print $xslt->transformToXML($doc);

?>
