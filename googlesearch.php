<?php
	require_once('./include/config.php');
	require_once(CLASSPATH.'DbConn.php');
	require_once(CLASSPATH.'videos.class.php');
	$dbconn	=  new DbConn;
	$videoGallery = new videos();
	$config_details = get_config_details();

	$strXML .= "<XML>";
	$strXML .= $config_details;
	$most_recent_one_result_list = $videoGallery->getarrMostRecentVideosHeaderTabLink($category_id,"0","1",$array_result);
	$strXML .="<MOST_RECENT_VIDEO_HEADER_LINK><![CDATA[".$most_recent_one_result_list."]]></MOST_RECENT_VIDEO_HEADER_LINK>";
	$strXML .= "</XML>";

	$strXML = mb_convert_encoding($strXML, "UTF-8");	

	if($_REQUEST['debug']==1){ header('Content-type: text/xml');echo $strXML;exit; }

	$doc = new DOMDocument();
	$doc->loadXML($strXML);
	$doc->saveXML();

	$xslt = new xsltProcessor;
	$xsl = DOMDocument::load('xsl/googlesearch.xsl');
	$xslt->importStylesheet($xsl);
	print $xslt->transformToXML($doc);
?>
