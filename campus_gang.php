<?php
//error_reporting(E_ALL);
//ini_set('display_errors',1);
require_once("includes/config.php");
require_once(CLASSPATH."/utility.class.php");
require_once(CLASSPATH."/authentication.class.php");
require_once(CLASSPATH."/user_details.class.php");
require_once(CLASSPATH."/college_details.class.php");
require_once(CLASSPATH."/xmlparser.class.php");
require_once(CLASSPATH."/pager.class.php");
require_once(CLASSPATH.'location.class.php');
require_once(CLASSPATH."/campus_reports.class.php");
require_once(CLASSPATH."/campus_discussion.class.php");
require_once(CLASSPATH."/course_stream.class.php");


$ObjAuth= new authentication;
$dbconn = new dbconn;
$dbconn_res = $dbconn->__construct();
$oUtility = new utility;
$oUser = new user_details;
$oCollege=new college(); 
$oXmlparser=new XMLParser;
$oLocation		= new location;
$oCampusReport		= new campus_reports;
$oCampusDiscussion	= new campus_discussion();
$college_id = $_REQUEST['id'];
$iFeatured= 0;
$ObjPager=new Pager();
$aParameters=array('college_id'=>$college_id,'is_active'=>1);
$aCount=$oCollege->fetchCampusMemberListCount($aParameters);
$iRecCnt=$aCount['0']['cnt'];
$nodesPaging="";
$sTemplate="xsl/campus_gang.xsl";
$iRecordPerPage=30;

$page_url=substr($_SERVER['REQUEST_URI'],1);

if($iRecCnt!=0){
        //print_r($_REQUEST);
        if($_REQUEST['pagination']==1){
                $sTemplate="xsl/campus_gang_pagination.xsl";
        }
        $page= 1;
        if(isset($_REQUEST['page'])){
                $page= $_REQUEST['page'];
        }
        $start  = $ObjPager->findStart($iRecordPerPage);
        $jsparams=$start.",".$iRecordPerPage.",".$page_url.",gangcontainer";
        $pages= $ObjPager->findPages($iRecCnt,$iRecordPerPage);
        if($pages > 1 ){
                $pagelist= $ObjPager->jsPageNumNextPrev($page,$pages,"sPagination",$jsparams,"text");
                $nodesPaging .= "<Pages><![CDATA[".$pagelist."]]></Pages>";
                $nodesPaging .= "<Page><![CDATA[".$page."]]></Page>";
                $nodesPaging .= "<Perpage><![CDATA[".$perpage."]]></Perpage>";
        }
        $aCollegeMember=$oCollege->fetchCampusMemberList($aParameters,$sOrderfield="college_id",$sOrder="DESC",$start,$iRecordPerPage);
}

$sCollegePhotoXML="";
$sGangXML="";
//print_R($aCollegeMember);die;
for($i=0;$i<count($aCollegeMember);$i++){
	//$temp[0]=$aCollegeMember[$i]['user_id'];
	$aMember = $oUser->fetchUserProfileDetails(array('user_id'=>$aCollegeMember[$i]['user_id']));
	if(count($aMember)>0){
	$displayname =  $oUtility->getRewriteData($aMember[$aCollegeMember[$i]['user_id']]['fname'].'-'.$aMember[$aCollegeMember[$i]['user_id']]['lname']);
	$name = $aMember[$aCollegeMember[$i]['user_id']]['fname'].' '.$aMember[$aCollegeMember[$i]['user_id']]['lname'];

	$sUserImage	= $aMember[$aCollegeMember[$i]['user_id']]['photo'];

	if(empty($sUserImage)){
		$sGender  = $aMember[$aCollegeMember[$i]['user_id']]['sex'];
		if($sGender=='F'){
			$sUserImage = IMAGE_PATH.'/avatar-girl.jpg';
		}else{
			$sUserImage = IMAGE_PATH.'/avatar-boy.jpg';
		}

	}else{
		$sUserImage = IMAGE_UPLOAD_SERVER.$sUserImage;
	}

	$sGangXML .= '<Campus_Member>';
	$sGangXML .= '<user_id>'.$aCollegeMember[$i]['user_id'].'</user_id>';
	$sGangXML .= '<display_name>'.$displayname.'</display_name>';
	$sGangXML .= '<name>'.$name.'</name>';
	$sGangXML .= "<image>$sUserImage</image>";
	$sGangXML .= '</Campus_Member>';
	}
}
// getting college details
$oCourseStream	= new course_stream;
	//$aParameters = array('college_id'=>$iCollegeId);
	$aCourse  = $oCourseStream->fetchCourseStreamByCollege($college_id);
	$sCourseListing	= $oXmlparser->generateListXml($aCourse,'COURSE_LISTING');

$aParameters=array("is_active"=>1,"college_id"=>$college_id);
$aCollegeDetail=$oCollege->fetchCollegeList($aParameters,"college_id","DESC",0,1);
if(is_array($aCollegeDetail) && count($aCollegeDetail)>0){
	$aColleges[0]['college_id']	=	$aCollegeDetail[0]['college_id'];
		$aColleges[0]['name']			=	$aCollegeDetail[0]['name'];
		$aColleges[0]['display_name']	=	$oUtility->getRewriteData($aCollegeDetail[0]['name']);
		$aColleges[0]['website']	=	$aCollegeDetail[0]['website'];
		$aColleges[0]['address']	=	$aCollegeDetail[0]['address'];
		$aColleges[0]['state_id']	=	$aCollegeDetail[0]['state_id'];
		$aColleges[0]['city_id']	=	$aCollegeDetail[0]['city_id'];
		$aColleges[0]['tags']	=	$aCollegeDetail[0]['tags'];
		$aColleges[0]['gradation']	=	$aCollegeDetail[0]['gradation'];
		$aColleges[0]['campus_details']	=	$aCollegeDetail[0]['campus_details'];
		$aColleges[0]['known_for']	=	$aCollegeDetail[0]['known_for'];
		$aColleges[0]['student_population']	=	$aCollegeDetail[0]['student_population'];
		$aColleges[0]['thumb_up']	=	$aCollegeDetail[0]['thumb_up'];
		$aColleges[0]['thumb_down']	=	$aCollegeDetail[0]['thumb_down'];
		$aColleges[0]['adda_place']	=	$aCollegeDetail[0]['adda_place'];
		$aColleges[0]['is_active']	=	$aCollegeDetail[0]['is_active'];								
		$aColleges[0]['awards_certificates']	=	$aCollegeDetail[0]['awards_certificates'];
		$aColleges[0]['create_date']	=	$aCollegeDetail[0]['create_date'];
		$aColleges[0]['update_date']	=	$aCollegeDetail[0]['update_date'];
		$aColleges[0]['hashkeys']	=	$aCollegeDetail[0]['hashkey'];		
		$aColleges[0]['is_featured']	=	$aCollegeDetail[0]['is_featured'];		
		$aColleges[0]['university']	=	$aCollegeDetail[0]['university'];

		if(!empty($aColleges[0]['image'])){
			$aColleges[0]['image']	=	IMAGE_UPLOAD_SERVER.$aCollegeDetail[0]['image'];
		}else{
			$aColleges[0]['image']	=	IMAGE_PATH.'/campus-avtar.png';
		}
		
		$iCityId	= $aCollegeDetail[0]['city_id'];
		$iStateId	= $aCollegeDetail[0]['state_id'];
		$aCity		= $oLocation->fetchCityDetails(array('city_id'=>$iCityId));
		$aState		= $oLocation->fetchStateDetails(array('state_id'=>$iStateId));

		$aColleges[0]['city']		= trim($aCity[0]['city_name']);
		$aColleges[0]['state']	= $aState[0]['state_name'];
		


	//get the gradation details
	/*$aGradation=$oCollegeGradation->fetchCollegeGradationList(array("college_gradation_id"=>$aCollegeDetails[0]['gradation']) );
	$aCollegeDetails[0]['gradation_name']=$aGradation[0]['gradation_name'];	*/
	$sCollegeDetailXML=$oUtility->arraytoxml($aColleges,"COLLEGEDETAIL");
}

// getting campus reporter details
$aParameters = array('college_id'=>$college_id);
$aReport			= $oCampusReport->fetchCampusReportList($aParameters,"campus_report_id","DESC",0,1);
if(is_array($aReport) && count($aReport)>0){
	$iCampusReportId	= $aReport[0]['campus_report_id'];
	$sReportTitle		= $aReport[0]['title'];
	$aCollege = $oCollege->fetchCollegeList($aParameters,"college_id","DESC",0,1);
	$iCollegeid = $aCollege[0]['college_id'];
	$aCollege[0]['report_title'] = $sReportTitle;

	$aCampusMember		= $oCollege->fetchCampusMemberList($aParameters,"college_id","DESC",0,1);
	$iUid				= $aCampusMember[0]['user_id'];	
	//$iCollegeid = ;
	$aMember						= $oUser->fetchUserProfileDetails(array('user_id'=>$iUid));
	$sUserName						= $aMember[$iUid]['fname'].' '.$aMember[$iUid]['lname'];
	$sUserImage						= $aMember[$iUid]['image'];

	if(empty($sUserImage)){
		$sGender  = $aMember[$iUid]['sex'];
		if($sGender=='F'){
			$sUserImage = IMAGE_PATH.'/avatar-girl.jpg';
		}else{
			$sUserImage = IMAGE_PATH.'/avatar-boy.jpg';
		}
	}else{
		$sUserImage = IMAGE_UPLOAD_SERVER.$sUserImage;
	}
	
	$displayname					=  $oUtility->getRewriteData($sUserName);
	$aCollege[0]['user_name']		= $sUserName;
	$aCollege[0]['user_disp_name']  = $displayname;
	$aCollege[0]['user_image']  = $sUserImage;

	$aParameters=Array("title"=>$iCampusReportId,"cid"=>5,"sid"=>SERVICEID);
	$aData = $oCampusDiscussion->getMBDetails($aParameters);
	$iTId  = $aData['tid'];
	$aMBReplyCnt=$oCampusDiscussion->MBReplyCnt(array("tid"=>$iTId));
	$iMbReplyCnt = $aMBReplyCnt['replycnt'];
	if(empty($aMBReplyCnt['replycnt'])){
		$iMbReplyCnt = 0;
	}
	$aCollege[0]['total_comments'] = $iMbReplyCnt;

	$sCampusReporterXML=$oUtility->arraytoxml($aCollege,"CAMPUS_REPORTER");

}
require_once(CLASSPATH."course_stream.class.php");
require_once(CLASSPATH.'xmlparser.class.php');
$oXmlparser		= new XMLParser;
//get the popular campus reports
$aParameters=Array("is_featured"=>1,"is_active"=>1);
$iStartlimit=0;
$iCnt=10;
$aFeatured=$oCollege->fetchCollegeList($aParameters,$sOrderfield="college_id",$sOrder="DESC",$iStartlimit,$iCnt);	

if(is_array($aFeatured) && count($aFeatured)>0){
	$i=0;
	foreach($aFeatured as $aFeature){
		$aFeatured[$i]['disp_name'] = $oUtility->getRewriteData($aFeature['name']);
		$aFeatured[$i]['city']		= trim($aCity[$aFeatured[$i]['city_id']]);
		$aFeatured[$i]['state']	= $aState[$aFeatured[$i]['state_id']];
		if(empty($aFeatured[$i]['image'])){
			$aFeatured[$i]['image']	= IMAGE_PATH.'/campus-avtar.png';
		}else{
			$aFeatured[$i]['image']	= IMAGE_UPLOAD_SERVER.$aFeatured[$i]['image'];
		}
		$i++;
	}
}
$sFeaturedXML=$oUtility->arraytoxml($aFeatured,"FEATURE_LISTING");
///$sTemplate="xsl/featured_campus_detail.xsl";


$strXML = "<?xml version='1.0' encoding='iso-8859-1'?>";
$strXML .= "<HOME>";
$sModuleCode="CAMPUS";
$strXML.="<MODULECODE><![CDATA[".$sModuleCode."]]></MODULECODE>";

$strXML.="<CATEGORIES>".$categoryxml."</CATEGORIES>";
$strXML.="<STARID>".$iStarId."</STARID>";
$strXML.="<ITEMID>".$iItemId."</ITEMID>";
$strXML.="<FEATUREID>".$iFeatureId."</FEATUREID>";
$strXML.="<CURRPAGE>1</CURRPAGE>";
$strXML.="<ERRMESSAGE>".$sErrMessage."</ERRMESSAGE>";
$strXML.="<FNAME><![CDATA[".$_COOKIE['fname']."]]></FNAME>";
$strXML.="<LNAME><![CDATA[".$_COOKIE['lname']."]]></LNAME>";
$strXML.="<UID><![CDATA[".$_COOKIE['uid']."]]></UID>";
$strXML.="<SESSIONID><![CDATA[".$_COOKIE['session_id']."]]></SESSIONID>";
$strXML .= $strSessionXML;
$strXML .= $strCONFIGXML;
$strXML .= $sGangXML;
$strXML .= $nodesPaging;
$strXML .= $sCollegeDetailXML;
$strXML.= $sCourseListing.$sFeaturedXML;
//$strXML .= $sGangXML;
$strXML.= $sCampusReporterXML;
$strXML.="<college_id><![CDATA[".$college_id."]]></college_id>";
$strXML .= "</HOME>";


if($_GET['debug'] == 2){
        header("content-type:text/xml");
        echo $strXML;die;
}

$xml=new DOMDocument();
$xml->loadXML($strXML);

$xsl=new DOMDocument;
$xsl->load($sTemplate);

// Configure the transformer
$proc=new XSLTProcessor;
$proc->importStyleSheet($xsl);
//echo $proc->transformToXML($xml);
echo $proc->transformToXML($xml);
?>
