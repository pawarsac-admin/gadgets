<?php
	require_once('./include/config.php');
	$config_details = get_config_details();
	$breadcrumb = CATEGORY_HOME." Search";
	$seo_title="Search on ".SEO_DOMAIN;

	$referer =$_SERVER['HTTP_REFERER'];

	$strXML .= "<XML>";
	$strXML .= $config_details;
	$strXML .= "<SEO_TITLE><![CDATA[$seo_title]]></SEO_TITLE>";
	$strXML .= "<SEO_TAGS><![CDATA[$seo_keywords]]></SEO_TAGS>";
	$strXML .= "<SEO_DESC><![CDATA[$seo_desc]]></SEO_DESC>";
	$strXML .= "<BREAD_CRUMB><![CDATA[$breadcrumb]]></BREAD_CRUMB>";
	$strXML .= "<SEND_SHARE_EMAIL_URL><![CDATA[$referer]]></SEND_SHARE_EMAIL_URL>";
	$strXML.= "</XML>";

	$strXML = mb_convert_encoding($strXML, "UTF-8");	

	if($_REQUEST['debug']==1){ header('Content-type: text/xml');echo $strXML;exit; }

	$doc = new DOMDocument();
	$doc->loadXML($strXML);
	$doc->saveXML();

	$xslt = new xsltProcessor;
	$xsl = DOMDocument::load('xsl/sendemail.xsl');
	$xslt->importStylesheet($xsl);
	print $xslt->transformToXML($doc);
?>
