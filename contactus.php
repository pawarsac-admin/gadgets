<?php
#ini_set("display_errors",1);
#error_reporting(E_ALL);
require_once('./include/config.php');
require_once(CLASSPATH.'DbConn.php');
require_once(CLASSPATH.'user_feedback.class.php');
require_once(CLASSPATH."/xmlparser.class.php");

$dbconn = new DbConn;
$FEEDBACKUSER = new FEEDBACKUSER;

//$objutility=new utility;
$sErrorMsg="";
if($_POST){
	//print "<pre>";print_r($_POST);exit;
	$sRc = base64_decode($_COOKIE['Rc_auto']);
 	$sEmail	= trim($_POST['emailid']);
	$scomment	= trim($_POST['comment']);
	$syour_name	= trim($_POST['your_name']);
	$sOthereSubject = trim($_POST['subject']);

	if(empty($sOthereSubject)){
		$sErrorMsg="Please select subject.";		
	}else if(strlen($syour_name)==0){
		$sErrorMsg="Your name cannot be blank.";
	}else if(strlen($sEmail)==0){
		$sErrorMsg="Email address cannot be blank.";
	}else if(strlen($sEmail)>0){
		$iValid=isValidEmail($sEmail);
		if($iValid!=1){
			$sErrorMsg="Please enter valid email address.";						
		}		
	}
	if(empty($scomment) && strlen($sErrorMsg)==0){
		$sErrorMsg="Comment cannot be blank.";
	}else if ($sRc!=$_POST['captcha'] && strlen($sErrorMsg)==0){
		$sErrorMsg="Please enter valid captcha code.";
	}

	
	
	if(strlen($sErrorMsg)==0){
	    $request_param['subject']=htmlentities($sOthereSubject,ENT_QUOTES);
		$request_param['your_name']=htmlentities($syour_name,ENT_QUOTES);
		$request_param['comment']=htmlentities($scomment,ENT_QUOTES);
		$request_param['email_id']=htmlentities($sEmail,ENT_QUOTES);
		$user_review_id = $FEEDBACKUSER->intInsertContactusInfo($request_param);
        if($user_review_id){
		$sInserMsg="1";
	    $request_array['emails'] =  "contact@oncars.in";
	   	$request_array['message'] = htmlentities($scomment,ENT_QUOTES);
		$request_array['subject'] = "Contact Us";
		$aSubjectRes = $FEEDBACKUSER->arrGetFeedbackSubjectDetails($sOthereSubject);
		$sNamesubject= $aSubjectRes['0']['subject'];
		$request_array['Inqsubject'] = $sNamesubject;
		$request_array['from_email'] = $sEmail;
		$request_array['from_name'] = $syour_name;
		//$request_array['reply-to'] = ONCARS_NO_REPLY;
		require_once(CLASSPATH.'mail.class.php');
		$mail = new mailer('contacts');
		$isSend = $mail->boolSendContacts($request_array);

	  }
	}
}


$aSubject = $FEEDBACKUSER->arrGetFeedbackSubjectDetails();
$cnt=count($aSubject);
$sOptions = "<FEEDBACK_SUBJECT_MASTER>";
$sOptions .="<COUNT><![CDATA[$cnt]]></COUNT>";
for($i=0;$i<$cnt;$i++){
	$sOptions .= "<FEEDBACK_SUBJECT_NAME>";
foreach($aSubject[$i] as $iKey=>$sValue){
	$sOptions.= "<$iKey><![CDATA[$sValue]]></$iKey>";
	
  }
  $sOptions .= "</FEEDBACK_SUBJECT_NAME>";
}
$sOptions .= "</FEEDBACK_SUBJECT_MASTER>";


 $strXML .= "<XML>";
 $config_details = get_config_details();
        $strXML .= $config_details;
		$strXML .= "<MSG><![CDATA[$msg]]></MSG>";
        $strXML .= "<SEO_URL><![CDATA[$seo_url]]></SEO_URL>";
        $strXML .= "<SEO_WEB_URL><![CDATA[".SEO_WEB_URL."]]></SEO_WEB_URL>";
        $strXML .= "<SEO_TITLE><![CDATA[$seo_title]]></SEO_TITLE>";
        $strXML .= "<SEO_TAGS><![CDATA[$seo_keywords]]></SEO_TAGS>";
        $strXML .= "<SEO_DESC><![CDATA[$seo_desc]]></SEO_DESC>";
        $strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$category_id]]></SELECTED_CATEGORY_ID>";        
	    $strXML.="<ERRMSG><![CDATA[".$sErrorMsg."]]></ERRMSG>";
		$strXML.="<SUCESMSG><![CDATA[".$sInserMsg."]]></SUCESMSG>";
		$sCaptchaUrl="captcha.php?r=".rand();
		$strXML.="<CAPTCHAURL><![CDATA[".$sCaptchaUrl."]]></CAPTCHAURL>";
		$strXML.="<EMAIL><![CDATA[".$sEmail."]]></EMAIL>";
        $strXML.="<FEEDBACK><![CDATA[".$scomment."]]></FEEDBACK>";
		$strXML.="<SUBJECT_SELECT><![CDATA[".$sOthereSubject."]]></SUBJECT_SELECT>";		
		$strXML.= $sOptions;
	    $strXML.= "</XML>";
$sTemplate = 'xsl/contactus.xsl';
if($_GET['debug']==2){
	header ("Content-Type:text/xml");  
	echo $strXML; 
	die();

}

$doc = new DOMDocument();
$doc->loadXML($strXML);
//print $sTemplate;
$xslt = new xsltProcessor;
$xsl = DOMDocument::load($sTemplate);
$xslt->importStylesheet($xsl);
echo html_entity_decode($xslt->transformToXML($doc));
?>