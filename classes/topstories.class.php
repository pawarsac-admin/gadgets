<?php
/**
 * @brief class is used to perform actions on user reviews
 * @author Sachin
 * @version 1.0
 * @created 11-Nov-2010 5:09:31 PM
 * @last updated on 08-Mar-2011 13:14:00 PM
 */
	class TopStories extends DbOperation{

		var $cache;
		var $priceKey;
		/**Intialize the consturctor.*/
		function TopStories(){
			$this->cache = new Cache;
			$this->cacheKey = MEMCACHE_MASTER_KEY."top_stories";
		}

		function getLatestGroupId( $group_type, $dbconn ){
			$sSql	= "SELECT group_id FROM TOP_STORIES_GROUP_MASTER WHERE group_type=$group_type ORDER BY group_id DESC limit 1";
			$arrData= $this->select($sSql);
			$group_id = ( isset( $arrData[0]['group_id'] ) )? $arrData[0]['group_id'] : 0 ; 
			return $group_id;
		}

		/**
		* @note function is used to update the position of story 
		* @param an associative array $arrData.
		* @pre $arrData must be valid associative array.
		* retun integer.
		*/
		function updatePosition( $group_id, $arrData,$dbconn){
			//print_r($arrData);
			foreach ($arrData as $position => $item) :
				$sSql	= "UPDATE TOP_STORIES_MASTER SET position = $position WHERE group_id= $group_id AND story_id = $item";
				//echo "<br/>", $sSql;
				$op		= $this->update($sSql);
			endforeach;
			//$this->cache->searchDeleteKeys($this->cacheKey);			
			return $op;
		}

		/**
		* @note function is used to update all the stories 
		* @param an associative array $arrData.
		* @pre $arrData must be valid associative array.
		* retun integer.
		*/
		function updateStories( $group_id, $arrData,$dbconn){
			for($i=1;$i<= 12;$i++){
				if(isset($arrData['story_id_'.$i])){
					$story_id							= $arrData['story_id_'.$i];
					$arrTemp[$story_id]['title']		= $arrData['title_'.$i];
					$arrTemp[$story_id]['product_link']	= $arrData['product_link_'.$i];
					$arrTemp[$story_id]['product_id']	= $arrData['product_id_'.$i];
					$arrTemp[$story_id]['product_name']	= $arrData['product_name_'.$i];
				}
			}

			foreach ($arrTemp as $key => $value ) :
				if(!empty($value['product_id'])){
					$sSql	= "UPDATE TOP_STORIES_MASTER SET title = '".$value['title']."', link = '".$value['product_link'] ."', product_id = '".$value['product_id']."', product_name = '".$value['product_name']."', mdate=now() WHERE group_id= $group_id AND story_id = $key ";
					//echo "<br/>" . $sSql;
					$op		= $this->update($sSql);
					//echo $op;
				} 
			endforeach;
			
			//$this->cache->searchDeleteKeys($this->cacheKey);			
			return $op;
		}

		/**
		* @note function is used to get all the stories for given group 
		* @param	: group_id int
		* retun		: arrData array 
		*/
		function getStoriesByGroup( $group_id, $dbconn ){
			$sSql	= "SELECT T.story_id,T.group_id,T.product_id,T.product_name,T.title,T.link,T.image,T.position,T.status ,T.mdate,P.image_path,V.variant_value FROM TOP_STORIES_MASTER T ,PRODUCT_MASTER P,PRICE_VARIANT_VALUES V WHERE T.group_id= $group_id AND T.product_id=P.product_id AND T.product_id = V.product_id ORDER BY position ASC";
			$arrData= $this->select($sSql);
			return $arrData;
		}

		/**
		* @note function is used get XML all the stories for given group
		* @param	: group_id int
		* retun		: XML
		*/
		function getStoriesByGroupXML( $group_id, $dbconn){
			$arrStories	=	$this->getStoriesByGroup( $group_id, $dbconn );
			$storyXML	=	generateXML2($arrStories,"STORIES","STORY",1);
			return $storyXML;
		}


		/**
		* @note function is used to get all Groups of the stories  
		* retun		: arrData array 
		*/
		function getStoryGroups( $group_type, $dbconn ){
			$sSql	= "SELECT * FROM TOP_STORIES_GROUP_MASTER WHERE group_type=$group_type ORDER BY group_id DESC";
			$arrData= $this->select($sSql);
			return $arrData;
		}

		/**
		* @note function is used get XML all groups of the stories
		* retun		: XML
		*/
		function getStoryGroupsXML( $group_type, $dbconn ){
			$arrStories	=	$this->getStoryGroups( $group_type, $dbconn );
			$storyXML	=	generateXML2($arrStories,"TOP_STORIES_GROUP","GROUP");
			return $storyXML;
		}

		/**
		* @note function is used to create new story group  
		* retun		: int op 
		*/
		function createGroup( $group_type, $grouptitle, $noofstories, $dbconn){
			$sSql	= "INSERT INTO TOP_STORIES_GROUP_MASTER (group_id,group_type,title,no_of_stories,cdate) VALUES ('',$group_type,'$grouptitle','$noofstories',now())";
			$op		= $this->insert($sSql);
			if( $op && $noofstories > 0 ) $this->createGroupRecords( $op,$grouptitle, $noofstories, $dbconn);
			return $op;
		}


		/**
		* @note function is used to create new story records  
		* retun		: arrData array 
		*/
		function createGroupRecords( $group_id,$grouptitle, $noofstories, $dbconn){
			for($i=0;$i<$noofstories;$i++){
				$j = $i + 1;
				$sSql	= "INSERT INTO TOP_STORIES_MASTER (story_id,group_id,title,link,image,position,status,mdate) VALUES ('',$group_id,'$grouptitle $j','$grouptitle link $j','',$i,1,now())";
				//echo "<br/>",$sql ; 
				$op		= $this->insert($sSql);
			}
			return $op;
		}

		/**
		* @note function is used to create new story record 
		* retun		: arrData array 
		*/
		function createGroupRecord( $group_id, $product_id, $product_name, $grouptitle ){
				$sSql	= "INSERT INTO TOP_STORIES_MASTER (story_id,group_id,product_id,product_name, title,link,image,position,status,mdate) VALUES ('', $group_id, $product_id, '$product_name', '$grouptitle $j', '$grouptitle link $j', '', 999, 1, now() )";
				//echo "<br/>",$sSql ; 
				$op		= $this->insert($sSql);
				return $op;
		}

		/**
		* @note function is used to remove story record 
		* retun		: arrData array 
		*/
		function removeGroupRecord( $story_id ){
				$sSql	= "DELETE FROM TOP_STORIES_MASTER where story_id=$story_id";
				//echo "<br/>",$sSql ; 
				$op		= $this->sql_delete_data($sSql);
				return $op;
		}
	}
?>
