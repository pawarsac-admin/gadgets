<?php
/**
 *
 * Setup:
 *
    edit the singleton() metod
    and define the list of memcached servers in a 2-d array
    in the format
    array(
        array('192.168.0.1'=>'11211'),
        array('192.168.0.2'=>'11211'),
    );
 *
 *
 * Usage:
 *
<?php
More information can be obtained here:
http://www.danga.com/memcached/
http://www.php.net/memcache

*/
/** @ingroup classes
 *  This is the Classes group
 *  @{
*/
/**
 * @brief class Cache The class makes it easier to work with memcached servers and provides hints in the IDE like Zend Studio
 * @version 1
 *
 */

class Cache {
/**
 * Resources of the opend memcached connections
 * @var array [memcache objects]
 */
var $mc_servers = array();
/**
 * Quantity of servers used
 * @var int
 */
var $mc_servers_count;


var $servers = array(

        array('192.168.1.50'=>'11211')
    );


/**
 * Accepts the 2-d array with details of memcached servers
 *
 * @param array $servers
 */
function Cache(){

/*
    for ($i = 0, $n = count($this->servers); $i < $n; ++$i){
        ( $con = memcache_connect(key($this->servers[$i]), current($this->servers[$i])) )&&
            $this->mc_servers[] = $con;
    }
    $this->mc_servers_count = count($this->mc_servers);
    if (!$this->mc_servers_count){
        $this->mc_servers[0]=null;
    }
*/
}
/**
 * Returns the resource for the memcache connection
 *
 * @param string $key
 * @return object memcache
 */
function getMemcacheLink($key){
    if ( $this->mc_servers_count <2 ){
        //no servers choice
        return $this->mc_servers[0];
    }
    return $this->mc_servers[(crc32($key) & 0x7fffffff)%$this->mc_servers_count];
}

/**
 * Clear the cache
 *
 * @return void
 */
function flush() {
    $x = $this->mc_servers_count;
    for ($i = 0; $i < $x; ++$i){
        $a = $this->mc_servers[$i];
        $a->flush();
    }
}

function get($key, $timeout="") {
	return $arr;
	if (!$key) return false;
	$key = base64_encode($key);
	$num = 0;
	foreach ($this->servers[$num] as $host => $port) {
		$memcache_obj = memcache_pconnect($host, $port);
	}
	if (!$memcache_obj) return false;
	$arr = unserialize(memcache_get($memcache_obj,$key));
	if(empty($timeout)){
		return $arr["var"];
	}
	if((time()-$arr["time"])<$timeout){
		return $arr["var"];
	}else{
		return false;
	}
}

/**
 * Store the value in the memcache memory (overwrite if key exists)
 *
 * @param string $key
 * @param mix $var
 * @param bool $compress
 * @param int $expire (seconds before item expires)
 * @return bool
 */
function set($key, $var, $compress=0, $expire=0) {
	return false;
	if (!$key) return false;
	$key = base64_encode($key);
	$num = 0;
	foreach ($this->servers[$num] as $host => $port) {
		$memcache_obj = memcache_pconnect($host, $port);
	}
	if (!$memcache_obj) return false;

	$arr["time"]=time();
	$arr["var"] = $var;

	$var = serialize($arr);
	return memcache_set($memcache_obj,$key, $var, $compress?MEMCACHE_COMPRESSED:null, $expire);
}
/**
 * Set the value in memcache if the value does not exist; returns FALSE if value exists
 *
 * @param sting $key
 * @param mix $var
 * @param bool $compress
 * @param int $expire
 * @return bool
 */
function add($key, $var, $compress=0, $expire=0) {
	$a = $this->getMemcacheLink($key);
    return $a->add($key, $var, $compress?MEMCACHE_COMPRESSED:null, $expire);
}
function getMemcacheKeys(){
	return false;
	$num = 0;
	foreach ($this->servers[$num] as $host => $port) {
		$memcache_obj = memcache_pconnect($host, $port);
	}
	$memcacheKeyArr = Array();
	$allSlabs = $memcache_obj->getExtendedStats('slabs'); 
	foreach($allSlabs as $server => $slabs) {
		foreach($slabs AS $slabId => $slabMeta) {
			$cdump = $memcache_obj->getExtendedStats('cachedump',(int)$slabId);
				foreach($cdump AS $keys => $arrVal) {
					foreach($arrVal AS $k => $v) {                   
						$memcacheKeyArr[] = $k;
					}
				}
		}
	} 
	return $memcacheKeyArr;
}
function searchDeleteKeys($searchkey){
	return false;
	foreach($this->getMemcacheKeys() as $memcacheKey){
		$memcacheExistsKey = base64_decode($memcacheKey);
		if(strpos($memcacheExistsKey,$searchkey) !== false){
			$this->delete($memcacheKey);
		}
	}
	return true;
}
function isKeySet($searchKey){
	return false;
	foreach($this->getMemcacheKeys() as $memcacheKey){
		$memcacheKey = base64_decode($memcacheKey);
		if(strpos($memcacheKey,$searchkey) !== false){
			return '1';
		}
	}
	return '0';
}
/**
 * Replace an existing value
 *
 * @param string $key
 * @param mix $var
 * @param bool $compress
 * @param int $expire
 * @return bool
 */
function replace($key, $var, $compress=0, $expire=0) {
   	return $a->replace($key, $var, $compress?MEMCACHE_COMPRESSED:null, $expire);
}
/**
 * Delete a record or set a timeout
 *
 * @param string $key
 * @param int $timeout
 * @return bool
 */
function delete($key, $timeout=0) {
	if (!$key) return false;
	$num = 0;
	foreach ($this->servers[$num] as $host => $port) {
		$memcache_obj = memcache_pconnect($host, $port);
	}
	if (!$memcache_obj) return false;
	$memcache_obj->delete($key,$timeout);
}
/**
 * Increment an existing integer value
 *
 * @param string $key
 * @param mix $value
 * @return bool
 */
function increment($key, $value=1) {
	$a = $this->getMemcacheLink($key);
    return $a->increment($key, $value);
}

/**
 * Decrement an existing value
 *
 * @param string $key
 * @param mix $value
 * @return bool
 */
function decrement($key, $value=1) {
	$a = $this->getMemcacheLink($key);
    return $a->decrement($key, $value);
}

//class end
}

?>
