<?php
	/**
	* @brief collection of utility functions.
	* @author Rajesh Ujade
	* @version 1.0
	* @created 23-Nov-2010 5:09:31 PM
	* @last updated on 08-Mar-2011 13:14:00 PM
	*/

	/**
	* function used to get dates, months, and years.
	* return array
	*/
	function arrGetDateMonthYearDate(){
		$currentmonth = date('m');
		$currentdate = date('d');
	        $currentYear = date('Y');
        	$currentHour = date('H');
	        $currentMinute = date('i');
		$result['currentdata']['current_month'] = $currentmonth;
		$result['currentdata']['current_date'] = $currentdate;
		$result['currentdata']['current_year'] = $currentYear;
		$result['currentdata']['current_hour'] = $currentHour;
		$result['currentdata']['current_minute'] = $currentMinute;
		for($m=1;$m<=12;$m++){
			$month   = date("m", mktime(0, 0, 0, $m, 1, 0));
			$monthname = date("F",mktime(0, 0, 0, $m, 1, 0));
			$result['month'][$monthname] = $month;
		}


		for($y=$currentYear-50;$y<=$currentYear;$y++){
			$result['year'][] = $y;
		}
		for($d=1;$d<=31;$d++){
			$result['date'][$d] = $d;
        }
        for($h=0;$h<24;$h++){
            if($h < 10){
                $h = '0'.$h;
            }
            $result['hour'][$h] = $h;
        }
        for($m=0;$m<60;$m++){
            if($m < 10){
                $m = '0'.$m;
            }
            $result['minute'][$m] = $m;
        }
		return $result;
	}
	/**
	* @note function is used to translate special characters into html format.
	* @param string $string.
	* @pre $string must be non-empty valid string.
	* @post string
	* return string.
	*/
	function translatechars($string) {

		$arrCharSetISO8859 =
			array('�'=>'&#192;','�'=>'&#193;','�'=>'&#194;','�'=>'&#195;','�'=>'&#196;','�'=>'&#197;','�'=>'&#198;','�'=>'&#199;','�'=>'&#200;','�'=>'&#201;','�'=>'&#202;','�'=>'&#203;','�'=>'&#204;','�'=>'&#205;','�'=>'&#206;','�'=>'&#207;','�'=>'&#208;','�'=>'&#209;','�'=>'&#210;','�'=>'&#211;','�'=>'&#212;','�'=>'&#213;','�'=>'&#214;','�'=>'&#216;','�'=>'&#217;','�'=>'&#218;','�'=>'&#219;','�'=>'&#220;','�'=>'&#221;','�'=>'&#222;','�'=>'&#223;','�'=>'&#224;','�'=>'&#225;','�'=>'&#226;','�'=>'&#227;','�'=>'&#228;','�'=>'&#229;','�'=>'&#230;','�'=>'&#231;','�'=>'&#232;','�'=>'&#233;','�'=>'&#234;','�'=>'&#235;','�'=>'&#236;','�'=>'&#237;','�'=>'&#238;','�'=>'&#239;','�'=>'&#240;','�'=>'&#241;','�'=>'&#242;','�'=>'&#243;','�'=>'&#244;','�'=>'&#245;','�'=>'&#246;','�'=>'&#248;','�'=>'&#249;','�'=>'&#250;','�'=>'&#251;','�'=>'&#252;','�'=>'&#253;','�'=>'&#254;','�'=>'&#255;'
		);

		$arrSymbSetISO8859 =
			array('�'=>'&#161;','�'=>'&#162;','�'=>'&#163;','�'=>'&#164;','�'=>'&#165;','�'=>'&#166;','�'=>'&#167;','�'=>'&#168;','�'=>'&#169;','�'=>'&#170;','�'=>'&#171;','�'=>'&#172;','�'=>'&#174;','�'=>'&#175;','�'=>'&#176;','�'=>'&#177;','�'=>'&#178;','�'=>'&#179;','�'=>'&#180;','�'=>'&#181;','�'=>'&#182;','�'=>'&#183;','�'=>'&#184;','�'=>'&#185;','�'=>'&#186;','�'=>'&#187;','�'=>'&#188;','�'=>'&#189;','�'=>'&#190;','�'=>'&#191;','�'=>'&#215;','�'=>'&#247;'
		);


		//following symbols are not supported
		//&#8242; &#8243; &#8254; &#8364; &#8592; &#8593; &#8594; &#8595; &#8596; &#8968; &#8969; &#8970; &#8971; &#9674; &#9824; &#9827; &#9829; &#9830;
		$arrOtherCharSet = array('�'=>'&#338;','�'=>'&#339;','�'=>'&#352;','�'=>'&#353;','�'=>'&#376;','�'=>'&#402;','�'=>'&#710;','�'=>'&#732;','�'=>'&#8211;','�'=>'&#8212;','�'=>'&#8216;','�'=>'&#8217;','�'=>'&#8218;','�'=>'&#8220;','�'=>'&#8221;','�'=>'&#8222;','�'=>'&#8224;','�'=>'&#8225;','�'=>'&#8226;','�'=>'&#8230;','�'=>'&#8240;','�'=>'&#8249;','�'=>'&#8250;','�'=>'&#8364;','�'=>'&#8482;'
		);

		//Following Maths Symbols not supported
		// &#8704; to &#8901;

		//Following Greek Letters not supported
		// &#913; to &#982;


		$charsetArr = array_merge($arrCharSetISO8859,$arrSymbSetISO8859,$arrOtherCharSet);
		return strtr($string,$charsetArr);

	}
	/**
         * @note function is used to update audio/video into the database.
         * @param an integer $media_id.
         * @param an integer $upload_media_id.
         * @param a string $table_name.
         * @param a string $default_img_path.
	 * @post array with uploaded data details.
         * retun an array.
         */
	function arrUpdateAudioVideo($media_id,$upload_media_id,$table_name,$default_img_path="",$language_video_id=""){
		if(empty($media_id)){ return false;}
		require_once(CLASSPATH.'article.class.php');
		require_once(CLASSPATH.'reviews.class.php');
		require_once(CLASSPATH.'videos.class.php');
		require_once(UPLOAD_CLIENT_PATH.'Upload.php');
		$upload = new Upload;
		$article = new article;
		$reviews = new reviews;
		$videoGallery = new videos();

		$post_param = array("service_name"=>SERVICE,"service_id"=>SERVICEID,"action"=>"api","media_id"=>$media_id,"type"=>"array");
		//	print_r($post_param);
		$res = unserialize($upload->get_method($post_param,CENTRAL_API_SERVER));
		$central_media_path = $res[0]['media_path'];
		$img_path = $res[0]['img_path'][0];
			//	print "<pre>"; print_r($res);exit;
		if(!empty($central_media_path)){

			if(empty($default_img_path) && !empty($img_path)){
				$request_param['video_img_path'] = $img_path;
				$result['video_img_path'] = $img_path;
			}


			$request_param['media_path'] = $central_media_path;
			$request_param['is_media_process'] = 1;
			if($table_name == 'UPLOAD_MEDIA_ARTICLE' or $table_name == 'UPLOAD_MEDIA_NEWS'){
				$request_param['upload_media_id'] = $upload_media_id;
				$iProdArtMediaId = $article->addUpdArticleDetails($request_param,$table_name);
			}elseif($table_name == 'UPLOAD_MEDIA_REVIEWS'){
				$request_param['upload_media_id'] = $upload_media_id;
				$iProdArtMediaId = $reviews->addUpdReviewsDetails($request_param,$table_name);
			}elseif($table_name == 'VIDEO_GALLERY'){
				$request_param['video_id'] = $upload_media_id;

				$iProdArtMediaId = $videoGallery->addUpdVideosDetails($request_param,$table_name);
			}elseif($table_name == 'LANGUAGE_VIDEO_GALLERY'){
				$request_param['media_id'] = $media_id;
				$iProdArtMediaId = $videoGallery->boolUpdateLanguageVideosDetails($request_param,$table_name,$language_video_id);
			}
			$result['media_path'] = $central_media_path;
		}
		//print "<pre>";print_r($result);
		return $result;
	}
	/**
         * @note function is used to remove slashes from string.
         * @param is a string $str
         * @post string with slashes removed.
         * retun a string.
         */
	function removeSlashes($str){
		$str = explode('\\',$str);
		$str = implode("",$str);
		return $str;
	}
	/**
         * @note function is used to evaluate a mathematical string.
         * @param is a string $mathStr
         * retun an integer.
         */
	function parse_mathematical_string($mathStr){
		$total = 0;
		eval("\$total=" .$mathStr. ";");
		return $total;
	}
	$multikey="";
	/**
	* @note function is used to sort an array.
	* @param is an array $array
	* retun an sorted array in descending order.
	*/
	function multi_sort_descending($array){
		usort($array, "compare_descending");
		return $array;
	 }
	 /**
	 * @note function is used to compare array element.It is used in multi_sort method as mentioned above.
	 * @param integer array key $a.
	 * @param integer array key $b.
	 * @pre $a and $b must be valid array keys.
	 * @post boolean true/false.
	 * return boolean/string.
	 * @author Rajesh Ujade.
	 * @created 23-Nov-2010
	 */
	 function compare_descending($a, $b)
	 {
			global $multikey;
	//        return strcmp($b[$key],$a[$key]);
			return ($a[$multikey] > $b[$multikey]) ? -1 : 1;
	 }
	 /**
         * @note function is used to sort an array.
         * @param is an array $array
         * retun an sorted array in ascending order.
         */

	 function multi_sort_ascending($array){
			usort($array, "compare_ascending");
	//array_walk($result, create_function('&$val,$key','$val=" $key=\'$val\'";')); // create new array.
			return $array;
	 }
	 /**
	 * @note function is used to compare array element.It is used in multi_sort method as mentioned above.
	 * @param integer array key $a.
	 * @param integer array key $b.
	 * @pre $a and $b must be valid array keys.
	 * @post boolean true/false.
	 * return boolean/string.
	 * @author Rajesh Ujade.
	 * @created 23-Nov-2010
	 */
	 function compare_ascending($a, $b)
	 {
			global $multikey;
	//        return strcmp($b[$key],$a[$key]);
			return ($a[$multikey] < $b[$multikey]) ? -1 : 1;
	 }
	/**
	* @note function is used to get configurational path.
	* return XML String.
	*/
	function get_config_details(){
		$xmlStr .= "<SEO_WEB_URL><![CDATA[".SEO_WEB_URL."]]></SEO_WEB_URL>";
		$xmlStr .= "<ADMIN_CSS_URL><![CDATA[".ADMIN_CSS_URL."]]></ADMIN_CSS_URL>";
		$xmlStr .= "<ADMIN_IMAGE_URL><![CDATA[".ADMIN_IMAGE_URL."]]></ADMIN_IMAGE_URL>";
		$xmlStr .= "<EXCEL_DOWNLOAD_PATH><![CDATA[".EXCEL_DOWNLOAD_PATH."]]></EXCEL_DOWNLOAD_PATH>";
		$xmlStr .= "<SEO_DOMAIN><![CDATA[".SEO_DOMAIN."]]></SEO_DOMAIN>";
		$xmlStr .= "<SEO_PHONE_FINDER_URL><![CDATA[".SEO_PHONE_FINDER_URL."]]></SEO_PHONE_FINDER_URL>";
		$xmlStr .= "<SEO_COMPARE_PHONES_URL><![CDATA[".SEO_COMPARE_PHONES_URL."]]></SEO_COMPARE_PHONES_URL>";
		$xmlStr .= "<SEO_PHONE_LIST_URL><![CDATA[".SEO_PHONE_LIST_URL."]]></SEO_PHONE_LIST_URL>";
		$xmlStr .= "<SEO_PHONE_FINDER_RESULT_URL><![CDATA[".SEO_PHONE_FINDER_RESULT_URL."]]></SEO_PHONE_FINDER_RESULT_URL>";
		$xmlStr .= "<SEO_AUTO_ALL_ARTICLE_DETAIL><![CDATA[".SEO_AUTO_ALL_ARTICLE_DETAIL."]]></SEO_AUTO_ALL_ARTICLE_DETAIL>";
		$xmlStr .= "<FACEBOOK_API_KEY><![CDATA[".FACEBOOK_API_KEY."]]></FACEBOOK_API_KEY>";
		$xmlStr .= "<FACEBOOK_PAGE_URL><![CDATA[".FACEBOOK_PAGE_URL."]]></FACEBOOK_PAGE_URL>";
		$xmlStr .= "<TWITTER_PAGE_URL><![CDATA[".TWITTER_PAGE_URL."]]></TWITTER_PAGE_URL>";
		$xmlStr .= "<WEB_URL><![CDATA[".WEB_URL."]]></WEB_URL>";
		$xmlStr .= "<FB_JS_URL><![CDATA[".FB_JS_URL."]]></FB_JS_URL>";
		$xmlStr .= "<JQUERY_TOOL><![CDATA[".JQUERY_TOOL."]]></JQUERY_TOOL>";
		$xmlStr .= "<TWITTER_JS_URL><![CDATA[".TWITTER_JS_URL."]]></TWITTER_JS_URL>";
		$xmlStr .= "<SEO_WEB_TITLE><![CDATA[".SEO_WEB_TITLE."]]></SEO_WEB_TITLE>";
		$xmlStr .= "<ADMIN_WEB_URL><![CDATA[".ADMIN_WEB_URL."]]></ADMIN_WEB_URL>";
		$xmlStr .= "<IMAGE_URL><![CDATA[".IMAGE_URL."]]></IMAGE_URL>";
		$xmlStr .= "<CSS_URL><![CDATA[".CSS_URL."]]></CSS_URL>";
		$xmlStr .= "<JS_URL><![CDATA[".JS_URL."]]></JS_URL>";
		$xmlStr .= "<ADMIN_IMAGE_URL><![CDATA[".ADMIN_IMAGE_URL."]]></ADMIN_IMAGE_URL>";
		$xmlStr .= "<CSS_URL><![CDATA[".CSS_URL."]]></CSS_URL>";
		$xmlStr .= "<PLAYER_JS_URL><![CDATA[".PLAYER_JS_URL."]]></PLAYER_JS_URL>";
		$xmlStr .= "<PLAYER_URL><![CDATA[".PLAYER_URL."]]></PLAYER_URL>";
		$xmlStr .= "<ADMIN_JS_URL><![CDATA[".ADMIN_JS_URL."]]></ADMIN_JS_URL>";
		$xmlStr .= "<SITE_CATEGORY_ID><![CDATA[".SITE_CATEGORY_ID."]]></SITE_CATEGORY_ID>";
		$xmlStr .= "<PRODUCT_INFO_DETAILS><![CDATA[".PRODUCT_INFO_DETAILS."]]></PRODUCT_INFO_DETAILS>";

		$xmlStr .= "<PIWIK_HTTPS_URL><![CDATA[".PIWIK_HTTPS_URL."]]></PIWIK_HTTPS_URL>";
		$xmlStr .= "<PIWIK_HTTP_URL><![CDATA[".PIWIK_HTTP_URL."]]></PIWIK_HTTP_URL>";
		$xmlStr .= "<PIWIK_IMAGE_URL><![CDATA[".PIWIK_IMAGE_URL."]]></PIWIK_IMAGE_URL>";
		$xmlStr .= "<PIWIK_PAGE><![CDATA[".PIWIK_PAGE."]]></PIWIK_PAGE>";
		$xmlStr .= "<PIWIK_SITE_ID><![CDATA[".PIWIK_SITE_ID."]]></PIWIK_SITE_ID>";
		$xmlStr.="<APP_ID><![CDATA[".FACEBOOK_APP_ID."]]></APP_ID>";
		$xmlStr.="<FNAME><![CDATA[".$_COOKIE['fname']."]]></FNAME>";
		$xmlStr.="<LNAME><![CDATA[".$_COOKIE['lname']."]]></LNAME>";
		$xmlStr.="<UID><![CDATA[".$_COOKIE['uid']."]]></UID>";
		$xmlStr.="<SESSIONID><![CDATA[".$_COOKIE['session_id']."]]></SESSIONID>";
		$xmlStr .= "<SEO_CAR_VIDEOS_IMAGES><![CDATA[".SEO_CAR_VIDEOS_IMAGES."]]></SEO_CAR_VIDEOS_IMAGES>";
		//$xmlStr .= "<SHARE_EMAIL_URL><![CDATA[".$_SERVER['REQUEST_URI']."]]></SHARE_EMAIL_URL>";
        $sCaptchaUrl="captcha.php?r=".rand();
        $xmlStr.="<SHARECAPTCHAURL><![CDATA[".$sCaptchaUrl."]]></SHARECAPTCHAURL>";
		$xmlStr .= "<SEO_CARS_MODEL_REVIEWS><![CDATA[".SEO_CARS_MODEL_REVIEWS."]]></SEO_CARS_MODEL_REVIEWS>";
		$xmlStr .= "<SEO_CARS_MODEL_FULLREVIEWS><![CDATA[".SEO_CARS_MODEL_FULLREVIEWS."]]></SEO_CARS_MODEL_FULLREVIEWS>";
		$xmlStr .= "<SEO_USER_REVIEWS><![CDATA[".SEO_USER_REVIEWS."]]></SEO_USER_REVIEWS>";
		$xmlStr .= "<SEO_CARS_USER_REVIEWS><![CDATA[".SEO_CARS_USER_REVIEWS."]]></SEO_CARS_USER_REVIEWS>";
		$xmlStr .= "<SEO_CARS_USER_FULLREVIEWS><![CDATA[".SEO_CARS_USER_FULLREVIEWS."]]></SEO_CARS_USER_FULLREVIEWS>";
		$xmlStr .= "<GOOGLE_AD_API_KEY><![CDATA[".GOOGLE_AD_API_KEY."]]></GOOGLE_AD_API_KEY>";
		$xmlStr .= "<GOOGLE_AD_SERVICE_JS><![CDATA[".GOOGLE_AD_SERVICE_JS."]]></GOOGLE_AD_SERVICE_JS>";
		$xmlStr .= "<ONCARS_SHARING_SERVICE_EMAIL><![CDATA[".ONCARS_SHARING_SERVICE_EMAIL."]]></ONCARS_SHARING_SERVICE_EMAIL>";
		$xmlStr .= "<ONCARS_EXPERT_EMAIL><![CDATA[".ONCARS_EXPERT_EMAIL."]]></ONCARS_EXPERT_EMAIL>";
		$xmlStr .= "<ONCARS_NO_REPLY><![CDATA[".ONCARS_NO_REPLY."]]></ONCARS_NO_REPLY>";

		$xmlStr .= "<ONCARS_EXPERT_EMAIL_NAME><![CDATA[".ONCARS_EXPERT_EMAIL_NAME."]]></ONCARS_EXPERT_EMAIL_NAME>";
		$xmlStr .= "<ONCARS_NO_REPLY_NAME><![CDATA[".ONCARS_NO_REPLY_NAME."]]></ONCARS_NO_REPLY_NAME>";
		$xmlStr .= "<SEO_AUTO_USER_REVIEWS><![CDATA[".SEO_AUTO_USER_REVIEWS."]]></SEO_AUTO_USER_REVIEWS>";
		$xmlStr .= "<VIDEO_GOOGLE_ANALYTICS><![CDATA[".VIDEO_GOOGLE_ANALYTICS."]]></VIDEO_GOOGLE_ANALYTICS>";

		$xmlStr .= "<VERSION><![CDATA[".VERSION."]]></VERSION>";
		$xmlStr .= "<VIEW_TRACKER_API_PATH><![CDATA[".VIEW_TRACKER_API_PATH."]]></VIEW_TRACKER_API_PATH>";


		$xmlStr .="<LOGIN_DETAIL_URL><![CDATA[".LOGIN_DETAIL_URL."]]></LOGIN_DETAIL_URL>";
		$xmlStr .="<AUTH_API_LOGIN_URL><![CDATA[".AUTH_API_LOGIN_URL."]]></AUTH_API_LOGIN_URL>";
		$xmlStr .="<SERVICEID><![CDATA[".SERVICEID."]]></SERVICEID>";
		$xmlStr .="<AUTH_API_DOMAIN><![CDATA[".AUTH_API_DOMAIN."]]></AUTH_API_DOMAIN>";
		$xmlStr .="<AUTH_API_ACTION_URL><![CDATA[".AUTH_API_ACTION_URL."]]></AUTH_API_ACTION_URL>";
		$xmlStr .="<AUTH_WEB_URL><![CDATA[".AUTH_API_WEB_URL."]]></AUTH_WEB_URL>";
		$xmlStr .= "<CKSESSION><![CDATA[".$_COOKIE['cksession']."]]></CKSESSION>";
		$xmlStr .= "<SITE_PATH><![CDATA[".SITE_PATH."]]></SITE_PATH>";
		$xmlStr .= "<ONCARS_HOT_COMPARISONS><![CDATA[".ONCARS_HOT_COMPARISONS."]]></ONCARS_HOT_COMPARISONS>";



		return $xmlStr;
	}
	/**
         * @note function is used to get a random value
         *
         * @param an integer $length.
         * retun a random integer value.
         */
	function get_rand_id($length)
	{
		if($length>0){
			$rand_id="";
			for($i=1; $i<=$length; $i++)
			{
				mt_srand((double)microtime() * 1000000);
				$num = mt_rand(1,36);
				$rand_id .= assign_rand_value($num);
			}
		}
		return $rand_id;
	}
	/**
         * @note function is used to assign  a random value to a number.
         *
         * @param an integer $num
         * retun an alphabet.
         */
	function assign_rand_value($num)
	{
		// accepts 1 - 36
		switch($num)
		{
		case "1":
		$rand_value = "a";
		break;
		case "2":
		$rand_value = "b";
		break;
		case "3":
		$rand_value = "c";
		break;
		case "4":
		$rand_value = "d";
		break;
		case "5":
		$rand_value = "e";
		break;
		case "6":
		$rand_value = "f";
		break;
		case "7":
		$rand_value = "g";
		break;
		case "8":
		$rand_value = "h";
		break;
		case "9":
		$rand_value = "i";
		break;
		case "10":
		$rand_value = "j";
		break;
		case "11":
		$rand_value = "k";
		break;
		case "12":
		$rand_value = "l";
		break;
		case "13":
		$rand_value = "m";
		break;
		case "14":
		$rand_value = "n";
		break;
		case "15":
		$rand_value = "o";
		break;
		case "16":
		$rand_value = "p";
		break;
		case "17":
		$rand_value = "q";
		break;
		case "18":
		$rand_value = "r";
		break;
		case "19":
		$rand_value = "s";
		break;
		case "20":
		$rand_value = "t";
		break;
		case "21":
		$rand_value = "u";
		break;
		case "22":
		$rand_value = "v";
		break;
		case "23":
		$rand_value = "w";
		break;
		case "24":
		$rand_value = "x";
		break;
		case "25":
		$rand_value = "y";
		break;
		case "26":
		$rand_value = "z";
		break;
		case "27":
		$rand_value = "0";
		break;
		case "28":
		$rand_value = "1";
		break;
		case "29":
		$rand_value = "2";
		break;
		case "30":
		$rand_value = "3";
		break;
		case "31":
		$rand_value = "4";
		break;
		case "32":
		$rand_value = "5";
		break;
		case "33":
		$rand_value = "6";
		break;
		case "34":
		$rand_value = "7";
		break;
		case "35":
		$rand_value = "8";
		break;
		case "36":
		$rand_value = "9";
		break;
		}
		return $rand_value;
	}
	/**
         * @note function is used to get selected drop down listing.
         *
         * @param a comma separated list ids $sListId
	 * @param is an array $aListing.
         * retun a string.
         */
	function getSelectedDropDownlising($aListing,$sListId){
		$aListingId = -1;
		if(!empty($sListId)){
			$aListingId = explode(',',$sListId);
		}
		if(is_array($aListing) && count($aListing)>0){
			foreach($aListing as $iLisingkey=>$sListingVal){
				if(in_array($iLisingkey,$aListingId)){
					$strOptions.="<option value='$iLisingkey' selected='selected'>".$sListingVal."</option>";
				}else{
					$strOptions.="<option value='$iLisingkey'>".$sListingVal."</option>";
				}
			}
		}
		return $strOptions;
	}
	/**
         * @note function is used to get image details
         *
         * @param is an integer $iMediaId.
	 * @param is an integer $iServiceId
	 * @param is a string $action
         * retun an array of image details.
         */
	function getImageDetails($iMediaId,$iServiceId,$action='api'){
		$sString = file_get_contents(IMAGE_READER_FILE."?service_id=$iServiceId&action=api&media_id=$iMediaId");
		/*header('content-type:text/xml');
		echo IMAGE_READER_FILE."?service_id=$iServiceId&action=api&media_id=$iMediaId";die;*/
		$doc = new DOMDocument('1.0', 'utf-8');
		$doc->loadXML($sString);
		$MainImg = $doc->getElementsByTagName('IMG_PATH')->item(0)->nodeValue;
		//$ThumbImg = $doc->getElementsByTagName('IMG_PATH')->item(1)->nodeValue;
	        $MainTitle = $doc->getElementsByTagName('TITLE')->item(0)->nodeValue;
		$aImage = array('main_image'=>$MainImg,'title'=>$MainTitle);
		return $aImage;

	}
	/**
         * @note function is used to convert an array  to xml
         *
         * @param is an array $arr.
         * @param is a string $node.
         * retun a xml string.
         */
	function arraytoxml($arr,$node="MAIN"){
		$nodes='';
		$cnt = count($arr);
		for($b=0;$b<$cnt;$b++){
			$arrData = $arr[$b];
			$nodes .="<".$node.">";
			if(is_array($arrData)){
				$keys = array_keys($arrData);
				$values = array_values($arrData);

				for($i=0;$i<sizeof($keys);$i++){
					if($keys[$i]=='title'){
						$sTitle = removeSlashes($values[$i]);
						$sTitle = html_entity_decode($sTitle,ENT_QUOTES);
						if(strlen($sTitle)>100){ $sTitle = getCompactString($sTitle, 95).' ...'; }
						$nodes.="<SHORT_TITLE><![CDATA[".$sTitle."]]></SHORT_TITLE>";
					}
					$values[$i] = removeSlashes($values[$i]);
					$values[$i] = html_entity_decode($values[$i],ENT_QUOTES);
					$nodes.="<".strtoupper($keys[$i])."><![CDATA[".$values[$i]."]]></".strtoupper($keys[$i]).">";

					//echo "DDDD--".strtoupper($keys[$i]);
				}
			}
			$nodes .="</".$node.">";
		}
		//echo "DDDDDD".$nodes;
		return $nodes;
	}
	/**
	* @note function used to get query start limit  and offset.
	* @param integer current pageno.
	* @param integer perpage record count.
	* @pre pageno and count must be non-empty integer.
	* @post an array.
	* return an array.
	*/
	function arrGetPageLimit($pageno,$perpagecnt){
        if($pageno <= 0){
            $pageno = 1;//starting index of query is always begin from zero.
        }
		$pageno = $pageno - 1;
		$startlimit = $pageno * $perpagecnt;
		$limitArr = array('startlimit' => $startlimit , 'recordperpage' => $perpagecnt);
		return $limitArr;
    	}
	/**
        * @note function used to convert a string to javascript parameters.
        * @param is a string $str.
        * return is a string.
        */
	function convertStrtoJSParams($str){
		//$reg = '/<a href="(.*?)">/';
		$reg = '/<a href="(.*?)" class="(.*?)">/';
		$jsParamregExp = '/pageno=(.*?)/';
		if(preg_match_all($reg,$str,$matches,PREG_SET_ORDER)){
			$count = sizeof($matches);
			for($i=0;$i<$count;$i++){
				$searchArr[] = $matches[$i][0];
				$classname = $matches[$i][2];
				$replaceStr = str_replace(array(WEB_URL.basename('browser.php').'?',basename('browser.php').'?'),'',$matches[$i][1]);//removed page and url info
				//echo $replaceStr;exit;
				$jsparams = preg_replace($jsParamregExp,"'\\1','\\2','\\3','\\4','\\5'",$replaceStr); // created js function params.
				$replaceArr[] = "<a href=\"javascript:undefined;\" onclick=\"browseItems($jsparams);\" class=\"$classname\">";
			}
		}
		$str = str_replace($searchArr,$replaceArr,$str);
		return $str;
	}

	/**
        * @note function used to compact a string upto a certain number of characters.
        * @param string $sStr.
	* @param is an integer $stringCharLimit
        * @author Rajesh Ujade.
        * @created 23-Nov-2010
        * @pre str must be non-empty string.
        * @post string.
        * return string.
        */
	function getCompactString($sStr,$stringCharLimit){
		$stringCharLimit=$stringCharLimit+10;
		//echo $sStr."<br>";
		$sString=substr($sStr,0,$stringCharLimit);
		//echo $sString."<br>";
		$aString=explode(" ",$sString);
		$aRetString=array_pop($aString);
		$sFinalString=implode(" ",$aString);
		return $sFinalString;
	}
	/**
	* @note function used to replace the seo string using regular expression.
	* @param string str.
	* @author Rajesh Ujade.
	* @created 23-Nov-2010
	* @pre str must be non-empty string.
	* @post string.
	* return string.
	*/
	function seo_str_replace($str){
		$str = preg_replace("/^[^a-z0-9]+/", "", $str);
		$str = preg_replace("/[^a-z0-9]+$/", "", $str);
		$str = preg_replace("/[^a-z0-9]/", "-", $str);
		return $str;
	}
	/**
        * @note function used to replace the seo description using regular expression.
        * @param string $str.
	* @param string $replaceStr.
        * @author Rajesh Ujade.
        * @created 23-Nov-2010
        * @pre $str must be non-empty string.
        * @pre $replaceStr must be non-empty string.
        * @post string.
        * return string.
        */
	function seo_description_replace($str,$replaceStr=""){
		$str = preg_replace("/<([A-Za-z][A-Za-z0-9]*)[^>]*>/",$replaceStr,$str);
		$str = preg_replace("/<\/([A-Za-z][A-Za-z0-9]*)>/",$replaceStr,$str);
		return $str;
	}
	/**
        * @note function used to replace the seo title using regular expression.
        * @param string $str.
        * @param string $replaceStr.
        * @author Rajesh Ujade.
        * @created 23-Nov-2010
        * @pre $str must be non-empty string.
        * @pre $replaceStr must be non-empty string.
        * @post string.
        * return string.
        */
	function seo_title_replace($str,$replaceStr="-"){
		//$str = str_replace("-","+.",$str);
		$str = trim($str);
		$str = implode($replaceStr,explode(" ",$str));
		$str = rawurlencode($str);
		return $str;
	}


	/**
	* @note function used to check if the email address is valid
	* @param string $email - email address
	* @access public
	*/
	function isValidEmail($email){
		return eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $email);
	}
	/**
        * @note function used to rewrite data.
        * @param string $sDispTitle.
        * return string.
        */
	function getRewriteData($sDispTitle){
			$sDispTitle = str_replace('-','',$sDispTitle);
			$sDispTitle = str_replace(' ','-',$sDispTitle);
			$sDispTitle = str_replace(',','-',$sDispTitle);
			$sDispTitle = str_replace('&','',$sDispTitle);
			$sDispTitle = str_replace('/','-',$sDispTitle);
			$sDispTitle = str_replace('.','',$sDispTitle);
			$sDispTitle = str_replace('?','',$sDispTitle);
			$sDispTitle = str_replace("'",'',$sDispTitle);
			$sDispTitle = str_replace("!",'',$sDispTitle);
			$sDispTitle = str_replace('"','',$sDispTitle);
			$sDispTitle = str_replace('%','',$sDispTitle);
			return $sDispTitle;
	}
	/**
        * @note function used to format price by comma separated.
        * @param string $price.
	* @post string formated price.
        * return string.
        */
	function priceFormat($price){
		$pos_flag=0;
		$pos = strpos($price, "-");
		if($pos !== false){
			$pos_flag="1";
			$price = substr($price , 1, (strlen($price)));
		}
		$flag=0;
        	$x=strlen($price);
	        $num1="";$num2="";
        	if($x > 3){
                	$num1 = substr($price , 0, (strlen($price)-3));
	                $num2 = substr($price, -3);
        	        $size = strlen($num1);
                	if(($size%2) == "1"){
                        	$flag=1;
	                        $num1 = str_pad($num1,strlen($num1)+1 ,"0",STR_PAD_LEFT);
        	        }
                	$arr2=str_split($num1, 2);
	                $num1=implode(",",$arr2);
        	        $price = $num1.",".$num2;
	        	if($flag == 1){$price = substr($price, 1);}
	       	}
		if($pos_flag == 1){
			$price = "-".$price;
		}
		return $price;
	}
	/**
        * @note function used to get different video resolution
        * @param string $media_path
        * @post array video path details.
        * return array.
        */
	function arrGetDifferentVideoResolution($media_path){
		$extpos = strrpos($media_path, ".");
		$file = substr($media_path, 0, $extpos);
		$ext = substr($media_path,$extpos);
		$low_media_path = $file."_low".$ext;
		$videoArr['low_media_path'] = $low_media_path;
		$normal_media_path = $file."_normal".$ext;
		$videoArr['normal_media_path'] = $normal_media_path;
		return $videoArr;
	}

	/**
	* @note function used to get image path of desired size
	* @param string $img_path
	* @param string $search_size
	* @post string new image path
	* return string
	*/
	function resizeImagePath($img_path,$search_size,$aModuleImageResize,$media_id=""){
		return $new_img_path = str_replace($aModuleImageResize,$search_size,$img_path);
		$res = file_get_contents(ORIGIN_CENTRAL_SERVER.$new_img_path);
		if(strlen($res) > 0){
			$img_path = $new_img_path;
		}else{
			if(!empty($media_id) && !empty($search_size)){

				$search_size = strtoupper($search_size);
				list($width,$height) = explode("X",$search_size);
				$img_path = file_get_contents(ORIGIN_CENTRAL_SERVER."resize.php?media_id=$media_id&w=$width&h=$height&service_id=".SERVICEID);
			}
		}
		return $img_path;
	}
	function multi_array_diff($mainArr,$newArr,$searchArr,$s='0'){
		foreach($mainArr as $key => $val){
			$searchkey = $newArr[$key];
			if(in_array($searchkey,$searchArr)){
				//print_r($mainArr);
				unset($mainArr[$key]);
			}
		}
		if(empty($s)){$mainArr = array_unique($mainArr,SORT_REGULAR);sort($mainArr);}
		return $mainArr;
	}
	function multi_array_search($array,$search_str,$is_two_dimensional='0'){
		if(!empty($is_two_dimensional)){
			$cnt = sizeof($array);
			for($i=0;$i<$cnt;$i++){
				if(in_array($search_str,$array[$i])){
					return $key = $i;
				}
			}
		}else{
			$key = array_search($search_str, $array[$i]);
		}
		return $key;
	}
	function create_dir($path){
		$path_dirs = explode("/",$path);
		array_splice($path_dirs, 0, 1);
		foreach($path_dirs as $dir){
			if($dir == "") {continue;}
			$currpath .= "/".$dir;
			if(!is_dir($currpath)){
				shell_exec("mkdir $currpath");
				shell_exec("chmod 777 $currpath");
			}
		}
		return $path;
	}

	function parse_csv($filename,$flag){
		$arrCSV = array();
		// Opening up the CSV file
		if (($handle = fopen($filename, "r")) !==FALSE) {
			// Set the parent array key to 0
			$key = 0;
			$row = 0;
			$csv_file_data = array();
			$csv_fields_num = TRUE;
			$csv_head_read = TRUE;
			$csv_head_label = TRUE;
			$csv_head_read  = FALSE;
			//$csv_head_read  = TRUE;
			$csv_head_label = TRUE;
			// While there is data available loop through unlimited times (0) using separator (,)
			while (($data = fgetcsv($handle, 0, ",")) !==FALSE) {
				// Count the total keys in each row $data is the variable for each line of the array
				//$c = count($data);
				$num = count($data);
				/* CSV First Row: Assumed as Head */
				if( $csv_head_read  == FALSE && $row == 1 ) {
					/* Next Row */
					$row++;
					/* Skip Head */
					continue;
				}
				/* Should We Take Fields Info */
				if( $csv_fields_num == TRUE ) {
					$csv_file_data[$row]['fields'] = $num;
				}
				/* Read CSV Fields in Current Row */
				for ( $c = 0; $c < $num; $c++ ) {
					/* CSV Standard Read */
					$csv_file_data[$row][$c] = $data[$c];
					$csv_head_read  = TRUE;
					/* CSV Head Label Logic */
					if( $csv_head_read  == TRUE && $csv_head_label == TRUE ) {
						$head_label = strtolower ( $csv_file_data[0][$c] );
						$head_label_array[] = strtolower ( $csv_file_data[0][$c] );
						$csv_file_data[$row][$head_label] = $data[$c];
						//echo "TESTR---".$row ."=========++++===".$head_label."==|||||====".$data[$c]."<br>";
						//echo "csv_file_data[."$row."][".$head_label."]<br>";
					}
				}
				/*  Next Row */
				$row++;
			} // end while
			// Close the CSV file
			fclose($handle);
		} // end if
		return $csv_file_data;
	}

	function file_error_log($fileErrstr,$city_name){
		$myFile = BASEPATH."uploadsheet/".str_replace("","_",$city_name)."_err_log_file.xls";
		$fh = fopen($myFile, 'w') or die("can't open file");
		$stringData = $fileErrstr;
		fwrite($fh, $stringData);
		//$stringData = "\n\r";
		//fwrite($fh, $stringData);
		fclose($fh);
	}


	function microtime_float(){
       list($usec, $sec) = explode(" ", microtime());
       return ((float)$usec + (float)$sec);
    }

	function generateXML1($aResult, $sMainTag="xml") {
        //print_r($aResult);
        $data_xml = "<".$sMainTag.">";
        if (is_array($aResult)) {
           foreach ($aResult as $sKey => $sValue) {
			 $sTag = strtoupper($sKey);
			 $data_xml .= "<".$sTag."><![CDATA[".$sValue."]]></".$sTag.">";
           }
        }
        $data_xml .= "</".$sMainTag.">";
        return $data_xml;
    }
	function generateXML2($aResult, $sMainTag="xml", $sLoopTag="data",$image=0) {
        //print_r($aResult);
        $data_xml = "<".$sMainTag.">";
        if (is_array($aResult)) {
           foreach ($aResult as $Row => $aValue) {
               $data_xml .= "<".$sLoopTag.">";
               if (is_array($aValue)) {
				   $product_id =  $product_name = '';
                   foreach ($aValue as $sKey => $sValue) {
                       $sTag = strtoupper($sKey);
					   
					   switch($sKey){
							case 'product_id' : $product_id = $sValue; break;
							case 'product_name' : $product_name = $sValue; break;
							case 'image_path' : if($image && $sValue){
									$image_path = resizeImagePath($sValue,"160X120",$aModuleImageResize);
									$sValue = CENTRAL_IMAGE_URL.str_replace(array(CENTRAL_IMAGE_URL),"",$image_path);
								} break;
					   }

					   $data_xml .= "<".$sTag."><![CDATA[".$sValue."]]></".$sTag.">";

                   }
				   if(!empty($product_id)) {
						$data_xml .= "<LINKURL><![CDATA[". WEB_URL . 'Mobile/'. toAscii($product_name) .'/' . $product_id . "]]></LINKURL>";
				   }
               }
               $data_xml .= "</".$sLoopTag.">";
           }
        }
        $data_xml .= "</".$sMainTag.">";
        return $data_xml;
    }

	function toAscii($str) {
		$clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $str);
		$clean = strtolower(trim($clean, '-'));
		$clean = preg_replace("/[\/_|+ -]+/", '-', $clean);

		return $clean;
		}
 		/**
                * @note function is used to create dir path and used to create directorys structures.
                * @param integer $id.
                * @param string $path.
                * @pre $id must be valid,non-zero integer.
                * @post string $path.
                * return string.
                */
                function create_path($path=""){
                        if(!$path){ return false;}
                        $path_dirs = explode("/",$path);
                        array_splice($path_dirs, 0, 1);
                        foreach($path_dirs as $dir){
                                if($dir == "") {continue;}
                                $currpath .= "/".$dir;
                                if(!is_dir($currpath)){
                                        @mkdir($currpath,0777,true);
                                        shell_exec("chmod 777 $currpath");
                                }
                        }
                        return $path;
                }
?>
