<?php
/**
 * @brief class is used add,update,delete,get product details.
 * @author Rajesh Ujade
 * @version 1.0
 * @created 11-Nov-2010 5:09:31 PM
 */
class ProductManagement extends DbOperation
{

	var $newBodyStyleArr;
	var $newFuelTypeArr;
	var $newImpFeatureArr;
	var $newTranmissionArr;
	var $newSeatingCapcityArr;
	var $cache;
	var $productKey;
	/**Intialize the consturctor.*/
	function ProductManagement(){
		$this->cache = new Cache;
		$this->productKey = MEMCACHE_MASTER_KEY."product";
		$this->featurekey = MEMCACHE_MASTER_KEY."feature";
	}
	/**
	 * @note function is used to insert the product information into the database.
	 * @param an associative array $insert_param.
	 * @pre $insert_param must be valid associative array.
	 * @post an integer $feature_id.
	 * retun integer.
	 */
	function intInsertProduct($insert_param)
	{
		$insert_param['create_date'] = date('Y-m-d H:i:s');
		$insert_param['update_date'] = date('Y-m-d H:i:s');
		$sql = $this->getInsertSql("PRODUCT_MASTER",array_keys($insert_param),array_values($insert_param));
		$product_id = $this->insert($sql);
		if($product_id == 'Duplicate entry'){ return 'exists';}
		$this->cache->searchDeleteKeys($this->productKey);
		return $product_id;
	}
	/**
	 * @note function is used to insert the product Feature information into the database.
	 * @param an associative array $insert_param.
	 * @pre $insert_param must be valid associative array.
	 * @post an integer $feature_id.
	 * retun integer.
	 */
	function intInsertProductFeature($insert_param)
	{
		$insert_param['create_date'] = date('Y-m-d H:i:s');
		$sql = $this->getInsertUpdateSql("PRODUCT_FEATURE",array_keys($insert_param),array_values($insert_param));
		$feature_id = $this->insertUpdate($sql);
		$this->cache->searchDeleteKeys($this->productKey."_feature");
		return $feature_id;
	}
	/**
	* @note function is used to update the product name in the database.
	* @param product_info_name is a string name
	* @param old_product_name is a string name
	* @param status is a boolean value 0,1 or null
	* @post an integer product_id.
	* retun integer.
	*/
	function boolUdateProductName($product_info_name,$old_product_name,$status){
		$sql = "update PRODUCT_MASTER set product_name = '$product_info_name',status='$status' where lower(product_name) = '$old_product_name'";
		$isUpdate = $this->update($sql);
		$this->cache->searchDeleteKeys($this->productKey);
		return $isUpdate;
	 }
	
	/**
	 * @note function is used to insert the product model information into the database.
	 * @param an associative array $insert_param.
	 * @pre $insert_param must be valid associative array.
	 * @post an integer $model_id.
	 * retun integer.
	 */
	function addUpdProductInfoDetails($insert_param)
	{
		require_once(CLASSPATH.'price.class.php');
		$oPrice	= new price;
		$insert_param['create_date'] = date('Y-m-d H:i:s');
		$insert_param['update_date']=date("Y-m-d H:i:s"); 
		$sql = $this->getInsertUpdateSql("PRODUCT_NAME_INFO",array_keys($insert_param),array_values($insert_param));
		$model_id = $this->insertUpdate($sql);
		$product_info_name = $insert_param['product_info_name'];
		$product_result = $this->arrGetProductByName($product_info_name);
		$status = $insert_param['status'];
		if(is_array($product_result)){
			foreach($product_result as $k=>$v){
				$product_id = $v['product_id'];
				if(!empty($product_id)){
					$update_param['status'] = $status;
					$update_param['product_id'] = $product_id;
					$this->boolUpdateProduct($product_id,$update_param);
					$oPrice->intInsertVariantValueDetail($update_param);
				}
			}
		}
		$this->cache->searchDeleteKeys($this->productKey);
		return $model_id;
	}
	/**
	 * @note function is used to update the product into the database.
	 * @param an associative array $update_param.
	 * @param an integer $product_id.
	 * @pre $update_param must be valid associative array and $product_id must be non-empty/zero valid integer.
	 * @post boolean true/false.
	 * retun boolean.
	 */
	 function boolUpdateProduct($product_id,$update_param)
	 {
		$update_param['update_date'] = date('Y-m-d H:i:s');
		$update_param['create_date'] = date('Y-m-d H:i:s');
		$sql = $this->getUpdateSql("PRODUCT_MASTER",array_keys($update_param),array_values($update_param),"product_id",$product_id);
		$isUpdate = $this->update($sql);
		$this->cache->searchDeleteKeys($this->productKey);
		$this->cache->searchDeleteKeys($this->featurekey);
			
		return $isUpdate;
	 }
	 /**
	 * @note function boolUpdateProductFeature is used to update the product feature into the database.
	 * @param an associative array $update_param
	 * @param an integer $iProdFeatureId.
	 * @pre $update_param must be valid associative array and $product_id must be non-empty/zero valid integer.
	 * @post boolean true/false.
	 * retun boolean.
	 */
	 function boolUpdateProductFeature($iProdFeatureId,$update_param)
	 {
		$sql = $this->getUpdateSql("PRODUCT_FEATURE",array_keys($update_param),array_values($update_param),"product_feature_id",$iProdFeatureId);
		$isUpdate = $this->update($sql);
		$this->cache->searchDeleteKeys($this->productKey."_feature");
		return $isUpdate;
	 }
	 /**
	 * @note function is used to delete the product.
	 * @param integer $product_id.
	 * @pre $product_id must be non-empty/zero valid integer.
	 * @post boolean true/false.
	 * return boolean.
	 */
	 function boolDeleteProduct($product_id)
	 {
		$sql = "delete from PRODUCT_MASTER where product_id = $product_id";
		$isDelete = $this->sql_delete_data($sql);
		$this->cache->searchDeleteKeys($this->productKey);
		return $isDelete;
	 }
	 /**
	 * @note function is used to delete the product.
	 * @param integer $product_id.
	 * @pre $product_id must be non-empty/zero valid integer.
	 * @post boolean true/false.
	 * return boolean.
	 */
	 function boolDeleteProductFeature($product_feature_id)
	 {
		$sql = "delete from PRODUCT_FEATURE where product_feature_id = $product_feature_id";
		$isDelete = $this->sql_delete_data($sql);
		$this->cache->searchDeleteKeys($this->productKey."_feature");
		return $isDelete;
	 }
	 /**
	 * @note function is used to get product details.
	 *
	 * @param an integer/comma seperated product ids/ product ids array $product_ids.
	 * @param an integer category_id.
	 * @param an integer brand_id.
	 * @param boolean Active/InActive $status.
	 * @param integer $startlimit.
	 * @param integer $count.
	 *
	 * @pre not required.
			 *
	 * @post product details in associative array.
	 * retun an array.
	 */
	 function arrGetProductDetails($product_ids="",$category_id="",$brand_id="",$status='1',$startprice="",$endprice="",$variant_id="1",$startlimit="",$count="",$default_city="1",$orderby="",$product_name="",$city_id=""){
		$keyArr[] = $this->productKey;
		$selectStr = "PRODUCT_MASTER.*";
		if(!empty($startprice)){
			$keyArr[] = "startprice_$startprice";
			$whereClauseArr[] = "PRICE_VARIANT_VALUES.variant_value>=$startprice";				
		}
		if(!empty($endprice)){
			$keyArr[] = "endprice_$endprice";
			$whereClauseArr[] = "PRICE_VARIANT_VALUES.variant_value<=$endprice";
		}
		if(!empty($variant_id)){
			$keyArr[] = "variant_id_$variant_id";
			$whereClauseArr[] = "PRICE_VARIANT_VALUES.variant_id = $variant_id";				
		}
		if(!empty($city_id)){
			$keyArr[] = "city_id_$city_id";
			$whereClauseArr[] = "PRICE_VARIANT_VALUES.city_id = $city_id";				
		}
		if(!empty($startprice) or !empty($endprice) or !empty($variant_id) or !empty($city_id) or !empty($default_city)){
			$whereClauseArr[] = "PRODUCT_MASTER.product_id = PRICE_VARIANT_VALUES.product_id";	
			if(!empty($default_city)){
				$whereClauseArr[] = "PRICE_VARIANT_VALUES.default_city=1";
			}
			$selectStr = "PRODUCT_MASTER.*,PRICE_VARIANT_VALUES.*";
			$tablenameArr[] = "PRICE_VARIANT_VALUES";
		}

		$tablenameArr[] = "PRODUCT_MASTER";
		if(is_array($product_ids)){
			$product_ids = implode(",",$product_ids);
		}
		if(is_array($category_id)){
			$category_id = implode(",",$category_id);
		}
		if($status != ''){
			$keyArr[] = "status_$status";
			$whereClauseArr[] = "PRODUCT_MASTER.status=$status";
		}
		if(!empty($product_ids)){
			$keyArr[] = "product_id_$product_ids";
			$whereClauseArr[] = "PRODUCT_MASTER.product_id in($product_ids)";
		}
		if(!empty($category_id)){
			$keyArr[] = "category_id_$category_id";
			$whereClauseArr[] = "PRODUCT_MASTER.category_id in ($category_id)";
		}
		if(!empty($brand_id)){
			$keyArr[] = "brand_id_$brand_id";
			$whereClauseArr[] = "PRODUCT_MASTER.brand_id in ($brand_id)";
		}
		if(!empty($product_name)){
			$keyArr[] = "product_name_$product_name";
			$product_name = strtolower($product_name);
			$whereClauseArr[] = "lower(PRODUCT_MASTER.product_name)='$product_name'";
		}
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
		}
		if(!empty($startlimit)){
			$keyArr[] = "startlimit_$startlimit";
			$limitArr[] = $startlimit;
		}
		if(!empty($count)){
			$keyArr[] = "count_$count";
			$limitArr[] = $count;
		}
		if(sizeof($limitArr) > 0){
			$limitStr = " limit ".implode(" , ",$limitArr);
		}
		if(!empty($orderby)) { 
			$orderby = $orderby; 
		}
		$key = implode("_",$keyArr);
		if($result = $this->cache->get($key)){return $result;}
		$tableStr = implode(",",$tablenameArr);
		$sql = "select $selectStr,availibility from $tableStr $whereClauseStr $orderby $limitStr";		
		$result = $this->select($sql);
		$this->cache->set($key,$result);
		return $result;
	 }
	/**
	* @note function is used to get unique product name.
	* @param an integer category_id.
	* @param an integer brand_id.
	* @post an associative array.
	* retun an array.
	*/
	function arrGetUniqueProductName($category_id,$brand_id){
		$keyArr[] = $this->productKey."_unique";
		$keyArr[] = "category_id_$category_id";
		$keyArr[] = "brand_id_$brand_id";
		$key = implode("_",$keyArr);
		if($result = $this->cache->get($key)){return $result;}
		$sql = "Select * from PRODUCT_MASTER where category_id = $category_id and brand_id = $brand_id group by product_name order by product_name asc";
		$result = $this->select($sql);
		$this->cache->set($key,$result);
		return $result;
	}
	/**
	* @note function is used to get product details count.
	* @pre not required.
	* @param an integer/comma seperated product ids/ product ids array $product_ids.
	* @param an integer/comma seperated product ids/ product ids array $category_id.
	* @param an integer/comma seperated product ids/ product ids array $brand_id.
	* @param is a boolean value $status.
	* @param $startprice.
	* @param $endprice.
	* @param is an integer value $variant_id.
	* @param is an integer value $startlimit.
	* @param is an integer value $cnt.
	* @param is an integer $default_city.
	* @param is a string $orderby.
	*
	* @post an associative array.
	* retun an array.
	*/
	function arrGetProductDetailsCount($product_ids="",$category_id="",$brand_id="",$status='1',$startprice="",$endprice="",$variant_id="1",$startlimit="",$cnt="",$default_city="1",$orderby="",$product_name="",$city_id="")
	 {
		$keyArr[] = $this->productKey."_count";
		if(!empty($startprice)){
			$keyArr[] = "startprice_$startprice";
			$whereClauseArr[] = "PRICE_VARIANT_VALUES.variant_value>=$startprice";				
		}
		if(!empty($endprice)){
			$keyArr[] = "endprice_$endprice";
			$whereClauseArr[] = "PRICE_VARIANT_VALUES.variant_value<=$endprice";
		}
		if(!empty($variant_id)){
			$keyArr[] = "variant_id_$variant_id";
			$whereClauseArr[] = "PRICE_VARIANT_VALUES.variant_id = $variant_id";				
			$whereClauseArr[] = "PRICE_VARIANT_VALUES.status = 1";
		}
		if(!empty($city_id)){
			$whereClauseArr[] = "PRICE_VARIANT_VALUES.city_id = $city_id";
		}
		if(!empty($startprice) or !empty($endprice) or !empty($variant_id) or !empty($city_id) or !empty($default_city)){
			$whereClauseArr[] = "PRODUCT_MASTER.product_id = PRICE_VARIANT_VALUES.product_id";
			if(!empty($default_city)){
				$whereClauseArr[] = "PRICE_VARIANT_VALUES.default_city=1";
			}
			$selectStr = "PRODUCT_MASTER.*,PRICE_VARIANT_VALUES.*";
			$tablenameArr[] = "PRICE_VARIANT_VALUES";
		}
		$tablenameArr[] = "PRODUCT_MASTER";
		if(is_array($product_ids)){
			$product_ids = implode(",",$product_ids);
		}
		if(is_array($category_id)){
			$category_id = implode(",",$category_id);
		}
		if($status != ''){
			$keyArr[] = "status_$status";
			$whereClauseArr[] = "PRODUCT_MASTER.status=$status";
		}
		if(!empty($product_ids)){
			$keyArr[] = "product_id_$product_ids";
			$whereClauseArr[] = "PRODUCT_MASTER.product_id in($product_ids)";
		}
		if(!empty($product_name)){
			$keyArr[] = "product_name_$product_name";
			$product_name = strtolower($product_name);
			$whereClauseArr[] = "lower(PRODUCT_MASTER.product_name)='$product_name'";
		}
		if(!empty($category_id)){
			$keyArr[] = "category_id_$category_id";
			$whereClauseArr[] = "PRODUCT_MASTER.category_id in ($category_id)";
		}
		if(!empty($brand_id)){
			$keyArr[] = "brand_id_$brand_id";
			$whereClauseArr[] = "PRODUCT_MASTER.brand_id in ($brand_id)";
		}
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
		}
		if(!empty($startlimit)){
			$keyArr[] = "startlimit_$startlimit";
			$limitArr[] = $startlimit;
		}
		if(!empty($count)){
			$keyArr[] = "count_$count";
			$limitArr[] = $count;
		}
		if(sizeof($limitArr) > 0){
			$limitStr = " limit ".implode(" , ",$limitArr);
		}
		if(!empty($orderby)) { 
			$orderby = $orderby; 
		}
		$key = implode("_",$keyArr);
		if($result = $this->cache->get($key)){return $result;}
		$tableStr = implode(",",$tablenameArr);			
		$sql = "select count(PRODUCT_MASTER.product_id) as cnt from $tableStr $whereClauseStr $orderby";
		$result = $this->select($sql);
		$this->cache->set($key,$result);
		return $result;
	}
	/**
	* @note function is used to get product feature details.
	* @pre not required.
	* @param an integer/comma seperated product feature ids $product_feature_id.
	* @param an integer/comma seperated feature ids $feature_id.
	* @param an integer/comma seperated product ids $product_id.
	* @param is an integer value $startlimit.
	* @param is an integer value $cnt.
	*
	* @post an associative array.
	* retun an array.
	*/
	function arrGetProductFeatureDetails($product_feature_id="",$feature_id="",$product_id="",$startlimit="",$cnt=""){
		$keyArr[] = $this->productKey."_feature";
		if(!empty($product_feature_id)){
			$keyArr[] = "product_feature_id_$product_feature_id";
			$whereClauseArr[] = "PRODUCT_FEATURE.product_feature_id in ($product_feature_id)";
		}
		if(!empty($feature_id)){
			$keyArr[] = "feature_id_$feature_id";
			$whereClauseArr[] = "PRODUCT_FEATURE.feature_id in ($feature_id)";
		}
		if(!empty($product_id)){
			$keyArr[] = "product_id_$product_id";
			$whereClauseArr[] = "PRODUCT_FEATURE.product_id in ($product_id)";
		}
		if(!empty($startlimit)){
			$keyArr[] = "startlimit_$startlimit";
			$limitArr[] = $startlimit;
		}
		if(!empty($cnt)){
			$keyArr[] = "cnt_$cnt";
			$limitArr[] = $cnt;
		}
		$whereClauseArr[] = "FEATURE_MASTER.feature_id = PRODUCT_FEATURE.feature_id";
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
		}
		if(sizeof($limitArr) > 0){
			$limitStr = " limit ".implode(",",$limitArr);
		}
		$key = implode("_",$keyArr);
		//echo $key."<br>";
		if($result = $this->cache->get($key)){return $result;}
		$sql = "select PRODUCT_FEATURE.*,FEATURE_MASTER.* from PRODUCT_FEATURE,FEATURE_MASTER $whereClauseStr order by PRODUCT_FEATURE.create_date desc $limitStr";
		//echo $sql;
		$result = $this->select($sql);
		$this->cache->set($key,$result);
		return $result;
	}
	function arrGetProductDetailsUsingFeatureid($product_feature_id="",$feature_id="",$product_id="",$startlimit="",$cnt=""){
		$keyArr[] = $this->productKey."_feature_id";
		if(!empty($product_feature_id)){
			$keyArr[] = "product_feature_id_$product_feature_id";
			$whereClauseArr[] = "PRODUCT_FEATURE.product_feature_id in ($product_feature_id)";
		}
		if(!empty($feature_id)){
			$keyArr[] = "feature_id_$feature_id";
			$whereClauseArr[] = "PRODUCT_FEATURE.feature_id in ($feature_id)";
		}
		if(!empty($product_id)){
			$keyArr[] = "product_id_$product_id";
			$whereClauseArr[] = "PRODUCT_FEATURE.product_id in ($product_id)";
		}
		if(!empty($startlimit)){
			$keyArr[] = "startlimit_$startlimit";
			$limitArr[] = $startlimit;
		}
		if(!empty($cnt)){
			$keyArr[] = "cnt_$cnt";
			$limitArr[] = $cnt;
		}
		$whereClauseArr[] = "FEATURE_MASTER.feature_id = PRODUCT_FEATURE.feature_id";
		$whereClauseArr[] = "PRODUCT_MASTER.product_id = PRODUCT_FEATURE.product_id";
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
		}
		if(sizeof($limitArr) > 0){
			$limitStr = " limit ".implode(",",$limitArr);
		}
		$key = implode("_",$keyArr);
		if($result = $this->cache->get($key)){return $result;}
		$sql = "select PRODUCT_FEATURE.*,FEATURE_MASTER.*,PRODUCT_MASTER.* from PRODUCT_MASTER,PRODUCT_FEATURE,FEATURE_MASTER $whereClauseStr order by PRODUCT_FEATURE.create_date desc $limitStr";
		$result = $this->select($sql);
		$this->cache->set($key,$result);
		return $result;
	}	
		
	/**
	 * @note function is used to insert the latest product information into the database.
	 * @param an associative array $insert_param.
	 * @pre $insert_param must be valid associative array.
	 * @post an integer $feature_id.
	 * retun integer.
	 */
	function intInsertLatestProduct($insert_param)
	{
		$insert_param['create_date'] = date('Y-m-d H:i:s');
		$sql = $this->getInsertUpdateSql("LATEST_PRODUCT",array_keys($insert_param),array_values($insert_param));
		$product_id = $this->insert($sql);
		if($product_id == 'Duplicate entry'){ return 'exists';}
		$this->cache->searchDeleteKeys($this->productKey."_latest");
		return $product_id;
	}
	/**
	 * @note function is used to update the latest product into the database.
	 * @param an associative array $update_param.
	 * @param an integer $latest_product_id.
	 * @pre $update_param must be valid associative array and $latest_product_id must be non-empty/zero valid integer.
	 * @post boolean true/false.
	 * retun boolean.
	 */
	 function boolUpdateLatestProduct($latest_product_id,$update_param)
	 {
		$insert_param['update_date'] = date('Y-m-d H:i:s');
		$sql = $this->getUpdateSql("LATEST_PRODUCT",array_keys($update_param),array_values($update_param),"latest_product_id",$latest_product_id);
		$isUpdate = $this->update($sql);
		$this->cache->searchDeleteKeys($this->productKey."_latest");
		return $isUpdate;
	 }
	/**
	 * @note function is used to delete the latest product.
	 * @param integer $latest_product_id.
	 * @pre $latest_product_id must be non-empty/zero valid integer.
	 * @post boolean true/false.
	 * return boolean.
	 */
	 function boolDeleteLatestProduct($latest_product_id)
	 {
		$sql = "delete from LATEST_PRODUCT where latest_product_id = $latest_product_id";
		$isDelete = $this->sql_delete_data($sql);
		$this->cache->searchDeleteKeys($this->productKey."_latest");
		return $isDelete;
	 }
	/**
	 * @note function is used to get latest product details.
	 * @param integer $latest_product_id.
	 * @param an integer/comma seperated product ids/ product ids array $product_ids.
	 * @param an integer category_id.
	 * @param an integer brand_id.
	 * @param boolean Active/InActive $status.
	 * @param integer $startlimit.
	 * @param integer $count.
	 * @pre not required.
	 * @post product details in associative array.
	 * retun an array.
	 */
	 function arrGetProductLatestDetails($latest_product_ids="",$product_ids="",$category_id="",$brand_id="",$status='1',$startlimit="",$count="")
	 {
		$keyArr[] = $this->productKey."_latest";
		if(is_array($latest_product_ids)){
			$latest_product_ids = implode(",",$latest_product_ids);
		}
		if(is_array($product_ids)){
			$product_ids = implode(",",$product_ids);
		}
		if(is_array($category_id)){
			$category_id = implode(",",$category_id);
		}
		if($status != '')
		{
			$keyArr[] = "status_$status";
			$whereClauseArr[] = "status=$status";
		}
		if(!empty($latest_product_ids)){
			$keyArr[] = "latest_product_id_$latest_product_ids";
			$whereClauseArr[] = "latest_product_id in ($latest_product_ids)";
		}
		if(!empty($product_ids)){
			$keyArr[] = "product_id_$product_ids";
			$whereClauseArr[] = "product_id in($product_ids)";
		}
		if(!empty($category_id)){
			$keyArr[] = "category_id_$category_id";
			$whereClauseArr[] = "category_id in ($category_id)";
		}
		if(!empty($brand_id)){
			$keyArr[] = "brand_id_$brand_id";
			$whereClauseArr[] = "brand_id in ($brand_id)";
		}
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
		}
		if(!empty($startlimit)){
			$keyArr[] = "startlimit_$startlimit";
			$limitArr[] = $startlimit;
		}
		if(!empty($count)){
			$keyArr[] = "count_$count";
			$limitArr[] = $count;
		}
		if(sizeof($limitArr) > 0){
			$limitStr = " limit ".implode(" , ",$limitArr);
		}
		$key = implode("_",$keyArr);
		if($result = $this->cache->get($key)){return $result;}
		$sql = "select * from LATEST_PRODUCT $whereClauseStr $limitStr order by product_position asc";
		$result = $this->select($sql);
		$this->cache->set($key,$result);
		return $result;
	 }

	 /**
	 * @note function is used to insert the upcoming product information into the database.
	 * @param an associative array $insert_param.
	 * @pre $insert_param must be valid associative array.
	 * @post an integer $feature_id.
	 * retun integer.
	 */
	function intInsertUpComingProduct($insert_param)
	{
		$insert_param['create_date'] = date('Y-m-d H:i:s');
		$sql = $this->getInsertUpdateSql("UPCOMING_PRODUCT",array_keys($insert_param),array_values($insert_param));
		$product_id = $this->insert($sql);
		if($product_id == 'Duplicate entry'){ return 'exists';}
		$this->cache->searchDeleteKeys($this->productKey."_upcoming");
		$this->arrGetProductUpComingDetails($product_id);
		return $product_id;
	}
	/**
	 * @note function is used to update the UpComing product into the database.
	 * @param an associative array $update_param.
	 * @param an integer $latest_product_id.
	 * @pre $update_param must be valid associative array and $latest_product_id must be non-empty/zero valid integer.
	 * @post boolean true/false.
	 * retun boolean.
	 */
	 function boolUpdateUpComingProduct($upcoming_product_id,$update_param)
	 {
		$insert_param['update_date'] = date('Y-m-d H:i:s');
		$sql = $this->getUpdateSql("UPCOMING_PRODUCT",array_keys($update_param),array_values($update_param),"upcoming_product_id",$upcoming_product_id);
		$isUpdate = $this->update($sql);
		$this->cache->searchDeleteKeys($this->productKey."_upcoming");
		$this->arrGetProductUpComingDetails($upcoming_product_id);
		return $isUpdate;
	 }
	/**
	 * @note function is used to delete the upcoming product.
	 * @param integer $latest_product_id.
	 * @pre $latest_product_id must be non-empty/zero valid integer.
	 * @post boolean true/false.
	 * return boolean.
	 */
	 function boolDeleteUpComingProduct($upcoming_product_id)
	 {
		$sql = "delete from UPCOMING_PRODUCT where upcoming_product_id = $upcoming_product_id";
		$isDelete = $this->sql_delete_data($sql);
		$this->cache->searchDeleteKeys($this->productKey."_upcoming");
		return $isDelete;
	 }
	/**
	 * @note function is used to get UpComing product details.
	 * @param integer $upcoming_product_id.
	 * @param an integer/comma seperated product ids/ product ids array $product_ids.
	 * @param an integer category_id.
	 * @param an integer brand_id.
	 * @param boolean Active/InActive $status.
	 * @param integer $startlimit.
	 * @param integer $count.
	 * @pre not required.
	 * @post product details in associative array.
	 * retun an array.
	 */
	 function arrGetProductUpComingDetails($upcoming_product_ids="",$product_ids="",$category_id="",$brand_id="",$status='1',$startlimit="",$count="")
	 {
		$keyArr[] = $this->productKey."_upcoming";
		if(is_array($upcoming_product_ids)){
			$upcoming_product_ids = implode(",",$upcoming_product_ids);
		}
		if(is_array($product_ids)){
			$product_ids = implode(",",$product_ids);
		}
		if(is_array($category_id)){
			$category_id = implode(",",$category_id);
		}
		if($status != '')
		{
			$keyArr[] = "status_$status";
			$whereClauseArr[] = "status=$status";
		}
		if(!empty($upcoming_product_ids)){
			$keyArr[] = "upcoming_product_id_$upcoming_product_ids";
			$whereClauseArr[] = "upcoming_product_id in ($upcoming_product_ids)";
		}
		if(!empty($product_ids)){
			$keyArr[] = "product_id_$product_ids";
			$whereClauseArr[] = "product_id in($product_ids)";
		}
		if(!empty($category_id)){
			$keyArr[] = "category_id_$category_id";
			$whereClauseArr[] = "category_id in ($category_id)";
		}
		if(!empty($brand_id)){
			$keyArr[] = "brand_id_$brand_id";
			$whereClauseArr[] = "brand_id in ($brand_id)";
		}
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
		}
		if(!empty($startlimit)){
			$keyArr[] = "startlimit_$startlimit";
			$limitArr[] = $startlimit;
		}
		if(!empty($count)){
			$keyArr[] = "count_$count";
			$limitArr[] = $count;
		}
		if(sizeof($limitArr) > 0){
			$limitStr = " limit ".implode(" , ",$limitArr);
		}
		$key = implode("_",$keyArr);
		if($result = $this->cache->get($key)){return $result;}
		$sql = "select * from UPCOMING_PRODUCT $whereClauseStr $limitStr order by product_position asc";
		$result = $this->select($sql);
		$this->cache->set($key,$result);
		return $result;
	 }

	 /**
	 * @note function is used to insert the featured_product product information into the database.
	 * @param an associative array $insert_param.
	 * @pre $insert_param must be valid associative array.
	 * @post an integer $feature_id.
	 * retun integer.
	 */
	function intInsertFeaturedProduct($insert_param)
	{
		$insert_param['create_date'] = date('Y-m-d H:i:s');
		$sql = $this->getInsertUpdateSql("FEATURED_PRODUCT",array_keys($insert_param),array_values($insert_param));
		$product_id = $this->insert($sql);
		if($product_id == 'Duplicate entry'){ return 'exists';}
		$this->cache->searchDeleteKeys($this->productKey."_featured");
		$this->arrGetProductFeaturedDetails($product_id);
		return $product_id;
	}
	/**
	 * @note function is used to update the featured product into the database.
	 * @param an associative array $update_param.
	 * @param an integer $latest_product_id.
	 * @pre $update_param must be valid associative array and $latest_product_id must be non-empty/zero valid integer.
	 * @post boolean true/false.
	 * retun boolean.
	 */
	 function boolUpdateFeaturedProduct($featured_product_id,$update_param)
	 {
		$insert_param['update_date'] = date('Y-m-d H:i:s');
		$sql = $this->getUpdateSql("FEATURED_PRODUCT",array_keys($update_param),array_values($update_param),"featured_product_id",$featured_product_id);
		$isUpdate = $this->update($sql);
		$this->cache->searchDeleteKeys($this->productKey."_featured");
		$this->arrGetProductFeaturedDetails($featured_product_id);
		return $isUpdate;
	 }
	/**
	 * @note function is used to delete the featured product.
	 * @param integer $latest_product_id.
	 * @pre $latest_product_id must be non-empty/zero valid integer.
	 * @post boolean true/false.
	 * return boolean.
	 */
	 function boolDeleteFeaturedProduct($featured_product_id)
	 {
		$sql = "delete from FEATURED_PRODUCT where featured_product_id = $featured_product_id";
		$isDelete = $this->sql_delete_data($sql);
		$this->cache->searchDeleteKeys($this->productKey."_featured");
		return $isDelete;
	 }
	/**
	 * @note function is used to get Featured product details.
	 * @param integer $Featured_product_id.
	 * @param an integer/comma seperated product ids/ product ids array $product_ids.
	 * @param an integer category_id.
	 * @param an integer brand_id.
	 * @param boolean Active/InActive $status.
	 * @param integer $startlimit.
	 * @param integer $count.
	 * @pre not required.
	 * @post product details in associative array.
	 * retun an array.
	 */
	 function arrGetProductFeaturedDetails($featured_product_ids="",$product_ids="",$category_id="",$brand_id="",$status='1',$startlimit="",$count="")
	 {
		$keyArr[] = $this->productKey."_featured";
		if(is_array($featured_product_ids)){
			$featured_product_ids = implode(",",$featured_product_ids);
		}
		if(is_array($product_ids)){
			$product_ids = implode(",",$product_ids);
		}
		if(is_array($category_id)){
			$category_id = implode(",",$category_id);
		}
		if($status != '')
		{
			$keyArr[] = "status_$status";
			$whereClauseArr[] = "status=$status";
		}
		if(!empty($featured_product_ids)){
			$keyArr[] = "featured_product_id_$featured_product_ids";
			$whereClauseArr[] = "featured_product_id in ($featured_product_ids)";
		}
		if(!empty($product_ids)){
			$keyArr[] = "product_id_$product_ids";
			$whereClauseArr[] = "product_id in($product_ids)";
		}
		if(!empty($category_id)){
			$keyArr[] = "category_id_$category_id";
			$whereClauseArr[] = "category_id in ($category_id)";
		}
		if(!empty($brand_id)){
			$keyArr[] = "brand_id_$brand_id";
			$whereClauseArr[] = "brand_id in ($brand_id)";
		}
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
		}
		if(!empty($startlimit)){
			$keyArr[] = "startlimit_$startlimit";
			$limitArr[] = $startlimit;
		}
		if(!empty($count)){
			$keyArr[] = "count_$count";
			$limitArr[] = $count;
		}
		if(sizeof($limitArr) > 0){
			$limitStr = " limit ".implode(" , ",$limitArr);
		}
		$key = implode("_",$keyArr);
		if($result = $this->cache->get($key)){return $result;}
		$sql = "select * from FEATURED_PRODUCT $whereClauseStr $limitStr order by product_position asc";
		$result = $this->select($sql);
		$this->cache->set($key,$result);
		return $result;
	 }
	/**
	* @note function is used to get product by name.
	* @pre not required.
	* @param a string product name $product_name.
	* @param an integer product id $product_id.
	* @param an integer variant $variant.
	* @param a boolean status $status.
	* @param is an integer value $startlimit.
	* @param is an integer value $cnt.
	*
	* @pre not required.
	*
	* @post an associative array.
	* retun an array.
	*/
	 function arrGetProductByName($product_name,$product_id,$variant="",$status="1",$startlimit="",$cnt=""){
		$keyArr[] = $this->productKey."_by_name";
		if(!empty($product_name)){
			$keyArr[] = "product_name_$product_name";
			$whereClauseArr[] = "lower(product_name) = '$product_name'";
		}
		if(!empty($product_id)){
			$whereClauseArr[] = "product_id != $product_id";
		}
		if(!empty($variant)){
			$keyArr[] = "variant_$variant";
			$whereClauseArr[] = "variant  = '$variant'";
		}
		if($status != '')
		{
			$keyArr[] = "status_$status";
			$whereClauseArr[] = "status=$status";
		}
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
		}
		 if(!empty($startlimit)){
			 $keyArr[] = "startlimit_$startlimit";
			$limitArr[] = $startlimit;
		}
		if(!empty($count)){
			$keyArr[] = "count_$count";
			$limitArr[] = $count;
		}
		if(sizeof($limitArr) > 0){
			$limitStr = " limit ".implode(" , ",$limitArr);
		}
		$key = implode("_",$keyArr);
		if($result = $this->cache->get($key)){return $result;}
		$sql = "select * from PRODUCT_MASTER $whereClauseStr $limitStr order by create_date desc";
		$result = $this->select($sql);
		$this->cache->set($key,$result);
		return $result;
	 }		
	/**
	* @note function is used to get search product details 
	*
	* @param an array $result.
	* @param an integer $category_ids.
	* @param an integer $city_id.
	*
	* @post is an associative array.
	* retun an array.
	*/ 
	 function constantProductDetails($result,$category_id,$city_id="",$price_other_param="",$variantcnt=""){
		global $searchShortDescArr;
		require_once(CLASSPATH.'price.class.php');
		require_once(CLASSPATH.'feature.class.php');
		require_once(CLASSPATH.'brand.class.php');
		$price = new price;
		$feature = new FeatureManagement;
		$brand = new BrandManagement;
		$cnt = sizeof($result);
		if(!empty($category_id)){
			$price_formula = $price->arrGetVariantFormulaDetail("","",$category_id);
			$variant_formula_id = $price_formula[0]['variant_formula_id'];
			$formula = $price_formula[0]['formula'];
		}

		for($i=0;$i<$cnt;$i++){
			$product_id = $result[$i]['product_id'];
			$categoryid = $result[$i]['category_id'];
			$brandid = $result[$i]['brand_id'];
			$product_name = $result[$i]['product_name'];
			unset($productNameArr);
			if(!empty($brandid)){
				$brand_result = $brand->arrGetBrandDetails($brandid);
				$productNameArr[] = $brand_result[0]['brand_name'];
			}
			$productNameArr[] = $result[$i]['product_name'];
			$result[$i]['link_product_name'] = implode(" ",$productNameArr);
			$productNameArr[] = $result[$i]['variant'];
			$display_product_name = implode(" ",$productNameArr);
			$result[$i]['display_product_name'] = $display_product_name;
			if(!empty($categoryid) && !empty($product_id)){
				$sOverviewArray = $feature->arrGetSummary($categoryid,$product_id,$type="array");
			}	
			if(is_array($sOverviewArray)){
				unset($productNameArr[0]);		// remove brand name form array.			
				//$aTechSpec = $sOverviewArray[implode(" ",$productNameArr)." Technical Specifications"][0];
				//$result[$i]['short_desc'] = implode(",",$aTechSpec);
				foreach($sOverviewArray as $key=>$val){
						if($sOverviewArray[$key][0]){
							$overviewArr[] = implode(",&#160;",$sOverviewArray[$key][0]);								
						}
				}
				$result[$i]['short_desc'] = implode(",&#160;",$overviewArr);
				unset($overviewArr);
			}else{
				$result[$i]['short_desc'] = "";
			}

			if(!empty($product_id)){	
				$price_result = $price->arrGetPriceDetails("",$product_id,$categoryid,"","",$city_id,"1","","","");
				$aVariant=$price->arrGetVariantDetail("",$categoryid,"1","","");
				$priceCnt = sizeof($price_result);
				//if(!empty($priceCnt) && ($priceCnt == $variantcnt)){
				if(!empty($priceCnt)){
					for($j=0;$j<$priceCnt;$j++){
						$variant_id = $price_result[$j]['variant_id'];
						$variant_value = $price_result[$j]['variant_value'];
						if(in_array(EX_SHOWROOM_STR,$price_result[$j])){
							$result[$i]['exshowroomprice'] = $variant_value ? priceFormat($variant_value) : '';
						}
						$formulaValuesArr[$variant_id] = $variant_value ? $variant_value : 0;
						$aVar[]=$variant_id;
						$result[$i]['price_details'][] = $price_result[$j];
					}
				}
				/*elseif(!empty($priceCnt) && ($priceCnt < $variantcnt)){
					if(!empty($price_other_param)){
							print_r($price_other_param);
							for($k=0;$k<count($price_other_param);$k++){
								$variant_id = $price_other_param[$k]['variant_id'];
								$variant_value = $price_other_param[$k]['variant_value'];
								if(in_array(EX_SHOWROOM_STR,$price_other_param[$k])){
									$result[$k]['exshowroomprice'] = $variant_value ? priceFormat($variant_value) : '';
								}
								$formulaValuesArr[$variant_id] = $variant_value ? $variant_value : 0;
								$aVar[]=$variant_id;
								
								$result[$i]['price_details'][] = $price_other_param[$k];
							}
					}
				}*/
				else{
					$result[$i]['exshowroomprice'] = 0;
				}
				for($k=0;$k<count($aVariant);$k++){
					if(!in_array($aVariant[$k]['variant_id'],$aVar)){
						$formulaValuesArr[$aVariant[$k]['variant_id']]=0;
					}
				}
				

				$feature_result = $this->arrGetProductFeatureDetails("","",$product_id);
				$featureCnt = sizeof($feature_result);
				//$result[$i]['feature_result']['count'] = $featureCnt;
				unset($short_desc_array);
				for($j=0;$j<$featureCnt;$j++){	
					unset($featureValueArr);		
					$feature_id = $feature_result[$j]['feature_id'];
					$feature_name = $feature_result[$j]['feature_name'];
					$feature_value = $feature_result[$j]['feature_value'];
					$featureValueArr[] = $feature_value;
					$feature_unit = $feature_result[$j]['unit_id'];
					if(!empty($feature_unit)){
						$feature_unit = $feature->arrFeatureUnitDetails($feature_unit,$categoryid);
						$unit_name = $feature_unit[0]['unit_name'];
						$featureValueArr[] = $unit_name;
					}

					
					//echo "short desc = ";print_r($searchShortDescArr);

					/*$key = array_search($feature_name,$searchShortDescArr);
					if($key){
						//print_r($featureValueArr);
						//echo "key = $key<Br/>";
						$feature_value = strtolower($feature_value) == 'yes' ? $feature_name : implode(" ",$featureValueArr);
						$short_desc_array[$key] = $feature_value;
						
					}*/
					
					$result[$i]['feature_result'][] = $feature_result[$j];
					
				}
					
				if(sizeof($formulaValuesArr) > 0){						
					$totalprice = strtr($formula,$formulaValuesArr);
					$totalprice = parse_mathematical_string($totalprice);
				}
				$onroadkey = str_replace(" ","_",ON_RAOD_PRICE_TITLE);
				$result[$i][$onroadkey] = $totalprice ? $totalprice :0;

				
				//echo "START TIME133----".date('Y-m-d m:i:s')."<br>";
				if(!empty($product_name)){
					$similar_product_result = $this->arrGetProductByName(strtolower($product_name),$product_id,"","1","0","6");
					$similarCnt = sizeof($similar_product_result);
					for($k=0;$k<$similarCnt;$k++){
						$similar_product_id = $similar_product_result[$k]['product_id'];
						$similar_brandid = $similar_product_result[$k]['brand_id'];
						$product_name = $similar_product_result[$k]['product_name'];
						unset($similarproductNameArr);
						if(!empty($similar_brandid)){
							$similar_brand_result = $brand->arrGetBrandDetails($similar_brandid);
							$similarproductNameArr[] = $similar_brand_result[0]['brand_name'];
						}
						$similarproductNameArr[] = $similar_product_result[$k]['product_name'];
						$similarproductNameArr[] = $similar_product_result[$k]['variant'];
						$similar_display_product_name = implode(" ",$similarproductNameArr);
						$similar_product_result[$k]['display_product_name'] = $similar_display_product_name;
						$similar_feature_result = $this->arrGetProductFeatureDetails("","",$similar_product_id);
						$featureCnt = sizeof($similar_feature_result);
						//$result[$i]['feature_result']['count'] = $featureCnt;
						unset($short_desc_array);

						//echo "START TIME123----".date('Y-m-d m:i:s')."<br>";
						for($j=0;$j<$featureCnt;$j++){
							//echo "START TIME124----".date('Y-m-d m:i:s')."<br>";
							unset($featureValueArr);		
							$feature_id = $similar_feature_result[$j]['feature_id'];
							$feature_name = $similar_feature_result[$j]['feature_name'];
							$feature_value = $similar_feature_result[$j]['feature_value'];
							$featureValueArr[] = $feature_value;
							$feature_unit = $similar_feature_result[$j]['unit_id'];
							if(!empty($feature_unit)){
								$similar_feature_unit = $feature->arrFeatureUnitDetails($feature_unit,$category_id);
								$unit_name = $similar_feature_unit[0]['unit_name'];
								$featureValueArr[] = $unit_name;
							}
							//echo "short desc = ";print_r($searchShortDescArr);

							/*$key = array_search($feature_name,$searchShortDescArr);
							if($key){
								//print_r($featureValueArr);
								//echo "key = $key<Br/>";
								$feature_value = strtolower($feature_value) == 'yes' ? $feature_name : implode(" ",$featureValueArr);
								$short_desc_array[$key] = $feature_value;
								
							}*/
							if(!empty($categoryid) && !empty($similar_product_id)){					
								
								$similarSetOverviewArr = $feature->arrGetSummary($categoryid,$similar_product_id,$type="array");
								
							}	
							
							if(is_array($similarSetOverviewArr)){
								unset($similarproductNameArr[0]);	// remove brand name form array.				
								//$aTechSpec = $similarSetOverviewArr[implode(" ",$similarproductNameArr)." Technical Specifications"][0];

								//$similar_product_result[$k]['short_desc'] = implode(",",$aTechSpec);

								foreach($similarSetOverviewArr as $key=>$val){
										if($similarSetOverviewArr[$key][0]){
											$overviewArr[] = implode(",&#160;",$similarSetOverviewArr[$key][0]);
										}
								}
								$similar_product_result[$k]['short_desc'] = implode(",&#160;",$overviewArr);
								unset($overviewArr);
								unset($similarSetOverviewArr);
								
							}else{
								$similar_product_result[$k]['short_desc'] = "";
							}
							//print_r($similar_product_result);
							//exit;
							//$similar_product_result[$k]['feature_result'][] = $feature_result[$j];
							
						}
						/*
						if(sizeof($short_desc_array) > 0){
							ksort($short_desc_array);
							$similar_product_result[$k]['short_desc'] = implode(" ",$short_desc_array);
						}else{
							$similar_product_result[$k]['short_desc'] = "";
						}
						*/
						$aVar='';
						$price_result = $price->arrGetPriceDetails("",$similar_product_id,$categoryid,"","",$city_id,"1","","","");
						$aVariant=$price->arrGetVariantDetail("",$categoryid,"1","","");
						$priceCnt = sizeof($price_result);
						if(!empty($priceCnt)){
							for($j=0;$j<$priceCnt;$j++){
								$variant_id = $price_result[$j]['variant_id'];
								$variant_value = $price_result[$j]['variant_value'];
								$formulaValuesArr[$variant_id] = $variant_value ? $variant_value : 0;
								$aSVar[]=$variant_id;
								}
						}
						for($l=0;$l<count($aVariant);$l++){
							if(!in_array($aVariant[$l]['variant_id'],$aSVar)){
								$formulaValuesArr[$aVariant[$l]['variant_id']]=0;
							}
						}
						$aVariant=''; $aSVar='';
						if(sizeof($formulaValuesArr) > 0){						
							$similartotalprice = strtr($formula,$formulaValuesArr);
							$similartotalprice = parse_mathematical_string($similartotalprice);
						}
						$onroadkey = str_replace(" ","_",ON_RAOD_PRICE_TITLE);
						$similar_product_result[$k][$onroadkey] = $similartotalprice ? $similartotalprice :0;
						$compare_price_difference = $totalprice - $similartotalprice;
						$similar_product_result[$k]['compare_price_difference'] = $compare_price_difference;

						//$similar_product_result[$k]['short_desc'] = implode(" ",$short_desc_array);

						$price_result = $price->arrGetPriceDetails("1",$similar_product_id,$categoryid);
						$exshowroom_price = $price_result[0]['variant_value'];
						$similar_product_result[$k]['exshowroomprice'] = $exshowroom_price ? $exshowroom_price : 0;							
						$result[$i]['similar_product'][] = $similar_product_result[$k];
					}
				}
				
			}
			//echo "START TIME13----".date('Y-m-d m:i:s')."<br>";
			
		}		
		//die();
		return $result;
	 }
	 function assignPivotToSearch(){
		require_once(CLASSPATH.'pivot.class.php');
		$pivot = new PivotManagement;

		$pivot_result = $pivot->arrGetPivotDetails("",$category_id,"","1","3");
		$pivotcnt = sizeof($pivot_result);
		for($i=0;$i<$pivotcnt;$i++){
			$this->bodyStyleArr[] = $pivot_result[$i]['feature_id'];
		}

		$pivot_result = $pivot->arrGetPivotDetails("",$category_id,"","1","1");
		$pivotcnt = sizeof($pivot_result);
		for($i=0;$i<$pivotcnt;$i++){
			$this->fuelTypeArr[] = $pivot_result[$i]['feature_id'];
		}

		$pivot_result = $pivot->arrGetPivotDetails("",$category_id,"","1","2");
		$pivotcnt = sizeof($pivot_result);
		for($i=0;$i<$pivotcnt;$i++){
			$this->impFeatureArr[] = $pivot_result[$i]['feature_id'];
		}
		
		$pivot_result = $pivot->arrGetPivotDetails("",$category_id,"","1","4");
		$pivotcnt = sizeof($pivot_result);
		for($i=0;$i<$pivotcnt;$i++){
			$this->tranmissionArr[] = $pivot_result[$i]['feature_id'];
		}
		
		$pivot_result = $pivot->arrGetPivotDetails("",$category_id,"","1","5");
		$pivotcnt = sizeof($pivot_result);
		for($i=0;$i<$pivotcnt;$i++){
			$this->seatingCapcityArr[] = $pivot_result[$i]['feature_id'];
		}
	 }
	 /**
	* @note function is used to get search product count 
	*
	* @param an integer/comma seperated category ids/ category ids array $category_ids.
	* @param an integer/comma seperated brand ids/ brand ids array $brand_ids.
	* @param is an integer/comma seperated product ids/ product ids array $product_ids.
	* @param is an integer/comma seperated feature ids/ feature ids array $feature_ids.
	* @param boolean Active/InActive $status.
	* @param string $startprice
	* @param string $endprice
	* @param is an integer $variant_id
	* @param integer $startlimit.
	* @param integer $cnt.
	*
	* @pre not required.
	*
	* @post is an integer count.
	* retun an integer.
	*/
	 function searchProductCount($category_ids="",$brand_ids="",$product_ids="",$feature_ids="",$status="1",$startprice="",$endprice="",$variant_id="1",$startlimit="",$cnt=""){

		$keyArr[] = $this->productKey."_carfinder_searchcnt";
		$this->assignPivotToSearch();

		if(is_array($category_ids)){
			$category_ids = implode(",",$category_ids);
		}
		if(is_array($brand_ids)){
			$brand_ids = implode(",",$brand_ids);
		}
		if(is_array($product_ids)){
			$product_ids = implode(",",$product_ids);
		}
		if(is_array($feature_ids)){
			$feature_ids = implode(",",$feature_ids);
		}

		if(!empty($feature_ids)){
			$featureArr = explode(",",$feature_ids);
			
			foreach($featureArr as $feature_id){
				
				if(in_array($feature_id,$this->bodyStyleArr)){
					
					$keyArr[] = "bodyStyle_$feature_id";
					$this->newBodyStyleArr[] = $feature_id;
				}elseif(in_array($feature_id,$this->fuelTypeArr)){
					$keyArr[] = "fuleType_$feature_id";
					$this->newFuelTypeArr[] = $feature_id;
				}elseif(in_array($feature_id,$this->impFeatureArr)){
					$keyArr[] = "impFeature_$feature_id";
					$this->newImpFeatureArr[] = "select product_id from PRODUCT_FEATURE where feature_id in ($feature_id)";
				}elseif(in_array($feature_id,$this->tranmissionArr)){
					$keyArr[] = "transmission_$feature_id";
					$this->newTranmissionArr[] = $feature_id;
				}elseif(in_array($feature_id,$this->seatingCapcityArr)){
					$keyArr[] = "seating_$feature_id";
					$this->newSeatingCapcityArr[] = $feature_id;
				}
			}
			
			if(sizeof($this->newBodyStyleArr) > 0){
				$sqlArr[] = "select product_id from PRODUCT_FEATURE where feature_id in (".implode(",",$this->newBodyStyleArr).")";
			}
			if(sizeof($this->newFuelTypeArr) > 0){					
				$sqlArr[] = "select product_id from PRODUCT_FEATURE where feature_id in (".implode(",",$this->newFuelTypeArr).")";
			}
			if(sizeof($this->newImpFeatureArr) > 0){
				$sqlStr = "";
				foreach($this->newImpFeatureArr as $k=>$featureSql){
					if(strlen($sqlStr) > 0){
						$sqlStr .= ' and product_id in('.$featureSql.')';
					}else{
						$sqlStr .= $featureSql;
					}
				}
				if(strlen($sqlStr) > 0){
					$whereClauseArr[] = "PRODUCT_MASTER.product_id in (".$sqlStr.")";
				}
			}
			if(sizeof($this->newTranmissionArr) > 0){
				$sqlArr[] = "select product_id from PRODUCT_FEATURE where feature_id in (".implode(",",$this->newTranmissionArr).")";
			}
			if(sizeof($this->newSeatingCapcityArr) > 0){
				$sqlArr[] = "select product_id from PRODUCT_FEATURE where feature_id in (".implode(",",$this->newSeatingCapcityArr).")";
			}
			
			if(sizeof($sqlArr) > 0){
				$sqlStr = "";
				foreach($sqlArr as $k=>$featureSql){
					if(strlen($sqlStr) > 0){
						$sqlStr .= ' and product_id in('.$featureSql.')';
					}else{
						$sqlStr .= $featureSql;
					}
				}
				if(strlen($sqlStr) > 0){
					$whereClauseArr[] = "PRODUCT_MASTER.product_id in (".$sqlStr.")";
				}
			}				
		}
		if(!empty($startprice)){
			$keyArr[] = "startprice_$startprice";
			$whereClauseArr[] = "PRICE_VARIANT_VALUES.variant_value>=$startprice";				
		}
		if(!empty($endprice)){
			$keyArr[] = "endprice_$endprice";
			$whereClauseArr[] = "PRICE_VARIANT_VALUES.variant_value<=$endprice";
		}
		if(!empty($variant_id)){
			$keyArr[] = "variant_id_$variant_id_pricestatus_1";
			$whereClauseArr[] = "PRICE_VARIANT_VALUES.variant_id = $variant_id";
			$whereClauseArr[] = "PRICE_VARIANT_VALUES.status = 1";
			$tablenameArr[] = "PRICE_VARIANT_VALUES";
		}
		if(!empty($startprice) or !empty($endprice) or !empty($variant_id)){
			$keyArr[] = "default_city_1_modelstatus_1";
			$whereClauseArr[] = "PRODUCT_MASTER.product_id = PRICE_VARIANT_VALUES.product_id";
			$whereClauseArr[] = "PRICE_VARIANT_VALUES.default_city=1";
			$tablenameArr[] = "PRODUCT_NAME_INFO";
			$whereClauseArr[] = "lower(PRODUCT_MASTER.product_name) = lower(PRODUCT_NAME_INFO.product_info_name)";
			$whereClauseArr[] = "PRODUCT_NAME_INFO.status=1";
		}
		if(!empty($category_ids)){
			$keyArr[] = "category_id_$category_ids";
			$whereClauseArr[] = "PRODUCT_MASTER.category_id in($category_ids)";
		}
		if(!empty($brand_ids)){
			$keyArr[] = "brand_id_$brand_ids";
			$whereClauseArr[] = "PRODUCT_MASTER.brand_id in($brand_ids)";
		}
		if(!empty($product_ids)){
			$keyArr[] = "product_id_$product_ids";
			$whereClauseArr[] = "PRODUCT_MASTER.product_id in($product_ids)";
		}
		$tablenameArr[] = "PRODUCT_MASTER";
		if($status != '')
		{
			$keyArr[] = "status_$status";
			$whereClauseArr[] = "PRODUCT_MASTER.status=$status";
		}
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
		}
		if(!empty($startlimit)){
			$keyArr[] = "startlimit_$startlimit";
			$limitArr[] = $startlimit;
		}
		if(!empty($count)){
			$keyArr[] = "count_$count";
			$limitArr[] = $count;
		}
		if(sizeof($limitArr) > 0){
			$limitStr = " limit ".implode(" , ",$limitArr);
		}
		$key = implode("_",$keyArr);
		if($result = $this->cache->get($key)){return $result;}

		$table_name = implode(",",$tablenameArr);

		$sql = "select count(distinct(PRODUCT_MASTER.product_name)) as cnt FROM  $table_name $whereClauseStr $limitStr";
		$result = $this->select($sql);
		$this->cache->set($key,$result);
		return $result;
	 }
	 /**
	* @note function is used to get search product details 
	*
	* @param an integer/comma seperated category ids/ category ids array $category_ids.
	* @param an integer/comma seperated brand ids/ brand ids array $brand_ids.
	* @param is an integer/comma seperated product ids/ product ids array $product_ids.
	* @param is an integer/comma seperated feature ids/ feature ids array $feature_ids.
	* @param boolean Active/InActive $status.
	* @param string $startprice
	* @param string $endprice
	* @param is an integer $variant_id
	* @param integer $startlimit.
	* @param integer $cnt.
	* @param is a string $oredrby.
	*
	* @pre not required.
	*
	* @post is an associative array.
	* retun an array.
	*/ 
	 function searchProduct($category_ids="",$brand_ids="",$product_ids="",$feature_ids="",$status="1",$startprice="",$endprice="",$variant_id="1",$startlimit="",$cnt="",$orderby="PRICE_VARIANT_VALUES.variant_value asc"){
		
		$keyArr[] = $this->productKey."_carfinder_search";
		$this->assignPivotToSearch();

		if(is_array($category_ids)){
			$category_ids = implode(",",$category_ids);
		}
		if(is_array($brand_ids)){
			$brand_ids = implode(",",$brand_ids);
		}
		if(is_array($product_ids)){
			$product_ids = implode(",",$product_ids);
		}
		if(is_array($feature_ids)){
			$feature_ids = implode(",",$feature_ids);
		}
		
		if(!empty($feature_ids)){
			$featureArr = explode(",",$feature_ids);

			foreach($featureArr as $feature_id){
				if(in_array($feature_id,$this->bodyStyleArr)){
					$keyArr[] = "bodyStyle_$feature_id";
					$this->newBodyStyleArr[] = $feature_id;
				}elseif(in_array($feature_id,$this->fuelTypeArr)){
					$keyArr[] = "fuleType_$feature_id";
					$this->newFuelTypeArr[] = $feature_id;
				}elseif(in_array($feature_id,$this->impFeatureArr)){
					$keyArr[] = "impFeature_$feature_id";
					$this->newImpFeatureArr[] = "select product_id from PRODUCT_FEATURE where feature_id in ($feature_id)";
				}elseif(in_array($feature_id,$this->tranmissionArr)){
					$keyArr[] = "transmission_$feature_id";
					$this->newTranmissionArr[] = $feature_id;
				}elseif(in_array($feature_id,$this->seatingCapcityArr)){
					$keyArr[] = "seating_$feature_id";
					$this->newSeatingCapcityArr[] = $feature_id;
				}
			}
			$this->newBodyStyleArr = array_unique($this->newBodyStyleArr,SORT_REGULAR);
			$this->newFuelTypeArr = array_unique($this->newFuelTypeArr,SORT_REGULAR);
			$this->newImpFeatureArr = array_unique($this->newImpFeatureArr,SORT_REGULAR);
			$this->newTranmissionArr = array_unique($this->newTranmissionArr,SORT_REGULAR);
			$this->newSeatingCapcityArr = array_unique($this->newSeatingCapcityArr,SORT_REGULAR);
			if(sizeof($this->newBodyStyleArr) > 0){
				$sqlArr[] = "select product_id from PRODUCT_FEATURE where feature_id in (".implode(",",$this->newBodyStyleArr).")";
			}
			if(sizeof($this->newFuelTypeArr) > 0){
				$sqlArr[] = "select product_id from PRODUCT_FEATURE where feature_id in (".implode(",",$this->newFuelTypeArr).")";
			}
			if(sizeof($this->newImpFeatureArr) > 0){
				$sqlStr = "";
				foreach($this->newImpFeatureArr as $k=>$featureSql){
					if(strlen($sqlStr) > 0){
						$sqlStr .= ' and product_id in('.$featureSql.')';
					}else{
						$sqlStr .= $featureSql;
					}
				}
				if(strlen($sqlStr) > 0){
					$whereClauseArr[] = "PRODUCT_MASTER.product_id in (".$sqlStr.")";
				}
			}
			if(sizeof($this->newTranmissionArr) > 0){
				$sqlArr[] = "select product_id from PRODUCT_FEATURE where feature_id in (".implode(",",$this->newTranmissionArr).")";
			}
			if(sizeof($this->newSeatingCapcityArr) > 0){
				$sqlArr[] = "select product_id from PRODUCT_FEATURE where feature_id in (".implode(",",$this->newSeatingCapcityArr).")";
			}
			
			if(sizeof($sqlArr) > 0){
				$sqlStr = "";
				foreach($sqlArr as $k=>$featureSql){
					if(strlen($sqlStr) > 0){
						$sqlStr .= ' and product_id in('.$featureSql.')';
					}else{
						$sqlStr .= $featureSql;
					}
				}
				if(strlen($sqlStr) > 0){
					$whereClauseArr[] = "PRODUCT_MASTER.product_id in (".$sqlStr.")";
				}
			}				
		}
		if(!empty($startprice)){
			$keyArr[] = "startprice_$startprice";
			$whereClauseArr[] = "PRICE_VARIANT_VALUES.variant_value>=$startprice";				
		}
		if(!empty($endprice)){
			$keyArr[] = "endprice_$endprice";
			$whereClauseArr[] = "PRICE_VARIANT_VALUES.variant_value<=$endprice";
		}
		if(!empty($variant_id)){
			$keyArr[] = "variant_id_$variant_id_pricestatus_1";
			$whereClauseArr[] = "PRICE_VARIANT_VALUES.variant_id = $variant_id";
			$whereClauseArr[] = "PRICE_VARIANT_VALUES.status = 1";
			$tablenameArr[] = "PRICE_VARIANT_VALUES";
		}
		if(!empty($startprice) or !empty($endprice) or !empty($variant_id)){
			$keyArr[] = "default_city_1_modelstatus_1";
			$whereClauseArr[] = "PRODUCT_MASTER.product_id = PRICE_VARIANT_VALUES.product_id";
			$whereClauseArr[] = "PRICE_VARIANT_VALUES.default_city=1";
			$tablenameArr[] = "PRODUCT_NAME_INFO";
			$whereClauseArr[] = "lower(PRODUCT_MASTER.product_name) = lower(PRODUCT_NAME_INFO.product_info_name)";
			$whereClauseArr[] = "PRODUCT_NAME_INFO.status=1";
		}
		if(!empty($category_ids)){
			$keyArr[] = "category_id_$category_ids";
			$whereClauseArr[] = "PRODUCT_MASTER.category_id in($category_ids)";
		}
		if(!empty($brand_ids)){
			$keyArr[] = "brand_id_$brand_ids";
			$whereClauseArr[] = "PRODUCT_MASTER.brand_id in($brand_ids)";
		}
		if(!empty($product_ids)){
			$keyArr[] = "product_id_$product_ids";
			$whereClauseArr[] = "PRODUCT_MASTER.product_id in($product_ids)";
		}
		$tablenameArr[] = "PRODUCT_MASTER";
		if($status != '')
		{
			$keyArr[] = "status_$status";
			$whereClauseArr[] = "PRODUCT_MASTER.status=$status";
		}
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
		}
		if(!empty($startlimit)){
			$keyArr[] = "startlimit_$startlimit";
			$limitArr[] = $startlimit;
		}
		if(!empty($cnt)){
			$keyArr[] = "cnt_$cnt";
			$limitArr[] = $cnt;
		}
		if(sizeof($limitArr) > 0){
			$limitStr = " limit ".implode(" , ",$limitArr);
		}

		$key = implode("_",$keyArr);
		$result = $this->cache->get($key);
		if(sizeof($result) <= 0){
			$table_name = implode(",",$tablenameArr);
			$sql = "select PRODUCT_MASTER.product_name as product_info FROM $table_name $whereClauseStr order by $orderby";
			//echo "<br/>SQL:",$sql;
			$result = $this->select($sql);
			$this->cache->set($key,$result);
		}
		
		//echo "$result,$startlimit,$cnt";
		$searchArr = $this->arrGetUniqueSearchModel($result,$startlimit,$cnt);

		if(strpos($orderby,"product_name")){
			$orderby="PRICE_VARIANT_VALUES.variant_value asc";
		}
		$result = $this->globalSearchSummary($searchArr,$category_ids,$startprice,$endprice,$variant_id,$brand_ids,$product_ids,$feature_ids,$orderby);

		return $result;
	 }
	 /**
	* @note function is used to get global search summary
	*
	* @param is an array $result 
	* @param an integer $category_ids.
	* @param string $startprice
	* @param string $endprice
	* @param is an integer $variant_id
	* @param an integer/comma seperated brand ids/ brand ids array $brand_ids.
	* @param is an integer/comma seperated product ids/ product ids array $product_ids.
	* @param is an integer/comma seperated feature ids/ feature ids array $feature_ids.
	* @param is a string $oredrby.
	*
	* @pre not required.
	*
	* @post is an associative array.
	* retun an array.
	*/ 
	 function globalSearchSummary($result,$category_id,$startprice,$endprice,$variant_id,$brand_ids="",$product_ids="",$feature_ids="",$orderby="PRICE_VARIANT_VALUES.variant_value asc"){
		
		require_once(CLASSPATH.'feature.class.php');
		require_once(CLASSPATH.'overview.class.php');

		$feature = new FeatureManagement;
		$overview = new OverviewManagement;
		$overviewresult = $overview->arrGetCarFinderFeatureOverview();
		$overviewCnt = sizeof($overviewresult);

		$cnt = sizeof($result);
		
		for($i=0;$i<$cnt;$i++){
			$product_info = $result[$i]['product_info'];
			
			$product_result = $this->globalResearchPriceProductDetails($product_info,$category_id,$startprice,$endprice,$variant_id,$brand_ids,$product_ids,$feature_ids,$orderby);
			$productcnt = sizeof($product_result);
			
			for($j=0;$j<$productcnt;$j++){
				
				$product_id = $product_result[$j]['product_id'];
				$categoryid = $product_result[$j]['categoryid'];
				if(!empty($category_id) && !empty($product_id)){
					unset($overviewArr);
					
					for($overview=0;$overview<$overviewCnt;$overview++){
						unset($featureoverviewArr);unset($productfeaturekey);
						$overview_feature_id = $overviewresult[$overview]['feature_id'];
						$overview_title = $overviewresult[$overview]['title'];
						$overview_unit = $overviewresult[$overview]['abbreviation'];
						$productfeaturekey = $this->productKey."_feature_overview_$overview_feature_id"."_product_id".$product_id;
						$overviewfeature_details = $this->cache->get($productfeaturekey);
						if(sizeof($overviewfeature_details) <= 0){
							$sql = "select * from PRODUCT_FEATURE where feature_id = $overview_feature_id and product_id = $product_id";
							$overviewfeature_details = $this->select($sql);
							$this->cache->set($productfeaturekey,$overviewfeature_details);
						}
						$feature_value = $overviewfeature_details[0]['feature_value'];
						if($feature_value == "-"){$feature_value="";}
						if(!empty($feature_value)){
							$featureoverviewArr[] = $feature_value;
						}
						if(!empty($overview_unit) && !empty($feature_value)){
							$featureoverviewArr[] = $overview_unit;
						}
						if(sizeof($featureoverviewArr) > 0){
							$desc = implode(" ",$featureoverviewArr);							
							if(!empty($desc) && !empty($overview_title)){
								$desc = "<span class=\"b\">$overview_title:&#160;</span>$desc";
								$overviewArr[] = $desc;
							}elseif(!empty($desc) && empty($overview_title)){
								$overviewArr[] = $desc;
							}
						}
						
					}						
					$product_result[$j]['short_desc'] = implode('<span class="dvder">|</span>',$overviewArr);
					unset($overviewArr);
				}
				
				/*
				if(!empty($category_id) && !empty($product_id)){
					
					$sOverviewArray = $feature->arrGetSummary($category_id,$product_id,$type="array");
				}
				if(is_array($sOverviewArray)){
					unset($productNameArr[0]);		// remove brand name form array.
					foreach($sOverviewArray as $key=>$val){
						if($sOverviewArray[$key][0]){
							$overviewArr[] = implode(",&#160;",$sOverviewArray[$key][0]);								
						}
					}
					$product_result[$j]['short_desc'] = implode(",&#160;",$overviewArr);
					unset($overviewArr);
				}else{
					$product_result[$j]['short_desc'] = "";
				}
				*/
			}
			$result[$i] = $product_result;
			
		}	
		//print_r($result);exit;
		return $result;
	 }
	 /**
	* @note function is used to get global research price product details
	*
	* @pre not required.
	*
	* @param string $product_info
	* @param an integer/comma seperated category ids/ category ids array $category_ids.
	* @param string $startprice
	* @param string $endprice
	* @param is an integer $variant_id
	* @param an integer/comma seperated brand ids/ brand ids array $brand_ids.
	* @param is an integer/comma seperated product ids/ product ids array $product_ids.
	* @param is an integer/comma seperated feature ids/ feature ids array $feature_ids.
	* @param is a string $oredrby.
	*
	* @post is an associative array.
	* retun an array.
	*/

	 function globalResearchPriceProductDetails($product_info,$category_id,$startprice,$endprice,$variant_id,$brand_ids="",$product_ids="",$feature_ids="",$orderby="PRICE_VARIANT_VALUES.variant_value asc",$default_city="1"){
	
		$keyArr[] = $this->productKey."_carfinder_global_search";
		unset($tablenameArr);
		$this->assignPivotToSearch();

		if(is_array($category_ids)){
			$category_ids = implode(",",$category_ids);
		}
		if(is_array($brand_ids)){
			$brand_ids = implode(",",$brand_ids);
		}
		if(is_array($product_ids)){
			$product_ids = implode(",",$product_ids);
		}
		if(is_array($feature_ids)){
			$feature_ids = implode(",",$feature_ids);
		}
		
		if(!empty($feature_ids)){
			$featureArr = explode(",",$feature_ids);
			foreach($featureArr as $feature_id){
				if(in_array($feature_id,$this->bodyStyleArr)){
					$keyArr[] = "bodyStyle_$feature_id";
					$this->newBodyStyleArr[] = $feature_id;
				}elseif(in_array($feature_id,$this->fuelTypeArr)){
					$keyArr[] = "fuleType_$feature_id";
					$this->newFuelTypeArr[] = $feature_id;
				}elseif(in_array($feature_id,$this->impFeatureArr)){
					$keyArr[] = "impFeature_$feature_id";
					$this->newImpFeatureArr[] = "select product_id from PRODUCT_FEATURE where feature_id in ($feature_id)";
				}elseif(in_array($feature_id,$this->tranmissionArr)){
					$keyArr[] = "transmission_$feature_id";
					$this->newTranmissionArr[] = $feature_id;
				}elseif(in_array($feature_id,$this->seatingCapcityArr)){
					$keyArr[] = "seating_$feature_id";
					$this->newSeatingCapcityArr[] = $feature_id;
				}
			}
			if(sizeof($this->newBodyStyleArr) > 0){
				$sqlArr[] = "select product_id from PRODUCT_FEATURE where feature_id in (".implode(",",$this->newBodyStyleArr).")";
			}
			if(sizeof($this->newFuelTypeArr) > 0){
				$sqlArr[] = "select product_id from PRODUCT_FEATURE where feature_id in (".implode(",",$this->newFuelTypeArr).")";
			}
			if(sizeof($this->newImpFeatureArr) > 0){
				$sqlStr = "";
				foreach($this->newImpFeatureArr as $k=>$featureSql){
					if(strlen($sqlStr) > 0){
						$sqlStr .= ' and product_id in('.$featureSql.')';
					}else{
						$sqlStr .= $featureSql;
					}
				}
				if(strlen($sqlStr) > 0){
					$whereClauseArr[] = "PRODUCT_MASTER.product_id in (".$sqlStr.")";
				}
				//$sqlArr[] = "select product_id from PRODUCT_FEATURE where feature_id in (".implode(",",$this->newImpFeatureArr).")";
			}
			if(sizeof($this->newTranmissionArr) > 0){
				$sqlArr[] = "select product_id from PRODUCT_FEATURE where feature_id in (".implode(",",$this->newTranmissionArr).")";
			}
			if(sizeof($this->newSeatingCapcityArr) > 0){
				$sqlArr[] = "select product_id from PRODUCT_FEATURE where feature_id in (".implode(",",$this->newSeatingCapcityArr).")";
			}
			
			if(sizeof($sqlArr) > 0){
				$sqlStr = "";
				foreach($sqlArr as $k=>$featureSql){
					if(strlen($sqlStr) > 0){
						$sqlStr .= ' and product_id in('.$featureSql.')';
					}else{
						$sqlStr .= $featureSql;
					}
				}
				if(strlen($sqlStr) > 0){
					$whereClauseArr[] = "PRODUCT_MASTER.product_id in (".$sqlStr.")";
				}
			}				
		}
		if(!empty($product_info)){	
			 $keyArr[] = "model_name_$product_info";
			 $whereClauseArr[] = "lower(PRODUCT_MASTER.product_name) = '".trim($product_info)."'";
			 $whereClauseArr[] = "PRODUCT_MASTER.status=1";
		}
		if(!empty($startprice)){
			$keyArr[] = "startprice_$startprice";
			$whereClauseArr[] = "PRICE_VARIANT_VALUES.variant_value>=$startprice";				
		}
		if(!empty($endprice)){
			$keyArr[] = "endprice_$endprice";
			$whereClauseArr[] = "PRICE_VARIANT_VALUES.variant_value<=$endprice";
		}
		if(!empty($variant_id)){
			$keyArr[] = "variant_id_$variant_id_pricestatus_1";
			$whereClauseArr[] = "PRICE_VARIANT_VALUES.variant_id = $variant_id";
			$whereClauseArr[] = "PRICE_VARIANT_VALUES.status = 1";
			$tablenameArr[] = "PRICE_VARIANT_VALUES";
		}
		if(!empty($startprice) or !empty($endprice) or !empty($variant_id)){
			$keyArr[] = "default_city_1_modelstatus_1";
			$whereClauseArr[] = "PRODUCT_MASTER.product_id = PRICE_VARIANT_VALUES.product_id";
			$whereClauseArr[] = "PRICE_VARIANT_VALUES.default_city = $default_city";
		}
		if(!empty($category_ids)){
			$keyArr[] = "category_id_$category_ids";
			$whereClauseArr[] = "PRODUCT_MASTER.category_id in($category_ids)";
		}
		if(!empty($brand_ids)){
			$keyArr[] = "brand_id_$brand_ids";
			$whereClauseArr[] = "PRODUCT_MASTER.brand_id in($brand_ids)";
		}
		if(!empty($product_ids)){
			$keyArr[] = "product_id_$product_ids";
			$whereClauseArr[] = "PRODUCT_MASTER.product_id in($product_ids)";
		}
		$tablenameArr[] = "PRODUCT_MASTER";
		if($status != '')
		{
			$keyArr[] = "status_$status";
			$whereClauseArr[] = "PRODUCT_MASTER.status=$status";
		}
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
		}
		if(!empty($startlimit)){
			$keyArr[] = "startlimit_$startlimit";
			$limitArr[] = $startlimit;
		}
		if(!empty($count)){
			$keyArr[] = "count_$count";
			$limitArr[] = $count;
		}
		if(sizeof($limitArr) > 0){
			$limitStr = " limit ".implode(" , ",$limitArr);
		}
		$key = implode("_",$keyArr);
		if($result = $this->cache->get($key)){return $result;}
		$table_name = implode(",",$tablenameArr);
		$sql = "select PRODUCT_MASTER.*,PRICE_VARIANT_VALUES.* FROM $table_name $whereClauseStr  order by $orderby";		
		$result = $this->select($sql);
		$this->cache->set($key,$result);
		return $result;
	 }
	/**
	* @note function is used to get  features details.
	*
	* @param $category_id
	*
	* @post is an associative array.
	* retun an array.
	*/
	function arrGetFeaturesDetails($category_id){
		$key = $this->productKey."_feature_details_$category_id";
		if($result = $this->cache->get($key)){return $result;}
		$sSql="SELECT MFG.main_group_name, FSG.sub_group_name, FM.feature_name, MFG.*,FSG.*,FM.* FROM `FEATURE_MASTER` FM, MAIN_FEATURE_GROUP MFG, FEATURE_SUB_GROUP FSG WHERE FM.main_feature_group = MFG.group_id AND FM.feature_group = FSG.sub_group_id AND FM.category_id =1 ORDER BY MFG.main_group_name, FSG.sub_group_name ASC ";
		$result = $this->select($sSql);
		$this->cache->set($key,$result);
		return $result;
	}
	/**
	* @note function is used to get  features product data.
	*
	* @param $category_id
	* @param $brand_ids
	* @param is an integer product_ids $product_ids
	* @param $feature_ids
	* @param boolean Active/InActive $status.
	* @param integer $startlimit.
	* @param integer $cnt.
	*
	* @post is an associative array.
	* retun an array.
	*/ 
	function arrGetFeaturesProductData($category_id,$brand_ids,$product_ids,$feature_ids,$status,$startlimit,$cnt){
		$key = $this->productKey."_feature_product_$product_ids";
		if($result = $this->cache->get($key)){return $result;}
		$sql = "SELECT * FROM `PRODUCT_FEATURE` PF,`FEATURE_MASTER` FM WHERE PF.product_id =$product_ids and PF.product_id=FM.product_id";
		$result = $this->select($sql);
		$this->cache->set($key,$result);
		return $result;
	}
		
	/**
	* @note function is used to get  product name info.
	* @param an integer/comma seperated product name ids/ product name ids array $product_name_ids.
	* @param an integer/comma seperated category_ids  $category_id.
	* @param an integer/comma seperated brand_ids  $brand_id.
	* @param string product_info_name $product_info_name
	* @param boolean Active/InActive $status.
	* @param integer $startlimit.
	* @param integer $cnt.
	* @pre not required.
	* @post is an associative array.
	* retun an array.
	*/ 
	function arrGetProductNameInfo($product_name_ids="",$category_id="",$brand_id="",$product_info_name="",$status="1",$startlimit="",$cnt=""){
		$keyArr[] = $this->productKey."_model";
		if(is_array($product_name_ids)){
			$product_name_ids = implode(",",$product_name_ids);
		}	
		if($category_id != ""){
			$keyArr[] = "category_id_$category_id";
			$whereClauseArr[] = "category_id in($category_id)";
		}
		if($brand_id != ""){
			$keyArr[] = "brand_id_$brand_id";
			$whereClauseArr[] = "brand_id in($brand_id)";
		}
		if(!empty($product_name_ids)){
			$keyArr[] = "product_name_id_$product_name_ids";
			$whereClauseArr[] = "product_name_id in($product_name_ids)";
		}
		if(!empty($product_info_name)){
			$keyArr[] = "product_info_name_$product_info_name";
			$product_info_name = strtolower($product_info_name);
			$whereClauseArr[] = "lower(product_info_name) = '$product_info_name'";
		}
		if($status != ""){
			$keyArr[] = "status_$status";
			$whereClauseArr[] = "status = $status";
		}
		if(!empty($startlimit)){
			$keyArr[] = "startlimit_$startlimit";
			$limitArr[] = $startlimit;
		}
		if(!empty($cnt)){
			$keyArr[] = "cnt_$cnt";
			$limitArr[] = $cnt;
		}

		if(sizeof($limitArr) > 0){
			$limitStr = " limit ".implode(" , ",$limitArr);
		}
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
		}
		$key = implode("_",$keyArr);
		if($result = $this->cache->get($key)){return $result;}
		$sSql = "select * from PRODUCT_NAME_INFO $whereClauseStr order by product_info_name asc $limitStr";
		//echo $sSql;
		$result = $this->select($sSql);
		$this->cache->set($key,$result);
		return $result;
	}

	/**
	* @note function is used to insert the product name information into the database.
	* @param an associative array $insert_param.
	* @pre $insert_param must be valid associative array.
	* @post an integer $product_name_id.
	* retun integer.
	*/	
	function intInsertProductNameInfo($insert_param){
		$insert_param['create_date'] = date('Y-m-d H:i:s');
		$sql = $this->getInsertUpdateSql("PRODUCT_NAME_INFO",array_keys($insert_param),array_values($insert_param));
		$product_name_id = $this->insert($sql);
		if($product_name_id == 'Duplicate entry'){ return 'exists';}
		$this->cache->searchDeleteKeys($this->productKey."_model");
		$this->arrGetProductNameInfo($product_name_id);
		return $product_name_id;
	}
	/**
	* @note function is used to update product name information in the database.
	* @param an integer $product_name_id.
	* @param an associative array $update_param.
	* @pre $update_param must be valid associative array.
	* @post an integer $product_name_id.
	* retun integer.
	*/
	function boolUpdateProductNameInfo($product_name_id,$update_param){
		$update_param['create_date'] = date('Y-m-d H:i:s');
		$sql = $this->getUpdateSql("PRODUCT_NAME_INFO",array_keys($update_param),array_values($update_param),"product_name_id",$product_name_id);
		$isUpdate = $this->update($sql);
		$this->cache->searchDeleteKeys($this->productKey."_model");
		$this->arrGetProductNameInfo($product_name_id);
		return $isUpdate;
	}

	/**
	* @note function is used to insert the Top Competitor information into the database.
	* @param an associative array $insert_param.
	* @pre $insert_param must be valid associative array.
	* @post an integer $competitor_product_id.
	* retun integer.
	*/
	function addUpdTopCompetitorDetails($insert_param)
	{
		$insert_param['create_date'] = date('Y-m-d H:i:s');
		$sql = $this->getInsertUpdateSql("TOP_COMPETITOR",array_keys($insert_param),array_values($insert_param));
		$competitor_product_id = $this->insertUpdate($sql);
		$this->cache->searchDeleteKeys($this->productKey."_top_cometitor");
		return $competitor_product_id;
	}

	/**
	* @note function is used to delete the TopCompetitor Detail.
	* @param integer $competitor_product_id.
	* @pre $competitor_product_id must be non-empty/zero valid integer.
	* @post boolean true/false.
	* return boolean.
	*/
	function boolDeleteTopCompetitorDetail($competitor_product_id)
	{
		$sql = "delete from TOP_COMPETITOR where competitor_product_id = $competitor_product_id";
		$isDelete = $this->sql_delete_data($sql);
		$this->cache->searchDeleteKeys($this->productKey."_top_cometitor");
		return $isDelete;
	}

	/**
	* @note function is used to get  product details.
	* @param an integer/comma seperated product ids/ product ids array $product_ids.
	* @param an integer category_id.
	* @param an integer brand_id.
	* @param boolean Active/InActive $status.
	* @param integer $startlimit.
	* @param integer $count.
	* @pre not required.
	* @post product details in associative array.
	* retun an array.
	*/	
	function arrGetProductCompetitorDetails($top_competitor_ids="",$product_ids="",$brand_ids="",$category_ids="",$status="1",$startlimit="",$cnt=""){
		$keyArr[] = $this->productKey."_top_cometitor";
		if(is_array($top_competitor_ids)){
			$top_competitor_ids = implode(",",$top_competitor_ids);
		}
		if(is_array($category_ids)){
			$category_ids = implode(",",$category_ids);
		}
		if(is_array($brand_ids)){
			$brand_ids = implode(",",$brand_ids);
		}
		if(is_array($product_ids)){
			$product_ids = implode(",",$product_ids);
		}
		if($top_competitor_ids != ""){
			$keyArr[] = "top_competitor_id_$top_competitor_ids";
			$whereClauseArr[] = " competitor_product_id in($top_competitor_ids)";
		}
		if($category_ids != ""){
			$keyArr[] = "category_id_$category_ids";
			$whereClauseArr[] = "category_id in($category_ids)";
		}
		if($brand_ids != ""){
			$keyArr[] = "brand_id_$brand_ids";
			$whereClauseArr[] = "brand_id in($brand_ids)";
		}
		if($product_ids != ""){
			$keyArr[] = "product_id_$product_ids";
			$whereClauseArr[] = "product_id in($product_ids)";
		}
		if($status != ""){
			$keyArr[] = "status_$status";
			$whereClauseArr[] = "status = $status";
		}
		if(!empty($startlimit)){
			$keyArr[] = "startlimit_$startlimit";
			$limitArr[] = $startlimit;
		}
		if(!empty($cnt)){
			$keyArr[] = "cnt_$cnt";
			$limitArr[] = $cnt;
		}
		if(sizeof($limitArr) > 0){
			$limitStr = " limit ".implode(" , ",$limitArr);
		}
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
		}
		$key = implode("_",$keyArr);
		if($result = $this->cache->get($key)){return $result;}
		$sSql = "select * from TOP_COMPETITOR $whereClauseStr $limitStr";
		$result = $this->select($sSql);
		$this->cache->set($key,$result);
		return $result;
	}
	/**
	* @note function is used to get Product Competitor Details.
	* @param an integer/comma seperated top competitor ids/ top competitor ids array $top_competitor_ids.
	* @param an integer/comma seperated product ids/ product ids array $product_ids.
	* @param an integer/comma seperated product info ids/ product info ids array $product_info_ids.
	* @param an integer/comma seperated brand ids/ brand ids array $brand_ids.
	* @param an integer/comma seperated category ids/ category ids array $category_ids.
	* @param boolean Active/InActive $status.
	* @param integer $startlimit.
	* @param integer $cnt.
	* @pre not required.
	* @post result is in associative array.
	* retun an array.
	*/
	function arrGetProdCompetitorDetails($top_competitor_ids="",$product_ids="",$product_info_ids="",$brand_ids="",$category_ids="",$status="1",$startlimit="",$cnt=""){
		$keyArr[] = $this->productKey."_top_cometitor_bymodel";
		if(is_array($top_competitor_ids)){
			$top_competitor_ids = implode(",",$top_competitor_ids);
		}
		if(is_array($category_ids)){
			$category_ids = implode(",",$category_ids);
		}
		if(is_array($brand_ids)){
			$brand_ids = implode(",",$brand_ids);
		}
		if(is_array($product_info_ids)){
			$product_info_ids = implode(",",$product_info_ids);
		}
		if(is_array($product_ids)){
			$product_ids = implode(",",$product_ids);
		}
		if($top_competitor_ids != ""){
			$keyArr[] = "top_competitor_id_$top_competitor_ids";
			$whereClauseArr[] = " competitor_product_id in($top_competitor_ids)";
		}
		if($category_ids != ""){
			$keyArr[] = "category_id_$category_ids";
			$whereClauseArr[] = "category_id in($category_ids)";
		}
		if($brand_ids != ""){
			$keyArr[] = "brand_id_$brand_ids";
			$whereClauseArr[] = "brand_id in($brand_ids)";
		}
		if($product_info_ids != ""){
			$keyArr[] = "product_info_id_$product_info_ids";
			$whereClauseArr[] = "product_info_id in($product_info_ids)";
		}
		if($product_ids != ""){
			$keyArr[] = "product_id_$product_ids";
			$whereClauseArr[] = "product_id in($product_ids)";
		}
		if($status != ""){
			$keyArr[] = "status_$status";
			$whereClauseArr[] = "status = $status";
		}
		if(!empty($startlimit)){
			$keyArr[] = "startlimit_$startlimit";
			$limitArr[] = $startlimit;
		}
		if(!empty($cnt)){
			$keyArr[] = "cnt_$cnt";
			$limitArr[] = $cnt;
		}
		if(sizeof($limitArr) > 0){
			$limitStr = " limit ".implode(" , ",$limitArr);
		}
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
		}
		$key = implode("_",$keyArr);
		if($result = $this->cache->get($key)){return $result;}
		$sSql="select * from TOP_COMPETITOR $whereClauseStr $limitStr";
		$result=$this->select($sSql);
		$this->cache->set($key,$result);
		return $result;
	}
		 
	/**
	* @note function is used to insert the Top Competitor information into the database.
	* @param an associative array $insert_param.
	* @pre $insert_param must be valid associative array.
	* @post an integer $competitor_product_id.
	* retun integer.
	*/
	function addUpdCompareTopCompetitorDetails($insert_param)
	{
		$insert_param['create_date'] = date('Y-m-d H:i:s');
		$sql = $this->getInsertUpdateSql("COMPARE_TOP_COMPETITOR",array_keys($insert_param),array_values($insert_param));
		$competitor_product_id = $this->insertUpdate($sql);
		$this->cache->searchDeleteKeys($this->productKey."_compare_top_cometitor");
		return $competitor_product_id;
	}

	/**
	* @note function is used to delete the TopCompetitor Detail.
	* @param integer $competitor_product_id.
	* @pre $competitor_product_id must be non-empty/zero valid integer.
	* @post boolean true/false.
	* return boolean.
	*/
	function boolDeleteCompareTopCompetitorDetail($competitor_product_id)
	{
		$sql = "delete from COMPARE_TOP_COMPETITOR where competitor_product_id = $competitor_product_id";
		$isDelete = $this->sql_delete_data($sql);
		$this->cache->searchDeleteKeys($this->productKey."_compare_top_cometitor");
		return $isDelete;
	}

	/**
	* @note function is used to get  product details.
	* @param an integer/comma seperated product ids/ product ids array $product_ids.
	* @param an integer category_id.
	* @param an integer brand_id.
	* @param boolean Active/InActive $status.
	* @param integer $startlimit.
	* @param integer $count.
	* @pre not required.
	* @post product details in associative array.
	* retun an array.
	*/	
	function arrGetProductCompareCompetitorDetails($top_competitor_ids="",$product_ids="",$brand_ids="",$category_ids="",$status="1",$startlimit="",$cnt=""){
		$keyArr[] = $this->productKey."_compare_top_cometitor";
		if(is_array($top_competitor_ids)){
			$top_competitor_ids = implode(",",$top_competitor_ids);
		}
		if(is_array($category_ids)){
			$category_ids = implode(",",$category_ids);
		}
		if(is_array($brand_ids)){
			$brand_ids = implode(",",$brand_ids);
		}
		if(is_array($product_ids)){
			$product_ids = implode(",",$product_ids);
		}
		if($top_competitor_ids != ""){
			$keyArr[] = "top_competitor_id_$top_competitor_ids";
			$whereClauseArr[] = " competitor_product_id in($top_competitor_ids)";
		}
		if($category_ids != ""){
			$keyArr[] = "category_id_$category_ids";
			$whereClauseArr[] = "category_id in($category_ids)";
		}
		if($brand_ids != ""){
			$keyArr[] = "brand_id_$brand_ids";
			$whereClauseArr[] = "brand_id in($brand_ids)";
		}
		if($product_ids != ""){
			$keyArr[] = "product_id_$product_ids";
			$whereClauseArr[] = "product_id in($product_ids)";
		}
		if(!empty($startlimit)){
			$keyArr[] = "startlimit_$startlimit";
			$limitArr[] = $startlimit;
		}
		if(!empty($count)){
			$keyArr[] = "count_$count";
			$limitArr[] = $count;
		}
		if(sizeof($limitArr) > 0){
			$limitStr = " limit ".implode(" , ",$limitArr);
		}
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
		}
		$key = implode("_",$keyArr);
		if($result = $this->cache->get($key)){return $result;}
		$sSql="select * from COMPARE_TOP_COMPETITOR $whereClauseStr $limitStr";
		$this->cache->set($key,$result);
		return $result;
	}

		 
	/**
	* @note function is used to insert the Top Competitor information into the database.
	* @param an associative array $insert_param.
	* @pre $insert_param must be valid associative array.
	* @post an integer $compare_id.
	* retun integer.
	*/
	function addUpdMostPopularSetDetails($insert_param)
	{
		$insert_param['create_date'] = date('Y-m-d H:i:s');
		$sql = $this->getInsertUpdateSql("MOST_POPULAR_COMPARE_SET_MASTER",array_keys($insert_param),array_values($insert_param));
		$compare_id = $this->insertUpdate($sql);
		$this->cache->searchDeleteKeys($this->productKey."_popular_compare_set");
		return $compare_id;
	}

	/**
	* @note function is used to delete the TopCompetitor Detail.
	* @param integer $compare_id.
	* @pre $compare_id must be non-empty/zero valid integer.
	* @post boolean true/false.
	* return boolean.
	*/
	function boolDeleteMostPopularSetDetail($compare_id)
	{
		$sql = "delete from MOST_POPULAR_COMPARE_SET_MASTER where compare_id = $compare_id";
		$isDelete = $this->sql_delete_data($sql);
		$this->cache->searchDeleteKeys($this->productKey."_popular_compare_set");
		return $isDelete;
	}

	/**
	* @note function is used to get  most popular set details.
	* @param an integer/comma seperated product ids/ product ids array $product_ids.
	* @param an integer category_id.
	* @param an integer brand_id.
	* @param boolean Active/InActive $status.
	* @param integer $startlimit.
	* @param integer $count.
	* @pre not required.
	* @post product details in associative array.
	* retun an array.
	*/	
	function arrGetMostPopularSetDetails($compare_ids="",$product_ids="",$brand_ids="",$category_ids="",$status="1",$startlimit="",$cnt=""){
		$keyArr[] = $this->productKey."_popular_compare_set";
		if(is_array($compare_ids)){
			$compare_ids = implode(",",$compare_ids);
		}
		if(is_array($category_ids)){
			$category_ids = implode(",",$category_ids);
		}
		if(is_array($brand_ids)){
			$brand_ids = implode(",",$brand_ids);
		}
		if(is_array($product_ids)){
			$product_ids = implode(",",$product_ids);
		}
		if($compare_ids != ""){
			$keyArr[] = "compare_id_$compare_ids";
			$whereClauseArr[] = " compare_id in($compare_ids)";
		}
		if($category_ids != ""){
			$keyArr[] = "category_id_$category_ids";
			$whereClauseArr[] = "category_id in($category_ids)";
		}
		if($brand_ids != ""){
			$keyArr[] = "brand_id_$brand_ids";
			$whereClauseArr[] = "brand_id in($brand_ids)";
		}
		if($product_ids != ""){
			$keyArr[] = "product_id_$product_ids";
			$whereClauseArr[] = "compare_set in($product_ids)";
		}
		if(!empty($startlimit)){
			$keyArr[] = "startlimit_$startlimit";
			$limitArr[] = $startlimit;
		}
		if(!empty($count)){
			$keyArr[] = "count_$count";
			$limitArr[] = $count;
		}
		if(sizeof($limitArr) > 0){
			$limitStr = " limit ".implode(" , ",$limitArr);
		}
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
		}
		$key = implode("_",$keyArr);
		if($result = $this->cache->get($key)){return $result;}
		$sSql="select * from MOST_POPULAR_COMPARE_SET_MASTER $whereClauseStr $limitStr";
		$result=$this->select($sSql);
		$this->cache->set($key,$result);
		return $result;
	}	
	/**
	* @note function is used to delete model from PRODUCT_NAME_INFO table
	* @param integer $product_name_id
	* @pre $product_name_id must be non-empty/zero valid integer.
	* @post boolean true/false.
	* return boolean.
	*/
	function deleteModel($product_name_id){
		$sql = "delete from PRODUCT_NAME_INFO where product_name_id = $product_name_id";
		$result = $this->sql_delete_data($sql);		
		$this->cache->searchDeleteKeys($this->productKey."_model");
		return $result;
	}
	/**
	* @note function is used to get Product count by price ascending 
	*
	* @param an integer/comma seperated product ids/ product ids array $product_ids.
	* @param an integer/comma seperated category_ids/ category_id array $category_id.
	* @param an integer/comma seperated  brand ids $brand_id.
	* @param a boolean $status .
	* @param $startprice.
	* @param $endprice.
	* @param an integer variant_id $variant_id.
	* @param an integer startlimit $startlimit.
	* @param an integer cnt $cnt.
	*
	* @pre not required.
	*
	* @post an associative array.
	* retun an array.
	*/
	function arrGetProductByPriceAscCount($product_ids="",$category_id="",$brand_id="",$status='1',$startprice="",$endprice="",$variant_id="1",$startlimit="",$cnt=""){
		$keyArr[] = $this->productKey."_carfinder_bypriceasc_searchcnt";
		if(!empty($startprice)){
			$keyArr[] = "startprice_$startprice";
			$whereClauseArr[] = "PRICE_VARIANT_VALUES.variant_value>=$startprice";				
		}
		if(!empty($endprice)){
			$keyArr[] = "endprice_$endprice";
			$whereClauseArr[] = "PRICE_VARIANT_VALUES.variant_value<=$endprice";
		}
		if(!empty($variant_id)){
			$keyArr[] = "status_1_variant_id_$variant_id";
			$whereClauseArr[] = "PRICE_VARIANT_VALUES.variant_id = $variant_id";				
			$whereClauseArr[] = "PRICE_VARIANT_VALUES.status = 1";
			$tablenameArr[] = "PRICE_VARIANT_VALUES";
		}
		if(!empty($startprice) or !empty($endprice) or !empty($variant_id)){
			$keyArr[] = "default_city_1_status_1";
			$whereClauseArr[] = "PRODUCT_MASTER.product_id = PRICE_VARIANT_VALUES.product_id";				
			$whereClauseArr[] = "PRICE_VARIANT_VALUES.default_city=1";
			$tablenameArr[] = "PRODUCT_NAME_INFO";
			$whereClauseArr[] = "PRODUCT_NAME_INFO.status=1";
		}

		$tablenameArr[] = "PRODUCT_MASTER";
		if(is_array($product_ids)){
			$product_ids = implode(",",$product_ids);
		}
		if(is_array($category_id)){
			$category_id = implode(",",$category_id);
		}
		if($status != '')
		{
			$keyArr[] = "status_$status";
			$whereClauseArr[] = "PRODUCT_MASTER.status=$status";
		}
		if(!empty($product_ids)){
			$keyArr[] = "product_id_$product_ids";
			$whereClauseArr[] = "PRODUCT_MASTER.product_id in($product_ids)";
		}
		if(!empty($category_id)){
			$keyArr[] = "category_id_$category_id";
			$whereClauseArr[] = "PRODUCT_MASTER.category_id in ($category_id)";
		}
		if(!empty($brand_id)){
			$keyArr[] = "brand_id_$brand_id";
			$whereClauseArr[] = "PRODUCT_MASTER.brand_id in ($brand_id)";
		}
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
		}
		if(!empty($startlimit)){
			$keyArr[] = "startlimit_$startlimit";
			$limitArr[] = $startlimit;
		}
		if(!empty($count)){
			$keyArr[] = "count_$count";
			$limitArr[] = $count;
		}
		if(sizeof($limitArr) > 0){
			$limitStr = " limit ".implode(" , ",$limitArr);
		}
		if(!empty($orderby)) { 
			$orderby = $orderby; 
		}
		$key = implode("_",$keyArr);
		if($result = $this->cache->get($key)){return $result;}
		$tableStr = implode(",",$tablenameArr);			
		$sql = "select count(distinct(PRODUCT_MASTER.product_name)) as cnt from $tableStr $whereClauseStr";	
		$result = $this->select($sql);
		$this->cache->set($key,$result);
		return $result;
	}
	/**
	* @note function is used to get Product by price ascending 
	*
	* @param an integer/comma seperated product ids/ product ids array $product_ids.
	* @param an integer/comma seperated category_ids/ category_id array $category_id.
	* @param an integer/comma seperated  brand ids $brand_id.
	* @param a boolean $status .
	* @param $startprice.
	* @param $endprice.
	* @param an integer variant_id $variant_id.
	* @param an integer startlimit $startlimit.
	* @param an integer cnt $cnt.
	*
	* @pre not required.
	*
	* @post an associative array.
	* retun an array.
	*/
	function arrGetProductByPriceAsc($product_ids="",$category_id="",$brand_id="",$status='1',$startprice="",$endprice="",$variant_id="1",$startlimit="",$cnt=""){
		$keyArr[] = $this->productKey."_carfinder_bypriceasc";
		$selectStr = "PRODUCT_MASTER.*";
		if(!empty($startprice)){
			$keyArr[] = "startprice_$startprice";
			$whereClauseArr[] = "PRICE_VARIANT_VALUES.variant_value>=$startprice";				
		}
		if(!empty($endprice)){
			$keyArr[] = "endprice_$endprice";
			$whereClauseArr[] = "PRICE_VARIANT_VALUES.variant_value<=$endprice";
		}
		if(!empty($variant_id)){
			$keyArr[] = "status_1_variant_id_$variant_id";
			$whereClauseArr[] = "PRICE_VARIANT_VALUES.variant_id = $variant_id";
			$whereClauseArr[] = "PRICE_VARIANT_VALUES.status = 1";
			$tablenameArr[] = "PRICE_VARIANT_VALUES";
		}

		if(!empty($startprice) or !empty($endprice) or !empty($variant_id)){
			$keyArr[] = "default_city_1_status_1";
			$whereClauseArr[] = "PRODUCT_MASTER.product_id = PRICE_VARIANT_VALUES.product_id";	
			$whereClauseArr[] = "PRICE_VARIANT_VALUES.default_city=1";			
			$selectStr = "PRODUCT_MASTER.*,PRICE_VARIANT_VALUES.*";
			$tablenameArr[] = "PRODUCT_NAME_INFO";
			$whereClauseArr[] = "lower(PRODUCT_MASTER.product_name) = lower(PRODUCT_NAME_INFO.product_info_name)";
			$whereClauseArr[] = "PRODUCT_NAME_INFO.status=1";	
		}
		if(!empty($default_city)){
			$keyArr[] = "default_city_1";
			$whereClauseArr[] = "PRICE_VARIANT_VALUES.default_city=1";
		}
		$tablenameArr[] = "PRODUCT_MASTER";
		if(is_array($product_ids)){
			$product_ids = implode(",",$product_ids);
		}
		if(is_array($category_id)){
			$category_id = implode(",",$category_id);
		}
		if($status != ''){
			$keyArr[] = "status_$status";
			$whereClauseArr[] = "PRODUCT_MASTER.status=$status";
		}
		if(!empty($product_ids)){
			$keyArr[] = "product_id_$product_ids";
			$whereClauseArr[] = "PRODUCT_MASTER.product_id in($product_ids)";
		}
		if(!empty($category_id)){
			$keyArr[] = "category_id_$category_id";
			$whereClauseArr[] = "PRODUCT_MASTER.category_id in ($category_id)";
		}
		if(!empty($brand_id)){
			$keyArr[] = "brand_id_$brand_id";
			$whereClauseArr[] = "PRODUCT_MASTER.brand_id in ($brand_id)";
		}
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
		}
		if(!empty($startlimit)){
			$keyArr[] = "startlimit_$startlimit";
			$limitArr[] = $startlimit;
		}
		if(!empty($cnt)){
			$keyArr[] = "count_$cnt";
			$limitArr[] = $cnt;
		}
		if(sizeof($limitArr) > 0){
			$limitStr = " limit ".implode(" , ",$limitArr);
		}
		
		$key = implode("_",$keyArr);
		$result = $this->cache->get($key);
		if(sizeof($result) <= 0){
			$tableStr = implode(",",$tablenameArr);
			$sql = "select PRODUCT_MASTER.product_name as product_info from $tableStr $whereClauseStr order by PRICE_VARIANT_VALUES.variant_value asc";	

			$result = $this->select($sql);			
			$this->cache->set($key,$result);
		}
		$searchArr = $this->arrGetUniqueSearchModel($result,$startlimit,$cnt);

		$result = $this->researchSummary($searchArr,$category_id,$startprice,$endprice,$variant_id,"PRICE_VARIANT_VALUES.variant_value asc");
		return $result;
	}
	/**
	* @note function is used to get Product by price descending 
	*
	* @param an integer/comma seperated product ids/ product ids array $product_ids.
	* @param an integer/comma seperated category_ids/ category_id array $category_id.
	* @param an integer/comma seperated  brand ids $brand_id.
	* @param a boolean $status .
	* @param $startprice.
	* @param $endprice.
	* @param an integer variant_id $variant_id.
	* @param an integer startlimit $startlimit.
	* @param an integer cnt $cnt.
	*
	* @pre not required.
	*
	* @post an associative array.
	* retun an array.
	*/
	function arrGetProductByPriceDesc($product_ids="",$category_id="",$brand_id="",$status='1',$startprice="",$endprice="",$variant_id="1",$startlimit="",$cnt=""){
		$keyArr[] = $this->productKey."_carfinder_bypricedesc";
		$selectStr = "PRODUCT_MASTER.*";
		if(!empty($startprice)){
			$keyArr[] = "startprice_$startprice";
			$whereClauseArr[] = "PRICE_VARIANT_VALUES.variant_value>=$startprice";				
		}
		if(!empty($endprice)){
			$keyArr[] = "endprice_$endprice";
			$whereClauseArr[] = "PRICE_VARIANT_VALUES.variant_value<=$endprice";
		}
		if(!empty($variant_id)){
			$keyArr[] = "status_1_variant_id_$variant_id";
			$whereClauseArr[] = "PRICE_VARIANT_VALUES.variant_id = $variant_id";
			$whereClauseArr[] = "PRICE_VARIANT_VALUES.status = 1";
			$tablenameArr[] = "PRICE_VARIANT_VALUES";
		}

		if(!empty($startprice) or !empty($endprice) or !empty($variant_id)){
			$keyArr[] = "default_city_1_status_1";
			$whereClauseArr[] = "PRODUCT_MASTER.product_id = PRICE_VARIANT_VALUES.product_id";	
			$whereClauseArr[] = "PRICE_VARIANT_VALUES.default_city=1";			
			$selectStr = "PRODUCT_MASTER.*,PRICE_VARIANT_VALUES.*";
			$tablenameArr[] = "PRODUCT_NAME_INFO";
			$whereClauseArr[] = "lower(PRODUCT_MASTER.product_name) = lower(PRODUCT_NAME_INFO.product_info_name)";
			$whereClauseArr[] = "PRODUCT_NAME_INFO.status=1";	
		}
		if(!empty($default_city)){
			$keyArr[] = "default_city_1";
			$whereClauseArr[] = "PRICE_VARIANT_VALUES.default_city=1";
		}
		$tablenameArr[] = "PRODUCT_MASTER";
		if(is_array($product_ids)){
			$product_ids = implode(",",$product_ids);
		}
		if(is_array($category_id)){
			$category_id = implode(",",$category_id);
		}
		if($status != ''){
			$keyArr[] = "status_$status";
			$whereClauseArr[] = "PRODUCT_MASTER.status=$status";
		}
		if(!empty($product_ids)){
			$keyArr[] = "product_id_$product_ids";
			$whereClauseArr[] = "PRODUCT_MASTER.product_id in($product_ids)";
		}
		if(!empty($category_id)){
			$keyArr[] = "category_id_$category_id";
			$whereClauseArr[] = "PRODUCT_MASTER.category_id in ($category_id)";
		}
		if(!empty($brand_id)){
			$keyArr[] = "brand_id_$brand_id";
			$whereClauseArr[] = "PRODUCT_MASTER.brand_id in ($brand_id)";
		}
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
		}
		if(!empty($startlimit)){
			$keyArr[] = "startlimit_$startlimit";
			$limitArr[] = $startlimit;
		}
		if(!empty($cnt)){
			$keyArr[] = "count_$cnt";
			$limitArr[] = $cnt;
		}
		if(sizeof($limitArr) > 0){
			$limitStr = " limit ".implode(" , ",$limitArr);
		}
		$key = implode("_",$keyArr);
		$result = $this->cache->get($key);
		if(sizeof($result) <= 0){
			$tableStr = implode(",",$tablenameArr);
			$sql = "select PRODUCT_MASTER.product_name as product_info from $tableStr $whereClauseStr order by PRICE_VARIANT_VALUES.variant_value desc";	
			$result = $this->select($sql);			
			$this->cache->set($key,$result);
		}

		$searchArr = $this->arrGetUniqueSearchModel($result,$startlimit,$cnt);

		$result = $this->researchSummary($searchArr,$category_id,$startprice,$endprice,$variant_id,"PRICE_VARIANT_VALUES.variant_value desc");
		return $result;
	}
	/**
	* @note function is used to get Product by name ascending 
	*
	* @param an integer/comma seperated product ids/ product ids array $product_ids.
	* @param an integer/comma seperated category_ids/ category_id array $category_id.
	* @param an integer/comma seperated  brand ids $brand_id.
	* @param a boolean $status .
	* @param $startprice.
	* @param $endprice.
	* @param an integer variant_id $variant_id.
	* @param an integer startlimit $startlimit.
	* @param an integer cnt $cnt.
	*
	* @pre not required.
	*
	* @post an associative array.
	* retun an array.
	*/
	function arrGetProductByNameAsc($product_ids="",$category_id="",$brand_id="",$status='1',$startprice="",$endprice="",$variant_id="1",$startlimit="",$cnt=""){
		$keyArr[] = $this->productKey."_carfinder_bynameasc";
		$selectStr = "PRODUCT_MASTER.*";
		if(!empty($startprice)){
			$keyArr[] = "startprice_$startprice";
			$whereClauseArr[] = "PRICE_VARIANT_VALUES.variant_value>=$startprice";				
		}
		if(!empty($endprice)){
			$keyArr[] = "endprice_$endprice";
			$whereClauseArr[] = "PRICE_VARIANT_VALUES.variant_value<=$endprice";
		}
		if(!empty($variant_id)){
			$keyArr[] = "status_1_variant_id_$variant_id";
			$whereClauseArr[] = "PRICE_VARIANT_VALUES.variant_id = $variant_id";
			$whereClauseArr[] = "PRICE_VARIANT_VALUES.status = 1";
			$tablenameArr[] = "PRICE_VARIANT_VALUES";
		}

		if(!empty($startprice) or !empty($endprice) or !empty($variant_id)){
			$keyArr[] = "default_city_1_status_1";
			$whereClauseArr[] = "PRODUCT_MASTER.product_id = PRICE_VARIANT_VALUES.product_id";	
			$whereClauseArr[] = "PRICE_VARIANT_VALUES.default_city=1";			
			$selectStr = "PRODUCT_MASTER.*,PRICE_VARIANT_VALUES.*";
			$tablenameArr[] = "PRODUCT_NAME_INFO";
			$whereClauseArr[] = "lower(PRODUCT_MASTER.product_name) = lower(PRODUCT_NAME_INFO.product_info_name)";
			$whereClauseArr[] = "PRODUCT_NAME_INFO.status=1";	
		}
		if(!empty($default_city)){
			$keyArr[] = "default_city_1";
			$whereClauseArr[] = "PRICE_VARIANT_VALUES.default_city=1";
		}
		$tablenameArr[] = "PRODUCT_MASTER";
		if(is_array($product_ids)){
			$product_ids = implode(",",$product_ids);
		}
		if(is_array($category_id)){
			$category_id = implode(",",$category_id);
		}
		if($status != ''){
			$keyArr[] = "status_$status";
			$whereClauseArr[] = "PRODUCT_MASTER.status=$status";
		}
		if(!empty($product_ids)){
			$keyArr[] = "product_id_$product_ids";
			$whereClauseArr[] = "PRODUCT_MASTER.product_id in($product_ids)";
		}
		if(!empty($category_id)){
			$keyArr[] = "category_id_$category_id";
			$whereClauseArr[] = "PRODUCT_MASTER.category_id in ($category_id)";
		}
		if(!empty($brand_id)){
			$keyArr[] = "brand_id_$brand_id";
			$whereClauseArr[] = "PRODUCT_MASTER.brand_id in ($brand_id)";
		}
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
		}
		if(!empty($startlimit)){
			$keyArr[] = "startlimit_$startlimit";
			$limitArr[] = $startlimit;
		}
		if(!empty($cnt)){
			$keyArr[] = "count_$cnt";
			$limitArr[] = $cnt;
		}
		if(sizeof($limitArr) > 0){
			$limitStr = " limit ".implode(" , ",$limitArr);
		}
		$key = implode("_",$keyArr);
		$result = $this->cache->get($key);
		if(sizeof($result) <= 0){
			$tableStr = implode(",",$tablenameArr);
			$sql = "select PRODUCT_MASTER.product_name as product_info from $tableStr $whereClauseStr order by PRODUCT_MASTER.product_full_name asc";		
			$result = $this->select($sql);	
			$this->cache->set($key,$result);
		}

		$searchArr = $this->arrGetUniqueSearchModel($result,$startlimit,$cnt);

		$result = $this->researchSummary($searchArr,$category_id,$startprice,$endprice,$variant_id,"PRICE_VARIANT_VALUES.variant_value asc");
		return $result;
	}
	 /**
	* @note function is used to get Product by name descending 
	*
	* @param an integer/comma seperated product ids/ product ids array $product_ids.
	* @param an integer/comma seperated category_ids/ category_id array $category_id.
	* @param an integer/comma seperated  brand ids $brand_id.
	* @param a boolean $status .
	* @param $startprice.
	* @param $endprice.
	* @param an integer variant_id $variant_id.
	* @param an integer startlimit $startlimit.
	* @param an integer cnt $cnt.
	*
	* @pre not required.
	*
	* @post an associative array.
	* retun an array.
	*/
	 function arrGetProductByNameDesc($product_ids="",$category_id="",$brand_id="",$status='1',$startprice="",$endprice="",$variant_id="1",$startlimit="",$cnt=""){
		$keyArr[] = $this->productKey."_carfinder_bynamedesc";
		$selectStr = "PRODUCT_MASTER.*";
		if(!empty($startprice)){
			$keyArr[] = "startprice_$startprice";
			$whereClauseArr[] = "PRICE_VARIANT_VALUES.variant_value>=$startprice";				
		}
		if(!empty($endprice)){
			$keyArr[] = "endprice_$endprice";
			$whereClauseArr[] = "PRICE_VARIANT_VALUES.variant_value<=$endprice";
		}
		if(!empty($variant_id)){
			$keyArr[] = "status_1_variant_id_$variant_id";
			$whereClauseArr[] = "PRICE_VARIANT_VALUES.variant_id = $variant_id";
			$whereClauseArr[] = "PRICE_VARIANT_VALUES.status = 1";
			$tablenameArr[] = "PRICE_VARIANT_VALUES";
		}

		if(!empty($startprice) or !empty($endprice) or !empty($variant_id)){
			$keyArr[] = "default_city_1_status_1";
			$whereClauseArr[] = "PRODUCT_MASTER.product_id = PRICE_VARIANT_VALUES.product_id";	
			$whereClauseArr[] = "PRICE_VARIANT_VALUES.default_city=1";			
			$selectStr = "PRODUCT_MASTER.*,PRICE_VARIANT_VALUES.*";
			$tablenameArr[] = "PRODUCT_NAME_INFO";
			$whereClauseArr[] = "lower(PRODUCT_MASTER.product_name) = lower(PRODUCT_NAME_INFO.product_info_name)";
			$whereClauseArr[] = "PRODUCT_NAME_INFO.status=1";	
		}
		if(!empty($default_city)){
			$keyArr[] = "default_city_1";
			$whereClauseArr[] = "PRICE_VARIANT_VALUES.default_city=1";
		}
		$tablenameArr[] = "PRODUCT_MASTER";
		if(is_array($product_ids)){
			$product_ids = implode(",",$product_ids);
		}
		if(is_array($category_id)){
			$category_id = implode(",",$category_id);
		}
		if($status != ''){
			$keyArr[] = "status_$status";
			$whereClauseArr[] = "PRODUCT_MASTER.status=$status";
		}
		if(!empty($product_ids)){
			$keyArr[] = "product_id_$product_ids";
			$whereClauseArr[] = "PRODUCT_MASTER.product_id in($product_ids)";
		}
		if(!empty($category_id)){
			$keyArr[] = "category_id_$category_id";
			$whereClauseArr[] = "PRODUCT_MASTER.category_id in ($category_id)";
		}
		if(!empty($brand_id)){
			$keyArr[] = "brand_id_$brand_id";
			$whereClauseArr[] = "PRODUCT_MASTER.brand_id in ($brand_id)";
		}
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
		}
		if(!empty($startlimit)){
			$keyArr[] = "startlimit_$startlimit";
			$limitArr[] = $startlimit;
		}
		if(!empty($cnt)){
			$keyArr[] = "count_$cnt";
			$limitArr[] = $cnt;
		}
		if(sizeof($limitArr) > 0){
			$limitStr = " limit ".implode(" , ",$limitArr);
		}
		$key = implode("_",$keyArr);
		$result = $this->cache->get($key);
		if(sizeof($result) <= 0){
			$tableStr = implode(",",$tablenameArr);
			$sql = "select PRODUCT_MASTER.product_name as product_info from $tableStr $whereClauseStr order by PRODUCT_MASTER.product_full_name desc";	
			$result = $this->select($sql);	
			$this->cache->set($key,$result);
		}

		$searchArr = $this->arrGetUniqueSearchModel($result,$startlimit,$cnt);

		$result = $this->researchSummary($searchArr,$category_id,$startprice,$endprice,$variant_id,"PRICE_VARIANT_VALUES.variant_value asc");
		return $result;
	 }
	/**
	* @note function is used to get research iprice product details
	*
	* @param is string product_info $product_info
	* @param an integer category_id $category_id.
	* @param $startprice.
	* @param $endprice.
	* @param an integer variant_id $variant_id.
	* @param a string order by $orderby.
	* @param an integer default_city $default_city.
	*
	* @post an associative array.
	* retun an array.
	*/
	 function researchPriceProductDetails($product_info,$category_id,$startprice,$endprice,$variant_id,$orderby=" PRICE_VARIANT_VALUES.variant_value asc",$default_city="1"){
		 $key = $this->productKey."_research_price_startprice_$startprice"."_endprice_$endprice"."_variant_id_$variant_id"."_default_city_$default_city"."_status_1_category_id_$category_id"."_product_info_$product_info";

		 if($result = $this->cache->get($key)){return $result;}

		 $sql = "select PRODUCT_MASTER.*,PRICE_VARIANT_VALUES.* from PRICE_VARIANT_VALUES,PRODUCT_MASTER where PRICE_VARIANT_VALUES.variant_value>=$startprice and PRICE_VARIANT_VALUES.variant_value<=$endprice and PRICE_VARIANT_VALUES.variant_id = $variant_id and PRODUCT_MASTER.product_id = PRICE_VARIANT_VALUES.product_id and PRICE_VARIANT_VALUES.default_city=$default_city and PRODUCT_MASTER.status=1 and PRODUCT_MASTER.category_id in ($category_id) and lower(PRODUCT_MASTER.product_name) = '".trim($product_info)."' order by $orderby";			
		 $result = $this->select($sql);
		 $this->cache->set($key,$result);
		 return $result;
	 }
	/**
	* @note function is used to get research Summary
	*
	* @param an array $result.
	* @param an integer category_id $category_id.
	* @param $startprice.
	* @param $endprice.
	* @param an integer variant_id $variant_id.
	* @param a string order by $orderby.
	*
	* @post an associative array.
	* retun an array.
	*/ 
	 function researchSummary($result,$category_id,$startprice,$endprice,$variant_id,$orderby="PRICE_VARIANT_VALUES.variant_value asc"){
		
		require_once(CLASSPATH.'feature.class.php');
		require_once(CLASSPATH.'overview.class.php');

		$feature = new FeatureManagement;
		$overview = new OverviewManagement;
		$overviewresult = $overview->arrGetCarFinderFeatureOverview();
		$overviewCnt = sizeof($overviewresult);

		$cnt = sizeof($result);
		
		for($i=0;$i<$cnt;$i++){
			$product_info = $result[$i]['product_info'];
			
			$product_result = $this->researchPriceProductDetails($product_info,$category_id,$startprice,$endprice,$variant_id,$orderby);

			$productcnt = sizeof($product_result);
			
			for($j=0;$j<$productcnt;$j++){
				
				$product_id = $product_result[$j]['product_id'];
				$categoryid = $product_result[$j]['categoryid'];
				if(!empty($category_id) && !empty($product_id)){
					unset($overviewArr);
					
					for($overview=0;$overview<$overviewCnt;$overview++){
						unset($featureoverviewArr);
						$overview_feature_id = $overviewresult[$overview]['feature_id'];
						$overview_title = $overviewresult[$overview]['title'];
						$overview_unit = $overviewresult[$overview]['abbreviation'];
						$overviewkey = $this->productKey."_carfinder_researchSummary_feature_id_$overview_feature_id"."_product_id_$product_id";
						$overviewfeature_details = $this->cache->get($overviewkey);
						if(sizeof($overviewfeature_details) <= 0){
							$sql = "select * from PRODUCT_FEATURE where feature_id = $overview_feature_id and product_id = $product_id";
							$overviewfeature_details = $this->select($sql);
							$this->cache->set($overviewkey,$overviewfeature_details);
						}
						$feature_value = $overviewfeature_details[0]['feature_value'];
						if($feature_value == "-"){$feature_value="";}
						if(!empty($feature_value)){
							$featureoverviewArr[] = $feature_value;
						}
						if(!empty($overview_unit) && !empty($feature_value)){
							$featureoverviewArr[] = $overview_unit;
						}
						if(sizeof($featureoverviewArr) > 0){
							$desc = implode(" ",$featureoverviewArr);							
							if(!empty($desc) && !empty($overview_title)){
								$desc = "<span class=\"b\">$overview_title:&#160;</span>$desc";
								$overviewArr[] = $desc;
							}elseif(!empty($desc) && empty($overview_title)){
								$overviewArr[] = $desc;
							}
						}
						
					}						
					$product_result[$j]['short_desc'] = implode('<span class="dvder">|</span>',$overviewArr);
					unset($overviewArr);
				}
				
			}
			$result[$i] = $product_result;
			
		}	
		
		return $result;
	}
	/**
	* @note function is used to get product name with price
	*
	* @param a string product_name $product_name.
	* @param an integer category_id $category_id.
	*
	* @post an associative array.
	* retun an array.
	*/
	function arrGetProductNameWithPrice($product_name,$category_id){
		$key = $this->productKey."_ProductNameWithPrice_product_name_$product_name"."_category_id_$category_id"."_status=$status";
		if($result = $this->cache->get($key)){return $result;}
		$product_name = strtolower($product_name);
		$sql = "select PRODUCT_MASTER.* from PRODUCT_MASTER,PRICE_VARIANT_VALUES where trim(product_name) = '".trim($product_name)."' and PRODUCT_MASTER.product_id = PRICE_VARIANT_VALUES.product_id and PRODUCT_MASTER.category_id = $category_id and PRODUCT_MASTER.status=1 and PRICE_VARIANT_VALUES.variant_id=1 and PRICE_VARIANT_VALUES.default_city=1 group by PRODUCT_MASTER.product_id";
		$result = $this->select($sql);
		$this->cache->set($key,$result);
		return $result;
	 }

	/**
	* @note function is used to get product name
	*
	* @param a string product_name $product_name.
	* @param an integer category_id $category_id.
	*
	* @post an associative array.
	* retun an array.
	*/
	function arrGetProductName($product_name,$category_id,$brand_id){
		$key = $this->productKey."_ProductNameWithPrice_product_name_$product_name"."_category_id_$category_id"."_status=$status"."_brand_id_$brand_id";
		if($result = $this->cache->get($key)){return $result;}
		$product_name = strtolower($product_name);
		$sql = "select PRODUCT_MASTER.* from PRODUCT_MASTER where trim(product_name) = '".trim($product_name)."' and PRODUCT_MASTER.brand_id = $brand_id and PRODUCT_MASTER.category_id = $category_id and PRODUCT_MASTER.status=1 group by PRODUCT_MASTER.product_id";
		$result = $this->select($sql);
		$this->cache->set($key,$result);
		return $result;
	}
	function arrGetProductNameByName($model_name,$product_name,$category_id,$brand_id){
		$key = $this->productKey."_ProductNameWithPrice_product_name_$product_name"."_category_id_$category_id"."_status=$status"."_brand_id_$brand_id"."_model_name_$model_name";
		if($result = $this->cache->get($key)){return $result;}

		$product_name = strtolower($product_name);
		$model_name = strtolower($model_name);

		$sql = "select PRODUCT_MASTER.* from PRODUCT_MASTER where trim(product_name) = '".trim($product_name)."' and trim(product_name) = '".trim($model_name)."' and PRODUCT_MASTER.brand_id = $brand_id and PRODUCT_MASTER.category_id = $category_id and PRODUCT_MASTER.status=1 group by PRODUCT_MASTER.product_id";
		$result = $this->select($sql);
		$this->cache->set($key,$result);
		return $result;
	}


	/**
	* @note function is used to get search product details 
	*
	* @param an array $result.
	* @param an integer $category_ids.
	* @param an integer $city_id.
	*
	* @post is an associative array.
	* retun an array.
	*/ 
	 function constantProductInfoDetails($result,$category_id,$city_id=""){
		global $searchShortDescArr;
		require_once(CLASSPATH.'price.class.php');
		require_once(CLASSPATH.'feature.class.php');
		require_once(CLASSPATH.'brand.class.php');
		$price = new price;
		$feature = new FeatureManagement;
		$brand = new BrandManagement;
		$cnt = sizeof($result);
		if(!empty($category_id)){
			$price_formula = $price->arrGetVariantFormulaDetail("","",$category_id);
			$variant_formula_id = $price_formula[0]['variant_formula_id'];
			$formula = $price_formula[0]['formula'];
		}

		for($i=0;$i<$cnt;$i++){
			$product_id = $result[$i]['product_id'];
			$categoryid = $result[$i]['category_id'];
			$brandid = $result[$i]['brand_id'];
			$product_name = $result[$i]['product_name'];
			unset($productNameArr);
			if(!empty($brandid)){
				$brand_result = $brand->arrGetBrandDetails($brandid);
				$productNameArr[] = $brand_result[0]['brand_name'];
			}
			$productNameArr[] = $result[$i]['product_name'];
			$result[$i]['link_product_name'] = implode(" ",$productNameArr);
			$productNameArr[] = $result[$i]['variant'];
			$display_product_name = implode(" ",$productNameArr);
			$result[$i]['display_product_name'] = $display_product_name;
			if(!empty($categoryid) && !empty($product_id)){
				$sOverviewArray = $feature->arrGetSummary($categoryid,$product_id,$type="array");
			}	
			if(is_array($sOverviewArray)){
				unset($productNameArr[0]);		// remove brand name form array.			
				foreach($sOverviewArray as $key=>$val){
						if($sOverviewArray[$key][0]){
							$overviewArr[] = implode(",&#160;",$sOverviewArray[$key][0]);								
						}
				}
				$result[$i]['short_desc'] = implode(",&#160;",$overviewArr);
				unset($overviewArr);
			}else{
				$result[$i]['short_desc'] = "";
			}

			if(!empty($product_id)){					
				if(!empty($city_id)){
					$price_result = $price->arrGetPriceDetails("",$product_id,$categoryid,"","",$city_id,"1","","","");
				}else{
					$price_result = $price->arrGetPriceDetails("",$product_id,$categoryid,"","","","1","","","1");
				}
				$aVariant=$price->arrGetVariantDetail("",$categoryid,"1","","");
				$priceCnt = sizeof($price_result);
				if(!empty($priceCnt)){
					for($j=0;$j<$priceCnt;$j++){
						$variant_id = $price_result[$j]['variant_id'];
						$variant_value = $price_result[$j]['variant_value'];
						if(in_array(EX_SHOWROOM_STR,$price_result[$j])){
							$result[$i]['exshowroomprice'] = $variant_value ? priceFormat($variant_value) : '';
						}
						$formulaValuesArr[$variant_id] = $variant_value ? $variant_value : 0;
						$aVar[]=$variant_id;
						$result[$i]['price_details'][] = $price_result[$j];
						
						
					}
				}else{
					$result[$i]['exshowroomprice'] = 0;
				}
				
				for($k=0;$k<count($aVariant);$k++){
					if(!in_array($aVariant[$k]['variant_id'],$aVar)){
						$formulaValuesArr[$aVariant[$k]['variant_id']]=0;
					}
				}
				$feature_result = $this->arrGetProductFeatureDetails("","",$product_id);
				$featureCnt = sizeof($feature_result);
				unset($short_desc_array);
				for($j=0;$j<$featureCnt;$j++){	
					unset($featureValueArr);		
					$feature_id = $feature_result[$j]['feature_id'];
					$feature_name = $feature_result[$j]['feature_name'];
					$feature_value = $feature_result[$j]['feature_value'];
					$featureValueArr[] = $feature_value;
					$feature_unit = $feature_result[$j]['unit_id'];
					if(!empty($feature_unit)){
						$feature_unit = $feature->arrFeatureUnitDetails($feature_unit,$categoryid);
						$unit_name = $feature_unit[0]['unit_name'];
						$featureValueArr[] = $unit_name;
					}
				
					$result[$i]['feature_result'][] = $feature_result[$j];
					
				}
					
				if(sizeof($formulaValuesArr) > 0){						
					$totalprice = strtr($formula,$formulaValuesArr);
					$totalprice = parse_mathematical_string($totalprice);
				}
				$onroadkey = str_replace(" ","_",ON_RAOD_PRICE_TITLE);
				$result[$i][$onroadkey] = $totalprice ? $totalprice :0;

				
				if(!empty($product_name)){
					$similar_product_result = $this->arrGetProductByName(strtolower($product_name),$product_id,"","1","0","6");
					$similarCnt = sizeof($similar_product_result);
					for($k=0;$k<$similarCnt;$k++){
						$similar_product_id = $similar_product_result[$k]['product_id'];
						$similar_brandid = $similar_product_result[$k]['brand_id'];
						$product_name = $similar_product_result[$k]['product_name'];
						unset($similarproductNameArr);
						if(!empty($similar_brandid)){
							$similar_brand_result = $brand->arrGetBrandDetails($similar_brandid);
							$similarproductNameArr[] = $similar_brand_result[0]['brand_name'];
						}
						$similarproductNameArr[] = $similar_product_result[$k]['product_name'];
						$similarproductNameArr[] = $similar_product_result[$k]['variant'];
						$similar_display_product_name = implode(" ",$similarproductNameArr);
						$similar_product_result[$k]['display_product_name'] = $similar_display_product_name;
						$similar_feature_result = $this->arrGetProductFeatureDetails("","",$similar_product_id);
						$featureCnt = sizeof($similar_feature_result);
						unset($short_desc_array);

						$aVar='';
						$price_result = $price->arrGetPriceDetails("",$similar_product_id,$categoryid,"","",$city_id);
						$aVariant=$price->arrGetVariantDetail("",$categoryid,"1","","");
						$priceCnt = sizeof($price_result);
						if(!empty($priceCnt)){
							for($j=0;$j<$priceCnt;$j++){
								$variant_id = $price_result[$j]['variant_id'];
								$variant_value = $price_result[$j]['variant_value'];
								$formulaValuesArr[$variant_id] = $variant_value ? $variant_value : 0;
								$aSVar[]=$variant_id;
								}
						}
						for($l=0;$l<count($aVariant);$l++){
							if(!in_array($aVariant[$l]['variant_id'],$aSVar)){
								$formulaValuesArr[$aVariant[$l]['variant_id']]=0;
							}
						}
						$aVariant=''; $aSVar='';
						if(sizeof($formulaValuesArr) > 0){						
							$similartotalprice = strtr($formula,$formulaValuesArr);
							$similartotalprice = parse_mathematical_string($similartotalprice);
						}
						$onroadkey = str_replace(" ","_",ON_RAOD_PRICE_TITLE);
						$similar_product_result[$k][$onroadkey] = $similartotalprice ? $similartotalprice :0;
						$compare_price_difference = $totalprice - $similartotalprice;
						$similar_product_result[$k]['compare_price_difference'] = $compare_price_difference;

						$price_result = $price->arrGetPriceDetails("1",$similar_product_id,$categoryid,"1","","","1");
						$exshowroom_price = $price_result[0]['variant_value'];
						$similar_product_result[$k]['exshowroomprice'] = $exshowroom_price ? $exshowroom_price : 0;							
						$result[$i]['similar_product'][] = $similar_product_result[$k];
					}
				}
				
			}
		
		}		
		return $result;
	 }

	/**
	* @note function is used to insert the oncars compare set information into the database.
	* @param an associative array $insert_param.
	* @pre $insert_param must be valid associative array.
	* @post an integer $oncars_compare_id.
	* retun integer.
	*/
	function addUpdOncarsCompareSetDetails($insert_param){
		$insert_param['create_date'] = date('Y-m-d H:i:s');
		$insert_param['update_date'] = date('Y-m-d H:i:s');
		$sql = $this->getInsertUpdateSql("ONCARS_COMPARISON",array_keys($insert_param),array_values($insert_param));
		$oncars_compare_id = $this->insertUpdate($sql);
		$this->cache->searchDeleteKeys($this->productKey."_oncars_compare_Set");		
		return $oncars_compare_id;
	}

	/**
	* @note function is used to delete the Oncars Compare set Detail.
	* @param integer $oncars_compare_id.
	* @pre $oncars_compare_id must be non-empty/zero valid integer.
	* @post boolean true/false.
	* return boolean.
	*/
	function boolDeleteOncarsCompareSetDetail($oncars_compare_id){
		$sql = "delete from ONCARS_COMPARISON where oncars_compare_id = $oncars_compare_id";
		$isDelete = $this->sql_delete_data($sql);
		$this->cache->searchDeleteKeys($this->productKey."_oncars_compare_Set");
		$sql1 = "delete from FEATURED_ONCARS_COMPARISON	where oncars_compare_id = $oncars_compare_id";
		$isDelete1 = $this->sql_delete_data($sql1);
		$this->cache->searchDeleteKeys($this->productKey."_oncars_featured_compare_Set");		
		return $isDelete;
	}
	/**
	* @note function is used to get oncars compare set details.
	* @param an integer/comma seperated oncars compare ids/ oncars compare ids array $oncars_compare_ids.
	* @param an integer category_id.
	* @param an integer/comma separated string of compare set.
	* @param an integer position.
	* @param boolean Active/InActive $status.
	* @param integer $startlimit.
	* @param integer $count.
	* @pre not required.
	* @post product details in associative array.
	* retun an array.
	*/
	function arrGetOncarsCompareSetDetails($oncars_compare_ids="",$category_ids="",$compare_set="",$position="",$status="1",$startlimit="",$cnt="",$orderby=""){
		$keyArr[] = $this->productKey."_oncars_compare_Set";
		if(is_array($oncars_compare_ids)){
			$oncars_compare_ids = implode(",",$oncars_compare_ids);
		}
		if(is_array($category_ids)){
			$category_ids = implode(",",$category_ids);
		}
		if($oncars_compare_ids != ""){
			$keyArr[] = "oncars_compare_id_$oncars_compare_ids";
			$whereClauseArr[] = " oncars_compare_id in($oncars_compare_ids)";
		}
		if($category_ids != ""){
			$keyArr[] = "category_id_$category_ids";
			$whereClauseArr[] = " category_id in($category_ids)";
		}
		if($compare_set != ""){
			$keyArr[] = "compare_set_$compare_set";
			$whereClauseArr[] = " compare_set = $compare_set";
		}
		if($position != ""){
			$keyArr[] = "position_$position";
			$whereClauseArr[] = " position = $position";
		}
		if($status != ""){
			$keyArr[] = "status_$status";
			$whereClauseArr[] = " status = $status";
		}
		if(!empty($startlimit)){
			$keyArr[] = "startlimit_$startlimit";
			$limitArr[] = $startlimit;
		}
		if(!empty($cnt)){
			$keyArr[] = "cnt_$cnt";
			$limitArr[] = $cnt;
		}
		if(sizeof($limitArr) > 0){
			$limitStr = " limit ".implode(" , ",$limitArr);
		}
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
		}
		if(empty($orderby)){
			$orderby = " order by create_date desc";
		}
		$key = implode("_",$keyArr);
		if($result = $this->cache->get($key)){return $result;}
		$sSql="select * from ONCARS_COMPARISON $whereClauseStr $orderby $limitStr";
		$result=$this->select($sSql);
		$this->cache->set($key,$result);
		return $result;
	}
	/**
	* @note function is used to insert the featured oncars compare set information into the database.
	* @param an associative array $insert_param.
	* @pre $insert_param must be valid associative array.
	* @post an integer $featured_compare_id.
	* retun integer.
	*/
	function addUpdFeaturedCompareSetDetails($insert_param,$table_name){
		$insert_param['create_date'] = date('Y-m-d H:i:s');
		$insert_param['update_date'] = date('Y-m-d H:i:s');
		$sql = $this->getInsertUpdateSql($table_name,array_keys($insert_param),array_values($insert_param));
		$featured_compare_id = $this->insertUpdate($sql);
		$this->cache->searchDeleteKeys($this->productKey."_oncars_featured_compare_Set");
		return $featured_compare_id;
	}

	/**
	* @note function is used to delete the Featured Oncars Compare set Detail.
	* @param integer $featured_compare_id.
	* @pre $featured_compare_id must be non-empty/zero valid integer.
	* @post boolean true/false.
	* return boolean.
	*/
	function boolDeleteFeaturedCompareSetDetail($featured_compare_id,$table_name){
		$sql = "delete from $table_name where featured_compare_id = $featured_compare_id";
		$isDelete = $this->sql_delete_data($sql);
		$this->cache->searchDeleteKeys($this->productKey."_oncars_featured_compare_Set");
		return $isDelete;
	}
	/**
	* @note function is used to get oncars compare set details.
	* @param an integer/comma seperated oncars compare ids/ oncars compare ids array $oncars_compare_ids.
	* @param an integer category_id.
	* @param an integer/comma separated string of compare set.
	* @param an integer position.
	* @param boolean Active/InActive $status.
	* @param integer $startlimit.
	* @param integer $count.
	* @pre not required.
	* @post product details in associative array.
	* retun an array.
	*/
	function arrGetFeaturedCompareSetDetails($featured_compare_ids="",$oncars_compare_id="",$category_ids="",$status="1",$ordering="",$startlimit="",$cnt="",$orderby=""){
		$keyArr[] = $this->productKey."_oncars_featured_compare_Set";
		if(is_array($featured_compare_ids)){
			$featured_compare_ids = implode(",",$featured_compare_ids);
		}
		if(is_array($oncars_compare_ids)){
			$keyArr[] = "oncars_compare_ids_$oncars_compare_ids";
			$oncars_compare_ids = implode(",",$oncars_compare_ids);
		}
		if(is_array($category_ids)){
			$category_ids = implode(",",$category_ids);
		}
		if($featured_compare_ids != ""){
			$keyArr[] = "featured_compare_ids_$featured_compare_ids";
			$whereClauseArr[] = " featured_compare_id in($featured_compare_ids)";
		}
		if($oncars_compare_ids != ""){
			$keyArr[] = "oncars_compare_ids_$oncars_compare_ids";
			$whereClauseArr[] = " oncars_compare_id in($oncars_compare_ids)";
		}
		if($category_ids != ""){
			$keyArr[] = "category_ids_$category_ids";
			$whereClauseArr[] = " category_id in($category_ids)";
		}
		if($status != ""){
			$keyArr[] = "status_$status";
			$whereClauseArr[] = " status = $status";
		}
		if($ordering != ""){
			$keyArr[] = "ordering_$ordering";
			$whereClauseArr[] = " ordering = $ordering";
		}
		if(!empty($startlimit)){
			$keyArr[] = "startlimit_$startlimit";
			$limitArr[] = $startlimit;
		}
		if(!empty($cnt)){
			$keyArr[] = "cnt_$cnt";
			$limitArr[] = $cnt;
		}
		if(sizeof($limitArr) > 0){
			$limitStr = " limit ".implode(" , ",$limitArr);
		}
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
		}
		if(empty($orderby)){
			$orderby = " order by create_date desc";
		}
		$key = implode("_",$keyArr);
		if($result = $this->cache->get($key)){return $result;}
		$sSql="select * from FEATURED_ONCARS_COMPARISON $whereClauseStr $orderby $limitStr";
		$result=$this->select($sSql);
		$this->cache->set($key,$result);
		return $result;
	}
	function arrGetUniqueSearchModel($result,$startlimit="0",$cnt="10"){
		$count = sizeof($result);
		for($i=0;$i<$count;$i++){
			$product_info = $result[$i]['product_info'];
			if(!in_array($product_info,$tmpArr)){											
				$tmpArr[] = $product_info;
			}
		}
		$count = sizeof($tmpArr);
		$startlimit = $startlimit ? $startlimit : 0;
		$cnt = $startlimit+$cnt;

		for($i=0;$i<$count;$i++){
			if($i >= $startlimit && $i < $cnt){
				$searchArr[]['product_info'] = $tmpArr[$i];
			}
		}
		return $searchArr;
	}


	function arrGetProductDetailsXML($product_ids,$category_id,$brand_id,$status,$startprice,$endprice,$variant_id,$startlimit,$count,$default_city,$orderby,$product_name,$city_id){
		$aResult	= $this->arrGetProductDetails($product_ids,$category_id,$brand_id,$status,$startprice,$endprice,$variant_id,$startlimit,$count,$default_city,$orderby,$product_name,$city_id);
		$productsXML= generateXML2($aResult, "PRODUCTS", "PRODUCT");
		return $productsXML;
	}
	 /**
	 * @note function is used to get product details in associative form.
	 *
	 * @param an integer/comma seperated product ids/ product ids array $product_ids.
	 * @param an integer category_id.
	 * @param an integer brand_id.
	 * @param boolean Active/InActive $status.
	 * @param integer $startlimit.
	 * @param integer $count.
	 *
	 * @pre not required.
	 *
	 * @post product details in associative array.
	 * retun an array.
	 */
	function arrGetProductDetailsInAssociativeForm($product_ids,$category_id,$brand_id,$status,$startprice,$endprice,$variant_id,$startlimit,$count,$default_city,$orderby,$product_name,$city_id){
		$aResult	= $this->arrGetProductDetails($product_ids,$category_id,$brand_id,$status,$startprice,$endprice,$variant_id,$startlimit,$count,$default_city,$orderby,$product_name,$city_id);
		if($aResult){
			foreach($aResult as $key => $value ){
				$data[$value['product_id']]	= $value['product_name'];
			}
		}
		return $data;
	}


	/**
	* @note function is used to get product_ids of price range
	* @param integer $minprice
	* @param integer $mxprice
	* @param array $brandids
	* return array.
	*/
	function arrGetProductIdsOfPriceRange($mnprice,$mxprice,$brandids=''){
		if(!empty($brandids)) $cond = " AND brand_id in ( " . $brandids  . ") " ;
		$sql	= "SELECT product_id from PRICE_VARIANT_VALUES where variant_value BETWEEN $mnprice AND $mxprice $cond order by variant_value ASC";
		//echo "\n<br/>", $sql;
		$oData	= $this->select($sql);
		//print_r($oData);
		foreach($oData as $key=>$value){
			$arrTemp[]	= $value['product_id'];
		}
		return $arrTemp;
	}

	/**
	* @note function is used to get product_ids of given Info
	* @param integer $announced
	* return array.
	*/
	function arrGetProductIdsOfInfo($announced){
		if(!empty($announced)){
			$sql	= "SELECT product_id from PRODUCT_MASTER where availibility LIKE '%$announced%'";
			$oData	= $this->select($sql);
			//echo "\n<br/>", $sql;
			//print_r($oData);
			foreach($oData as $key=>$value){
				$arrTemp[]	= $value['product_id'];
			}
			//print_r($arrTemp);
		}
		return $arrTemp;
	}

	/**
	* @note function is used to get product_ids of given feature
	* @param integer $announced
	* return array.
	*/
	function arrGetProductIdsOfFeature( $feature_id, $featuredata , $operator='' ){
		if(1){ //!empty($featuredata)){
			if(is_array($featuredata)){
				foreach($featuredata as $value){
					$arrCondition[]	=  " feature_numeric_value = $value ";
				}
			} else {
				$arrCondition[]	=  " feature_numeric_value = $featuredata ";
			}

			if($operator=='OR'){
				$condition	= implode(' OR ', $arrCondition);
			} else if($operator=='AND'){
				$condition	= implode(' AND ', $arrCondition);
			} else {
				$condition	= $arrCondition[0];
			}
		
			$sql	= "SELECT product_id from PRODUCT_FEATURE where feature_id= $feature_id AND ( $condition ) ";
			$oData	= $this->select($sql);
			//echo "\n<br/>", $sql;
			//print_r($oData);
			foreach($oData as $key=>$value){
				$arrTemp[]	= $value['product_id'];
			}
		}
		return $arrTemp;
	}

	/**
	* @note function is used to get product_ids order by given argument
	* @param condition string
	* @param orderBy string
	* @param direction string
	* return array.
	*/
	function arrGetProducts( $condition, $orderBy = 'product_id', $direction = 'ASC' ){

		if(empty($condition)) $conditionText = ' AND 1=1 ';
		else $conditionText = ' AND ' . $condition;

		$sql	= "SELECT product_id, product_full_name from PRODUCT_MASTER where status=1 $conditionText order by $orderBy $direction";
		$oData	= $this->select($sql);
		return $oData;
	}

}
?>