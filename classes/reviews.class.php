<?php
/**
 * @brief class is used to perform actions on the reviews
 * @author Sachin
 * @version 1.0
 * @created 11-Nov-2010 5:09:31 PM
 * @last updated on 08-Mar-2011 11:40:00 AM
 */
class reviews extends DbOperation{
	
	var $cache;
	var $categoryid;
	var $reviewkey;
	var $reviewtypekey;
	var $reviewgroupkey;
	var $revieweditorkey;
	var $reviewexpertkey;
	

	/**Initialize the consturctor.*/
	function reviews(){
		$this->cache = new Cache;
		$this->reviewkey = MEMCACHE_MASTER_KEY."review";
		$this->reviewtypekey = MEMCACHE_MASTER_KEY."review_type";
		$this->reviewgroupkey = MEMCACHE_MASTER_KEY."review_group";
		$this->revieweditorkey = MEMCACHE_MASTER_KEY."review_editor";
		$this->reviewexpertkey = MEMCACHE_MASTER_KEY."review_expert";
	}
	/**
	* @note function is used to add/update reviews details
	*
	* @pre  aParameters is array review details
	*
	* @param an associative array $aParameters
	* @param is a string $sTableName
	*
	* @pre $aParameters must be valid associative array.
	*
	* @post an integer $review_id.
	* retun integer.
	*/
	function addUpdReviewsDetails($aParameters,$sTableName){
		$aParameters['create_date'] = date('Y-m-d H:i:s');
		$aParameters['update_date'] = date('Y-m-d H:i:s');
	 	$sSql=$this->getInsertUpdateSql($sTableName,array_keys($aParameters),array_values($aParameters));	
		//echo "TEST---".$sSql."<br>";	
		$iRes=$this->insertUpdate($sSql);
		$this->cache->searchDeleteKeys($this->reviewkey);
		$this->cache->searchDeleteKeys($this->reviewkey."_featured");
		$this->cache->searchDeleteKeys($this->reviewkey."_latest");
		$this->cache->searchDeleteKeys($this->reviewkey."_related");
		$this->cache->searchDeleteKeys($this->reviewexpertkey);
		return $iRes;
	}
	/**
	* @note function is used to delete review
	* @param integer $iRId
	* @param string $sTableName
	* @pre $iRId must be non-empty/zero valid integer.
	* @post boolean true/false.
	* return boolean.
	*/
	function booldeleteReviews($iRId,$sTableName){
		$sSql="delete from REVIEWS where review_id='".$iRId."'";      
		$iRes=$this->sql_delete_data($sSql);
		$sSql='';
		$sSql="delete from PRODUCT_REVIEWS where review_id='".$iRId."'";      
		$iRes=$this->sql_delete_data($sSql);
		$this->cache->searchDeleteKeys($this->reviewkey);
		//$this->cache->searchDeleteKeys("reviews_detail");
		
	}
	/**
	* @note function is used to delete related review
	* @param integer $iRId
	* @param string $sTableName
	* @pre $iRId must be non-empty/zero valid integer.
	* @post boolean true/false.
	* return boolean.
	*/
	function booldeleteRelatedReviews($iRId,$sTableName){
		$sSql="delete from $sTableName where section_review_id='".$iRId."'";      
		$iRes=$this->sql_delete_data($sSql);
		$this->cache->searchDeleteKeys($this->reviewkey."_featured");
		$this->cache->searchDeleteKeys($this->reviewkey."_latest");
		$this->cache->searchDeleteKeys($this->reviewkey."_related");
	}
	/**
	* @note function is used to fetch review group for reviews details
	*
	* @param an integer/comma seperated group ids/ group ids array $group_ids.
	* @param a string/comma seperated group names/ group names array $group_names.
	* @param an integer/comma seperated category ids/ category ids array $category_ids.
	* @param boolean Active/InActive $status.
	*
	* @pre not required.
	*
	* @post reviews group details in associative array.
	* retun an array.
	*/
	function arrGetReviewsGroupDetails($group_ids="",$group_names="",$category_ids="",$status="1"){
		$keysArr[] = $this->reviewgroupkey;
		if(is_array($group_ids)){
			$group_ids=implode(",",$group_ids);
		}
		if(is_array($group_names)){
			$group_names=implode(",",$group_names);
		}
		if(is_array($category_ids)){
			$category_ids=implode(",",$category_ids);
		}
		if($group_names!=""){
			$whereClauseArr[] = "group_name in ($group_names)";
			$keysArr[] = "group_name_".$group_names;
		}
		if($group_ids!=""){
			$whereClauseArr[] = "group_id in ($group_ids)";
			$keysArr[] = "group_id_".$group_ids;
		}
		if($category_ids!=""){
			$whereClauseArr[] = "category_id in ($category_ids)";
			$keysArr[] = "category_id_".$category_ids;
		}
		if($status!=""){
			$whereClauseArr[] = "status in ($status)";
			$keysArr[] = "status_".$status;
		}
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
		}
		if(!empty($startlimit)){
			$limitArr[] = $startlimit;
			$keysArr[] = "startlimit_".$startlimit;
		}
		if(!empty($cnt)){
			$limitArr[] = $cnt;
			$keysArr[] = "cnt_".$cnt;
		}
		if(sizeof($limitArr) > 0){
			$limitStr = " limit ".implode(" , ",$limitArr);
		}
		$key = implode('_',$keysArr);
		//echo $key."<br>";
		$result = $this->cache->get($key);
		if(!empty($result)){ return $result;}
		
		$sSql="select * from REVIEW_GROUP $whereClauseStr $limitStr order by group_id asc";
		//echo  $sSql."<br>";
		$result = $this->select($sSql);
		$this->cache->set($key,$result);
		return $result;
	}
	/**
	* @note function is used to fetch review type for reviews details
	*
	* @param an integer/comma seperated article type ids/ article type ids array $article_type_ids.
	* @param a string/comma seperated type names/ type names array $type_names.
	* @param an integer/comma seperated category ids/ category ids array $category_ids.
	* @param boolean Active/InActive $status.
	* @param integer $startlimit.
	* @param integer $cnt.
	*
	* @pre not required.
	*
	* @post reviews type details in associative array.
	* retun an array.
	*/
	function arrGetReviewsTypeDetails($article_type_ids="",$type_names="",$category_ids="",$status="1",$startlimit="",$cnt=""){
		$keysArr[] = $this->reviewtypekey;
		if(is_array($article_type_ids)){
			$article_type_ids=implode(",",$article_type_ids);
		}
		if(is_array($type_names)){
			$type_names=implode(",",$type_names);
		}
		if(is_array($category_ids)){
			$category_ids=implode(",",$category_ids);
		}
		if($article_type_ids!=""){
			$whereClauseArr[] = "article_type_id in ($article_type_ids)";
			$keysArr[] = "article_type_id_".$article_type_ids;
		}
		if($type_names!=""){
			$whereClauseArr[] = "type_name in ($type_names)";
			$keysArr[] = "type_name_".$type_names;
		}
		if($category_ids!=""){
			$whereClauseArr[] = "category_id in ($category_ids)";
			$keysArr[] = "category_id_".$category_ids;
		}
		if($status!=""){
			$whereClauseArr[] = "status in ($status)";
			$keysArr[] = "status_".$status;
		}
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
		}
		if(!empty($startlimit)){
			$limitArr[] = $startlimit;
			$keysArr[] = "startlimit_".$startlimit;
		}
		if(!empty($cnt)){
			$limitArr[] = $cnt;
			$keysArr[] = "cnt_".$cnt;
		}
		if(sizeof($limitArr) > 0){
			$limitStr = " limit ".implode(" , ",$limitArr);
		}
		$key = implode('_',$keysArr);
		$result = $this->cache->get($key);
		if(!empty($result)){ return $result;}
		$sSql = "select * from ARTICLE_TYPE $whereClauseStr $limitStr";
		//echo $sSql."<br>";
		$result = $this->select($sSql);
		$this->cache->set($key, $result);
		return $result;
	}
	/**
	* @note function is used to get review detail list
	*
	* @param an integer/comma seperated review ids/ review ids array $review_ids.
	* @param an integer/comma seperated group ids/ group ids array $group_ids.
	* @param an integer/comma seperated type ids/ type ids array $type_ids.
	* @param an integer/comma seperated product ids/ product ids array $product_ids.
	* @param an integer/comma seperated product info ids/ product info ids array $product_info_id.
	* @param an integer/comma seperated category ids/ category ids array $category_id.
	* @param an integer/comma seperated brand ids/ brand ids array $brand_id.
	* @param boolean Active/InActive $status.
	* @param integer $startlimit.
	* @param integer $cnt.
	* @param string $orderby.
	*
	* @pre not required.
	*
	* @post reviews details in associative array.
	* retun an array.
	*/
	function getReviewsDetails($review_ids="",$group_ids="",$type_ids="",$product_ids="",$product_info_id="",$category_id="",$brand_id="",$status="1",$startlimit="",$cnt="", $orderby=""){
		$keysArr[] = $this->reviewkey."_detail";
		//$keysArr[] = "O_";
		$iCnt=$aParamaters['cnt'];
		if(is_array($review_ids)){
			$review_ids = implode(",",$review_ids);
		}
		if(is_array($type_ids)){
			$type_ids = implode(",",$type_ids);
		}
		if(is_array($group_ids)){
			$group_ids = implode(",",$group_ids);
		}
		if(is_array($product_info_id)){
			$product_info_id = implode(",",$product_info_id);
		}
		if(is_array($product_ids)){
			$product_ids = implode(",",$product_ids);
		}
		if(is_array($category_id)){
			$category_id = implode(",",$category_id);
		}
		if(is_array($brand_id)){
			$brand_id = implode(",",$brand_id);
		}
		if($status != ''){
			$whereClauseArr[] = "R.status=$status";
			$keysArr[] = "status_".$status;
		}
		if($review_ids!=""){
			$whereClauseArr[] = "R.review_id in ($review_ids)";
			$keysArr[] = "review_id_".$review_ids;
		}
		
		if($product_ids!=''){
		  $whereClauseArr[] = " PR.product_id in($product_ids)";
		  $keysArr[] = "product_id_".$product_ids;
		}
		if($product_info_id!=""){
			$whereClauseArr[] = " PR.product_info_id in($product_info_id)";
			$keysArr[] = "product_info_id_".$product_info_id;
		}
		if($group_ids!=""){
			$whereClauseArr[] = " PR.group_id in($group_ids)";
			$keysArr[] = "group_id_".$group_ids;

		}
		if($type_ids!=""){
			$whereClauseArr[] = " R.review_type in($type_ids)";
			$keysArr[] = "review_type_".$type_ids;
		}
		
		if($category_id!=""){
			$whereClauseArr[] = " PR.category_id in ($category_id)";
			$keysArr[] = "category_id_".$category_id;
		}
		if($brand_id!=""){
			$whereClauseArr[] = " PR.brand_id in ($brand_id)";
			$keysArr[] = "brand_id_".$brand_id;
		}
		$whereClauseArr[] = " PR.review_id=R.review_id ";
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
		}
		if(!empty($startlimit)){
			$limitArr[] = $startlimit;
			$keysArr[] = "startlimit_".$startlimit;
		}
		if(!empty($cnt)){
			$limitArr[] = $cnt;
			$keysArr[] = "cnt_".$cnt;
		}
		if(sizeof($limitArr) > 0){
			$limitStr = " limit ".implode(" , ",$limitArr);
		}
		$key = implode('_',$keysArr);
		//echo $key."<br>";
		$result = $this->cache->get($key);
		if(!empty($result)){ return $result;}
		$sSql="select *, DATE_FORMAT(R.create_date,'%d/%m/%Y') as disp_date from PRODUCT_REVIEWS PR, REVIEWS R $whereClauseStr $orderby $limitStr";
		//echo $sSql."<br/>";
		$result = $this->select($sSql);
		$this->cache->set($key,$result);
        return $result;
	}
	/**
	* @note function is used to get review detail list by editor
	*
	* @param an integer/comma seperated review ids/ review ids array $review_ids.
	* @param an integer/comma seperated group ids/ group ids array $group_ids.
	* @param an integer/comma seperated type ids/ type ids array $type_ids.
	* @param an integer/comma seperated product ids/ product ids array $product_ids.
	* @param an integer/comma seperated product info ids/ product info ids array $product_info_id.
	* @param an integer/comma seperated category ids/ category ids array $category_id.
	* @param an integer/comma seperated brand ids/ brand ids array $brand_id.
	* @param boolean Active/InActive $status.
	* @param integer $startlimit.
	* @param integer $cnt.
	* @param string $orderby.
	*
	* @pre not required.
	*
	* @post reviews details in associative array.
	* retun an array.
	*/
     function arrGetReviewsByEditor($review_ids="",$uids="",$group_ids="",$type_ids="",$product_ids="",$product_info_id="",$category_id="",$brand_id="",$status="1",$startlimit="",$cnt="", $orderby=""){
		    $keysArr[] = $this->revieweditorkey;
			$iCnt=$aParamaters['cnt'];
			if(is_array($review_ids)){
				$review_ids = implode(",",$review_ids);
			}
			if(is_array($uids)){
				$uids = implode(",",$uids);
			}
			if(is_array($type_ids)){
				$type_ids = implode(",",$type_ids);
			}
			if(is_array($group_ids)){
				$group_ids = implode(",",$group_ids);
			}
			if(is_array($product_info_id)){
				$product_info_id = implode(",",$product_info_id);
			}
			if(is_array($product_ids)){
				$product_ids = implode(",",$product_ids);
			}
			if(is_array($category_id)){
				$category_id = implode(",",$category_id);
			}
			if(is_array($brand_id)){
				$brand_id = implode(",",$brand_id);
			}
			if($status != ''){
				$whereClauseArr[] = "R.status=$status";
				$keysArr[] = "status_".$status;
			}
			if($review_ids!=""){
				$whereClauseArr[] = "R.review_id in ($review_ids)";
				$keysArr[] = "review_id_".$review_ids;
			}
			if($uids!=""){
				$whereClauseArr[] = "R.uid in ($uids)";
				$keysArr[] = "uid_".$uids;
			}

			if($product_ids!=''){
				$whereClauseArr[] = " PR.product_id in($product_ids)";
				$keysArr[] = "product_id_".$product_ids;
			}
			if($product_info_id!=""){
				$whereClauseArr[] = " PR.product_info_id in($product_info_id)";
				$keysArr[] = "product_info_id_".$product_info_id;
			}
			if($group_ids!=""){
				$whereClauseArr[] = " PR.group_id in($group_ids)";
				$keysArr[] = "group_id_".$group_ids;
			}
			if($type_ids!=""){
				$whereClauseArr[] = " R.review_type in($type_ids)";
				$keysArr[] = "review_type_".$type_ids;
			}
			if($category_id!=""){
				$whereClauseArr[] = " PR.category_id in ($category_id)";
				$keysArr[] = "category_id_".$category_id;
			}
			if($brand_id!=""){
				$whereClauseArr[] = " PR.brand_id in ($brand_id)";
				$keysArr[] = "brand_id_".$brand_id;
			}
			$whereClauseArr[] = " PR.review_id=R.review_id ";
			if(sizeof($whereClauseArr) > 0){
				$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
			}
			if(!empty($startlimit)){
				$limitArr[] = $startlimit;
				$keysArr[] = "startlimit_".$startlimit;
			}
			if(!empty($cnt)){
				$limitArr[] = $cnt;
				$keysArr[] = "cnt_".$cnt;
			}
			if(sizeof($limitArr) > 0){
				$limitStr = " limit ".implode(" , ",$limitArr);
			}
			$key = implode('_',$keysArr);
			$result = $this->cache->get($key);
			if(!empty($result)){ return $result;}
			$sSql="select *, DATE_FORMAT(R.create_date,'%d/%m/%Y') as disp_date from PRODUCT_REVIEWS PR, REVIEWS R $whereClauseStr $orderby $limitStr";
			//echo $sSql;
			$result=$this->select($sSql);
			$this->cache->set($key, $result);
			return $result;
     }
	/**
	 * @note function is used to get review detail list without featured reviews 
	 *
	 * @param an integer/comma seperated review ids/ review ids array $review_ids.
	 * @param an integer/comma seperated group ids/ group ids array $group_ids.
	 * @param an integer/comma seperated type ids/ type ids array $type_ids.
	 * @param an integer/comma seperated product ids/ product ids array $product_ids.
	 * @param an integer/comma seperated product info ids/ product info ids array $product_info_id.
	 * @param an integer/comma seperated category ids/ category ids array $category_id.
	 * @param an integer/comma seperated brand ids/ brand ids array $brand_id.
	 * @param boolean Active/InActive $status.
	 * @param integer $startlimit.
	 * @param integer $cnt.
	 * @param string $orderby.
	 *
	 * @pre not required.
	 *
	 * @post reviews details without featured reviews in associative array.
	 * retun an array.
	 */
	function getReviewsDetailsWithoutFeatured($review_ids="",$group_ids="",$type_ids="",$product_ids="",$product_info_id="",$category_id="",$brand_id="",$status="1",$startlimit="",$cnt="", $orderby=""){
		$keysArr[] = $this->reviewkey."_without_featured";
		$iCnt = $aParamaters['cnt'];
		if(is_array($review_ids)){
			$review_ids = implode(",",$review_ids);
		}
		if(is_array($type_ids)){
			$type_ids = implode(",",$type_ids);
		}
		if(is_array($group_ids)){
			$group_ids = implode(",",$group_ids);
		}
		if(is_array($product_info_id)){
			$product_info_id = implode(",",$product_info_id);
		}
		if(is_array($product_ids)){
			$product_ids = implode(",",$product_ids);
		}
		if(is_array($category_id)){
			$category_id = implode(",",$category_id);
		}
		if(is_array($brand_id)){
			$brand_id = implode(",",$brand_id);
		}
		if($status != ''){
			$whereClauseArr[] = "R.status=$status";
			$keysArr[] = "status_".$status;
		}
		if($review_ids!=""){
			$whereClauseArr[] = "R.review_id not in ($review_ids)";
			$keysArr[] = "review_id_".$review_ids;
		}
		
		if($product_ids!=''){
		  $whereClauseArr[] = " PR.product_id in($product_ids)";
		  $keysArr[] = "product_id_".$product_ids;
		}
		if($product_info_id!=""){
			$whereClauseArr[] = " PR.product_info_id in($product_info_id)";
			$keysArr[] = "product_info_id_".$product_info_id;
		}
		if($group_ids!=""){
			$whereClauseArr[] = " PR.group_id in($group_ids)";
			$keysArr[] = "group_id_".$group_ids;
		}
		if($type_ids!=""){
			$whereClauseArr[] = " R.review_type in($type_ids)";
			$keysArr[] = "review_type_".$type_ids;
		}
		
		if($category_id!=""){
			$whereClauseArr[] = " PR.category_id in ($category_id)";
			$keysArr[] = "category_id_".$category_id;
		}
		if($brand_id!=""){
			$whereClauseArr[] = " PR.brand_id in ($brand_id)";
			$keysArr[] = "brand_id_".$brand_id;
		}
		$whereClauseArr[] = " PR.review_id=R.review_id ";
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
		}
		if(!empty($startlimit)){
			$limitArr[] = $startlimit;
			$keysArr[] = "startlimit_".$startlimit;
		}
		if(!empty($cnt)){
			$limitArr[] = $cnt;
			$keysArr[] = "cnt_".$cnt;
		}
		if(sizeof($limitArr) > 0){
			$limitStr = " limit ".implode(" , ",$limitArr);
		}
		$key = implode('_',$keysArr);
		//echo $key."<br>";
		$result = $this->cache->get($key);
		if(!empty($result)){ return $result;}
		$sSql="select *, DATE_FORMAT(R.create_date,'%d %b %Y') as disp_date from PRODUCT_REVIEWS PR, REVIEWS R $whereClauseStr $orderby $limitStr";
		//echo $sSql;
		$result = $this->select($sSql);
		$this->cache->set($key, $result);
		return $result;
	}
	/**
	* @note function is used to get review detail count
	*
	* @param an integer/comma seperated review ids/ review ids array $review_ids.
	* @param an integer/comma seperated group ids/ group ids array $group_ids.
	* @param an integer/comma seperated type ids/ type ids array $type_ids.
	* @param an integer/comma seperated product ids/ product ids array $product_ids.
	* @param an integer/comma seperated product info ids/ product info ids array $product_info_id.
	* @param an integer/comma seperated category ids/ category ids array $category_id.
	* @param an integer/comma seperated brand ids/ brand ids array $brand_id.
	* @param boolean Active/InActive $status.
	*
	* @pre not required.
	*
	* @post integer count.
	* retun an integer.
	*/
	function getReviewsDetailsCount($review_ids="",$group_ids="",$type_ids="",$product_ids="",$product_info_id="",$category_id="",$brand_id="",$status="1"){
		$keysArr[] = $this->reviewkey."_count";
		$iCnt=$aParamaters['cnt'];
		if(is_array($review_ids)){
			$review_ids = implode(",",$review_ids);
		}
		if(is_array($type_ids)){
			$type_ids = implode(",",$type_ids);
		}
		if(is_array($group_ids)){
			$group_ids = implode(",",$group_ids);
		}
		if(is_array($product_info_id)){
			$product_info_id = implode(",",$product_info_id);
		}
		if(is_array($product_ids)){
			$product_ids = implode(",",$product_ids);
		}
		if(is_array($category_id)){
			$category_id = implode(",",$category_id);
		}
		if(is_array($brand_id)){
			$brand_id = implode(",",$brand_id);
		}
		if($status != ''){
			$whereClauseArr[] = "R.status=$status";
			$keysArr[] = "status_".$status;
		}
		if($review_ids!=""){
			$whereClauseArr[] = "R.review_id in ($review_ids)";
			$keysArr[] = "review_id_".$review_ids;
		}
		if($product_ids!=""){
			$whereClauseArr[] = " PR.product_id in($product_ids)";
			$keysArr[] = "product_id_".$product_ids;
		}
		if($product_info_id!=""){
			$whereClauseArr[] = " PR.product_info_id in($product_info_id)";
			$keysArr[] = "product_info_id_".$product_info_id;
		}
		if($group_ids!=""){
			$whereClauseArr[] = " PR.group_id in($group_ids)";
			$keysArr[] = "group_id_".$group_ids;
		}
		if($type_ids!=""){
			$whereClauseArr[] = " R.review_type in($type_ids)";
			$keysArr[] = "review_type_".$type_ids;
		}
		
		if($category_id!=""){
			$whereClauseArr[] = " PR.category_id in ($category_id)";
			$keysArr[] = "category_id_".$category_id;
		}
		if($brand_id!=""){
			$whereClauseArr[] = " PR.brand_id in ($brand_id)";
			$keysArr[] = "brand_id_".$brand_id;
		}
		$whereClauseArr[] = " PR.review_id=R.review_id ";
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
		}

		$key = implode('_',$keysArr);
		//echo "KEY---".$key."<br>";
		$result = $this->cache->get($key);
		if(!empty($result)){ return $result[0]['count'];}
		$sSql="select count(*) as count from PRODUCT_REVIEWS PR, REVIEWS R $whereClauseStr";
		//echo $sSql;
		$result=$this->select($sSql);	
		$this->cache->set($key, $result);
		return $result[0]['count'];
	}
	/**
	* @note function is used to get review detail count without featured reviews.
	*
	* @param an integer/comma seperated review ids/ review ids array $review_ids.
	* @param an integer/comma seperated group ids/ group ids array $group_ids.
	* @param an integer/comma seperated type ids/ type ids array $type_ids.
	* @param an integer/comma seperated product ids/ product ids array $product_ids.
	* @param an integer/comma seperated product info ids/ product info ids array $product_info_id.
	* @param an integer/comma seperated category ids/ category ids array $category_id.
	* @param an integer/comma seperated brand ids/ brand ids array $brand_id.
	* @param boolean Active/InActive $status.
	*
	* @pre not required.
	*
	* @post integer count.
	* retun an integer.
	*/
	function getReviewsDetailsCountWithoutFeatured($review_ids="",$group_ids="",$type_ids="",$product_ids="",$product_info_id="",$category_id="",$brand_id="",$status="1"){
		$keysArr[] = $this->reviewkey."_without_featured_cnt";
		$iCnt=$aParamaters['cnt'];
		if(is_array($review_ids)){
			$review_ids = implode(",",$review_ids);
		}
		if(is_array($type_ids)){
			$type_ids = implode(",",$type_ids);
		}
		if(is_array($group_ids)){
			$group_ids = implode(",",$group_ids);
		}
		if(is_array($product_info_id)){
			$product_info_id = implode(",",$product_info_id);
		}
		if(is_array($product_ids)){
			$product_ids = implode(",",$product_ids);
		}
		if(is_array($category_id)){
			$category_id = implode(",",$category_id);
		}
		if(is_array($brand_id)){
			$brand_id = implode(",",$brand_id);
		}
		if($status != ''){
			$whereClauseArr[] = "R.status=$status";
			$keysArr[] = "status_".$status;
		}
		if($review_ids!=""){
			$whereClauseArr[] = " R.review_id not in ($review_ids)";
			$keysArr[] = "review_id_".$review_ids;
		}
		if($product_ids!=""){
			$whereClauseArr[] = " PR.product_id in($product_ids)";
			$keysArr[] = "product_id_".$product_ids;
		}
		if($product_info_id!=""){
			$whereClauseArr[] = " PR.product_info_id in($product_info_id)";
			$keysArr[] = "product_info_id_".$product_info_id;
		}
		if($group_ids!=""){
			$whereClauseArr[] = " PR.group_id in($group_ids)";
			$keysArr[] = "group_id_".$group_ids;
		}
		if($type_ids!=""){
			$whereClauseArr[] = " R.review_type in($type_ids)";
			$keysArr[] = "review_type_".$type_ids;
		}
		
		if($category_id!=""){
			$whereClauseArr[] = " PR.category_id in ($category_id)";
			$keysArr[] = "category_id_".$category_id;
		}
		if($brand_id!=""){
			$whereClauseArr[] = " PR.brand_id in ($brand_id)";
			$keysArr[] = "brand_id_".$brand_id;
		}
		$whereClauseArr[] = " PR.review_id=R.review_id ";
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
		}
		$key = implode('_',$keysArr);
		//echo "KEY---".$key."<br>";
		$result = $this->cache->get($key);
		if(!empty($result)){ return $result[0]['count'];}
		$sSql="select count(*) as count from PRODUCT_REVIEWS PR, REVIEWS R $whereClauseStr";
		//echo $sSql;
		$result = $this->select($sSql);
		$this->cache->set($key, $result);
		return $result[0]['count'];
	}
	/**
	* @note function is used to get upload media reviews details.
	*
	* @param an integer $prdRid.
	*
	* @post get upload media reviews details without featured reviews in associative array.
	* retun an array.
	*/	
	function arrGetUploadMediaReviewsDetails($prdRid,$content_type=""){
		 $keysArr[] = $this->reviewkey."_upload_media";
		 if(!empty($prdRid)){
			$whereClauseArr[] = "product_review_id = $prdRid";
			$keysArr[] = "product_review_id_".$prdRid;
		 }
		 if(!empty($content_type)){
			 $whereClauseArr[] = "content_type = $content_type";
			 $keysArr[] = "content_type_".$content_type;
		 }
		 if(sizeof($whereClauseArr) > 0){
			 $whereClauseStr = " where ".implode(" and ",$whereClauseArr);
		 }
		$key = implode('_',$keysArr);
		//echo "KEY---".$key."<br>";
		$result = $this->cache->get($key);
		if(!empty($result)){ return $result;}
		$sSql = "select * from UPLOAD_MEDIA_REVIEWS $whereClauseStr";
		$result = $this->select($sSql);
		$this->cache->set($key, $result);
		return $result;
	}
	/**
	* @note function is used to get expert reviews details list.
	*
	* @param an integer/comma seperated review ids/ review ids array $review_ids.
	* @param an integer/comma seperated group ids/ group ids array $group_ids.
	* @param an integer/comma seperated type ids/ type ids array $type_ids.
	* @param an integer/comma seperated product ids/ product ids array $product_ids.
	* @param an integer/comma seperated product info ids/ product info ids array $product_info_id.
	* @param an integer/comma seperated category ids/ category ids array $category_id.
	* @param an integer/comma seperated brand ids/ brand ids array $brand_id.
	* @param boolean Active/InActive $status.
	* @param integer $startlimit.
	* @param integer $cnt.
	* @param string $orderby.
	*
	* @post get expert reviews details without featured reviews in associative array.
	* retun an array.
	*/
	function getExpertReviewsDetails($review_ids="",$group_ids="",$type_ids="",$product_ids="",$product_info_id="",$category_id="",$brand_id="",$status="1",$startlimit="",$cnt="", $orderby=""){
		$keysArr[] = $this->reviewexpertkey;
		$iCnt = $aParamaters['cnt'];
		if(is_array($review_ids)){
			$review_ids = implode(",",$review_ids);
		}
		if(is_array($type_ids)){
			$type_ids = implode(",",$type_ids);
		}
		if(is_array($group_ids)){
			$group_ids = implode(",",$group_ids);
		}
		if(is_array($product_info_id)){
			$product_info_id = implode(",",$product_info_id);
		}
		if(is_array($product_ids)){
			$product_ids = implode(",",$product_ids);
		}
		if(is_array($category_id)){
			$category_id = implode(",",$category_id);
		}
		if(is_array($brand_id)){
			$brand_id = implode(",",$brand_id);
		}
		if($status != ''){
			$whereClauseArr[] = "R.status=$status";
			$keysArr[] = "status_".$status;
		}
		if($review_ids!=""){
			$whereClauseArr[] = "R.review_id in ($review_ids)";
			$keysArr[] = "review_id_".$review_ids;
		}
		if($product_ids!=""){
			$whereClauseArr[] = " PR.product_id in($product_ids)";
			$keysArr[] = "product_id_".$product_ids;
		}
		if($group_ids!=""){
			$whereClauseArr[] = " PR.group_id in($group_ids)";
			$keysArr[] = "group_id_".$group_ids;
		}
		if($type_ids!=""){
			$whereClauseArr[] = " R.review_type in($type_ids)";
			$keysArr[] = "review_type_".$type_ids;
		}
		if($product_info_id!=""){
			$whereClauseArr[] = " PR.product_info_id in($product_info_id)";	
			$keysArr[] = "product_info_id_".$product_info_id;	
		}
		if($category_id!=""){
			$whereClauseArr[] = " PR.category_id in ($category_id)";
			$keysArr[] = "category_id_".$category_id;
		}
		if($brand_id!=""){
			$whereClauseArr[] = " PR.brand_id in ($brand_id)";
			$keysArr[] = "brand_id_".$brand_id;
		}
		$whereClauseArr[] = " PR.review_id=R.review_id ";
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
		}
		if(!empty($startlimit)){
			$limitArr[] = $startlimit;
			$keysArr[] = "startlimit_".$startlimit;
		}
		if(!empty($cnt)){
			$limitArr[] = $cnt;
			$keysArr[] = "cnt_".$cnt;
		}
		if(sizeof($limitArr) > 0){
			$limitStr = " limit ".implode(" , ",$limitArr);
		}
		$key = implode('_',$keysArr);
		//echo "KEY---".$key."<br>";
		$result = $this->cache->get($key);
		if(!empty($result)){ return $result;}
		$sSql="select *, DATE_FORMAT(R.create_date,'%d/%m/%Y') as disp_date from PRODUCT_EXPERT_REVIEWS PR, EXPERT_REVIEWS R $whereClauseStr $orderby $limitStr";
		//echo $sSql;
		$result = $this->select($sSql);
		$this->cache->set($key, $result);
		return $result;
	}
	/**
	* @note function is used to get expert reviews details count.
	*
	* @param an integer/comma seperated review ids/ review ids array $review_ids.
	* @param an integer/comma seperated group ids/ group ids array $group_ids.
	* @param an integer/comma seperated type ids/ type ids array $type_ids.
	* @param an integer/comma seperated product ids/ product ids array $product_ids.
	* @param an integer/comma seperated product info ids/ product info ids array $product_info_id.
	* @param an integer/comma seperated category ids/ category ids array $category_id.
	* @param an integer/comma seperated brand ids/ brand ids array $brand_id.
	* @param boolean Active/InActive $status.
	* @param integer $startlimit.
	* @param integer $cnt.
	* @param string $orderby.
	*
	* @post integer expert reviews details count.
	* retun an array.
	*/
	function getExpertReviewsDetailsCount($review_ids="",$group_ids="",$type_ids="",$product_ids="",$product_info_id="",$category_id="",$brand_id="",$status="1",$startlimit="",$cnt="", $orderby=""){
		$keysArr[] = $this->reviewexpertkey."_cnt";
		$iCnt=$aParamaters['cnt'];
		if(is_array($review_ids)){
			$review_ids = implode(",",$review_ids);
		}
		if(is_array($type_ids)){
			$type_ids = implode(",",$type_ids);
		}
		if(is_array($group_ids)){
			$group_ids = implode(",",$group_ids);
		}
		if(is_array($product_info_id)){
			$product_info_id = implode(",",$product_info_id);
		}
		if(is_array($product_ids)){
			$product_ids = implode(",",$product_ids);
		}
		if(is_array($category_id)){
			$category_id = implode(",",$category_id);
		}
		if(is_array($brand_id)){
			$brand_id = implode(",",$brand_id);
		}
		if($status != ''){
			$whereClauseArr[] = "R.status=$status";
			$keysArr[] = "status_".$status;
		}
		if($review_ids!=""){
			$whereClauseArr[] = "R.review_id in ($review_ids)";
			$keysArr[] = "review_id_".$review_ids;
		}
		if($product_ids!=""){
			$whereClauseArr[] = " PR.product_id in($product_ids)";
			$keysArr[] = "product_id_".$product_ids;
		}
		if($product_info_id!=""){
			$whereClauseArr[] = " PR.product_info_id in($product_info_id)";
			$keysArr[] = "product_info_id_".$product_info_id;
		}
		if($group_ids!=""){
			$whereClauseArr[] = " PR.group_id in($group_ids)";
			$keysArr[] = "group_id_".$group_ids;
		}
		if($type_ids!=""){
			$whereClauseArr[] = " R.review_type in($type_ids)";
			$keysArr[] = "review_type_".$type_ids;
		}
		if($category_id!=""){
			$whereClauseArr[] = " PR.category_id in ($category_id)";
			$keysArr[] = "category_id_".$category_id;
		}
		if($brand_id!=""){
			$whereClauseArr[] = " PR.brand_id in ($brand_id)";
			$keysArr[] = "brand_id_".$brand_id;
		}
		$whereClauseArr[] = " PR.review_id=R.review_id ";
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
		}
		$key = implode('_',$keysArr);
		//echo "KEY---".$key."<br>";
		$result = $this->cache->get($key);
		if(!empty($result)){ return $result;}
		$sSql="select count(*) as count from PRODUCT_EXPERT_REVIEWS PR, EXPERT_REVIEWS R $whereClauseStr";
		//echo $sSql;
		$result = $this->select($sSql);
		$this->cache->set($key, $result);
		return $result;
	}
	
	/**
	* @note function is used to delete expert reviews.
	* @param integer $iRId.
	* @param string $sTableName
	* @pre $iRId must be non-empty/zero valid integer.
	* @post boolean true/false.
	* return boolean.
	*/
	function booldeleteExpertReviews($iRId,$sTableName){
		$sSql="delete from EXPERT_REVIEWS where review_id='".$iRId."'";      
		$iRes=$this->sql_delete_data($sSql);
		$sSql='';
		$sSql="delete from PRODUCT_EXPERT_REVIEWS where review_id='".$iRId."'";      
		$iRes=$this->sql_delete_data($sSql);
		$this->cache->searchDeleteKeys($this->reviewexpertkey);
	}
	
	/**
	* @note function is used  get latest reviews details 
	*
	* @param an integer/comma seperated section ids/ section ids array $section_ids.
	* @param an integer/comma seperated review ids/ review ids array $review_ids.
	* @param an integer/comma seperated type ids/ type ids array $type_ids.
	* @param an integer/comma seperated category ids/ category ids array $category_ids.
	* @param boolean Active/InActive $status.
	* @param integer $startlimit.
	* @param integer $cnt.
	*
	* @pre not required.
	*
	* @post latest reviews details in associative array.
	* retun an array.
	*/
	function arrGetLatestReviewsDetails($section_ids="",$review_ids="",$type_ids="",$category_ids="",$status="1",$startlimit="",$cnt=""){
		$keysArr[] = $this->reviewkey."_latest";
		if(is_array($section_ids)){
			$section_ids = implode(",",$section_ids);
		}
		if(is_array($category_ids)){
			$category_ids = implode(",",$category_ids);
		}
		if(is_array($review_ids)){
			$review_ids = implode(",",$review_ids);
		}
		if(is_array($type_ids)){
			$type_ids = implode(",",$type_ids);
		}
		if($status != ''){
			$whereClauseArr[] = "R.status=$status";
			$keysArr[] = "status_".$status;
		}
		if($review_ids!=""){
			$whereClauseArr[] = "R.review_id in ($review_ids)";
			$keysArr[] = "review_id_".$review_ids;
		}
		if($category_ids!=""){
			$whereClauseArr[] = "LR.category_id in ($category_ids)";
			$keysArr[] = "category_id_".$category_ids;
		}
		if($section_ids!=""){
			$whereClauseArr[] = " LR.section_review_id in ($section_ids)";
			$keysArr[] = "section_review_id_".$section_ids;
		}
		if($type_ids!=""){
			$whereClauseArr[] = " LR.type_id in ($type_ids)";
			$keysArr[] = "type_id_".$type_ids;
		}
		$whereClauseArr[] = " LR.review_id=R.review_id ";
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
		}
		if(!empty($startlimit)){
			$limitArr[] = $startlimit;
			$keysArr[] = "startlimit_".$startlimit;
		}
		if(!empty($cnt)){
			$limitArr[] = $cnt;
			$keysArr[] = "cnt_".$cnt;
		}
		if(sizeof($limitArr) > 0){
			$limitStr = " limit ".implode(" , ",$limitArr);
		}

		$key = implode('_',$keysArr);
		$result = $this->cache->get($key);
		if(!empty($result)){ return $result;}	

		$sSql="SELECT * , LR.status as status  FROM `LATEST_REVIEWS` LR,REVIEWS R $whereClauseStr $orderby $limitStr";
		$result = $this->select($sSql);
		$this->cache->set($key, $result);
		return $result;
	}
	
	/**
	* @note function is used to get Featured reviews details
	*
	* @param an integer/comma seperated section ids/ section ids array $section_ids.
	* @param an integer/comma seperated review ids/ review ids array $review_ids.
	* @param an integer/comma seperated type ids/ type ids array $type_ids.
	* @param an integer/comma seperated category ids/ category ids array $category_ids.
	* @param boolean Active/InActive $status.
	* @param integer $startlimit.
	* @param integer $cnt.
	* @param string $orderby.
	*
	* @pre not required.
	*
	* @post featured reviews details in associative array.
	* retun an array.
	*/
	function arrGetFeaturedReviewsDetails($section_ids="",$review_ids="",$type_ids="",$category_ids="",$status="1",$startlimit="",$cnt="",$orderby=""){
		$keysArr[] = $this->reviewkey."_featured";
		if(is_array($section_ids)){
			$section_ids = implode(",",$section_ids);
		}
		
		if(is_array($review_ids)){
			$review_ids = implode(",",$review_ids);
		}
		if(is_array($category_ids)){
			$category_ids = implode(",",$category_ids);
		}
		if(is_array($type_ids)){
			$type_ids = implode(",",$type_ids);
		}
		if($status != ''){
			$whereClauseArr[] = "R.status=$status";
			$keysArr[] = "status_".$status;
		}
		if($category_ids!=""){
			$whereClauseArr[] = "FR.category_id in ($category_ids)";
			$keysArr[] = "category_id_".$category_ids;
		}
		if($review_ids!=""){
			$whereClauseArr[] = "R.review_id in ($review_ids)";
			$keysArr[] = "review_id_".$review_ids;
		}
		if($section_ids!=""){
			$whereClauseArr[] = " FR.section_review_id in ($section_ids)";
			$keysArr[] = "section_review_id_".$section_ids;
		}
		if($type_ids!=""){
			$whereClauseArr[] = " FR.type_id in ($type_ids)";
			$keysArr[] = "type_id_".$type_ids;
		}
		$whereClauseArr[] = " FR.review_id=R.review_id ";
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
		}
		if(!empty($startlimit)){
			$limitArr[] = $startlimit;
			$keysArr[] = "startlimit_".$startlimit;
		}
		if(!empty($cnt)){
			$limitArr[] = $cnt;
			$keysArr[] = "cnt_".$cnt;
		}
		if(sizeof($limitArr) > 0){
			$limitStr = " limit ".implode(" , ",$limitArr);
		}
		$key = implode('_',$keysArr);
		$result = $this->cache->get($key);
		if(!empty($result)){ return $result;}	

		$sSql="SELECT *, FR.status as status FROM `FEATURED_REVIEWS` FR,REVIEWS R $whereClauseStr $orderby $limitStr";
		$result = $this->select($sSql);
		$this->cache->set($key, $result);
		return $result;
	}

	/**
	* @note function is used to get Featured reviews name details
	*
	* @param an integer/comma seperated section ids/ section ids array $section_ids.
	* @param an integer/comma seperated review ids/ review ids array $review_ids.
	* @param an integer/comma seperated type ids/ type ids array $type_ids.
	* @param an integer/comma seperated category ids/ category ids array $category_ids.
	* @param boolean Active/InActive $status.
	* @param integer $startlimit.
	* @param integer $cnt.
	* @param string $orderby.
	*
	* @pre not required.
	*
	* @post featured reviews name details in associative array.
	* retun an array.
	*/
	function arrGetFeaturedReviewsNameDetails($section_ids="",$review_ids="",$type_ids="",$category_ids="",$status="1",$startlimit="",$cnt="",$orderby=""){
		$keysArr[] = $this->reviewkey."_featured_name";
		if(is_array($section_ids)){
			$section_ids = implode(",",$section_ids);
		}
		if(is_array($review_ids)){
			$review_ids = implode(",",$review_ids);
		}
		if(is_array($category_ids)){
			$category_ids = implode(",",$category_ids);
		}
		if(is_array($type_ids)){
			$type_ids = implode(",",$type_ids);
		}
		if($status != ''){
			$whereClauseArr[] = " FR.status=$status";
			$whereClauseArr[] = " R.status=$status";
			$keysArr[] = "status_".$status;
		}
		if($category_ids!=""){
			$whereClauseArr[] = "FR.category_id in ($category_ids)";
			$keysArr[] = "category_id_".$category_ids;
		}
		if($review_ids!=""){
			$whereClauseArr[] = "R.review_id in ($review_ids)";
			$keysArr[] = "review_id_".$review_ids;
		}
		if($section_ids!=""){
			$whereClauseArr[] = " FR.section_review_id in ($section_ids)";
			$keysArr[] = "section_review_id_".$section_ids;
		}
		if($type_ids!=""){
			$whereClauseArr[] = " FR.type_id in ($type_ids)";
			$keysArr[] = "type_id_".$type_ids;
		}
		$whereClauseArr[] = " FR.review_id=R.review_id and PR.review_id=R.review_id ";
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
		}
		if(!empty($startlimit)){
			$limitArr[] = $startlimit;
			$keysArr[] = "startlimit_".$startlimit;
		}
		if(!empty($cnt)){
			$limitArr[] = $cnt;
			$keysArr[] = "cnt_".$cnt;
		}
		if(sizeof($limitArr) > 0){
			$limitStr = " limit ".implode(" , ",$limitArr);
		}
		$key = implode('_',$keysArr);
		//echo $key."<br>"; 
		$result = $this->cache->get($key);
		if(!empty($result)){ return $result;}	

		$sSql="SELECT *,DATE_FORMAT(R.create_date,'%d %b %Y') as disp_date, FR.status as status FROM `FEATURED_REVIEWS` FR,REVIEWS R, PRODUCT_REVIEWS PR $whereClauseStr $orderby $limitStr";
		//echo $sSql."<br/>";
        $result = $this->select($sSql);
		$this->cache->set($key, $result);
		return $result;
	}
	
	/**
	* @note function is used to get related reviews details
	*
	* @param an integer/comma seperated section ids/ section ids array $section_ids.
	* @param an integer/comma seperated review ids/ review ids array $review_ids.
	* @param an integer/comma seperated type ids/ type ids array $type_ids.
	* @param boolean Active/InActive $status.
	* @param integer $startlimit.
	* @param integer $cnt.
	*
	* @pre not required.
	*
	* @post related reviews details in associative array.
	* retun an array.
	*/
	function arrGetRelatedReviewsDetails($section_ids="",$review_ids="",$type_ids="",$status="1",$startlimit="",$cnt=""){
		$keysArr[] = $this->reviewkey."_related";
		if(is_array($section_ids)){
			$section_ids = implode(",",$section_ids);
		}
		if(is_array($review_ids)){
			$review_ids = implode(",",$review_ids);
		}
		if(is_array($category_ids)){
			$category_ids = implode(",",$category_ids);
		}
		if(is_array($type_ids)){
			$type_ids = implode(",",$type_ids);
		}
		if($status != ''){
			$whereClauseArr[] = "R.status=$status";
			$keysArr[] ="status_".$status;
		}
		if($review_ids!=""){
			$whereClauseArr[] = "R.review_id in ($review_ids)";
			$keysArr[] = "review_id_".$review_ids;
		}
		if($category_ids!=""){
			$whereClauseArr[] = "RR.category_id in ($category_ids)";
			$keysArr[] = "category_id_".$category_ids;
		}
		if($section_ids!=""){
			$whereClauseArr[] = " RR.section_review_id in ($section_ids)";
			$keysArr[] = "section_review_id_".$section_ids;
		}
		if($type_ids!=""){
			$whereClauseArr[] = " RR.type_id in ($type_ids)";
			$keysArr[] = "type_id_".$type_ids;
		}
		$whereClauseArr[] = " RR.review_id=R.review_id ";
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
		}
		if(!empty($startlimit)){
			$limitArr[] = $startlimit;
			$keysArr[] = $startlimit;
		}
		if(!empty($cnt)){
			$limitArr[] = $cnt;
			$keysArr[] = $cnt;
		}
		if(sizeof($limitArr) > 0){
			$limitStr = " limit ".implode(" , ",$limitArr);
		}
		$key = implode('_',$keysArr);
		//echo $key."<br>";
		$result = $this->cache->get($key);
		if(!empty($result)){ return $result;}	

		$sSql="SELECT *,DATE_FORMAT(R.create_date,'%d/%m/%Y') as disp_date ,RR.status as status FROM `RELATED_REVIEWS` RR,REVIEWS R $whereClauseStr $orderby $limitStr";
		//echo $sSql;
		$result = $this->select($sSql);
		$this->cache->set($key, $result);
		return $result;
	}
	
	/**
	* @note function is used to get latest expert reviews details
	*
	* @param an integer/comma seperated section ids/ section ids array $section_ids.
	* @param an integer/comma seperated review ids/ review ids array $review_ids.
	* @param an integer/comma seperated type ids/ type ids array $type_ids.
	* @param boolean Active/InActive $status.
	* @param integer $startlimit.
	* @param integer $cnt.
	*
	* @pre not required.
	*
	* @post latest expert reviews details in associative array.
	* retun an array.
	*/
	function arrGetLatestExpertReviewDetails($section_ids="",$review_ids="",$type_ids="",$status="1",$startlimit="",$cnt=""){
		$keysArr[] = $this->reviewexpertkey."_latest";
		if(is_array($section_ids)){
			$section_ids = implode(",",$section_ids);
		}
		if(is_array($review_ids)){
			$review_ids = implode(",",$review_ids);
		}
		if(is_array($type_ids)){
			$type_ids = implode(",",$type_ids);
		}
		if($status != ''){
			$whereClauseArr[] = "R.status=$status";
			$keysArr[] = "status_".$status;
		}
		if($review_ids!=""){
			$whereClauseArr[] = "R.review_id in ($review_ids)";
			$keysArr[] = "review_id_".$review_ids;
		}
		if($section_ids!=""){
			$whereClauseArr[] = " PR.section_review_id in ($section_ids)";
			$keysArr[] = "section_review_id_".$section_ids;
		}
		if($type_ids!=""){
			$whereClauseArr[] = " PR.type_id in ($type_ids)";
			$keysArr[] = "type_id_".$type_ids;
		}
		$whereClauseArr[] = " PR.review_id=R.review_id ";
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
		}
		if(!empty($startlimit)){
			$limitArr[] = $startlimit;
			$keysArr[] = "startlimit_".$startlimit;
		}
		if(!empty($cnt)){
			$limitArr[] = $cnt;
			$keysArr[] = "cnt_".$cnt;
		}
		if(sizeof($limitArr) > 0){
			$limitStr = " limit ".implode(" , ",$limitArr);
		}

		$key = implode('_',$keysArr);
		$result = $this->cache->get($key);
		if(!empty($result)){ return $result;}	

		$sSql="SELECT * FROM `LATEST_EXPERT_REVIEWS` LR,EXPERT_REVIEWS R $whereClauseStr $orderby $limitStr";
		$result = $this->select($sSql);
		$this->cache->set($key, $result);
		return $result;
	}
	
	/**
	* @note function is used to get featured expert reviews details
	*
	* @param an integer/comma seperated section ids/ section ids array $section_ids.
	* @param an integer/comma seperated review ids/ review ids array $review_ids.
	* @param an integer/comma seperated type ids/ type ids array $type_ids.
	* @param boolean Active/InActive $status.
	* @param integer $startlimit.
	* @param integer $cnt.
	*
	* @pre not required.
	*
	* @post latest featured expert reviews details in associative array.
	* retun an array.
	*/
	function arrGetFeaturedExpertReviewDetails($section_ids="",$review_ids="",$type_ids="",$status="1",$startlimit="",$cnt=""){
		$keysArr[] = $this->reviewexpertkey."_featued";
		if(is_array($section_ids)){
			$section_ids = implode(",",$section_ids);
		}
		if(is_array($review_ids)){
			$review_ids = implode(",",$review_ids);
		}
		if(is_array($type_ids)){
			$type_ids = implode(",",$type_ids);
		}
		if($status != ''){
			$whereClauseArr[] = "R.status=$status";
			$keysArr[] = "status_".$status;
		}
		if($review_ids!=""){
			$whereClauseArr[] = "R.review_id in ($review_ids)";
			$keysArr[] = "review_id_".$review_ids;
		}
		if($section_ids!=""){
			$whereClauseArr[] = " FR.section_review_id in ($section_ids)";
			$keysArr[] = "section_review_id_".$section_ids;
		}
		if($type_ids!=""){
			$whereClauseArr[] = " FR.type_id in ($type_ids)";
			$keysArr[] = "type_id_".$type_ids;
		}
		$whereClauseArr[] = " FR.review_id=R.review_id ";
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
		}
		if(!empty($startlimit)){
			$limitArr[] = $startlimit;
			$keysArr[] = "startlimit_".$startlimit;
		}
		if(!empty($cnt)){
			$limitArr[] = $cnt;
			$keysArr[] = "cnt_".$cnt;
		}
		if(sizeof($limitArr) > 0){
			$limitStr = " limit ".implode(" , ",$limitArr);
		}

		$key = implode('_',$keysArr);
		$result = $this->cache->get($key);
		if(!empty($result)){ return $result;}	

		$sSql="SELECT * FROM `FEATURED_EXPERT_REVIEWS` FR,EXPERT_REVIEWS R $whereClauseStr $orderby $limitStr";
		$result = $this->select($sSql);
		$this->cache->set($key, $result);
		return $result;
	}
	
	/**
	* @note function is used to get related expert reviews details
	*
	* @param an integer/comma seperated section ids/ section ids array $section_ids.
	* @param an integer/comma seperated review ids/ review ids array $review_ids.
	* @param an integer/comma seperated type ids/ type ids array $type_ids.
	* @param boolean Active/InActive $status.
	* @param integer $startlimit.
	* @param integer $cnt.
	*
	* @pre not required.
	*
	* @post latest related expert reviews details in associative array.
	* retun an array.
	*/

	function arrGetRelatedExpertReviewDetails($section_ids="",$review_ids="",$type_ids="",$status="1",$startlimit="",$cnt=""){
		$keysArr[] = $this->reviewexpertkey."_related";
		if(is_array($section_ids)){
			$section_ids = implode(",",$section_ids);
		}
		if(is_array($review_ids)){
			$review_ids = implode(",",$review_ids);
		}
		if(is_array($type_ids)){
			$type_ids = implode(",",$type_ids);
		}
		if($status != ''){
			$whereClauseArr[] = "R.status=$status";
			$keysArr[] = "status_".$status;
		}
		if($review_ids!=""){
			$whereClauseArr[] = "R.review_id in ($review_ids)";
			$keysArr[] = "review_id_".$review_ids;
		}
		if($section_ids!=""){
			$whereClauseArr[] = " RR.section_review_id in ($section_ids)";
			$keysArr[] = "section_review_id_".$section_ids;
		}
		if($type_ids!=""){
			$whereClauseArr[] = " RR.type_id in ($type_ids)";
			$keysArr[] = "type_id_".$type_ids;
		}
		$whereClauseArr[] = " RR.review_id=R.review_id ";
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
		}
		if(!empty($startlimit)){
			$limitArr[] = $startlimit;
			$keysArr[] = "startlimit_".$startlimit;
		}
		if(!empty($cnt)){
			$limitArr[] = $cnt;
			$keysArr[] = "cnt_".$cnt;
		}
		if(sizeof($limitArr) > 0){
			$limitStr = " limit ".implode(" , ",$limitArr);
		}
		$key = implode('_',$keysArr);
		$result = $this->cache->get($key);
		if(!empty($result)){ return $result;}	

		$sSql="SELECT * FROM `RELATED_EXPERT_REVIEWS` RR,EXPERT_REVIEWS R $whereClauseStr $orderby $limitStr";
		$result = $this->select($sSql);
		$this->cache->set($key, $result);
		return $result;
	}
	/**
	* @note function is used to get product reviews list details
	*
	* @param an integer/comma seperated  $sreviewsIds.
	* @param an integer $sType.
	* @param a string $sTableName.
	*
	* @post latest product reviews list details in associative array.
	* retun an array.
	*/	
	function getProductReviewsList($sreviewsIds,$sType,$sTableName){
		$sRequest="";
		$aConstraintsArr=$aParamaters;
		$sOrderfield=$aParamaters['orderfield'];
		$sOrder=$aParamaters['order'];
		$iStartlimit=$aParamaters['start'];
		$iCnt=$aParamaters['cnt'];
		$keysArr[] = $this->reviewkey."_list";
		
		if($sreviewsIds!=''){ $keysArr[] = "review_id_".$sreviewsIds; }
		if($sType!=''){ $keysArr[] = "review_type_".$sType; }
		
		$key = implode('_',$keysArr);
		echo $key."<br>";
		$result = $this->cache->get($key);
		if(!empty($result)){ return $result;}	

		$sSql="select * from REVIEWS where ";
		if($sreviewsIds!=''){ $sSql .=" review_id in ($sreviewsIds) "; }
		if($sreviewsIds!='' && $sType!=''){ $sSql .=" and";}
		if($sType!=''){ $sSql .=" review_type=$sType"; }
		echo $sSql;
		$result = $this->select($sSql);
		$this->cache->set($key, $result);
		return $result;
	}
	/**
	 * @note function is used to get reviews video details list
	 *
	 * @param an integer $video_id.
	 * @param an integer $group_id.
	 * @param an integer $product_id.
	 * @param an integer $product_info_id.
	 * @param an integer $category_id.
	 * @param an integer $brand_id.
	 * @param boolean Active/InActive $status.
	 * @param integer $startlimit.
	 * @param integer $cnt.
	 *
	 * @pre not required.
	 *
	 * @post reviews video details in associative array.
	 * retun an array.
	 */	
	function arrGetReviewsVideoDetails($video_id="",$group_id="",$product_id="",$product_info_id="",$category_id="",$brand_id="",$status="1",$startlimit="",$cnt="",$orderby=""){
		$keysArr[] = $this->reviewkey."_video";
        if($status != ''){
		  $whereClauseArr[] = "R.status = $status";
		  $keysArr[] = "status_".$status;
		}
 
		if($group_id!=''){
			$whereClauseArr[] = "PR.group_id = $group_id";
			$keysArr[] = "group_id_".$group_id;
		}
		if($product_id!=''){
			$whereClauseArr[] = "PR.product_id = $product_id";
			$keysArr[] = "product_id_".$product_id;
		}
		if($product_info_id!=""){
			$whereClauseArr[] = " PR.product_info_id = $product_info_id";
			$keysArr[] = "product_info_id_".$product_info_id;
		}
		if($category_id!=""){
			$whereClauseArr[] = " PR.category_id = $category_id";
			$keysArr[] = "category_id_".$category_id;
		}
		if($brand_id!=""){
			$whereClauseArr[] = " PR.brand_id = $brand_id";
			$keysArr[] = "brand_id_".$brand_id;
		}
		if($video_id!=""){
			$whereClauseArr[] = " UMR.upload_media_id=$video_id ";
			$keysArr[] = "upload_media_id_".$video_id;
		}
		$whereClauseArr[] = " R.review_id = PR.review_id ";
		$whereClauseArr[] = " PR.product_review_id = UMR.product_review_id ";
		$whereClauseArr[] = " UMR.content_type=1 ";
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
		}
		//echo $startlimit."STSRATAT";
		if($startlimit!=''){
			$limitArr[] = $startlimit;
			$keysArr[] = "startlimit_".$startlimit;
		}
		if(!empty($cnt)){
			$limitArr[] = $cnt;
			$keysArr[] ="cnt_".$cnt;
		}
		if(sizeof($limitArr) > 0){
			$limitStr = " limit ".implode(" , ",$limitArr);
		}
	
		$key = implode('_',$keysArr);
		//echo $key."<br>";
		$result = $this->cache->get($key);
		if(!empty($result)){ return $result;}

		$sSql="SELECT UMR.upload_media_id as video_id,R.title,R.tags,UMR.media_id,UMR.media_path,UMR.video_img_id,UMR.video_img_path,UMR.content_type,UMR.is_media_process,R.status,UMR.create_date,UMR.update_date,PR.brand_id,PR.product_id,PR.product_info_id FROM REVIEWS R,PRODUCT_REVIEWS PR,UPLOAD_MEDIA_REVIEWS UMR $whereClauseStr group by UMR.upload_media_id $orderby $limitStr ";
		//echo $sSql."<br/>";
		$result = $this->select($sSql);
		$this->cache->set($key, $result);
		return $result;
	}

	function arrGetReviewsVideoDetailsCnt($video_id="",$group_id="",$product_id="",$product_info_id="",$category_id="",$brand_id="",$status="1"){
		$keysArr[] = $this->reviewkey."_video_cnt";
		if($status != ''){
			$whereClauseArr[] = "R.status=$status";
			$keysArr[] = "status_".$status;
		}
		if($group_id!=''){
			$whereClauseArr[] = "PR.group_id = $group_id";
			$keysArr[] = "group_id_".$group_id;
		}
		if($product_id!=''){
			$whereClauseArr[] = "PR.product_id = $product_id";
			$keysArr[] = "product_id_".$product_id;
		}
		if($product_info_id!=""){
			$whereClauseArr[] = " PR.product_info_id = $product_info_id";
			$keysArr[] = "product_info_id_".$product_info_id;
		}
		if($category_id!=""){
			$whereClauseArr[] = " PR.category_id = $category_id";
			$keysArr[] = "category_id_".$category_id;
		}
		if($brand_id!=""){
			$whereClauseArr[] = " PR.brand_id = $brand_id";
			$keysArr[] = "brand_id_".$brand_id;
		}
		if($video_id!=""){
			$whereClauseArr[] = " UMR.upload_media_id = $video_id";
			$keysArr[] = "upload_media_id_".$video_id;
		}
		$whereClauseArr[] = " R.review_id = PR.review_id ";
		$whereClauseArr[] = " PR.product_review_id = UMR.product_review_id ";
		$whereClauseArr[] = " UMR.content_type=1 ";
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
		}

		$key = implode('_',$keysArr);
		///echo $key."<br>";
		$result = $this->cache->get($key);
		if(!empty($result)){ return $result;}

		$sSql="SELECT count(UMR.upload_media_id) as cnt FROM REVIEWS R,PRODUCT_REVIEWS PR,UPLOAD_MEDIA_REVIEWS UMR $whereClauseStr group by UMR.upload_media_id $limitStr";
		//echo $sSql;
		$result = $this->select($sSql);
		$this->cache->set($key, $result);
		return $result;
	}
	/**
	* @note function is used to insert ask expert questions into the database.
	* @param an associative array $insert_param.
	* @param is a string $table_name.
	* @pre $insert_param must be valid associative array.
	* @post an integer $question_id.
	* retun integer.
	*/
	function intInsertAskExpert($insert_param,$table_name){
		$insert_param['create_date'] = date('Y-m-d H:i:s');
		$insert_param['update_date'] = date('Y-m-d H:i:s');
		$sql = $this->getInsertUpdateSql($table_name,array_keys($insert_param),array_values($insert_param));
		//echo $sql;
		$result=$this->insertUpdate($sql);
		$this->cache->searchDeleteKeys("ask_expert");
		return $result;
     }
	/**
	* @note function is used to get question details list
	*
	* @param an integer/comma seperated question ids/ question ids array $question_ids.
	* @param an integer/comma seperated email ids/ email ids array $email_ids.
	* @param integer $startlimit.
	* @param integer $cnt.
	* @param string $orderby.
	*
	* @pre not required.
	*
	* @post question details in associative array.
	* retun an array.
	*/
	function arrGetQuestionDetails($question_ids="",$email_ids="",$startlimit="",$cnt="",$orderby=""){
	
		$keysArr[] = "ask_expert";
		if(is_array($question_ids)){
			$question_ids = implode(",",$question_ids);
		}
		if(is_array($email_ids)){
			$email_ids = implode(",",$email_ids);
		}
		if($question_ids != ""){
			$whereClauseArr[] = "question_id in ($question_ids)";
			$keysArr[] = "question_id_".$question_ids;
		}

		if($email_ids != ""){
			$whereClauseArr[] = "email_id in ($email_ids)";
			$keysArr[] = "email_id_".$email_ids;
		}
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
		}
		if($orderby !=""){
			$orderby ="order by ".$orderby." DESC";
			$keysArr[] = "order_".str_replace(" ","_",$orderby);
		}
		if(!empty($startlimit)){
			$limitArr[] = $startlimit;
			$keysArr[] = "startlimit_".$startlimit;
		}
		if(!empty($cnt)){
			$limitArr[] = $cnt;
			$keysArr[] = "cnt_".$cnt;
		}
		if(sizeof($limitArr) > 0){
			$limitStr = " limit ".implode(" , ",$limitArr);
		}

		$key = implode('_',$keysArr);
		//echo $key."<br>";
		$result = $this->cache->get($key);
		if(!empty($result)){ return $result;}

		$sSql="SELECT * from ASK_EXPERT $whereClauseStr $orderby $limitStr";
		//echo $sSql;
		$result = $this->select($sSql);
		$this->cache->set($key, $result);
		return $result;
    }
}
?>