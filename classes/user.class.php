<?php
/**
* @brief class is used add,update,delete,get user details.
* @author Rajesh Ujade
* @version 1.0
* @created 11-Nov-2010 5:09:31 PM
*/
class user extends DbOperation{
	var $cache;
	var $userkey;
	var $editorkey;
	/**Initialize the consturctor.*/
	function user(){
		$this->cache = new Cache;
		$this->userkey = MEMCACHE_MASTER_KEY."user";
		$this->editorkey = MEMCACHE_MASTER_KEY."editor";
	}
	/**
	* @note function is used to insert the user details into the database.
	* @param an associative array $insert_param.
	* @pre $insert_param must be valid associative array.
	* @post an integer $user_profile_id.
	* retun integer.
	*/

	function intInsertUserDetail($insert_param){
		$insert_param['create_date'] = date('Y-m-d H:i:s');
		$sSql = $this->getInsertUpdateSql("USER_INFO",array_keys($insert_param),array_values($insert_param));
		$user_profile_id = $this->insertUpdate($sSql);
		$this->cache->searchDeleteKeys($this->userkey);
		return $user_profile_id;
	}
	/**
	* @note function is used to update the user info into the database.
	* @param an associative array $update_param.
	* @param an integer $user_profile_id.
	* @pre $update_param must be valid associative array and $user_profile_id must be non-empty/zero valid integer.
	* @post boolean true/false.
	* retun boolean.
	*/
	function intUpdateUserDetail($user_profile_id,$update_param){
		$update_param['update_date'] = date('Y-m-d H:i:s');
		$sql = $this->getUpdateSql("USER_INFO",array_keys($update_param),array_values($update_param),"user_profile_id",$user_profile_id);
		$isUpdate = $this->update($sql);
		$this->cache->searchDeleteKeys($this->userkey);
		return $isUpdate;
	}
	/**
	* @note function is used to insert the editor details into the database.
	* @param an associative array $insert_param.
	* @pre $insert_param must be valid associative array.
	* @post an integer $editor_id.
	* retun integer.
	*/
	function intInsertEditorDetails($insert_param){
		$insert_param['create_date'] = date('Y-m-d H:i:s');
		$insert_param['update_date'] = date('Y-m-d H:i:s');
		$sSql = $this->getInsertUpdateSql("EDITOR_INFO",array_keys($insert_param),array_values($insert_param));
		$editor_id = $this->insertUpdate($sSql);
		$this->cache->searchDeleteKeys($this->editorkey);
		return $editor_id;
	}
	/**
	* @note function is used to update the editor information in the database.
	* @param $update_param is an associative array
	* @pre $update_param must be valid associative array.
	* @post an $editor_id.
	* retun integer.
	*/
	function boolUpdateEditorDetails($update_param){
		$update_param['create_date'] = date('Y-m-d H:i:s');
		$update_param['update_date'] = date('Y-m-d H:i:s');
		$sSql = $this->getInsertUpdateSql("EDITOR_INFO",array_keys($update_param),array_values($update_param));
		$res = $this->insertUpdate($sSql);
		$this->cache->searchDeleteKeys($this->editorkey);
		return $res;
	}
	/**
	* @note function is used  delete editor info
	*
	* @param editor_id ,table_name	
	* @pre  editor id is single editor id of integer type
	* @pre  table_name is database table name 
	* @post return true if successful , false if error occurs
	*/
	function booldeleteEditorInfo($editor_id="",$table_name="EDITOR_INFO"){
		$sSql="delete from $table_name where editor_id = $editor_id";
		$iRes=$this->sql_delete_data($sSql);
		$this->cache->searchDeleteKeys($this->editorkey);
		return $iRes;
	}
	/**
	* @note function is used to get editor details
	*
	* @param an integer $editor_id
	* @param a string $editor_name
	* @param integer $startlimit.
	* @param integer $cnt.
	* @pre not required. 
	*
	* @post associative array.
	* @post return array if successful , 0 if error occurs
	* 
	*/
	function arrGetEditorDetails($editor_id,$editor_name="",$startlimit="",$cnt="",$status="1"){
		$keysArr[] = $this->editorkey."_detail";
		if($editor_id != ''){
			$whereClauseArr[] = " editor_id=$editor_id ";
			$keysArr[] ="editor_id_".$editor_id; 
		}
		if($editor_name != ''){
			$whereClauseArr[] = " editor_name=$editor_name ";
			$keysArr[] ="editor_name_".$editor_name; 
		}
		if($status!=""){
			$whereClauseArr[] = "status=$status";
			$keysArr[] ="status_".$status; 
		}
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
		}
		if(!empty($startlimit)){
			$limitArr[] = $startlimit;
			$keysArr[] ="startlimit_".$startlimit;
		}
		if(!empty($cnt)){
			$limitArr[] = $cnt;
			$keysArr[] ="cnt_".$cnt;
		}
		if(sizeof($limitArr) > 0){
			$limitStr = " limit ".implode(" , ",$limitArr);
		}
		$key = implode('_',$keysArr);
		//echo $key."<br>";
		$result = $this->cache->get($key);if(!empty($result)){ return $result;}
		$sSql="SELECT * FROM EDITOR_INFO $whereClauseStr $limitStr";
		$result = $this->select($sSql);
		$this->cache->set($key, $result);
		return $result;
	}
	/**
	* @note function is used to get editor info details
	*
	* @param an integer $editor_id
	* @param a string $editor_name
	* @param integer $startlimit.
	* @param integer $cnt.
	* @pre not required. 
	*
	* @post associative array.
	* @post return array if successful , 0 if error occurs
	* 
	*/
	function arrGetEditorInfoDetails($editor_ids="",$category_ids="",$editor_name="",$designation="",$phone_no="",$status="1",$startlimit="",$cnt="",$orderby=""){
		$keysArr[] = $this->editorkey."_infodetail";
		if(is_array($editor_ids)){
			$editor_ids=implode(",",$editor_ids);
		}	
		if(is_array($category_ids)){
			$category_ids=implode(",",$category_ids);
		}	
		if($editor_ids != ''){
			$whereClauseArr[] = " editor_id in ($editor_ids) ";
			$keysArr[] ="editor_id_".$editor_ids;
		}
		if($category_ids!=""){
			$whereClauseArr[] = "category_id in ($category_ids)";
			$keysArr[] ="category_id_".$category_ids;
		}
		if($editor_name != ''){
			$whereClauseArr[] = " editor_name=$editor_name ";
			$keysArr[] ="editor_name_".$editor_name;
		}
		if($designation != ''){
			$whereClauseArr[] = " designation=$designation ";
			$keysArr[] ="designation_".$designation;
		}
		if($phone_no != ''){
			$whereClauseArr[] = " phone_no=$phone_no ";
			$keysArr[] ="phone_no_".$phone_no;
		}
		if($status!=""){
			$whereClauseArr[] = "status=$status";
			$keysArr[] ="status_".$status;
		}
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
		}
		if(!empty($startlimit)){
			$limitArr[] = $startlimit;
			$keysArr[] ="startlimit_".$startlimit;
		}
		if(!empty($cnt)){
			$limitArr[] = $cnt;
			$keysArr[] ="cnt_".$cnt;
		}
		if(sizeof($limitArr) > 0){
			$limitStr = " limit ".implode(" , ",$limitArr);
		}
		if($orderby == ""){
			$orderby = "order by create_date DESC";
			$keysArr[] ="order_".str_replace(" ","_",$orderby);

		}
		$key = implode('_',$keysArr);
		//echo $key."<br>";
		$result = $this->cache->get($key);if(!empty($result)){ return $result;}	
		$sSql="SELECT *,DATE_FORMAT(create_date,'%d/%m/%Y') as disp_date FROM EDITOR_INFO $whereClauseStr $orderby $limitStr";
		$result = $this->select($sSql);
		$this->cache->set($key, $result);
		return $result;
	}
	/**
	* @note function is used to get user info details
	*
	* @param an array/a string/an integer $user_profile_ids.
	* @param an array/a string/an integer $category_ids.
	* @param an array/a string/an integer $brand_ids.
	* @param an array/a string/an integer $product_info_ids.
	* @param an array/a string/an integer $product_ids.
	* @param an array/a string/an integer $city_ids.
	* @param an array/a string/an integer $profile_name.
	* @param an integer $status.
	* @param integer $startlimit.
	* @param integer $cnt.
	* @pre not required. 
	*
	* @post associative array.
	* @post return array if successful , 0 if error occurs
	* 
	*/
	function arrGetUserInfoDetails($user_profile_ids="",$category_ids="",$brand_ids="",$product_info_ids="",$product_ids="",$city_ids="",$profile_name="",$status="1",$startlimit="",$cnt=""){
		$keysArr[] = $this->userkey."_infodetail";
		if(is_array($user_profile_ids)){
			$user_profile_ids = implode(",",$user_profile_ids);
		}
		if(is_array($category_ids)){
			$category_ids = implode(",",$category_ids);
		}
		if(is_array($brand_ids)){
			$brand_ids = implode(",",$brand_ids);
		}
		if(is_array($product_info_ids)){
			$product_info_ids = implode(",",$product_info_ids);
		}
		if(is_array($product_ids)){
			$product_ids = implode(",",$product_ids);
		}
		if(is_array($city_ids)){
			$city_ids = implode(",",$city_ids);
		}
		if($user_profile_ids!=""){
			$whereClauseArr[] = "user_profile_id in ($user_profile_ids)";
			$keysArr[] ="user_profile_id_".$user_profile_ids; 
		}
		if($category_ids!=""){
			$whereClauseArr[] = "category_id in ($category_ids)";
			$keysArr[] ="category_id_".$category_ids;
		}
		if($brand_ids!=""){
			$whereClauseArr[] = "brand_id in ($brand_ids)";
			$keysArr[] ="brand_id_".$brand_ids;
		}
		if($product_info_ids!=""){
			$whereClauseArr[] = "product_info_id in ($product_info_ids)";
			$keysArr[] ="product_info_id_".$product_info_ids;
		}
		if($product_ids!=""){
			$whereClauseArr[] = "product_id in ($product_ids)";
			$keysArr[] ="product_id_".$product_ids;
		}
		if($city_ids!=""){
			$whereClauseArr[] = "city_id in ($city_ids)";
			$keysArr[] ="city_id_".$city_ids;
		}
		if($profile_name!=""){
			$whereClauseArr[] = "profile_name in ($profile_name)";
			$keysArr[] ="profile_name_".$profile_name;
		}
		if($status != ''){
			$whereClauseArr[] = "status=$status";
			$keysArr[] ="status_".$status;
		}
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
		}
		if(!empty($startlimit)){
			$limitArr[] = $startlimit;
			$keysArr[] ="startlimit_".$startlimit;
		}
		if(!empty($cnt)){
			$limitArr[] = $cnt;
			$keysArr[] ="cnt_".$cnt;
		}
		if(sizeof($limitArr) > 0){
			$limitStr = " limit ".implode(" , ",$limitArr);
		}
		$orderby = "order by create_date DESC ";
		
		$key = implode('_',$keysArr);
		//echo $key."<br>";
		$result = $this->cache->get($key);if(!empty($result)){ return $result;}	
		$sSql="SELECT * from USER_INFO $whereClauseStr $orderby $limitStr";
		$result =$this->select($sSql);
		$this->cache->set($key, $result);
		return $result;
	}
	/**
	* @note function is used to get user info count 
	*
	* @param an array/a string/an integer $user_profile_ids.
	* @param an array/a string/an integer $category_ids.
	* @param an array/a string/an integer $brand_ids.
	* @param an array/a string/an integer $product_info_ids.
	* @param an array/a string/an integer $product_ids.
	* @param an array/a string/an integer $city_ids.
	* @param an array/a string/an integer $profile_name.
	* @param an integer $status.
	* @param integer $startlimit.
	* @param integer $cnt.
	* @pre not required. 
	*
	* @post associative array.
	* @post return array if successful , 0 if error occurs
	* 
	*/
	function getUserInfoCount($user_profile_ids="",$category_ids="",$brand_ids="",$product_info_ids="",$product_ids="",$city_ids="",$profile_name="",$status="1",$startlimit="",$cnt=""){
		$keysArr[] = $this->userkey."_info_count";
		if(is_array($user_profile_ids)){
			$user_profile_ids = implode(",",$user_profile_ids);
		}
		if(is_array($category_ids)){
			$category_ids = implode(",",$category_ids);
		}
		if(is_array($brand_ids)){
			$brand_ids = implode(",",$brand_ids);
		}
		if(is_array($product_info_ids)){
			$product_info_ids = implode(",",$product_info_ids);
		}
		if(is_array($product_ids)){
			$product_ids = implode(",",$product_ids);
		}
		if(is_array($city_ids)){
			$city_ids = implode(",",$city_ids);
		}
		if($user_profile_ids!=""){
			$whereClauseArr[] = "user_profile_id in ($user_profile_ids)";
			$keysArr[] ="user_profile_id_".$user_profile_ids; 
		}
		if($category_ids!=""){
			$whereClauseArr[] = "category_id in ($category_ids)";
			$keysArr[] ="category_id_".$category_ids;
		}
		if($brand_ids!=""){
			$whereClauseArr[] = "brand_id in ($brand_ids)";
			$keysArr[] ="brand_id_".$brand_ids;
		}
		if($product_info_ids!=""){
			$whereClauseArr[] = "product_info_id in ($product_info_ids)";
			$keysArr[] ="product_info_id_".$product_info_ids;
		}
		if($product_ids!=""){
			$whereClauseArr[] = "product_id in ($product_ids)";
			$keysArr[] ="product_id_".$product_ids;
		}
		if($city_ids!=""){
			$whereClauseArr[] = "city_id in ($city_ids)";
			$keysArr[] ="city_id_".$city_ids;
		}
		if($profile_name!=""){
			$whereClauseArr[] = "profile_name in ($profile_name)";
			$keysArr[] ="profile_name_".$profile_name;
		}
		if($status != ''){
			$whereClauseArr[] = "status=$status";
			$keysArr[] ="status_".$status;
		}
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
		}
		if(!empty($startlimit)){
			$limitArr[] = $startlimit;
			$keysArr[] ="startlimit_".$startlimit;
		}
		if(!empty($cnt)){
			$limitArr[] = $cnt;
			$keysArr[] ="cnt_".$cnt;
		}
		if(sizeof($limitArr) > 0){
			$limitStr = " limit ".implode(" , ",$limitArr);
		}
		$orderby = "order by create_date DESC ";
		$key = implode('_',$keysArr);
		//echo $key."<br>";
		$result = $this->cache->get($key);if(!empty($result)){ return $result[0]['cnt'];}	
		$sSql="SELECT count(user_profile_id) as cnt from USER_INFO $whereClauseStr $orderby $limitStr";
		$result = $this->select($sSql);
		$this->cache->set($key, $result);
		$count=$result[0]['cnt'];
		return $count;
	}
}
?>