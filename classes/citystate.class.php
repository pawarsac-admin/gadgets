<?php
	/**
	* @brief class is used to perform actions on state and city information
	* @author Sachin
	* @version 1.0
	* @created 11-Nov-2010 05:09:31 PM 
	* @last updated on 09-Mar-2011 13:14:00 PM
	*/	
	class citystate extends DbOperation{
		var $cache;
		var $citystateKey;
		/**Intialize the consturctor.*/
		function citystate(){
			$this->cache = new Cache;
			$this->citystateKey = MEMCACHE_MASTER_KEY."citystate";
		}
		/**
		* @note function is used to insert/update the state information into the database.
		* @param an associative array $insert_param.
		* @pre $insert_param must be valid associative array.
		* @post an integer $state_id.
		* retun integer.
		*/		
		function intInsertUpdateState($insert_param){
			$insert_param['create_date'] = date('Y-m-d H:i:s');
			$insert_param['update_date'] = date('Y-m-d H:i:s');
			$sql = $this->getInsertUpdateSql("STATE_MASTER",array_keys($insert_param),array_values($insert_param));
			$state_id = $this->insert($sql);
			if($state_id == 'Duplicate entry'){ return 'exists';}
			$this->cache->searchDeleteKeys($this->citystateKey);	
			return $state_id; 
		}
		function intUpdateState($state_id,$update_param){
			$insert_param['update_date'] = date('Y-m-d H:i:s');
			$sql = $this->getUpdateSql("STATE_MASTER",array_keys($update_param),array_values($update_param),'state_id',$state_id);
			$isUpdate = $this->update($sql);
			$this->cache->searchDeleteKeys($this->citystateKey);
			return $isUpdate;
		}
		/**
		* @note function is used to delete state.
		* @param integer $state_id
		* @pre $state_id must be non-empty/zero valid integer.
		* @post boolean true/false.
		* return boolean.
		*/			
		function boolDeleteState($state_id){
			$sql = "delete from STATE_MASTER where state_id = $state_id";
			$result = $this->sql_delete_data($sql);
			$this->cache->searchDeleteKeys($this->citystateKey);
			return $result;
		}
		/**
		* @note function is used to insert/update the city information into the database.
		* @param an associative array $insert_param.
		* @pre $insert_param must be valid associative array.
		* @post an integer $city_id.
		* retun integer.
		*/
		function intInsertUpdateCity($insert_param){
			$insert_param['create_date'] = date('Y-m-d H:i:s');
			$insert_param['update_date'] = date('Y-m-d H:i:s');
			$sql = $this->getInsertUpdateSql("CITY_MASTER",array_keys($insert_param),array_values($insert_param));
			$city_id = $this->insert($sql);
			if($city_id == 'Duplicate entry'){ return 'exists';}
			$this->cache->searchDeleteKeys($this->citystateKey);
			return $city_id;
		}
		/**
		* @note function is used to delete city.
		* @param integer $city_id.
		* @pre $city_id must be non-empty/zero valid integer.
		* @post boolean true/false.
		* return boolean.
		*/
		function boolDeleteCity($city_id){
			$sql = "delete from CITY_MASTER where city_id = $city_id";
			$result = $this->sql_delete_data($sql);
			$this->cache->searchDeleteKeys($this->citystateKey);
			return $result;
		}
		/**
		* @note function is used to get country details 
		*
		* @param an integer/comma seperated country ids $country_id.
		* @param boolean Active/InActive $status.
		* @param integer $startlimit.
		* @param integer $cnt.
		*
		* @pre not required.
		*
		* @post country details in associative array.
		* retun an array.
		*/
		function arrGetCountryDetails($country_id="",$status="1",$startlimit="",$cnt=""){
			$keyArr[] = $this->citystateKey."_country";
			if(!empty($country_id)){
				$keyArr[] = "country_id_$country_id";
				$whereClauseArr[] = "country_id in ($country_id)";
			}
			if(!empty($startlimit)){
				$keyArr[] = "startlimit_$startlimit";
				$limitArr[] = $startlimit;
			}
			if(!empty($cnt)){
				$keyArr[] = "cnt_$cnt";
				$limitArr[] = $cnt;
			}
			if(sizeof($whereClauseArr) > 0){
				$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
			}
			if(sizeof($limitArr) > 0){
				$limitStr = " limit ".implode(",",$limitArr);
			}
			$key = implode("_",$keyArr);
			if($result = $this->cache->get($key)){return $result;}
			$sql = "select * from COUNTRY_MASTER $whereClauseStr order by country_name asc $limitStr";
			$result = $this->select($sql);
			$this->cache->set($key,$result);
			return $result;
		}
		/**
		* @note function is used to get state details 
		*
		* @param an integer/comma seperated state ids $state_id.
		* @param an integer/comma seperated country ids $country_id.
		* @param boolean Active/InActive $status.
		* @param integer $startlimit.
		* @param integer $cnt.
		*
		* @pre not required.
		*
		* @post state details in associative array.
		* retun an array.
		*/	
		function arrGetStateDetails($state_id="",$country_id="",$status="1",$startlimit="",$cnt=""){
			$keyArr[] = $this->citystateKey."_state";
			if(!empty($state_id)){
				$keyArr[] = "state_id_$state_id";
				$whereClauseArr[] = "state_id in ($state_id)";
			}
			if(!empty($country_id)){
				$keyArr[] = "country_id_$country_id";
				$whereClauseArr[] = "country_id in ($country_id)";
			}
			if(!empty($startlimit)){
				$keyArr[] = "startlimit_$startlimit";
				$limitArr[] = $startlimit;
			}
			if(!empty($cnt)){
				$keyArr[] = "cnt_$cnt";
				$limitArr[] = $cnt;
			}
			if($status != '')
			{
				$keyArr[] = "state_status_$status";
				$whereClauseArr[] = "state_status=$status";
			}
			if(sizeof($whereClauseArr) > 0){
				$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
			}
			if(sizeof($limitArr) > 0){
				$limitStr = " limit ".implode(",",$limitArr);
			}
			$key = implode("_",$keyArr);
			if($result = $this->cache->get($key)){return $result;}
			$sql = "select * from STATE_MASTER $whereClauseStr order by state_name asc $limitStr";
			$result = $this->select($sql);
			$this->cache->set($key,$result);
			return $result;
		}
		/**
		* @note function is used to get city details 
		*
		* @param an integer/comma seperated city ids $city_id.
		* @param an integer/comma seperated state ids $state_id.
		* @param boolean Active/InActive $status.
		* @param integer $startlimit.
		* @param integer $cnt.
		*
		* @pre not required.
		*
		* @post city details in associative array.
		* retun an array.
		*/
		function arrGetCityDetails($city_id="",$state_id="",$status="1",$startlimit="",$cnt=""){
			$keyArr[] = $this->citystateKey."_city";
			if(is_array($city_id)){
				$city_id = implode(",",$city_id);
			}
			if(!empty($state_id)){
				$keyArr[] = "state_id_$state_id";
				$whereClauseArr[] = "state_id in ($state_id)";
			}
			if(!empty($city_id)){
				$keyArr[] = "city_id_$city_id";
				$whereClauseArr[] = "city_id in ($city_id)";
			}
			if(!empty($startlimit)){
				$keyArr[] = "startlimit_$startlimit";
				$limitArr[] = $startlimit;
			}
			if(!empty($cnt)){
				$keyArr[] = "cnt_$cnt";
				$limitArr[] = $cnt;
			}
			if($status != '')
			{
				$keyArr[] = "city_status_$status";
				$whereClauseArr[] = "city_status=$status";
			}
			if(sizeof($whereClauseArr) > 0){
				$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
			}
			if(sizeof($limitArr) > 0){
				$limitStr = " limit ".implode(",",$limitArr);
			}
			$key = implode("_",$keyArr);
			if($result = $this->cache->get($key)){return $result;}
			$sql = "select * from CITY_MASTER $whereClauseStr order by city_name asc $limitStr";
			$result = $this->select($sql);
			$this->cache->set($key,$result);
			return $result;
		}
		/**
		* @note function used to get city and state details.
		* @param string $city_name.
		* @param integer $city_id.
		* @param integer $state_id.
		* @param boolean $status.
		* @param integer $startlimit.
		* @param integer $cnt.
		* @pre not required.
		* @post array details with city and state.
		*/
		function arrGetCityStateDetails($city_name="",$city_id="",$state_id="",$status="1",$startlimit="",$cnt=""){
			$keyArr[] = $this->citystateKey;
			if(is_array($city_id)){
				$city_id = implode(",",$city_id);
			}
			$whereClauseArr[] = " CITY_MASTER.state_id = STATE_MASTER.state_id ";
			if(!empty($state_id)){
				$keyArr[] = "state_id_$state_id";
				$whereClauseArr[] = "CITY_MASTER.state_id in ($state_id)";
			}
			if(!empty($city_name)){
				$keyArr[] = "city_name_$city_name";
				$whereClauseArr[] = " LOWER(CITY_MASTER.city_name) = '".$city_name."'";
			}
			if(!empty($city_id)){
				$keyArr[] = "city_id_$city_id";
				$whereClauseArr[] = "city_id in ($city_id)";
			}
			if(!empty($startlimit)){
				$keyArr[] = "startlimit_$startlimit";
				$limitArr[] = $startlimit;
			}
			if(!empty($cnt)){
				$keyArr[] = "cnt_$cnt";
				$limitArr[] = $cnt;
			}
			if($status != '')
			{
				$keyArr[] = "city_status_$status";
				$whereClauseArr[] = "city_status=$status";
			}
			if(sizeof($whereClauseArr) > 0){
				$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
			}
			if(sizeof($limitArr) > 0){
				$limitStr = " limit ".implode(",",$limitArr);
			}
			$key = implode("_",$keyArr);
			if($result = $this->cache->get($key)){return $result;}
			$sql = "select * from CITY_MASTER,STATE_MASTER $whereClauseStr order by city_name asc $limitStr";
			$result = $this->select($sql);
			$this->cache->set($key,$result);		
			return $result;
		}
		/**
                * @note function is used to insert/update the related city information into the database.
                * @param an associative array $insert_param.
                * @pre $insert_param must be valid associative array.
                * @post an integer $related_id.
                * retun integer.
                */              
                function intInsertUpdateRelatedCity($insert_param){
                        $insert_param['create_date'] = date('Y-m-d H:i:s');
                        $insert_param['update_date'] = date('Y-m-d H:i:s');
                        $sql = $this->getInsertUpdateSql("RELATED_CITY",array_keys($insert_param),array_values($insert_param));
			//echo $sql;
                        $related_id = $this->insert($sql);
                        if($related_id == 'Duplicate entry'){ return 'exists';}
                        $this->cache->searchDeleteKeys($this->citystateKey."_related_city");    
                        return $related_id; 
                }
		/**
                * @note function is used to delete related city.
                * @param integer $related_id
                * @pre $related_id must be non-empty/zero valid integer.
                * @post boolean true/false.
                * return boolean.
                */
                function boolDeleteRelatedCity($related_id){
                        $sql = "delete from RELATED_CITY where related_id = $related_id";
                        $result = $this->sql_delete_data($sql);
                        $this->cache->searchDeleteKeys($this->citystateKey."_related_city");
                        return $result;
                }
		/**
                * @note function is used to get related city details 
                *
                * @param an integer/comma seperated related ids $related_ids.
		* @param an integer/comma seperated city ids $city_ids.
		* @param an integer/comma seperated related city ids $related_city_ids.
                * @param an integer/comma seperated category ids $category_ids.
                * @param boolean Active/InActive $status.
                * @param integer $startlimit.
                * @param integer $cnt.
                *
                * @pre not required.
                *
                * @post city details in associative array.
                * retun an array.
                */
                function arrGetRelatedCityDetails($related_ids="",$city_ids="",$related_city_ids="",$category_ids="",$status="1",$startlimit="",$cnt="",$orderby=""){
                        $keyArr[] = $this->citystateKey."_related_city";
			if(is_array($related_ids)){
                                $related_ids = implode(",",$related_ids);
                        }
                        if(is_array($city_ids)){
                                $city_ids = implode(",",$city_ids);
                        }
			if(is_array($related_city_ids)){
                                $related_city_ids = implode(",",$related_city_ids);
                        }
			if(is_array($category_ids)){
                                $category_ids = implode(",",$category_ids);
                        }
                        if(!empty($related_ids)){
                                $keyArr[] = "related_id_$related_ids";
                                $whereClauseArr[] = "related_id in ($related_ids)";
                        }
                        if(!empty($city_ids)){
                                $keyArr[] = "city_id_$city_ids";
                                $whereClauseArr[] = "city_id in ($city_ids)";
                        }
			if(!empty($related_city_ids)){
                                $keyArr[] = "related_city_id_$related_city_ids";
                                $whereClauseArr[] = "related_city_id in ($related_city_ids)";
                        }
			if(!empty($category_ids)){
                                $keyArr[] = "category_id_$category_id";
                                $whereClauseArr[] = "category_id in ($category_ids)";
                        }
			if(!empty($status)){
                                $keyArr[] = "status_$status";
                                $whereClauseArr[] = "status=$status";
                        }
			if(!empty($startlimit)){
                                $keyArr[] = "startlimit_$startlimit";
                                $limitArr[] = $startlimit;
                        }
                        if(!empty($cnt)){
                                $keyArr[] = "cnt_$cnt";
                                $limitArr[] = $cnt;
                        }
                        
                        if(sizeof($whereClauseArr) > 0){
                                $whereClauseStr = " where ".implode(" and ",$whereClauseArr);
                        }
                        if(sizeof($limitArr) > 0){
                                $limitStr = " limit ".implode(",",$limitArr);
                        }
			if(empty($orderby)){
                                $orderby = "order by create_date desc";
                        }
                        $key = implode("_",$keyArr);
                        if($result = $this->cache->get($key)){return $result;}
                        $sql = "select * from RELATED_CITY $whereClauseStr $orderby $limitStr";
			//echo $sql;
                        $result = $this->select($sql);
                        $this->cache->set($key,$result);
                        return $result;
                }
	}
?>
