<?php
	/**
	 * @brief class is used to add,update,delete,get menu details.
	 * @author Rajesh Ujade
	 * @version 1.0
	 * @created 11-Nov-2010 6:29:23 PM
	 * @last updated on 09-Mar-2011 13:14:00 PM
	*/
	class MenuManagement extends DbOperation
	{

		/**Initialize the constructor.*/
		function MenuManagement()
		{
		}
		/**
		* @note function is used to insert the header menu details into the database.
		* @param an associative array $insert_param.
		* @pre $insert_param must be valid associative array.
		* @post integer menu id.
		* return integer.
		*/
		function intInsertHeaderMenu($insert_param){
			$insert_param['create_date'] = date('Y-m-d H:i:s');
			$insert_param['update_date'] = date('Y-m-d H:i:s');
			$sql = $this->getInsertSql("HEADER_MENU_MASTER",array_keys($insert_param),array_values($insert_param));
			$menu_id = $this->insert($sql);
			if($menu_id == 'Duplicate entry'){ return 'exists';}
			return $menu_id;
		}
		/**
                * @note function is used to insert the footer menu details into the database.
                * @param an associative array $insert_param.
                * @pre $insert_param must be valid associative array.
                * @post integer menu id.
                * return integer.
                */
		function intInsertFooterMenu($insert_param){
                        $insert_param['create_date'] = date('Y-m-d H:i:s');
                        $insert_param['update_date'] = date('Y-m-d H:i:s');
                        $sql = $this->getInsertSql("FOOTER_MENU_MASTER",array_keys($insert_param),array_values($insert_param));
                        $menu_id = $this->insert($sql);
                        if($menu_id == 'Duplicate entry'){ return 'exists';}
                        return $menu_id;
                }
		/**
		* @note function is used to update the header menu information.
		* @param integer $menu_id.
		* @param an associative array $update_param.
		* @pre $menu_id must be valid non empty/zero integer value and $update_param must be valid associative array.
		* @post boolean true/false.
		* return boolean.
		*/
		function boolUpdateHeaderMenu($menu_id,$update_param){
			$insert_param['update_date'] = date('Y-m-d H:i:s');
			$sql = $this->getUpdateSql("HEADER_MENU_MASTER",array_keys($update_param),array_values($update_param),"menu_id",$menu_id);
			$isUpdate = $this->update($sql);
			return $isUpdate;
		}
		/**
                * @note function is used to update the footer menu information.
                * @param integer $menu_id.
                * @param an associative array $update_param.
                * @pre $menu_id must be valid non empty/zero integer value and $update_param must be valid associative array.
                * @post boolean true/false.
                * return boolean.
                */
		function boolUpdateFooterMenu($menu_id,$update_param){
                        $insert_param['update_date'] = date('Y-m-d H:i:s');
                        $sql = $this->getUpdateSql("FOOTER_MENU_MASTER",array_keys($update_param),array_values($update_param),"menu_id",$menu_id);
                        $isUpdate = $this->update($sql);
                        return $isUpdate;
                }
		/**
		* @note function is use to delete the header menu.
		* @param integer $menu_id.
		* @pre $menu_id must be valid non empty/zero integer value.
		* @post boolean true/false.
		* return boolean.
		*/
		function boolDeleteHeaderMenu($menu_id){
			$sql = "delete from HEADER_MENU_MASTER where menu_id = $menu_id";
			$isDelete = $this->sql_delete_data($sql);
			return $isDelete;
		}
		/**
                * @note function is use to delete the footer menu.
                * @param integer $menu_id.
                * @pre $menu_id must be valid non empty/zero integer value.
                * @post boolean true/false.
                * return boolean.
                */
		function boolDeleteFooterMenu($menu_id){
                        $sql = "delete from FOOTER_MENU_MASTER where menu_id = $menu_id";
                        $isDelete = $this->sql_delete_data($sql);
                        return $isDelete;
                }
		/**
		* @note function is used to get header menu details.
		* @param integer $menu_id.
		* @param integer $menu_level i.e. 0 is used for root(1st) level menu and its decending menu is child of the root menu.
		* @param boolean Active/InActive $status.
		* @param integer $startlimit.
		* @param integer $count.
		* @pre not required.
		* @post an associative array of header menu details.
		* return menu details.
		*/
		function arrGetHeaderMenuDetails($menu_id="", $menu_level="", $category_id="", $menu_page="", $title="", $status="",$startlimit="",$count=""){
			if(!empty($status)){
				$whereClauseArr[] = "$status in ($status)";
			}
			if(!empty($menu_id)){
				$whereClauseArr[] = "menu_id in ($menu_id)";
			}
			if($menu_level != ""){
				$whereClauseArr[] = "menu_level = $menu_level";
			}
			if($category_id != ""){
                                $whereClauseArr[] = "category_id = $category_id";
                        }
			if($menu_page != ""){
				$whereClauseArr[] = "menu_page = $menu_page";
			}
			if($title != ""){
				$whereClauseArr[] = "title = $title";
			}
			if(sizeof($whereClauseArr) > 0){
				$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
			}
			if(!empty($startlimit)){
				$limitArr[] = $startlimit;
			}
			if(!empty($count)){
				$limitArr[] = $count;
			}
			if(sizeof($limitArr) > 0){
				$limitStr = " limit ".implode(" , ",$limitArr);
			}
			$sql = "select * from HEADER_MENU_MASTER $whereClauseStr $limitStr";
			$result = $this->select($sql);
			return $result;
		}
		/**
                * @note function is used to get footer menu details.
                * @param integer $menu_id.
                * @param integer $menu_level i.e. 0 is used for root(1st) level menu and its decending menu is child of the root menu.
                * @param boolean Active/InActive $status.
                * @param integer $startlimit.
                * @param integer $count.
                * @pre not required.
                * @post an associative array of menu details.
                * return menu details.
                */
		function arrGetFooterMenuDetails($menu_id="", $menu_level="", $category_id="", $menu_page="", $title="", $status="",$startlimit="",$count=""){
                        if(!empty($status)){
                                $whereClauseArr[] = "$status in ($status)";
                        }
                        if(!empty($menu_id)){
                                $whereClauseArr[] = "menu_id in ($menu_id)";
                        }
                        if($menu_level != ""){
                                $whereClauseArr[] = "menu_level = $menu_level";
                        }
                        if($category_id != ""){
                                $whereClauseArr[] = "category_id = $category_id";
                        }
                        if($menu_page != ""){
                                $whereClauseArr[] = "menu_page = $menu_page";
                        }
                        if($title != ""){
                                $whereClauseArr[] = "title = $title";
                        }
                        if(sizeof($whereClauseArr) > 0){
                                $whereClauseStr = " where ".implode(" and ",$whereClauseArr);
                        }
                        if(!empty($startlimit)){
                                $limitArr[] = $startlimit;
                        }
                        if(!empty($count)){
                                $limitArr[] = $count;
                        }
                        if(sizeof($limitArr) > 0){
                                $limitStr = " limit ".implode(" , ",$limitArr);
                        }
                        $sql = "select * from FOOTER_MENU_MASTER $whereClauseStr $limitStr";
                        $result = $this->select($sql);
                        return $result;
                }

		/**
		* @note function is used to get header menu tree.
		* @param integer $menu_id.
		* @param integer $menu_level i.e. 0 is used for root(1st) level menu and its decending menu is child of the root menu.
		* @pre $menu_id must be valid non empty/zero integer value.
		* @post menu tree array.It is sort out with the root level menu.
		* return array.
		*/
		function arrGetHeaderMenuLevel($menu_id="",$menu_level='0',$status='1'){
			if(!empty($status)){
				$whereClauseArr[] = "status = $status";
			}
			if(!empty($menu_id)){
				$whereClauseArr[] = "menu_id = $menu_id";
			}
			if(!empty($menu_level)){
				$whereClauseArr[] = "menu_level = $menu_level";
			}
			if(sizeof($whereClauseArr) > 0){
				$whereClauseStr = ' where '.implode(' and ',$whereClauseArr);
			}
			$sql = "select * from HEADER_MENU_MASTER $whereClauseStr";
			$result = $this->select($sql);
			return $result;

		}
		/**
                * @note function is used to get footer menu tree.
                * @param integer $menu_id.
                * @param integer $menu_level i.e. 0 is used for root(1st) level menu and its decending menu is child of the root menu.
                * @pre $menu_id must be valid non empty/zero integer value.
                * @post menu tree array.It is sort out with the root level menu.
                * return array.
                */
		function arrGetFooterMenuLevel($menu_id="",$menu_level='0',$status='1'){
                        if(!empty($status)){
                                $whereClauseArr[] = "status = $status";
                        }
                        if(!empty($menu_id)){
                                $whereClauseArr[] = "menu_id = $menu_id";
                        }
                        if(!empty($menu_level)){
                                $whereClauseArr[] = "menu_level = $menu_level";
                        }
                        if(sizeof($whereClauseArr) > 0){
                                $whereClauseStr = ' where '.implode(' and ',$whereClauseArr);
                        }
                        $sql = "select * from FOOTER_MENU_MASTER $whereClauseStr";
                        $result = $this->select($sql);
                        return $result;

                }
		/**
		* @note function is used to get header menu tree.
		* @param integer $menu_id.
		* @param integer $menu_level i.e. 0 is used for root(1st) level menu and its decending menu is child of the root menu.
		* @pre $menu_id must be valid non empty/zero integer value.
		* @post menu tree array.It is sort out with the root level menu.
		* return array.
		*/
		function arrGetHeaderMenuBreadCrumb($menu_id,&$menuTreeArr=''){
			$result = $this->arrGetHeaderMenuLevel($menu_id);
			if(sizeof($result) > 0){
				$menu_id = $result[0]['menu_id'];
				$menu_level = $result[0]['menu_level'];
				$menu_name = $result[0]['menu_name'];
				$menuTreeArr[$menu_id] = $menu_name;
				if($menu_level != 0){ $this->arrGetHeaderMenuBreadCrumb($menu_level,$menuTreeArr); }
			}
			ksort($menuTreeArr);
			return $menuTreeArr;
		}
		/**
                * @note function is used to get footer menu tree.
                * @param integer $menu_id.
                * @param integer $menu_level i.e. 0 is used for root(1st) level menu and its decending menu is child of the root menu.
                * @pre $menu_id must be valid non empty/zero integer value.
                * @post menu tree array.It is sort out with the root level menu.
                * return array.
                */
		function arrGetFooterMenuBreadCrumb($menu_id,&$menuTreeArr=''){
                        $result = $this->arrGetFooterMenuLevel($menu_id);
                        if(sizeof($result) > 0){
                                $menu_id = $result[0]['menu_id'];
                                $menu_level = $result[0]['menu_level'];
                                $menu_name = $result[0]['menu_name'];
                                $menuTreeArr[$menu_id] = $menu_name;
                                if($menu_level != 0){ $this->arrGetFooterMenuBreadCrumb($menu_level,$menuTreeArr); }
                        }
                        ksort($menuTreeArr);
                        return $menuTreeArr;
                }
	}
?>
