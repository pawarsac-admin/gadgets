<?php
/**
 * @brief class is used add,update,delete,get Dealer details.
 * @author 
 * @version 1.0
 * @created 
 */
class Dealer extends DbOperation
{
        	var $cache;
	        var $dealerKey;
		/**Intialize the consturctor.*/
		function Dealer(){
			$this->cache = new Cache;
                        $this->dealerKey = MEMCACHE_MASTER_KEY."dealer";
		}
	
		 /**
		 * @note function is used to insert the Dealer information into the database.
		 * @param an associative array $insert_param.
		 * @pre $insert_param must be valid associative array.
		 * @post an integer $dealer_id.
		 * retun integer.
		 */
		function addUpdDealerDetails($aParameters,$sTableName){
			$aParameters['create_date'] = date('Y-m-d H:i:s');
			$aParameters['update_date'] = date('Y-m-d H:i:s');
			$sSql=$this->getInsertUpdateSql($sTableName,array_keys($aParameters),array_values($aParameters));	
			//echo "TEST---".$sSql."<br>";	
			$iRes=$this->insertUpdate($sSql);
                        $this->cache->searchDeleteKeys($this->dealerKey);
			return $iRes;
		}


		/**
		 * @note function is used to insert the Dealer product information into the database.
		 * @param an associative array $insert_param.
		 * @pre $insert_param must be valid associative array.
		 * @post an integer $dealer_id.
		 * retun integer.
		 */
		function intInsertDealerDetails($insert_param)
		{
			$insert_param['create_date'] = date('Y-m-d H:i:s');
			$sql = $this->getInsertUpdateSql("DEALER_PRODUCT",array_keys($insert_param),array_values($insert_param));
			$dealer_id = $this->insert($sql);
			if($dealer_id == 'Duplicate entry'){ return 'exists';}
                        $this->cache->searchDeleteKeys($this->dealerKey."_product");
                        return $dealer_id;
		}

		/**
		 * @note function is used to delete the Dealer.
		 * @param integer $dealer_id
		 * @pre $dealer_id must be non-empty/zero valid integer.
		 * @post boolean true/false.
		 * return boolean.
		 */
		 function boolDeleteDealer($dealer_id)
		 {
			$sql = "delete from DEALER_PRODUCT where dealer_id = $dealer_id";
			$isDelete = $this->sql_delete_data($sql);
                       $this->cache->searchDeleteKeys($this->dealerKey."_product");
			$sql = "delete from DEALER where dealer_id = $dealer_id";
			$isDelete = $this->sql_delete_data($sql);
                	$this->cache->searchDeleteKeys($this->dealerKey);
			return $isDelete;
		 }


		/**
		 * @note function is used to delete the Dealer Product.
		 * @param integer $dealer_id
		 * @pre $dealer_id must be non-empty/zero valid integer.
		 * @post boolean true/false.
		 * return boolean.
		 */
		 function boolDeleteFeaturedDealer($featured_dealer_id)
		 {
			$sql = "delete from FEATURED_DEALER where featured_dealer_id = $featured_dealer_id";
			$isDelete = $this->sql_delete_data($sql);
                        $this->cache->searchDeleteKeys($this->dealerKey."_feature");
			return $isDelete;
		 }


		 /**
		 * @note function is used to get Dealer details.
		 * @param integer dealer_id.
		 * @param an integer/comma seperated dealer_id/ dealer_id array $dealer_ids.
		 * @param an integer country_id.
		 * @param an integer state_id.
		 * @param boolean Active/InActive $status.
		 * @param integer $startlimit.
		 * @param integer $count.
		 * @pre not required.
		 * @post dealer details in associative array.
		 * retun an array.
		 */
		 function arrGetDealerDetails($dealer_ids="",$country_ids="",$state_id="",$city_id="",$category_id="",$brand_id="",$status='1',$startlimit="",$count="")
		 {
			$keyArr[] = $this->dealerKey;
			if(is_array($dealer_ids)){
				$dealer_ids = implode(",",$dealer_ids);
			}
			if(is_array($category_id)){
				$category_id = implode(",",$category_id);
			}
			if(is_array($country_ids)){
				$country_ids = implode(",",$country_ids);
			}
			if(is_array($state_id)){
				$state_id = implode(",",$state_id);
			}
			if(is_array($brand_id)){
				$brand_id = implode(",",$brand_id);
			}
			if($status != ''){
				$keyArr[] = "status_$status";
				$whereClauseArr[] = "status=$status";
			}
			if(!empty($dealer_ids)){
				$keyArr[] = "dealer_id_$dealer_ids";
				$whereClauseArr[] = "D.dealer_id in ($dealer_ids)";
			}
			if(!empty($category_id)){
				$keyArr[] = "category_id_$category_id";
				$whereClauseArr[] = "category_id in ($category_id)";
			}
			if(!empty($country_ids)){
				$keyArr[] = "country_id_$country_ids";
				$whereClauseArr[] = "country_id in($country_ids)";
			}
			if(!empty($state_id)){
				$keyArr[] = "state_id_$state_id";
				$whereClauseArr[] = "state_id in ($state_id)";
			}
			if(!empty($city_id)){
				$keyArr[] = "city_id_$city_id";
				$whereClauseArr[] = "city_id in ($city_id)";
			}
			if(!empty($brand_id)){
				$keyArr[] = "brand_id_$brand_id";
				$whereClauseArr[] = "brand_id in ($brand_id)";
			}
			$whereClauseArr[] = " D.dealer_id = DP.dealer_id";
			if(sizeof($whereClauseArr) > 0){
				$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
			}
			if(!empty($startlimit)){
				$keyArr[] = "startlimit_$startlimit";
				$limitArr[] = $startlimit;
			}
			if(!empty($count)){
				$keyArr[] = "count_$count";
				$limitArr[] = $count;
			}
			if(sizeof($limitArr) > 0){
				$limitStr = " limit ".implode(" , ",$limitArr);
			}
			$key = implode("_",$keyArr);
			if($result = $this->cache->get($key)){return $result;}
			$sql = "select * from DEALER D,DEALER_PRODUCT DP $whereClauseStr order by D.create_date desc $limitStr ";
			//echo $sql;
			$result = $this->select($sql);
			$this->cache->set($key,$result);
			return $result;
		 }


		 /**
                 * @note function is used to get City List of Dealers.
                 * @param integer dealer_id.
                 * @param an integer/comma seperated dealer_id/ dealer_id array $dealer_ids.
                 * @param an integer country_id.
                 * @param an integer state_id.
                 * @param boolean Active/InActive $status.
                 * @param integer $startlimit.
                 * @param integer $count.
                 * @pre not required.
                 * @post dealer details in associative array.
                 * retun an array.
                 */
		function arrGetCityByDealer($dealer_ids="",$country_ids="",$state_id="",$city_id="",$category_id="",$brand_id="",$status='1',$startlimit="",$count="",$orderby="")
                 {
			$keyArr[] = $this->dealerKey."_city_dealer";
                        if(is_array($dealer_ids)){
                                $dealer_ids = implode(",",$dealer_ids);
                        }
                        if(is_array($category_id)){
                                $category_id = implode(",",$category_id);
                        }
                        if(is_array($country_ids)){
                                $country_ids = implode(",",$country_ids);
                        }
                        if(is_array($state_id)){
                                $state_id = implode(",",$state_id);
                        }
                        if(is_array($brand_id)){
                                $brand_id = implode(",",$brand_id);
                        }
                        if($status != ''){
				$keyArr[] = "status_$status";
                                $whereClauseArr[] = "status=$status";
                        }
                        if(!empty($dealer_ids)){
				$keyArr[] = "dealer_id_$dealer_ids";
                                $whereClauseArr[] = "D.dealer_id in ($dealer_ids)";
                        }
                        if(!empty($category_id)){
				$keyArr[] = "category_id_$category_id";
                                $whereClauseArr[] = "category_id in ($category_id)";
                        }
                        if(!empty($country_ids)){
				$keyArr[] = "country_id_$country_ids";
                                $whereClauseArr[] = "country_id in($country_ids)";
                        }
                        if(!empty($state_id)){
				$keyArr[] = "state_id_$state_id";
                                $whereClauseArr[] = "state_id in ($state_id)";
                        }
			if(!empty($city_id)){
				$keyArr[] = "city_id_$city_id";
                                $whereClauseArr[] = "city_id in ($city_id)";
                        }
                        if(!empty($brand_id)){
				$keyArr[] = "brand_id_$brand_id";
                                $whereClauseArr[] = "brand_id in ($brand_id)";
                        }
                        $whereClauseArr[] = " D.dealer_id = DP.dealer_id";
                        if(sizeof($whereClauseArr) > 0){
                                $whereClauseStr = " where ".implode(" and ",$whereClauseArr);
                        }
                        if(!empty($startlimit)){
				$keyArr[] = "startlimit_$startlimit";
                                $limitArr[] = $startlimit;
                        }
                        if(!empty($count)){
				$keyArr[] = "count_$count";
                                $limitArr[] = $count;
                        }
                        if(sizeof($limitArr) > 0){
                                $limitStr = " limit ".implode(" , ",$limitArr);
                        }
                        if(empty($orderby)){
                                $orderby = "order by D.create_date desc";
                        }
			$key = implode("_",$keyArr);
	                if($result = $this->cache->get($key)){return $result;}
                        $sql = "select distinct(D.city_id) as city_id from DEALER D,DEALER_PRODUCT DP $whereClauseStr  $orderby $limitStr";
                        //echo $sql."<br>";
                        $result = $this->select($sql);
			$this->cache->set($key,$result);
                        return $result;
                 }

		 /**
                 * @note function is used to get Brand List of Dealers.
                 * @param integer dealer_id.
                 * @param an integer/comma seperated dealer_id/ dealer_id array $dealer_ids.
                 * @param an integer country_id.
                 * @param an integer state_id.
                 * @param boolean Active/InActive $status.
                 * @param integer $startlimit.
                 * @param integer $count.
                 * @pre not required.
                 * @post dealer details in associative array.
                 * retun an array.
                 */
		function arrGetBrandByDealer($dealer_ids="",$country_ids="",$state_id="",$city_id="",$category_id="",$brand_id="",$status='1',$startlimit="",$count="",$orderby="")
                 {
			$keyArr[] = $this->dealerKey."_brand_dealer";
                        if(is_array($dealer_ids)){
                                $dealer_ids = implode(",",$dealer_ids);
                        }
                        if(is_array($category_id)){
                                $category_id = implode(",",$category_id);
                        }
                        if(is_array($country_ids)){
                                $country_ids = implode(",",$country_ids);
                        }
                        if(is_array($state_id)){
                                $state_id = implode(",",$state_id);
                        }
                        if(is_array($brand_id)){
                                $brand_id = implode(",",$brand_id);
                        }
                        if($status != ''){	
				$keyArr[] = "status_$status";
                                $whereClauseArr[] = "status=$status";
                        }
                        if(!empty($dealer_ids)){
				$keyArr[] = "dealer_id_$dealer_ids";
                                $whereClauseArr[] = "D.dealer_id in ($dealer_ids)";
                        }
                        if(!empty($category_id)){
				$keyArr[] = "category_id_$category_id";
                                $whereClauseArr[] = "category_id in ($category_id)";
                        }
                        if(!empty($country_ids)){
				$keyArr[] = "country_id_$country_ids";
                                $whereClauseArr[] = "country_id in($country_ids)";
                        }
                        if(!empty($state_id)){
				$keyArr[] = "state_id_$state_id";
                                $whereClauseArr[] = "state_id in ($state_id)";
                        }
			if(!empty($city_id)){
				$keyArr[] = "city_id_$city_id";
                                $whereClauseArr[] = "city_id in ($city_id)";
                        }
                        if(!empty($brand_id)){
				$keyArr[] = "brand_id_$brand_id";
                                $whereClauseArr[] = "brand_id in ($brand_id)";
                        }
                        $whereClauseArr[] = " D.dealer_id = DP.dealer_id";
                        if(sizeof($whereClauseArr) > 0){
                                $whereClauseStr = " where ".implode(" and ",$whereClauseArr);
                        }
                        if(!empty($startlimit)){
				$keyArr[] = "startlimit_$startlimit";
                                $limitArr[] = $startlimit;
                        }
                        if(!empty($count)){
				$keyArr[] = "count_$count";
                                $limitArr[] = $count;
                        }
                        if(sizeof($limitArr) > 0){
                                $limitStr = " limit ".implode(" , ",$limitArr);
                        }
                        if(empty($orderby)){
                                $orderby = "order by D.create_date desc";
                        }
			$key = implode("_",$keyArr);
                        if($result = $this->cache->get($key)){return $result;}
                        $sql = "select distinct(DP.brand_id) as brand_id from DEALER D,DEALER_PRODUCT DP $whereClauseStr  $orderby $limitStr";;
                        //echo $sql."<br>";
                        $result = $this->select($sql);
			$this->cache->set($key,$result);
                        return $result;
                 }

		  /**
		 * @note function is used to get Dealer details.
		 * @param integer dealer_id.
		 * @param an integer/comma seperated dealer_id/ dealer_id array $dealer_ids.
		 * @param an integer country_id.
		 * @param an integer state_id.
		 * @param boolean Active/InActive $status.
		 * @param integer $startlimit.
		 * @param integer $count.
		 * @pre not required.
		 * @post dealer details in associative array.
		 * retun an array.
		 */
		 function arrGetFeaturedDealerDetails($featured_dealer_ids="",$dealer_ids="",$country_ids="",$state_id="",$city_id="",$category_id="",$brand_id="",$status='1',$startlimit="",$count="")
		 {
			$keyArr[] = $this->dealerKey."_feature";
			if(is_array($featured_dealer_ids)){
				$featured_dealer_ids = implode(",",$featured_dealer_ids);
			}
			if(is_array($dealer_ids)){
				$dealer_ids = implode(",",$dealer_ids);
			}
			if(is_array($category_id)){
				$category_id = implode(",",$category_id);
			}
			if(is_array($country_ids)){
				$country_ids = implode(",",$country_ids);
			}
			if(is_array($state_id)){
				$state_id = implode(",",$state_id);
			}
			if(is_array($brand_id)){
				$brand_id = implode(",",$brand_id);
			}
			if($status != ''){
				$keyArr[] = "status_$status";
				$whereClauseArr[] = "FD.status=$status";
			}
			if(!empty($featured_dealer_ids)){
				$keyArr[] = "featured_dealer_id_$featured_dealer_ids";
				$whereClauseArr[] = "FD.featured_dealer_id in ($featured_dealer_ids)";
			}
			if(!empty($dealer_ids)){	
				$keyArr[] = "dealer_id_$dealer_ids";
				$whereClauseArr[] = "D.dealer_id in ($dealer_ids)";
			}
			if(!empty($category_id)){
				$keyArr[] = "category_id_$category_id";
				$whereClauseArr[] = "FD.category_id in ($category_id)";
			}
			if(!empty($country_ids)){
				$keyArr[] = "country_id_$country_ids";
				$whereClauseArr[] = "country_id in($country_ids)";
			}
			if(!empty($state_id)){
				$keyArr[] = "state_id_$state_id";
				$whereClauseArr[] = "state_id in ($state_id)";
			}
			if(!empty($city_id)){
				$keyArr[] = "city_id_$city_id";
				$whereClauseArr[] = "city_id in ($city_id)";
			}
			if(!empty($brand_id)){
				$keyArr[] = "brand_id_$brand_id";
				$whereClauseArr[] = "brand_id in ($brand_id)";
			}
			$whereClauseArr[] = " D.dealer_id = DP.dealer_id";
			$whereClauseArr[] = " D.dealer_id = FD.dealer_id";
			if(sizeof($whereClauseArr) > 0){
				$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
			}
			if(!empty($startlimit)){
				$keyArr[] = "startlimit_$startlimit";
				$limitArr[] = $startlimit;
			}
			if(!empty($count)){
				$keyArr[] = "count_$count";
				$limitArr[] = $count;
			}
			if(sizeof($limitArr) > 0){
				$limitStr = " limit ".implode(" , ",$limitArr);
			}
			$key = implode("_",$keyArr);
                        if($result = $this->cache->get($key)){return $result;}
			$sql = "select * from DEALER D,DEALER_PRODUCT DP,FEATURED_DEALER FD $whereClauseStr  order by FD.create_date desc $limitStr";
			//echo $sql."<br>";
			$result = $this->select($sql);
			$this->cache->set($key,$result);
			return $result;
		 }

		 /**
		 * @note function is used to get Dealer Product details.
		 * @param integer dealer_id.
		 * @param an integer/comma seperated dealer_id/ dealer_id array $dealer_ids.
		 * @param integer $startlimit.
		 * @param integer $count.
		 * @pre not required.
		 * @post dealer details in associative array.
		 * retun an array.
		 */
		 function arrGetDealerProductDetails($dealer_ids="",$product_ids="",$category_ids="",$brand_ids="",$status='0',$startlimit="",$count="")
		 {
			$keyArr[] = $this->dealerKey."_product";
			if(is_array($dealer_ids)){
				$dealer_ids = implode(",",$dealer_ids);
			}
			if(is_array($product_ids)){
				$product_ids = implode(",",$product_ids);
			}
			if(is_array($category_ids)){
				$category_ids = implode(",",$category_ids);
			}
			
			if($status != ''){
				$keyArr[] = "status_$status";
				$whereClauseArr[] = "status=$status";
			}
			if(!empty($dealer_ids)){
				$keyArr[] = "dealer_id_$dealer_ids";
				$whereClauseArr[] = "dealer_id in ($dealer_ids)";
			}
			if(!empty($product_ids)){
				$keyArr[] = "product_id_$product_ids";
				$whereClauseArr[] = "product_id in($product_ids)";
			}
			if(!empty($category_ids)){
				$keyArr[] = "category_id_$category_ids";
				$whereClauseArr[] = "category_id in($category_ids)";
			}
			if(!empty($state_id)){
				$keyArr[] = "state_id_$state_id";
				$whereClauseArr[] = "state_id in ($state_id)";
			}
			
			if(sizeof($whereClauseArr) > 0){
				$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
			}
			if(!empty($startlimit)){
				$keyArr[] = "startlimit_$startlimit";
				$limitArr[] = $startlimit;
			}
			if(!empty($count)){
				$keyArr[] = "count_$count";
				$limitArr[] = $count;
			}
			if(sizeof($limitArr) > 0){
				$limitStr = " limit ".implode(" , ",$limitArr);
			}
			$key = implode("_",$keyArr);
                        if($result = $this->cache->get($key)){return $result;}
			$sql = "select * from DEALER_PRODUCT $whereClauseStr $limitStr ";
			//echo $sql."<br>";
			$result = $this->select($sql);
			$this->cache->set($key,$result);
			return $result;
		 }
		/**
                 * @note function is used to insert the Premium Dealer information into the database.
                 * @param an associative array $insert_param.
                 * @pre $insert_param must be valid associative array.
                 * @post an integer $premium_dealer_id.
                 * retun integer.
                 */
                function addUpdPremiumDealerDetails($aParameters,$sTableName){
                        $aParameters['create_date'] = date('Y-m-d H:i:s');
                        $aParameters['update_date'] = date('Y-m-d H:i:s');
                        $sSql=$this->getInsertUpdateSql($sTableName,array_keys($aParameters),array_values($aParameters));
                        //echo "TEST---".$sSql."<br>";  
                        $iRes=$this->insertUpdate($sSql);
                        $this->cache->searchDeleteKeys($this->dealerKey."_premium");
			return $iRes;
		}
                 /**
                 * @note function is used to delete the Premium Dealer.
                 * @param integer $premium_dealer_id
                 * @pre $premium_dealer_id must be non-empty/zero valid integer.
                 * @post boolean true/false.
                 * return boolean.
                 */
                 function boolDeletePremiumDealer($premium_dealer_id)
                 {
                        $sql = "delete from PREMIUM_DEALER where premium_dealer_id = $premium_dealer_id";
                        $isDelete = $this->sql_delete_data($sql);
                        $this->cache->searchDeleteKeys($this->dealerKey."_premium");
			return $isDelete;
                 }
		 /**
                 * @note function is used to get Premium Dealer details.
                 * @param integer premium_dealer_id.
                 * @param an integer/comma seperated premium_dealer_id/ premium_dealer_id array $premium_dealer_ids.
                 * @param an integer country_id.
                 * @param an integer state_id.
                 * @param boolean Active/InActive $status.
                 * @param integer $startlimit.
                 * @param integer $count.
                 * @pre not required.
                 * @post dealer details in associative array.
                 * retun an array.
                 */
                 function arrGetPremiumDealerDetails($premium_dealer_ids="",$dealer_ids="",$country_ids="",$state_id="",$city_id="",$category_id="",$brand_id="",$status='1',$startlimit="",$count="",$orderby=""){
			$keyArr[] = $this->dealerKey."_premium";
                        if(is_array($premium_dealer_ids)){
                                $premium_dealer_ids = implode(",",$premium_dealer_ids);
                        }
                        if(is_array($dealer_ids)){
                                $dealer_ids = implode(",",$dealer_ids);
                        }
                        if(is_array($category_id)){
                                $category_id = implode(",",$category_id);
                        }
                        if(is_array($country_ids)){
                                $country_ids = implode(",",$country_ids);
                        }
                        if(is_array($state_id)){
                                $state_id = implode(",",$state_id);
                        }
                        if(is_array($brand_id)){
                                $brand_id = implode(",",$brand_id);
                        }
			if($status != ''){
				$keyArr[] = "status_$status";
                                $whereClauseArr[] = "PD.status=$status";
                        }
                        if(!empty($premium_dealer_ids)){
				$keyArr[] = "premium_dealer_id_$premium_dealer_ids";
                                $whereClauseArr[] = "PD.premium_dealer_id in ($premium_dealer_ids)";
                        }
                        if(!empty($dealer_ids)){
				$keyArr[] = "dealer_id_$dealer_ids";
                                $whereClauseArr[] = "D.dealer_id in ($dealer_ids)";
                        }
                        if(!empty($category_id)){
				$keyArr[] = "category_id_$category_id";
                                $whereClauseArr[] = "PD.category_id in ($category_id)";
                        }
                        if(!empty($country_ids)){
				$keyArr[] = "country_id_$country_ids";
                                $whereClauseArr[] = "country_id in($country_ids)";
                        }
                        if(!empty($state_id)){
				$keyArr[] = "state_id_$state_id";
                                $whereClauseArr[] = "state_id in ($state_id)";
                        }
                        if(!empty($city_id)){
				$keyArr[] = "city_id_$city_id";
                                $whereClauseArr[] = "city_id in ($city_id)";
                        }
                        if(!empty($brand_id)){
				$keyArr[] = "brand_id_$brand_id";
                                $whereClauseArr[] = "brand_id in ($brand_id)";
                        }
                        $whereClauseArr[] = " D.dealer_id = DP.dealer_id";
                        $whereClauseArr[] = " D.dealer_id = PD.dealer_id";
                        if(sizeof($whereClauseArr) > 0){
                                $whereClauseStr = " where ".implode(" and ",$whereClauseArr);
                        }
                        if(!empty($startlimit)){
				$keyArr[] = "startlimit_$startlimit";
                                $limitArr[] = $startlimit;
                        }
			if(!empty($count)){
				$keyArr[] = "count_$count";
                                $limitArr[] = $count;
                        }
                        if(sizeof($limitArr) > 0){
                                $limitStr = " limit ".implode(" , ",$limitArr);
                        }
			if(empty($orderby)){
                                $oredrby = "order by PD.create_date desc";
                        }
			$key = implode("_",$keyArr);
                        if($result = $this->cache->get($key)){return $result;}
                        $sql = "select * from DEALER D,DEALER_PRODUCT DP,PREMIUM_DEALER PD $whereClauseStr  $orderby $limitStr";
                        //echo $sql."<br>";
                        $result = $this->select($sql);
			$this->cache->set($key,$result);
                        return $result;
                 }
		 /**
                 * @note function is used to get Premium Dealers count.
                 * @param integer premium_dealer_id.
                 * @param an integer/comma seperated premium_dealer_id/ premium_dealer_id array $premium_dealer_ids.
                 * @param an integer country_id.
                 * @param an integer state_id.
                 * @param boolean Active/InActive $status.
                 * @param integer $startlimit.
                 * @param integer $count.
                 * @pre not required.
                 * @post dealer details in associative array.
                 * retun an array.
                 */
		 function arrGetPremiumDealerDetailsCount($premium_dealer_ids="",$dealer_ids="",$country_ids="",$state_id="",$city_id="",$category_id="",$brand_id="",$status='1',$startlimit="",$count="",$orderby=""){
			$keyArr[] = $this->dealerKey."_premium_count";
                        if(is_array($premium_dealer_ids)){
                                $premium_dealer_ids = implode(",",$premium_dealer_ids);
                        }
                        if(is_array($dealer_ids)){
                                $dealer_ids = implode(",",$dealer_ids);
                        }
                        if(is_array($category_id)){
                                $category_id = implode(",",$category_id);
                        }
                        if(is_array($country_ids)){
                                $country_ids = implode(",",$country_ids);
                        }
                        if(is_array($state_id)){
                                $state_id = implode(",",$state_id);
                        }
                        if(is_array($brand_id)){
                                $brand_id = implode(",",$brand_id);
                        }
                        if($status != ''){
				$keyArr[] = "status_$status";
                                $whereClauseArr[] = "status=$status";
                        }
                        if(!empty($premium_dealer_ids)){
				$keyArr[] = "premium_dealer_id_$premium_dealer_ids";
                                $whereClauseArr[] = "PD.premium_dealer_id in ($premium_dealer_ids)";
                        }
                        if(!empty($dealer_ids)){
				$keyArr[] = "dealer_id_$dealer_ids";
                                $whereClauseArr[] = "D.dealer_id in ($dealer_ids)";
                        }
                        if(!empty($category_id)){
				$keyArr[] = "category_id_$category_id";
                                $whereClauseArr[] = "PD.category_id in ($category_id)";
                        }
                        if(!empty($country_ids)){
				$keyArr[] = "country_id_$country_ids";
                                $whereClauseArr[] = "country_id in($country_ids)";
                        }
                        if(!empty($state_id)){
				$keyArr[] = "state_id_$state_id";
                                $whereClauseArr[] = "state_id in ($state_id)";
                        }
                        if(!empty($city_id)){
				$keyArr[] = "city_id_$city_id";
                                $whereClauseArr[] = "city_id in ($city_id)";
                        }
			if(!empty($brand_id)){
				$keyArr[] = "brand_id_$brand_id";
                                $whereClauseArr[] = "brand_id in ($brand_id)";
                        }
                        $whereClauseArr[] = " D.dealer_id = DP.dealer_id";
                        $whereClauseArr[] = " D.dealer_id = PD.dealer_id";
                        if(sizeof($whereClauseArr) > 0){
                                $whereClauseStr = " where ".implode(" and ",$whereClauseArr);
                        }
                        if(!empty($startlimit)){
				$keyArr[] = "startlimit_$startlimit";
                                $limitArr[] = $startlimit;
                        }
                        if(!empty($count)){
				$keyArr[] = "count_$count";
                                $limitArr[] = $count;
                        }
                        if(sizeof($limitArr) > 0){
                                $limitStr = " limit ".implode(" , ",$limitArr);
                        }
			if(empty($orderby)){
                                $oredrby = "order by PD.create_date desc";
                        }
			$key = implode("_",$keyArr);
                        if($result = $this->cache->get($key)){return $result;}
                        $sql = "select count(PD.premium_dealer_id) as cnt from DEALER D,DEALER_PRODUCT DP,PREMIUM_DEALER PD $whereClauseStr $limitStr $orderby";
                        //echo $sql."<br>";
                        $result = $this->select($sql);
			$this->cache->set($key,$result);
                        return $result;
                 }

		/**
                 * @note function is used to insert the Related Dealer information into the database.
                 * @param an associative array $insert_param.
                 * @pre $insert_param must be valid associative array.
                 * @post an integer $dealer_id.
                 * retun integer.
                 */
                function addUpdRelatedDealerDetails($aParameters,$sTableName){
                        $aParameters['create_date'] = date('Y-m-d H:i:s');
                        $aParameters['update_date'] = date('Y-m-d H:i:s');
                        $sSql=$this->getInsertUpdateSql($sTableName,array_keys($aParameters),array_values($aParameters));
                        //echo "TEST---".$sSql."<br>";  
                        $iRes=$this->insertUpdate($sSql);
                        $this->cache->searchDeleteKeys($this->dealerKey."_related");
			return $iRes;
                }
                 /**
                 * @note function is used to delete the Related Dealer.
                 * @param integer $dealer_id
                 * @pre $dealer_id must be non-empty/zero valid integer.
                 * @post boolean true/false.
                 * return boolean.
                 */
                 function boolDeleteRelatedDealer($dealer_id,$sTableName){
                        $sql = "delete from $sTableName where dealer_id = $dealer_id";
                        $isDelete = $this->sql_delete_data($sql);
                        $this->cache->searchDeleteKeys($this->dealerKey."_related");
			return $isDelete;
                 }

		 /**
                 * @note function is used to insert the Dealer tie up information into the database.
                 * @param an associative array $insert_param.
                 * @pre $insert_param must be valid associative array.
                 * @post an integer $dealer_tieup_id.
                 * retun integer.
                 */
                function addUpdDealerTieupDetails($aParameters,$sTableName){
                        $aParameters['create_date'] = date('Y-m-d H:i:s');
                        $aParameters['update_date'] = date('Y-m-d H:i:s');
                        $sSql=$this->getInsertUpdateSql($sTableName,array_keys($aParameters),array_values($aParameters));
                        //echo "TEST---".$sSql."<br>";  
                        $iRes=$this->insertUpdate($sSql);
                        $this->cache->searchDeleteKeys($this->dealerKey."_tieup");
                        return $iRes;
                }
                 /**
                 * @note function is used to delete the Dealer Tieup.
                 * @param integer $dealer_tieup_id
                 * @pre $dealer_id must be non-empty/zero valid integer.
                 * @post boolean true/false.
                 * return boolean.
                 */
                 function boolDeleteDealerTieup($dealer_tieup_id,$sTableName){
                        $sql = "delete from $sTableName where dealer_tieup_id = $dealer_tieup_id";
                        $isDelete = $this->sql_delete_data($sql);
                        $this->cache->searchDeleteKeys($this->dealerKey."_tieup");
                        return $isDelete;
                 }
		 /**
                 * @note function is used to get  Dealer Tie-up details.
                 * @param integer dealer_tieup_id.
                 * @param an integer/comma seperated dealer_tieup_id/ dealer_tieup_id array $dealer_tiup_ids.
                 * @param an integer country_id.
                 * @param an integer state_id.
                 * @param boolean Active/InActive $status.
                 * @param integer $startlimit.
                 * @param integer $count.
                 * @pre not required.
                 * @post dealer tieup details in associative array.
                 * retun an array.
                 */
                 function arrGetDealerTieupDetails($dealer_tieup_ids="",$dealer_ids="",$country_ids="",$state_id="",$city_id="",$category_id="",$brand_id="",$status='1',$startlimit="",$count="",$orderby=""){
			$keyArr[] = $this->dealerKey."_tieup";
                        if(is_array($dealer_tieup_ids)){
                                $dealer_tieup_ids = implode(",",$dealer_tieup_ids);
                        }
                        if(is_array($dealer_ids)){
                                $dealer_ids = implode(",",$dealer_ids);
                        }
                        if(is_array($category_id)){
                                $category_id = implode(",",$category_id);
                        }
                        if(is_array($country_ids)){
                                $country_ids = implode(",",$country_ids);
                        }
                        if(is_array($state_id)){
                                $state_id = implode(",",$state_id);
                        }
                        if(is_array($brand_id)){
                                $brand_id = implode(",",$brand_id);
                        }
			if($status != ''){
				$keyArr[] = "status_$status";
                                $whereClauseArr[] = "DT.status=$status";
                        }
                        if(!empty($dealer_tieup_ids)){
				$keyArr[] = "dealer_tieup_id_$dealer_tieup_ids";
                                $whereClauseArr[] = "DT.dealer_tieup_id in ($dealer_tieup_ids)";
                        }
                        if(!empty($dealer_ids)){
				$keyArr[] = "dealer_id_$dealer_ids";
                                $whereClauseArr[] = "D.dealer_id in ($dealer_ids)";
                        }
                        if(!empty($category_id)){
				$keyArr[] = "category_id_$category_id";
                                $whereClauseArr[] = "DT.category_id in ($category_id)";
                        }
                        if(!empty($country_ids)){
				$keyArr[] = "country_id_$country_ids";
                                $whereClauseArr[] = "country_id in($country_ids)";
                        }
                        if(!empty($state_id)){
				$keyArr[] = "state_id_$state_id";
                                $whereClauseArr[] = "state_id in ($state_id)";
                        }
                        if(!empty($city_id)){
				$keyArr[] = "city_id_$city_id";
                                $whereClauseArr[] = "city_id in ($city_id)";
                        }
                        if(!empty($brand_id)){
				$keyArr[] = "brand_id_$brand_id";
                                $whereClauseArr[] = "brand_id in ($brand_id)";
                        }
                        $whereClauseArr[] = " D.dealer_id = DP.dealer_id";
                        $whereClauseArr[] = " D.dealer_id = DT.dealer_id";
                        if(sizeof($whereClauseArr) > 0){
                                $whereClauseStr = " where ".implode(" and ",$whereClauseArr);
                        }
                        if(!empty($startlimit)){
				$keyArr[] = "startlimit_$startlimit";
                                $limitArr[] = $startlimit;
                        }
                        if(!empty($count)){
				$keyArr[] = "count_$count";
                                $limitArr[] = $count;
                        }
			if(sizeof($limitArr) > 0){
                                $limitStr = " limit ".implode(" , ",$limitArr);
                        }
                        if(empty($orderby)){
                                $oredrby = "order by DT.create_date desc";
                        }
			$key = implode("_",$keyArr);
                        if($result = $this->cache->get($key)){return $result;}
                        $sql = "select * from DEALER D,DEALER_PRODUCT DP,DEALERS_TIEUP DT $whereClauseStr  $orderby $limitStr";
                        //echo $sql."<br>";
                        $result = $this->select($sql);
			$this->cache->set($key,$result);
                        return $result;
                 }
		 
		 /**
                 * @note function is used to insert the Request Type information into the database.
                 * @param an associative array $insert_param.
                 * @pre $insert_param must be valid associative array.
                 * @post an integer $request_type_id.
                 * retun integer.
                 */
                function addUpdRequestTypeDetails($aParameters,$sTableName){
                        $aParameters['create_date'] = date('Y-m-d H:i:s');
                        $aParameters['update_date'] = date('Y-m-d H:i:s');
                        $sSql=$this->getInsertUpdateSql($sTableName,array_keys($aParameters),array_values($aParameters));
                        $iRes=$this->insertUpdate($sSql);
                        $this->cache->searchDeleteKeys($this->dealerKey."_request_type");
                        return $iRes;
                }
                 /**
                 * @note function is used to delete the Request Type.
                 * @param integer $request_type_id
                 * @pre $request_type_id must be non-empty/zero valid integer.
                 * @post boolean true/false.
                 * return boolean.
                 */
                 function boolDeleteRequestType($request_type_id,$sTableName){
                        $sql = "delete from $sTableName where request_type_id = $request_type_id";
                        $isDelete = $this->sql_delete_data($sql);
                        $this->cache->searchDeleteKeys($this->dealerKey."_request_type");
                        return $isDelete;
                 }
		 /**
                 * @note function is used to get Request Type details.
                 * @param integer request_type_id.
                 * @param an integer/comma seperated request_type_id/ request_type_id array $request_type_ids.
                 * @param boolean Active/InActive $status.
                 * @param integer $startlimit.
                 * @param integer $count.
                 * @pre not required.
                 * @post request type details in associative array.
                 * retun an array.
                 */
                 function arrGetRequestTypeDetails($request_type_ids="",$request_type="",$category_ids="",$status='1',$startlimit="",$count="",$orderby=""){
                        $keyArr[] = $this->dealerKey."_request_type";
                        if(is_array($request_type_ids)){
                                $request_type_ids = implode(",",$request_type_ids);
                        }
                        if(is_array($category_ids)){
                                $category_ids= implode(",",$category_ids);
                        }
                        if(!empty($request_type_ids)){
                                $keyArr[] = "request_type_id_$request_type_ids";
                                $whereClauseArr[] = "request_type_id in ($request_type_ids)";
                        }
                        if(!empty($request_type)){
                                $keyArr[] = "request_type_$request_type";
				$whereClauseArr[] = "lower(request_type) = '".strtolower($request_type)."'";
                        }
                        if(!empty($category_ids)){
                                $keyArr[] = "category_id_$category_ids";
                                $whereClauseArr[] = "category_id in ($category_ids)";
                        }
			if($status != ''){
                                $keyArr[] = "status_$status";
                                $whereClauseArr[] = "status=$status";
                        }
                        if(sizeof($whereClauseArr) > 0){
                                $whereClauseStr = " where ".implode(" and ",$whereClauseArr);
                        }
                        if(!empty($startlimit)){
                                $keyArr[] = "startlimit_$startlimit";
                                $limitArr[] = $startlimit;
                        }
                        if(!empty($count)){
                                $keyArr[] = "count_$count";
                                $limitArr[] = $count;
                        }
                        if(sizeof($limitArr) > 0){
                                $limitStr = " limit ".implode(" , ",$limitArr);
                        }
                        if(empty($orderby)){
                                $oredrby = "order by create_date desc";
                        }
                        $key = implode("_",$keyArr);
			if($result = $this->cache->get($key)){return $result;}
                        $sql = "select * from REQUEST_TYPE $whereClauseStr  $orderby $limitStr";
                        //echo $sql."<br>";
                        $result = $this->select($sql);
                        $this->cache->set($key,$result);
                        return $result;
                 }
		 /**
                 * @note function is used to insert the Black Listed Numbers information into the database.
                 * @param an associative array $insert_param.
                 * @pre $insert_param must be valid associative array.
                 * @post an integer $black_list_number_id.
                 * retun integer.
                 */
                function addUpdBlackListNumbersDetails($aParameters){
                        $aParameters['create_date'] = date('Y-m-d H:i:s');
                        $aParameters['update_date'] = date('Y-m-d H:i:s');
                        $sSql=$this->getInsertUpdateSql("BLACK_LISTED_NUMBERS",array_keys($aParameters),array_values($aParameters));
                        $iRes=$this->insertUpdate($sSql);
                        $this->cache->searchDeleteKeys($this->dealerKey."_black_list_numbers");
                        return $iRes;
                }
                 /**
                 * @note function is used to delete the Black Listed Numbers.
                 * @param integer $black_list_number_id
                 * @pre $black_list_number_id must be non-empty/zero valid integer.
                 * @post boolean true/false.
                 * return boolean.
                 */
                 function boolDeleteBlackListNumbers($black_list_number_id){
                        $sql = "delete from BLACK_LISTED_NUMBERS where black_list_number_id = $black_list_number_id";
                        $isDelete = $this->sql_delete_data($sql);
                        $this->cache->searchDeleteKeys($this->dealerKey."_black_list_numbers");
                        return $isDelete;
                 }
		 /**
                 * @note function is used to get Black Listed Numbers list.
                 * @param integer black_list_number_id.
                 * @param an integer/comma seperated black_list_number_id/ black_list_number_id array $black_list_number_id.
                 * @param boolean Active/InActive $status.
                 * @param integer $startlimit.
                 * @param integer $count.
                 * @pre not required.
                 * @post black list number id in associative array.
                 * retun an array.
                 */
                 function arrGetBlackListNumbers($black_list_number_ids="",$black_list_number="",$type_ids="",$category_ids="",$status='1',$startlimit="",$count="",$orderby=""){
                        $keyArr[] = $this->dealerKey."_black_list_numbers";
                        if(is_array($black_list_number_ids)){
                                $black_list_number_ids = implode(",",$black_list_number_ids);
                        }
                        if(is_array($type_ids)){
                                $type_ids = implode(",",$type_ids);
                        }
                        if(is_array($category_ids)){
                                $category_ids= implode(",",$category_ids);
                        }
                        if(!empty($black_list_number_ids)){
                                $keyArr[] = "black_list_number_id_$black_list_number_ids";
                                $whereClauseArr[] = "black_list_number_id in ($black_list_number_ids)";
                        }
                        if(!empty($black_list_number)){
                                $keyArr[] = "black_list_number_$black_list_number";
                                $whereClauseArr[] = "black_list_number = '".$black_list_number."'";
                        }
                        if(!empty($type_ids)){
                                $keyArr[] = "type_id_$type_ids";
                                $whereClauseArr[] = "type_id in ($type_ids)";
                        }
                        if(!empty($category_ids)){
                                $keyArr[] = "category_id_$category_ids";
                                $whereClauseArr[] = "category_id in ($category_ids)";
                        }
                        if($status != ''){
                                $keyArr[] = "status_$status";
                                $whereClauseArr[] = "status=$status";
                        }
			if(sizeof($whereClauseArr) > 0){
                                $whereClauseStr = " where ".implode(" and ",$whereClauseArr);
                        }
                        if(!empty($startlimit)){
                                $keyArr[] = "startlimit_$startlimit";
                                $limitArr[] = $startlimit;
                        }
                        if(!empty($count)){
                                $keyArr[] = "count_$count";
                                $limitArr[] = $count;
                        }
                        if(sizeof($limitArr) > 0){
                                $limitStr = " limit ".implode(" , ",$limitArr);
                        }
                        if(empty($orderby)){
                                $oredrby = "order by create_date desc";
                        }
                        $key = implode("_",$keyArr);
                        if($result = $this->cache->get($key)){return $result;}
                        $sql = "select * from BLACK_LISTED_NUMBERS $whereClauseStr  $orderby $limitStr";
                        //echo $sql."<br>";
                        $result = $this->select($sql);
                        $this->cache->set($key,$result);
                        return $result;
                 }
}
