<?php
/**
 * @brief class is used to perform actions on user reviews
 * @author Sachin
 * @version 1.0
 * @created 11-Nov-2010 5:09:31 PM
 * @last updated on 08-Mar-2011 13:14:00 PM
 */
class USERREVIEW extends DbOperation
{

	var $cache;
	var $categoryid;
	var $userreviewkey;
	/**Initialize the consturctor.*/
	function USERREVIEW(){
		$this->cache = new Cache;
		$this->userreviewkey = MEMCACHE_MASTER_KEY."userreview";
	}
	/**
	* @note function is used to insert the user review information into the database.
	* @param an associative array $insert_param.
	* @pre $insert_param must be valid associative array.
	* @post an integer $answer_id.
	* retun integer.
	*/
	function intInsertUserReviewInfo($insert_param){
		$insert_param['create_date'] = date('Y-m-d H:i:s');
		$insert_param['update_date'] = date('Y-m-d H:i:s');
		$sql = $this->getInsertSql("USER_REVIEW",array_keys($insert_param),array_values($insert_param));
		//echo $sql;
		$answer_id = $this->insert($sql);
		$this->cache->searchDeleteKeys($this->userreviewkey);
		if($answer_id == 'Duplicate entry'){ return 'exists';}
		return $answer_id;
	}
	/**
	 * @note function is used to insert the user review answer into the database.
	 * @param an associative array $insert_param.
	 * @pre $insert_param must be valid associative array.
	 * @post an integer $answer_id.
	 * retun integer.
	 */
	function intInsertUserReviewAnswer($insert_param)
	{
		$insert_param['create_date'] = date('Y-m-d H:i:s');
		$insert_param['update_date'] = date('Y-m-d H:i:s');
		$sql = $this->getInsertSql("USER_REVIEW_ANSWER",array_keys($insert_param),array_values($insert_param));
		//echo $sql;
		$answer_id = $this->insert($sql);
		$this->cache->searchDeleteKeys($this->userreviewkey);
		if($answer_id == 'Duplicate entry'){ return 'exists';}
		return $answer_id;
	}
	/**
	 * @note function is used to update user review answer into the database.
	 * @param an associative array $update_param.
	 * @param is an integer $usr_review_ans_id.
	 * @pre $insert_param must be valid associative array.
	 * retun integer.
	 */
	function intUpdateUserReviewAnswer($update_param,$usr_review_ans_id){
		$insert_param['update_date'] = date('Y-m-d H:i:s');
		$sql = $this->getUpdateSql("USER_REVIEW_ANSWER",array_keys($update_param),array_values($update_param),'usr_review_ans_id',$usr_review_ans_id);
		$isUpdate = $this->update($sql);
		$this->cache->searchDeleteKeys($this->userreviewkey);
		return $isUpdate;
	}
	/**
	* @note function is used to insert/update the user review answer into the database.
	* @param an associative array $aParameters.
	* @param is a string $sTableName.
	* @pre $aParameters must be valid associative array.
	* retun integer.
	*/
	function addUpdUserReviewsDetails($aParameters,$sTableName){
		$aParameters['create_date'] = date('Y-m-d H:i:s');
		$aParameters['update_date'] = date('Y-m-d H:i:s');
		$sSql=$this->getInsertUpdateSql($sTableName,array_keys($aParameters),array_values($aParameters));
		//echo "TEST---".$sSql."<br>";    die();
		$iRes=$this->insertUpdate($sSql);
		$this->cache->searchDeleteKeys($this->userreviewkey);
		return $iRes;
	}
	/**
	 * @note function is used to update the user review into the database.
	 * @param an associative array $update_param.
	 * @param an integer $ans_id.
	 * @param $algoritham.
	 * @pre $update_param must be valid associative array and $brand_id must be non-empty/zero valid integer.
	 * @post boolean true/false.
	 * retun boolean.
	 */
	 function Update_USER_Review($ans_id,$algoritham,$update_param){
		 $update_param['update_date'] = date('Y-m-d H:i:s');
		 $update_param['algoritham'] = $algoritham;
	     $sql = $this->getUpdateSql("USER_REVIEW_ANSWER",array_keys($update_param),array_values($update_param),"ans_id",$ans_id);
		 $isUpdate = $this->update($sql);
		 $this->cache->searchDeleteKeys($this->userreviewkey);
		 return $isUpdate;
	 }
	/**
	* @note function is used to get question details list
	*
	* @param an integer/comma seperated question ids $queid.
	* @param an integer/comma seperated category ids/ category ids array $category_id.
	* @param boolean Active/InActive $status.
	* @param integer $startlimit.
	* @param integer $count.
	*
	* @pre not required.
	*
	* @post question details in associative array.
	* retun an array.
	*/
	function arrGetQuestions($queid="",$category_id="",$status="1",$startlimit="",$count=""){
		$keysArr[] = $this->userreviewkey."_question";
		if(is_array($queid)){
	 		$queid = implode(",",$queid);
	 	}
		if(!empty($queid)){
	 		$whereClauseArr[] = "queid in ($queid)";
			$keysArr[] = "queid_".$queid;
	 	}
		if($status != ''){
			$whereClauseArr[] = "status = $status";
			$keysArr[] = "status_".$status;
		}
		if(sizeof($whereClauseArr) > 0){
	 		$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
	 	}
	 	if(!empty($startlimit)){
	 		$limitArr[] = $startlimit;
			$keysArr[] = "startlimit_".$startlimit;
	 	}
	 	if(!empty($count)){
	 		$limitArr[] = $count;
			$keysArr[] = "count_".$count;
	 	}
	 	if(sizeof($limitArr) > 0){
	 		$limitStr = " limit ".implode(" , ",$limitArr);
	 	}
		$key = implode('_',$keysArr);
		//echo $key."<br>";
		$result = $this->cache->get($key);
		if(!empty($result)){ return $result;}

		$sql = "select * from USER_REVIEW_QUESTIONAIRE $whereClauseStr order by queid asc $limitStr";
		$result = $this->select($sql);
		$this->cache->set($key,$result);
		return $result;
	}
	/**
         * @note function is used to get question answer details list
         *
         * @param an integer/comma seperated answer ids $ans_id.
         * @param an integer/comma seperated question ids $queid.
         * @param boolean Active/InActive $status.
         * @param integer $startlimit.
         * @param integer $count.
         *
         * @pre not required.
         *
         * @post question answer details in associative array.
         * retun an array.
         */

	function arrGetQueAnswer($ans_id="",$queid="",$status="1",$startlimit="",$count=""){
		$keysArr[] = $this->userreviewkey."_answer";
		if(is_array($ans_id)){
	 		$ans_id = implode(",",$ans_id);
	 	}
		if(!empty($ans_id)){
	 		$whereClauseArr[] = "ans_id in ($ans_id)";
			$keysArr[] = "ans_id_".$ans_id;
	 	}
		if(is_array($queid)){
	 		$queid = implode(",",$queid);
	 	}
		if(!empty($queid)){
	 		$whereClauseArr[] = "queid in ($queid)";
			$keysArr[] = "queid_".$queid;
	 	}
		if($status != ''){
			$whereClauseArr[] = "status = $status";
			$keysArr[] = "status_".$status;
		}
		if(sizeof($whereClauseArr) > 0){
	 		$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
	 	}
	 	if(!empty($startlimit)){
	 		$limitArr[] = $startlimit;
			$keysArr[] = "startlimit_".$startlimit;
	 	}
	 	if(!empty($count)){
	 		$limitArr[] = $count;
			$keysArr[] = "count_".$count;
	 	}
	 	if(sizeof($limitArr) > 0){
	 		$limitStr = " limit ".implode(" , ",$limitArr);
	 	}
		$key = implode('_',$keysArr);
		//echo $key."<br>";
		$result = $this->cache->get($key);
		if(!empty($result)){ return $result;}
		$sql = "select * from USER_REVIEW_QUESTIONAIRE_ANSWER $whereClauseStr order by ans_id asc $limitStr";
		//echo $sql;exit;
		$result = $this->select($sql);
		$this->cache->set($key,$result);
		return $result;
	}
	/**
	* @note function is used to get user review details list
	*
	* @param an integer/comma seperated user review ids/user review ids array $user_review_id.
	* @param an integer/comma seperated uids $uid.
	* @param string $user_name.
	* @param string $email.
	* @param string $location.
	* @param an integer/comma seperated brand_ids $brand_id.
	* @param an integer/comma seperated category_ids $category_id.
	* @param an integer/comma seperated product_info_ids $product_info_id.
	* @param an integer/comma seperated product_ids $product_id.
	* @param boolean Active/InActive $status.
	* @param integer $startlimit.
	* @param integer $cnt.
	* @param string $orderby.
	*
	* @pre not required.
	*
	* @post user review details in associative array.
	* retun an array.
	*/
	function arrGetUserReviewDetails($user_review_id="",$uid="",$user_name="",$email="",$location="",$brand_id="",$category_id="",$product_info_id="",$product_id="",$status="1",$startlimit="",$cnt="",$orderby=""){
		$keysArr[] = $this->userreviewkey;
		if(is_array($user_review_id)){
			$user_review_id = implode(",",$user_review_id);
		}

		if(!empty($user_review_id)){
			$whereClauseArr[] = "user_review_id in ($user_review_id)";
			$keysArr[] = "user_review_id_".$user_review_id;
		}

		if(!empty($uid)){
			$whereClauseArr[] = "uid in ($uid)";
			$keysArr[] = "uid_".$uid;
		}

		if(!empty($user_name)){
			$whereClauseArr[] = "lower(user_name) = '".strtolower($user_name)."'";
			$keysArr[] = "user_name_".strtolower($user_name);
		}
		if(!empty($email)){
			$whereClauseArr[] = "email = '".$email."'";
			$keysArr[] = "email_".$email;
		}
		if(!empty($location)){
			$whereClauseArr[] = "lower(locate) = '".strtolower($location)."'";
			$keysArr[] = "locate_".strtolower($location);
		}
		if(!empty($brand_id)){
			$whereClauseArr[] = "brand_id in ($brand_id)";
			$keysArr[] = "brand_id_".$brand_id;
		}
		if(!empty($category_id)){
			$whereClauseArr[] = "category_id in ($category_id)";
			$keysArr[] = "category_id_".$category_id;
		}
		if(!empty($product_info_id)){
			$whereClauseArr[] = "product_info_id in ($product_info_id)";
			$keysArr[] = "product_info_id_".$product_info_id;
		}
		if(!empty($product_id)){
			$whereClauseArr[] = "product_id in ($product_id)";
			$keysArr[] = "product_id_".$product_id;
		}
		if($status != ''){
			$whereClauseArr[] = "status = $status";
			$keysArr[] = "status_".$status;
		}
		if($orderby != ''){
			$orderby = $orderby;
			$keysArr[] ="order_".str_replace(" ","_",$orderby);
		}
		if(!empty($startlimit)){
			$limitArr[] = $startlimit;
			$keysArr[] = "startlimit_".$startlimit;
		}
		if(!empty($cnt)){
			$limitArr[] = $cnt;
			$keysArr[] = "cnt_".$cnt;
		}
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = ' where '.implode(' and ',$whereClauseArr);
		}
		if(sizeof($limitArr) > 0){
			$limitStr = ' limit '.implode(',',$limitArr);
		}
		if(empty($orderby)){
			$orderby = "order by create_date desc";
			$keysArr[] = "order_".str_replace(" ","_",$orderby);
		}
		$key = implode('_',$keysArr);
		//echo $key."<br>";
		$result = $this->cache->get($key);
		if(!empty($result)){ return $result;}

		$sql = "select * from USER_REVIEW $whereClauseStr $orderby $limitStr";
		//echo $sql;
		$result = $this->select($sql);
		$this->cache->set($key,$result);
		return $result;
	}
	/**
	 * @note function is used to get distinct model user review details list
	 *
	 * @param an integer/comma seperated user review ids/user review ids array $user_review_id.
	 * @param an integer/comma seperated uids $uid.
	 * @param string $user_name.
	 * @param string $email.
	 * @param string $location.
	 * @param an integer/comma seperated brand_ids $brand_id.
	 * @param an integer/comma seperated category_ids $category_id.
	 * @param an integer/comma seperated product_info_ids $product_info_id.
	 * @param an integer/comma seperated product_ids $product_id.
	 * @param boolean Active/InActive $status.
	 * @param integer $startlimit.
	 * @param integer $cnt.
	 * @param string $orderby.
	 *
	 * @pre not required.
	 *
	 * @post distinct model user review details in associative array.
	 * retun an array.
	 */
	function getDistinctModelUserReviews($user_review_id="",$uid="",$user_name="",$email="",$location="",$brand_id="",$category_id="",$product_info_id="",$product_id="",$status="1",$startlimit="",$cnt="",$orderby="order by user_review_id"){
		$keysArr[] = $this->userreviewkey."_model";
		if(is_array($user_review_id)){
			$user_review_id = implode(",",$user_review_id);
		}

		if(!empty($user_review_id)){
			$whereClauseArr[] = "user_review_id in ($user_review_id)";
			$keysArr[] ="user_review_id_".$user_review_id;
		}

		if(!empty($uid)){
			$whereClauseArr[] = "uid in ($uid)";
			$keysArr[] = "uid_".$uid;
		}

		if(!empty($user_name)){
			$whereClauseArr[] = "lower(user_name) = '".strtolower($user_name)."'";
			$keysArr[] = "user_name_".strtolower($user_name);
		}
		if(!empty($email)){
			$whereClauseArr[] = "email = '".$email."'";
			$keysArr[] = "email_".$email;
		}
		if(!empty($location)){
			$whereClauseArr[] = "lower(locate) = '".strtolower($location)."'";
			$keysArr[] = "locate_".strtolower($location);
		}
		if(!empty($brand_id)){
			$whereClauseArr[] = "R.brand_id in ($brand_id)";
			$keysArr[] = "brand_id_".$brand_id;
		}
		if(!empty($category_id)){
			$whereClauseArr[] = "R.category_id in ($category_id)";
			$keysArr[] = "category_id_".$category_id;
		}
		if(!empty($product_info_id)){
			$whereClauseArr[] = "R.product_info_id in ($product_info_id)";
			$keysArr[] = "product_info_id_".$product_info_id;
		}
		if(!empty($product_id)){
			$whereClauseArr[] = "R.product_id in ($product_id)";
			$keysArr[] = "product_id_".$product_id;
		}
		if($status != ''){
			$whereClauseArr[] = "R.status = $status";
			$keysArr[] = "status_".$status;

		}
		if($orderby != ''){
			$orderby = $orderby;
			$keysArr[] = "order_".str_replace(" ","_",$orderby);
		}
		if(!empty($startlimit)){
			$limitArr[] = $startlimit;
			$keysArr[] = "startlimit_".$startlimit;
		}
		if(!empty($cnt)){
			$limitArr[] = $cnt;
			$keysArr[] = "cnt_".$cnt;
		}
		$whereClauseArr[] = "R.product_info_id = PR.`product_name_id`";
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = ' where '.implode(' and ',$whereClauseArr);
		}
		if(sizeof($limitArr) > 0){
			$limitStr = ' limit '.implode(',',$limitArr);
		}

		$key = implode('_',$keysArr);
		//echo $key."<br>";
		$result = $this->cache->get($key);
		if(!empty($result)){ return $result;}
		$sql = "select DISTINCT (product_info_id), PR.`product_info_name`,PR.`brand_id` FROM USER_REVIEW R, PRODUCT_NAME_INFO PR $whereClauseStr $orderby $limitStr";
		//echo $sql;
		$result = $this->select($sql);
		$this->cache->set($key,$result);
		return $result;
	}
	/**
	 * @note function is used to get distinct model user review count
	 *
	 * @param an integer/comma seperated user review ids/user review ids array $user_review_id.
	 * @param an integer/comma seperated uids $uid.
	 * @param string $user_name.
	 * @param string $email.
	 * @param string $location.
	 * @param an integer/comma seperated brand_ids $brand_id.
	 * @param an integer/comma seperated category_ids $category_id.
	 * @param an integer/comma seperated product_info_ids $product_info_id.
	 * @param an integer/comma seperated product_ids $product_id.
	 * @param boolean Active/InActive $status.
	 * @param integer $startlimit.
	 * @param integer $cnt.
	 * @param string $orderby.
	 *
	 * @pre not required.
	 *
	 * @post integer distinct model user review count
	 * retun an integer.
	 */
	function getDistinctModelUserReviewsCount($user_review_id="",$uid="",$user_name="",$email="",$location="",$brand_id="",$category_id="",$product_info_id="",$product_id="",$status="1",$startlimit="",$cnt="",$orderby="order by user_review_id"){
		$keysArr[] = $this->userreviewkey."_model_cnt";
		if(is_array($user_review_id)){
			$user_review_id = implode(",",$user_review_id);
		}
		if(!empty($user_review_id)){
			$whereClauseArr[] = "user_review_id in ($user_review_id)";
			$keysArr[] = "user_review_id_".$user_review_id;
		}
		if(!empty($uid)){
			$whereClauseArr[] = "uid in ($uid)";
			$keysArr[] = "uid_".$uid;
		}
		if(!empty($user_name)){
			$whereClauseArr[] = "lower(user_name) = '".strtolower($user_name)."'";
			$keysArr[] = "user_name_".strtolower($user_name);
		}
		if(!empty($email)){
			$whereClauseArr[] = "email = '".$email."'";
			$keysArr[] = "email_".$email;
		}
		if(!empty($location)){
			$whereClauseArr[] = "lower(locate) = '".strtolower($location)."'";
			$keysArr[] = "locate_".strtolower($location);
		}
		if(!empty($brand_id)){
			$whereClauseArr[] = "R.brand_id in ($brand_id)";
			$keysArr[] = "brand_id_".$brand_id;
		}
		if(!empty($category_id)){
			$whereClauseArr[] = "R.category_id in ($category_id)";
			$keysArr[] = "category_id_".$category_id;
		}
		if(!empty($product_info_id)){
			$whereClauseArr[] = "R.product_info_id in ($product_info_id)";
			$keysArr[] = "product_info_id_".$product_info_id;
		}
		if(!empty($product_id)){
			$whereClauseArr[] = "R.product_id in ($product_id)";
			$keysArr[] = "product_id_".$product_id;
		}
		if($status != ''){
			$whereClauseArr[] = "R.status = $status";
			$keysArr[] = "status_".$status;
		}
		if($orderby != ''){
			$orderby = $orderby;
			$keysArr[] = "order_".str_replace(" ","_",$orderby);
		}
		if(!empty($startlimit)){
			$limitArr[] = $startlimit;
			$keysArr[] = "startlimit_".$startlimit;
		}
		if(!empty($cnt)){
			$limitArr[] = $cnt;
			$keysArr[] = "cnt_".$cnt;
		}
		$whereClauseArr[] = "R.product_info_id = PR.`product_name_id`";
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = ' where '.implode(' and ',$whereClauseArr);
		}
		if(sizeof($limitArr) > 0){
			$limitStr = ' limit '.implode(',',$limitArr);
		}
		$key = implode('_',$keysArr);
		//echo $key."<br>";
		$result = $this->cache->get($key);
		if(!empty($result)){  return $result[0]['cnt'];}

		$sql = "select count(DISTINCT (product_info_id)) as cnt FROM USER_REVIEW R, PRODUCT_NAME_INFO PR $whereClauseStr";
		//echo $sql;
		$result = $this->select($sql);
		$this->cache->set($key,$result);
		$count =  $result[0]['cnt'];
		//die();
		return $count;
	}

		/**
		 * @note function is used to get user reviews details count
		 *
		 * @param an integer/comma seperated user review ids/user review ids array $user_review_id.
		 * @param an integer/comma seperated uids $uid.
		 * @param string $user_name.
		 * @param string $email.
		 * @param string $location.
		 * @param an integer/comma seperated brand_ids $brand_id.
		 * @param an integer/comma seperated category_ids $category_id.
		 * @param an integer/comma seperated product_info_ids $product_info_id.
		 * @param an integer/comma seperated product_ids $product_id.
		 * @param boolean Active/InActive $status.
		 * @param integer $startlimit.
		 * @param integer $cnt.
		 * @param string $orderby.
		 *
		 * @pre not required.
		 *
		 * @post integer user reviews details count
		 * retun an integer.
		 */
	function arrGetUserReviewDetailsCount($user_review_id="",$uid="",$user_name="",$email="",$location="",$brand_id="",$category_id="",$product_info_id="",$product_id="",$status="1"){
		$keysArr[] = $this->userreviewkey."_cnt";
		if(is_array($user_review_id)){
			$user_review_id = implode(",",$user_review_id);
		}

		if(!empty($user_review_id)){
			$whereClauseArr[] = "user_review_id in ($user_review_id)";
			$keysArr[] = "user_review_id_".$user_review_id;
		}

		if(!empty($uid)){
			$whereClauseArr[] = "uid in ($uid)";
			$keysArr[] = "uid_".$uid;
		}

		if(!empty($user_name)){
			$whereClauseArr[] = "lower(user_name) = '".strtolower($user_name)."'";
			$keysArr[] = "user_name_".strtolower($user_name);
		}
		if(!empty($email)){
			$whereClauseArr[] = "email = '".$email."'";
			$keysArr[] = "email_".$email;
		}
		if(!empty($location)){
			$whereClauseArr[] = "lower(locate) = '".strtolower($location)."'";
			$keysArr[] = "locate_".strtolower($location);
		}
		if(!empty($brand_id)){
			$whereClauseArr[] = "brand_id in ($brand_id)";
			$keysArr[] = "brand_id_".$brand_id;
		}
		if(!empty($category_id)){
			$whereClauseArr[] = "category_id in ($category_id)";
			$keysArr[] = "category_id_".$category_id;
		}
		if(!empty($product_info_id)){
			$whereClauseArr[] = "product_info_id in ($product_info_id)";
			$keysArr[] = "product_info_id_".$product_info_id;
		}
		if(!empty($product_id)){
			$whereClauseArr[] = "product_id in ($product_id)";
			$keysArr[] = "product_id_".$product_id;
		}
		if($status != ''){
			$whereClauseArr[] = "status = $status";
			$keysArr[] = "status_".$status;
		}

		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = ' where '.implode(' and ',$whereClauseArr);
		}

		$key = implode('_',$keysArr);
		//echo $key."<br>";
		$result = $this->cache->get($key);
		if(!empty($result)){ return $result[0]['cnt'];}

		$sql = "select count(user_review_id) as cnt from USER_REVIEW $whereClauseStr";
		//echo $sql."<br>";
		$result = $this->select($sql);
		$this->cache->set($key,$result);
		return $result[0]['cnt'];
		//return $resultcnt;
	}
	/**
	* @note function is used to get user question and answer details
	*
	* @param an integer/comma seperated user review answers ids $usr_review_ans_id.
	* @param an integer/comma seperated questions ids $que_id.
	* @param an integer/comma seperated user review ids $user_review_id.
	* @param an integer $is_rating.
	* @param an integer $is_comment_ans.
	* @param integer $startlimit.
	* @param integer $cnt.
	*
	* @pre not required.
	*
	* @post user question and answer details in associative array.
	* retun an array.
	*/
	function arrGetUserQnA($usr_review_ans_id='',$que_id='',$user_review_id='',$is_rating="",$is_comment_ans="",$startlimit="",$cnt=""){
		$keysArr[] = $this->userreviewkey."_qna";
		if(!empty($usr_review_ans_id)){
			$whereClauseArr[] = "usr_review_ans_id in ($usr_review_ans_id)";
			$keysArr[] = "usr_review_ans_id_".$usr_review_ans_id;
		}
		if(!empty($que_id)){
			$whereClauseArr[] = "que_id in ($que_id)";
			$keysArr[] = "que_id_".$que_id;
		}
		if(!empty($user_review_id)){
			$whereClauseArr[] = "user_review_id in ($user_review_id)";
			$keysArr[] = "user_review_id_".$user_review_id;
		}
		if(!empty($is_rating)){
			$whereClauseArr[] = "is_rating = $is_rating";
			$keysArr[] = "is_rating_".$is_rating;
		}
		if(!empty($is_comment_ans)){
			$whereClauseArr[] = "is_comment_ans = $is_comment_ans";
			$keysArr[] = "is_comment_ans_".$is_comment_ans;
		}
		if(!empty($startlimit)){
			$limitArr[] = $startlimit;
			$keysArr[] = "startlimit_".$startlimit;
		}
		if(!empty($cnt)){
			$limitArr[] = $cnt;
			$keysArr[] = "cnt_".$cnt;
		}
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = ' where '.implode(' and ',$whereClauseArr);
		}
		if(sizeof($limitArr) > 0){
			$limitStr = ' limit '.implode(',',$limitArr);
		}
		$key = implode('_',$keysArr);
		//echo $key."<br>";
		$result = $this->cache->get($key);
		if(!empty($result)){ return $result;}

		$sql = "select * from USER_REVIEW_ANSWER $whereClauseStr order by usr_review_ans_id $usr_review_ans_id $limitStr";
		//echo $sql;//exit;
		$result = $this->select($sql);
		$this->cache->set($key,$result);
		return $result;
	}
	/**
	 * @note function is used to insert overall ratings into the database.
	 * @param an associative array $insert_param.
	 * @pre $insert_param must be valid associative array.
	 * @post an integer $answer_id.
	 * retun integer.
	 */
	function intInsertOverallRating($insert_param){
		$insert_param['create_date'] = date('Y-m-d H:i:s');
		$insert_param['update_date'] = date('Y-m-d H:i:s');
		$sql = $this->getInsertSql("USER_OVERALL_RATING",array_keys($insert_param),array_values($insert_param));
		//echo $sql;exit;
		$answer_id = $this->insert($sql);
		$this->cache->searchDeleteKeys($this->userreviewkey);
		if($answer_id == 'Duplicate entry'){ return 'exists';}
		return $answer_id;
	}
	/**
	* @note function is used to get overall grade details
	*
	* @param an integer $category_id.
	* @param an integer $brand_id.
	* @param an integer $product_id.
	* @param an integer $model_id.
	* @param an integer $user_review_id.
	* @param boolean Active/InActive $status.
	*
	* @pre not required.
	*
	* @post overall grade details in associative array.
	* retun an array.
	*/
	function arrGetOverallGrade($category_id,$brand_id='',$product_id='',$model_id='',$status="1",$user_review_id=""){
		$result = $this->arrGetUserReviewDetails($user_review_id,"","","","",$brand_id,$category_id,$model_id,$product_id,$status);
		//print"<pre>";print_r($result);print"</pre>";exit;
		$cnt = sizeof($result);
		for($i=0;$i<$cnt;$i++){
			$usrReviewIdsArr[] = $result[$i]['user_review_id'];
		}
		$user_review_ids = implode(",",$usrReviewIdsArr);
		if(!empty($user_review_ids)){
			$keysArr[] = $this->userreviewkey."_user_overall_grade";
			$keysArr[] = "user_review_id_".$user_review_ids;
			$key = implode('_',$keysArr);
			//echo $key."<br>";
			$result = $this->cache->get($key);
			if(!empty($result)){ return $result;}

			$sql = "select sum(overallgrade) AS totaloverallcnt, avg(`overallgrade`) AS overallavg from USER_OVERALL_RATING where user_review_id in ($user_review_ids)";
			//echo $sql;
			$result = $this->select($sql);
			$this->cache->set($key,$result);
			return $result;
		}
		return $result;
	}

	/**
	* @note function is used to get admin overall grade details
	*
	* @param an integer/comma seperated category ids $category_id.
	* @param an integer/comma seperated brand ids $brand_id.
	* @param an integer/comma seperated product ids $product_id
	* @param an integer/comma seperated model ids $model_id
	* @param boolean Active/InActive $status.
	* @param integer $startlimit.
	* @param integer $cnt.
	*
	* @pre not required.
	*
	* @post admin overall grade details in associative array.
	* retun an array.
	*/
	function arrGetAdminOverallGrade($category_id,$brand_id='',$product_id='',$model_id='',$status='1',$startlimit="",$cnt=""){
		$keysArr[] = $this->userreviewkey."_admin_overall_grade";
		if(!empty($category_id)){
			$whereClauseArr[] = "category_id in ($category_id)";
			$keysArr[] = "category_id_".$category_id;
		}
		if(!empty($brand_id)){
			$whereClauseArr[] = "brand_id in ($brand_id)";
			$keysArr[] = "brand_id_".$brand_id;
		}
		if($product_id != ''){
			$whereClauseArr[] = "product_id in ($product_id)";
			$keysArr[] = "product_id_".$product_id;
		}
		if(!empty($model_id)){
			$whereClauseArr[] = "product_info_id in ($model_id)";
			$keysArr[] = "product_info_id_".$model_id;
		}
		if($status != ''){
			$whereClauseArr[] = "status = $status";
			$keysArr[] = "status_".$status;
		}
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = ' where '.implode(' and ',$whereClauseArr);
		}
		if(sizeof($limitArr) > 0){
			$limitStr = ' limit '.implode(',',$limitArr);
		}
		$key = implode('_',$keysArr);
		//echo $key."<br>";
		$result = $this->cache->get($key);
		if(!empty($result)){ return $result;}
		$sql = "select * from ADMIN_OVERALL_RATING $whereClauseStr $limitStr";
		//echo $sql."<br>";
		$result = $this->select($sql);
		$this->cache->set($key,$result);
		return $result;
	}
	/**
	 * @note function is used to delete admin overall ratings.
	 * @param integer $admin_rating_id
	 * @pre $admin_rating_id must be non-empty/zero valid integer.
	 * @post boolean true/false.
	 * return boolean.
	 */
	function boolAdminOverallRating($admin_rating_id){
		$sql = "delete from ADMIN_OVERALL_RATING where admin_rating_id = $admin_rating_id";
		$result = $this->sql_delete_data($sql);
		$this->cache->searchDeleteKeys($this->userreviewkey);
		return $result;
	}
	/**
         * @note function is used to update admin overall ratings in the database.
         * @param an associative array $update_param.
         * @param an integer $admin_rating_id.
         * @pre $update_param must be valid associative array.
         * @post an integer $admin_rating_id.
         * retun integer.
         */
	function boolUpdateAdminOverallRating($admin_rating_id,$update_param){
		$update_param['update_date'] = date('Y-m-d H:i:s');
		$sql = $this->getUpdateSql("ADMIN_OVERALL_RATING",array_keys($update_param),array_values($update_param),"admin_rating_id",$admin_rating_id);
		$isUpdate = $this->update($sql);
		$this->cache->searchDeleteKeys($this->userreviewkey);
		return $isUpdate;
	}
	/**
         * @note function is used to insert admin overall ratings into the database.
         * @param an associative array $insert_param.
         * @pre $insert_param must be valid associative array.
         * @post an integer $answer_id.
         * retun integer.
         */
	function intInsertAdminOverallRating($insert_param){
		$update_param['update_date'] = date('Y-m-d H:i:s');
		$insert_param['create_date'] = date('Y-m-d H:i:s');
		$sql = $this->getInsertSql("ADMIN_OVERALL_RATING",array_keys($insert_param),array_values($insert_param));
		$answer_id = $this->insert($sql);
		$this->cache->searchDeleteKeys($this->userreviewkey);
		if($answer_id == 'Duplicate entry'){ return 'exists';}
		return $answer_id;
	}
	 /**
         * @note function is used to get user review questions details
         *
         * @param an integer/comma seperated questions ids/questions ids array $queid.
         * @param an integer/comma seperated category ids $category_id.
         * @param integer $startlimit.
         * @param integer $cnt.
         *
         * @pre not required.
         *
         * @post user review questions details in associative array.
         * retun an array.
         */
	 function arrGetUserReviewQue($queid="",$category_id="",$startlimit="",$count="") {
	 	$keysArr[] = $this->userreviewkey."_user_review_que";
		if(is_array($queid)){
	 		$queid = implode(",",$queid);
	 	}
	 	if(!empty($queid)){
	 		$whereClauseArr[] = "USER_REVIEW_QUESTIONAIRE_ANSWER.queid in ($queid)";
			$whereClauseArr[] = "USER_REVIEW_QUESTIONAIRE.queid = USER_REVIEW_QUESTIONAIRE_ANSWER.queid";
			$keysArr[] = "queid_".$queid;

	 	}
	 	if(!empty($category_id)){
	 		$whereClauseArr[] = "USER_REVIEW_QUESTIONAIRE.category_id in ($category_id)";
			$keysArr[] = "category_id_".$category_id;
	 	}
	 	if(sizeof($whereClauseArr) > 0){
	 		$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
	 	}
	 	if(!empty($startlimit)){
	 		$limitArr[] = $startlimit;
			$keysArr[] = "startlimit_".$startlimit;
	 	}
	 	if(!empty($count)){
	 		$limitArr[] = $count;
			$keysArr[] = "count_".$count;
	 	}
	 	if(sizeof($limitArr) > 0){
	 		$limitStr = " limit ".implode(" , ",$limitArr);
	 	}
		//$sql = "select Q.queid,Q.category_id,Q.quename,Q.uid,Q.create_date,Q.update_date,A.ans_id,A.ans from USER_REVIEW_QUESTIONAIRE Q left join USER_REVIEW_QUESTIONAIRE_ANSWER A on  Q.queid=A.queid $whereClauseStr $limitStr order by Q.queid";

		$key = implode('_',$keysArr);
		//echo $key."<br>";
		$result = $this->cache->get($key);
		if(!empty($result)){ return $result;}

		$sql = "select USER_REVIEW_QUESTIONAIRE.*,USER_REVIEW_QUESTIONAIRE_ANSWER.* from USER_REVIEW_QUESTIONAIRE,USER_REVIEW_QUESTIONAIRE_ANSWER $whereClauseStr order by USER_REVIEW_QUESTIONAIRE.queid asc $limitStr";
		//echo $sql;exit;
	 	$result = $this->select($sql);
		$this->cache->set($key,$result);
	 	return $result;
	 }
	 /**
         * @note function is used to get feedback subjects details
         *
	 * @param boolean Active/InActive $status.
         * @param integer $startlimit.
         * @param integer $cnt.
         *
         * @pre not required.
         *
         * @post feedback subjects details in associative array.
         * retun an array.
         */
	 function arrGetFeedbackSubject($status="1",$startlimit="",$count=""){
		$keysArr[] = $this->userreviewkey."_feedback_sub";
		if($status != ''){
			$whereClauseArr[] = "status = $status";
			$keysArr[] = "status_".$status;
		}
		if(sizeof($whereClauseArr) > 0){
	 		$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
	 	}
	 	if(!empty($startlimit)){
	 		$limitArr[] = $startlimit;
			$keysArr[] = "startlimit_".$startlimit;
	 	}
	 	if(!empty($count)){
	 		$limitArr[] = $count;
			$keysArr[] = "count_".$count;
	 	}
	 	if(sizeof($limitArr) > 0){
	 		$limitStr = " limit ".implode(" , ",$limitArr);
	 	}
		$key = implode('_',$keysArr);
		//echo $key."<br>";
		$result = $this->cache->get($key);
		if(!empty($result)){ return $result;}
		$sql = "SELECT subject_id,subject FROM FEEDBACK_SUBJECT $whereClauseStr order by subject_id asc $limitStr";
		$result =$this->select($sql);
		$this->cache->set($key,$result);
		return $result;
	}


  	/**
	 * @note function is used to insert feedback information into the database.
	 * @param an associative array $insert_param.
	 * @pre $insert_param must be valid associative array.
	 * @post an integer $answer_id.
	 * retun integer.
	 */
	function intInsertFeedbackInfo($insert_param){
		$insert_param['create_date'] = date('Y-m-d H:i:s');
		$insert_param['update_date'] = date('Y-m-d H:i:s');
		$sql = $this->getInsertSql("FEEDBACK",array_keys($insert_param),array_values($insert_param));
		$answer_id = $this->insert($sql);
		//$this->cache->searchDeleteKeys($this->userreviewkey);
		if($answer_id == 'Duplicate entry'){ return 'exists';}
		return $answer_id;
	}

	function intInsertUserReviewOptions($review_id,$category_id,$flag){
		$review_type="user_review";
		$likeflag = ($flag == 'y') ? 'like_yes' : 'like_no';
		$sql="select $likeflag from USER_REVIEW_LIKES where category_id= $category_id and review_id=$review_id";
		$result = $this->select($sql);
		if(sizeof($result) > 0){
			//$flagcount = $result['0']['$likeflag']+1;
			$sql = "INSERT INTO USER_REVIEW_LIKES(`category_id`,`review_id`,`create_date`,`update_date`) VALUES ($category_id,$review_id,now(),now()) ON DUPLICATE KEY UPDATE $likeflag = $likeflag+1";
		}else{
			$sql = "INSERT INTO USER_REVIEW_LIKES(`category_id`,`review_id`,`create_date`,`update_date`,$likeflag) VALUES ($category_id,$review_id,now(),now(),1)";
			//$flagcount = 1;
		}
		$result = $this->insert($sql);
		$this->cache->searchDeleteKeys($this->userreviewkey."_userthumb");

		if(!empty($result)){
			$keysArr[] = $this->userreviewkey."_userthumb";
			if(!empty($category_id)){
				$keysArr[] = "category_id_".$category_id;
			}
			if(!empty($review_id)){
				$keysArr[] = "review_id_".$review_id;
			}
			$key = implode('_',$keysArr);
			//echo $key."<br>";
			$result = $this->cache->get($key);
			if(!empty($result)){ return $result['0'][$likeflag];}

			$sql="select * from USER_REVIEW_LIKES where category_id= $category_id and review_id=$review_id";
			$result = $this->select($sql);
			//print_r($result);
			//$key   = ($review_type.$review_id.$category_id);
			//$result = serialize($result);
		}
		$flagcount = $result['0'][$likeflag];
		return $flagcount;
	}

	function GetUserReviewOptions($id="",$review_id="",$category_id="",$flag="",$startlimit="",$count=""){
		$keysArr[] = $this->userreviewkey."_option";
		$review_type="user_review";
		if(is_array($review_id)){
			$review_id = implode(",",$review_id);
		}
		if(!empty($review_id)){
			$whereClauseArr[] = "review_id in ( $review_id )";
			$keysArr[] = "review_id_".$review_id;
		}
		if(!empty($category_id)){
			$whereClauseArr[] = "category_id = $category_id";
			$keysArr[] = "category_id_".$category_id;
		}
		if(!empty($flag)){
			$whereClauseArr[] = "flag = $flag";
			$keysArr[] = "flag_".$flag;
		}
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
		}
		if(!empty($startlimit)){
			$limitArr[] = $startlimit;
			$keysArr[] = "startlimit_".$startlimit;
		}
		if(!empty($count)){
			$limitArr[] = $count;
			$keysArr[] = "count_".$count;
		}
		if(sizeof($limitArr) > 0){
			$limitStr = " limit ".implode(" , ",$limitArr);
		}

		$key = implode('_',$keysArr);
		//echo $key."<br>";
		$result = $this->cache->get($key);
		if(!empty($result)){ return $result;}
		//$orderby = "order by create_date";
		$sql="SELECT * , DATE_FORMAT(create_date,'%d %b %Y') as disp_date FROM USER_REVIEW_LIKES $whereClauseStr $limitStr";
		//echo $sql."<br>";
		$result = $this->select($sql);
		$this->cache->set($key, $result);
		return $result;
	}

	/**
	 * @note function is used to update user review answer into the database.
	 * @param an associative array $update_param.
	 * @param is an integer $usr_review_ans_id.
	 * @pre $insert_param must be valid associative array.
	 * retun integer.
	 */

	function intInsertUpdateUserReviewWidget($aParameters,$sTableName){
		$aParameters['create_date'] = date('Y-m-d H:i:s');
		$aParameters['update_date'] = date('Y-m-d H:i:s');
		$sSql=$this->getInsertUpdateSql($sTableName,array_keys($aParameters),array_values($aParameters));
		//echo "TEST---".$sSql."<br>";    //die();
		$iRes=$this->insertUpdate($sSql);
		$this->cache->searchDeleteKeys($this->userreviewkey);
		return $iRes;
     }

	/**
	* @note function is used to delete user review widget.
	* @param integer $id.
	* @pre $id must be non-empty/zero valid integer.
	* @post boolean true/false.
	* return boolean.
	*/
	function boolDeleteUserReviewWidget($id=""){
		$sSql="delete from USER_REVIEW_WIDGET where id='".$id."'";
		//echo $sSql;
		$iRes=$this->sql_delete_data($sSql);
		$this->cache->searchDeleteKeys($this->userreviewkey);
		return $iRes;
	}

	function arrGetUserReviewWidget($id="",$brand_id="",$product_info_id="",$category_id="",$status="1",$startlimit="",$cnt=""){
		$keysArr[] = $this->userreviewkey."_widget";
		if(!empty($id)){
			$whereClauseArr[] = "id = $id";
			$keysArr[] = "id_".$id;
		}
		if(!empty($brand_id)){
			$whereClauseArr[] = " brand_id = $brand_id";
			$keysArr[] = "brand_id_".$brand_id;
		}
		if(!empty($category_id)){
			$whereClauseArr[] = "category_id = $category_id";
			$keysArr[] = "category_id_".$category_id;
		}
		if(!empty($product_info_id)){
			$whereClauseArr[] = "product_info_id= $product_info_id";
			$keysArr[] = "product_info_id_".$product_info_id;
		}
		if($status != ''){
			$whereClauseArr[] = " status = $status";
			$keysArr[] = "status_".$status;
		}
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
		}
		if(!empty($startlimit)){
			$limitArr[] = $startlimit;
			$keysArr[] = "startlimit_".$startlimit;
		}
		if(!empty($count)){
			$limitArr[] = $cnt;
			$keysArr[] = "cnt_".$cnt;
		}
		if(sizeof($limitArr) > 0){
			$limitStr = " limit ".implode(" , ",$limitArr);
		}

		$key = implode('_',$keysArr);
		//echo $key."<br>";
		$result = $this->cache->get($key);
		if(!empty($result)){ return $result;}

		$orderby = "order by create_date desc";
		$sSql="SELECT * , DATE_FORMAT(create_date,'%d %b %Y') as disp_date FROM USER_REVIEW_WIDGET $whereClauseStr $orderby $limitStr";
        $result = $this->select($sSql);
		//echo $sSql."<br>";
		$this->cache->set($key, $result);
		print_r($aRes);
		return $result;
	}
	/**
	 * @note function is used to delete admin expert ratings.
	 * @param integer $admin_rating_id
	 * @pre $admin_rating_id must be non-empty/zero valid integer.
	 * @post boolean true/false.
	 * return boolean.
	 */
	function boolDeleteAdminExpertRating($expert_rating_id){
		$sql = "delete from EXPERT_OVERALL_RATING where expert_rating_id = $expert_rating_id";
		$result = $this->sql_delete_data($sql);
		$this->cache->searchDeleteKeys($this->userreviewkey);
		return $result;
	}
	/**
	 * @note function is used to update admin expert ratings in the database.
	 * @param an associative array $update_param.
	 * @param an integer $admin_rating_id.
	 * @pre $update_param must be valid associative array.
	 * @post an integer $admin_rating_id.
	 * retun integer.
	 */
	function boolUpdateAdminExpertRating($expert_rating_id,$update_param){
		$update_param['update_date'] = date('Y-m-d H:i:s');
		$sql = $this->getUpdateSql("EXPERT_OVERALL_RATING",array_keys($update_param),array_values($update_param),"expert_rating_id",$expert_rating_id);

		$isUpdate = $this->update($sql);
		$this->cache->searchDeleteKeys($this->userreviewkey);
		return $isUpdate;
	}
	/**
	 * @note function is used to insert admin expert ratings into the database.
	 * @param an associative array $insert_param.
	 * @pre $insert_param must be valid associative array.
	 * @post an integer $answer_id.
	 * retun integer.
	 */
	function intInsertAdminExpertRating($insert_param){
		$update_param['update_date'] = date('Y-m-d H:i:s');
		$insert_param['create_date'] = date('Y-m-d H:i:s');
		$sql = $this->getInsertSql("EXPERT_OVERALL_RATING",array_keys($insert_param),array_values($insert_param));
		//	echo $sql;exit;
		$answer_id = $this->insert($sql);
		$this->cache->searchDeleteKeys($this->userreviewkey);
		if($answer_id == 'Duplicate entry'){ return 'exists';}
		return $answer_id;
	}
	/**
	* @note function is used to get admin expert grade details
	* @param an integer/comma seperated category ids $category_id.
	* @param an integer/comma seperated brand ids $brand_id.
	* @param an integer/comma seperated product ids $product_id
	* @param an integer/comma seperated model ids $model_id
	* @param boolean Active/InActive $status.
	* @param integer $startlimit.
	* @param integer $cnt.
	*
	* @pre not required.
	*
	* @post admin overall grade details in associative array.
	* retun an array.
	*/
	function arrGetAdminExpertGrade($category_id,$brand_id='',$product_id='',$model_id='',$status='1',$startlimit="",$cnt=""){
		$keysArr[] = $this->userreviewkey."_adminexpertgrade";
		if(!empty($category_id)){
			$whereClauseArr[] = "category_id in ($category_id)";
			$keysArr[] = "category_id_".$category_id;
		}
		if(!empty($brand_id)){
			$whereClauseArr[] = "brand_id in ($brand_id)";
			$keysArr[] = "brand_id_".$brand_id;
		}
		if($product_id != ''){
			$whereClauseArr[] = "product_id in ($product_id)";
			$keysArr[] = "product_id_".$product_id;
		}
		if(!empty($model_id)){
			$whereClauseArr[] = "product_info_id in ($model_id)";
			$keysArr[] = "product_info_id_".$model_id;
		}
		if($status != ''){
			$whereClauseArr[] = "status = $status";
			$keysArr[] = "status_".$status;
		}
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = ' where '.implode(' and ',$whereClauseArr);
		}
		if(sizeof($limitArr) > 0){
			$limitStr = ' limit '.implode(',',$limitArr);
		}
		$key = implode('_',$keysArr);
		//echo $key."<br>";
		$result = $this->cache->get($key);
		if(!empty($result)){ return $result;}

		$sql = "select *,format((design_rating+performance_rating+user_rating)/3,1) as overallgrade from EXPERT_OVERALL_RATING $whereClauseStr  order by create_date desc $limitStr";
		//echo $sql."<br>";
		$result = $this->select($sql);
		$this->cache->set($key, $result);
		return $result;
	}

	/**
	 * @note function is used to get count of most reviewed product
	 *
	 * @param an integer/comma seperated user review ids/user review ids array $user_review_id.
	 * @param an integer/comma seperated uids $uid.
	 * @param string $user_name.
	 * @param string $email.
	 * @param string $location.
	 * @param an integer/comma seperated brand_ids $brand_id.
	 * @param an integer/comma seperated category_ids $category_id.
	 * @param an integer/comma seperated product_info_ids $product_info_id.
	 * @param an integer/comma seperated product_ids $product_id.
	 * @param boolean Active/InActive $status.
	 * @param integer $startlimit.
	 * @param integer $cnt.
	 * @param string $orderby.
	 *
	 * @pre not required.
	 *
	 * @post most reviewed details in associative array.
	 * retun an array.
	 */
	function getMostReviewedCount($user_review_id="",$uid="",$user_name="",$email="",$location="",$brand_id="",$category_id="",$product_info_id="",$product_id="",$status="1",$startlimit="",$cnt="",$orderby=""){
		$keysArr[] = $this->userreviewkey."_most_cnt";
		if(is_array($user_review_id)){
		   $user_review_id = implode(",",$user_review_id);
		}
		if(!empty($user_review_id)){
		   $whereClauseArr[] = "user_review_id in ($user_review_id)";
		   $keysArr[] = "user_review_id_".$user_review_id;
		}
		if(!empty($uid)){
		   $whereClauseArr[] = "uid in ($uid)";
		   $keysArr[] = "uid_".$uid;
		}
		if(!empty($user_name)){
			$whereClauseArr[] = "lower(user_name) = '".strtolower($user_name)."'";
			$keysArr[] = "user_name_".strtolower($user_name);
		}
		if(!empty($email)){
			$whereClauseArr[] = "email = '".$email."'";
			$keysArr[] = "email_".$email;
		}
		if(!empty($location)){
			$whereClauseArr[] = "lower(locate) = '".strtolower($location)."'";
			$keysArr[] = "locate_".strtolower($location);
		}
		if(!empty($brand_id)){
			$whereClauseArr[] = "brand_id in ($brand_id)";
			$keysArr[] = "brand_id_".$brand_id;
		}
		if(!empty($category_id)){
			$whereClauseArr[] = "category_id in ($category_id)";
			$keysArr[] = "category_id_".$category_id;
		}
		if(!empty($product_info_id)){
			$whereClauseArr[] = "product_info_id in ($product_info_id)";
			$keysArr[] = "product_info_id_".$product_info_id;
		}
		if(!empty($product_id)){
			$whereClauseArr[] = "product_id in ($product_id)";
			$keysArr[] = "product_id_".$product_id;
		}
		if($status != ''){
		   $whereClauseArr[] = "status = $status";
		   $keysArr[] = "status_".$status;
		}
		if($orderby != ''){
		   $orderby = $orderby;
		   $keysArr[] = "order_".str_replace("","_",$orderby);
		}
		if(!empty($startlimit)){
			$limitArr[] = $startlimit;
			$keysArr[] = "startlimit_".$startlimit;
		}
		if(!empty($cnt)){
		   $limitArr[] = $cnt;
		   $keysArr[] = "cnt_".$cnt;
		}
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = ' where '.implode(' and ',$whereClauseArr);
		}
		if(sizeof($limitArr) > 0){
			$limitStr = ' limit '.implode(',',$limitArr);
		}
		if(empty($orderby)){
			$orderby = "ORDER BY cnt DESC";
			$keysArr[] = "order_".str_replace("","_",$orderby);
		}
		$key = implode('_',$keysArr);
		//echo $key."<br>";
		$result = $this->cache->get($key);
		if(!empty($result)){ return $result;}
		$sql = "SELECT count(user_review_id) as cnt, product_info_id,brand_id from USER_REVIEW $whereClauseStr group by product_info_id $orderby $limitStr";
		//echo $sql;
		$result = $this->select($sql);
		$this->cache->set($key, $result);
		return $result;
    }

	/**
	 * @note function is used to get highest rated product
	 *
	 * @param an integer $category_id.
	 * @param an integer $brand_id.
	 * @param an integer $product_id.
	 * @param an integer $model_id.
	 * @param an integer $user_review_id.
	 * @param boolean Active/InActive $status.
	 *
	 * @pre not required.
	 *
	 * @post overall grade details in associative array.
	 * retun an array.
	 */
	function getHighestRatedProduct($category_id='',$brand_id='',$product_id='',$model_id='',$status="1",$user_review_ids=""){
		$keysArr[] = $this->userreviewkey."_highestrated";
		//if(!empty($user_review_ids)){
				//$keysArr[] = "user_review_id_".$user_review_ids;
				$key = implode('_',$keysArr);
				//echo $key."<br>";
				$result = $this->cache->get($key);
				if(!empty($result)){ return $result;}

				//$sql = "select sum(overallgrade) AS totaloverallcnt, avg(`overallgrade`) AS overallavg from USER_OVERALL_RATING where user_review_id in ($user_review_ids)";
				$sql ="SELECT sum(overallgrade) AS totaloverallcnt, avg(overallgrade) AS overallavg, B.product_info_id FROM USER_OVERALL_RATING A, USER_REVIEW B WHERE A.user_review_id = B.user_review_id and B.status = $status GROUP BY B.product_info_id order by overallavg DESC";
				//echo $sql;
				$result = $this->select($sql);
				$this->cache->set($key, $result);
				return $result;
		//}
		return $result;
	}
}
?>
