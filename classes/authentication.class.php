<?php
/**
 * @brief class Contains all the properties and methods related to authentication class.
 * @author Sachin
 * @version 1.0
 * @created 11-Nov-2010 5:09:31 PM
 * @last updated on 08-Mar-2011 13:14:00 PM
 */
class authentication {
	/**
	* constructor used to initialize variables if neeeded
	*/
	public function authentication(){

	}
	/**
	* @note logout
	* used for logout given user session 
	* @param integer iUId - User Id of the user 
	* @access public
	*/
	public function logout($domain){
		setcookie("email", '', time()-3600, "/",$domain);
		setcookie("fname", '', time()-3600, "/",$domain);
		setcookie("lname", '', time()-3600, "/",$domain);
		setcookie("session_id", '', time()-3600, "/",$domain);
		setcookie("uid", '', time()-3600, "/",$domain);
		// unsetting twitter sessions
		setcookie("tt", '', time()-3600, "/",$domain);
		setcookie("tsk", '', time()-3600, "/",$domain);
	}
        /**
        * @note create_keys 
        * used to create keys  
	*
        * Returns md5 hash value of the key.
        */	
	static function create_keys(){
		$key1 = "ai49fnrr9F94bvRjgop4ld8hF$@";
		$key2 = "sfn4r49fcnkj3r3-0FN#(3rjg03";

		//$date = date("dMY");

		return md5($key1.$date)."|".md5($key2.$date);
	}
	/**
        * @note auth_checkSession 
        * used to check authenticated session  
        *
        * Returns 1 if successful,0 if authentication fails.
        */
	public function auth_checkSession($email, $session,$iServiceId,$iUId){
		list($key1, $key2) = explode("|", self::create_keys());
		$email=strtolower($email);
		$chksession = md5($email.$iUId.$key1);
		$chksession = md5($chksession.$key2);
		$sessiontochk = substr($session, 0, 32);
		$time = base64_decode(strrev(substr($session, 32)));
		if(time()-$time < 168*3600 && $chksession == $sessiontochk){
			return 1;
		}else {
			return 0;
		}
 	 }
	/**
        * @note get_facebook_cookie
        * used to get facebook cookie  
        *
	* @param app_id 
	* @param application_secret
	*
        * Returns cookie string.
        */
	function get_facebook_cookie($app_id, $application_secret) {
		$args = array();
		parse_str(trim($_COOKIE['fbs_' . $app_id], '\\"'), $args);
		ksort($args);
		$payload = '';
		foreach ($args as $key => $value) {
		if ($key != 'sig') {
		$payload .= $key . '=' . $value;
		}
		}
		if (md5($payload . $application_secret) != $args['sig']) {
		return null;
		}
		return $args;
	}
	/**
        * @note getFacebookUsersDetails
        * used to get facebook user details
        *
        * @param sAccessToken
        *
        * Returns user details array.
        */
	function getFacebookUsersDetails($sAccessToken){
			$user		= json_decode(file_get_contents("https://graph.facebook.com/me?access_token=$sAccessToken"));
			return $user;
	}
	
	/**
        * @note create_session
        * used to create a session
        *
        * @param request_string
        *
        * Returns the session string.
        */	
	static function create_session($request_string)
	{
		list($email, $uid) = array($request_string['email'], $request_string['uid']);
		$email=strtolower($email);
		list($key1, $key2) = explode("|", self::create_keys());
		$session = md5($email.$uid.$key1);
		$session = md5($session.$key2);
		$session = $session.strrev(base64_encode(time()));
		return $session;
	}
}
?>
