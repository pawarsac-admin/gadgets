<?php
/**************************************************************************************
 * Class: Pager
 * Author: 
 * Methods:
 *         findStart
 *         findPages
 *         pageList
 *         nextPrev
 * Redistribute as you see fit.
 **************************************************************************************/
 class Pager
  {
	/***********************************************************************************
	* int findStart (int limit)
	* Returns the start offset based on $_GET['page'] and $limit
	***********************************************************************************/
	function findStart($limit){
		if ((!isset($_REQUEST['page'])) || ($_REQUEST['page'] <= "1")){
			$start = 0;
			$_REQUEST['page'] = 1;
		}
		else{
			$start = ($_REQUEST['page']-1) * $limit;
		}

		return $start;
	}

	function findStartPage($limit){
		if ((!isset($_REQUEST['pagec'])) || ($_REQUEST['pagec'] <= "1")){
			$start = 0;
			$_REQUEST['pagec'] = 1;
		}
		else{
			$start = ($_REQUEST['pagec']-1) * $limit;
		}
		//echo "START".$start;
		return $start;
	}
	/***********************************************************************************
	* int findPages (int count, int limit)
	* Returns the number of pages needed based on a count and a limit
	***********************************************************************************/
	function findPages($count, $limit){
		$pages = (($count % $limit) == 0) ? $count / $limit : floor($count / $limit) + 1;
		return $pages;
	}
	/***********************************************************************************
	* string pageNumNextPrev (int curpage, int pages)
	* Returns "Previous  Next" for individual pagination
	***********************************************************************************/
	/*public function pageNumNextPrev($curpage, $pages, $siteurl="", $link_type = "")
	{
		$iLimitPages=3;
		if($curpage==1 ){
		      $iStartNo=$curpage;
		}
		elseif($curpage==2 ){
		      $iStartNo=$curpage-1;
		}else{
		    $iStartNo=$curpage-2;
		}

		$iEndNo =($iStartNo+$iLimitPages)-1;
	        if($iEndNo>$pages) $iEndNo=$pages; 	
		$iDiff =$iEndNo-$iStartNo;
		if($iDiff<($iLimitPages-1)) {
		    $iStartNo=$iStartNo-($iLimitPages-$iDiff-1);
		}
		if($iStartNo<1) $iStartNo=1;
        	$next_prev  = "";
		//Adding the optional parameters that have to be passed to the JS function - 2006-Sep-12
		/*if ($qryparams){
		    $qryparams = trim($qryparams);
		    if ($qryparams{0} != ","){
		        $qryparams = "," . $qryparams;
		    }
		}
		$qrparamsarr=explode(',',$qryparams);
		$qryparams=implode("','",$qrparamsarr);*/
		//	Setting up Previous / Next text or image links - 
		/*
		$first_link = ($link_type == "text") ? "First" : "First";
		$prev_link = ($link_type == "text") ? "Prev" :" Prev";
		$next_link = ($link_type == "text") ? "Next" : "Next";
		$last_link = ($link_type == "text") ? "Last" : "Last";
		/

		$first_link = "";
		$prev_link = "Prev";
		$next_link = "Next";
	        $last_link = "";

		if (($curpage-1) <= 0)
		{
		    //$next_prev .= "<span class=\"pagination_text\">".$first_link."</span>";
		    if($curpage >1 )
		    {
		        $next_prev .=  "<span class=\"pagination_text\">".$prev_link."</span>";
		    }
		}
		else
		{
		    //$next_prev .= "<a href=\"".$siteurl."?page=1\" title=\"First\" class=\"pagination_text\">" . $first_link . "</a>";
		    $next_prev .="<a href=\"".$siteurl."?page=".($curpage-1)."\" title=\"Previous\" class=\"vwMr fl bG2e3137\"><span class=\"cfff plr5\">" . $prev_link . "</span></a>";
		}
		/* Print the numeric page list; make the current page unlinked and bold /
		if($curpage+4 < $pages)
		    $pk=$curpage+4;
		else
		    $pk=$pages;
	       //echo $iStartNo;
		for ($i=$iStartNo; $i<=$iEndNo; $i++){
		    if ($i == $curpage){
		        $next_prev .= "<b><span class='sel'>".$i."</span></b>";
		    }
		    else {
		        $next_prev .= "<a href=\"javascript:void(0);\" title=\"Page $i\"  onClick=\"Javascript:" . $jsfunc . "('" . $i . $qryparams . "')" . ";return false;\">" . $i . "</a> ";
		    }
	            $next_prev .= " ";
	        }/
		if (($curpage+1) > $pages)
		{
		    if($curpage < $pages){
		        $next_prev .= "<span class=\"pagination_text\" >".$next_link."</span>";
		    }
		    //$next_prev .=  "<span class=\"pagination_text\">".$last_link."</span> ";
		}
		else
		{
			$next_prev .=  "<a href=\"".$siteurl."?page=".($curpage+1)."\" title=\"Next\" class=\"vwMr fr bG2e3137\"><span class=\"cfff plr5\">" . $next_link . "</span></a> ";
		    	//$next_prev .=  "<a href=\"".$siteurl."?page=".$pages."\" title=\"Last\">" . $last_link . "</a> ";
		}
		$next_prev .= "<div class=\"cb\"></div>";
		return $next_prev;
    }*/
	public function pageNumNextPrev($curpage, $pages, $siteurl="", $param = "")
	{
		//echo $pages."PAGES";
		if(!empty($param)){$param='&tid='.$param;}
		$iLimitPages=3;
		if($curpage==1 ){
		      $iStartNo=$curpage;
		}
		elseif($curpage==2 ){
		      $iStartNo=$curpage-1;
		}else{
		    $iStartNo=$curpage-2;
		}

		$iEndNo =($iStartNo+$iLimitPages)-1;
	        if($iEndNo>$pages) $iEndNo=$pages; 	
		$iDiff =$iEndNo-$iStartNo;
		if($iDiff<($iLimitPages-1)) {
		    $iStartNo=$iStartNo-($iLimitPages-$iDiff-1);
		}
		if($iStartNo<1) $iStartNo=1;
        	$next_prev  = "";				

		$first_link = "";
		$prev_link = "";
		$next_link = "";
	        $last_link = "";

		if (($curpage-1) <= 0)
		{
		    if($curpage >1 )
		    {
		        $next_prev .=  "<a class=\"nxt fl\">".$prev_link."</a>";
		    }
		}
		else
		{
		    $next_prev .="<a href=\"".$siteurl."?page=".($curpage-1).$param."\" title=\"Previous\" class=\"pre fl\">" . $prev_link . "</a>";
		}
		/* Print the numeric page list; make the current page unlinked and bold */
		$next_prev .= "<div class=\"fl\">";
		if($curpage+4 < $pages)
		    $pk=$curpage+4;
		else
		    $pk=$pages;

		for ($i=$iStartNo; $i<=$iEndNo; $i++){
		    if ($i == $curpage){
		        $next_prev .= "<a class='b'>".$i."</a>";
		    }
		    else {
		        $next_prev .= "<a href=\"".$siteurl."?page=".$i.$param."\" title=\"Page $i\" >" . $i . "</a> ";
		    }
	            $next_prev .= " ";
		}
		$next_prev .= "</div>";
		//echo $curpage."----".$pages;
		if (($curpage+1) > $pages)
		{
			if($curpage < $pages){
				$next_prev .= "<a class=\"nxt fl\" >".$next_link." </a>";
			}
		}
		else
		{
			$next_prev .=  "<a href=\"".$siteurl."?page=".($curpage+1).$param."\" title=\"Next\" class=\"nxt fl\">" . $next_link . "</a> ";
		}
		$next_prev .= "<div class=\"cb\"></div>";
		return $next_prev;
    }

	public function pageNumNextPrevVideo($curpage, $pages, $siteurl="", $param = "")
        {
                //if(!empty($param)){$param='&tid='.$param;}
                $iLimitPages=3;
                if($curpage==1 ){
                      $iStartNo=$curpage;
                }
                elseif($curpage==2 ){
                      $iStartNo=$curpage-1;
                }else{
                    $iStartNo=$curpage-2;
                }

                $iEndNo =($iStartNo+$iLimitPages)-1;
                if($iEndNo>$pages) $iEndNo=$pages;
                $iDiff =$iEndNo-$iStartNo;
                if($iDiff<($iLimitPages-1)) {
                    $iStartNo=$iStartNo-($iLimitPages-$iDiff-1);
                }
                if($iStartNo<1) $iStartNo=1;
                $next_prev  = "";      

                $first_link = "";
                $prev_link = "";
                $next_link = "";
                $last_link = "";
		
		if (($curpage-1) <= 0)
                {
                    if($curpage >1 )
                    {
                        $next_prev .=  "<span class=\"pagination_text\">".$prev_link."</span>";
                    }
                }
                else
                {
                    $next_prev .="<a href=\"".$siteurl."&page=".($curpage-1).$param."\" title=\"Previous\" class=\"pre fl\">" . $prev_link . "</a>";
                }
                /* Print the numeric page list; make the current page unlinked and bold */
                $next_prev .= "<div class=\"fl\">";
                if($curpage+4 < $pages)
                    $pk=$curpage+4;
                else
                    $pk=$pages;

                for ($i=$iStartNo; $i<=$iEndNo; $i++){
                    if ($i == $curpage){
                        $next_prev .= "<a class='b'>".$i."</a>";
                    }
                    else {
                        $next_prev .= "<a href=\"".$siteurl."&page=".$i.$param."\" title=\"Page $i\" >" . $i . "</a> ";
                    }
                    $next_prev .= " ";
                }
                $next_prev .= "</div>";
		if (($curpage+1) > $pages)
                {
                    if($curpage < $pages){
                        $next_prev .= "<span class=\"pagination_text\" >".$next_link."</span>";
                    }
                }
                else
                {
                        $next_prev .=  "<a href=\"".$siteurl."&page=".($curpage+1).$param."\" title=\"Next\" class=\"nxt fl\">" . $next_link . "</a> ";
                }
                $next_prev .= "<div class=\"cb\"></div>";
                return $next_prev;
    }



	public function pageNumNextNewPrevVideo($curpage, $pages, $siteurl="", $param = "")
        {
                //if(!empty($param)){$param='&tid='.$param;}
                $iLimitPages=3;
                if($curpage==1 ){
                      $iStartNo=$curpage;
                }
                elseif($curpage==2 ){
                      $iStartNo=$curpage-1;
                }else{
                    $iStartNo=$curpage-2;
                }

                $iEndNo =($iStartNo+$iLimitPages)-1;
                if($iEndNo>$pages) $iEndNo=$pages;
                $iDiff =$iEndNo-$iStartNo;
                if($iDiff<($iLimitPages-1)) {
                    $iStartNo=$iStartNo-($iLimitPages-$iDiff-1);
                }
                if($iStartNo<1) $iStartNo=1;
                $next_prev  = "";      

                $first_link = "";
                $prev_link = "";
                $next_link = "";
                $last_link = "";
		
		if (($curpage-1) <= 0)
                {
                    if($curpage >1 )
                    {
                        $next_prev .=  "<span class=\"pagination_text\">".$prev_link."</span>";
                    }
                }
                else
                {
                    $next_prev .="<a href=\"".$siteurl."&pagec=".($curpage-1).$param."\" title=\"Previous\" class=\"pre fl\">" . $prev_link . "</a>";
                }
                /* Print the numeric page list; make the current page unlinked and bold */
                $next_prev .= "<div class=\"fl\">";
                if($curpage+4 < $pages)
                    $pk=$curpage+4;
                else
                    $pk=$pages;

                for ($i=$iStartNo; $i<=$iEndNo; $i++){
                    if ($i == $curpage){
                        $next_prev .= "<a class='b'>".$i."</a>";
                    }
                    else {
                        $next_prev .= "<a href=\"".$siteurl."&pagec=".$i.$param."\" title=\"Page $i\" >" . $i . "</a> ";
                    }
                    $next_prev .= " ";
                }
                $next_prev .= "</div>";
		if (($curpage+1) > $pages)
                {
                    if($curpage < $pages){
                        $next_prev .= "<span class=\"pagination_text\" >".$next_link."</span>";
                    }
                }
                else
                {
                        $next_prev .=  "<a href=\"".$siteurl."&pagec=".($curpage+1).$param."\" title=\"Next\" class=\"nxt fl\">" . $next_link . "</a> ";
                }
                $next_prev .= "<div class=\"cb\"></div>";
                return $next_prev;
    }

	/***********************************************************************************
	* string jsPageNumNextPrev (int curpage, int pages)
	* Returns "Previous | 1 | 2 |......| Next" for individual pagination (as an image with an onClick js function call!)
	***********************************************************************************/
	public function jsPageNumNextPrev($curpage, $pages, $jsfunc, $qryparams="", $link_type = "")
	{
		$iLimitPages=5;
		if($curpage==1 ){
		      $iStartNo=$curpage;
		}
		elseif($curpage==2 ){
		      $iStartNo=$curpage-1;
		}else{
		    $iStartNo=$curpage-2;
		}

		$iEndNo =($iStartNo+$iLimitPages)-1;
	        if($iEndNo>$pages) $iEndNo=$pages; 	
		$iDiff =$iEndNo-$iStartNo;
		if($iDiff<($iLimitPages-1)) {
		    $iStartNo=$iStartNo-($iLimitPages-$iDiff-1);
		}
		if($iStartNo<1) $iStartNo=1;
        	$next_prev  = "";
		//Adding the optional parameters that have to be passed to the JS function - 2006-Sep-12
		if ($qryparams){
		    $qryparams = trim($qryparams);
		    if ($qryparams{0} != ","){
		        $qryparams = "," . $qryparams;
		    }
		}
		$qrparamsarr=explode(',',$qryparams);
		$qryparams=implode("','",$qrparamsarr);
		//	Setting up Previous / Next text or image links - 
		
		$first_link = ($link_type == "text") ? "First" : "First";
		$prev_link = ($link_type == "text") ? "Prev" :" Prev";
		$next_link = ($link_type == "text") ? "Next" : "Next";
		$last_link = ($link_type == "text") ? "Last" : "Last";
		

		$first_link = "";
		$prev_link = "<img  src=\"".IMAGE_URL."/view_arr2_on.png\" align=\"absmiddle\"/>";
		$next_link = "<img  src=\"".IMAGE_URL."/view_arr_on.png\" align=\"absmiddle\"/>";
	        $last_link = "";

		if (($curpage-1) <= 0)
		{
		    $next_prev .= "<span class=\"pagination_text\">".$first_link."</span>";
		    if($curpage >1 )
		    {
		        $next_prev .=  "<span class=\"pagination_text\">".$prev_link."</span>";
		    }
		}
		else
		{
		    $next_prev .= "<a href=\"javascript:void(0);\" title=\"First\" class=\"pagination_text\" onClick=\"Javascript:" . $jsfunc . "('1" . $qryparams . "')" . ";return false;\">" . $first_link . "</a>";
		    $next_prev .="<a href=\"javascript:void(0);\" title=\"Previous\" class=\"pagination_text\" onClick=\"Javascript:" . $jsfunc . "('" . ($curpage-1) . $qryparams . "')" . ";return false;\">" . $prev_link . "</a>";
		}
		/* Print the numeric page list; make the current page unlinked and bold */
		if($curpage+4 < $pages)
		    $pk=$curpage+4;
		else
		    $pk=$pages;
	       //echo $iStartNo;
		for ($i=$iStartNo; $i<=$iEndNo; $i++){
		    if ($i == $curpage){
		        $next_prev .= "<b><span class='b'>".$i."</span></b>";
		    }
		    else {
		        $next_prev .= "<a href=\"javascript:void(0);\" title=\"Page $i\"  onClick=\"Javascript:" . $jsfunc . "('" . $i . $qryparams . "')" . ";return false;\">" . $i . "</a> ";
		    }
	            $next_prev .= " ";
	        }
		if (($curpage+1) > $pages)
		{
		    if($curpage < $pages){
		        $next_prev .= "<span class=\"pagination_text\" >".$next_link."</span>";
		    }
		    $next_prev .=  "<span class=\"pagination_text\">".$last_link."</span> ";
		}
		else
		{
		    $next_prev .=  "<a href=\"javascript:void(0);\" title=\"Next\"  onClick=\"Javascript:" . $jsfunc . "('" . ($curpage+1) . $qryparams . "')" . ";return false;\">" . $next_link . "</a> ";
		    $next_prev .=  "<a href=\"javascript:void(0);\" title=\"Last\"  onClick=\"Javascript:" . $jsfunc . "('" . $pages . $qryparams . "')" . ";return false;\">" . $last_link . "</a> ";
		}
		return $next_prev;
    }



	/***********************************************************************************
	* string jsPageNumNextPrev (int curpage, int pages)
	* Returns "Previous | 1 | 2 |......| Next" for individual pagination (as an image with an onClick js function call!)
	***********************************************************************************/
	public function jsPageNumNextPrevVideoCat($curpage, $pages, $jsfunc, $qryparams="", $link_type = "")
	{
		$iLimitPages=5;
		if($curpage==1 ){
		      $iStartNo=$curpage;
		}
		elseif($curpage==2 ){
		      $iStartNo=$curpage-1;
		}else{
		    $iStartNo=$curpage-2;
		}

		$iEndNo =($iStartNo+$iLimitPages)-1;
	        if($iEndNo>$pages) $iEndNo=$pages; 	
		$iDiff =$iEndNo-$iStartNo;
		if($iDiff<($iLimitPages-1)) {
		    $iStartNo=$iStartNo-($iLimitPages-$iDiff-1);
		}
		if($iStartNo<1) $iStartNo=1;
        	$next_prev  = "";
		//Adding the optional parameters that have to be passed to the JS function - 2006-Sep-12
		if ($qryparams){
		    $qryparams = trim($qryparams);
		    if ($qryparams{0} != ","){
		        $qryparams = "," . $qryparams;
		    }
		}
		$qrparamsarr=explode(',',$qryparams);
		$qryparams=implode("','",$qrparamsarr);
		//	Setting up Previous / Next text or image links - 
		
		$first_link = ($link_type == "text") ? "First" : "First";
		$prev_link = ($link_type == "text") ? "Prev" :" Prev";
		$next_link = ($link_type == "text") ? "Next" : "Next";
		$last_link = ($link_type == "text") ? "Last" : "Last";
		

		$first_link = "";
		$prev_link = "Prev";
		$next_link = "Next";
        $last_link = "";
	    //$next_prev.="<ul>";
		if (($curpage-1) <= 0)
		{
		    $next_prev .= "<span class=\"pagination_text\">".$first_link."</span>";
		    if($curpage >1 )
		    {
		        $next_prev .=  "<span class=\"pagination_text\">".$prev_link."</span>";
		    }
		}
		else
		{
		    $next_prev .= "<a href=\"javascript:void(0);\" title=\"First\" class=\"pagination_text\" onClick=\"Javascript:" . $jsfunc . "('1" . $qryparams . "')" . ";return false;\">" . $first_link . "</a>";
		    $next_prev .="<a href=\"javascript:void(0);\" title=\"Previous\" class=\"pagination_text\" onClick=\"Javascript:" . $jsfunc . "('" . ($curpage-1) . $qryparams . "')" . ";return false;\">" . $prev_link . "</a>";
		}
		/* Print the numeric page list; make the current page unlinked and bold */
		if($curpage+4 < $pages)
		    $pk=$curpage+4;
		else
		    $pk=$pages;
	       //echo $iStartNo;
		for ($i=$iStartNo; $i<=$iEndNo; $i++){
		    if ($i == $curpage){
		        $next_prev .= "<span class='active'>".$i."</span>";
		    }
		    else {
		        $next_prev .= "<a href=\"javascript:void(0);\" title=\"Page $i\"  onClick=\"Javascript:" . $jsfunc . "('" . $i . $qryparams . "')" . ";return false;\">" . $i . "</a> ";
		    }
	            $next_prev .= " ";
	        }
		if (($curpage+1) > $pages)
		{
		    if($curpage < $pages){
		        $next_prev .= "<span class=\"pagination_text\" >".$next_link."</span>";
		    }
		    $next_prev .=  "<span class=\"pagination_text\">".$last_link."</span> ";
		}
		else
		{
		    $next_prev .=  "<a href=\"javascript:void(0);\" title=\"Next\"  onClick=\"Javascript:" . $jsfunc . "('" . ($curpage+1) . $qryparams . "')" . ";return false;\">" . $next_link . "</a>";
		    //$next_prev .=  "<a href=\"javascript:void(0);\" title=\"Last\"  onClick=\"Javascript:" . $jsfunc . "('" . $pages . $qryparams . "')" . ";return false;\">" . $last_link . "</a> ";
		}
		// $next_prev.="</ul>";
		return $next_prev;
    }


	public function jsAjaxPageNumNextPrev($curpage, $pages, $jsfunc, $qryparams="", $link_type = ""){
		$iLimitPages=5;
		if($curpage==1 ){
			$iStartNo=$curpage;
		}
		elseif($curpage==2 ){
			$iStartNo=$curpage-1;
		}else{
			$iStartNo=$curpage-2;
		}
		$iEndNo =($iStartNo+$iLimitPages)-1;
		if($iEndNo>$pages) $iEndNo=$pages; 	
		$iDiff =$iEndNo-$iStartNo;
		if($iDiff<($iLimitPages-1)) {
			$iStartNo=$iStartNo-($iLimitPages-$iDiff-1);
		}
		if($iStartNo<1) $iStartNo=1;
			$next_prev  = "";
			//Adding the optional parameters that have to be passed to the JS function - 2006-Sep-12
			if ($qryparams){
				$qryparams = trim($qryparams);
				if ($qryparams{0} != ","){
					$qryparams = "," . $qryparams;
				}
			}
			$qrparamsarr=explode(',',$qryparams);
			$qryparams=implode("','",$qrparamsarr);
			//	Setting up Previous / Next text or image links - 
			/*
			$first_link = ($link_type == "text") ? "First" : "First";
			$prev_link = ($link_type == "text") ? "Prev" :" Prev";
			$next_link = ($link_type == "text") ? "Next" : "Next";
			$last_link = ($link_type == "text") ? "Last" : "Last";
			*/
			$first_link = "";
			$prev_link = "<";
			$next_link = ">";
			$last_link = "";
			/*if (($curpage-1) <= 0)
			{
				$next_prev .= "<span class=\"pagination_text\">".$first_link."</span>";
				if($curpage >1 )
				{
					$next_prev .=  "<span class=\"pagination_text\">".$prev_link."</span>";
				}
			}
			else
			{
				$next_prev .= "<a href=\"javascript:void(0);\" title=\"First\" class=\"pagination_text\" onClick=\"Javascript:" . $jsfunc . "('1" . $qryparams . "')" . ";return false;\">" . $first_link . "</a>";
				$next_prev .="<a href=\"javascript:void(0);\" title=\"Previous\" class=\"pagination_text\" onClick=\"Javascript:" . $jsfunc . "('" . ($curpage-1) . $qryparams . "')" . ";return false;\">" . $prev_link . "</a>";
			}
			*/
			/* Print the numeric page list; make the current page unlinked and bold */
			if($curpage+4 < $pages)
			$pk=$curpage+4;
			else
			$pk=$pages;
			//echo $iStartNo;
			for ($i=$iStartNo; $i<=$iEndNo; $i++){
			if ($i == $curpage){
				$next_prev .= "<b class=\"pg\"><span class='sel cfff plr5'>".$i."</span></b>";
			}
			else {
				$next_prev .= "<a href=\"javascript:void(0);\" title=\"Page $i\"  onClick=\"Javascript:" . $jsfunc . "('" . $i . $qryparams . "')" . ";return false;\">" . $i . "</a> ";
			}
			$next_prev .= " ";
		}
		/*if (($curpage+1) > $pages)
		{
			if($curpage < $pages){
				$next_prev .= "<span class=\"pagination_text\" >".$next_link."</span>";
			}
			$next_prev .=  "<span class=\"pagination_text\">".$last_link."</span> ";
		}
		else
		{
			$next_prev .=  "<a href=\"javascript:void(0);\" title=\"Next\"  onClick=\"Javascript:" . $jsfunc . "('" . ($curpage+1) . $qryparams . "')" . ";return false;\">" . $next_link . "</a> ";
			$next_prev .=  "<a href=\"javascript:void(0);\" title=\"Last\"  onClick=\"Javascript:" . $jsfunc . "('" . $pages . $qryparams . "')" . ";return false;\">" . $last_link . "</a> ";
		}
		*/
		return $next_prev;
	}

/***********************************************************************************
	* int findStartArticle (int limit)
	* Returns the start offset based on $_GET['page'] and $limit
	***********************************************************************************/
	function findStartArticle($limit){
		
		if ((!isset($_REQUEST['pg'])) || ($_REQUEST['pg'] <= "1")){
			$start = 0;
			$_REQUEST['pg'] = 1;
		}
		else{
			$start = ($_REQUEST['pg']-1) * $limit;
		}
       
		return $start;
	}


	public function pageNumNextPrevArticle($curpage, $pages, $siteurl="", $param = ""){
		
		if(!empty($param)){$param='&tid='.$param;}
		$iLimitPages=3;
		if($curpage==1 ){
		      $iStartNo=$curpage;
		}
		elseif($curpage==2 ){
		      $iStartNo=$curpage-1;
		}else{
		    $iStartNo=$curpage-2;
		}

		$iEndNo =($iStartNo+$iLimitPages)-1;
	        if($iEndNo>$pages) $iEndNo=$pages; 	
		$iDiff =$iEndNo-$iStartNo;
		if($iDiff<($iLimitPages-1)) {
		    $iStartNo=$iStartNo-($iLimitPages-$iDiff-1);
		}
		if($iStartNo<1) $iStartNo=1;
        	$next_prev  = "";				

		$first_link = "&lt;&#160;Previous";
		$prev_link = "&lt;&#160;Previous";
		$next_link = "Next&#160;&gt;";
        $last_link = "Next&#160;&gt;";

		if (($curpage-1) <= 0)
		{
		    if($curpage >1 )
		    {
		        $next_prev .=  "<a class=\"fl\">".$prev_link."</a>";
		    }
		}
		else
		{
		    $next_prev .="<a href=\"".$siteurl."?pg=".($curpage-1).$param."\" title=\"Previous\" class=\"fl\">" . $prev_link . "</a>";
		}
		/* Print the numeric page list; make the current page unlinked and bold */
		$next_prev .= "<div class=\"dtTxt fl\">";
		if($curpage+4 < $pages)
		    $pk=$curpage+4;
		else
		    $pk=$pages;

		for ($i=$iStartNo; $i<=$iEndNo; $i++){
		    if ($i == $curpage){
              if($i==$pages){
				   $next_prev .= "<a class='b'>".$i."</a>";
				 }else{
					if($iEndNo > $i){
						$next_prev .= "<a class='b'>".$i."</a>&#160;|&#160;";
					}else{
						$next_prev .= "<a class='b'>".$i."</a>";
					}
			   }

				
		    }
		    else{
				 
				 if($iEndNo > $i){
					 $next_prev .= "<a href=\"".$siteurl."?pg=".$i.$param."\" title=\"Page $i\" >".$i."</a>&#160;|&#160;";
				 }else{
					 $next_prev .= "<a href=\"".$siteurl."?pg=".$i.$param."\" title=\"Page $i\" >".$i."</a>";
				 }
			     
			}
	            $next_prev .= " ";
		}
		$next_prev .= "</div>";
		//echo $curpage."----".$pages;
		if (($curpage+1) > $pages)
		{
			if($curpage < $pages){
				$next_prev .= "<a class=\"fl\" >".$next_link." </a>";
			}
		}
		else
		{
			$next_prev .=  "<a href=\"".$siteurl."?pg=".($curpage+1).$param."\" title=\"Next\" class=\"fl\">" . $next_link . "</a> ";
		}
		$next_prev .= "<div class=\"cb\"></div>";
		
		return $next_prev;
    }

 
}
?>
