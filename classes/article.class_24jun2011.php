<?php
/**
 * This class is used to perform actions on the articles
 * @author sachin
 * @version 1.0
 * @updated 
 */
/**
 * include database connection class
*/
/**
 * include database operation class
*/
class article extends DbOperation
{

	/**
	 * @note function is used  add/update article details
	 * @pre  aParameters is array article details
	 * values  -
		article_id
		title
		content
		city_id

	 * @post return article id if successful , 0 if error occurs
	 * 
	 * 
	 * @param aParameters
	 */
	function addUpdArticleDetails($aParameters,$sTableName){
		$aParameters['create_date'] = date('Y-m-d H:i:s');
		$aParameters['update_date'] = date('Y-m-d H:i:s');
	 	$sSql=$this->getInsertUpdateSql($sTableName,array_keys($aParameters),array_values($aParameters));	
		//echo "TEST---".$sSql."<br>";	
		$iRes=$this->insertUpdate($sSql);
		return $iRes;
	}
	/**
	* @note function is used  add/update article
	* @pre  aParameters is array article details
	* values  -
	* @post return article id if successful , -2 if error occurs
	* 
	* 
	* @param aParameters
	*/
	function addArticle($aParameters,$sTableName){
		$sSql=$this->getInsertSql($sTableName,array_keys($aParameters),array_values($aParameters));      
		return $iRes=$this->insert($sSql);
		
	}

	/**
    * @note function is used  update article
    * @pre  aParameters is array article details
    * values  -
    * @post return true if successful , false if error occurs
    * 
    * 
    * @param aParameters
    */
    function updateArticle($aParameters,$sTableName){
		$sSql=$this->getUpdateSql($sTableName,array_keys($aParameters),array_values($aParameters),"article_id",$aParameters['article_id']);
		return $iRes=$this->update($sSql);
	}
	
	/**
	* @note function is used  get  Article list 
	* @pre  aParameters is array of article details
	* values  -
	* @post return array if successful , -2 if error occurs
	* 
	* 
	* @param 
	*/

	function getArticleList($aParamaters,$iStartlimit,$iCnt,$sTableName){
		//print_r($aParamaters);
		$sRequest="";
		//$aConstraintsArr=Array("category_id"=>1,"parent_id"=>1);
		$aConstraintsArr=$aParamaters;
		//$sOrderfield=$aParamaters['orderfield'];
		//$sOrder=$aParamaters['order'];
		$iStartlimit=$aParamaters['start'];
		$iCnt=$aParamaters['cnt'];
		//echo $sOrderfield,$sOrder,$iStartlimit,$iCnt;
		$sSql=$this->getSelectSql($sTableName,$sRequest,$aConstraintsArr,$sOrderfield,$sOrder,$iStartlimit,$iCnt);
		//$sSql="select * from PRODUCT_ARTICLE where category_id="
		$aRes=$this->select($sSql);
		return $aRes;
	}

	
	function arrGetArticleGroupDetails($category_id){
		$sSql="select * from ARTICLE_GROUP where category_id=$category_id";
		//echo $sSql;
		$aRes=$this->select($sSql);
		//print_r($aRes);
		return $aRes;
	}

	function arrGetArticleTypeDetails($category_id="",$article_type=""){
		if(!empty($category_id)){
			$whereClauseArr[] = "category_id = $category_id";
		}
		if(!empty($article_type)){
			$whereClauseArr[] = "article_type_id = $article_type";
		}
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = "where ".implode(" and ",$whereClauseArr);
		}
		$sSql="select * from ARTICLE_TYPE $whereClauseStr order by category_id asc";
		$aRes=$this->select($sSql);
		return $aRes;
	}
	
	function getArticleListByBrand($article_ids="",$product_ids="",$product_info_id="",$category_id="",$brand_id="",$status="1",$startlimit="",$cnt=""){
		$iCnt=$aParamaters['cnt'];
		if(is_array($article_ids)){
			$article_ids = implode(",",$article_ids);
		}
		if(is_array($product_info_id)){
			$product_info_id = implode(",",$product_info_id);
		}
		if(is_array($product_ids)){
			$product_ids = implode(",",$product_ids);
		}
		if(is_array($category_id)){
			$category_id = implode(",",$category_id);
		}
		if(is_array($brand_id)){
			$brand_id = implode(",",$brand_id);
		}
		if($status != '')
		{
			$whereClauseArr[] = "status=$status";
		}
		if($article_ids!=""){
				$whereClauseArr[] = "A.article_id in ($article_ids)";
		}
		if($product_ids!=""){
			$whereClauseArr[] = " PA.product_id in($product_ids)";
			$whereClauseArr[] = " PA.article_id=A.article_id ";
		}
		if($product_info_id!=""){
			$whereClauseArr[] = " PA.product_info_id in($product_info_id)";
			$whereClauseArr[] = " PA.article_id=A.article_id ";
		}
		if($category_id!=""){
			$whereClauseArr[] = " PA.category_id in ($category_id)";
		}
		if($brand_id!=""){
			$whereClauseArr[] = " PA.brand_id in ($brand_id)";

		}
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
		}
		if(!empty($startlimit)){
			$limitArr[] = $startlimit;
		}
		if(!empty($count)){
			$limitArr[] = $count;
		}
		if(sizeof($limitArr) > 0){
			$limitStr = " limit ".implode(" , ",$limitArr);
		}

		$sSql="select * from PRODUCT_ARTICLE PA, ARTICLE A $whereClauseStr $limitStr";
		//echo $sSql;
		$aRes=$this->select($sSql);
		//print_r($aRes);
		return $aRes;
	}

	/**
	* @note function is used  get  Article list 
	* @pre  aParameters is array of article details
	* values  -
	* @post return array if successful , -2 if error occurs
	* 
	* 
	* @param 
	*/

	function getProductArticleList($sArticleIds,$sType,$sTableName){
		//print_r($aParamaters);
		$sRequest="";
		//$aConstraintsArr=Array("feature_id"=>1,"parent_id"=>1);
		$aConstraintsArr=$aParamaters;
		$sOrderfield=$aParamaters['orderfield'];
		$sOrder=$aParamaters['order'];
		$iStartlimit=$aParamaters['start'];
		$iCnt=$aParamaters['cnt'];
		//echo $sOrderfield,$sOrder,$iStartlimit,$iCnt;
		//$sSql=$this->getSelectSql($sTableName,$sRequest,$aConstraintsArr,$sOrderfield,$sOrder,$iStartlimit,$iCnt);
		$sSql="select * from ARTICLE where ";
		if($sArticleIds!=''){ $sSql .=" article_id in ($sArticleIds) "; }
		if($sArticleIds!='' && $sType!=''){ $sSql .=" and";}
		if($sType!=''){ $sSql .=" article_type=$sType"; }
		//echo $sSql;
		$aRes=$this->select($sSql);
		return $aRes;
	}
	
	



	

	/**
	* @note function is used  get  article detail
	* @pre  aParameters is array of article details
	* values  -
	* @post return array if successful , -2 if error occurs
	* 
	* 
	* @param iAId
	*/
	function getArticleDetail($iAId){
		$sRequest="";
		//$aConstraintsArr=Array("feature_id"=>1,"parent_id"=>1);
		$aConstraintsArr=Array("article_id"=>$iAId);
		$sOrderfield="";
		$sOrder="";
		$iStartlimit="";
		$iCnt="";
		$sTableName="ARTICLE";
		$sSql=$this->getSelectSql($sTableName,$sRequest,$aConstraintsArr,$sOrderfield,$sOrder,$iStartlimit,$iCnt);		
		$aRes=$this->select($sSql);
		return $aRes; 
   }



	
	/**
	* @note function is used  get  article count by type
	* @pre  aParameters is array of article count 
	* values  -
	* @post return array if successful , -2 if error occurs
	* 
	* 
	* @param 
	*/
	/*function getArticleCount(){
		$sRequest="count(*) as iCnt ";
		//$aConstraintsArr=Array("feature_id"=>1,"parent_id"=>1);
		$aConstraintsArr=Array();
		$sOrderfield="";
		$sOrder="";
		$iStartlimit="";
		$iCnt="";
		$sTableName="ARTICLE";
		$sSql=$this->getSelectSql($sTableName,$sRequest,$aConstraintsArr,$sOrderfield,$sOrder,$iStartlimit,$iCnt);
		$aRes=$this->select($sSql);
		return $aRes;
	}
	*/
	/**
	* @note function is used  delete article
	* @pre  aParameters is  article id
	* values  -
	* @post return true if successful , -2 if error occurs
	* 
	* 
	* @param iAId,sTableName
	*/
	function deleteArticle($iAId,$sTableName){
		$sSql="delete from ARTICLE where article_id='".$iAId."'";      
		$iRes=$this->sql_delete_data($sSql);
		$sSql='';
		$sSql="delete from PRODUCT_ARTICLE where article_id='".$iAId."'";      
		$iRes=$this->sql_delete_data($sSql);
	}


/**
* @note function is used to get article count
* values  -
* @post return count
* @param $article_ids="",$group_ids="",$type_ids="",$product_ids="",$product_info_id="",$category_id="",$brand_id="",$status="1",$startlimit="",$cnt="", $orderby=""
*/
function getGroupByArticleCount($article_ids="",$group_ids="",$type_ids="",$product_ids="",$product_info_id="",$category_id="",$brand_id="",$status="1",$startlimit="",$cnt="", $orderby=""){
	//echo 'startlimit = '.$startlimit.' count = '.$cnt."<br/>";
	$iCnt=$aParamaters['cnt'];
	if(is_array($article_ids)){
		$article_ids = implode(",",$article_ids);
	}
	if(is_array($type_ids)){
		$type_ids = implode(",",$type_ids);
	}
	if(is_array($group_ids)){
		$group_ids = implode(",",$group_ids);
	}
	if(is_array($product_info_id)){
		$product_info_id = implode(",",$product_info_id);
	}
	if(is_array($product_ids)){
		$product_ids = implode(",",$product_ids);
	}
	if(is_array($category_id)){
		$category_id = implode(",",$category_id);
	}
	if(is_array($brand_id)){
		$brand_id = implode(",",$brand_id);
	}
	if($status != ''){
		$whereClauseArr[] = "status=$status";
	}
	if($article_ids!=""){
		$whereClauseArr[] = "A.article_id in ($article_ids)";
	}
	if($product_ids!=""){
		$whereClauseArr[] = " PA.product_id in($product_ids)";
	}
	if($product_info_id!=""){
		$whereClauseArr[] = " PA.product_info_id in($product_info_id)";
	}
	if($group_ids!=""){
		$whereClauseArr[] = " PA.group_id in($group_ids)";
	}
	if($type_ids!=""){
		$whereClauseArr[] = " A.article_type in($type_ids)";
	}
	if($product_info_id!=""){
		$whereClauseArr[] = " PA.product_info_id in($product_info_id)";
	}
	if($category_id!=""){
		$whereClauseArr[] = " PA.category_id in ($category_id)";
	}
	if($brand_id!=""){
		$whereClauseArr[] = " PA.brand_id in ($brand_id)";
	}
	$whereClauseArr[] = " PA.article_id=A.article_id ";
	if(sizeof($whereClauseArr) > 0){
		$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
	}	
	 $sSql="select count(*) as count from PRODUCT_ARTICLE PA, ARTICLE A $whereClauseStr";
	//echo $sSql;
	$aRes=$this->select($sSql);
	//print_r($aRes);
	return $aRes[0]['count'];
}// End of function getArticleCount

function getGroupByArticleCountData($article_ids="",$group_ids="",$type_ids="",$product_ids="",$product_info_id="",$category_id="",$brand_id="",$status="1",$startlimit="",$cnt="", $orderby=""){
	//echo 'startlimit = '.$startlimit.' count = '.$cnt."<br/>";
	$iCnt=$aParamaters['cnt'];
	if(is_array($article_ids)){
		$article_ids = implode(",",$article_ids);
	}
	if(is_array($type_ids)){
		$type_ids = implode(",",$type_ids);
	}
	if(is_array($group_ids)){
		$group_ids = implode(",",$group_ids);
	}
	if(is_array($product_info_id)){
		$product_info_id = implode(",",$product_info_id);
	}
	if(is_array($product_ids)){
		$product_ids = implode(",",$product_ids);
	}
	if(is_array($category_id)){
		$category_id = implode(",",$category_id);
	}
	if(is_array($brand_id)){
		$brand_id = implode(",",$brand_id);
	}
	if($status != ''){
		$whereClauseArr[] = "status=$status";
	}
	if($article_ids!=""){
		$whereClauseArr[] = "A.article_id not in ($article_ids)";
	}
	if($product_ids!=""){
		$whereClauseArr[] = " PA.product_id in($product_ids)";
	}
	if($product_info_id!=""){
		$whereClauseArr[] = " PA.product_info_id in($product_info_id)";
	}
	if($group_ids!=""){
		$whereClauseArr[] = " PA.group_id in($group_ids)";
	}
	if($type_ids!=""){
		$whereClauseArr[] = " A.article_type in($type_ids)";
	}
	if($product_info_id!=""){
		$whereClauseArr[] = " PA.product_info_id in($product_info_id)";
	}
	if($category_id!=""){
		$whereClauseArr[] = " PA.category_id in ($category_id)";
	}
	if($brand_id!=""){
		$whereClauseArr[] = " PA.brand_id in ($brand_id)";
	}
	$whereClauseArr[] = " PA.article_id=A.article_id ";
	if(sizeof($whereClauseArr) > 0){
		$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
	}	
	 $sSql="select count(*) as count from PRODUCT_ARTICLE PA, ARTICLE A $whereClauseStr";
	//echo $sSql;
	$aRes=$this->select($sSql);
	//print_r($aRes);
	return $aRes[0]['count'];
}// End of function getArticleCount

function getArticleCount($article_ids="",$group_ids="",$type_ids="",$product_ids="",$product_info_id="",$category_id="",$brand_id="",$status="1",$orderby=""){
	if(is_array($article_ids)){
		$article_ids = implode(",",$article_ids);
	}
	if(is_array($type_ids)){
		$type_ids = implode(",",$type_ids);
	}
	if(is_array($group_ids)){
		$group_ids = implode(",",$group_ids);
	}
	if(is_array($product_info_id)){
		$product_info_id = implode(",",$product_info_id);
	}
	if(is_array($product_ids)){
		$product_ids = implode(",",$product_ids);
	}
	if(is_array($category_id)){
		$category_id = implode(",",$category_id);
	}
	if(is_array($brand_id)){
		$brand_id = implode(",",$brand_id);
	}
	if($status != ''){
		$whereClauseArr[] = "status=$status";
	}
	if($article_ids!=""){
		$whereClauseArr[] = "A.article_id in ($article_ids)";
	}
	if($product_ids!=""){
		$whereClauseArr[] = " PA.product_id in($product_ids)";
	}
	if($product_info_id!=""){
		$whereClauseArr[] = " PA.product_info_id in($product_info_id)";
	}
	if($group_ids!=""){
		$whereClauseArr[] = " PA.group_id in($group_ids)";
	}
	if($type_ids!=""){
		$whereClauseArr[] = " A.article_type in($type_ids)";
	}
	if($product_info_id!=""){
		$whereClauseArr[] = " PA.product_info_id in($product_info_id)";
	}
	if($category_id!=""){
		$whereClauseArr[] = " PA.category_id in ($category_id)";
	}
	if($brand_id!=""){
		$whereClauseArr[] = " PA.brand_id in ($brand_id)";
	}
	$whereClauseArr[] = " PA.article_id=A.article_id ";
	if(sizeof($whereClauseArr) > 0){
		$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
	}	
	$sSql="select count(*) as count from PRODUCT_ARTICLE PA, ARTICLE A $whereClauseStr";
	$aRes=$this->select($sSql);
	return $aRes[0]['count'];
}// End of function getArticleCount

	function getFeaturedArticleDetails($article_ids="",$group_ids="",$type_ids="",$product_ids="",$product_info_id="",$category_id="",$brand_id="",$status="1",$startlimit="",$cnt="", $orderby=""){
		//echo 'startlimit = '.$startlimit.' count = '.$cnt."<br/>";
		$iCnt=$aParamaters['cnt'];
		if(is_array($article_ids)){
			$article_ids = implode(",",$article_ids);
		}
		if(is_array($type_ids)){
			$type_ids = implode(",",$type_ids);
		}
		if(is_array($group_ids)){
			$group_ids = implode(",",$group_ids);
		}
		if(is_array($product_info_id)){
			$product_info_id = implode(",",$product_info_id);
		}
		if(is_array($product_ids)){
			$product_ids = implode(",",$product_ids);
		}
		if(is_array($category_id)){
			$category_id = implode(",",$category_id);
		}
		if(is_array($brand_id)){
			$brand_id = implode(",",$brand_id);
		}
		if($status != '')
		{
			$whereClauseArr[] = "FA.status=$status";
			$whereClauseArr[] = "A.status=$status";
		}
		if($article_ids!=""){
				$whereClauseArr[] = "A.article_id in ($article_ids)";
		}
		if($product_ids!=""){
			$whereClauseArr[] = " PA.product_id in($product_ids)";
		}
		if($product_info_id!=""){
			$whereClauseArr[] = " PA.product_info_id in($product_info_id)";
		}
		if($group_ids!=""){
			$whereClauseArr[] = " PA.group_id in($group_ids)";
			//$whereClauseArr[] = " PA.group_id=A.article_id ";
		}
		if($type_ids!=""){
			$whereClauseArr[] = " A.article_type in($type_ids)";
		}
		if($product_info_id!=""){
			$whereClauseArr[] = " PA.product_info_id in($product_info_id)";			
		}
		if($category_id!=""){
			$whereClauseArr[] = " PA.category_id in ($category_id)";
		}
		if($brand_id!=""){
			$whereClauseArr[] = " PA.brand_id in ($brand_id)";

		}
		$whereClauseArr[] = " PA.article_id=A.article_id ";
		$whereClauseArr[] = " FA.article_id=A.article_id ";
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
		}
		if(!empty($startlimit)){
			$limitArr[] = $startlimit;
		}
		if(!empty($cnt)){
			$limitArr[] = $cnt;
		}
		if(sizeof($limitArr) > 0){
			$limitStr = " limit ".implode(" , ",$limitArr);
		}
		$sSql="select *, DATE_FORMAT(A.create_date,'%d %b %Y') as disp_date from FEATURED_ARTICLE FA,PRODUCT_ARTICLE PA, ARTICLE A $whereClauseStr $orderby $limitStr";
		//echo $sSql;
		//exit;
		$aRes=$this->select($sSql);
		//print_r($aRes);
		return $aRes;
	}

	function getFeaturedArticleDetailsCount($article_ids="",$group_ids="",$type_ids="",$product_ids="",$product_info_id="",$category_id="",$brand_id="",$status="1"){
		//echo 'startlimit = '.$startlimit.' count = '.$cnt."<br/>";
		$iCnt=$aParamaters['cnt'];
		if(is_array($article_ids)){
			$article_ids = implode(",",$article_ids);
		}
		if(is_array($type_ids)){
			$type_ids = implode(",",$type_ids);
		}
		if(is_array($group_ids)){
			$group_ids = implode(",",$group_ids);
		}
		if(is_array($product_info_id)){
			$product_info_id = implode(",",$product_info_id);
		}
		if(is_array($product_ids)){
			$product_ids = implode(",",$product_ids);
		}
		if(is_array($category_id)){
			$category_id = implode(",",$category_id);
		}
		if(is_array($brand_id)){
			$brand_id = implode(",",$brand_id);
		}
		if($status != '')
		{
			$whereClauseArr[] = "FA.status=$status";
			$whereClauseArr[] = "A.status=$status";
		}
		if($article_ids!=""){
				$whereClauseArr[] = "A.article_id in ($article_ids)";
		}
		if($product_ids!=""){
			$whereClauseArr[] = " PA.product_id in($product_ids)";
		}
		if($product_info_id!=""){
			$whereClauseArr[] = " PA.product_info_id in($product_info_id)";
		}
		if($group_ids!=""){
			$whereClauseArr[] = " PA.group_id in($group_ids)";
			//$whereClauseArr[] = " PA.group_id=A.article_id ";
		}
		if($type_ids!=""){
			$whereClauseArr[] = " A.article_type in($type_ids)";
		}
		if($product_info_id!=""){
			$whereClauseArr[] = " PA.product_info_id in($product_info_id)";			
		}
		if($category_id!=""){
			$whereClauseArr[] = " PA.category_id in ($category_id)";
		}
		if($brand_id!=""){
			$whereClauseArr[] = " PA.brand_id in ($brand_id)";

		}
		$whereClauseArr[] = " PA.article_id=A.article_id ";
		$whereClauseArr[] = " FA.article_id=A.article_id ";
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
		}
		
		$sSql="select count(FA.article_id) as cnt from FEATURED_ARTICLE FA,PRODUCT_ARTICLE PA, ARTICLE A $whereClauseStr";
		//echo $sSql;
		//exit;
		$aRes=$this->select($sSql);
		$count=$aRes[0]['cnt'];
		return $count;
	}

	function getFeaturedArticleListDetails($article_ids="",$group_ids="",$type_ids="",$product_ids="",$product_info_id="",$category_id="",$brand_id="",$status="1",$startlimit="",$cnt="", $orderby=""){
		//echo 'startlimit = '.$startlimit.' count = '.$cnt."<br/>";
		$iCnt=$aParamaters['cnt'];
		if(is_array($article_ids)){
			$article_ids = implode(",",$article_ids);
		}
		if(is_array($type_ids)){
			$type_ids = implode(",",$type_ids);
		}
		if(is_array($group_ids)){
			$group_ids = implode(",",$group_ids);
		}
		if(is_array($product_info_id)){
			$product_info_id = implode(",",$product_info_id);
		}
		if(is_array($product_ids)){
			$product_ids = implode(",",$product_ids);
		}
		if(is_array($category_id)){
			$category_id = implode(",",$category_id);
		}
		if(is_array($brand_id)){
			$brand_id = implode(",",$brand_id);
		}
		if($status != '')
		{
			$whereClauseArr[] = "A.status=$status";
		}
		if($article_ids!=""){
				$whereClauseArr[] = "A.article_id not in ($article_ids)";
		}
		if($product_ids!=""){
			$whereClauseArr[] = " PA.product_id in($product_ids)";
		}
		if($product_info_id!=""){
			$whereClauseArr[] = " PA.product_info_id in($product_info_id)";
		}
		if($group_ids!=""){
			$whereClauseArr[] = " PA.group_id in($group_ids)";
			//$whereClauseArr[] = " PA.group_id=A.article_id ";
		}
		if($type_ids!=""){
			$whereClauseArr[] = " A.article_type in($type_ids)";
		}
		if($product_info_id!=""){
			$whereClauseArr[] = " PA.product_info_id in($product_info_id)";			
		}
		if($category_id!=""){
			$whereClauseArr[] = " PA.category_id in ($category_id)";
		}
		if($brand_id!=""){
			$whereClauseArr[] = " PA.brand_id in ($brand_id)";

		}
		$whereClauseArr[] = " PA.article_id=A.article_id ";
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
		}
		if(!empty($startlimit)){
			$limitArr[] = $startlimit;
		}
		if(!empty($cnt)){
			$limitArr[] = $cnt;
		}
		if(sizeof($limitArr) > 0){
			$limitStr = " limit ".implode(" , ",$limitArr);
		}
		$sSql="select *, DATE_FORMAT(A.create_date,'%d %b %Y') as disp_date from PRODUCT_ARTICLE PA, ARTICLE A $whereClauseStr $orderby $limitStr";
		//echo $sSql;exit;
		$aRes=$this->select($sSql);
		//print_r($aRes);
		return $aRes;
	}


	function getMaintainanceArticleDetails($article_ids="",$group_ids="",$type_ids="",$product_ids="",$product_info_id="",$category_id="",$brand_id="",$status="1",$startlimit="",$cnt="", $orderby=""){
		//echo 'startlimit = '.$startlimit.' count = '.$cnt."<br/>";
		$iCnt=$aParamaters['cnt'];
		if(is_array($article_ids)){
			$article_ids = implode(",",$article_ids);
		}
		if(is_array($type_ids)){
			$type_ids = implode(",",$type_ids);
		}
		if(is_array($group_ids)){
			$group_ids = implode(",",$group_ids);
		}
		if(is_array($product_info_id)){
			$product_info_id = implode(",",$product_info_id);
		}
		if(is_array($product_ids)){
			$product_ids = implode(",",$product_ids);
		}
		if(is_array($category_id)){
			$category_id = implode(",",$category_id);
		}
		if(is_array($brand_id)){
			$brand_id = implode(",",$brand_id);
		}
		if($status != '')
		{
			$whereClauseArr[] = " FA.status=$status";
			$whereClauseArr[] = " A.status=$status";
		}
		if($article_ids!=""){
				$whereClauseArr[] = "A.article_id in ($article_ids)";
		}
		if($product_ids!=""){
			$whereClauseArr[] = " PA.product_id in($product_ids)";
		}
		if($product_info_id!=""){
			$whereClauseArr[] = " PA.product_info_id in($product_info_id)";
		}
		if($group_ids!=""){
			$whereClauseArr[] = " PA.group_id in($group_ids)";
			//$whereClauseArr[] = " PA.group_id=A.article_id ";
		}
		if($type_ids!=""){
			$whereClauseArr[] = " A.article_type in($type_ids)";
		}
		if($product_info_id!=""){
			$whereClauseArr[] = " PA.product_info_id in($product_info_id)";			
		}
		if($category_id!=""){
			$whereClauseArr[] = " PA.category_id in ($category_id)";
		}
		if($brand_id!=""){
			$whereClauseArr[] = " PA.brand_id in ($brand_id)";

		}
		$whereClauseArr[] = " PA.article_id=A.article_id ";
		$whereClauseArr[] = " FA.article_id=A.article_id ";
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
		}
		if(!empty($startlimit)){
			$limitArr[] = $startlimit;
		}
		if(!empty($cnt)){
			$limitArr[] = $cnt;
		}
		if(sizeof($limitArr) > 0){
			$limitStr = " limit ".implode(" , ",$limitArr);
		}
		$sSql="select *, DATE_FORMAT(A.create_date,'%d %b %Y') as disp_date from FEATURED_ARTICLE FA,PRODUCT_ARTICLE PA, ARTICLE A $whereClauseStr $orderby $limitStr";
		//echo $sSql;
		//exit;
		$aRes=$this->select($sSql);
		//print_r($aRes);
		return $aRes;
	}

	function getAccessoriesArticleDetails($article_ids="",$group_ids="",$type_ids="",$product_ids="",$product_info_id="",$category_id="",$brand_id="",$status="1",$startlimit="",$cnt="", $orderby=""){
		//echo 'startlimit = '.$startlimit.' count = '.$cnt."<br/>";
		$iCnt=$aParamaters['cnt'];
		if(is_array($article_ids)){
			$article_ids = implode(",",$article_ids);
		}
		if(is_array($type_ids)){
			$type_ids = implode(",",$type_ids);
		}
		if(is_array($group_ids)){
			$group_ids = implode(",",$group_ids);
		}
		if(is_array($product_info_id)){
			$product_info_id = implode(",",$product_info_id);
		}
		if(is_array($product_ids)){
			$product_ids = implode(",",$product_ids);
		}
		if(is_array($category_id)){
			$category_id = implode(",",$category_id);
		}
		if(is_array($brand_id)){
			$brand_id = implode(",",$brand_id);
		}
		if($status != '')
		{
			$whereClauseArr[] = " FA.status=$status";
			$whereClauseArr[] = " A.status=$status";
		}
		if($article_ids!=""){
				$whereClauseArr[] = "A.article_id in ($article_ids)";
		}
		if($product_ids!=""){
			$whereClauseArr[] = " PA.product_id in($product_ids)";
		}
		if($product_info_id!=""){
			$whereClauseArr[] = " PA.product_info_id in($product_info_id)";
		}
		if($group_ids!=""){
			$whereClauseArr[] = " PA.group_id in($group_ids)";
			//$whereClauseArr[] = " PA.group_id=A.article_id ";
		}
		if($type_ids!=""){
			$whereClauseArr[] = " A.article_type in($type_ids)";
		}
		if($product_info_id!=""){
			$whereClauseArr[] = " PA.product_info_id in($product_info_id)";			
		}
		if($category_id!=""){
			$whereClauseArr[] = " PA.category_id in ($category_id)";
		}
		if($brand_id!=""){
			$whereClauseArr[] = " PA.brand_id in ($brand_id)";

		}
		$whereClauseArr[] = " PA.article_id=A.article_id ";
		$whereClauseArr[] = " FA.article_id=A.article_id ";
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
		}
		if(!empty($startlimit)){
			$limitArr[] = $startlimit;
		}
		if(!empty($cnt)){
			$limitArr[] = $cnt;
		}
		if(sizeof($limitArr) > 0){
			$limitStr = " limit ".implode(" , ",$limitArr);
		}
		$sSql="select *, DATE_FORMAT(A.create_date,'%d %b %Y') as disp_date from FEATURED_ARTICLE FA,PRODUCT_ARTICLE PA, ARTICLE A $whereClauseStr $orderby $limitStr";
		//echo $sSql;
		//exit;
		$aRes=$this->select($sSql);
		//print_r($aRes);
		return $aRes;
	}
	
	/**
	* @note function is used  get  article detail list
	* @pre  aParameters is array of article details list
	* values  -
	* @post return array if successful , -2 if error occurs
	* 
	* 
	* @param $article_ids,
	*/
	function getArticleDetails($article_ids="",$group_ids="",$type_ids="",$product_ids="",$product_info_id="",$category_id="",$brand_id="",$status="1",$startlimit="",$cnt="", $orderby=""){
		//echo 'startlimit = '.$startlimit.' count = '.$cnt."<br/>";
		$iCnt=$aParamaters['cnt'];
		if(is_array($article_ids)){
			$article_ids = implode(",",$article_ids);
		}
		if(is_array($type_ids)){
			$type_ids = implode(",",$type_ids);
		}
		if(is_array($group_ids)){
			$group_ids = implode(",",$group_ids);
		}
		if(is_array($product_info_id)){
			$product_info_id = implode(",",$product_info_id);
		}
		if(is_array($product_ids)){
			$product_ids = implode(",",$product_ids);
		}
		if(is_array($category_id)){
			$category_id = implode(",",$category_id);
		}
		if(is_array($brand_id)){
			$brand_id = implode(",",$brand_id);
		}
		if($status != '')
		{
			$whereClauseArr[] = "status=$status";
		}
		if($article_ids!=""){
				$whereClauseArr[] = "A.article_id in ($article_ids)";
		}
		if($product_ids!=""){
			$whereClauseArr[] = " PA.product_id in($product_ids)";
		}
		if($product_info_id!=""){
			$whereClauseArr[] = " PA.product_info_id in($product_info_id)";
		}
		if($group_ids!=""){
			$whereClauseArr[] = " PA.group_id in($group_ids)";
			//$whereClauseArr[] = " PA.group_id=A.article_id ";
		}
		if($type_ids!=""){
			$whereClauseArr[] = " A.article_type in($type_ids)";
		}
		if($product_info_id!=""){
			$whereClauseArr[] = " PA.product_info_id in($product_info_id)";			
		}
		if($category_id!=""){
			$whereClauseArr[] = " PA.category_id in ($category_id)";
		}
		if($brand_id!=""){
			$whereClauseArr[] = " PA.brand_id in ($brand_id)";

		}
		$whereClauseArr[] = " PA.article_id=A.article_id ";
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
		}
		if(!empty($startlimit)){
			$limitArr[] = $startlimit;
		}
		if(!empty($cnt)){
			$limitArr[] = $cnt;
		}
		if(sizeof($limitArr) > 0){
			$limitStr = " limit ".implode(" , ",$limitArr);
		}
		$sSql="select *, DATE_FORMAT(A.create_date,'%d %b %Y') as disp_date from PRODUCT_ARTICLE PA, ARTICLE A $whereClauseStr $orderby $limitStr";
		//echo $sSql;exit;
		$aRes=$this->select($sSql);
		//print_r($aRes);
		return $aRes;
	}
	/**
        * @note function is used  get  article detail list by editor
        * @pre  aParameters is array of article details list
        * values  -
        * @post return array if successful , -2 if error occurs
        * 
        * 
        * @param $article_ids,
        */
        function arrGetArticleDetailsByEditor($article_ids="",$uids="",$group_ids="",$type_ids="",$product_ids="",$product_info_id="",$category_id="",$brand_id="",$status="1",$startlimit="",$cnt="", $orderby=""){
                //echo 'startlimit = '.$startlimit.' count = '.$cnt."<br/>";
                $iCnt=$aParamaters['cnt'];
                if(is_array($article_ids)){
                        $article_ids = implode(",",$article_ids);
                }
		if(is_array($uids)){
                        $uids = implode(",",$uids);
                }
                if(is_array($type_ids)){
                        $type_ids = implode(",",$type_ids);
                }
                if(is_array($group_ids)){
                        $group_ids = implode(",",$group_ids);
                }
                if(is_array($product_info_id)){
                        $product_info_id = implode(",",$product_info_id);
                }
                if(is_array($product_ids)){
                        $product_ids = implode(",",$product_ids);
                }
                if(is_array($category_id)){
                        $category_id = implode(",",$category_id);
                }
		if(is_array($brand_id)){
                        $brand_id = implode(",",$brand_id);
                }
                if($status != '')
                {
                        $whereClauseArr[] = "status=$status";
                }
                if($article_ids!=""){
                                $whereClauseArr[] = "A.article_id in ($article_ids)";
                }
		if($uids!=""){
                                $whereClauseArr[] = "A.uid in ($uids)";
                }
                if($product_ids!=""){
                        $whereClauseArr[] = " PA.product_id in($product_ids)";
                }
                if($product_info_id!=""){
                        $whereClauseArr[] = " PA.product_info_id in($product_info_id)";
                }
                if($group_ids!=""){
                        $whereClauseArr[] = " PA.group_id in($group_ids)";
                        //$whereClauseArr[] = " PA.group_id=A.article_id ";
                }
                if($type_ids!=""){
                        $whereClauseArr[] = " A.article_type in($type_ids)";
                }
                if($product_info_id!=""){
                        $whereClauseArr[] = " PA.product_info_id in($product_info_id)";
                }
                if($category_id!=""){
                        $whereClauseArr[] = " PA.category_id in ($category_id)";
                }
                if($brand_id!=""){
                        $whereClauseArr[] = " PA.brand_id in ($brand_id)";

                }
                $whereClauseArr[] = " PA.article_id=A.article_id ";
		if(sizeof($whereClauseArr) > 0){
                        $whereClauseStr = " where ".implode(" and ",$whereClauseArr);
                }
                if(!empty($startlimit)){
                        $limitArr[] = $startlimit;
                }
                if(!empty($cnt)){
                        $limitArr[] = $cnt;
                }
                if(sizeof($limitArr) > 0){
                        $limitStr = " limit ".implode(" , ",$limitArr);
                }
                $sSql="select *, DATE_FORMAT(A.create_date,'%d %b %Y') as disp_date from PRODUCT_ARTICLE PA, ARTICLE A $whereClauseStr $orderby $limitStr";
                //echo $sSql;exit;
                $aRes=$this->select($sSql);
                //print_r($aRes);
                return $aRes;
        }
	//news section //

	/**
	 * @note function is used  add/update news details
	 * @pre  aParameters is array news details
	 * values  -
		article_id
		title
		content
		city_id
	 * @post return article id if successful , 0 if error occurs
	 * 
	 * 
	 * @param aParameters
	 */
	function addUpdNewsDetails($aParameters,$sTableName){
		$aParameters['create_date'] = date('Y-m-d H:i:s');
		$aParameters['update_date'] = date('Y-m-d H:i:s');
		$sSql=$this->getInsertUpdateSql($sTableName,array_keys($aParameters),array_values($aParameters));
		//echo "SQL--".$sSql."<br>";
		$iRes=$this->insertUpdate($sSql);
		return $iRes;
	}

	/**
	* @note function is used  get  News list 
	* @pre  aParameters is array of News details
	* values  -
	* @post return array if successful , -2 if error occurs
	* 
	* 
	* @param 
	*/
	function getNewsList($aParamaters,$iStartlimit,$iCnt,$sTableName){
		$sRequest="";
		$aConstraintsArr=$aParamaters;
		$iStartlimit=$aParamaters['start'];
		$iCnt=$aParamaters['cnt'];
		$sSql=$this->getSelectSql($sTableName,$sRequest,$aConstraintsArr,$sOrderfield,$sOrder,$iStartlimit,$iCnt);
		$aRes=$this->select($sSql);
		return $aRes;
	}

	/**
	* @note function is used  get  News list 
	* @pre  aParameters is array of News details
	* values  -
	* @post return array if successful , -2 if error occurs
	* 
	* 
	* @param 
	*/
	function getProductNewsList($sArticleIds,$sType,$sTableName){
		$sRequest="";
		$aConstraintsArr=$aParamaters;
		$sOrderfield=$aParamaters['orderfield'];
		$sOrder=$aParamaters['order'];
		$iStartlimit=$aParamaters['start'];
		$iCnt=$aParamaters['cnt'];
		$sSql="select * from NEWS ";
		if($sArticleIds!='' || $sType!=''){ $sSql .=" where";}
		if($sArticleIds!=''){ $sSql .=" article_id in ($sArticleIds) "; }
		if($sArticleIds!='' && $sType!=''){ $sSql .=" and";}
		if($sType!=''){ $sSql .=" article_type=$sType"; }
		//echo $sSql;
		$aRes=$this->select($sSql);
		return $aRes;
	}

	/**
	* @note function is used  delete News
	* @pre  aParameters is  article id
	* values  -
	* @post return true if successful , -2 if error occurs
	* 
	* 
	* @param iAId,sTableName
	*/
	function deleteNews($iAId,$sTableName){
		$sSql="delete from NEWS where article_id='".$iAId."'";      
		$iRes=$this->sql_delete_data($sSql);
		$sSql='';
		$sSql="delete from PRODUCT_NEWS where article_id='".$iAId."'";      
		$iRes=$this->sql_delete_data($sSql);
	}

	/**
	* @note function is used  get  news detail
	* @pre  aParameters is array of article details
	* values  -
	* @post return array if successful , -2 if error occurs
	* 
	* 
	* @param iAId
	*/
	function getNewsDetail($iAId){
		$sRequest="";
		//$aConstraintsArr=Array("feature_id"=>1,"parent_id"=>1);
		$aConstraintsArr=Array("article_id"=>$iAId);
		$sOrderfield="";
		$sOrder="";
		$iStartlimit="";
		$iCnt="";
		$sTableName="NEWS";
		$sSql=$this->getSelectSql($sTableName,$sRequest,$aConstraintsArr,$sOrderfield,$sOrder,$iStartlimit,$iCnt);		
		$aRes=$this->select($sSql);
		return $aRes; 
	}

/**
* @note function is used to get  news count
* @pre  aParameters is array of news details list
* values  -
* @post return count
* @param $article_ids="",$group_ids="",$type_ids="",$product_ids="",$product_info_id="",$category_id="",$brand_id="",$status="1"
*/
function getNewsCount($article_ids="",$group_ids="",$type_ids="",$product_ids="",$product_info_id="",$category_id="",$brand_id="",$status="1",$orderby=""){
	$iCnt=$aParamaters['cnt'];
	if(is_array($article_ids)){
		$article_ids = implode(",",$article_ids);
	}
	if(is_array($type_ids)){
		$type_ids = implode(",",$type_ids);
	}
	if(is_array($group_ids)){
		$group_ids = implode(",",$group_ids);
	}
	if(is_array($product_info_id)){
		$product_info_id = implode(",",$product_info_id);
	}
	if(is_array($product_ids)){
		$product_ids = implode(",",$product_ids);
	}
	if(is_array($category_id)){
		$category_id = implode(",",$category_id);
	}
	if(is_array($brand_id)){
		$brand_id = implode(",",$brand_id);
	}
	if($status != '')
	{
		$whereClauseArr[] = "status=$status";
	}
	if($article_ids!=""){
			$whereClauseArr[] = "A.article_id in ($article_ids)";
	}
	if($product_ids!=""){
		$whereClauseArr[] = " PA.product_id in($product_ids)";
	}
	if($product_info_id!=""){
		$whereClauseArr[] = " PA.product_info_id in($product_info_id)";
	}
	if($group_ids!=""){
		$whereClauseArr[] = " PA.group_id in($group_ids)";
		//$whereClauseArr[] = " PA.group_id=A.article_id ";
	}
	if($type_ids!=""){
		$whereClauseArr[] = " A.article_type in($type_ids)";
		//$whereClauseArr[] = " PA.group_id=A.article_id ";
	}
	if($product_info_id!=""){
		$whereClauseArr[] = " PA.product_info_id in($product_info_id)";
	}
	if($category_id!=""){
		$whereClauseArr[] = " PA.category_id in ($category_id)";
	}
	if($brand_id!=""){
		$whereClauseArr[] = " PA.brand_id in ($brand_id)";

	}
	$whereClauseArr[] = " PA.article_id=A.article_id ";
	if(sizeof($whereClauseArr) > 0){
		$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
	}          
	$sSql="select count(*) as count from PRODUCT_NEWS PA, NEWS A $whereClauseStr";
	//echo $sSql;
	$aRes=$this->select($sSql);
	//print_r($aRes);
	return $aRes[0][count];
}//End of function getNewsCount

function getNewsCountWithoutFeatured($article_ids="",$group_ids="",$type_ids="",$product_ids="",$product_info_id="",$category_id="",$brand_id="",$status="1",$orderby=""){
	$iCnt=$aParamaters['cnt'];
	if(is_array($article_ids)){
		$article_ids = implode(",",$article_ids);
	}
	if(is_array($type_ids)){
		$type_ids = implode(",",$type_ids);
	}
	if(is_array($group_ids)){
		$group_ids = implode(",",$group_ids);
	}
	if(is_array($product_info_id)){
		$product_info_id = implode(",",$product_info_id);
	}
	if(is_array($product_ids)){
		$product_ids = implode(",",$product_ids);
	}
	if(is_array($category_id)){
		$category_id = implode(",",$category_id);
	}
	if(is_array($brand_id)){
		$brand_id = implode(",",$brand_id);
	}
	if($status != ''){
		$whereClauseArr[] = "status=$status";
	}
	if($article_ids!=""){
			$whereClauseArr[] = "A.article_id not in ($article_ids)";
	}
	if($product_ids!=""){
		$whereClauseArr[] = " PA.product_id in($product_ids)";
	}
	if($product_info_id!=""){
		$whereClauseArr[] = " PA.product_info_id in($product_info_id)";
	}
	if($group_ids!=""){
		$whereClauseArr[] = " PA.group_id in($group_ids)";
		//$whereClauseArr[] = " PA.group_id=A.article_id ";
	}
	if($type_ids!=""){
		$whereClauseArr[] = " A.article_type in($type_ids)";
		//$whereClauseArr[] = " PA.group_id=A.article_id ";
	}
	if($product_info_id!=""){
		$whereClauseArr[] = " PA.product_info_id in($product_info_id)";
	}
	if($category_id!=""){
		$whereClauseArr[] = " PA.category_id in ($category_id)";
	}
	if($brand_id!=""){
		$whereClauseArr[] = " PA.brand_id in ($brand_id)";

	}
	$whereClauseArr[] = " PA.article_id=A.article_id ";
	if(sizeof($whereClauseArr) > 0){
		$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
	}          
	$sSql="select count(*) as count from PRODUCT_NEWS PA, NEWS A $whereClauseStr";
	//echo $sSql;
	$aRes=$this->select($sSql);
	//print_r($aRes);
	return $aRes[0][count];
}//End of function getNewsCount

   /**
	* @note function is used  get  news detail list
	* @pre  aParameters is array of news details list
	* values  -
	* @post return array if successful , -2 if error occurs
	* 
	* 
	* @param $article_ids,
	*/
	function getNewsDetails($article_ids="",$group_ids="",$type_ids="",$product_ids="",$product_info_id="",$category_id="",$brand_id="",$status="1",$startlimit="",$count="",$orderby=""){
		$iCnt=$aParamaters['cnt'];
		if(is_array($article_ids)){
			$article_ids = implode(",",$article_ids);
		}
		if(is_array($type_ids)){
			$type_ids = implode(",",$type_ids);
		}
		if(is_array($group_ids)){
			$group_ids = implode(",",$group_ids);
		}
		if(is_array($product_info_id)){
			$product_info_id = implode(",",$product_info_id);
		}
		if(is_array($product_ids)){
			$product_ids = implode(",",$product_ids);
		}
		if(is_array($category_id)){
			$category_id = implode(",",$category_id);
		}
		if(is_array($brand_id)){
			$brand_id = implode(",",$brand_id);
		}
		if($status != '')
		{
			$whereClauseArr[] = "status=$status";
		}
		if($article_ids!=""){
				$whereClauseArr[] = "A.article_id in ($article_ids)";
		}
		if($product_ids!=""){
			$whereClauseArr[] = " PA.product_id in($product_ids)";
		}
		if($product_info_id!=""){
			$whereClauseArr[] = " PA.product_info_id in($product_info_id)";
		}
		if($group_ids!=""){
			$whereClauseArr[] = " PA.group_id in($group_ids)";
		}
		if($type_ids!=""){
			$whereClauseArr[] = " A.article_type in($type_ids)";
		}
		if($category_id!=""){
			$whereClauseArr[] = " PA.category_id in ($category_id)";
		}
		if($brand_id!=""){
			$whereClauseArr[] = " PA.brand_id in ($brand_id)";
		}		
	 	$whereClauseArr[] = " PA.article_id=A.article_id ";
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
		}
		if(!empty($orderby)) { 
			$sOrderBy .= $orderby; 
		}
		if(!empty($startlimit)){
			$limitArr[] = $startlimit;
		}
		if(!empty($count)){
			$limitArr[] = $count;
		}
		if(sizeof($limitArr) > 0){
			$limitStr = " limit ".implode(" , ",$limitArr);
		}		
		$sSql="select *, DATE_FORMAT(A.create_date,'%d %b %Y') as disp_date from PRODUCT_NEWS PA, NEWS A $whereClauseStr $sOrderBy $limitStr";
		//echo $sSql;
		$aRes=$this->select($sSql);
		//print_r($aRes);
		return $aRes;
	}
	
	/**
	* @note function is used  get  news detail list
	* @pre  aParameters is array of news details list
	* values  -
	* @post return array if successful , -2 if error occurs
	* 
	* 
	* @param $article_ids,
	*/
	function getNewsDetailsWithoutFeatured($article_ids="",$group_ids="",$type_ids="",$product_ids="",$product_info_id="",$category_id="",$brand_id="",$status="1",$startlimit="",$count="",$orderby=""){
		$iCnt=$aParamaters['cnt'];
		if(is_array($article_ids)){
			$article_ids = implode(",",$article_ids);
		}
		if(is_array($type_ids)){
			$type_ids = implode(",",$type_ids);
		}
		if(is_array($group_ids)){
			$group_ids = implode(",",$group_ids);
		}
		if(is_array($product_info_id)){
			$product_info_id = implode(",",$product_info_id);
		}
		if(is_array($product_ids)){
			$product_ids = implode(",",$product_ids);
		}
		if(is_array($category_id)){
			$category_id = implode(",",$category_id);
		}
		if(is_array($brand_id)){
			$brand_id = implode(",",$brand_id);
		}
		if($status != '')
		{
			$whereClauseArr[] = "status=$status";
		}
		if($article_ids!=""){
				$whereClauseArr[] = "A.article_id not in ($article_ids)";
		}
		if($product_ids!=""){
			$whereClauseArr[] = " PA.product_id in($product_ids)";
		}
		if($product_info_id!=""){
			$whereClauseArr[] = " PA.product_info_id in($product_info_id)";
		}
		if($group_ids!=""){
			$whereClauseArr[] = " PA.group_id in($group_ids)";
		}
		if($type_ids!=""){
			$whereClauseArr[] = " A.article_type in($type_ids)";
		}
		if($category_id!=""){
			$whereClauseArr[] = " PA.category_id in ($category_id)";
		}
		if($brand_id!=""){
			$whereClauseArr[] = " PA.brand_id in ($brand_id)";
		}		
	 	$whereClauseArr[] = " PA.article_id=A.article_id ";
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
		}
		if(!empty($orderby)) { 
			$sOrderBy .= $orderby; 
		}
		if(!empty($startlimit)){
			$limitArr[] = $startlimit;
		}
		if(!empty($count)){
			$limitArr[] = $count;
		}
		if(sizeof($limitArr) > 0){
			$limitStr = " limit ".implode(" , ",$limitArr);
		}		
		$sSql="select *, DATE_FORMAT(A.create_date,'%d %b %Y') as disp_date from PRODUCT_NEWS PA, NEWS A $whereClauseStr $sOrderBy $limitStr";
		//echo $sSql;
		$aRes=$this->select($sSql);
		//print_r($aRes);
		return $aRes;
	}
	


	function arrGetUploadMediaNewsDetails($prdAid){
		$sSql="select * from UPLOAD_MEDIA_NEWS where product_article_id=$prdAid";
		//echo $sSql;
		$aRes=$this->select($sSql);
		//print_r($aRes);
		return $aRes;
	}
	
	function arrGetUploadMediaArticleDetails($prdAid){
		$sSql="select * from UPLOAD_MEDIA_ARTICLE where product_article_id=$prdAid";
		//echo $sSql;
		$aRes=$this->select($sSql);
		//print_r($aRes);
		return $aRes;
	}
function arrGetLatestArticleDetails($section_id="",$article_id="",$type_id="",$status="1",$startlimit="",$cnt=""){
	//echo $section_id;
	$sSql="SELECT * FROM `LATEST_ARTICLE` LA,ARTICLE A WHERE LA.`article_id`=A.article_id";
	if($section_id!=''){ $sSql .=" and LA.section_article_id=$section_id";}
	//echo $sSql;
	$aRes=$this->select($sSql);
	//print_r($aRes);
	return $aRes;
}




function arrGetFeaturedArticleDetails($section_id="",$article_id="",$type_id="",$status="1",$startlimit="",$cnt=""){
	$sSql="SELECT *,FA.status as status FROM `FEATURED_ARTICLE` FA,ARTICLE A WHERE FA.`article_id`=A.article_id";
	if($section_id!=''){ $sSql .=" and FA.section_article_id=$section_id";}
	//echo $sSql;
	$aRes=$this->select($sSql);
	//print_r($aRes);
	return $aRes;
}

function arrGetRelatedArticleDetails($section_id="",$article_id="",$category_id="",$article_type_id="",$status="1",$startlimit="",$cnt=""){
	$whereClauseArr[] = "RA.article_id=A.article_id";
	if(!empty($section_id)){
		$whereClauseArr[] = "section_article_id = $section_id";
	}
	if(!empty($article_id)){
		$whereClauseArr[] = "RA.article_id = $article_id";
	}
	if(!empty($category_id)){
		$whereClauseArr[] = "RA.category_id = $category_id";
	}
	if(!empty($article_type_id)){
		$whereClauseArr[] = "RA.article_type = $article_type_id";
	}
	if(sizeof($whereClauseArr) > 0){
		$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
	}
	if(!empty($startlimit)){
		$limitArr[] = $startlimit;
	}
	if(!empty($count)){
		$limitArr[] = $count;
	}
	if(sizeof($limitArr) > 0){
		$limitStr = " limit ".implode(" , ",$limitArr);
	}
	$sSql="SELECT *, DATE_FORMAT(A.create_date,'%d %b %Y') as disp_date FROM `RELATED_ARTICLE` RA,ARTICLE A $whereClauseStr $limitStr";
	$aRes=$this->select($sSql);
	//print_r($aRes);
	return $aRes;
}


function arrGetNewsDetails($article_id="",$category_id="",$status="1",$startlimit="",$count="", $orderby=""){

	//echo $section_id;
	$sSql="SELECT *, DATE_FORMAT(create_date,'%d/%m/%Y') as disp_date FROM NEWS ";
	//if($category_id!=""){ $sSql .= " category_id in ($category_id) "; }
	if(!empty($orderby)) { $sSql .= $orderby; }
	if(!empty($startlimit)){
		$limitArr[] = $startlimit;
	}
	if(!empty($count)){
		$limitArr[] = $count;
	}
	if(sizeof($limitArr) > 0){ $limitStr = " limit ".implode(" , ",$limitArr); }
	$sSql = $sSql.$limitStr;
	//echo $sSql;
	$aRes=$this->select($sSql);
	//print_r($aRes);
	return $aRes;
}


function arrGetLatestNewsDetailsold($section_id="",$article_id="",$type_id="",$category_id="",$status="1",$startlimit="",$count="", $orderby=""){

	//echo $section_id;
	$sSql="SELECT *, DATE_FORMAT(A.create_date,'%d %b %Y') as disp_date FROM `LATEST_NEWS` LA,NEWS A WHERE LA.`article_id`=A.article_id";
	if($category_id!=""){ $sSql .= " and LA.category_id in ($category_id) "; }
	if($section_id!=''){ $sSql .=" and LA.section_news_id=$section_id"; }
	
	if(!empty($orderby)) { $sSql .= $orderby; }
	if(!empty($startlimit)){
		$limitArr[] = $startlimit;
	}
	if(!empty($count)){
		$limitArr[] = $count;
	}
	if(sizeof($limitArr) > 0){ $limitStr = " limit ".implode(" , ",$limitArr); }
	$sSql = $sSql.$limitStr;
	//echo $sSql;
	$aRes=$this->select($sSql);
	//print_r($aRes);
	return $aRes;
}

function arrGetFeaturedNewsDetailsold($section_id="",$article_id="",$type_id="",$status="1",$startlimit="",$count="",$category_id="",$orderby=""){
$sSql="SELECT *, DATE_FORMAT(A.create_date,'%d/%m/%Y') as disp_date FROM `FEATURED_NEWS` FA,NEWS A WHERE FA.`article_id`=A.article_id";
if($category_id!=""){ $sSql .= " and FA.category_id in ($category_id) "; }
if($section_id!=''){ $sSql .=" and FA.section_news_id=$section_id";}
if(!empty($orderby)) { $sSql .= $orderby; }
if(!empty($startlimit)){
$limitArr[] = $startlimit;
}
if(!empty($count)){
$limitArr[] = $count;
}
if(sizeof($limitArr) > 0){ $limitStr = " limit ".implode(" , ",$limitArr); }
$sSql = $sSql.$limitStr;
//echo $sSql;
$aRes=$this->select($sSql);
//print_r($aRes);
return $aRes;
}

function arrGetFeaturedNewsDetails($section_id="",$article_id="",$category_id="",$article_type_id="",$status="1",$startlimit="",$cnt=""){
	$whereClauseArr[] = "RA.article_id=A.article_id";
	if(!empty($section_id)){
		$whereClauseArr[] = "section_news_id = $section_id";
	}
	if(!empty($article_id)){
		$whereClauseArr[] = "RA.article_id = $article_id";
	}
	if(!empty($category_id)){
		$whereClauseArr[] = "RA.category_id = $category_id";
	}
	if(!empty($article_type_id)){
		$whereClauseArr[] = "RA.article_type = $article_type_id";
	}
	
	if($status != '')
	{
		$whereClauseArr[] = " RA.status=$status";
		//$whereClauseArr[] = " RA.status=$status";
	}

	if(sizeof($whereClauseArr) > 0){
		$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
	}
	if(!empty($startlimit)){
		$limitArr[] = $startlimit;
	}
	if(!empty($count)){
		$limitArr[] = $count;
	}
	if(sizeof($limitArr) > 0){
		$limitStr = " limit ".implode(" , ",$limitArr);
	}
	$sSql="SELECT *,RA.status as status ,DATE_FORMAT(A.create_date,'%d/%m/%Y') as disp_date FROM `FEATURED_NEWS` RA,NEWS A $whereClauseStr $limitStr";
	$aRes=$this->select($sSql);
	//print_r($aRes);
	return $aRes;
}

function GetFeaturedNewsDetails($section_id="",$article_id="",$category_id="",$article_type_id="",$status="1",$startlimit="",$cnt="",$orderby=""){
	$whereClauseArr[] = "RA.article_id=A.article_id";
	if(!empty($section_id)){
		$whereClauseArr[] = "section_news_id = $section_id";
	}
	if(!empty($article_id)){
		$whereClauseArr[] = "RA.article_id = $article_id";
	}
	if(!empty($category_id)){
		$whereClauseArr[] = "RA.category_id = $category_id";
	}
	if(!empty($article_type_id)){
		$whereClauseArr[] = "RA.article_type = $article_type_id";
	}
	
	if($status != '')
	{
		$whereClauseArr[] = " RA.status=$status";
		$whereClauseArr[] = " A.status=$status";
	}

	if(sizeof($whereClauseArr) > 0){
		$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
	}
	if($startlimit!=""){
		$limitArr[] = $startlimit;
	}
	if(!empty($cnt)){
		$limitArr[] = $cnt;
	}
	if(sizeof($limitArr) > 0){
		$limitStr = " limit ".implode(" , ",$limitArr);
	}
	$sSql="SELECT *, DATE_FORMAT(A.create_date,'%d %b %Y') as disp_date FROM `FEATURED_NEWS` RA,NEWS A $whereClauseStr $orderby $limitStr";
	$aRes=$this->select($sSql);
	//echo $sSql;
	//print_r($aRes);
	return $aRes;
}

function GetFeaturedNewsDetailsCount($section_id="",$article_id="",$category_id="",$article_type_id="",$status="1",$startlimit="",$cnt="",$orderby=""){
	$whereClauseArr[] = "RA.article_id=A.article_id";
	if(!empty($section_id)){
		$whereClauseArr[] = "section_news_id = $section_id";
	}
	if(!empty($article_id)){
		$whereClauseArr[] = "RA.article_id = $article_id";
	}
	if(!empty($category_id)){
		$whereClauseArr[] = "RA.category_id = $category_id";
	}
	if(!empty($article_type_id)){
		$whereClauseArr[] = "RA.article_type = $article_type_id";
	}
	
	if($status != '')
	{
		$whereClauseArr[] = " RA.status=$status";
		$whereClauseArr[] = " A.status=$status";
	}

	if(sizeof($whereClauseArr) > 0){
		$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
	}
	if($startlimit!=""){
		$limitArr[] = $startlimit;
	}
	if(!empty($cnt)){
		$limitArr[] = $cnt;
	}
	if(sizeof($limitArr) > 0){
		$limitStr = " limit ".implode(" , ",$limitArr);
	}
	$sSql="select count(*) as count from `FEATURED_NEWS` RA,NEWS A $whereClauseStr";
	//echo $sSql;
	$aRes=$this->select($sSql);
	return $aRes[0]['count'];
}


function arrGetLatestNewsDetails($section_id="",$article_id="",$category_id="",$article_type_id="",$status="1",$startlimit="",$cnt=""){
	$whereClauseArr[] = "RA.article_id=A.article_id";
	if(!empty($section_id)){
		$whereClauseArr[] = "section_news_id = $section_id";
	}
	if(!empty($article_id)){
		$whereClauseArr[] = "RA.article_id = $article_id";
	}
	if(!empty($category_id)){
		$whereClauseArr[] = "RA.category_id = $category_id";
	}
	if(!empty($article_type_id)){
		$whereClauseArr[] = "RA.article_type = $article_type_id";
	}
	
	if($status != ''){
		$whereClauseArr[] = " RA.status=$status";
		//$whereClauseArr[] = " RA.status=$status";
	}

	if(sizeof($whereClauseArr) > 0){
		$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
	}
	if(!empty($startlimit)){
		$limitArr[] = $startlimit;
	}
	if(!empty($count)){
		$limitArr[] = $count;
	}
	if(sizeof($limitArr) > 0){
		$limitStr = " limit ".implode(" , ",$limitArr);
	}
	$sSql="SELECT *, RA.status as status, DATE_FORMAT(A.create_date,'%d/%m/%Y') as disp_date FROM `LATEST_NEWS` RA,NEWS A $whereClauseStr $limitStr";
	$aRes=$this->select($sSql);
	//print_r($aRes);
	return $aRes;
}

function arrGetRelatedNewsDetails($section_id="",$article_id="",$category_id="",$article_type_id="",$status="1",$startlimit="",$cnt=""){
	$whereClauseArr[] = "RA.article_id=A.article_id";
	if(!empty($section_id)){
		$whereClauseArr[] = "section_news_id = $section_id";
	}
	if(!empty($article_id)){
		$whereClauseArr[] = "RA.article_id = $article_id";
	}
	if(!empty($category_id)){
		$whereClauseArr[] = "RA.category_id = $category_id";
	}
	if(!empty($article_type_id)){
		$whereClauseArr[] = "RA.article_type = $article_type_id";
	}
	if($status != '')
	{
		$whereClauseArr[] = " RA.status=$status";
		//$whereClauseArr[] = " RA.status=$status";
	}
	if(sizeof($whereClauseArr) > 0){
		$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
	}
	if(!empty($startlimit)){
		$limitArr[] = $startlimit;
	}
	if(!empty($count)){
		$limitArr[] = $count;
	}
	if(sizeof($limitArr) > 0){
		$limitStr = " limit ".implode(" , ",$limitArr);
	}
	$sSql="SELECT *, RA.status as status , DATE_FORMAT(A.create_date,'%d %b %Y') as disp_date FROM `RELATED_NEWS` RA,NEWS A $whereClauseStr $limitStr";
	//echo $sSql;
       $aRes=$this->select($sSql);
	//print_r($aRes);
	return $aRes;
}

	function arrGetRelatedNews($article_ids="",$uids="",$article_types="",$product_article_ids="",$category_ids="",$brand_ids="",$product_ids="",$product_info_ids="",$status="1",$startlimit="",$count="", $orderby=""){

		if(is_array($article_ids)){
                        $article_ids = implode(",",$article_ids);
                }
                if(is_array($uids)){
                        $uids = implode(",",$uids);
                }
                if(is_array($article_types)){
                        $article_types = implode(",",$article_types);
                }
                if(is_array($product_article_ids)){
                        $product_article_ids = implode(",",$product_article_ids);
                }
                if(is_array($category_ids)){
                        $category_ids = implode(",",$category_ids);
                }
                if(is_array($brand_ids)){
                        $brand_ids = implode(",",$brand_ids);
                }
                if(is_array($product_info_ids)){
                        $product_info_ids = implode(",",$product_info_ids);
                }
                if(is_array($product_ids)){
                        $product_ids = implode(",",$product_ids);
                }
                if($status != ''){
                        $whereClauseArr[] = "N.status=$status";
                }
                if($article_ids != ""){
                        $whereClauseArr[] = "N.article_id in ($article_ids)";
                }
                if($uids != ""){
                        $whereClauseArr[] = "N.uid in ($uids)";
                }
                if($article_types != ""){
                        $whereClauseArr[] = "N.article_type in ($article_types)";
                }
                if($product_article_ids != ""){
                        $whereClauseArr[] = " PN.product_article_id in ($product_article_ids)";
                }
                if($category_ids != ""){
                        $whereClauseArr[] = "PN.category_id in ($category_ids)";
                }
                if($brand_ids != ""){
                        $whereClauseArr[] = "PN.brand_id in ($brand_ids)";
                }
                if($product_ids != ""){
                        $whereClauseArr[] = "PN.product_id in ($product_ids)";
                }
                if($product_info_ids != ""){
                        $whereClauseArr[] = "PN.product_info_id in ($product_info_ids)";
                }
		$whereClauseArr[] = " PN.article_id=N.article_id ";
                if(sizeof($whereClauseArr) > 0){
                        $whereClauseStr = " where ".implode(" and ",$whereClauseArr);
                }
                if(!empty($orderby)) {
			$orderby = " order by N.".$orderby." DESC";
                }
                if(!empty($startlimit)){
                        $limitArr[] = $startlimit;
                }
                if(!empty($count)){
                        $limitArr[] = $count;
                }
                if(sizeof($limitArr) > 0){
                        $limitStr = " limit ".implode(" , ",$limitArr);
                }

        	$sSql="SELECT *, DATE_FORMAT(N.create_date,'%d/%m/%Y') as disp_date FROM NEWS N,PRODUCT_NEWS PN $whereClauseStr $orderby $limitStr";
        	$aRes=$this->select($sSql);
	        //print_r($aRes);
        	return $aRes;
	}


	function deleteRelatedArticle($iAId,$sTableName){
		$sSql='';
		$sSql="delete from $sTableName where section_article_id='".$iAId."'";      
		$iRes=$this->sql_delete_data($sSql);
	}

	function deleteRelatedNews($iAId,$sTableName){
		$sSql='';
		$sSql="delete from $sTableName where section_news_id='".$iAId."'";   
              //echo  $sSql;  
		$iRes=$this->sql_delete_data($sSql);
	}

       function deleteCategoryNews($iAId,$sTableName){
		$sSql='';
		$sSql="delete from $sTableName where section_article_id='".$iAId."'";   
              //echo  $sSql;  
		$iRes=$this->sql_delete_data($sSql);
	}

	
	function arrGetArticleDetailsWithMedia($product_article_id,$startlimit="",$cnt=""){
               //echo 'startlimit = '.$startlimit.' count = '.$cnt."<br/>"."product=".$product_article_id;
                if(!empty($startlimit)){
                        $limitArr[] = $startlimit;
                }
                if(!empty($cnt)){
                        $limitArr[] = $cnt;
                }
                if(sizeof($limitArr) > 0){
                        $limitStr = " limit ".implode(" , ",$limitArr);
                }

		$sql = "select * from UPLOAD_MEDIA_ARTICLE where product_article_id = $product_article_id order by upload_media_id asc $limitStr";
             // echo $sql; //exit;
		$result = $this->select($sql);
		return $result;
	}
	
	function arrGetNewsDetailsWithMedia($product_article_id){
		$sql = "select * from UPLOAD_MEDIA_NEWS where product_article_id = $product_article_id order by upload_media_id asc";
		$result = $this->select($sql);
		return $result;
	}
	function arrGetArticleVideoDetails($video_id="",$group_id="",$product_id="",$product_info_id="",$category_id="",$brand_id="",$status="",$article_type="",$startlimit="",$cnt=""){
              
		if($group_id!=''){
                        $whereClauseArr[] = "PA.group_id=$group_id";
                }
                if($product_id!=''){
                        $whereClauseArr[] = "PA.product_id=$product_id";
                }
                if($product_info_id!=""){
                        $whereClauseArr[] = " PA.product_info_id=$product_info_id";
                }
                if($category_id!=""){
                        $whereClauseArr[] = " PA.category_id=$category_id";
                }
                if($brand_id!=""){
                        $whereClauseArr[] = " PA.brand_id=$brand_id";
                }
                if($status != ''){
                        $whereClauseArr[] = "A.status=$status";
                }
                if($video_id != ''){
                        $whereClauseArr[] = "UMA.upload_media_id=$video_id";
                }
               if($article_type  != ''){
                        $whereClauseArr[] = "A.article_type =$article_type";
                }
                
               if(!empty($startlimit)){
			$limitArr[] = $startlimit;
		}
		if(!empty($cnt)){
			$limitArr[] = $cnt;
		}
		if(sizeof($limitArr) > 0){
			$limitStr = " limit ".implode(" , ",$limitArr);
		}


    
                $whereClauseArr[] = " A.article_id = PA.article_id ";
                $whereClauseArr[] = " PA.product_article_id = UMA.product_article_id ";
                $whereClauseArr[] = " UMA.content_type=1 ";
                if(sizeof($whereClauseArr) > 0){
                        $whereClauseStr = " where ".implode(" and ",$whereClauseArr);
                }
                $sSql="SELECT UMA.upload_media_id as video_id,A.title,A.tags,UMA.media_id,UMA.media_path,UMA.video_img_id,UMA.video_img_path,UMA.content_type,UMA.is_media_process,A.status,UMA.create_date,UMA.update_date,PA.brand_id,PA.product_id,PA.product_info_id FROM ARTICLE A,PRODUCT_ARTICLE PA,UPLOAD_MEDIA_ARTICLE UMA 
                      $whereClauseStr group by UMA.upload_media_id $limitStr";
                
                $aRes=$this->select($sSql);
//echo $sSql;exit;
                return $aRes;
        }


		function arrGetArticleVideoDetailsCnt($video_id="",$group_id="",$product_id="",$product_info_id="",$category_id="",$brand_id="",$status="",$article_type=""){
		if($group_id!=''){
                        $whereClauseArr[] = "PA.group_id=$group_id";
                }
                if($product_id!=''){
                        $whereClauseArr[] = "PA.product_id=$product_id";
                }
                if($product_info_id!=""){
                        $whereClauseArr[] = " PA.product_info_id=$product_info_id";
                }
                if($category_id!=""){
                        $whereClauseArr[] = " PA.category_id=$category_id";
                }
                if($brand_id!=""){
                        $whereClauseArr[] = " PA.brand_id=$brand_id";
                }
                if($status != ''){
                        $whereClauseArr[] = "A.status=$status";
                }
                if($video_id != ''){
                        $whereClauseArr[] = "UMA.upload_media_id=$video_id";
                }
               if($article_type  != ''){
                        $whereClauseArr[] = "A.article_type =$article_type";
                }


                
                $whereClauseArr[] = " A.article_id = PA.article_id ";
                $whereClauseArr[] = " PA.product_article_id = UMA.product_article_id ";
                $whereClauseArr[] = " UMA.content_type=1 ";
                if(sizeof($whereClauseArr) > 0){
                        $whereClauseStr = " where ".implode(" and ",$whereClauseArr);
                }
                //$sSql="SELECT count(UMA.upload_media_id) as cnt FROM ARTICLE A,PRODUCT_ARTICLE PA,UPLOAD_MEDIA_ARTICLE UMA $whereClauseStr group by UMA.upload_media_id";
				$sSql="SELECT count(UMA.upload_media_id) as cnt FROM ARTICLE A,PRODUCT_ARTICLE PA,UPLOAD_MEDIA_ARTICLE UMA $whereClauseStr";
                //echo $sSql;
                $aRes=$this->select($sSql);
                return $aRes;
        }
	function arrGetNewsVideoDetails($video_id="",$group_id="",$product_id="",$product_info_id="",$category_id="",$brand_id="",$status="",$article_id ="",$startlimit="",$cnt="",$orderby=""){
		if($video_id != ''){
                        $whereClauseArr[] = "UMN.upload_media_id=$video_id";
                }
		if($status != ''){
                        $whereClauseArr[] = "N.status=$status";
                }
				if($group_id!=''){
                        $whereClauseArr[] = "PN.group_id=$group_id";
                }
                if($product_id!=''){
                        $whereClauseArr[] = "PN.product_id=$product_id";
                }
                if($product_info_id!=""){
                        $whereClauseArr[] = " PN.product_info_id=$product_info_id";
                }
                if($category_id!=""){
                        $whereClauseArr[] = " PN.category_id=$category_id";
                }
                if($brand_id!=""){
                        $whereClauseArr[] = " PN.brand_id=$brand_id";
                }
                if($article_id !=""){
                        $whereClauseArr[] = " N.article_id =$article_id ";
                }

                $whereClauseArr[] = " N.article_id = PN.article_id ";
                $whereClauseArr[] = " PN.product_article_id = UMN.product_article_id ";
                $whereClauseArr[] = " UMN.content_type=1 ";
                if(sizeof($whereClauseArr) > 0){
                        $whereClauseStr = " where ".implode(" and ",$whereClauseArr);
                }
              if(!empty($startlimit)){
			$limitArr[] = $startlimit;
		}
		if(!empty($cnt)){
			$limitArr[] = $cnt;
		}
		if(sizeof($limitArr) > 0){
			$limitStr = " limit ".implode(" , ",$limitArr);
		}



               $sSql="SELECT UMN.upload_media_id as video_id,N.title,N.article_id,N.tags,UMN.media_id,UMN.media_path,UMN.video_img_id,UMN.video_img_path,UMN.content_type,
                   UMN.is_media_process,N.status,UMN.create_date,UMN.update_date,PN.brand_id,PN.product_id,PN.product_info_id FROM NEWS N,PRODUCT_NEWS PN,UPLOAD_MEDIA_NEWS UMN 
                    $whereClauseStr   group by UMN.upload_media_id $orderby $limitStr";
                //echo $sSql; // exit;
                $aRes=$this->select($sSql);
                return $aRes;
        }

		function arrGetNewsVideoDetailsCnt($video_id="",$group_id="",$product_id="",$product_info_id="",$category_id="",$brand_id="",$status="",$article_id =""){
		if($video_id != ''){
                        $whereClauseArr[] = "UMN.upload_media_id=$video_id";
                }
		if($status != ''){
                        $whereClauseArr[] = "N.status=$status";
                }
				if($group_id!=''){
                        $whereClauseArr[] = "PN.group_id=$group_id";
                }
                if($product_id!=''){
                        $whereClauseArr[] = "PN.product_id=$product_id";
                }
                if($product_info_id!=""){
                        $whereClauseArr[] = " PN.product_info_id=$product_info_id";
                }
                if($category_id!=""){
                        $whereClauseArr[] = " PN.category_id=$category_id";
                }
                if($brand_id!=""){
                        $whereClauseArr[] = " PN.brand_id=$brand_id";
                }
                if($article_id !=""){
                        $whereClauseArr[] = " N.article_id =$article_id ";
                }

                $whereClauseArr[] = " N.article_id = PN.article_id ";
                $whereClauseArr[] = " PN.product_article_id = UMN.product_article_id ";
                $whereClauseArr[] = " UMN.content_type=1 ";
                if(sizeof($whereClauseArr) > 0){
                        $whereClauseStr = " where ".implode(" and ",$whereClauseArr);
                }
               //$sSql="SELECT count(UMN.upload_media_id) as cnt FROM NEWS N,PRODUCT_NEWS PN,UPLOAD_MEDIA_NEWS UMN $whereClauseStr group by UMN.upload_media_id";
                $sSql="SELECT count(UMN.upload_media_id) as cnt FROM NEWS N,PRODUCT_NEWS PN,UPLOAD_MEDIA_NEWS UMN $whereClauseStr";

               // echo $sSql; // exit;
                $aRes=$this->select($sSql);
                return $aRes;
        }

	/**
        * @note function is used to insert Article Caegory details.
        *
        * @param associative array $insert_param.
        * @pre $insert_param must be valid non-empty associative array.
        * @post integer $id.
        * return integer.
        */
        function intInsertArticleCategoryBase($insert_param){
                $insert_param['create_date'] = date('Y-m-d H:i:s');
                $insert_param['update_date'] = date('Y-m-d H:i:s');
                $sql = $this->getInsertSql("ARTICLE_CATEGORYBASE",array_keys($insert_param),array_values($insert_param));
                //echo $sql;
		$id = $this->insert($sql);
		if($id == 'Duplicate entry'){ return 'exists';}
                return $id;
        }
        /**
         * @note function is used to update the Article Category details into the database.
         * @param an associative array $update_param.
         * @param an integer $id.
         * @pre $update_param must be valid associative array and $id must be non-empty/zero valid integer.
         * @post boolean true/false.
         * retun boolean.
         */
         function boolUpdateArticleCategoryBase($id,$update_param){
                 $update_param['create_date'] = date('Y-m-d H:i:s');
                 $update_param['update_date'] = date('Y-m-d H:i:s');
                 $sql = $this->getUpdateSql("ARTICLE_CATEGORYBASE",array_keys($update_param),array_values($update_param),"id",$id);
                 //echo $sql;
                 $isUpdate = $this->update($sql);
                 return $isUpdate;
         }

	/**
        * @note function is used  delete video type
        *
        * @param id ,table_name
        * @pre  id is single id of integer type
        * @pre  table_name is database table name 
        * @post return true if successful , false if error occurs
        */
        function booldeleteArticleCategory($id="",$table_name=""){
                $sSql="delete from $table_name where id='".$id."'";
                $iRes=$this->sql_delete_data($sSql);
                return $iRes;
        }
	/**
        * @note function is used to get video type details list 
        *
        * @param an integer/comma seperated ids/ ids array $ids.
	* @param an integer/comma seperated article type ids/ article type ids array $article_type_ids.
	* @param an integer/comma seperated article sub type ids/ article sub type ids array $article_sub_type_ids.
        * @param an integer/comma seperated category ids/ category ids array $category_ids.
	* @param an integer/comma seperated article ids/ article ids array $article_ids.
        * @param boolean Active/InActive $status
        * @param integer $startlimit.
        * @param integer $cnt.
        * @param is a string $orderby.
        * @pre not required.
        *
        * @post return array if successful , 0 if error occurs
        * 
        */
	function arrGetArticleCategoryBaseDetails($ids="",$article_type_ids="",$article_sub_type_ids="",$category_ids="",$article_ids="",$status="1",$startlimit="",$cnt="",$orderby=""){
                if(is_array($ids)){
                        $ids = implode(",",$ids);
                }
                if(is_array($article_type_ids)){
                        $article_type_ids = implode(",",$article_type_ids);
                }
                if(is_array($article_sub_type_ids)){
                        $article_sub_type_ids = implode(",",$article_sub_type_ids);
                }
                if(is_array($article_ids)){
                        $article_ids = implode(",",$article_ids);
                }
                if(is_array($category_ids)){
                        $category_ids = implode(",",$category_ids);
                }
		if($ids!=""){
                        $whereClauseArr[] = "id in ($ids)";
                }
		if($article_type_ids!=""){
                        $whereClauseArr[] = "article_type_id in ($article_type_ids)";
                }
		if($article_sub_type_ids!=""){
                        $whereClauseArr[] = "article_sub_type_id in ($article_sub_type_ids)";
                }
		if($article_ids!=""){
                        $whereClauseArr[] = "article_id in ($article_ids)";
                }
                if($category_ids!=""){
                        $whereClauseArr[] = "category_id in ($category_ids)";
                }
                if($status != ''){
                        $whereClauseArr[] = "status=$status";
                }
                if(sizeof($whereClauseArr) > 0){
                        $whereClauseStr = " where ".implode(" and ",$whereClauseArr);
                }
                if(empty($orderby)) {
                        $orderby= " order by create_date DESC ";
                }
                if(!empty($startlimit)){
                        $limitArr[] = $startlimit;
                }
                if(!empty($cnt)){
                        $limitArr[] = $cnt;
                }
                if(sizeof($limitArr) > 0){
                        $limitStr = " limit ".implode(" , ",$limitArr);
                }
                $sSql="SELECT * FROM  ARTICLE_CATEGORYBASE $whereClauseStr $orderby $limitStr";
		//echo $sSql;
                $aRes=$this->select($sSql);
		
		return $aRes;
	}

	/**
        * @note function is used to insert Article Sub Type details.
        *
        * @param associative array $insert_param.
        * @pre $insert_param must be valid non-empty associative array.
        * @post integer $article_sub_type_id.
        * return integer.
        */
        function intInsertArticleSubType($insert_param){
                $insert_param['create_date'] = date('Y-m-d H:i:s');
                $sql = $this->getInsertSql("ARTICLE_SUB_TYPE",array_keys($insert_param),array_values($insert_param));
                //echo $sql;
                $article_sub_type_id = $this->insert($sql);
                if($article_sub_type_id == 'Duplicate entry'){ return 'exists';}
                return $article_sub_type_id;
        }
        /**
         * @note function is used to update the article sub type details into the database.
         * @param an associative array $update_param.
         * @param an integer $article_sub_type_id.
         * @pre $update_param must be valid associative array and $id must be non-empty/zero valid integer.
         * @post boolean true/false.
         * retun boolean.
         */
         function boolUpdateArticleSubType($article_sub_type_id,$update_param){
                 $update_param['create_date'] = date('Y-m-d H:i:s');
                 $sql = $this->getUpdateSql("ARTICLE_SUB_TYPE",array_keys($update_param),array_values($update_param),"article_sub_type_id",$article_sub_type_id);
                 //echo $sql;
                 $isUpdate = $this->update($sql);
                 return $isUpdate;
         }
	/**
        * @note function is used  delete article sub type
        *
        * @param article_sub_type_id,table_name
        * @pre  article_sub_type_id is single id of integer type
        * @pre  table_name is database table name 
        * @post return true if successful , false if error occurs
        */
        function booldeleteArticleSubType($article_sub_type_id="",$table_name=""){
                $sSql="delete from $table_name where article_sub_type_id='".$article_sub_type_id."'";
                $iRes=$this->sql_delete_data($sSql);
                return $iRes;
        }

	/**
        * @note function is used to get article sub type details list
        *
        * @param an integer/comma seperated article sub type ids/ article sub type ids array $article_sub_type_ids.
        * @param an integer/comma seperated article type ids/ article type ids array $article_type_ids.
        * @param an integer/comma seperated category ids/ category ids array $category_ids.
        * @param is a string $sub_type_name.
	* @param boolean Active/InActive $status
        * @param integer $startlimit.
        * @param integer $cnt.
        * @param is a string $orderby.
        * @pre not required.
        *
        * @post associative array.
        * @post return array if successful , 0 if error occurs
        * 
        */
        function arrGetArticleSubTypeDetails($article_sub_type_ids="",$article_type_ids="",$category_ids="",$sub_type_name="",$status="",$startlimit="",$cnt="",$orderby=""){
                if(is_array($article_sub_type_ids)){
                        $article_sub_type_ids = implode(",",$article_sub_type_ids);
                }
                if(is_array($article_type_ids)){
                        $article_type_ids = implode(",",$article_type_ids);
                }
                if(is_array($category_ids)){
                        $category_ids = implode(",",$category_ids);
                }
                 if($article_sub_type_ids != ''){
                        $whereClauseArr[] = " article_sub_type_id in ($article_sub_type_ids) ";
                }
                if($article_type_ids != ''){
                        $whereClauseArr[] = " article_type_id in ($article_type_ids) ";
                } 
                if($category_ids != ''){
                        $whereClauseArr[] = " category_id in ($category_ids) ";
                }
		if(!empty($sub_type_name)){
                        $whereClauseArr[] = "lower(sub_type_name) = ".strtolower($sub_type_name);
                }
		if($status != ''){
                        $whereClauseArr[] = "status=$status";
                } 
                if(sizeof($whereClauseArr) > 0){
                        $whereClauseStr = " where ".implode(" and ",$whereClauseArr);
                }
                if(!empty($startlimit)){
                        $limitArr[] = $startlimit;
                }
                if(!empty($cnt)){
                        $limitArr[] = $cnt;
                }
                if(sizeof($limitArr) > 0){
                        $limitStr = " limit ".implode(" , ",$limitArr);
                }
                if(empty($orderby)){
                        $orderby= "order by create_date desc";
                }
                $sql="Select * from ARTICLE_SUB_TYPE $whereClauseStr $limitStr $orderby";
		//echo $sql;
		$result = $this->select($sql);
                return $result;
        }
	

	/**
	* @note function is used  get  Article list 
	* @pre  aParameters is array of article details
	* values  -
	* @post return array if successful , -2 if error occurs
	* 
	* 
	* @param 
	*/

	function getProductRivewCNT($sArticleIds,$sType,$sTableName){
		//print_r($aParamaters);
		$sRequest="";
		//$aConstraintsArr=Array("feature_id"=>1,"parent_id"=>1);
		#$aConstraintsArr=$aParamaters;
		#$sOrderfield=$aParamaters['orderfield'];
		#$sOrder=$aParamaters['order'];
		#$iStartlimit=$aParamaters['start'];
		#$iCnt=$aParamaters['cnt'];
		//echo $sOrderfield,$sOrder,$iStartlimit,$iCnt;
		//$sSql=$this->getSelectSql($sTableName,$sRequest,$aConstraintsArr,$sOrderfield,$sOrder,$iStartlimit,$iCnt);
		$sSql="select * from $sTableName where article_id in ($sArticleIds) ";
		
		//echo $sSql;
		$aRes=$this->select($sSql);
		return $aRes;
	}


   function arrGetCatgoryNewsDetails($section_id="",$article_id="",$category_id="",$article_type_id="",$status="1",$startlimit="",$cnt=""){
	$whereClauseArr[] = "RA.article_id=A.article_id";
	if(!empty($section_id)){
		$whereClauseArr[] = "section_article_id = $section_id";
	}
	if(!empty($article_id)){
		$whereClauseArr[] = "RA.article_id = $article_id";
	}
	if(!empty($category_id)){
		$whereClauseArr[] = "RA.category_id = $category_id";
	}
	if(!empty($article_type_id)){
		$whereClauseArr[] = "RA.article_type = $article_type_id";
	}
	
	if($status != ''){
		$whereClauseArr[] = " RA.status=$status";
		//$whereClauseArr[] = " RA.status=$status";
	}

	if(sizeof($whereClauseArr) > 0){
		$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
	}
	if(!empty($startlimit)){
		$limitArr[] = $startlimit;
	}
	if(!empty($count)){
		$limitArr[] = $count;
	}
	if(sizeof($limitArr) > 0){
		$limitStr = " limit ".implode(" , ",$limitArr);
	}
	$sSql="SELECT *, RA.status as status, DATE_FORMAT(A.create_date,'%d/%m/%Y') as disp_date FROM `CATEGORY_NEWS` RA,NEWS A $whereClauseStr $limitStr";
	//echo $sSql;
       $aRes=$this->select($sSql);    
	//print_r($aRes);
	return $aRes;
    }

 
        function getCatNews($type_name,$sTableName){
             if(!empty($type_name)){
              $whereClauseArr[] = "type_name='$type_name'";
             } 
              if(sizeof($whereClauseArr) > 0){
		$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
	       }
		$sSql="select * from $sTableName $whereClauseStr";
		//echo $sSql;
		$aRes=$this->select($sSql);
		return $aRes;
	}


	
}
?>
