<?php
	require_once(CLASSPATH."campus_discussion.class.php");
	/**
	* @brief class is used add,update,delete,get product details.
	* @author Rajesh Ujade
	* @version 1.0
	* @created 16-Feb-2011 5:09:31 PM
	*/
	class report extends DbOperation{
		/**Intialize the consturctor.*/
		var $oCampusDiscussion;
		function report(){
			$this->oCampusDiscussion	        =  new campus_discussion();
		}
		function arrGetOverAllUserReviewByModel(){
		}
		function arrCalculateUserReview(){
		}
		function intInsertUpdateCommentCount($request_param){
			$result = $this->oCampusDiscussion->addCampusDiscussion($request_param);
			//print "<pre>"; print_r($result);
			//echo MB_API_HOST;
			//die();
			if(sizeof($result) > 0){
				$iTId = $result['tid'];
				$id = $request_param["title"];
				//settype($id,"integer");
				//echo "IDID--".$id;
				$service_id = $request_param['sid'];
				$comment_type_id = $request_param['cid'];
				$sRequestUrl=WEB_URL.substr($_SERVER['REQUEST_URI'],1);
				//$aParameters=Array("title"=>$article_id,"turl"=>$sRequestUrl,"cid"=>$category_id,"sid"=>SERVICEID);
				$aParameters = Array("title"=>$id,"turl"=>$sRequestUrl,"cid"=>$comment_type_id,"sid"=>$service_id);
                $comment_result = $this->oCampusDiscussion->getMBDetails($aParameters);
				//print "<pre>"; print_r($comment_result);
				$comment_count = $comment_result['reply_cnt'];
			}
			$result = Array("iTId"=>$iTId,"comment_count"=>$comment_count);
			//print "<pre>"; print_r($result); //die();
			return $result;
		}

		function arrGetCommentCount($id="",$service_id="",$comment_type_id="",$category_id=""){
			if(!empty($category_id)){
				$whereClauseArr[] = " category_id = $category_id";
			}
			if(!empty($service_id)){
				$whereClauseArr[] = " service_id = $service_id";
			}
			if(!empty($id)){
				$whereClauseArr[] = " id = $id";
			}
			if(!empty($comment_type_id)){
				$whereClauseArr[] = " comment_type_id = $comment_type_id";
			}
			if(!empty($id) && !empty($comment_type_id) && !empty($service_id)){
				$md5id = md5($id.$comment_type_id.$service_id);
				$whereClauseArr[] = " md5id = '$md5id'";
			}
			if(sizeof($whereClauseArr) > 0){
				$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
			}
			$sql = "select * from COMMENT_REPORT $whereClauseStr order by category_id asc";
			$result = $this->select($sql);
			return $result;
		}
		
		function arrGetArticleCommentCount($article_id){
			$result = $this->arrGetCommentCount($article_id,SERVICEID,ARTICLE_CATEGORYID,SITE_CATEGORY_ID);
			return $result;
		}

		function arrGetNewsCommentCount($news_id){
			$result = $this->arrGetCommentCount($news_id,SERVICEID,NEWS_CATEGORYID,SITE_CATEGORY_ID);
			return $result;
		}

		function arrGetOnCarsReviewCommentCount($review_id){
			$result = $this->arrGetCommentCount($review_id,SERVICEID,ONCARS_REVIEW_CATEGORYID,SITE_CATEGORY_ID);
			return $result;
		}

		function arrGetUserReviewCommentCount($user_review_id){
			$result = $this->arrGetCommentCount($user_review_id,SERVICEID,USER_REVIEW_MODEL_CATEGORY_ID,SITE_CATEGORY_ID);
			return $result;
		}

		function arrGetOncarsUserReviewCommentCount($user_review_id){
			$result = $this->arrGetCommentCount($user_review_id,SERVICEID,USER_REVIEW_VARIANT_CATEGORY_ID,SITE_CATEGORY_ID);
			return $result;
		}

		
		function arrGetVideoCommentCount($video_id){
			$result = $this->arrGetCommentCount($video_id,SERVICEID,VIDEO_CATEGORYID,SITE_CATEGORY_ID);
			return $result;
		}
		
		function arrGetVideoReviewCommentCount($video_id){
                     $result = $this->arrGetCommentCount($video_id,SERVICEID,VIDEO_REVIEW_CATEGORYID,SITE_CATEGORY_ID);
			return $result;
		}
		
		function arrGetVideoArticleCommentCount($video_id){
			$result = $this->arrGetCommentCount($video_id,SERVICEID,VIDEO_ARTICLE_CATEGORYID,SITE_CATEGORY_ID);
			return $result;
		}
		
		function arrGetVideoNewsCommentCount($video_id){
			$result = $this->arrGetCommentCount($video_id,SERVICEID,VIDEO_NEWS_CATEGORYID,SITE_CATEGORY_ID);
			return $result;
		}

		function arrGetSlideShowCommentCount($slideshow_id){
			$result = $this->arrGetCommentCount($slideshow_id,SERVICEID,SLIDESHOW_CATEGORYID,SITE_CATEGORY_ID);
			return $result;
		}

		function UpdateCommentCount($array_param){
				$iTId = $array_param['iTId'];
				$id = $array_param['id'];
				$comment_type_id = $array_param['cid'];
				$iServiceId = $array_param['sid'];
				
				$aParameters = Array("title"=>$id,"cid"=>$comment_type_id,"sid"=>$iServiceId);
				$comment_result = $this->oCampusDiscussion->getMBDetails($aParameters);
				$comment_count = $comment_result['reply_cnt'];
							
				$insert_param = Array("id"=>$id,"category_id"=>SITE_CATEGORY_ID,"comment_type_id"=>$comment_type_id,"service_id"=>$iServiceId,"md5id"=>md5(implode("",$aParameters)),"comment_board_id"=>$iTId,"comment_count"=>$comment_count,"create_date"=>date('Y-m-d H:i:s'),"update_date"=>date('Y-m-d H:i:s'));
				
				$sSql = $this->getUpdateSql("COMMENT_REPORT",array_keys($insert_param),array_values($insert_param),"comment_board_id",$iTId);	
				$count_result = $this->update($sSql);
			
				//print_r($count_result); echo "<br>";
		}

	
		function intInsertUpdateViewsCount($request_param){
				////$views_board_id = $result['views_board_id'];
				//error_log("SQLLOG---");
				$item_id = $request_param["item_id"];
				$service_id = $request_param['sid'];
				$views_type_id = $request_param['cid'];
				$category_id = SITE_CATEGORY_ID;
				$aParameters = Array("item_id"=>$item_id,"cid"=>$views_type_id,"sid"=>$service_id);
				//print_r($aParameters);
				$insert_param = Array("item_id"=>$item_id,"category_id"=>SITE_CATEGORY_ID,"views_type_id"=>$views_type_id,"service_id"=>$service_id,"md5id"=>md5(implode("",$aParameters)),"create_date"=>date('Y-m-d H:i:s'),"update_date"=>date('Y-m-d H:i:s'));
				
				//$insert_param = Array("item_id"=>$item_id,"category_id"=>SITE_CATEGORY_ID,"views_type_id"=>$views_type_id,"service_id"=>$service_id,"md5id"=>md5(implode("",$aParameters)),"views_board_id"=>$views_board_id,"views_count"=>$views_count,"create_date"=>date('Y-m-d H:i:s'),"update_date"=>date('Y-m-d H:i:s'));
				$md5id = md5(implode("",$aParameters));
				$sql = "select views_count from VIEWS_REPORT where md5id ='".$md5id."' and category_id = $category_id";
				//error_log("SQLLOG---".$sql);
				$result = $this->select($sql);
				//print_r($result);
				#echo "----SELECT---<br>";
				if($result[0]['views_count'] > 0){
					$view_count = $result[0]['views_count']+1;
				}else{$view_count = 1;}
				$insert_param["views_count"] = $view_count;
				//print_r($insert_param);
			    $sSql = $this->getInsertUpdateSql("VIEWS_REPORT",array_keys($insert_param),array_values($insert_param));
				//error_log("SQLLOG11111---".$sSql);
				$count_result = $this->insertUpdate($sSql);
				return $view_count;
		}

		function arrGetViewsCount($item_id="",$service_id="",$views_type_id="",$category_id=""){
			if(!empty($category_id)){
				$whereClauseArr[] = " category_id = $category_id";
			}
			if(!empty($service_id)){
				$whereClauseArr[] = " service_id = $service_id";
			}
			if(!empty($item_id)){
				$whereClauseArr[] = " item_id = $item_id";
			}
			if(!empty($comment_type_id)){
				$whereClauseArr[] = " views_type_id = $views_type_id";
			}
			if(!empty($item_id) && !empty($views_type_id) && !empty($service_id)){
				$md5id = md5($item_id.$views_type_id.$service_id);
				$whereClauseArr[] = " md5id = '$md5id'";
			}
			if(sizeof($whereClauseArr) > 0){
				$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
			}
			$sql = "select * from VIEWS_REPORT $whereClauseStr order by category_id asc";
			//echo $sql;
			$result = $this->select($sql);
			return $result;
		}

		function arrGetArticleViewsCount($article_id){
			$result = $this->arrGetViewsCount($article_id,SERVICEID,ARTICLE_CATEGORYID,SITE_CATEGORY_ID);
			return $result;
		}

		function arrGetNewsViewsCount($news_id){
			$result = $this->arrGetViewsCount($news_id,SERVICEID,NEWS_CATEGORYID,SITE_CATEGORY_ID);
			return $result;
		}

		function arrGetOnCarsReviewViewsCount($review_id){
			$result = $this->arrGetViewsCount($review_id,SERVICEID,ONCARS_REVIEW_CATEGORYID,SITE_CATEGORY_ID);
			return $result;
		}

		function arrGetUserReviewViewsCount($user_review_id){
			$result = $this->arrGetViewsCount($user_review_id,SERVICEID,USER_REVIEW_MODEL_CATEGORY_ID,SITE_CATEGORY_ID);
			return $result;
		}

		function arrGetOncarsUserReviewViewsCount($user_review_id){
			$result = $this->arrGetViewsCount($user_review_id,SERVICEID,USER_REVIEW_MODEL_CATEGORY_ID,SITE_CATEGORY_ID);
			return $result;
		}

		
		function arrGetVideoViewsCount($video_id){
			$result = $this->arrGetViewsCount($video_id,SERVICEID,VIDEO_CATEGORYID,SITE_CATEGORY_ID);
			return $result;
		}
		
		function arrGetVideoReviewViewsCount($video_id){
			$result = $this->arrGetViewsCount($video_id,SERVICEID,VIDEO_REVIEW_CATEGORYID,SITE_CATEGORY_ID);
			return $result;
		}
		
		function arrGetVideoArticleViewsCount($video_id){
			$result = $this->arrGetViewsCount($video_id,SERVICEID,VIDEO_ARTICLE_CATEGORYID,SITE_CATEGORY_ID);
			return $result;
		}
		
		function arrGetVideoNewsViewsCount($video_id){
			$result = $this->arrGetViewsCount($video_id,SERVICEID,VIDEO_NEWS_CATEGORYID,SITE_CATEGORY_ID);
			return $result;
		}

		function arrGetSlideShowViewsCount($slideshow_id){
			$result = $this->arrGetViewsCount($slideshow_id,SERVICEID,SLIDESHOW_CATEGORYID,SITE_CATEGORY_ID);
			return $result;
		}


		function getPageViews($aViewCntUrl,$aEncViewCntUrl=""){
                //echo "aViewCntUrl" ; print_r($aViewCntUrl); 
			if(count($aViewCntUrl)>0){
				require_once(CLASSPATH.'utility.class.php');
				require_once(CLASSPATH.'xmlparser.class.php');
				$oXmlparser = new XMLParser;
				$sUrlList=implode(",",array_keys($aViewCntUrl));
                          
				$sContent = "action=get&vurl=$sUrlList&sid=".SERVICEID;
				//echo "<br>=====================<br>".$sContent;

				$sPageCountXML =utility::curlaccess($sContent,VIEW_TRACKER_API_PATH);

				$oXmlparser->XMLParse($sPageCountXML);
				$aResultXML =$oXmlparser->getOutput();
				$oXmlparser->clearOutput();
				//print "<pre>";print_r($aResultXML); echo "TEST";
			

				$iViews=0;
				if(is_array($aResultXML) && count($aResultXML)>0){
                               foreach($aResultXML['response'] as $iK =>$aData){
                                      $aViewCnt[$aEncViewCntUrl[trim($aData['view_url'])]]=$aData['view_count'];   
					}
					//echo "dsadasdasd";
					//print_r($aViewCnt);
				}
				
			}
			
			//die();
			return $aViewCnt;
		}
		/**
		* @note function is used to get date for solar article search.
		* @author Manish Tabkade.
		* @createdate 07-07-2011
		* @param integer $category_id
		* @param integer $startlimit.
		* @param integer $cnt.
		* @param string date format $startdate.
		* @param string date format $enddate.
		* @param boolean $status.
		* @pre $category_id must be non-empty/non-zero valid integer.
		* @post array article details.
		* return array.	
		*/
		function arrSolarArticleDetails($category_id="",$startlimit="",$cnt="",$startdate="",$enddate="",$status="1"){
			if(!empty($category_id)){
				$whereClauseArr[] = " PA.category_id = $category_id";
			}
			if($status != ''){
				$whereClauseArr[] = "A.status = $status";
			}
			if(!empty($startdate)){
				$startdate = $startdate.' 00:00:00';
				$whereClauseArr[] = "A.create_date >= '$startdate'";
			}
			if(!empty($enddate)){
				$enddate = $enddate.' 23:23:59';
				$whereClauseArr[] = "A.create_date <= '$enddate'";
			}
			if(!empty($startlimit)){
				$limitArr[] = $startlimit;
			}
			if(!empty($cnt)){
				$limitArr[] = $cnt;
			}
			$whereClauseArr[] = " PA.editor_id=A.uid ";
			if(sizeof($whereClauseArr) > 0){
				$whereClauseStr = ' where '.implode(" and ",$whereClauseArr);
			}
			if(sizeof($limitArr) > 0){
				$limitStr = ' limit '.implode(",",$limitArr);
			}
		    $sql = "select * from EDITOR_INFO PA, ARTICLE A $whereClauseStr  $limitStr";
		    //echo $sql;exit;
			$result = $this->select($sql);
			return $result;
		}
		/**
                * @note function is used to get date for solar news search.
                * @author Manish.
                * @createdate 07-07-2011
                * @param integer $category_id
                * @param integer $startlimit.
                * @param integer $cnt.
                * @param string date format $startdate.
                * @param string date format $enddate.
                * @param boolean $status.
                * @pre $category_id must be non-empty/non-zero valid integer.
                * @post array article details.
                * return array.
                */
		function arrSolarNewsDetails($category_id="",$startlimit="",$cnt="",$startdate="",$enddate="",$status="1"){
              
				  if(!empty($category_id)){
						$whereClauseArr[] = " category_id = $category_id";
					}
					if($status != ''){
						$whereClauseArr[] = "status = $status";
					}
					if(!empty($startdate)){
						$startdate = $startdate.' 00:00:00';
						$whereClauseArr[] = "create_date >= '$startdate'";
					}
					if(!empty($enddate)){
						$enddate = $enddate.' 23:23:59';
						$whereClauseArr[] = "create_date <= '$enddate'";
					}
					if(!empty($startlimit)){
						$limitArr[] = $startlimit;
					}
					if(!empty($cnt)){
						$limitArr[] = $cnt;
					}
					
					if(sizeof($whereClauseArr) > 0){
						$whereClauseStr = ' where '.implode(" and ",$whereClauseArr);
					}
					if(sizeof($limitArr) > 0){
						$limitStr = ' limit '.implode(",",$limitArr);
					}
					$sql = "select * from  NEWS  $whereClauseStr  $limitStr";
					//echo $sql;exit;
					$result = $this->select($sql);
					return $result;

                }
		/**
                * @note function is used to get date for solar reviews search.
                * @author Manish.
                * @createdate 07-07-2011
                * @param integer $category_id
                * @param integer $startlimit.
                * @param integer $cnt.
                * @param string date format $startdate.
                * @param string date format $enddate.
                * @param boolean $status.
                * @pre $category_id must be non-empty/non-zero valid integer.
                * @post array article details.
                * return array.
                */
		function arrSolarReviewsDetails($category_id="",$startlimit="",$cnt="",$startdate="",$enddate="",$status="1"){
			 
			   if(!empty($category_id)){
					$whereClauseArr[] = " PR.category_id = $category_id";
				}
				if($status != ''){
					$whereClauseArr[] = "R.status = $status";
				}
				if(!empty($startdate)){
					$startdate = $startdate.' 00:00:00';
					$whereClauseArr[] = "R.create_date >= '$startdate'";
				}
				if(!empty($enddate)){
					$enddate = $enddate.' 23:23:59';
					$whereClauseArr[] = "R.create_date <= '$enddate'";
				}
				if(!empty($startlimit)){
					$limitArr[] = $startlimit;
				}
				if(!empty($cnt)){
					$limitArr[] = $cnt;
				}
				$whereClauseArr[] = " EI.editor_id=R.uid ";
				$whereClauseArr[] = " PR.review_id=R.review_id";
				 
				if(sizeof($whereClauseArr) > 0){
					$whereClauseStr = ' where '.implode(" and ",$whereClauseArr);
				}
				if(sizeof($limitArr) > 0){
					$limitStr = ' limit '.implode(",",$limitArr);
				}
		    $sql = "select * from EDITOR_INFO EI, REVIEWS R, PRODUCT_REVIEWS PR $whereClauseStr  $limitStr";
		    //echo $sql;exit;
			$result = $this->select($sql);
			return $result;
      }
		/**
                * @note function is used to get date for solar video search.
                * @author Manish.
                * @createdate 07-07-2011
                * @param integer $category_id
                * @param integer $startlimit.
                * @param integer $cnt.
                * @param string date format $startdate.
                * @param string date format $enddate.
                * @param boolean $status.
                * @pre $category_id must be non-empty/non-zero valid integer.
                * @post array article details.
                * return array.
                */
		function arrSolarVideoDetails($category_id="",$startlimit="",$cnt="",$startdate="",$enddate="",$status="1"){

	    		if(!empty($category_id)){
		    		$whereClauseArr[] = "PV.category_id = $category_id";
				}

				if($status != ''){
				$whereClauseArr[] = "V.status=$status";
				}
				
				
				$whereClauseArr[] = " PV.video_id=V.video_id ";
				$whereClauseArr[] = " V.content_type=1 ";

				if($status != ''){
					$whereClauseArr[] = "status = $status";
				}
				if(!empty($startdate)){
					$startdate = $startdate.' 00:00:00';
					$whereClauseArr[] = "create_date >= '$startdate'";
				}
				if(!empty($enddate)){
					$enddate = $enddate.' 23:23:59';
					$whereClauseArr[] = "create_date <= '$enddate'";
				}
				if(!empty($startlimit)){
					$limitArr[] = $startlimit;
				}
				if(!empty($cnt)){
					$limitArr[] = $cnt;
				}

               $whereClauseArr[] = "content_type=1";
			   $whereClauseArr[] = "is_media_process=1";
               if(sizeof($whereClauseArr) > 0){
					$whereClauseStr = ' where '.implode(" and ",$whereClauseArr);
			   }
			   if(sizeof($limitArr) > 0){
					$limitStr = ' limit '.implode(",",$limitArr);
				}
				$sql = "select * from VIDEO_GALLERY V, PRODUCT_VIDEOS PV $whereClauseStr $limitStr";
				//echo $sql;exit;
				$result = $this->select($sql);
				return $result;


                }
		/**
                * @note function is used to get date for solar car search.
                * @author Manish.
                * @createdate 07-07-2011
                * @param integer $category_id
                * @param integer $startlimit.
                * @param integer $cnt.
                * @param string date format $startdate.
                * @param string date format $enddate.
                * @param boolean $status.
                * @pre $category_id must be non-empty/non-zero valid integer.
                * @post array article details.
                * return array.
                */
		function arrSolarCarDetails($category_id,$startlimit="",$cnt="",$startdate="",$enddate="",$status="1"){
		}
	}
?>
