<?php
/**
 * @brief class feature management.This class is use to get,update,insert feature
 * management data.
 * @author Rajesh Ujade.
 * @version 1.0
 * @created 11-Nov-2010 4:06:02 PM
 */
class FeatureManagement extends DbOperation
{

	 /**Initialize the consturctor.*/
	var $featurekey;
	var $cache;
	function FeatureManagement()
	{
		$this->cache = new Cache;
		$this->featurekey = MEMCACHE_MASTER_KEY.'feature';
	}

	/**
	 * @note function is used to insert the feature into the database.
	 * @param an associative array $insert_param.
	 * @pre $insert_param must be valid associative array.
	 * @post an integer $feature_id.
	 * retun integer.
	 */
	function intInsertFeature($insert_param)
	{
		$result = $this->intMaxFeatureDisplayOrder();
		$insert_param['feature_display_order'] = $result[0]['feature_display_order'];
		$insert_param['create_date'] = date('Y-m-d H:i:s');
		$insert_param['update_date'] = date('Y-m-d H:i:s');
		$sql = $this->getInsertSql("FEATURE_MASTER",array_keys($insert_param),array_values($insert_param));
		$feature_id = $this->insert($sql);
		if($feature_id == 'Duplicate entry'){ return 'exists';}
		$this->cache->searchDeleteKeys($this->featurekey);
		$this->arrGetFeatureDetails($feature_id);
		return $feature_id;
	}

	/**
	 * @note function is used to update the feature into the database.
	 * @param an associative array $update_param.
	 * @param an integer $feature_id.
	 * @pre $update_param must be valid associative array and $feature_id must be non empty/zero valid integer.
	 * @post boolean true/false.
	 * retun boolean.
	 */
	 function boolUpdateFeature($feature_id,$update_param)
	 {
		$update_param['update_date'] = date('Y-m-d H:i:s');
		$sql = $this->getUpdateSql("FEATURE_MASTER",array_keys($update_param),array_values($update_param),"feature_id",$feature_id);
		$isUpdate = $this->update($sql);
		$this->cache->searchDeleteKeys($this->featurekey);
		$this->arrGetFeatureDetails($feature_id);
		return $isUpdate;
	 }
	 /**
	 * @note function is used to get feature details.
	 * @param an integer/comma seperated feature ids/feature ids array $feature_ids.
	 * @param an integer category_id.
	 * @param boolean Active/InActive $status.
	 * @param integer $startlimit.
	 * @param integer $count.
	 * @pre not required.
	 * @post feature details in associative array.
	 * retun an array.
	 */
	 function arrGetFeatureDetails($feature_ids="",$category_id="",$main_group_id="",$sub_group_id="",$status="1",$startlimit="",$count="",$feature_name="")
	 {
		$keyArr[] = $this->featurekey;
	 	if(is_array($feature_ids)){
	 		$feature_ids = implode(",",$feature_ids);
			
	 	}
		if(is_array($category_id)){
			$category_id = implode(",",$category_id);
		}
		if(is_array($main_group_id)){
			$main_group_id = implode(",",$main_group_id);
		}
		if(is_array($sub_group_id)){
			$sub_group_id = implode(",",$sub_group_id);
		}
		if(!empty($feature_name)){
			$keyArr[] = "feature_name_$feature_name";
	 		$whereClauseArr[] = "lower(feature_name)= '$feature_name'";
	 	}
	 	if($status != '')
	 	{
			$keyArr[] = "status_$status";
	 		$whereClauseArr[] = "status=$status";
	 	}
	 	if(!empty($category_id)){
			$keyArr[] = "category_id_$category_id";
	 		$whereClauseArr[] = "category_id in ($category_id)";
	 	}
	 	if(!empty($feature_ids)){
			$keyArr[] = "feature_id_$feature_ids";
	 		$whereClauseArr[] = "feature_id in ($feature_ids)";
	 	}
		if(!empty($main_group_id)){
			$keyArr[] = "main_feature_group_$main_group_id";
			$whereClauseArr[] = "main_feature_group in ($main_group_id)";
		}
		if(!empty($sub_group_id)){
			$keyArr[] = "feature_sub_group_$sub_group_id";
			$whereClauseArr[] = "feature_group in ($sub_group_id)";
		}
	 	if(sizeof($whereClauseArr) > 0){
	 		$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
	 	}
	 	if(!empty($startlimit)){
			$keyArr[] = "startlimit_$startlimit";
	 		$limitArr[] = $startlimit;
	 	}
	 	if(!empty($count)){
			$keyArr[] = "count_$count";
	 		$limitArr[] = $count;
	 	}
	 	if(sizeof($limitArr) > 0){
	 		$limitStr = " limit ".implode(" , ",$limitArr);
	 	}
		$key = implode("_",$keyArr);			
		if($result = $this->cache->get($key)){return $result;}
	 	$sql = "select * from FEATURE_MASTER $whereClauseStr order by feature_display_order asc $limitStr" ;
	 	$result = $this->select($sql);
		$this->cache->set($key,$result);
	 	return $result;
	 }

	/**
	 * @note function is used to get product feature details.
	 * @param an integer/comma seperated product ids/product ids array $product_ids.
	 * @param boolean Active/InActive $status.
	 * @param integer $startlimit.
	 * @param integer $count.
	 * @pre $product_ids must be non-empty valid an integer/comma seperated product ids/product ids array.
	 * @post product details in associative array.
	 * retun an array.
	 */
	 function arrGetProductFeatureDetails($product_ids,$status=1,$startlimit="",$count="")
	 {
		$keyArr[] = $this->featurekey."_product";
	 	if(is_array($product_ids)){
	 		$product_ids = implode(",",$product_ids);
	 	}

	 	$whereClauseArr[] = "FEATURE_MASTER.feature_id=PRODUCT_FEATURE.feature_id";
	 	if($status != '')
	 	{
			$keyArr[] = "status_$status";
	 		$whereClauseArr[] = "FEATURE_MASTER.status=$status";
	 	}
	 	if(!empty($product_ids)){
			$keyArr[] = "product_id_$product_ids";
	 		$whereClauseArr[] = "PRODUCT_FEATURE.product_id in($product_ids)";
	 	}
	 	if(sizeof($whereClauseArr) > 0){
	 		$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
	 	}
	 	if(!empty($startlimit)){
			$keyArr[] = "startlimit_$startlimit";
	 		$limitArr[] = $startlimit;
	 	}
	 	if(!empty($count)){
			$keyArr[] = "count_$count";
	 		$limitArr[] = $count;
	 	}
	 	if(sizeof($limitArr) > 0){
	 		$limitStr = " limit ".implode(" , ",$limitArr);
	 	}
		$key = implode("_",$keyArr);			
		if($result = $this->cache->get($key)){return $result;}

	 	$sql = "select PRODUCT_FEATURE.*,FEATURE_MASTER.* from PRODUCT_FEATURE,FEATURE_MASTER $whereClauseStr $limitStr group by FEATURE_MASTER.feature_group order by FEATURE_MASTER.feature_name asc";
	 	$result = $this->select($sql);
		$this->cache->set($key,$result);
		echo "<pre> product feature - ";print_r($result);
	 	return $result;
	 }

	 /**
	 * @note function is used to delete the feature.
	 * @param integer $feature_id.
	 * @pre $feature_id must be non-empty/zero valid integer.
	 * @post boolean true/false.
	 * return boolean.
	 */
	 function boolDeleteFeature($feature_id)
	 {
	 	$sql = "delete from FEATURE_MASTER where feature_id = $feature_id";
	 	$isDelete = $this->sql_delete_data($sql);
		$this->cache->searchDeleteKeys($this->featurekey);
	 	return $isDelete;
	 }

	 /**
	 * @note function is used to insert feature unit.
	 * @param an associate array $insert_param.
	 * @pre $insert_param must be valid associative array.
	 * @post integer $unit_id.
	 * return integer.
	 */
	 function intInsertFeatureUnit($insert_param){
		$insert_param['create_date'] = date('Y-m-d H:i:s');
		$sql = $this->getInsertSql("FEATURE_UNIT",array_keys($insert_param),array_values($insert_param));
		$unit_id = $this->insert($sql);
		if($unit_id == 'Duplicate entry'){ return 'exists';}
		$this->cache->searchDeleteKeys($this->featurekey."_unit");
		$this->arrFeatureUnitDetails($unit_id);
		return $unit_id;
	 }
	 /**
	 * @note function is used to update the feature unit into the database.
	 * @param an associative array $update_param.
	 * @param an integer $unit_id.
	 * @pre $update_param must be valid associative array and $unit_id must be non-empty/zero valid integer.
	 * @post boolean true/false.
	 * retun boolean.
	 */
	 function boolUpdateFeatureUnit($unit_id,$update_param)
	 {
		$update_param['create_date'] = date('Y-m-d H:i:s');
		$sql = $this->getUpdateSql("FEATURE_UNIT",array_keys($update_param),array_values($update_param),"unit_id",$unit_id);
		$isUpdate = $this->update($sql);
		$this->cache->searchDeleteKeys($this->featurekey."_unit");
		$this->arrFeatureUnitDetails($unit_id);
	 	return $isUpdate;
	 }

	 /**
	 * @note function is used to delete the feature.
	 * @param integer $unit_id.
	 * @pre $unit_id must be non-empty/zero valid integer.
	 * @post boolean true/false.
	 * return boolean.
	 */
	 function boolDeleteFeatureUnit($unit_id)
	 {
	 	$sql = "delete from FEATURE_UNIT where unit_id = $unit_id";
	 	$isDelete = $this->sql_delete_data($sql);
		$this->cache->searchDeleteKeys($this->featurekey."_unit");
	 	return $isDelete;
	 }

	 /**
	 * @note function is used to feature unit details.
	 * @param integer $feature_id.
	 * @param integer $unit_id.
	 * @param integer $startlimit.
	 * @param integer $count.
	 * @pre not required.
	 * @post an associative array of feature unit.
	 * return an array.
	 */
	 function arrFeatureUnitDetails($unit_id="",$category_id="",$status="1",$startlimit="",$count="")
	 {
		$keyArr[] = $this->featurekey."_unit";
	 	if(!empty($unit_id)){
			$keyArr[] = "unit_id_$unit_id";
	 		$whereClauseArr[] = "unit_id in($unit_id)";
	 	}
		if(!empty($category_id)){
			$keyArr[] = "category_id_$category_id";
	 		$whereClauseArr[] = "category_id in($category_id)";
	 	}
		if(!empty($status)){
			$keyArr[] = "status_$status";
			$whereClauseArr[] = "status = $status";
		}
	 	if(sizeof($whereClauseArr) > 0){
	 		$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
	 	}
	 	if(!empty($startlimit)){
			$keyArr[] = "startlimit_$startlimit";
	 		$limitArr[] = $startlimit;
	 	}
	 	if(!empty($count)){
			$keyArr[] = "count_$count";
	 		$limitArr[] = $count;
	 	}
	 	if(sizeof($limitArr) > 0){
	 		$limitStr = " limit ".implode(" , ",$limitArr);
	 	}
		$key = implode("_",$keyArr);			
		if($result = $this->cache->get($key)){return $result;}
	 	$sql = "select * from FEATURE_UNIT $whereClauseStr order by unit_name asc $limitStr";
	 	$result = $this->select($sql);
		$this->cache->set($key,$result);
		return $result;
	 }
	/**
	* @note function is used to get unique feature group.
	* @pre not required.
	* @post an array of feature group.
	* return array.
	*/
	function arrGetFeatureGroup(){
		$key = $this->featurekey."_group";
		if($result = $this->cache->get($key)){return $result;}
		$sql = "select distinct(feature_group) as feature_group from FEATURE_MASTER where feature_group != '' and feature_group != 'NULL' order by feature_group asc";
		$result = $this->select($sql);
		$this->cache->set($key,$result);
		return $result;
	}
	/**
	* @note function is used to get max pivot display order id.
	* @param integer $category_id.
	* @param integer $feature_id.
	* @param integer $pivot_id.
	* @pre $category_id and $feature_id must be valid,non-empty,non-zero integer.
	* @post integer max display order.
	* return integer.
	*/
	function intMaxFeatureDisplayOrder($category_id="",$feature_id=""){
		$keyArr[] = $this->featurekey."_display_order";
		if(!empty($category_id)){
			$keyArr[] = "category_id_$category_id";
			$whereClauseArr[] = "category_id = $category_id";
		}
		if(!empty($feature_id)){
			$keyArr[] = "feature_id_$feature_id";
			$whereClauseArr[] = "feature_id = $feature_id";
		}
		
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
		}
		$key = implode("_",$keyArr);			
		if($result = $this->cache->get($key)){return $result;}
		$sql = "select max(feature_display_order) as feature_display_order from FEATURE_MASTER $whereClauseStr";
		$result = $this->select($sql);
		$this->cache->set($key,$result);
		return $result;
	}
	/**
	* @note function is used to insert main feature group into the db.
	* @param associative array $insert_param.
	* @pre $insert_param must be valid,non-empty associative array.
	* @post integer feature group id.
	* return integer.
	*/
	function insertFeatureMainGroup($insert_param){
		$insert_param['create_date'] = date('Y-m-d H:i:s');
        $sql = $this->getInsertSql("MAIN_FEATURE_GROUP",array_keys($insert_param),array_values($insert_param));
        $feature_group_id = $this->insert($sql);
        if($feature_group_id == 'Duplicate entry'){ return 'exists';}
	$this->cache->searchDeleteKeys($this->featurekey."_main_group");
	$this->arrGetFeatureMainGroupDetails($feature_group_id);
        return $feature_group_id;
	}
	 /**
	 * @note function is used to update the feature main group into the database.
	 * @param an associative array $update_param.
	 * @param an integer $feature_group_id.
	 * @pre $update_param must be valid associative array and $feature_group_id must be non-empty/zero valid integer.
	 * @post boolean true/false.
	 * retun boolean.
	 */
	 function boolUpdateFeatureMainGroup($feature_group_id,$update_param)
	 {
			$update_param['create_date'] = date('Y-m-d H:i:s');
			$sql = $this->getUpdateSql("MAIN_FEATURE_GROUP",array_keys($update_param),array_values($update_param),"group_id",$feature_group_id);
			$isUpdate = $this->update($sql);
			$this->cache->searchDeleteKeys($this->featurekey."_main_group");
			$this->arrGetFeatureMainGroupDetails($feature_group_id);
			return $isUpdate;
	 }

	 /**
	 * @note function is used to delete the main feature group.
	 * @param integer $feature_group_id.
	 * @pre $feature_group_id must be non-empty/zero valid integer.
	 * @post boolean true/false.
	 * return boolean.
	 */
	 function boolDeleteFeatureMainGroup($feature_group_id)
	 {
		$sql = "delete from MAIN_FEATURE_GROUP where group_id = $feature_group_id";
		$isDelete = $this->sql_delete_data($sql);
		$this->cache->searchDeleteKeys($this->featurekey."_main_group");
		return $isDelete;
	 }
	/**
	* @note function is used to get mail feature group.
	* @param integer $feature_group_id.
	* @param integer $category_id.
	* @param boolean $status.
	* @param integer $startlimit.
	* @param integer $cnt.
	* @pre not required.
	* @post associative array of feature group details
	* return array.
	*/
	function arrGetFeatureMainGroupDetails($feature_group_id="",$category_id="",$status="1",$startlimit="",$cnt=""){
		$keyArr[] = $this->featurekey."_main_group";
		if(is_array($feature_group_id)){
			$feature_group_id = implode(",",$feature_group_id);
		}
		if(is_array($category_id)){
			$category_id = implode(",",$category_id);
		}
		if($status != ''){	
			$keyArr[] = "status_$status";
			$whereClauseArr[] = "status = $status";
		}
		if(!empty($feature_group_id)){
			$keyArr[] = "group_id_$feature_group_id";
			$whereClauseArr[] = "group_id in ($feature_group_id)";
		}
		if(!empty($category_id)){
			$keyArr[] = "category_id_$category_id";
			$whereClauseArr[] = "category_id in ($category_id)";
		}
		if(!empty($startlimit)){
			$keyArr[] = "startlimit_$startlimit";
			$limitArr[] = $startlimit;
		}
		if(!empty($cnt)){
			$keyArr[] = "cnt_$cnt";
			$limitArr[] = $cnt;
		}
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
		}
		if(sizeof($limitArr) > 0){
			$limitStr = " limit ".implode(",",$limitArr);
		}
		$key = implode("_",$keyArr);			
		if($result = $this->cache->get($key)){return $result;}
		$sql = "select * from MAIN_FEATURE_GROUP $whereClauseStr order by position ASC $limitStr ";
		$result = $this->select($sql);
		$this->cache->set($key,$result);
		return $result;
	}
	/**
	 * @note function is used to insert the feature sub group details into the database.
	 * @param an associative array $insert_param.
	 * @pre $insert_param must be valid associative array.
	 * @post an integer $feature_group_id.
	 * retun integer.
	 */
	function intInsertFeatureSubGroupDetails($insert_param){
		$insert_param['create_date'] = date('Y-m-d H:i:s');
	        $sql = $this->getInsertSql("FEATURE_SUB_GROUP",array_keys($insert_param),array_values($insert_param));		
        	$feature_group_id = $this->insert($sql);
	        if($feature_group_id == 'Duplicate entry'){ return 'exists';}
		$this->cache->searchDeleteKeys($this->featurekey."_sub_group");
		$this->arrFeatureSubGroupDetails($feature_group_id);
        	return $feature_group_id;
	}
	/**
	 * @note function is used to update the feature sub group details into the database.
	 * @param an associative array $update_param.
	 * @param an integer $sub_group_id.
	 * @pre $update_param must be valid associative array and $sub_group_id must be non empty/zero valid integer.
	 * @post boolean true/false.
	 * retun boolean.
	 */
	function boolUpdateFeatureSubGroupDetails($sub_group_id,$update_param){
		$update_param['create_date'] = date('Y-m-d H:i:s');
        	$sql = $this->getUpdateSql("FEATURE_SUB_GROUP",array_keys($update_param),array_values($update_param),"sub_group_id",$sub_group_id);
	        $isUpdate = $this->update($sql);
		$this->cache->searchDeleteKeys($this->featurekey."_sub_group");
		$this->arrFeatureSubGroupDetails($sub_group_id);
        	return $isUpdate;
	}
	/**
	 * @note function is used to delete the feature sub group details.
	 * @param integer $sub_group_id.
	 * @pre $sub_group_id must be non-empty/zero valid integer.
	 * @post boolean true/false.
	 * return boolean.
	 */
	function boolDeleteFeatureSubGroupDetails($sub_group_id){
		$sql = "delete from FEATURE_SUB_GROUP where sub_group_id = $sub_group_id";
	        $isDelete = $this->sql_delete_data($sql);
		$this->cache->searchDeleteKeys($this->featurekey."_sub_group");
	        return $isDelete;
	}
	/**
	 * @note function is used to get feature sub group details 
	 *
	 * @param an integer $sub_group_id.
	 * @param an integer $category_id.
	 * @param boolean Active/InActive $status.
	 * @param integer $startlimit.
	 * @param integer $cnt.
	 *
	 * @pre not required.
	 *
	 * @post feature sub group details in associative array.
	 * retun an array.
	 */
	function arrFeatureSubGroupDetails($sub_group_id="",$category_id="",$status="1",$startlimit="",$cnt=""){
		$keyArr[] = $this->featurekey."_sub_group";
		if(!empty($sub_group_id)){
			$keyArr[] = "sub_group_id_$sub_group_id";
			$whereClauseArr[] = "sub_group_id = $sub_group_id";
		}
		if(!empty($category_id)){
			$keyArr[] = "category_id_$category_id";
			$whereClauseArr[] = "category_id = $category_id";
		}
		if($status != ''){
			$keyArr[] = "status_$status";
			$whereClauseArr[] = "status = $status";
		}
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
		}
		if(!empty($startlimit)){
			$limitArr[] = $startlimit;
		}
		if(!empty($cnt)){
			$limitArr[] = $cnt;
		}
		if(sizeof($limitArr) > 0){
			$limitStr = " limit ".implode(",",$limitArr);
		}
		$key = implode("_",$keyArr);			
		if($result = $this->cache->get($key)){return $result;}
		$sql = "select * from FEATURE_SUB_GROUP $whereClauseStr order by sub_group_position asc $limitStr ";
		$result = $this->select($sql);
		$this->cache->set($key,$result);
		return $result;
	}
	/**
	 * @note function is used to get feature sub group details 
	 *
	 * @param an integer $sub_group_id.
	 * @param an integer $main_group_id.
	 * @param an integer $category_id.
	 * @param boolean Active/InActive $status.
	 * @param integer $startlimit.
	 * @param integer $cnt.
	 *
	 * @pre not required.
	 *
	 * @post feature sub group details in associative array.
	 * retun an array.
	 */	
	function arrFetchFeatureSubGroupDetails($sub_group_id="",$main_group_id="",$category_id="",$status="1",$startlimit="",$cnt=""){
		$keyArr[] = $this->featurekey."_sub_group";
		if(!empty($sub_group_id)){
			$keyArr[] = "sub_group_id_$sub_group_id";
			$whereClauseArr[] = "sub_group_id = $sub_group_id";
		}
		if(!empty($main_group_id)){
			$keyArr[] = "main_group_id_$main_group_id";
			$whereClauseArr[] = "main_group_id = $main_group_id";
		}
		if(!empty($category_id)){
			$keyArr[] = "category_id_$category_id";
			$whereClauseArr[] = "category_id = $category_id";
		}
		if($status != ''){
			$keyArr[] = "status_$status";
			$whereClauseArr[] = "status = $status";
		}
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
		}
		if(!empty($startlimit)){
			$limitArr[] = $startlimit;
		}
		if(!empty($cnt)){
			$limitArr[] = $cnt;
		}
		if(sizeof($limitArr) > 0){
			$limitStr = " limit ".implode(",",$limitArr);
		}
		$key = implode("_",$keyArr);			
		if($result = $this->cache->get($key)){return $result;}
		$sql = "select * from FEATURE_SUB_GROUP $whereClauseStr order by sub_group_position asc $limitStr ";
		$result = $this->select($sql);
		$this->cache->set($key,$result);
		return $result;
	}
	/**
	 * @note function is used to get summary.
	 *
	 * @param an integer $category_id.
	 * @param an integer $product_id.
	 *
	 *
	 * @post xml string.
	 * retun string.
	 */
	function arrGetSummary($category_id,$product_id,$type="xml"){

		require_once(CLASSPATH.'price.class.php');
		require_once(CLASSPATH.'product.class.php');

		$product = new ProductManagement;
		$price = new price;
		
		$product_result = $product->arrGetProductDetails($product_id,$category_id,"","1","","","1","","","1");
		$product_name = $product_result[0]['product_name'];
		$variant = $product_result[0]['variant'];

		if(!empty($product_name)){
			$productArr[] = $product_name;
		}
		if(!empty($variant)){
			$productArr[] = $variant;
		}
		
		$product_name = implode(" ",$productArr);

		$all_variant_result = $price->arrGetVariantDetail();
		$cnt = sizeof($all_variant_result);
		$variantArr = Array();
		for($i=0;$i<$cnt;$i++){
			$variant = $all_variant_result[$i]['variant'];
			$variant_id = $all_variant_result[$i]['variant_id'];
			if(!stripos($variant,'showroom')){
				$variantArr[] = $variant;
			}
		}
		
		$exshowroom_price  = $product_result[0]['variant_value'];
		
		if(!empty($exshowroom_price)){
			$exshowroom_price = "Rs.".priceFormat($exshowroom_price);
			$extra_variant = " (".implode(",",$variantArr)." and other taxes/charges extra)";
		}
		$aFeatureData=array();
		$aFeatureData["$product_name Price"]["Avg Ex-Showroom Price"] = $exshowroom_price.$extra_variant;
		
		$sSql="SELECT * FROM FEATURE_OVERVIEW_MASTER FM, MAIN_FEATURE_GROUP SB WHERE FM.category_id = $category_id AND 	FM.overview_sub_group_id = SB.group_id order by FM.position asc";
		$key = $this->featurekey."_model_page_overview";
		$feature_result = $this->cache->get($key);

		if(empty($feature_result)){
			$feature_result = $this->select($sSql);	
			$this->cache->set($key,$feature_result);
		}
		
		foreach($feature_result as $ikey=>$aValueData){
			$feature_id = $aValueData['feature_id'];
			if($feature_id!=''){
				$sql = "select PRODUCT_FEATURE.*,FEATURE_MASTER.* from PRODUCT_FEATURE,FEATURE_MASTER where PRODUCT_FEATURE.feature_id = $feature_id and FEATURE_MASTER.feature_id = PRODUCT_FEATURE.feature_id and PRODUCT_FEATURE.product_id=$product_id";
				$key = $this->featurekey."_$feature_id"."_product_$product_id"."_model_page_summary";
				$product_feature = $this->cache->get($key);
				if(empty($product_feature)){
					$product_feature = $this->select($sql);
					$this->cache->set($key,$product_feature);
				}
				$feature_value = trim($product_feature[0]['feature_value']);
				$feature_name = trim($product_feature[0]['feature_name']);
				$abbreviation = $aValueData['abbreviation'];

				/*if(strtolower($feature_value) == 'yes'){
					$feature_value = $feature_name;
				}elseif(strtolower($feature_value) == 'no'){
					$feature_value = '';
				}*/
			}
			unset($featureWithUnitArr);
			if(!empty($feature_value)){
				$featureWithUnitArr[] = $feature_value;
			}
			if(!empty($abbreviation) && !empty($feature_value)){
				$featureWithUnitArr[] = $abbreviation;
			}
			$overview_display_name = $aValueData['overview_display_name'] ? $aValueData['overview_display_name'] : $aValueData['main_group_name'];
			
			if($aValueData['title']!=""){
				if(sizeof($featureWithUnitArr) > 0){
					$aFeatureData[$product_name." ".$overview_display_name][$aValueData['title']][]= implode(" ",$featureWithUnitArr);
				}
			}else{
				if(!empty($feature_value) && $feature_value != '-'){
					$titleArr[] = $feature_value;
				}
				if(!empty($abbreviation) && !empty($feature_value) && $feature_value != '-'){
					$titleArr[] = $abbreviation;
				}
				if(sizeof($titleArr) > 0){
					$aFeatureData[$product_name." ".$overview_display_name][0][] = implode("",$titleArr);
					$aFeatureData[$product_name." ".$overview_display_name]['main_group_id'] = $aValueData['overview_sub_group_id'];
					$aFeatureData[$product_name." ".$overview_display_name]['main_group_name'] = $overview_display_name;
				}
				unset($titleArr);
			}
		}
		if($type != 'xml'){
			return $aFeatureData;
		}
		
		if(is_array($aFeatureData)){
			$xml .= "<GROUP_MASTER>";
			foreach($aFeatureData as $group_name=>$groupArr){
				$main_group_id = $groupArr['main_group_id'];
				$main_group_name = $groupArr['main_group_name'];
				if(is_array($groupArr)){
					$xml .= "<GROUP_MASTER_DATA>";
					$xml .= "<MAIN_GROUP_ID><![CDATA[$main_group_id]]></MAIN_GROUP_ID>";
					$xml .= "<MAIN_GROUP_NAME><![CDATA[$main_group_name]]></MAIN_GROUP_NAME>";
					unset($groupArr['main_group_id']);unset($groupArr['main_group_name']);
					$xml .= "<GROUP_NAME><![CDATA[$group_name]]></GROUP_NAME>";				
					foreach($groupArr as $featuretitle=>$featurevalArr){
						if($featuretitle!=''){
						$xml .="<SUB_GROUP_DATA>";
						$featuretitle = empty($featuretitle) ? '' : $featuretitle;
						$xml .= "<FEATURE_TITLE><![CDATA[$featuretitle]]></FEATURE_TITLE>";
						$xml .="<FEATURE_VALUE><![CDATA[".implode(",&#160;",$featurevalArr)."]]></FEATURE_VALUE>";
						$xml .="</SUB_GROUP_DATA>";
						}
					}						
					$xml .= "</GROUP_MASTER_DATA>";
				}
				
			}
			$xml .= "</GROUP_MASTER>";
		}			
		return $xml;
	}
	function arrGetPivotFeatureDetails($feature_name,$category_id,$status="1"){
		$keyArr[] = $this->featurekey."_pivot_details";
		$whereClauseArr[] = "FEATURE_MASTER.feature_id = PIVOT_MASTER.feature_id";
		if(!empty($feature_name)){
			$keyArr[] = "feature_name_$feature_name";
			$whereClauseArr[] = "lower(FEATURE_MASTER.feature_name) = '$feature_name'";
		}
		if(!empty($category_id)){
			$keyArr[] = "category_id_$category_id";
			$whereClauseArr[] = "FEATURE_MASTER.category_id = $category_id";
		}
		if($status != ''){
			$keyArr[] = "status_$status";
			$whereClauseArr[] = "FEATURE_MASTER.status = $status";
		}
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
		}
		$key = implode("_",$keyArr);			
		if($result = $this->cache->get($key)){return $result;}
		$sql = "select * from FEATURE_MASTER,PIVOT_MASTER $whereClauseStr";
		$result = $this->select($sql);
		$this->cache->set($key,$result);
		return $result;
	}

	/**
	 * @note function is used to insert the popular feature car details into the database.
	 * @param an associative array $insert_param.
	 * @pre $insert_param must be valid associative array.
	 * @post an integer $popular_feature_id.
	 * retun integer.
	 */
	function intInsertPopularFeatureCars($insert_param)
	{
		$insert_param['create_date'] = date('Y-m-d H:i:s');
		$insert_param['update_date'] = date('Y-m-d H:i:s');
		$sql = $this->getInsertSql("POPULAR_FEATURE_CARS",array_keys($insert_param),array_values($insert_param));
		$popular_feature_id = $this->insert($sql);
		if($popular_feature_id == 'Duplicate entry'){ return 'exists';}
		$this->cache->searchDeleteKeys($this->featurekey."_popular");
		$this->arrGetPopularFeatureCarDetails($popular_feature_id);
		return $popular_feature_id;
	}
	
	/**
	 * @note function is used to update the popular feature car details into the database.
	 * @param an associative array $update_param.
	 * @param an integer $popular_feature_id.
	 * @pre $update_param must be valid associative array and $popular_feature_id must be non-empty/zero valid integer.
	 * @post boolean true/false.
	 * retun boolean.
	 */
	 function boolUpdatePopularFeatureCars($popular_feature_id,$update_param)
	 {

		 $update_param['create_date'] = date('Y-m-d H:i:s');
		 $update_param['update_date'] = date('Y-m-d H:i:s');
		 $sql = $this->getUpdateSql("POPULAR_FEATURE_CARS",array_keys($update_param),array_values($update_param),"popular_feature_id",$popular_feature_id);
		 
		 $isUpdate = $this->update($sql);
		 $this->cache->searchDeleteKeys($this->featurekey."_popular");
		 $this->arrGetPopularFeatureCarDetails($popular_feature_id);
		 return $isUpdate;
	 }

	 /**
	 * @note function is used to delete the popular feature car.
	 * @param integer $popular_feature_id.
	 * @pre $popular_feature_id must be non-empty/zero valid integer.
	 * @post boolean true/false.
	 * return boolean.
	 */
	 function boolDeletePopularFeatureCars($popular_feature_id)
	 {
		$sql = "delete from POPULAR_FEATURE_CARS where popular_feature_id = $popular_feature_id";
		$isDelete = $this->sql_delete_data($sql);
		$this->cache->searchDeleteKeys($this->featurekey."_popular");
		return $isDelete;
	 }

	 /**
	 * @note function is used to get popular feature car.
	 * @param an integer/comma seperated popular feature ids/ popular feature ids array $popular_feature_ids.
	 * @param an integer/comma seperated pivot groups/ pivot groups array $pivot_groups.
	 * @param an integer/comma seperated pivot ids/ pivot ids array $pivot_ids.
	 * @param an integer/comma seperated brand ids/ brand ids array $brand_ids.
	 * @param an integer/comma seperated model ids/ model ids array $model_ids.
	 * @param an integer/comma seperated product ids/ product ids array $product_ids.
	 * @param an integer/comma separated category_id $category_id.
	 * @param boolean Active/InActive $status.
	 * @param integer $startlimit.
	 * @param integer $count.
	 * @pre not required.
	 * @post popular feature car details in associative array.
	 * retun an array.
	 */
	function arrGetPopularFeatureCarDetails($popular_feature_ids="", $pivot_groups="", $pivot_ids="", $brand_ids="", $model_ids="", $product_ids="", $category_ids="", $status="1",$startlimit="",$count="",$orderby="",$feature_ids="")
    {
		$keyArr[] = $this->featurekey."_popular";
		if(is_array($popular_feature_ids)){
				$popular_feature_ids = implode(",",$popular_feature_ids);
		}
		if(is_array($pivot_groups)){
				$pivot_groups = implode(",",$pivot_groups);
		}
		if(is_array($pivot_ids)){
				$pivot_ids = implode(",",$pivot_ids);
		}
		if(is_array($brand_ids)){
				$brand_ids = implode(",",$brand_ids);
		}
		if(is_array($model_ids)){
				$model_ids = implode(",",$model_ids);
		}
		if(is_array($feature_ids)){
				$feature_ids = implode(",",$feature_ids);
		}
		if(is_array($product_ids)){
				$product_ids = implode(",",$product_ids);
		}
		if(is_array($category_ids)){
				$category_ids = implode(",",$category_ids);
		}
		if(!empty($popular_feature_ids)){
			$keyArr[] = "popular_feature_id_$popular_feature_ids";
			$whereClauseArr[] = "popular_feature_id in($popular_feature_ids)";
		}
		if(!empty($pivot_groups)){
			$keyArr[] = "pivot_group_$pivot_groups";
			$whereClauseArr[] = "pivot_group in($pivot_groups)";
		}
		if(!empty($pivot_ids)){
			$keyArr[] = "pivot_id_$pivot_ids";
			$whereClauseArr[] = "pivot_id in($pivot_ids)";
		}
		if(!empty($feature_ids)){
			$keyArr[] = "feature_id_$feature_ids";
			$whereClauseArr[] = "feature_id in($feature_ids)";
		}
		if(!empty($brand_ids)){
			$keyArr[] = "brand_id_$brand_ids";
			$whereClauseArr[] = "brand_id in($brand_ids)";
		}
		if(!empty($model_ids)){
			$keyArr[] = "model_id_$model_ids";
			$whereClauseArr[] = "model_id in($model_ids)";
		}
		if(!empty($product_ids)){
			$keyArr[] = "product_id_$product_ids";
			$whereClauseArr[] = "product_id in($product_ids)";
		}
		if(!empty($category_ids)){
			$keyArr[] = "category_id_$category_ids";
			$whereClauseArr[] = "category_id in ($category_ids)";
		}
		if($status != ''){
			$keyArr[] = "status_$status";
			$whereClauseArr[] = "status=$status";
		}
		if(sizeof($whereClauseArr) > 0){
				$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
		}
		if(!empty($startlimit)){
			$keyArr[] = "startlimit_$startlimit";
			$limitArr[] = $startlimit;
		}
		if(!empty($count)){
			$keyArr[] = "count_$count";
			$limitArr[] = $count;
		}
		if(sizeof($limitArr) > 0){
			$limitStr = " limit ".implode(" , ",$limitArr);
		}
		if(empty($orderby)){
			$orderby = "order by create_date desc";
		}
		$sql = "select * from POPULAR_FEATURE_CARS $whereClauseStr $orderby $limitStr";
		$key = implode("_",$keyArr);			
		if($result = $this->cache->get($key)){return $result;}
		$result = $this->select($sql);
		$this->cache->set($key,$result);
		return $result;
     }
	/**
	* @note function is used to get variant page summary.
	*
	* @param an integer $category_id.
	* @param an integer $product_id.
	*
	*
	* @post xml string.
	* retun string.
	*/
	function arrGetVariantPageSummary($category_id,$product_id){
		require_once(CLASSPATH.'price.class.php');
		require_once(CLASSPATH.'product.class.php');
		$product = new ProductManagement;

		$price = new price;

		$product_result = $product->arrGetProductDetails($product_id,$category_id,"","1","","","1","","","1");
		
		$product_name = $product_result[0]['product_name'];
		$variant = $product_result[0]['variant'];
		$product_desc = $product_result[0]['product_desc'];
		$exshowroom_price  = $product_result[0]['variant_value'];

		if(!empty($product_name)){
			$productArr[] = $product_name;
		}
		if(!empty($variant)){
			$productArr[] = $variant;
		}
		$product_name = implode(" ",$productArr);

		$all_variant_result = $price->arrGetVariantDetail();
		$cnt = sizeof($all_variant_result);
		$variantArr = Array();
		for($i=0;$i<$cnt;$i++){
			$variant = $all_variant_result[$i]['variant'];
			$variant_id = $all_variant_result[$i]['variant_id'];
			if(!stripos($variant,'showroom')){
				$variantArr[] = $variant;
			}
		}
		
		if(!empty($exshowroom_price)){
			$exshowroom_price = "Rs.".priceFormat($exshowroom_price);
			//$extra_variant = " (".implode(",",$variantArr)." and other taxes/charges extra)";
		}
		
		$aFeatureData=array();
		$aFeatureData["$product_name Price"]["Ex-Showroom Price"]= $exshowroom_price.$extra_variant;
		$aFeatureData["$product_name Price"]["product_desc"]= $product_desc;

		$sSql="SELECT * FROM FEATURE_OVERVIEW_MASTER FM, MAIN_FEATURE_GROUP SB WHERE FM.category_id = $category_id AND 	FM.overview_sub_group_id = SB.group_id order by FM.position asc";

		$key = $this->featurekey."_variant_page_overview_category_id_$category_id";
		$feature_result = $this->cache->get($key);
		
		if(empty($feature_result)){
			$feature_result = $this->select($sSql);
			
			$this->cache->set($key,$feature_result);
		}
				
		foreach($feature_result as $ikey=>$aValueData){
			$feature_id = $aValueData['feature_id'];
			if($feature_id != ''){
				unset($feature_img_path);unset($feature_description);unset($abbreviation);
				$sql = "select PRODUCT_FEATURE.*,FEATURE_MASTER.* from PRODUCT_FEATURE,FEATURE_MASTER where PRODUCT_FEATURE.feature_id = $feature_id and FEATURE_MASTER.feature_id = PRODUCT_FEATURE.feature_id and PRODUCT_FEATURE.product_id=$product_id";

				$key = $this->featurekey."_$feature_id"."_product_$product_id"."_variant_page_summary";
				$product_feature = $this->cache->get($key);
				
				if(empty($product_feature)){
					$product_feature = $this->select($sql);
					$this->cache->set($key,$product_feature);
				}
				$feature_value = trim($product_feature[0]['feature_value']);
				$feature_name = trim($product_feature[0]['feature_name']);
				$abbreviation = $aValueData['abbreviation'];
				$feature_description = trim($product_feature[0]['feature_description']);
				$feature_img_path = trim($product_feature[0]['feature_img_path']);				
			}
			unset($featureWithUnitArr);
			if(!empty($feature_value)){
				$featureWithUnitArr[] = $feature_value;
			}
			if(!empty($abbreviation) && !empty($feature_value)){
				$featureWithUnitArr[] = $abbreviation;
			}
			$overview_display_name = $aValueData['overview_display_name'] ? $aValueData['overview_display_name'] : $aValueData['main_group_name'];
			
			if($aValueData['title'] != ""){
				if(sizeof($featureWithUnitArr) > 0){
					$aFeatureData[$product_name." ".$overview_display_name][$aValueData['title']]['feature_value']= implode(" ",$featureWithUnitArr);
					$aFeatureData[$product_name." ".$overview_display_name][$aValueData['title']]['feature_description']= $feature_description;
					$aFeatureData[$product_name." ".$overview_display_name][$aValueData['title']]['feature_img_path']= $feature_img_path;
				}				
			}else{
				if(!empty($feature_value) && $feature_value != '-'){
					$titleArr[] = $feature_value;
				}
				if(!empty($abbreviation) && !empty($feature_value) && $feature_value != '-'){
					$titleArr[] = $abbreviation;
				}
				if(sizeof($titleArr) > 0){
					$aFeatureData[$product_name." ".$overview_display_name][0][] = implode("",$titleArr);
					
					$aFeatureData[$product_name." ".$overview_display_name]['main_group_id'] = $aValueData['overview_sub_group_id'];
					$aFeatureData[$product_name." ".$overview_display_name]['main_group_name'] = $overview_display_name;
				}
				unset($titleArr);
			}
		}
		return $aFeatureData;		
	}
}
?>
