<?php
/**
 * @brief class is used to perform actions on brand details.
 * @author Rajesh Ujade
 * @version 1.0
 * @created 11-Nov-2010 5:09:31 PM
 * @last updated on 08-Mar-2011 15:05:00 PM
 */
class BrandManagement extends DbOperation
{

	var $cache;
	var $categoryid;
	var $brandkey;
	/**Initialize the consturctor.*/
	function BrandManagement(){
		$this->cache = new Cache;
		$this->brandkey = MEMCACHE_MASTER_KEY."brand";
	}

	/**
	 * @note function is used to insert the brand details into the database.
	 * @param an associative array $insert_param.
	 * @pre $insert_param must be valid associative array.
	 * @post an integer $brand_id.
	 * retun integer.
	 */
	function intInsertBrand($insert_param){
		$insert_param['create_date'] = date('Y-m-d H:i:s');
		$insert_param['update_date'] = date('Y-m-d H:i:s');
		$sql = $this->getInsertSql("BRAND_MASTER",array_keys($insert_param),array_values($insert_param));
		$brand_id = $this->insert($sql);
		$this->cache->searchDeleteKeys($this->brandkey);
		if($brand_id == 'Duplicate entry'){ return 'exists';}
		return $brand_id;
	}
	/**
	 * @note function is used to update the brand details into the database.
	 * @param an associative array $update_param.
	 * @param an integer $brand_id.
	 * @pre $update_param must be valid associative array and $brand_id must be non-empty/zero valid integer.
	 * @post boolean true/false.
	 * retun boolean.
	 */
	 function boolUpdateBrand($brand_id,$update_param){
		$update_param['create_date'] = date('Y-m-d H:i:s');
		$update_param['update_date'] = date('Y-m-d H:i:s');
		$sql = $this->getUpdateSql("BRAND_MASTER",array_keys($update_param),array_values($update_param),"brand_id",$brand_id);
		//echo $sql;
		$isUpdate = $this->update($sql);
		$this->cache->searchDeleteKeys($this->brandkey);
		return $isUpdate;
	 }

	 /**
	 * @note function is used to get brand details.
	 * @param an integer/comma seperated brand ids/ brand ids array $brand_ids.
	 * @param an integer/comma separated category_id $category_id.
	 * @param boolean Active/InActive $status.
	 * @param integer $startlimit.
	 * @param integer $count.
	 * @pre not required.
	 * @post brand details in associative array.
	 * retun an array.
	 */
	 function arrGetBrandDetails($brand_ids="",$category_id="",$status="1",$startlimit="",$count="",$brand_name="",$orderby="")
	 {
  		$keys[] = $this->brandkey;
	 	if(is_array($brand_ids)){
	 		$brand_ids = implode(",",$brand_ids);
	 	}
	 	if($status != ''){
	 		$whereClauseArr[] = "status=$status";
			$keys[] = "status_$status";
	 	}
	 	if(!empty($brand_ids)){
	 		$whereClauseArr[] = "brand_id in($brand_ids)";
			$keys[] = "brand_id_$brand_ids";

	 	}
	 	if(!empty($category_id)){
	 		$whereClauseArr[] = "category_id in ($category_id)";
			$keys[] = "category_id_$category_id";
	 	}
		if(!empty($brand_name)){
			$brand_name = strtolower($brand_name);
	 		$whereClauseArr[] = "lower(brand_name)= '$brand_name'";
			$keys[] = "brand_name_$brand_name";
	 	}
	 	if(sizeof($whereClauseArr) > 0){
	 		$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
	 	}
	 	if(!empty($startlimit)){
	 		$limitArr[] = $startlimit;
			$keys[] = "startlimit_".$startlimit;
	 	}
	 	if(!empty($count)){
	 		$limitArr[] = $count;
			$keys[] = "count_".$count;
	 	}
	 	if(sizeof($limitArr) > 0){
	 		$limitStr = " limit ".implode(" , ",$limitArr);
	 	}
		if(empty($orderby)){
			$orderby = "order by brand_name asc";
		}
		$key = implode('_',$keys);
		//echo $key."<br>";
		$result = $this->cache->get($key);
		if(!empty($result)){ return $result;}
		$sql = "select * from BRAND_MASTER $whereClauseStr $orderby $limitStr";
		//echo $sql;
              $result = $this->select($sql);
		$this->cache->set($key, $result);
	 	return $result;
	 }

	 /**
	 * @note function is used to delete the brand.
	 * @param integer $brand_id.
	 * @pre $brand_id must be non-empty/zero valid integer.
	 * @post boolean true/false.
	 * return boolean.
	 */
	 function boolDeleteBrand($brand_id){
	 	$sql = "delete from BRAND_MASTER where brand_id = $brand_id";
	 	$isDelete = $this->sql_delete_data($sql);
		$this->cache->searchDeleteKeys($this->brandkey);
		//$this->cache->searchDeleteKeys("popular_brand");
	 	return $isDelete;
	 }

	/**
	 * @note function is used to insert the popular brand details into the database.
	 * @param an associative array $insert_param.
	 * @pre $insert_param must be valid associative array.
	 * @post an integer $popular_id.
	 * retun integer.
	 */
	function intInsertPopularBrand($insert_param){
		$insert_param['create_date'] = date('Y-m-d H:i:s');
		$insert_param['update_date'] = date('Y-m-d H:i:s');
		$sql = $this->getInsertSql("POPULAR_BRAND",array_keys($insert_param),array_values($insert_param));
		//echo $sql;
		$brand_id = $this->insert($sql);
		$this->cache->searchDeleteKeys($this->brandkey);
		if($brand_id == 'Duplicate entry'){ return 'exists';}
		return $brand_id;
	}
	 /**
	 * @note function is used to update the popular brand details into the database.
	 * @param an associative array $update_param.
	 * @param an integer $popular_id.
	 * @pre $update_param must be valid associative array and $popular_id must be non-empty/zero valid integer.
	 * @post boolean true/false.
	 * retun boolean.
	 */
	 function boolUpdatePopularBrand($popular_id,$update_param){
		 $update_param['create_date'] = date('Y-m-d H:i:s');
		 $update_param['update_date'] = date('Y-m-d H:i:s');
		 $sql = $this->getUpdateSql("POPULAR_BRAND",array_keys($update_param),array_values($update_param),"popular_id",$popular_id);
		 //echo $sql;
		 $isUpdate = $this->update($sql);
		 $this->cache->searchDeleteKeys($this->brandkey);
		 return $isUpdate;
	 }
	/**
	 * @note function is used to delete the popular brand.
	 * @param integer $popular_id.
	 * @pre $popular_id must be non-empty/zero valid integer.
	 * @post boolean true/false.
	 * return boolean.
	 */
	 function boolDeletePopularBrand($popular_id){
		$sql = "delete from POPULAR_BRAND where popular_id = $popular_id";
		$isDelete = $this->sql_delete_data($sql);
		$this->cache->searchDeleteKeys($this->brandkey);
		return $isDelete;
	 }

	/**
	 * @note function is used to get popular brand details.
	 * @param an integer/comma seperated popular ids/ popular ids array $popular_ids.
	 * @param an integer/comma seperated brand ids/ brand ids array $brand_ids.
	 * @param an integer/comma seperated popular model ids/ popular model ids array $popular_model_ids.
	 * @param an integer/comma separated category_id $category_id.
	 * @param boolean Active/InActive $status.
	 * @param integer $startlimit.
	 * @param integer $count.
	 * @pre not required.
	 * @post popular brand details in associative array.
	 * retun an array.
	 */
	function arrGetPopularBrandDetails($popular_ids="", $brand_ids="", $popular_model_ids="", $category_ids="", $status="1",$startlimit="",$count="",$orderby=""){
		$keys[] = $this->brandkey."_pop";
		if(is_array($popular_ids)){
			$popular_ids = implode(",",$popular_ids);
		}
		if(is_array($brand_ids)){
			$brand_ids = implode(",",$brand_ids);
		}
		if(is_array($popular_model_ids)){
		   $popular_model_ids = implode(",",$popular_model_ids);
		}
		if(is_array($category_ids)){
		   $category_ids = implode(",",$category_ids);
		}
		if(!empty($popular_ids)){
		   $whereClauseArr[] = "popular_id in($popular_ids)";
			$keys[] = "popular_ids_".$popular_ids;
		}
		if(!empty($brand_ids)){
		   $whereClauseArr[] = "brand_id in($brand_ids)";
		   $keys[] = "brand_id_".$brand_ids;
		}
		if(!empty($popular_model_ids)){
			$whereClauseArr[] = "popular_model_id in($popular_model_ids)";
			$keys[] = "popular_model_id_".$popular_model_ids;
		}
		if(!empty($category_ids)){
			$whereClauseArr[] = "category_id in ($category_ids)";
			$keys[] = "category_id_".$category_ids;
		}
		if($status != ''){
			$whereClauseArr[] = "status=$status";
			$keys[] = "status_".$status;
		}
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
		}
		if(!empty($startlimit)){
			$limitArr[] = $startlimit;
			$keys[] = "startlimit_".$startlimit;
		}
		if(!empty($count)){
			$limitArr[] = $count;
			$keys[] = "count_".$count;
		}
		if(sizeof($limitArr) > 0){
			$limitStr = " limit ".implode(" , ",$limitArr);
		}
		if(empty($orderby)){
			$orderby = "order by create_date desc";
			$keys[] = " order_".str_replace(" ","_",$orderby);
		}
		$key = implode('_',$keys);
		$result = $this->cache->get($key);
		if(!empty($result)){ return $result;}
		$sql = "select * from POPULAR_BRAND $whereClauseStr $orderby $limitStr";
		//echo $sql;
		$result = $this->select($sql);
		$this->cache->set($key, $result);
		return $result;
	 }

	/**
	 * @note function is used to get popular brand details count.
	 * @param an integer/comma seperated popular ids/ popular ids array $popular_ids.
	 * @param an integer/comma seperated brand ids/ brand ids array $brand_ids.
	 * @param an integer/comma seperated popular model ids/ popular model ids array $popular_model_ids.
	 * @param an integer/comma separated category_id $category_id.
	 * @param boolean Active/InActive $status.
	 * @param integer $startlimit.
	 * @param integer $count.
	 * @pre not required.
	 * @post integer count.
	 * retun an array.
	 */
	 function arrGetPopularBrandCount($popular_ids="", $brand_ids="", $popular_model_ids="", $category_ids="", $status="1") {
		$keys[] = $this->brandkey."_popcnt";
		if(is_array($popular_ids)){
			$popular_ids = implode(",",$popular_ids);
		}
		if(is_array($brand_ids)){
			$brand_ids = implode(",",$brand_ids);
		}
		if(is_array($popular_model_ids)){
			$popular_model_ids = implode(",",$popular_model_ids);
		}
		if(is_array($category_ids)){
			$category_ids = implode(",",$category_ids);
		}
		if(!empty($popular_ids)){
			$whereClauseArr[] = "popular_id in($popular_ids)";
			$keys[] = "popular_id_".$popular_ids;
		}
		if(!empty($brand_ids)){
			$whereClauseArr[] = "brand_id in($brand_ids)";
			$keys[] = "brand_id_".$brand_ids;
		}
		if(!empty($popular_model_ids)){
			$whereClauseArr[] = "popular_model_id in($popular_model_ids)";
			$keys[] = "popular_model_id_".$popular_model_ids;
		}
		if(!empty($category_ids)){
			$whereClauseArr[] = "category_id in ($category_ids)";
			$keys[] = "category_id_".$category_ids;
		}
		if($status != ''){
			$whereClauseArr[] = "status=$status";
			$keys[] = "status_".$status;
		}
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
		}
		$key = implode('_',$keys);
		$result = $this->cache->get($key);
		if(!empty($result)){return $result;}
		$sql = "select count(popular_id) as cnt from POPULAR_BRAND $whereClauseStr";
		//echo $sql;
		$result = $this->select($sql);
		$this->cache->set($key, $result);
		return $result;
	}


	 /**
	 * @note function is used to get brand details in array[brand_id]=brand_name form.
	 * @param an integer/comma seperated brand ids/ brand ids array $brand_ids.
	 * @param an integer/comma separated category_id $category_id.
	 * @param boolean Active/InActive $status.
	 * @param integer $startlimit.
	 * @param integer $count.
	 * @pre not required.
	 * @post brand details in associative array.
	 * retun an array.
	 */
	 function arrGetBrandDetailsInAssociativeForm($brand_ids="",$category_id="",$status="1",$startlimit="",$count="",$brand_name="",$orderby="")
	 {
	 	$result	= $this->arrGetBrandDetails($brand_ids,$category_id,$status,$startlimit,$count,$brand_name,$orderby);
		if($result){
			foreach($result as $key => $value ){
				$data[$value['brand_id']]	= $value['brand_name'];
			}
		}
		return $data;

	}
	function updateBrandPosUp($category_id,$brand_id,$pos){
		$prevpos = $pos-1;
		$sql = "select brand_id from BRAND_MASTER where position = $prevpos and category_id = $category_id";
		$result = $this->select($sql);
		$prev_brand_id = $result[0]['brand_id'];
		$sql = "update BRAND_MASTER set position = $pos where brand_id = $prev_brand_id";
		$result = $this->update($sql);
		$sql = "update BRAND_MASTER set position = $prevpos where brand_id = $brand_id";
		$result = $this->update($sql);
		return true;
	}
	function updateBrandPosDown($category_id,$brand_id,$pos){
		$nextpos = $pos+1;
		$sql = "select brand_id from BRAND_MASTER where position = $nextpos and category_id = $category_id";
		$result = $this->select($sql);
		$next_brand_id = $result[0]['brand_id'];
		$sql = "update BRAND_MASTER set position = $pos where brand_id = $next_brand_id";
		$result = $this->update($sql);
		$sql = "update BRAND_MASTER set position = $nextpos where brand_id = $brand_id";
		$result = $this->update($sql);
		return true;
	}

	/**
	* @note function is used to update the position of Brands
	* @param an associative array $arrData.
	* @pre $arrData must be valid associative array.
	* retun integer.
	*/
	function updatePosition( $arrData,$dbconn){
			//print_r($arrData);
			foreach ($arrData as $position => $item) :
					$sSql   = "UPDATE BRAND_MASTER SET position = $position WHERE brand_id = $item";
					//echo "<br/>", $sSql;
					$op             = $this->update($sSql);
			endforeach;
			//$this->cache->searchDeleteKeys($this->cacheKey);
			return $op;
	}
}
?>