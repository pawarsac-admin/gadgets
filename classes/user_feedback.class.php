<?php
	/**
	* @brief class is used to perform actions on user feedback
	* @author Sachin(sachin@corp.india.com)
	* @version 1.0
	* @created 3rdMar2011
	* @last updated on 08-Mar-2011 13:14:00 PM
	*/
	class FEEDBACKUSER extends DbOperation
	{
		var $cache;
		var $feedbackKey;
		/**Initialize constructor*/
		function FEEDBACKUSER(){
			$this->cache = new Cache;
			$this->feedbackKey = MEMCACHE_MASTER_KEY."feedback";
		}
		/**
		* @note function is used  to get feedback subject details.
		* @param an integer/comma seperated subject ids/ subject ids array $subject_ids.
		* @param boolean Active/InActive $status.
		* @param integer $startlimit.
		* @param integer $count.
		* @param string $orderby.
		* @pre not required.
		* @post return group detail if successful , 0 if error occurs
		*/
		function arrGetFeedbackSubjectDetails($subject_ids="",$status="1",$startlimit="",$count="",$orderby=""){
			$keyArr[] = $this->feedbackKey."_subject";	

			if(is_array($subject_ids)){
				$subject_ids=implode(",",$subject_ids);
			}
			if($subject_ids !=""){
				$keyArr[] = "subjects_$subject_ids";
				$whereClauseArr[] = "subject_id in ($subject_ids)";
			}
			if($status != ''){
				$keyArr[] = "status_$status";
				$whereClauseArr[] = "status = $status";
			}
			if(sizeof($whereClauseArr) > 0){
				$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
			}
			if(!empty($startlimit)){
				$keyArr[] = "startlimit_$startlimit";
				$limitArr[] = $startlimit;
			}
			if(!empty($count)){
				$keyArr[] = "count_$count";
				$limitArr[] = $count;
			}
			if(sizeof($limitArr) > 0){
				$limitStr = " limit ".implode(" , ",$limitArr);
			}
			if(!empty($orderby)){
				$orderby = " order by ".$orderby." ASC";
			}
			$key = implode("_",$keyArr);
            if($result = $this->cache->get($key)){return $result;}
			$sql="SELECT *, DATE_FORMAT(create_date,'%d/%m/%Y') as create_date from FEEDBACK_SUBJECT $whereClauseStr $orderby $limitStr";
			$result =$this->select($sql);
			$this->cache->set($key,$result);
			return $result;
		}
		/**
		* @note function is used  to get feedback details.
		* @param an integer/comma seperated feedback ids $feedback_ids.
		* @param an string/comma seperated subjects $subjects.
		* @param boolean Active/InActive $status.
		* @param integer $startlimit.
		* @param integer $count.
		* @param string $orderby.
		* @pre not required.
		* @post return group detail if successful , 0 if error occurs
		*/
		function arrGetFeedbackDetails($feedback_ids="",$subjects="",$status="1",$startlimit="",$count="",$orderby=""){
			$keyArr[] = $this->feedbackKey;		
	
			if(is_array($feedback_ids)){
				$feedback_ids=implode(",",$feedback_ids);
			}
			if($feedback_ids !=""){
				$keyArr[] = "feedback_id_$feedback_ids";
				$whereClauseArr[] = "feedback_id in ($feedback_ids)";
			}
			if(is_array($subjects)){
				$subjects=implode(",",$subjects);
			}
			if($subjects !=""){
				$keyArr[] = "subjects_$subject_ids";
				$whereClauseArr[] = "subject in ($subjects)";
			}
			if($status != ''){
				$keyArr[] = "status_$status";
				$whereClauseArr[] = "status = $status";
			}
			if(sizeof($whereClauseArr) > 0){
				$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
			}
			if(!empty($startlimit)){
				$keyArr[] = "startlimit_$startlimit";
				$limitArr[] = $startlimit;
			}
			if(!empty($count)){
				$keyArr[] = "count_$count";
				$limitArr[] = $count;
			}
			if(sizeof($limitArr) > 0){
				$limitStr = " limit ".implode(" , ",$limitArr);
			}
			if(!empty($orderby)){
				$orderby = " order by ".$orderby." DESC";
			}
			$key = implode("_",$keyArr);
            if($result = $this->cache->get($key)){return $result;}
			$sql="SELECT *, DATE_FORMAT(create_date,'%d/%m/%Y') as create_date from FEEDBACK $whereClauseStr $orderby $limitStr";
			$result =$this->select($sql);
			$this->cache->set($key,$result);
			return $result;
		}
		/**
		* @note function is used to insert feedback subject details.
		* @param associative array $insert_param.
		* @pre $insert_param must be valid non-empty associative array.
		* @post integer id.
		* return integer.
		*/
		function intInsertFeedbackSubject($insert_param){
			$insert_param['create_date'] = date('Y-m-d H:i:s');
			$insert_param['update_date'] = date('Y-m-d H:i:s');
			$sql = $this->getInsertUpdateSql('FEEDBACK_SUBJECT',array_keys($insert_param),array_values($insert_param));
			$result=$this->insertUpdate($sql);
			$this->cache->searchDeleteKeys($this->feedbackKey."_subject");
			return $result;
		}
		/**
		* @note function is used to insert feedback details.
		* @param associative array $insert_param.
		* @pre $insert_param must be valid non-empty associative array.
		* @post integer id.
		* return integer.
		*/
		function intInsertFeedback($insert_param){
			$insert_param['create_date'] = date('Y-m-d H:i:s');
			$insert_param['update_date'] = date('Y-m-d H:i:s');
			$sql = $this->getInsertUpdateSql('FEEDBACK',array_keys($insert_param),array_values($insert_param));
			$result=$this->insertUpdate($sql);
			$this->cache->searchDeleteKeys($this->feedbackKey);
			return $result;
		}
		/**
		* @note function is used  delete feedback
		* @param an integer $feedback_id.
		* @pre $feedback_id must be valid non-empty/zero integer.
		* @post boolean true/false.
		* return boolean.
		*/
		function booldeleteFeedback($feedback_id=""){
			$sSql="delete from FEEDBACK where feedback_id='".$feedback_id."'";
			$iRes=$this->sql_delete_data($sSql);
			$this->cache->searchDeleteKeys($this->feedbackKey);
			return $iRes;
		}
		/**
		* @note function is used  delete feedback subject
		* @param an integer $subject_id.
		* @pre $subject_id must be valid non-empty/zero integer.
		* @post boolean true/false.
		* return boolean.
		*/
		function booldeleteFeedbackSubject($subject_id=""){
			$sSql="delete from FEEDBACK_SUBJECT where subject_id='".$subject_id."'";
			$iRes=$this->sql_delete_data($sSql);
			$this->cache->searchDeleteKeys($this->feedbackKey."_subject");
			return $iRes;
		}
		/**
		* @note function is used to insert feedback information details.
		* @param associative array $insert_param.
		* @pre $insert_param must be valid non-empty associative array.
		* @post integer $answer_id.
		* return integer.
		*/
		function intInsertFeedbackInfo($insert_param){
			$insert_param['create_date'] = date('Y-m-d H:i:s');
			$insert_param['update_date'] = date('Y-m-d H:i:s');
			$sql = $this->getInsertSql("FEEDBACK",array_keys($insert_param),array_values($insert_param));
			$answer_id = $this->insert($sql);
			if($answer_id == 'Duplicate entry'){ return 'exists';}
			$this->cache->searchDeleteKeys($this->feedbackKey);
			return $answer_id;
		}
		/**
		* @note function is used to insert contactus information details.
		* @param associative array $insert_param.
		* @pre $insert_param must be valid non-empty associative array.
		* @post integer $answer_id.
		* return integer.
		*/
		function intInsertContactusInfo($insert_param){
			$insert_param['create_date'] = date('Y-m-d H:i:s');
			$insert_param['update_date'] = date('Y-m-d H:i:s');
			$sql = $this->getInsertSql("CONTACTUS",array_keys($insert_param),array_values($insert_param));
			$answer_id = $this->insert($sql);
			if($answer_id == 'Duplicate entry'){ return 'exists';}
			$this->cache->searchDeleteKeys($this->feedbackKey."_contactusinfo");
			return $answer_id;
		}
		/**
		* @note function is used to insert share email details.
		* @param associative array $insert_param.
		* @pre $insert_param must be valid non-empty associative array.
		* @post integer $answer_id.
		* return integer.
		*/
		function intInsertShareEmail($insert_param){
			$insert_param['create_date'] = date('Y-m-d H:i:s');
			$insert_param['update_date'] = date('Y-m-d H:i:s');
			$sql = $this->getInsertSql("SHARE_EMAIL_FRIENDS",array_keys($insert_param),array_values($insert_param));
			$answer_id = $this->insert($sql);
			if($answer_id == 'Duplicate entry'){ return 'exists';}
			$this->cache->searchDeleteKeys($this->feedbackKey."_sharemail");
			return $answer_id;
		}
	}
?>
