<?

class RSS {

    var $rss;
    var $title;
    var $description;
    var $link;
    var $image;
    var $item_list;

    function RSS()
    {
        $this->add_header();
    }

    function get_date($date)
    {
        return $date;
    }

    function get_product_details(&$title,&$link,&$description,$product_id){


    }
    function get_expert_review($id)
    {

    }

    function add_header()
    {
        $this->rss .= '<?xml version="1.0" encoding="utf-8"?>';
        $this->rss .= '<rss version="2.0">';
        $this->rss .= '<channel>';
    }

    function add_info()
    {
        $this->rss .= "<title>".htmlentities($this->title)."</title>";
        $this->rss .= "<link>".$this->link."</link>";
        $this->rss .= "<description>".$this->description."</description>";
        $this->rss .= "<image>".$this->image."</image>";
        $this->rss .= "<lastBuildDate>".date("D, d M Y H:i:s IST")."</lastBuildDate>";
        $this->rss .= "<language>en-us</language>";
    }

    function add_item()
    {

    }

    function add_footer()
    {
        $this->rss .= "</channel>";
        $this->rss .= "</rss>";
    }

    function get_data($type ,$id)
    {
        $temp = split("_",$type);
        $data_type = $temp[0];
        $data_pivot = $temp[1];
        switch ($data_type)
        {
            case "review" : $this->get_product_data($id);
                            $this->get_product_review($id);
                            break;
            case "product" :
                            $this->get_product_master_data($data_pivot,$id);
                            break;
            case "price" :
                            $this->get_product_by_price($data_pivot,$id);
                            break;
            case "rating" :
                            $this->get_top_rated_product($data_pivot,$id);
                            break;
            case "topviewusr" :
                            $this->get_top_view_products_by_user($data_pivot,$id);
                            break;
            case "topviewcat" :
                            $this->get_top_view_categories_by_user($data_pivot,$id);
                            break;
            case "topcompare" :
                            //to do
                            $this->get_top_compared_products($id);
                            break;

            default : return false;
        }
        return true;
    }

    function get_product_data($id)
    {

    }

    function get_product_review($id)
    {

    }

    function get_rss($type,$id)
    {
        if ($this->get_data($type,$id)) $this->add_info();
        $this->add_item();
        $this->add_footer();
        return $this->rss;
    }
/*** Functions for related to seller listing of products ***/
    function get_product_master_data($pivot,$id)
    {
        switch ($pivot)
        {
            case "cat" :
                                $this->get_product_by_category($id);
                                break;
            case "brand" :
                                $this->get_product_by_brand($id);
                                break;

            default :           $this->get_product_all();
                                break;
        }
    }



    function get_product_by_category($id)
    {

    }

    function get_product_by_brand($id)
    {

    }

    function get_product_all($id)
    {


    }

    function create_product_description($result)
    {
		$count = count($result);
        if ($count > 20) $count = 20;
        for ($i = 0;$i < $count; $i++)
        {

            $this->get_product_details($title,$link,$description,$result[$i][product_id]);

            $description .= "<br /><b>Price</b> : ";

            $description .= $this->get_expert_review($result[$i][product_id]);
            $this->item_list[] = array("title" => $title,"link" => $link, "description" => $description, "pubDate" => $this->get_date($result[$i][create_date]) );
        }
    }



/*** Functions for related to lowest/highest priced products ***/

    function get_product_by_price($pivot,$id)
    {

        $list = substr($pivot,0,strlen($pivot) - 1);
        $type = substr($pivot,-1);
        switch ($list)
        {
            case "cat" :
                                $this->get_price_by_category($type,$id);
                                break;
            case "brand" :
                                $this->get_price_by_brand($type,$id);
                                break;
            case "model" :
                                $this->get_price_by_model($type,$id);
                                break;

            default :           $this->get_price_all($type);
                                break;
        }
    }

    function get_price_by_category($type,$id)
    {


    function get_price_by_brand($type,$id)
    {

    }

    function get_price_by_model($type,$id)
    {

    }

    function get_price_all($type)
    {

    }

    function create_price_description($result)
    {

    }

/*** Functions for related to lowest/highest priced products ends***/

/*** Functions for related to top rated products ***/

    function get_top_rated_product($pivot,$id)
    {

        switch ($pivot)
        {
            case "cat" :
                                $this->get_top_rated_by_category($id);
                                break;
            case "brand" :
                                $this->get_top_rated_by_brand($id);
                                break;

            default :           $this->get_top_rated_all();
                                break;
        }
    }

    function get_top_rated_by_category($id)
    {

    }

    function get_top_rated_by_brand($id)
    {

    }

    function get_top_rated_all()
    {

    }

    function create_top_rated_description($result)
    {

    }

/*** Functions for related to top rated priced products ends***/



/*** Function for top view by users start ***/

    function get_top_view_products_by_user($pivot,$id)
    {
        switch ($pivot)
        {
            case "cat" :
                                $this->get_top_view_products_by_user_by_category($id);
                                break;
            case "brand" :
                                $this->get_top_view_products_by_user_by_brand($id);
                                break;

            default :           $this->get_top_view_products_by_user_by_all();
                                break;
        }
    }

    function get_top_view_products_by_user_by_category($id)
    {


    }

    function get_top_view_products_by_user_by_brand($id)
    {

    }

    function get_top_view_products_by_user_by_all($id)
    {


    }

    function create_top_view_products_by_user_description($result)
    {
	}
/*** Function for top view by user ends ****/

/*** Function for top view categories by user ends ****/

    function get_top_view_categories_by_user()
    {

    }

/*** Function for top view categories by user ends ****/



}
?>
