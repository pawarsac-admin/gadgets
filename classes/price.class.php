<?php
/**
 * @brief class is used to perform actions on user reviews
 * @author Sachin
 * @version 1.0
 * @created 11-Nov-2010 5:09:31 PM
 * @last updated on 08-Mar-2011 13:14:00 PM
 */
	class price extends DbOperation{

		var $cache;
		var $priceKey;
		var $productKey;
		/**Intialize the consturctor.*/
		function price(){
			$this->cache = new Cache;
			$this->priceKey = MEMCACHE_MASTER_KEY."price";
			$this->productKey = MEMCACHE_MASTER_KEY."product";
		}

		/**
		* @note function is used to insert/update variant details into the database.
		* @param an associative array $insert_param.
		* @pre $insert_param must be valid associative array.
		* @post an integer $variant_id.
		* retun integer.
		*/
		function intInsertUpdateVariantDetail($insert_param){
			$insert_param['create_date'] = date('Y-m-d H:i:s');
			$insert_param['update_date'] = date('Y-m-d H:i:s');
			$sSql = $this->getInsertUpdateSql("PRICE_VARIANT_MASTER",array_keys($insert_param),array_values($insert_param));
			$variant_id = $this->insertUpdate($sSql);
			$this->cache->searchDeleteKeys($this->priceKey."_variant");			
			return $variant_id;
		}

		function intInsertUpdateVariantDetailTest($insert_param){
			$insert_param['create_date'] = date('Y-m-d H:i:s');
			$insert_param['update_date'] = date('Y-m-d H:i:s');
			$sSql = $this->getInsertUpdateSql("PRICE_VARIANT_VALUES_BENGALURU",array_keys($insert_param),array_values($insert_param));
			$variant_id = $this->insertUpdate($sSql);
			return $variant_id;
		}
		/**
		* @note function is used to insert variant value details into the database.
		* @param an associative array $insert_param.
		* @pre $insert_param must be valid associative array.
		* @post an integer $price_variant.
		* retun integer.
		*/
		function intInsertVariantValueDetail($insert_param){
			$insert_param['create_date'] = date('Y-m-d H:i:s');
			$insert_param['update_date'] = date('Y-m-d H:i:s');
			$sSql = $this->getInsertUpdateSql("PRICE_VARIANT_VALUES",array_keys($insert_param),array_values($insert_param));
			$price_variant = $this->insertUpdate($sSql);
			$this->cache->searchDeleteKeys($this->priceKey);
			$this->cache->searchDeleteKeys($this->productKey);
			return $price_variant;
		}
		/**
		* @note function is used to insert/update variant formula details into the database.
		* @param an associative array $insert_param.
		* @pre $insert_param must be valid associative array.
		* @post an integer $variant_id.
		* retun integer.
		*/
		function intInsertUpdateVariantFormula($insert_param){
			$insert_param['create_date'] = date('Y-m-d H:i:s');
			$insert_param['update_date'] = date('Y-m-d H:i:s');
			$sSql = $this->getInsertUpdateSql("PRICE_VARIANT_FORMULA",array_keys($insert_param),array_values($insert_param));
			$Variant_id = $this->insertUpdate($sSql);
			$this->cache->searchDeleteKeys($this->priceKey."_formula");
			return $Variant_id;
		}
		/**
		* @note function is used to delete variant detail.
		* @param integer $iPid.
		* @pre $iPid must be non-empty/zero valid integer.
		* @post boolean true/false.
		* return boolean.
		*/
		function boolDeleteVariantDetail($iPid){
			$sql = "delete from PRICE_VARIANT_MASTER where variant_id = $iPid";
			$isDelete = $this->sql_delete_data($sql);
			$this->cache->searchDeleteKeys($this->priceKey."_variant");
			return $isDelete;
		}
		/**
		* @note function is used to delete variant value detail.
		* @param integer $iRid.
		* @pre $iRid must be non-empty/zero valid integer.
		* @post boolean true/false.
		* return boolean.
		*/
		function boolDeleteVariantValueDetail($iRid){
			$sql = "delete from PRICE_VARIANT_VALUES where price_variant=$iRid";
			$isDelete = $this->sql_delete_data($sql);
			$this->cache->searchDeleteKeys($this->priceKey);
			return $isDelete;
		}
		/**
		* @note function is used to delete variant formula detail.
		* @param integer $iFid.
		* @pre $iFid must be non-empty/zero valid integer.
		* @post boolean true/false.
		* return boolean.
		*/
		function boolDeleteVariantFormula($iFid){
			$sql = "delete from PRICE_VARIANT_FORMULA where	variant_formula_id  = $iFid";
			$isDelete = $this->sql_delete_data($sql);
			$this->cache->searchDeleteKeys($this->priceKey."_formula");
			$this->arrGetVariantFormulaDetail();
			return $isDelete;
		}
		/**
		* @note function is used to get variant details
		*
		* @param an integer/comma seperated variant ids $variant_id. 
		* @param an integer/comma seperated category ids $category_id. 
		* @param boolean Active/InActive $status.
		* @param integer $startlimit.
		* @param integer $cnt.
		*
		* @pre not required.
		*
		* @post variant details in associative array.
		* retun an array.
		*/
		function arrGetVariantDetail($variant_id,$category_id,$status="1",$startlimit="",$cnt=""){
			$keyArr[] = $this->priceKey."_variant";
			if(!empty($variant_id)){
				$keyArr[] = "variant_id_$variant_id";
				$whereClauseArr[]=" variant_id in ($variant_id)";
			}
			if(!empty($category_id)){
				$keyArr[] = "category_id_$category_id";
				$whereClauseArr[]=" category_id in ($category_id)";
			}
			if($status != ""){
				$keyArr[] = "status_$status";
				$whereClauseArr[] = "status = $status";
			}
			if(!empty($startlimit)){
				$keyArr[] = "startlimit_$startlimit";
				$limitArr[] = $startlimit;
			}
			if(!empty($cnt)){
				$keyArr[] = "cnt_$cnt";
				$limitArr[] = $cnt;
			} 
			if(sizeof($limitArr) > 0){
				$limitStr = " limit ".implode(" , ",$limitArr);
			}

			if(sizeof($whereClauseArr) > 0){
				$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
			}
			$key = implode("_",$keyArr);
			if($result = $this->cache->get($key)){return $result;}
			$sql = "select PRICE_VARIANT_MASTER.* from PRICE_VARIANT_MASTER $whereClauseStr order by variant $orderby $limitStr";
			$result = $this->select($sql);
			$this->cache->set($key,$result);
			return $result;
		}
		function arrGetVariantDetailCount($category_id,$status="1"){
			$keyArr[] = $this->priceKey."_variant_count";
			if(!empty($category_id)){
				$keyArr[] = "category_id_$category_id";
				$whereClauseArr[]=" category_id in ($category_id)";
			}
			if($status != ""){
				$keyArr[] = "status_$status";
				$whereClauseArr[] = "status = $status";
			}
			if(sizeof($whereClauseArr) > 0){
				$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
			}
			$key = implode("_",$keyArr);
			if($result = $this->cache->get($key)){return $result;}
			$sql = "select count(variant_id) as cnt from PRICE_VARIANT_MASTER $whereClauseStr";
			$result = $this->select($sql);
			$this->cache->set($key,$result);
			return $result;
		}
		/**
		* @note function is used to get variant details
		*
		* @param an integer/comma seperated product variant ids $product_variant_id. 
		* @param an integer/comma seperated variant ids $variant_id. 
		* @param an integer/comma seperated product ids/ product ids array $product_id.
		* @param an integer/comma seperated category ids $category_id. 
		* @param an integer/comma seperated brand ids $brand_id. 
		* @param an integer/comma seperated state ids $state_id. 
		* @param an integer/comma seperated city ids $city_id. 
		* @param boolean Active/InActive $status.
		* @param integer $startlimit.
		* @param integer $cnt.
		*
		* @pre not required.
		*
		* @post user variant value details in associative array.
		* retun an array.
		*/
		function arrGetVariantValueDetail($price_variant_id="1",$variant_id="",$product_id="",$category_id="",$brand_id="",$state_id="",$city_id="",$status="1",$startlimit="",$cnt="",$default_city="",$orderby="order by price_variant asc"){
			$keyArr[] = $this->priceKey;
			if(is_array($product_id)){
				$product_id = implode(",",$product_id);
			}
			if(!empty($variant_id)){
				$keyArr[] = "variant_id_$variant_id";
				$whereClauseArr[]=" variant_id in ($variant_id)";
			}
			if(!empty($price_variant_id)){
				$keyArr[] = "price_variant_id_$price_variant_id";
				$whereClauseArr[]=" price_variant in ($price_variant_id)";
			}
			if(!empty($product_id)){
				$keyArr[] = "product_id_$product_id";
				$whereClauseArr[]=" product_id in ($product_id)";
			}
			if(!empty($category_id)){
				$keyArr[] = "category_id_$category_id";
				$whereClauseArr[]=" category_id in ($category_id)";
			}
			if(!empty($brand_id)){
				$keyArr[] = "brand_id_$brand_id";
				$whereClauseArr[]=" brand_id in ($brand_id)";
			}
			if(!empty($state_id)){
				$keyArr[] = "state_id_$state_id";
				$whereClauseArr[]=" state_id in ($state_id)";
			}
			if(!empty($city_id)){
				$keyArr[] = "city_id_$city_id";
				$whereClauseArr[]=" city_id in ($city_id)";
			}
			if($default_city != ""){
				$keyArr[] = "default_city_$default_city";
				$whereClauseArr[] = "PRICE_VARIANT_VALUES.default_city = $default_city";
			}
			if($status != ""){
				$keyArr[] = "status_$status";
				$whereClauseArr[] = "status = $status";
			}
			if(!empty($startlimit)){
				$keyArr[] = "startlimit_$startlimit";
				$limitArr[] = $startlimit;
			}
			if(!empty($cnt)){
				$keyArr[] = "cnt_$cnt";
				$limitArr[] = $cnt;
			}
			if(sizeof($limitArr) > 0){
				$limitStr = " limit ".implode(" , ",$limitArr);
			}
			if(sizeof($whereClauseArr) > 0){
				$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
			}
			$key = implode("_",$keyArr);
			if($result = $this->cache->get($key)){return $result;}
			$sql = "select PRICE_VARIANT_VALUES .* from PRICE_VARIANT_VALUES $whereClauseStr $orderby $limitStr";
			$result = $this->select($sql);
			$this->cache->set($key,$result);
			return $result;
		}
		function arrGetVariantValueDetailCount($price_variant_id="1",$variant_id="",$product_id="",$category_id="",$brand_id="",$state_id="",$city_id="",$status="1",$default_city=""){
			$keyArr[] = $this->priceKey."_count";
			if(is_array($product_id)){
				$product_id = implode(",",$product_id);
			}
			if(!empty($variant_id)){
				$keyArr[] = "variant_id_$variant_id";
				$whereClauseArr[]=" variant_id in ($variant_id)";
			}
			if(!empty($price_variant_id)){
				$keyArr[] = "price_variant_id_$price_variant_id";
				$whereClauseArr[]=" price_variant in ($price_variant_id)";
			}
			if(!empty($product_id)){
				$keyArr[] = "product_id_$product_id";
				$whereClauseArr[]=" product_id in ($product_id)";
			}
			if(!empty($category_id)){
				$keyArr[] = "category_id_$category_id";
				$whereClauseArr[]=" category_id in ($category_id)";
			}
			if(!empty($brand_id)){
				$keyArr[]= "brand_id_$brand_id";
				$whereClauseArr[]=" brand_id in ($brand_id)";
			}
			if(!empty($state_id)){
				$keyArr[] = "state_id_$state_id";
				$whereClauseArr[]=" state_id in ($state_id)";
			}
			if(!empty($city_id)){
				$keyArr[] = "city_id_$city_id";
				$whereClauseArr[]=" city_id in ($city_id)";
			}
			if($default_city != ""){
				$keyArr[] = "default_city_$default_city";
				$whereClauseArr[] = "PRICE_VARIANT_VALUES.default_city = $default_city";
			}
			if($status != ""){
				$keyArr[] = "status_$status";
				$whereClauseArr[] = "status = $status";
			}
			if(sizeof($whereClauseArr) > 0){
				$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
			}
			$key = implode("_",$keyArr);
			if($result = $this->cache->get($key)){return $result;}
			$sql = "select count(PRICE_VARIANT_VALUES .price_variant) as cnt from PRICE_VARIANT_VALUES $whereClauseStr";
			$result = $this->select($sql);
			$this->cache->set($key,$result);
			return $result;
		}
		function arrGetVariantValueDetailTest($price_variant_id="1",$variant_id="",$product_id="",$category_id="",$brand_id="",$state_id="",$city_id="",$status="1",$startlimit="",$cnt=""){

			if(is_array($product_id)){
				$product_id = implode(",",$product_id);
			}

			if(!empty($variant_id)){
				$whereClauseArr[]=" variant_id in ($variant_id)";
			}
			if(!empty($price_variant_id)){
				$whereClauseArr[]=" price_variant in ($price_variant_id)";
			}
			if(!empty($product_id)){
				$whereClauseArr[]=" product_id in ($product_id)";
			}
			if(!empty($category_id)){
				$whereClauseArr[]=" category_id in ($category_id)";
			}
			if(!empty($brand_id)){
				$whereClauseArr[]=" brand_id in ($brand_id)";
			}
			if(!empty($state_id)){
				$whereClauseArr[]=" state_id in ($state_id)";
			}
			if(!empty($city_id)){
				$whereClauseArr[]=" city_id in ($city_id)";
			}
			if($status != ""){
				$whereClauseArr[] = "status = $status";
			}
			if(!empty($startlimit)){
				$limitArr[] = $startlimit;
			}
			if(!empty($cnt)){
				$limitArr[] = $cnt;
			}
			if(sizeof($limitArr) > 0){
				$limitStr = " limit ".implode(" , ",$limitArr);
			}
			if(sizeof($whereClauseArr) > 0){
				$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
			}
			$sql = "select PRICE_VARIANT_VALUES_BENGALURU .* from PRICE_VARIANT_VALUES_BENGALURU $whereClauseStr order by price_variant $limitStr";
			$result = $this->select($sql);
			return $result;
		}
		/**
		* @note function is used to get variant formula details
		*
		* @param an integer/comma seperated variant formula ids $variant_formula_id. 
		* @param an integer/comma seperated product ids $product_id.
		* @param an integer/comma seperated category ids $category_id. 
		* @param an integer/comma seperated brand ids $brand_id. 
		* @param boolean Active/InActive $status.
		* @param integer $startlimit.
		* @param integer $cnt.
		*
		* @pre not required.
		*
		* @post user variant formula details in associative array.
		* retun an array.
		*/
		function arrGetVariantFormulaDetail($variant_formula_id="",$product_id="",$category_id="",$brand_id="",$status="1",$startlimit="",$cnt=""){
			$keyArr[] = $this->priceKey."_formula";
			if(!empty($variant_formula_id)){
				$keyArr[] = "variant_formula_id_$variant_formula_id";
				$whereClauseArr[]=" variant_formula_id in ($variant_formula_id)";
			}
			if(!empty($brand_id)){
				$keyArr[] = "brand_id_$brand_id";
				$whereClauseArr[]=" brand_id in ($brand_id)";
			}
			if(!empty($product_id)){
				$keyArr[] = "product_id_$product_id";
				$whereClauseArr[]=" product_id in ($product_id)";
			}
			if(!empty($category_id)){
				$keyArr[] = "category_id_$category_id";
				$whereClauseArr[]="category_id in ($category_id)";
			}
			if($status != ""){
				$keyArr[] = "status_$status";
				$whereClauseArr[] = "status = $status";
			}
			if(!empty($startlimit)){
				$keyARr[] = "startlimit_$startlimit";
				$limitArr[] = $startlimit;
			}
			if(!empty($cnt)){
				$keyArr[] = "cnt_$cnt";
				$limitArr[] = $cnt;
			}
			if(sizeof($whereClauseArr) > 0){
				$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
			}
			$key = implode("_",$keyArr);
			if($result = $this->cache->get($key)){return $result;}
			$sql = "select PRICE_VARIANT_FORMULA.* from PRICE_VARIANT_FORMULA $whereClauseStr order by variant_formula_id $orderby $limitStr";
			$result = $this->select($sql);			
			$this->cache->set($key,$result);
			return $result;
		}
		/**
		* @note function is used to get price details
		*
		* @param an integer/comma seperated price variant ids $price_variant_id. 
		* @param an integer/comma seperated product ids $product_id.
		* @param an integer/comma seperated category ids $category_id. 
		* @param an integer/comma seperated brand ids $brand_id. 
		* @param an integer/comma seperated state ids $state_id. 
		* @param an integer/comma seperated city ids $city_id. 
		* @param an integer $default_city. 
		* @param boolean Active/InActive $status.
		* @param integer $startlimit.
		* @param integer $cnt.
		*
		* @pre not required.
		*
		* @post user price details in associative array.
		* retun an array.
		*/
		function arrGetPriceDetails($price_variant_id="1",$product_id="",$category_id="",$brand_id="",$state_id="",$city_id="",$status="1",$startlimit="",$cnt="",$default_city="1"){
			$keyArr[] = $this->priceKey."_frontend";
			$whereClauseArr[] = "PRICE_VARIANT_VALUES.variant_id = PRICE_VARIANT_MASTER.variant_id";
			$whereClauseArr[] = "PRICE_VARIANT_VALUES.state_id = STATE_MASTER.state_id";
			$whereClauseArr[] = "PRICE_VARIANT_VALUES.city_id = CITY_MASTER.city_id";
			if(!empty($price_variant_id)){
				$keyArr[] = "price_variant_id_$price_variant_id";
				$whereClauseArr[] = "PRICE_VARIANT_VALUES.variant_id in($price_variant_id)";
			}
			if(!empty($product_id)){
				$keyArr[] = "product_id_$product_id";
				$whereClauseArr[] = "PRICE_VARIANT_VALUES.product_id in($product_id)";
			}
			if(!empty($category_id)){
				$keyArr[] = "category_id_$category_id";
				$whereClauseArr[] = "PRICE_VARIANT_VALUES.category_id in ($category_id)";
			}
			if(!empty($brand_id)){
				$keyArr[] = "brand_id_$brand_id";
				$whereClauseArr[] = "PRICE_VARIANT_VALUES.brand_id in ($brand_id)";
			}
			if(!empty($state_id)){
				$keyArr[] = "state_id_$state_id";
				$whereClauseArr[] = "PRICE_VARIANT_VALUES.state_id in ($state_id)";
			}
			if(!empty($city_id)){
				$keyArr[] = "city_id_$city_id";
				$whereClauseArr[] = "PRICE_VARIANT_VALUES.city_id in ($city_id)";
			}
			if($status != ""){
				$keyArr[] = "status_$status";
				$whereClauseArr[] = "PRICE_VARIANT_VALUES.status = $status";
			}
			if($default_city != ""){
				$keyArr[] = "default_city_$default_city";
				$whereClauseArr[] = "PRICE_VARIANT_VALUES.default_city = $default_city";
			}
			if(!empty($startlimit)){
				$keyArr[] = "startlimit_$startlimit";
				$limitArr[] = $startlimit;
			}
			if(!empty($cnt)){
				$keyArr[] = "cnt_$cnt";
				$limitArr[] = $cnt;
			}
			if(sizeof($whereClauseArr) > 0){
				$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
			}
			if(sizeof($limitArr) > 0){
				$limitStr = " limit ".implode(",",$limitArr);
			}
			$key = implode("_",$keyArr);
			if($result = $this->cache->get($key)){return $result;}
			$sql = "SELECT PRICE_VARIANT_VALUES.*,PRICE_VARIANT_MASTER.*,STATE_MASTER.*,CITY_MASTER.* FROM PRICE_VARIANT_VALUES,PRICE_VARIANT_MASTER,STATE_MASTER,CITY_MASTER  $whereClauseStr order by variant_value $limitStr";
			$result = $this->select($sql);
			$this->cache->set($key,$result);
			return $result;
		}
		function arrGetPriceDetailsCount($price_variant_id="1",$product_id="",$category_id="",$brand_id="",$state_id="",$city_id="",$status="1",$startlimit="",$cnt="",$default_city=""){
			$keyArr[] = $this->priceKey."_frontend_count";
			$whereClauseArr[] = "PRICE_VARIANT_VALUES.variant_id = PRICE_VARIANT_MASTER.variant_id";
			$whereClauseArr[] = "PRICE_VARIANT_VALUES.state_id = STATE_MASTER.state_id";
			$whereClauseArr[] = "PRICE_VARIANT_VALUES.city_id = CITY_MASTER.city_id";
			if(!empty($price_variant_id)){
				$keyArr[] = "price_variant_id_$price_variant_id";
				$whereClauseArr[] = "PRICE_VARIANT_VALUES.variant_id in($price_variant_id)";
			}
			if(!empty($product_id)){
				$keyArr[] = "product_id_$product_id";
				$whereClauseArr[] = "PRICE_VARIANT_VALUES.product_id in($product_id)";
			}
			if(!empty($category_id)){
				$keyArr[] = "category_id_$category_id";
				$whereClauseArr[] = "PRICE_VARIANT_VALUES.category_id in ($category_id)";
			}
			if(!empty($brand_id)){
				$keyArr[] = "brand_id_$brand_id";
				$whereClauseArr[] = "PRICE_VARIANT_VALUES.brand_id in ($brand_id)";
			}
			if(!empty($state_id)){
				$keyArr[] = "state_id_$state_id";
				$whereClauseArr[] = "PRICE_VARIANT_VALUES.state_id in ($state_id)";
			}
			if(!empty($city_id)){
				$keyArr[] = "city_id_$city_id";
				$whereClauseArr[] = "PRICE_VARIANT_VALUES.city_id in ($city_id)";
			}
			if($status != ""){
				$keyArr[] = "status_$status";
				$whereClauseArr[] = "PRICE_VARIANT_VALUES.status = $status";
			}
			if($default_city != ""){
				$keyArr[] = "default_city_$default_city";
				$whereClauseArr[] = "PRICE_VARIANT_VALUES.default_city = $default_city";
			}
			if(!empty($startlimit)){
				$keyArr[] = "startlimit_$startlimit";
				$limitArr[] = $startlimit;
			}
			if(!empty($cnt)){
				$keyArr[] = "cnt_$cnt";
				$limitArr[] = $cnt;
			}
			if(sizeof($whereClauseArr) > 0){
				$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
			}
			if(sizeof($limitArr) > 0){
				$limitStr = " limit ".implode(",",$limitArr);
			}
			$key = implode("_",$keyArr);
			if($result = $this->cache->get($key)){return $result;}
			$sql = "SELECT count(PRICE_VARIANT_VALUES.variant_id) as vcnt FROM PRICE_VARIANT_VALUES,PRICE_VARIANT_MASTER,STATE_MASTER,CITY_MASTER  $whereClauseStr order by variant_value $limitStr";
			$result = $this->select($sql);
			$this->cache->set($key,$result);
			return $result;
		}
		/**
		* @note function is used to insert variant percentage details into the database.
		* @param an associative array $insert_param.
		* @pre $insert_param must be valid associative array.
		* @post an integer $price_variant.
		* retun integer.
		*/
		function intInsertVariantPercentageDetail($insert_param){
			$insert_param['create_date'] = date('Y-m-d H:i:s');
			$insert_param['update_date'] = date('Y-m-d H:i:s');
			$sSql = $this->getInsertUpdateSql("PRICE_VARIANT_PERCENTAGE",array_keys($insert_param),array_values($insert_param));
			$price_variant = $this->insertUpdate($sSql);
			$this->cache->searchDeleteKeys($this->priceKey."_percentage");
			return $price_percentage;
		}

		/**
		* @note function is used to delete variant percentage detail.
		* @param integer $iRid.
		* @pre $iRid must be non-empty/zero valid integer.
		* @post boolean true/false.
		* return boolean.
		*/
		function boolDeleteVariantPercentageDetail($iRid){
			$sql = "delete from PRICE_VARIANT_PERCENTAGE  where id=$iRid";
			$isDelete = $this->sql_delete_data($sql);
			$this->cache->searchDeleteKeys($this->priceKey."_percentage");
			return $isDelete;
		}

   
		/**
		* @note function is used to get variant percentage  details
		*
		* @param an integer/comma seperated product variant ids $product_variant_id. 
		* @param an integer/comma seperated variant ids $variant_id. 
		* @param an integer/comma seperated product ids/ product ids array $product_id.
		* @param an integer/comma seperated category ids $category_id. 
		* @param an integer/comma seperated brand ids $brand_id. 
		* @param an integer/comma seperated state ids $state_id. 
		* @param an integer/comma seperated city ids $city_id. 
		* @param boolean Active/InActive $status.
		* @param integer $startlimit.
		* @param integer $cnt.
		*
		* @pre not required.
		*
		* @post user variant value details in associative array.
		* retun an array.
		*/
		function arrGetVariantPercentageDetail($id="",$category_id="",$brand_id="",$country_id="",$state_id="",$city_id="",$variant_id="",$status="1",$startlimit="",$cnt=""){
			$keyArr[] = $this->priceKey."_percentage";
			$whereClauseArr[]=" PRICE_VARIANT_PERCENTAGE.variant_id = PRICE_VARIANT_MASTER.variant_id";
			$whereClauseArr[] = "PRICE_VARIANT_PERCENTAGE.city_id = CITY_MASTER.city_id";
			if(!empty($id)){
				$keyArr[] = "id_$id";
				$whereClauseArr[]=" id in ($id)";
			}
			if(!empty($variant_id)){
				$keyArr[] = "variant_id_$variant_id";
				$whereClauseArr[]=" PRICE_VARIANT_PERCENTAGE.variant_id in ($variant_id)";
			}
			if(!empty($category_id)){
				$keyArr[] = "category_id_$category_id";
				$whereClauseArr[]=" PRICE_VARIANT_PERCENTAGE.category_id in ($category_id)";
			}
			if(!empty($brand_id)){
				$keyArr[] = "brand_id_$brand_id";
				$whereClauseArr[]=" PRICE_VARIANT_PERCENTAGE.brand_id in ($brand_id)";
			}
			if(!empty($state_id)){
				$keyArr[] = "state_id_$state_id";
				$whereClauseArr[]=" PRICE_VARIANT_PERCENTAGE.state_id in ($state_id)";
			}
			if(!empty($city_id)){
				$keyArr[] = "city_id_$city_id";
				$whereClauseArr[]=" PRICE_VARIANT_PERCENTAGE.city_id in ($city_id)";
			}
			if($status != ""){
				$keyArr[] = "status_$status";
				$whereClauseArr[] = "PRICE_VARIANT_PERCENTAGE.status = $status";
			}
			if(!empty($startlimit)){
				$keyArr[] = "startlimit_$startlimit";
				$limitArr[] = $startlimit;
			}
			if(!empty($cnt)){
				$keyArr[] = "cnt_$cnt";
				$limitArr[] = $cnt;
			}
			if(sizeof($limitArr) > 0){
				$limitStr = " limit ".implode(" , ",$limitArr);
			}
			if(sizeof($whereClauseArr) > 0){
				$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
			}
			$key = implode("_",$keyArr);
			if($result = $this->cache->get($key)){return $result;}
			$sql = "select PRICE_VARIANT_PERCENTAGE.*,PRICE_VARIANT_MASTER.*,CITY_MASTER.* from PRICE_VARIANT_PERCENTAGE,PRICE_VARIANT_MASTER,CITY_MASTER  $whereClauseStr order by id $limitStr";			
			$result = $this->select($sql);
			$this->cache->set($key,$result);
			return $result;
		}
	}
?>
