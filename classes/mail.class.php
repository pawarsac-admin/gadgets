<?php
	require_once(MAILERPATH."class.phpmailer.php");
	/**
	* @brief class is used to get friends email ids and send mail to those ids.
	* @author rajesh.
	* @version 1.0
	* @date 18-02-2011.
	*/
	class mailer{
		/*** initialize the constructor.*/
		var $invite_type = "";
		function mailer($invite_type){
				$this->invite_type = $invite_type;
		}
		/**
		* @note function used to initialize php mailer class
		**/
		function boolsendMail($from_email,$from_name,$to_email,$to_name,$subject,$message,$replyto='',$attachment_path='',$attachement_name=''){
			$mail = new PHPMailer();
			$mail->IsSMTP();                                      // set mailer to use SMTP
			$mail->Host = MAILER_HOST_ADDRESS;  // specify main and backup server
			$mail->SMTPAuth = false;     // turn on SMTP authentication
			$mail->Username = MAILER_USER_NAME;  // SMTP username
			$mail->Password = MAILER_PWD; // SMTP password

			$mail->From = $from_email;
			$mail->FromName = $from_name;
			if(!empty($to_name)){
					$mail->AddAddress($to_email, $to_name);
			}else{
					$mail->AddAddress($to_email);                  // name is optional
			}
			if($replyto) $mail->AddReplyTo($replyto, $from_name);
			//$mail->Sender = MODULE_REPLY_TO;
			$mail->Sender = $from_email;

			$mail->WordWrap = 50;                                 // set word wrap to 50 characters
			/*if(empty($attachement_name)){
					$mail->AddAttachment($attachment_path);         // add attachments
			}else{
					$mail->AddAttachment($attachment_path, $attachement_name);    // optional name
			}*/
			$mail->IsHTML(true);                                  // set email format to HTML

			$mail->Subject = $subject;
			$mail->Body    = $message;
			//$mail->AltBody = $message;
			if(!$mail->Send()){
					return "0";
			}else{
					return "1";
			}
		}
		/**
		* @brief function used to send invitation.
		* @param an array request_array.
		* @pre request_array must be non-empty associate array.
		* @post 0/1.
		* return boolean.
		*/
		function boolSendShare($request_array){
			
			list($emails,$message_content,$subject,$from_email,$from_name,$url,$replyto) = array($request_array['emails'],$request_array['message'],$request_array['subject'],$request_array['from_email'],$request_array['from_name'],$request_array['url'],$request_array['reply-to']);
			$emailIds = explode(",",$emails);
			if(is_array($emailIds)){
					foreach($emailIds as $to_email){
							$to_name = $this->get_user_name_from_email($to_email);
							$tmp_message_content = $this->get_html_msg($url,$to_name,$from_name,$message_content);
							$sendmail = $this->boolsendMail($from_email,$from_name,$to_email,$to_name,$subject,$tmp_message_content, $replyto);
					}
			}else{
					$to_email = $emailIds;
					$to_name = $this->get_user_name_from_email($to_email);
					
					$message_content = $this->get_html_msg($url,'',$from_name,$message_content);
					$sendmail = $this->boolsendMail($from_email,$from_name,$to_email,$to_name,$subject,$message_content);
			}
			
			
			return $sendmail;
		}

		/**
		* @note function used to read html file and replace its contents.
		* @param integer item_id .
		* @param string to_name to whom mail is send.
		* @param string from_name.
		* @param string user entered msg.
		* @param string item_title.
		* @param string item_keywords.
		* @param string item_description.
		* @pre item_id must be non-empty/non-zero integer.file_path,to_name,from_name and reasonmsg must be non-emtpy string.
		* @post string html msg.
		* return string.
		*/
		function get_html_msg($url,$to_name,$from_name,$reasonmsg){
			$url = "<a href='$url'>$url</a>";
			if($this->invite_type == 'share'){
					//send message for showcase item.
					$htmlStr = file_get_contents(BASEPATH.'emailtemplate/share.html');

			}elseif($this->invite_type == 'newsletter'){
					//send message for showcase item.
					$htmlStr = file_get_contents(BASEPATH.'emailtemplate/invite_torate_mail.html');
			}

			$dateStr = date('d M, Y');
			
			$replaceArr = array("#replace_username#"=>$to_name,"#replace_sendername#"=>$from_name,"#message#"=>nl2br($reasonmsg),'#IMAGE_URL#'=>IMAGE_URL,'#CSS_URL#'=>CSS_URL,"#current_date#"=>date('Y-m-d:H:i:s'),"#imgalt#"=>'oncars.in',"#URL#"=>WEB_URL,"#share_url#"=>$url);
			//print "<pre>";print_r($replaceArr);
                     $message = strtr($htmlStr,$replaceArr);
			
			return $message;
		}
		/**
		* @note function used to get username from email id.
		* @param string emailid.
		* @pre email id must be valid string.
		* @post string username.
		* return string.
		*/
		function get_user_name_from_email($email){
			$pattern = '/([^@]+)/';
			if(preg_match_all($pattern,$email,$matches,PREG_SET_ORDER)){
					$username = $matches[0][1];
			}
			return $username;
		}


		/**
		* @brief function used to send invitation.
		* @param an array request_array.
		* @pre request_array must be non-empty associate array.
		* @post 0/1.
		* return boolean.
		*/
		function boolSendContacts($request_array){
			
			list($emails,$message_content,$subject,$from_email,$from_name,$Inqsubject,$replyto) = array($request_array['emails'],$request_array['message'],$request_array['subject'],$request_array['from_email'],$request_array['from_name'],$request_array['Inqsubject'],$request_array['reply-to']);
			$emailIds = explode(",",$emails);
			if(is_array($emailIds)){
					foreach($emailIds as $to_email){
							$to_name = $this->get_user_name_from_email($to_email);
							$tmp_message_content = $this->get_html_msgContacts($Inqsubject,$to_name,$from_name,$message_content,$from_email);
							$sendmail = $this->boolsendMail($from_email,$from_name,$to_email,$to_name,$subject,$tmp_message_content, $replyto);
					}
			}else{
					$to_email = $emailIds;
					$to_name = $this->get_user_name_from_email($to_email);
					
					$message_content = $this->get_html_msgContacts($Inqsubject,'',$from_name,$message_content,$from_email);
					$sendmail = $this->boolsendMail($from_email,$from_name,$to_email,$to_name,$subject,$message_content);
			}
			
			
			return $sendmail;
		}


		function get_html_msgContacts($Inqsubject,$to_name,$from_name,$reasonmsg,$from_email){
			
			if($this->invite_type == 'contacts'){
					//send message for showcase item.
					$htmlStr = file_get_contents(BASEPATH.'emailtemplate/contacts.html');

			}

			$dateStr = date('d M, Y');
			
			$replaceArr = array("#replace_username#"=>$to_name,"#replace_sendername#"=>$from_name,"#message#"=>nl2br($reasonmsg),'#IMAGE_URL#'=>IMAGE_URL,'#CSS_URL#'=>CSS_URL,"#current_date#"=>date('Y-m-d:H:i:s'),"#imgalt#"=>'oncars.in',"#URL#"=>WEB_URL,"#inSubject#"=>$Inqsubject,"#FROM_EMAI#"=>$from_email);
			//print "<pre>";print_r($replaceArr);
                     $message = strtr($htmlStr,$replaceArr);
			
			return $message;
		}

	}
?>
