<?php
// include
require_once('./include/config.php');
require_once(CLASSPATH.'DbConn.php');
require_once(CLASSPATH.'brand.class.php');
require_once(CLASSPATH.'category.class.php');
require_once(CLASSPATH.'product.class.php');
require_once(CLASSPATH.'user_review.class.php');
require_once(CLASSPATH.'videos.class.php');
require_once(CLASSPATH."citystate.class.php");
require_once(CLASSPATH.'Utility.php');

// object declaration
$dbconn = new DbConn;
$oBrand = new BrandManagement;
$category = new CategoryManagement;
$oProduct = new ProductManagement;
$userreview = new USERREVIEW;
$videoGallery = new videos();
$oCityState = new citystate;

//print "<pre>"; print_r($_REQUEST);
$category_id = $_REQUEST['catid'] ? $_REQUEST['catid'] : SITE_CATEGORY_ID;

//Brand List Starts
unset($result);
if(!empty($category_id)){
        $result = $oBrand->arrGetBrandDetails("",$category_id,1);
}
$cnt = sizeof($result);
$xml = "<BRAND_MASTER>";
$xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
for($i=0;$i<$cnt;$i++){
        $brand_id = $result[$i]['brand_id'];
        $status = $result[$i]['status'];
        $categoryid = $result[$i]['category_id'];
        $result[$i]['brand_name'] = html_entity_decode($result[$i]['brand_name'],ENT_QUOTES,'UTF-8');
        $result[$i] = array_change_key_case($result[$i],CASE_UPPER);
        $xml .= "<BRAND_MASTER_DATA>";
        foreach($result[$i] as $k=>$v){
                $xml .= "<$k><![CDATA[$v]]></$k>";
        }
        $xml .= "</BRAND_MASTER_DATA>";
}
$xml .= "</BRAND_MASTER>";
//Brand List Ends

//Most Reviewd start
unset($most_reviewed_result);
$most_reviewed_result = $userreview->getMostReviewedCount("","","","","","",$category_id,"","","1","","","");
$tot_cnt = sizeof($most_reviewed_result);
$xml .="<MOST_REVIEWED_MASTER>";
$xml .= "<COUNT><![CDATA[$tot_cnt]]></COUNT>";
$j=1;
for($i=0;$i<$tot_cnt;$i++){
	$brand_id = $most_reviewed_result[$i]["brand_id"];
	$product_info_id = $most_reviewed_result[$i]["product_info_id"];

	$review_cnt = $userreview->arrGetUserReviewDetailsCount("","","","","","",$category_id,$product_info_id,"","1");

	$brand_name="";
        if(!empty($brand_id)){
                $brand_result = $oBrand->arrGetBrandDetails($brand_id,$category_id,"1");
                if(is_array($brand_result)){
                        $brand_name = $brand_result["0"]["brand_name"];
                }
        }

        $product_info_name = "";
        if(!empty($product_info_id)){
                $product_info_result = $oProduct->arrGetProductNameInfo($product_info_id,$category_id,"","","1","","");

                if(is_array($product_info_result)){
                        $product_info_name = $product_info_result[0]["product_info_name"];
                        $image_path = $product_info_result[0]["image_path"];
			$img_media_id = $product_info_result[0]["img_media_id"];
                        if(!empty($image_path)){
				if($i == 0){
			                $image_path = resizeImagePath($image_path,"160X120",$aModuleImageResize,$img_media_id);
				}else{
					$image_path = resizeImagePath($image_path,"87X65",$aModuleImageResize,$img_media_id);
				}
                                $image_path = $image_path ? CENTRAL_IMAGE_URL.$image_path : '';
                        }
                }
        }
	//start user rating
	$reviewsresult = $userreview->arrGetAdminOverallGrade($category_id,$brand_id,'0',$product_name_id);
	$reviewscnt = sizeof($reviewsresult);
	$overallcnt = 0;
	$overallavg = round($reviewsresult[0]['overallgrade']);
	if($reviewscnt <= 0){
	        $reviewsresult = $userreview->arrGetOverallGrade($category_id,$brand_id,'0',$product_name_id);
	        $overallavg = round($reviewsresult[0]['overallavg']);
	        $overallcnt = $reviewsresult[0]['totaloverallcnt'] ? $reviewsresult[0]['totaloverallcnt'] : 0;
	        $totalcnt = $userreview->arrGetUserReviewDetailsCount("","","","","",$brand_id,$category_id,$product_name_id,"","1");
	}

	$product_brand_name = html_entity_decode($brand_name,ENT_QUOTES,'UTF-8');
	$product_brand_name = removeSlashes($product_brand_name);
	$product_brand_name = seo_title_replace($product_brand_name);

	$product_link_name = $product_brand_name."-".$product_info_name;
	$product_link_name = html_entity_decode($product_link_name,ENT_QUOTES,'UTF-8');
	$product_link_name = removeSlashes($product_link_name);
	$product_link_name = seo_title_replace($product_link_name);
	unset($seoTitleArr);
	$seoTitleArr[] = SEO_WEB_URL;
	$seoTitleArr[] = $product_brand_name."-cars";
	$seoTitleArr[] = $product_link_name;
	$seoTitleArr[] = "reviews-ratings";
	$seoTitleArr[] = SEO_CARS_MODEL_REVIEWS;
	$seoTitleArr[] = $product_info_name;
	$seoTitleArr[] = $product_info_id;
	$seo_model_url = implode("/",$seoTitleArr);
	unset($seoTitleArr);
	$html = "";
	for($grade=1;$grade<=5;$grade++){
	        if($grade <= $overallavg){
	                $html .= '<img src="'.IMAGE_URL.'spacer.gif" class="vsblStr"/>';
	        }else{
	                $html .= '<img src="'.IMAGE_URL.'spacer.gif" class="dsblStr"/>';
	        }
	}
	//End user rating
	

	$result[$i]["brand_id"] = $brand_id;
        $result[$i]["product_info_id"] = $product_info_id;
        $result[$i]["brand_name"] = $brand_name;
        $result[$i]["product_info_name"] = $product_info_name;
        $result[$i]["product_image_path"] = $image_path;
	$result[$i]["display_name"] = $brand_name." ".$product_info_name;
	$result[$i]["review_count"] = $review_cnt;
	$result[$i]["overall_avg_html"] = $html;
	$result[$i]["overall_avg_cnt"] = $overallavg;
	$result[$i]["overall_total_cnt"] = $overallcnt;
	$result[$i]["overall_cnt"] = $totalcnt;
	$result[$i]["seo_model_rating_page_url"] = $seo_model_url;

        $result[$i] = array_change_key_case($result[$i],CASE_UPPER);
        //print"<pre>";print_r($result[$i]);print"</pre>";
        $xml .="<MOST_REVIEWED_MASTER_DATA>";
        foreach($result[$i] as $k => $v){
                $xml .= "<$k><![CDATA[$v]]></$k>";
        }
        $xml .="</MOST_REVIEWED_MASTER_DATA>";
	$j++;
        if($j=="11"){break;}

}
$xml .="</MOST_REVIEWED_MASTER>";

//Most Reviewd End

//Highest Rated start
//$cnt = sizeo
$new_result = Array();
unset($result);
$user_review_result = $userreview->getHighestRatedProduct($category_id,"","","","1","");
//echo "DSDSDSDSD---"; print"<pre>";print_r($user_review_result);print"</pre>"; die();
$cnt = sizeof($user_review_result);
for($i=0;$i<$cnt;$i++){
       	$overallcnt = 0;
        $product_info_id = $user_review_result[$i]['product_info_id'];
	$user_review_overallavg = $user_review_result[$i]['overallavg'];
	$user_review_totaloverallcnt = $user_review_result[$i]['totaloverallcnt'];
	$overallavg = round($user_review_overallavg);
	$overallcnt = $user_review_totaloverallcnt;

	$brand_name="";
	$product_info_name = "";
	if(!empty($product_info_id)){
		$product_info_result = $oProduct->arrGetProductNameInfo($product_info_id,$category_id,"","","1","","");
		if(is_array($product_info_result)){
	                $product_info_name = $product_info_result[0]["product_info_name"];
	                $brand_id = $product_info_result[0]["brand_id"];
			$image_path = $product_info_result[0]["image_path"];
			$img_media_id = $product_info_result[0]["img_media_id"];
			if(!empty($brand_id)){
			        $brand_result = $oBrand->arrGetBrandDetails($brand_id,$category_id,"1");
			        if(is_array($brand_result)){
				        $brand_name = $brand_result["0"]["brand_name"];
			        }
			}
		}	
	}

	//start user rating
       	/*$reviewsresult = $userreview->arrGetAdminOverallGrade($category_id,$brand_id,'0',$product_info_id);
        $reviewscnt = sizeof($reviewsresult);
       	if($reviewscnt <= 0){
               	//$reviewsresult = $userreview->arrGetOverallGrade($category_id,$brand_id,'0',$product_info_id);
                //$overallavg = round($reviewsresult[0]['overallavg']);
       	        //$overallcnt = $reviewsresult[0]['totaloverallcnt'] ? $reviewsresult[0]['totaloverallcnt'] : 0;
               	$totalcnt = $userreview->arrGetUserReviewDetailsCount("","","","","",$brand_id,$category_id,$product_info_id);
        }*/
	

	$product_brand_name = html_entity_decode($brand_name,ENT_QUOTES,'UTF-8');
	$product_brand_name = removeSlashes($product_brand_name);
	$product_brand_name = seo_title_replace($product_brand_name);

	$product_link_name = $product_brand_name."-".$product_info_name;
	$product_link_name = html_entity_decode($product_link_name,ENT_QUOTES,'UTF-8');
	$product_link_name = removeSlashes($product_link_name);
	$product_link_name = seo_title_replace($product_link_name);
	unset($seoTitleArr); 
	$seoTitleArr[] = SEO_WEB_URL;
	$seoTitleArr[] = $product_brand_name."-cars";
	$seoTitleArr[] = $product_link_name;
	$seoTitleArr[] = "reviews-ratings";
	$seoTitleArr[] = SEO_CARS_MODEL_REVIEWS;
	$seoTitleArr[] = $product_info_name;
	$seoTitleArr[] = $product_info_id;
	$seo_model_url = implode("/",$seoTitleArr);
	unset($seoTitleArr);
	$html = "";
	for($grade=1;$grade<=5;$grade++){
		if($grade <= $overallavg){
			$html .= '<img src="'.IMAGE_URL.'spacer.gif" class="vsblStr"/>';
		}else{
			$html .= '<img src="'.IMAGE_URL.'spacer.gif" class="dsblStr"/>';
		}
	}
        $review_cnt = $userreview->arrGetUserReviewDetailsCount("","","","","","",$category_id,$product_info_id,"","1");
	//End user rating
	//if($overallavg > 0){
		$new_result[$i]["brand_id"] = $brand_id;
		$new_result[$i]["product_info_id"] = $product_info_id;
		$new_result[$i]["brand_name"] = $brand_name;
		$new_result[$i]["product_info_name"] = $product_info_name;
		$new_result[$i]["review_count"] = $review_cnt;
		$new_result[$i]["product_image_path"] = $image_path;
		$new_result[$i]["img_media_id"] = $img_media_id;
		$new_result[$i]["display_name"] = $brand_name." ".$product_info_name;
		$new_result[$i]["totaloverallcnt"] = $totaloverallcnt;
		$new_result[$i]["overallavg"] = $overallavg;
		$new_result[$i]["overall_avg_html"] = $html;
		$new_result[$i]["overall_avg_cnt"] = $overallavg;
		$new_result[$i]["overall_total_cnt"] = $overallcnt;
		$new_result[$i]["overall_cnt"] = $totalcnt;
		$new_result[$i]["seo_model_rating_page_url"] = $seo_model_url;
	//}

}
unset($result);
//$result = $videoGallery->sort2d($new_result, "overallavg", "desc", "", "");
$result = $new_result;
$cnt=sizeof($result);
$xml .="<HIGHEST_RATED_MASTER>";
$xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
//Display rest of the highest rated elements in the array start

if($cnt >= 10){$cnt=10;}
for($i=0;$i<$cnt;$i++){
	$product_image_path = $result[$i]["product_image_path"];
	$img_media_id = $result[$i]["img_media_id"];
        if(!empty($product_image_path)){
		if($i == 0){
                	$product_image_path = resizeImagePath($product_image_path,"160X120",$aModuleImageResize,$img_media_id);
		}else{
                	$product_image_path = resizeImagePath($product_image_path,"87X65",$aModuleImageResize,$img_media_id);
		}
                $product_image_path = $product_image_path ? CENTRAL_IMAGE_URL.$product_image_path : '';
		$result[$i]["product_image_path"] = $product_image_path;
        }

	$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
	//print"<pre>";print_r($result[$i]);print"</pre>";
        $xml .="<HIGHEST_RATED_MASTER_DATA>";
        foreach($result[$i] as $k => $v){
		$xml .= "<$k><![CDATA[$v]]></$k>";
        }
        $xml .="</HIGHEST_RATED_MASTER_DATA>";

}
//Display rest of the highest rated elements in the array start

$xml .="</HIGHEST_RATED_MASTER>";
//Highest Rated End

//Recently Reviewed start
$tot_cnt =  $userreview->arrGetUserReviewDetailsCount("","","","","","",$category_id,"","","1");
unset($user_review_result);
$user_review_result =  $userreview->arrGetUserReviewDetails("","","","","","",$category_id,"","","1","","","");
$cnt = sizeof($user_review_result);
$product_info_arr = Array();
$xml .="<RECENTLY_REVIEWED_MASTER>";
$xml .= "<COUNT><![CDATA[$tot_cnt]]></COUNT>";
$j=1;
for($i=0;$i<$cnt;$i++){
	$user_review_id = $user_review_result[$i]["user_review_id"];
	$uid = $user_review_result[$i]["uid"];
	$title = $user_review_result[$i]["title"];
	$user_name = $user_review_result[$i]["user_name"];
	$location = $user_review_result[$i]["location"];
	$brand_id = $user_review_result[$i]["brand_id"];
	$product_info_id = $user_review_result[$i]["product_info_id"];
	$product_id = $user_review_result[$i]["product_id"];

	if (!in_array($product_info_id, $product_info_arr)) {
		$brand_name="";
	        if(!empty($brand_id)){
        	        $brand_result = $oBrand->arrGetBrandDetails($brand_id,$category_id,"1");
			if(is_array($brand_result)){
                	        $brand_name = $brand_result["0"]["brand_name"];
        	        }
	        }

		$product_info_name = "";
		if(!empty($product_info_id)){
			$product_info_result = $oProduct->arrGetProductNameInfo($product_info_id,$category_id,"","","1","","");
			if(is_array($product_info_result)){
				$product_info_name = $product_info_result[0]["product_info_name"];
				$image_path = $product_info_result[0]["image_path"];
				$img_media_id = $product_info_result[0]["img_media_id"];
				if(!empty($image_path)){
					if($i == 0){
                                        	$image_path = resizeImagePath($image_path,"160X120",$aModuleImageResize,$img_media_id);
                	                }else{
        	                                $image_path = resizeImagePath($image_path,"87X65",$aModuleImageResize,$img_media_id);
	                                }
                        	        $image_path = $image_path ? CENTRAL_IMAGE_URL.$image_path : '';
				}
			}
		}


		$review_cnt = $userreview->arrGetUserReviewDetailsCount("","","","","","",$category_id,$product_info_id,"","1");

		//start user rating
	        $reviewsresult = $userreview->arrGetAdminOverallGrade($category_id,$brand_id,'0',$product_name_id);
        	$reviewscnt = sizeof($reviewsresult);
	        $overallcnt = 0;
        	$overallavg = round($reviewsresult[0]['overallgrade']);
	        if($reviewscnt <= 0){	
        	        $reviewsresult = $userreview->arrGetOverallGrade($category_id,$brand_id,'0',$product_name_id);
	                $overallavg = round($reviewsresult[0]['overallavg']);
                	$overallcnt = $reviewsresult[0]['totaloverallcnt'] ? $reviewsresult[0]['totaloverallcnt'] : 0;
        	        $totalcnt = $userreview->arrGetUserReviewDetailsCount("","","","","",$brand_id,$category_id,$product_name_id,"","1");
	        }

	        $product_brand_name = html_entity_decode($brand_name,ENT_QUOTES,'UTF-8');
        	$product_brand_name = removeSlashes($product_brand_name);
	        $product_brand_name = seo_title_replace($product_brand_name);

        	$product_link_name = $product_brand_name."-".$product_info_name;
	        $product_link_name = html_entity_decode($product_link_name,ENT_QUOTES,'UTF-8');
        	$product_link_name = removeSlashes($product_link_name);
	        $product_link_name = seo_title_replace($product_link_name);
        	unset($seoTitleArr); 
	        $seoTitleArr[] = SEO_WEB_URL;
        	$seoTitleArr[] = $product_brand_name."-cars";
	        $seoTitleArr[] = $product_link_name;
        	$seoTitleArr[] = "reviews-ratings";
	        $seoTitleArr[] = SEO_CARS_MODEL_REVIEWS;
        	$seoTitleArr[] = $product_info_name;
	        $seoTitleArr[] = $product_info_id;
        	$seo_model_url = implode("/",$seoTitleArr);
	        unset($seoTitleArr);
		$html = "";
	        for($grade=1;$grade<=5;$grade++){
                	if($grade <= $overallavg){
        	                $html .= '<img src="'.IMAGE_URL.'spacer.gif" class="vsblStr"/>';
	                }else{
                        	$html .= '<img src="'.IMAGE_URL.'spacer.gif" class="dsblStr"/>';
                	}
        	}
	        //End user rating

		$user_review_result[$i]["brand_id"] = $brand_id;
		$user_review_result[$i]["product_info_id"] = $product_info_id;
		$user_review_result[$i]["product_id"] = $product_id;
		$user_review_result[$i]["brand_name"] = $brand_name;
		$user_review_result[$i]["product_info_name"] = $product_info_name;
		$user_review_result[$i]["product_image_path"] = $image_path;
		$user_review_result[$i]["display_name"] = $brand_name." ".$product_info_name;
        	$user_review_result[$i]["review_count"] = $review_cnt;
		$user_review_result[$i]["overall_avg_html"] = $html;
        	$user_review_result[$i]["overall_avg_cnt"] = $overallavg;
	        $user_review_result[$i]["overall_total_cnt"] = $overallcnt;
        	$user_review_result[$i]["overall_cnt"] = $totalcnt;
	        $user_review_result[$i]["seo_model_rating_page_url"] = $seo_model_url;
	

		$user_review_result[$i] = array_change_key_case($user_review_result[$i],CASE_UPPER);
		$xml .="<RECENTLY_REVIEWED_MASTER_DATA>";
	        foreach($user_review_result[$i] as $k => $v){
			$xml .= "<$k><![CDATA[$v]]></$k>";
        	}
		$xml .="</RECENTLY_REVIEWED_MASTER_DATA>";
		array_push($product_info_arr,$product_info_id);
		$j++;
		if($j=="11"){break;}
	}
}
$xml .="</RECENTLY_REVIEWED_MASTER>";
//Recently Reviewed End


$breadcrumb = CATEGORY_HOME." User Reviews";
$config_details = get_config_details();

$strXML .= "<XML>";
$strXML .= $config_details;
$strXML .= "<TYPE><![CDATA[$type]]></TYPE>";
$strXML .= "<BREAD_CRUMB><![CDATA[$breadcrumb]]></BREAD_CRUMB>";
$strXML .= "<SEO_WEB_URL><![CDATA[".SEO_WEB_URL."]]></SEO_WEB_URL>";
$strXML .= "<SEO_AUTO_NEWS><![CDATA[".SEO_AUTO_NEWS."]]></SEO_AUTO_NEWS>";
$strXML .= "<SEO_TITLE><![CDATA[$seo_title]]></SEO_TITLE>";
$strXML .= "<SEO_TAGS><![CDATA[$seo_keywords]]></SEO_TAGS>";
$strXML .= "<SEO_DESC><![CDATA[$seo_desc]]></SEO_DESC>";
$strXML .= "<MODEL_BRAND_NAME><![CDATA[$sModelBrandName]]></MODEL_BRAND_NAME>";
$strXML .= "<MODEL_NAME><![CDATA[$product_info_name]]></MODEL_NAME>";
$strXML .= "<STARTLIMIT><![CDATA[$startlimit]]></STARTLIMIT>";
$strXML .= "<CNT><![CDATA[$limitcnt]]></CNT>";
$strXML.="<MBREPLYLIST>";
$strXML.="<MBTID><![CDATA[".$iTId."]]></MBTID>";
$strXML.="<CPAGE><![CDATA[".$page."]]></CPAGE>";
$strXML.="<SERVICEID><![CDATA[".SERVICEID."]]></SERVICEID>";
$strXML.="<CATEGORY><![CDATA[".$category_id."]]></CATEGORY>";
$strXML.="</MBREPLYLIST>";
$strXML .= $xml;
	
$strXML .= "<OC_ROS_BOTTOM_NORTH_728x90><![CDATA[OC_ROS_Bottom_North_728x90]]></OC_ROS_BOTTOM_NORTH_728x90>";
$strXML .= "<OC_ROS_TOP_RHS_LREC_300x250_1><![CDATA[OC_ROS_Top_RHS_Lrec_300x250_1]]></OC_ROS_TOP_RHS_LREC_300x250_1>";
$most_recent_one_result_list = $videoGallery->getarrMostRecentVideosHeaderTabLink($category_id,"0","1",$array_result);
$strXML.="<MOST_RECENT_VIDEO_HEADER_LINK><![CDATA[".$most_recent_one_result_list."]]></MOST_RECENT_VIDEO_HEADER_LINK>";	
$strXML .= "</XML>";

$strXML = mb_convert_encoding($strXML, "UTF-8");

if($_GET['debug']==1) { header('Content-type: text/xml');echo $strXML;exit; }
$doc = new DOMDocument();
$doc->loadXML($strXML);
$doc->saveXML();

$xslt = new xsltProcessor;
$xsl = DOMDocument::load('xsl/user_review_rating.xsl');

$xslt->importStylesheet($xsl);
print $xslt->transformToXML($doc);
?>
