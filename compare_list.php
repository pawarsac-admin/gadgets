<?php
	require_once('./include/config.php');
	require_once('./include/config_search.php');
	require_once(CLASSPATH.'DbConn.php');
	require_once(CLASSPATH.'product.class.php');
	require_once(CLASSPATH.'Utility.php');
	require_once(CLASSPATH.'kgPager.class.php');
	require_once(CLASSPATH.'topstories.class.php');

	$dbconn			= new DbConn;
	$product		= new ProductManagement;
	$oTopStories	= new TopStories;

	$group_id		= 1;        // Top Rated Phones
	$category_id	= SITE_CATEGORY_ID;
	define("PERPAGE", 20);
	$pagenum		= $_REQUEST['pagenum'];
	if(empty($pagenum))	$pagenum = 1;
	$start			= ($pagenum - 1) * PERPAGE ;
	$end			= PERPAGE ;

	$debug			= (empty($_REQUEST['debug']))? '' : $_REQUEST['debug'] ; 

	$product_ids	= array();
	$arrPricePID	= $product->arrGetProducts( '' , 'product_id', 'ASC' ); //product_full_name , product_id
	$noOfProducts	= count($arrPricePID);
	
	//print_r($arrPricePID);die;

	$maxRecords		= $start + $end ;
	$i				= 0;
	foreach($arrPricePID as $key => $val ){
		foreach($arrPricePID as $key1 => $val1 ){
			if($val['product_id'] != $val1['product_id']){
				
				$strId			   = $val['product_id'].",".$val1['product_id'] ;
				$strName		   = "<a href='".WEB_URL."Compare-mobiles/1/". $strId . "/1'>" . $val['product_full_name'] . " Vs " . $val1['product_full_name'] . "</a>";
				$arrProductsList[] = $strName;
				$i++;
				if($i > $maxRecords ) break 2;

			}
		}
	}

	//print_r($arrProductsList);die;

	$totalCount				= $noOfProducts  * ( $noOfProducts - 1 );
	$arrFinalProductsList	= array_slice($arrProductsList,$start, $end);
	$listxml .= '<RECORDS>';
	foreach($arrFinalProductsList as $key => $val ){
		$listxml .= '<RECORD><![CDATA['. $val . ']]></RECORD>';
	}
	$listxml .= '</RECORDS>';
	

	//print_r($arrFinalProductsList);die;

	$pagename			= WEB_URL . 'Compare-all' ;

	$total_records		= $totalCount;	
	$scroll_page		= 5; 
	$per_page			= PERPAGE; 
	$current_page		= $pagenum;
	$pager_url			= $pagename. '/page-';
	$inactive_page_tag	= 'id="current_page"';
	$previous_page_text = '&lt;';
	$next_page_text		= '&gt;';
	$first_page_text	= '&lt;&lt;';
	$last_page_text		= '&gt;&gt;';

	$kgPagerOBJ = & new kgPager();
	$kgPagerOBJ -> pager_set($pager_url, $total_records, $scroll_page, $per_page, $current_page, $inactive_page_tag, $previous_page_text, $next_page_text, $first_page_text, $last_page_text);

	$strPage  = '<p id="pager_links">';
	$strPage .= $kgPagerOBJ -> first_page;
	$strPage .= $kgPagerOBJ -> previous_page;
	$strPage .= $kgPagerOBJ -> page_links;
	$strPage .= $kgPagerOBJ -> next_page;
	$strPage .= $kgPagerOBJ -> last_page;
	$strPage .= '</p>';
	$pageNavStr	= $strPage;


	if( $debug == 1 ){
		echo "<br/>pagenum : " , $pagenum;
		echo "<br/>start : " , $start;
		echo "<br/>end : " , $end;
		echo "<br/>maxRecords : " , $maxRecords;
		echo "<br/>noOfProducts : " , $noOfProducts;
		echo "<br/>totalCombinationCount : " , $totalCount;
		echo "<br/>pageNavStr : " , $pageNavStr;
		echo "<br/>";
		print_r($arrFinalProductsList);
	}

	$config_details		= get_config_details();
	$topStoriesXML		= $oTopStories->getStoriesByGroupXML( $group_id , $dbconn );

	$strXML = "<XML>";
	$strXML .= "<PAGE_NAME><![CDATA[".$_SERVER['SCRIPT_URI']."]]></PAGE_NAME>";
	$strXML .= "<SEO_JS><![CDATA[$seo_js]]></SEO_JS>";
	$strXML .= "<SEO_TITLE><![CDATA[".$SEO_TITLE."]]></SEO_TITLE>";
	$strXML .= "<SEO_DESC><![CDATA[".$SEO_DESC."]]></SEO_DESC>";
	$strXML .= "<SEO_TAGS><![CDATA[".$SEO_TAGS."]]></SEO_TAGS>";
	$strXML .= "<SEO_URL><![CDATA[$seo_url]]></SEO_URL>";
	$strXML .= $config_details;
	$strXML .= $listxml;
	$strXML .= "<TOP_RATED>".$topStoriesXML."</TOP_RATED>";

	$strXML .= "<PAGER><![CDATA[$pageNavStr]]></PAGER>";
	$strXML .= "</XML>";

	if($_REQUEST['debug']==2){ header('Content-type: text/xml');echo $strXML;exit;}
	$doc = new DOMDocument();
	$doc->loadXML($strXML);
	$doc->saveXML();
	$xslt = new xsltProcessor;
	$xsl = DOMDocument::load('xsl/compare_list.xsl');
	$xslt->importStylesheet($xsl);
	print $xslt->transformToXML($doc);
?>