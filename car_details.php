<?php
	ini_set("display_errors","1");
	require_once('./include/config.php');
	require_once(CLASSPATH.'DbConn.php');
	require_once(CLASSPATH.'brand.class.php');
	require_once(CLASSPATH.'category.class.php');
	require_once(CLASSPATH.'pivot.class.php');
	require_once(CLASSPATH.'feature.class.php');
	require_once(CLASSPATH.'product.class.php');
	require_once(CLASSPATH.'price.class.php');
	require_once(CLASSPATH.'wallpaper.class.php');
	require_once(CLASSPATH.'reviews.class.php');
	require_once(CLASSPATH."campus_discussion.class.php");
	require_once(CLASSPATH.'pager.class.php');
	require_once(CLASSPATH.'user.class.php');
	require_once(CLASSPATH.'user_review.class.php');
	require_once(CLASSPATH.'report.class.php');
	require_once(CLASSPATH.'videos.class.php');
	require_once(CLASSPATH.'Utility.php');

	//print_r($_SERVER); die();
	//print"<pre>";print_r($_REQUEST);print"</pre>";//die();
	//print"<pre>";print_r($_COOKIE);print"</pre>";
	$dbconn		=	new DbConn;
	$oBrand		= 	new BrandManagement;
	$category 	= 	new CategoryManagement;
	$oPivot 	= 	new PivotManagement;
	$oFeature 	= 	new FeatureManagement;
	$oProduct 	= 	new ProductManagement;
	$oPrice 	=	new price;
	$oWallpapers =	new Wallpapers;
	$oReview 	 =	new reviews;
	$oCampusDiscussion	=  new campus_discussion();
	$ObjPager	=  new Pager();
	$obj_user   = new user;
	$userreview = new USERREVIEW;
	$report = new report;
    $videoGallery = new videos();

	$category_id = $_REQUEST['catid'] ? $_REQUEST['catid'] : SITE_CATEGORY_ID;
	$product_id = $_REQUEST['pid'];
	$rev_product_id = $_REQUEST['pid'];
 	$tab=$_REQUEST['tab'];
	$tab_id = $_REQUEST['fid'];
	if(!empty($category_id)){
		$price_data = $oPrice->arrGetVariantDetail("",$category_id,"1");
		$variant_id = $price_data[0]['variant_id'];
	}
	if(!empty($selected_city_id) && !empty($product_id)){
		$rProductDetail = $oProduct->arrGetProductDetails($product_id,$category_id,"",'1',"","","1","","","","","",$selected_city_id);
	}
	
	$cnt_product_detail = sizeof($rProductDetail);
	if($cnt_product_detail == 0){
		$selected_city_id = "";
        $selected_city_name = "";
	}
	
	if(!empty($product_id)){
		if(!empty($selected_city_id)){
			$aProductDetail = $oProduct->arrGetProductDetails($product_id,$category_id,"",'1',"","","1","","","","","",$selected_city_id);
		}else{
			$aProductDetail = $oProduct->arrGetProductDetails($product_id,$category_id,"",'1',"","","1","","","1");
		}
	}

	//start code added by rajesh on dated 02-06-2011 for expert rating and graph.
		$rating_brand_id = $aProductDetail[0]['brand_id'];
		$product_info_name = $aProductDetail[0]['product_name'];

		$result = $oProduct->arrGetProductNameInfo("",$category_id,$rating_brand_id,$product_info_name);
		$product_name_id = $result[0]['product_name_id'];
		
		$result = $userreview->arrGetAdminExpertGrade($category_id,'','',$product_name_id);
		
		$design_rating = $result[0]['design_rating'];
		$performance_rating = $result[0]['performance_rating'];
		$user_rating = $result[0]['user_rating'];
		
		$design_rating_proportion = ($design_rating*100)/10;
		$performance_rating_proportion = ($performance_rating*100)/10;
		$user_rating_proportion = ($user_rating*100)/10;

		$overallgrade = $result[0]['overallgrade'];

		$rating_algo_key = "";
		foreach($rangeArr as $key => $range){
			if($overallgrade >= $range[0] && $overallgrade <= $range[1]){
				$rating_algo_key = $key;
				break;
			}
		}
		
		$expertratinghtml = '';
		foreach($ratingAlgoArr[$rating_algo_key] as $classname){
			$expertratinghtml .= "<img class=\"$classname\" src=\"".IMAGE_URL."spacer.gif\">";
		}
		$rating_algo_key = $rating_algo_key ? $rating_algo_key : 'Not Yet Rated';
		$expertratingxml .= "<STAR_EXPERT_GRAPH_RATING_STR><![CDATA[$expertratinghtml]]></STAR_EXPERT_GRAPH_RATING_STR>";
		$expertratingxml .= "<STAR_EXPERT_GRAPH_RATING_MSG><![CDATA[$rating_algo_key]]></STAR_EXPERT_GRAPH_RATING_MSG>";
		//$expertratinghtml =  ($rating_algo_key != 'onestar' && $rating_algo_key != 'halfstar' && $rating_algo_key != '') ? $expertratinghtml."($rating_algo_key)" : $expertratinghtml;

		$expertratingxml .= "<STAR_EXPERT_RATING_STR><![CDATA[$expertratinghtml]]></STAR_EXPERT_RATING_STR>";
		
		$expertratingxml .= "<EXPERT_DESIGN_RATING_PROPORTION><![CDATA[$design_rating_proportion]]></EXPERT_DESIGN_RATING_PROPORTION>";
		$expertratingxml .= "<EXPERT_DESIGN_RATING><![CDATA[$design_rating]]></EXPERT_DESIGN_RATING>";
		$expertratingxml .= "<EXPERT_PERFORMANCE_RATING_PROPORTION><![CDATA[$performance_rating_proportion]]></EXPERT_PERFORMANCE_RATING_PROPORTION>";
		$expertratingxml .= "<EXPERT_PERFORMANCE_RATING><![CDATA[$performance_rating]]></EXPERT_PERFORMANCE_RATING>";
		$expertratingxml .= "<EXPERT_USER_RATING_PROPORTION><![CDATA[$user_rating_proportion]]></EXPERT_USER_RATING_PROPORTION>";
		$expertratingxml .= "<EXPERT_USER_RATING><![CDATA[$user_rating]]></EXPERT_USER_RATING>";
	
	//end code added by rajesh on dated 02-06-2011 for expert rating and graph.

	//start  code added by rajesh on dated 02-06-2011 for user.
		
		$reviewsresult = $userreview->arrGetAdminOverallGrade($category_id,$rating_brand_id,'0',$product_name_id);
		$reviewscnt = sizeof($reviewsresult);
		$overallcnt = 0;
		$overallavg = round($reviewsresult[0]['overallgrade']);
		if($reviewscnt <= 0){
			$reviewsresult = $userreview->arrGetOverallGrade($category_id,$rating_brand_id,'0',$product_name_id);	
			$overallavg = round($reviewsresult[0]['overallavg']);
			$overallcnt = $reviewsresult[0]['totaloverallcnt'] ? $reviewsresult[0]['totaloverallcnt'] : 0;
			$totalcnt = $userreview->arrGetUserReviewDetailsCount("","","","","",$rating_brand_id,$category_id,$product_name_id);			
		}

		$brandresult = $oBrand->arrGetBrandDetails($rating_brand_id);
		$product_brand_name = html_entity_decode($brandresult[0]['brand_name'],ENT_QUOTES,'UTF-8');
		$product_brand_name = removeSlashes($product_brand_name);
		$product_brand_name = seo_title_replace($product_brand_name);
		$product_link_name = $product_brand_name."-".$product_info_name;
		$product_link_name = html_entity_decode($product_link_name,ENT_QUOTES,'UTF-8');
		$product_link_name = removeSlashes($product_link_name);
		$product_link_name = seo_title_replace($product_link_name);
		unset($seoTitleArr);
		$seoTitleArr[] = SEO_WEB_URL;
		$seoTitleArr[] = $product_brand_name."-cars";
		$seoTitleArr[] = $product_link_name;
		$seoTitleArr[] = "reviews-ratings";
		$seoTitleArr[] = SEO_CARS_MODEL_REVIEWS; 
		$seoTitleArr[] = $product_info_name;
		$seoTitleArr[] = $product_name_id;
		$seo_model_url = implode("/",$seoTitleArr);
		unset($seoTitleArr);

		$html = "";
		for($grade=1;$grade<=5;$grade++){
			if($grade <= $overallavg){
				$html .= '<img src="'.IMAGE_URL.'spacer.gif" class="vsblStr"/>';
			}else{
				$html .= '<img src="'.IMAGE_URL.'spacer.gif" class="dsblStr"/>';
			}
		}
		if($overallavg == 1){
			$gradeStr = "Poor";
		}else if($overallavg == 2){
			$gradeStr = "Fair";
		}else if($overallavg == 3){
			$gradeStr = "Average";
		}else if($overallavg == 4){
			$gradeStr = "Good";
		}else if($overallavg == 5){
			$gradeStr = "Excellent";
		}
		
		$expertratingxml .= "<OVERALL_AVG_HTML><![CDATA[$html]]></OVERALL_AVG_HTML>";
		$expertratingxml .= "<OVERALL_AVG_HTML_MSG><![CDATA[$gradeStr]]></OVERALL_AVG_HTML_MSG>";
		$expertratingxml .= "<OVERALL_AVG_CNT><![CDATA[$overallavg]]></OVERALL_AVG_CNT>";
		$expertratingxml .= "<OVERALL_TOTAL_CNT><![CDATA[$overallcnt]]></OVERALL_TOTAL_CNT>";
		$expertratingxml .= "<OVERALL_CNT><![CDATA[$totalcnt]]></OVERALL_CNT>";
		$expertratingxml .= "<SEO_MODEL_RATING_PAGE_URL><![CDATA[$seo_model_url]]></SEO_MODEL_RATING_PAGE_URL>";
	//end code added by rajesh on dated 02-06-2011 for user.
	
	if(empty($aProductDetail)){
		header("Location:".WEB_URL."404.php");exit;
	}

	if(!empty($selected_city_id)){
		$price_result = $oPrice->arrGetPriceDetails("",$product_id,$categoryid,"","",$selected_city_id,"1","","","");
	}else{
		$price_result = $oPrice->arrGetPriceDetails("",$product_id,$categoryid,"","","","1","","","1");
        }
	
                    
	$iSelCity=$price_result[0]['city_id'];
	$iSelCityName=$price_result[0]['city_name'];
	$aBrandDetail = $oBrand->arrGetBrandDetails("",$category_id);
	if(is_array($aBrandDetail)){
		foreach($aBrandDetail as $ibKey=>$aBrandData){
			$aBrandDetailName[$aBrandData['brand_id']][]=$aBrandData['brand_name'];
		}
	}
	
	//echo "START TIME----".date('Y-m-d m:i:s')."<br>";
	
	$aProductWithPriceDetail = $oProduct->constantProductInfoDetails($aProductDetail,$category_id,$iSelCity);
	//print"<pre>";print_r($aProductWithPriceDetail);print"</pre>";
	
	//echo "START TIME 12----".date('Y-m-d m:i:s')."<br>";
	if(is_array($aProductWithPriceDetail)){
		$sProductName=$aProductWithPriceDetail['0']['product_name'];
		$sItemSelProductName=$aProductWithPriceDetail['0']['product_name'];
		$iOnRoadPrice=$aProductWithPriceDetail['0']['On_Road_Price'];
		$iExShowRoomPriceOriginal = $aProductWithPriceDetail['0']['exshowroomprice'];
		$iExShowRoomPrice = $aProductWithPriceDetail['0']['exshowroomprice'];
		$sDisplayName=$aProductWithPriceDetail['0']['display_product_name'];

		$productinforesult = $oProduct->arrGetProductNameInfo("",$category_id,"",$sProductName);
		$product_desc = html_entity_decode($productinforesult['0']['product_name_desc'],ENT_QUOTES,'UTF-8');
		$sLinkProductName=$aProductWithPriceDetail['0']['link_product_name'];
		$sImagePath=$aProductWithPriceDetail['0']['image_path'];
		$img_media_id = $aProductWithPriceDetail['0']['img_media_id'];

		if(!empty($sImagePath)){
			$sImagePath = resizeImagePath($sImagePath,"160X120",$aModuleImageResize,$img_media_id);
			$sImagePath = $sImagePath ? CENTRAL_IMAGE_URL.$sImagePath :"";
		}

		$sVideoPath=$aProductWithPriceDetail['0']['video_path'];
		$sVariant=$aProductWithPriceDetail['0']['variant'];
		$iBrandId=$aProductWithPriceDetail['0']['brand_id'];
		$aPriceDetails=$aProductWithPriceDetail['0']['price_details'];
		$prodCityId=$aPriceDetails['0']['city_id'];
		$brand_result=$oBrand->arrGetBrandDetails($iBrandId,$category_id);
		$brand_name=$brand_result[0]['brand_name'];

		$seo_ProductName = $sProductName;
		$seo_variant = $sVariant;
		//SEO details	
		$seo_title_part = $brand_name." ".$sProductName." ".str_replace(" ","-",$sVariant);
		$title = $brand_name." ".$sProductName." ".str_replace(" ","-",$sVariant);
		if(!empty($tab_id)){
			if($tab_id==2){
				$typeshow = 'Features';
			}
			if($tab_id==3){
				$typeshow ='Technical Specifications';
			}
		}
		else{
			$typeshow = ' Overview & Details';
		}

		$seoArr[0] = $title ." ".$typeshow;
		$breadcrumb = "Overview : $title";
		$seo_desc = $title.' - On Cars India. Get all the latest car reviews and ratings, available versions,on road price,technical specifications, features, colours, Photos and videos on '.SEO_DOMAIN;
		$seoArr[2] = $brand_name." ".$sProductName.' Cars';
		$seoArr[3] = SEO_DOMAIN;
		$seo_title = implode(" | ",$seoArr);
		
		$product_brand_name = html_entity_decode($brand_name,ENT_QUOTES,'UTF-8');
		$product_brand_name = removeSlashes($product_brand_name);
		$product_brand_name = seo_title_replace($product_brand_name);

		$product_link_name = html_entity_decode($sLinkProductName,ENT_QUOTES,'UTF-8');
		$product_link_name = removeSlashes($product_link_name);
		$product_link_name = seo_title_replace($product_link_name);

		$product_variant_name = html_entity_decode($sVariant,ENT_QUOTES,'UTF-8');
		$product_variant_name = removeSlashes($product_variant_name);
		$product_variant_name = seo_title_replace($product_variant_name);
		
		$aHostqstr=explode("/",$_SERVER['SCRIPT_URL']);
		/*
		if(rawurlencode($aHostqstr[1])!=$product_brand_name."-cars"){
			header("Location:404.php");exit;
		}else if(rawurlencode($aHostqstr[2])!=$product_link_name){
			header("Location:404.php");exit;
		}else if(rawurlencode($aHostqstr[3])!=$product_variant_name){
			header("Location:404.php");exit;
		}*/
		
		$aSelProductNameData=$oProduct->arrGetProductByName($sItemSelProductName,'',"","1","0","5");
		 if(is_array($aSelProductNameData)){
			foreach($aSelProductNameData as $iwKey=>$aValueWallPaperProd){
				$aSelProductIds[]=$aValueWallPaperProd['product_id'];
			}
		}
		
		unset($seoTitleArr);
		$seoTitleArr[] = SEO_WEB_URL;
		$seoTitleArr[] = trim($product_brand_name)."-cars";
		$seoTitleArr[] = trim($product_link_name);
		$seoTitleArr[] = trim($product_variant_name);
		$seoTitleArr[] = SEO_GET_ON_ROAD_PRICE;
		$seoTitleArr[] = $product_id;
		$seo_url = implode("/",$seoTitleArr);

		$aProductData[0]=array("product_id"=>$product_id,"product_name"=>$sProductName,"On_Road_Price"=>$iOnRoadPrice,"display_product_name"=>$sDisplayName,"link_product_name"=>$sLinkProductName,"video_path"=>$sVideoPath,"image_path"=>$sImagePath,"city_name"=>$sCityName,"variant"=>$sVariant,"exshowroomprice"=>$iExShowRoomPrice,"seo_url"=>$seo_url,"exshowroompriceorginal"=>$iExShowRoomPriceOriginal,"brand_name"=>$brand_name,"product_desc"=>$product_desc);

		$disp_title=$aProductData['0']['display_product_name'];
		//print "<pre>";print_r($aProductData);

		$sProductDet=arraytoxml($aProductData,"PRODUCT_DETAIL_DATA");
		$sProductDetXml ="<PRODUCT_DETAIL>".$sProductDet."</PRODUCT_DETAIL>";
	}

	//start code added by rajesh on dated 10-06-2011 for variant page summary.
if(!empty($selected_city_id) && !empty($product_id)){
	$aOverview = $oFeature->arrGetVariantPageSummary($category_id,$product_id,"",$selected_city_id);
}else{
	$aOverview = $oFeature->arrGetVariantPageSummary($category_id,$product_id,"1");
}

	foreach($aOverview as $key=>$val){
		if(!strpos($key,'Price') && !strpos($key,'Feature') ){
			unset($overviewArr);
			
			foreach($aOverview[$key] as $overviewtitle=>$overviewvalueArr){
				$overviewvalueArr = array_change_key_case($overviewvalueArr,CASE_UPPER);
				
				$techspecxml .= "<TECH_SPEC_DATA>";
				$techspecxml .= "<FEATURE_TITLE><![CDATA[$overviewtitle]]></FEATURE_TITLE>";
				foreach($overviewvalueArr as $techspeckey => $techspecval){
					$techspecxml .= "<$techspeckey><![CDATA[$techspecval]]></$techspeckey>";
				}
				$techspecxml .= "</TECH_SPEC_DATA>";
				unset($overviewvalueArr);
			}
		}
		if(strpos($key,'Feature') ){
			unset($overviewArr);
			foreach($aOverview[$key] as $overviewtitle=>$overviewvalueArr){
				$overviewvalueArr = array_change_key_case($overviewvalueArr,CASE_UPPER);
				$featurespecxml .= "<FEATURE_SPEC_DATA>";
				$featurespecxml .= "<FEATURE_TITLE><![CDATA[$overviewtitle]]></FEATURE_TITLE>";
				foreach($overviewvalueArr as $techspeckey => $techspecval){
					$featurespecxml .= "<$techspeckey><![CDATA[$techspecval]]></$techspeckey>";
				}
				$featurespecxml .= "</FEATURE_SPEC_DATA>";
				unset($overviewvalueArr);
			}
		}
		if(strpos($key,'Price')){
			unset($overviewArr);
			foreach($aOverview[$key] as $overviewtitle=>$overview_value){
				$techspecxml .= "<AVG_EX_SHOWROOM_PRICE><![CDATA[".str_replace("(insurance, road tax and other taxes/charges extra) ","",$overview_value)."]]></AVG_EX_SHOWROOM_PRICE>";
			}
		}
		if(strpos($key,'product_desc')){
			unset($overviewArr);
			foreach($aOverview[$key] as $overviewtitle=>$overview_value){
				$techspecxml .= "<PRODUCT_DESC><![CDATA[$overview_value]]></PRODUCT_DESC>";
			}
		}
	}
	$sOverviewXML =	"<TECH_SPEC_SHORT_DESC>$techspecxml</TECH_SPEC_SHORT_DESC>";
	$sOverviewXML .= "<FEATURE_SPEC_SHORT_DESC>$featurespecxml</FEATURE_SPEC_SHORT_DESC>";
	//end code added by rajesh on dated 10-06-2011 for variant page summary.

	if(!empty($category_id)){
		$result = $oFeature->arrFeatureSubGroupDetails("",$category_id);
		$featureSubGroupCnt = sizeof($result);
		for($i=0;$i<$featureSubGroupCnt;$i++){
			$sub_group_id = $result[$i]['sub_group_id'];
			$featureSubGroupArr[$sub_group_id] = $sub_group_id;
		}
		unset($result);
		$result = $oFeature->arrGetFeatureMainGroupDetails("",$category_id,"",$startlimit,$limitcnt);
	}
	$cnt = sizeof($result);
	$featureboxcntArr=Array();
	for($i=0;$i<$cnt;$i++){
		$status = $result[$i]['status'];
		$main_group_id = $result[$i]['group_id'];
		$categoryid = $result[$i]['category_id'];
		$main_feature_group_name = $result[$i]['main_group_name'];
		unset($seoTitleArr);
				$seoTitleArr[] = SEO_WEB_URL;
				$seoTitleArr[] = $product_brand_name."-cars";
				$seoTitleArr[] = $product_link_name;
				$seoTitleArr[] = $product_variant_name;
				if(!empty($main_group_id)){
					if($main_group_id==1){
						$seoTitleArr[] = 'Overviews';
					}
					if($main_group_id==2){
						$seoTitleArr[] = 'Features';
					}
					if($main_group_id==3){
						$seoTitleArr[] = 'Tech-specification';
					}
				}
				
				$seoTitleArr[] = $product_id;
				if($main_group_id!=1){
					$seoTitleArr[] = $main_group_id;
				}
				$seo_url = implode("/",$seoTitleArr);
				$result[$i]['seo_url']= $seo_url;
		if(!empty($categoryid)){
			$category_name = $category_result[0]['category_name'];
			$result[$i]['js_category_name'] = $category_name;
			$result[$i]['category_name'] = html_entity_decode($category_name,ENT_QUOTES,'UTF-8');
			$feature_result = $oFeature->arrGetFeatureDetails("",$category_id,$main_group_id);
			$featureCnt = sizeof($feature_result);
			$featureboxcntArr[] = $featureCnt;
			for($j=0;$j<$featureCnt;$j++){
				$feature_group = $feature_result[$j]['feature_group'];
				$feature_sub_group_array = $oFeature->arrFeatureSubGroupDetails($feature_group,$categoryid,"");
				$sub_group_name = $feature_sub_group_array[0]['sub_group_name'];
				$main_feature_group = $feature_result[$j]['main_feature_group'];
				$status = $feature_result[$j]['status'];
				$categoryid = $feature_result[$j]['category_id'];
				$feature_id = $feature_result[$j]['feature_id'];
				$unit_id = $feature_result[$j]['unit_id'];								
				
				if(!empty($feature_id)){
					$pivot_result = $oPivot->arrGetPivotDetails("",$categoryid,$feature_id,"");
					$product_result = $oProduct->arrGetProductFeatureDetails("",$feature_id,$product_id);
				}
				$feature_value = $product_result[0]['feature_value'];
				if(!empty($feature_value)){
					$featureNameArr[] = $feature_value;
				}
				if(!empty($unit_id)){
					$unit_result = $oFeature->arrFeatureUnitDetails($unit_id,$categoryid);
					$feature_unit = $unit_result[0]['unit_name'];
				}else{
					$feature_unit = "";
				}
				if(!empty($feature_unit) && !empty($feature_value) && $feature_value != '-'){
					$featureNameArr[] = $feature_unit;
				}
				$feature_value = implode(" ",$featureNameArr);
				unset($featureNameArr);
				if(strtolower($feature_value) == 'yes'){
					$feature_value = 'yes';
				}else if(strtolower($feature_value) == 'no'){
					$feature_value = 'no';
				}
				$feature_result[$j]['product_feature_id'] = $product_result[0]['feature_id'];
				$feature_result[$j]['feature_value'] = $feature_value;
				$feature_result[$j]['pivot_feature_id'] = $pivot_result[0]['feature_id'];
				$feature_result[$j]['js_feature_name'] = $feature_result[$j]['feature_name'];
				$feature_result[$j]['js_feature_group'] = $feature_result[$j]['feature_group'];
				$feature_result[$j]['js_feature_desc'] = $feature_result[$j]['feature_description'];
				$feature_result[$j]['js_feature_unit'] = $feature_unit;
				$feature_result[$j]['feature_status'] = ($status == 1) ? 'Active' : 'InActive';
				$feature_result[$j]['feature_unit'] = $feature_unit ? html_entity_decode($feature_unit,ENT_QUOTES,'UTF-8') : 'Nil';
				$feature_result[$j]['feature_group'] = $feature_result[$j]['feature_group'] ? html_entity_decode($feature_result[$j]['feature_group'],ENT_QUOTES,'UTF-8') : 'Nil';
				$feature_description = $feature_result[$j]['feature_description'];
                                if($feature_description != ""){
                                	$feature_description = html_entity_decode($feature_description,ENT_QUOTES,'UTF-8');
                                        $feature_description = str_replace('&amp;amp;',"",$feature_description);
                                        $feature_description = str_replace('&#039;',"'",$feature_description);
                                        $feature_description = str_replace('#039;',"'",$feature_description);
				}                                        
				$feature_result[$j]['feature_desc'] = $feature_description ? html_entity_decode($feature_description,ENT_QUOTES,'UTF-8') : 'Nil';
				$feature_result[$j]['create_date'] = date('d-m-Y',strtotime($feature_result[$j]['create_date']));
				$feature_result[$j]['js_feature_name'] = $feature_result[$j]['feature_name'];
				$feature_result[$j]['feature_name'] = $feature_result[$j]['feature_name'] ? html_entity_decode($feature_result[$j]['feature_name'],ENT_QUOTES,'UTF-8') : 'Nil';			
				$featureresult[$main_group_id][$feature_group][] = $feature_result[$j];
				$featureresult[$main_group_id][$feature_group]['sub_group_name'] = $feature_sub_group_array[0]['sub_group_name'];
				$featureresult[$main_group_id][$feature_group]['sub_group_id'] = $feature_group;
				
				$pivot_feature_id = $pivot_result[0]['feature_id'];
				$featureresult[$main_group_id][$feature_group]['is_pivot_group'] = ($pivot_feature_id == $feature_id) ? 'true' : 'false';
				$featureresult[$main_group_id][$feature_group]['pivot_feature_id'] = $pivot_feature_id;
				$featureresult[$main_group_id][$feature_group]['feature_id'] = $feature_id;
				
			}
			foreach($result[$i] as $k=> $v){
				$featureresult[$main_group_id][$k] = $v;
			}
		}
	}
		
	$groupnodexml .= "<GROUP_MASTER>";
	if($featureresult){
		foreach($featureresult as $maingroupkey => $maingroupval){
			if(is_array($maingroupval)){
				$groupnodexml .= "<GROUP_MASTER_DATA>";
				foreach($maingroupval as $subgroupkey=>$subgroupval){
					if(is_array($subgroupval)){
						$groupnodexml .= "<SUB_GROUP_MASTER>";
						 foreach($subgroupval as $key => $featuredata){
							if(is_array($featuredata)){
								$groupnodexml .= "<SUB_GROUP_MASTER_DATA>";
								$featuredata = array_change_key_case($featuredata,CASE_UPPER);
								foreach($featuredata as $featurekey => $featureval){
                                    $groupnodexml .= "<$featurekey><![CDATA[$featureval]]></$featurekey>";
                                }	
								$groupnodexml .= "</SUB_GROUP_MASTER_DATA>";
							}else{
								$groupnodexml .= "<".strtoupper($key)."><![CDATA[$featuredata]]></".strtoupper($key).">";
							}
						}
						$groupnodexml .= "</SUB_GROUP_MASTER>";
					}else{
						 $groupnodexml .= "<".strtoupper($subgroupkey)."><![CDATA[$subgroupval]]></".strtoupper($subgroupkey).">";
						 
					}
				}
				$groupnodexml .= "</GROUP_MASTER_DATA>";
			}
		}
	}	
	$groupnodexml .= "</GROUP_MASTER>";

	$config_details = get_config_details();
	$strXML .= "<XML>";
	$strXML .= "<BREAD_CRUMB><![CDATA[$breadcrumb]]></BREAD_CRUMB>";
	$strXML .= "<VIEW_DISP_TITLE><![CDATA[".$disp_title_name."]]></VIEW_DISP_TITLE>";
	$strXML .= "<OC_ROS_BOTTOM_NORTH_728x90><![CDATA[OC_ROS_Bottom_North_728x90]]></OC_ROS_BOTTOM_NORTH_728x90>";
	$strXML .= "<OC_ROS_TOP_RHS_LREC_300x250_1><![CDATA[OC_ROS_Top_RHS_Lrec_300x250_1]]></OC_ROS_TOP_RHS_LREC_300x250_1>";
	$strXML .= "<SEO_CAR_FINDER><![CDATA[".SEO_CAR_FINDER."]]></SEO_CAR_FINDER>";
	$strXML .= "<SEO_WEB_URL><![CDATA[".SEO_WEB_URL."]]></SEO_WEB_URL>";
	$strXML .= "<SEO_AUTO_NEWS><![CDATA[".SEO_AUTO_NEWS."]]></SEO_AUTO_NEWS>";
	$strXML .= "<ONCARS_SEO_URL><![CDATA[$oncars_seo_url]]></ONCARS_SEO_URL>";
	$strXML .= "<EXPERTS_SEO_URL><![CDATA[$expert_seo_url]]></EXPERTS_SEO_URL>";
	$strXML .= "<SEO_TITLE><![CDATA[$seo_title]]></SEO_TITLE>";
	$strXML .= "<SEO_TAGS><![CDATA[$seo_keywords]]></SEO_TAGS>";
	$strXML .= "<SEO_DESC><![CDATA[$seo_desc]]></SEO_DESC>";
	$strXML .= "<STARTLIMIT><![CDATA[$startlimit]]></STARTLIMIT>";
	$strXML .= "<CNT><![CDATA[$limitcnt]]></CNT>";
	$strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$category_id]]></SELECTED_CATEGORY_ID>"; 
	$strXML .= "<SELECTEDTABID><![CDATA[$tab_id]]></SELECTEDTABID>";
	$strXML .= "<PHOTO_TAB_ID><![CDATA[$photo_tab_id]]></PHOTO_TAB_ID>";
	$strXML .= "<SEO_PHOTO_TAB_URL><![CDATA[$seo_photo_tab_url]]></SEO_PHOTO_TAB_URL>";
	$strXML .= "<SEO_VIDEO_TAB_URL><![CDATA[$seo_video_tab_url]]></SEO_VIDEO_TAB_URL>";
	$strXML.="<CURR_CITY><![CDATA[".$curr_city_name."]]></CURR_CITY>";
	$strXML.="<DEALER_FLAG><![CDATA[".$dealer_flag."]]></DEALER_FLAG>";
	$strXML.="<DEALER_QUOTE_URL><![CDATA[$dealer_quote_url]]></DEALER_QUOTE_URL>";
	$strXML.="<BOOK_TEST_DRIVE_URL><![CDATA[$book_test_drive_url]]></BOOK_TEST_DRIVE_URL>";
	$strXML.="<RELATED_CITY_ID><![CDATA[".$related_city_id."]]></RELATED_CITY_ID>";
	$strXML.="<COMPARE_CARS_URL><![CDATA[$compareCarsUrl]]></COMPARE_CARS_URL>";
	$strXML .= $config_details;
	$strXML .= $sBrandDataDetXML;
	$strXML .= $sProductDetXml;
	$strXML .= $groupnodexml;
	$strXML .= $sSimilarProductDetXml;
	$strXML .= $sCityDetXml;
	$strXML .= $sTopCopmetitorsListing;
	$strXML .= $sWallpapersDetXml;
	$strXML .= $sProductNewsDetXml;
	$strXML .= $strmediaxml;
	$strXML .= $srevDetailxml;
	$strXML .= $sReviewDetailXml;
	$strXML .= $strRevGroupxml;
	$strXML .= $sReviewGROUPWISEDetailXml ;
	$strXML .= $sPhotoDetXML;
	$strXML .= $sPhotoVideoDetXML;
	$strXML.= $xml;
	$strXML .= "<OVERVIEW>$sOverviewXML</OVERVIEW>";
	$strXML .= "<PRODSELCITY>$prodCityId</PRODSELCITY>";
	$strXML .= "<REVIEW_NAME>$reviewName</REVIEW_NAME>";
	$strXML .= "<DEF_REVIEW_ID>$defRevId</DEF_REVIEW_ID>";
	$strXML .= "<ONCARS_GROUP_SEO_URL><![CDATA[$oncars_group_seo_url]]></ONCARS_GROUP_SEO_URL>";
	$strXML .= "<PRODSELGROUP>$rev_grp_id</PRODSELGROUP>";
	$strXML .= "<FEATURE_YES_IMAGE_URL><![CDATA[".FEATURE_YES_IMAGE_URL."]]></FEATURE_YES_IMAGE_URL>";
	$strXML .= "<FEATURE_NO_IMAGE_URL><![CDATA[".FEATURE_NO_IMAGE_URL."]]></FEATURE_NO_IMAGE_URL>";
	$strXML.="<MBREPLYLIST>";
	$strXML.=$sReplyXml.$nodesPaging;
	$strXML.="<MBREPLYCOUNT><![CDATA[".$iRecCnt."]]></MBREPLYCOUNT>";
	$strXML.="<MBTID><![CDATA[".$iTId."]]></MBTID>";
	$strXML.="<CPAGE><![CDATA[".$page."]]></CPAGE>";
	$strXML.="<SERVICEID><![CDATA[".SERVICEID."]]></SERVICEID>";
	$strXML.="<CATEGORY><![CDATA[".$rev_category_id."]]></CATEGORY>";
	$strXML.="</MBREPLYLIST>";
	$strXML.="<CITY_ID><![CDATA[".$iSelCity."]]></CITY_ID>";
	$strXML .= "<USERREVIEW_SEO_URL><![CDATA[$userreview_seo_url]]></USERREVIEW_SEO_URL>";
	$strXML .=  "<PAGING><![CDATA[$sPagingXml]]></PAGING>";
	$strXML .=  "<CATEGORY_ID><![CDATA[$category_id]]></CATEGORY_ID>";
	$strXML .=  "<BRAND_ID><![CDATA[$iBrandId]]></BRAND_ID>";
	$strXML .=  "<PRODUCT_NAME_ID><![CDATA[$productNameInfoId]]></PRODUCT_NAME_ID>";
	$strXML .=  "<PRODUCT_ID><![CDATA[$rev_product_id]]></PRODUCT_ID>";
	$strXML .=  "<RETURN_REVIEW_URL><![CDATA[".$_SERVER['SCRIPT_URI']."]]></RETURN_REVIEW_URL>";
	$strXML .=  "<PAGE_NAME><![CDATA[".$_SERVER['SCRIPT_URI']."]]></PAGE_NAME>";
	$strXML .=  "<USER_REVIEW_ID><![CDATA[$user_review_id]]></USER_REVIEW_ID>";
	$strXML .= "<COMMENT_COUNT><![CDATA[".$iRecCnt."]]></COMMENT_COUNT>";
	$strXML .= "<VIEWS_COUNT><![CDATA[$views_count]]></VIEWS_COUNT>";
	$strXML .= "<VIEWS_PAGE_NAME><![CDATA[".$views_page_name."]]></VIEWS_PAGE_NAME>";
	$strXML .= $expertratingxml;
	$most_recent_one_result_list = $videoGallery->getarrMostRecentVideosHeaderTabLink($category_id,"0","1",$array_result);
	$strXML.="<MOST_RECENT_VIDEO_HEADER_LINK><![CDATA[".$most_recent_one_result_list."]]></MOST_RECENT_VIDEO_HEADER_LINK>";
	$strXML .= "</XML>";


	//echo "view counter - ".$iRecCnt."====".$views_count."====".$views_page_name;

	$strXML = mb_convert_encoding($strXML, "UTF-8");
	if($_REQUEST['debug']==1){ header('Content-type: text/xml');echo $strXML;exit;}

	$doc = new DOMDocument();
	$doc->loadXML($strXML);
	$doc->saveXML();
	$xslt = new xsltProcessor;
	if($reviewName==='oncars'){
		$xsl = DOMDocument::load('xsl/oncars_reviews.xsl');
	}else if($reviewName==='experts'){
		$xsl = DOMDocument::load('xsl/expert_reviews.xsl');
	}else if($reviewName==='userreview'){
	if($revtype=='fullreview'){
		$xsl = DOMDocument::load('xsl/oncars_user_full_review.xsl');
		}else{
			$xsl = DOMDocument::load('xsl/oncars_page_user_review.xsl');
		}
	}else{
		if($tab=='photos_videos'){
			$xsl = DOMDocument::load('xsl/photos_videos.xsl');
		}else{
			$xsl = DOMDocument::load('xsl/productdetails-page.xsl');
		}
	}
	$xslt->importStylesheet($xsl);
	print $xslt->transformToXML($doc);
?>