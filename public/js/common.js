// common.js for India.com admin section, Author : Erfan Wavekar, Date : Feb, 04 2013
$(document).ready(function(){
	/* admin homepage listing */
	$("#homeblock ul li:odd").addClass('odd');
	
	$("#homeblock ul li").mouseenter(function() {
		$(this).addClass("active");
	}).mouseleave(function() {
		$(this).removeClass("active");
	});
	
	
	
	// to be removed later while coding admin 
	$("#widlist .listdiv").each(function(){
		var indx = $(this).index()+1;
        $(this).attr('rel',indx);
    });	
	// ---------

	function chklistcount(){
		var mylst = $("#widlist .listdiv").length;
		if(mylst == 0)
		{
			$('<div class="nolist-msg" style="text-align:center; color:#f00; padding:8px;">No Stories in Media Library</div>').insertAfter('.hdbx');
		}
	}
	
	$(".btn-flushmedia").click(function(){
		var flush_confirm = confirm("Are you sure you want to Flush Media Library ?")
		if (flush_confirm){
			$('.listdiv').remove();
			$('.nolist-msg').remove();
			chklistcount();
                        jQuery.ajax({
                                url: '/admin/flushmedialibrary',
                                type: "GET",
                                success: function(data) {
                                alert("Media Library Flushed Successfull");
                        },
                                error: function () {
                                alert('Tecnical issue !!');
                            }	
                        });

		}
		else
		alert("Media Library not flushed !!!")
	});

	$('.remv').click(function(){
		var remv_cnf = confirm("Are you sure you want to remove this story ?")
		if (remv_cnf){
			$(this).parent().remove();
			$(".lstno p:last").remove();
		}
		chklistcount();
	});

});



// Popup window for story editing
var popupWindow = null;
function edtwin(url){
w = 700; h = 350,
LeftPosition = (screen.width) ? (screen.width-w)/2 : 0; TopPosition = (screen.height) ? (screen.height-h)/2 : 0;
settings ='height='+h+',width='+w+',top='+TopPosition+',left='+LeftPosition+',scrollbars=yes,resizable'
popupWindow = window.open(url,'mywin',settings)}

function addstorytowidget(storyid,imgname){
	var wid = $('#widget').val();
	if(wid=="noval"){
		alert("No widget selected!!");
		return false;
	}
	jQuery.ajax({
  		url: '/admin/mapstory/storyid/'+storyid+'/widid/'+wid+'/imgname/'+imgname,
  		type: "GET",
  		success: function(data) {
    		alert('Added!!');
    	},
		error: function () {
	        alert('Story already added before!!');
	    }	
	});	
	
}

function showwidgetstories(){
	var wid = $('#widget').val();
	if(wid=="noval"){
		alert("No widget selected!!");
		return false;
	}
	$('#prehotelloader').show();
	jQuery.ajax({
  		url: '/admin/getwidgetstories/widid/'+wid,
  		type: "GET",
  		success: function(data) {
    		$('.liblist-itm').html(data);
    		$('#prehotelloader').hide();
    		$("#widlist").sortable({
    	        update: function(event, ui) { 
    	        	var wid = $('#widget').val();
    	        	if(wid=="noval"){
    	        		alert("No widget selected!!");
    	        		$('#prehotelloader').hide();
    	        		return false;
    	        	}
    	        	var a;
    	        	$('.listdiv').each(function(){
    	        		if(a == null || a == undefined || a == "") a=$(this).attr('id'); 
    	        		else a = a + "-" + $(this).attr('id');    	        		
    	        	});
    	        	jQuery.ajax({
    	          		url: '/admin/updatewidgetstories/widid/'+wid+'/storyorder/'+a,
    	          		type: "GET",
    	          		success: function(data) {
    	            		
    	            	}
    	        	});
    	        	$('#prehotelloader').hide();
    	        },
    	        start: function(){ $('#prehotelloader').show(); },
    	        stop: function(){ $('#prehotelloader').hide(); },
    			cursor: 'move'
    				
    		});
    	}
	});	
	//$('#prehotelloader').hide();
}

var myvar = 1;

function deletestoryfromwidget(sid,type,cnt){
	var wid = $('#widget').val();
	if(sid == undefined || sid == null || sid =="") return false;
	if(wid=="noval"){
		alert("No widget selected!!");
		return false;
	}

	jQuery.ajax({
  		url: '/admin/deletestoryfromwidget/widid/'+wid+'/sid/'+sid,
  		type: "GET",
  		success: function(data) {
  			$('#'+sid).hide();
  			$('#pid'+sid).hide();
  			

                        var j=1;
                        var htm;   
                        var orderarr = [0,1,1.1,1.2,
                                            2,2.1,2.2,
                                            3,3.1,3.2,
                                            4,4.1,4.2,
                                            5,5.1,5.2,
                                            6,6.1,6.2,
                                            7,7.1,7.2,
                                            8,8.1,8.2,
                                            9,9.1,9.2,
                                            10,10.1,10.2
                                            ];
                        htm = "";
                        for(i=0;i<cnt-myvar;i++){
                            htm = htm + '<p>';
                            if(type == 1) htm = htm + orderarr[j++]; 
                            else htm = htm + j++;                
                            htm = htm + '</p>';

                        }
                        jQuery(".lstno").html("");    
                        jQuery(".lstno").html(htm);
                        alert("Removed!!");
                        myvar++;
               }             
	});		
}

function publishwidget(){
        var wid = $('#widget').val();
	if(wid=="noval"){
		alert("No widget selected!!");
		return false;
	}
	jQuery.ajax({
  		url: '/admin/publishwidtofrontend/widid/'+wid,
  		type: "GET",
  		success: function(data) {
                        clearmemcache(0,wid); 
  			alert("Published!!!");
  		}
	});
    

}

// Outline Js
$(document).ready(function(){
        /*var layoutid = $('#layout').val();
	if(layoutid=="noval"){
		alert("No layout selected!!");
		return false;
	}*/
 
        $( "#sidebar" ).sortable({
		update: function( event, ui ) {

                       var layoutid = $("#laynum").val();
                        if(layoutid == "" || layoutid == null )        layoutid = 1;

                        var divid = "sidebar";

                        var liIds = $('#'+divid+' li').map(function(i,n) {
                            return $(n).attr('id');
                        }).get().join('-');

                        //alert(liIds);

                        jQuery.ajax({
                                url: '/outline/sidebarmapping/secmap/'+liIds+'/layout/'+layoutid,
                                type: "GET",
                                success: function(data) {
                                        clearmemcache(layoutid,0);    
                                }
                        });

			$(this).find('.nomargin').removeClass('nomargin');
			//$(this).find('li').last().addClass('nomargin');
			$(".innerlist").each(function(){
				$(this).find('li').last().addClass('nomargin');
			});
		}													 
	});

	$( "#sectionsortbx" ).sortable({
                update: function( event, ui ) {

                        var layoutid = $("#laynum").val();
                        if(layoutid == "" || layoutid == null )        layoutid = 1;

                        var secmap="";
                        $('.innersection').each(function(){
                            if(secmap == "" || secmap==null) secmap = $(this).attr('id');
                            else secmap = secmap +"-"+$(this).attr('id');    
                        }); 
   
			$(this).find('.nomargin').removeClass('nomargin');
			//$(this).find('li').last().addClass('nomargin');
			$(".innerlist").each(function(){
				$(this).find('li').last().addClass('nomargin');
			});

                        jQuery.ajax({
                                url: '/outline/updatesecmapping/secmap/'+secmap+'/layout/'+layoutid,
                                type: "GET",
                                success: function(data) {
                                        //alert("Sections Updated !!");
                                        clearmemcache(layoutid,0);     
                                }
                        });

		}													 
	});

	$( ".innerlist" ).sortable({
		update: function( event, ui ) {

                       var layoutid = $("#laynum").val();
                        if(layoutid == "" || layoutid == null )        layoutid = 1;

			var divid = $(this).attr('id');

			
                        var liIds = $('#'+divid+' li').map(function(i,n) {
                            return $(n).attr('id');
                        }).get().join('-');

                        //alert(liIds);

   			$(this).find('.nomargin').removeClass('nomargin');
			//$(this).find('li').last().addClass('nomargin');
			$(".innerlist").each(function(){
				$(this).find('li').last().addClass('nomargin');
			});

                        jQuery.ajax({
                                url: '/outline/sectionmapping/secmap/'+liIds+'/layout/'+layoutid,
                                type: "GET",
                                success: function(data) {
                                        //alert("Sections Updated !!");
                                        clearmemcache(layoutid,0);        
                                }
                        });
		}													 
	});

	$(".innerlist").each(function(){
		$(this).find('li').last().addClass('nomargin');
	});
});

function changelocation(){
    var v = $("#layoutnumber").val();
    $(this).click(function() { window.location = "/outline/layout/design/"+v; })
}

function flushmedialibrary(){

    

}

function clearmemcache(layout,wid){
        jQuery.ajax({
                url: '/admin/clearmemcache/wid/'+wid+'/layout/'+layout,
                type: "GET",
                success: function(data) {
                        //alert("Cache clear!!");
                }
        });        
}








