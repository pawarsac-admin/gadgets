<?php
require_once('./include/config.php');
require_once(CLASSPATH.'DbConn.php');
$dbconn	=  new DbConn;
$config_details = get_config_details();
$strXML = "<?xml version='1.0' encoding='iso-8859-1'?>";
$strXML .= "<XML>";
$strXML .= $config_details;
$strXML.="<ERRMESSAGE>".$sErrMessage."</ERRMESSAGE>";
$strXML.="<FNAME><![CDATA[".$_COOKIE['fname']."]]></FNAME>";
$strXML.="<LNAME><![CDATA[".$_COOKIE['lname']."]]></LNAME>";
$strXML.="<UID><![CDATA[".$_COOKIE['uid']."]]></UID>";
$strXML.="<SESSIONID><![CDATA[".$_COOKIE['session_id']."]]></SESSIONID>";
$strXML .= $strSessionXML;
$strXML .= $strCONFIGXML;
$strXML .= "</XML>";

if($_GET['debug']==1) { header('Content-type: text/xml');echo $strXML;exit; }

$sTemplate = 'xsl/404.xsl';
if($_GET['debug'] == 2){
        header("content-type:404/xml");
        echo $strXML;die;
}


$doc = new DOMDocument();
$doc->loadXML($strXML);
//print $sTemplate;
$xslt = new xsltProcessor;
$xsl = DOMDocument::load($sTemplate);
$xslt->importStylesheet($xsl);
echo html_entity_decode($xslt->transformToXML($doc));
?>