<?php
// include
require_once('./include/config.php');
require_once(CLASSPATH.'DbConn.php');
require_once(CLASSPATH.'brand.class.php');
require_once(CLASSPATH.'category.class.php');
require_once(CLASSPATH.'product.class.php');
require_once(CLASSPATH.'user_review.class.php');
require_once(CLASSPATH.'videos.class.php');
require_once(CLASSPATH."citystate.class.php");
require_once(CLASSPATH.'Utility.php');

// object declaration
$dbconn = new DbConn;
$oBrand = new BrandManagement;
$category = new CategoryManagement;
$oProduct = new ProductManagement;
$userreview = new USERREVIEW;
$videoGallery = new videos();
$oCityState = new citystate;


//print "<pre>"; print_r($_REQUEST);

// param
$category_id = $_REQUEST['catid'] ? $_REQUEST['catid'] : SITE_CATEGORY_ID;
$action = $_REQUEST['action'];

//Most Reviewd start
if($action == "most_reviewed"){
	$most_reviewed_html = "";
	unset($most_reviewed_result);
	$most_reviewed_result = $userreview->getMostReviewedCount("","","","","","",$category_id,"","","1","","","");
	$tot_cnt = sizeof($most_reviewed_result);
	$j=1;
	for($i=0;$i<$tot_cnt;$i++){
		$brand_id = $most_reviewed_result[$i]["brand_id"];
		$product_info_id = $most_reviewed_result[$i]["product_info_id"];

		$review_cnt = $userreview->arrGetUserReviewDetailsCount("","","","","","",$category_id,$product_info_id,"","1","","","");
	
		$brand_name="";
	        if(!empty($brand_id)){
       	        	$brand_result = $oBrand->arrGetBrandDetails($brand_id,$category_id,"1");
        	        if(is_array($brand_result)){
                	        $brand_name = $brand_result["0"]["brand_name"];
                	}
	        }

        	$product_info_name = "";
	        if(!empty($product_info_id)){
        	        $product_info_result = $oProduct->arrGetProductNameInfo($product_info_id,$category_id,"","","1","","");
	
        	        if(is_array($product_info_result)){
                	        $product_info_name = $product_info_result[0]["product_info_name"];
                        	$image_path = $product_info_result[0]["image_path"];
				$img_media_id = $product_info_result[0]["img_media_id"];
	                        if(!empty($image_path)){
					$image_path = resizeImagePath($image_path,"87X65",$aModuleImageResize,$img_media_id);
                	                $image_path = $image_path ? CENTRAL_IMAGE_URL.$image_path : '';
                        	}
	                }
        	}
		//start user rating
		$reviewsresult = $userreview->arrGetAdminOverallGrade($category_id,$brand_id,'0',$product_name_id);
		$reviewscnt = sizeof($reviewsresult);
		$overallcnt = 0;
		$overallavg = round($reviewsresult[0]['overallgrade']);
		if($reviewscnt <= 0){
	        	$reviewsresult = $userreview->arrGetOverallGrade($category_id,$brand_id,'0',$product_name_id);
		        $overallavg = round($reviewsresult[0]['overallavg']);
		        $overallcnt = $reviewsresult[0]['totaloverallcnt'] ? $reviewsresult[0]['totaloverallcnt'] : 0;
	        	$totalcnt = $userreview->arrGetUserReviewDetailsCount("","","","","",$brand_id,$category_id,$product_name_id);
		}


		$product_brand_name = html_entity_decode($brand_name,ENT_QUOTES,'UTF-8');
		$product_brand_name = removeSlashes($product_brand_name);
		$product_brand_name = seo_title_replace($product_brand_name);

		$product_link_name = $product_brand_name."-".$aProductInfoDetails[0]['product_info_name'];
		$product_link_name = html_entity_decode($product_link_name,ENT_QUOTES,'UTF-8');
		$product_link_name = removeSlashes($product_link_name);
		$product_link_name = seo_title_replace($product_link_name);
		unset($seoTitleArr);
		$seoTitleArr[] = SEO_WEB_URL;
		$seoTitleArr[] = $product_brand_name."-cars";
		$seoTitleArr[] = $product_link_name;
		$seoTitleArr[] = "reviews-ratings";
		$seoTitleArr[] = SEO_CARS_MODEL_REVIEWS;
		$seoTitleArr[] = $product_info_name;
		$seoTitleArr[] = $product_info_id;
		$seo_model_url = implode("/",$seoTitleArr);
		unset($seoTitleArr);
		$html = "";
		for($grade=1;$grade<=5;$grade++){
		        if($grade <= $overallavg){
	        	        $html .= '<img src="'.IMAGE_URL.'spacer.gif" class="vsblStr"/>';
		        }else{
		                $html .= '<img src="'.IMAGE_URL.'spacer.gif" class="dsblStr"/>';
	        	}
		}
		//End user rating
	
		$display_name = $brand_name." ".$product_info_name;

		if($j >= "11"){	
			$most_reviewed_html .='<div class="ResSml">';
			$most_reviewed_html .='<a href="'.$seo_model_url.'">';
			if(!empty($image_path)){
				$most_reviewed_html .='<img src="'.$image_path.'" alt="" width="98" height="56"/>';
			}else{
				$most_reviewed_html .='<img src="'.IMAGE_URL.'no-image.png" alt="" width="98" height="56"/>';
			}
			$most_reviewed_html .='</a>';
			$most_reviewed_html .='<div class="carNameSml"><a href="'.$seo_model_url.'">'.$display_name.'</a></div>';
			$most_reviewed_html .='<div class="starRating"> <a href="'.$seo_model_url.'">'.$html.'</a> </div>';
			$most_reviewed_html .='<div class="reviewsSml"><a href="'.$seo_model_url.'">'.$review_cnt.' Reviews</a></div>';
			$most_reviewed_html .='<div class="clear"></div></div>';
		}
		$j++;
	}
	echo $most_reviewed_html;
	exit;
	//Most Reviewd End
}elseif($action == "highest_rated"){
	//Highest Rated start
	$highest_rated_html = "";

	$new_result = Array();
	unset($result);
	$user_review_result = $userreview->getHighestRatedProduct($category_id,"","","","1","");
	
	$cnt = sizeof($user_review_result);
	for($i=0;$i<$cnt;$i++){
		$overallcnt = 0;
	        $product_info_id = $user_review_result[$i]['product_info_id'];
        	$user_review_overallavg = $user_review_result[$i]['overallavg'];
	        $user_review_totaloverallcnt = $user_review_result[$i]['totaloverallcnt'];
        	$overallavg = round($user_review_overallavg);
	        $overallcnt = $user_review_totaloverallcnt;

        	$brand_name="";
	        $product_info_name = "";
        	if(!empty($product_info_id)){
                	$product_info_result = $oProduct->arrGetProductNameInfo($product_info_id,$category_id,"","","1","","");
	                if(is_array($product_info_result)){
        	                $product_info_name = $product_info_result[0]["product_info_name"];
                	        $brand_id = $product_info_result[0]["brand_id"];
                        	$image_path = $product_info_result[0]["image_path"];
	                        $img_media_id = $product_info_result[0]["img_media_id"];
        	                if(!empty($brand_id)){
                	                $brand_result = $oBrand->arrGetBrandDetails($brand_id,$category_id,"1");
                        	        if(is_array($brand_result)){
                                	        $brand_name = $brand_result["0"]["brand_name"];
	                                }
        	                }
                	}
	        }
		//start user rating
	        /*$reviewsresult = $userreview->arrGetAdminOverallGrade($category_id,$brand_id,'0',$product_info_id);
        	$reviewscnt = sizeof($reviewsresult);
	        $overallcnt = 0;
        	$overallavg = round($reviewsresult[0]['overallgrade']);
	        if($reviewscnt <= 0){
        	        $reviewsresult = $userreview->arrGetOverallGrade($category_id,$brand_id,'0',$product_info_id);
                	$overallavg = round($reviewsresult[0]['overallavg']);
	                $overallcnt = $reviewsresult[0]['totaloverallcnt'] ? $reviewsresult[0]['totaloverallcnt'] : 0;
			$overallavg = $user_review_overallavg;
	                $overallcnt = $user_review_totaloverallcnt;
        	        $totalcnt = $userreview->arrGetUserReviewDetailsCount("","","","","",$brand_id,$category_id,$product_info_id);
        	}*/

        	$product_brand_name = html_entity_decode($brand_name,ENT_QUOTES,'UTF-8');
	        $product_brand_name = removeSlashes($product_brand_name);
        	$product_brand_name = seo_title_replace($product_brand_name);

	        $product_link_name = $product_brand_name."-".$aProductInfoDetails[0]['product_info_name'];
        	$product_link_name = html_entity_decode($product_link_name,ENT_QUOTES,'UTF-8');
	        $product_link_name = removeSlashes($product_link_name);
        	$product_link_name = seo_title_replace($product_link_name);
	        unset($seoTitleArr);
        	$seoTitleArr[] = SEO_WEB_URL;
	        $seoTitleArr[] = $product_brand_name."-cars";
        	$seoTitleArr[] = $product_link_name;
	        $seoTitleArr[] = "reviews-ratings";
        	$seoTitleArr[] = SEO_CARS_MODEL_REVIEWS;
	        $seoTitleArr[] = $product_info_name;
        	$seoTitleArr[] = $product_info_id;
	        $seo_model_url = implode("/",$seoTitleArr);
        	unset($seoTitleArr);
	        $html = "";
		for($grade=1;$grade<=5;$grade++){
                	if($grade <= $overallavg){
                        	$html .= '<img src="'.IMAGE_URL.'spacer.gif" class="vsblStr"/>';
	                }else{
        	                $html .= '<img src="'.IMAGE_URL.'spacer.gif" class="dsblStr"/>';
                	}
	        }
		$review_cnt = $userreview->arrGetUserReviewDetailsCount("","","","","","",$category_id,$product_info_id,"","1","","","");
        	//End user rating
		//if($overallavg > 0){
		        $new_result[$i]["brand_id"] = $brand_id;
        		$new_result[$i]["product_info_id"] = $product_info_id;
	        	$new_result[$i]["brand_name"] = $brand_name;
	        	$new_result[$i]["product_info_name"] = $product_info_name;
			$new_result[$i]["review_count"] = $review_cnt;
		        $new_result[$i]["product_image_path"] = $image_path;
        		$new_result[$i]["img_media_id"] = $img_media_id;
		        $new_result[$i]["display_name"] = $brand_name." ".$product_info_name;
        		$new_result[$i]["totaloverallcnt"] = $totaloverallcnt;
		        $new_result[$i]["overallavg"] = $overallavg;
        		$new_result[$i]["overall_avg_html"] = $html;
	        	$new_result[$i]["overall_avg_cnt"] = $overallavg;
	        	$new_result[$i]["overall_total_cnt"] = $overallcnt;
		        $new_result[$i]["overall_cnt"] = $totalcnt;
        		$new_result[$i]["seo_model_rating_page_url"] = $seo_model_url;
		//}
	}
	//print"<pre>";print_r($new_result);print"</pre>";
	//$result = $videoGallery->sort2d($new_result, "overallavg", "desc", "", "");
	unset($result);
	$result = $new_result;
	$cnt=sizeof($result);

	if($cnt > 10){
		for($i=10;$i<$cnt;$i++){
			$product_image_path = $result[$i]["product_image_path"];
			$img_media_id = $result[$i]["img_media_id"];
        		if(!empty($product_image_path)){
	                	$product_image_path = resizeImagePath($product_image_path,"87X65",$aModuleImageResize,$img_media_id);
	                	$product_image_path = $product_image_path ? CENTRAL_IMAGE_URL.$product_image_path : '';
		        }
			$seo_model_url = $result[$i]['seo_model_rating_page_url'];
			$display_name = $result[$i]['display_name'];
			$html  = $result[$i]['overall_avg_html'];
			$review_cnt = $result[$i]['review_count'];
        	        $highest_rated_html .='<div class="ResSml">';
                	$highest_rated_html .='<a href="'.$seo_model_url.'">';
	                if(!empty($product_image_path)){ 
        	                $highest_rated_html .='<img src="'.$product_image_path.'" alt="" width="98" height="56"/>';
                	}else{
                        	$highest_rated_html .='<img src="'.IMAGE_URL.'no-image.png" alt="" width="98" height="56"/>';
	                }
        	        $highest_rated_html .='</a>';
                	$highest_rated_html .='<div class="carNameSml"><a href="'.$seo_model_url.'">'.$display_name.'</a></div>';
	                $highest_rated_html .='<div class="starRating"> <a href="'.$seo_model_url.'">'.$html.'</a> </div>';
        	        $highest_rated_html .='<div class="reviewsSml"><a href="'.$seo_model_url.'">'.$review_cnt.' Reviews</a></div>';
                	$highest_rated_html .='<div class="clear"></div></div>';

		}
	}
	echo $highest_rated_html;
	exit;
	//Highest Rated End
}elseif($action == "recently_reviewed"){
	//Recently Reviewed start
	$recently_reviewed_html = "";
	$user_review_result = $userreview->arrGetUserReviewDetails("","","","","","",$category_id,"","","1","","","");
	$cnt = sizeof($user_review_result);
	$product_info_arr = Array();
	$j=1;
	for($i=0;$i<$cnt;$i++){
		$user_review_id = $user_review_result[$i]["user_review_id"];
		$uid = $user_review_result[$i]["uid"];
		$title = $user_review_result[$i]["title"];
		$user_name = $user_review_result[$i]["user_name"];
		$location = $user_review_result[$i]["location"];
		$brand_id = $user_review_result[$i]["brand_id"];
		$product_info_id = $user_review_result[$i]["product_info_id"];
		$product_id = $user_review_result[$i]["product_id"];

		if (!in_array($product_info_id, $product_info_arr)) {
			$brand_name="";
		        if(!empty($brand_id)){
        		        $brand_result = $oBrand->arrGetBrandDetails($brand_id,$category_id,"1");
				if(is_array($brand_result)){
                		        $brand_name = $brand_result["0"]["brand_name"];
	        	        }
		        }

			$product_info_name = "";
			if(!empty($product_info_id)){
				$product_info_result = $oProduct->arrGetProductNameInfo($product_info_id,$category_id,"","","1","","");
				if(is_array($product_info_result)){
					$product_info_name = $product_info_result[0]["product_info_name"];
					$image_path = $product_info_result[0]["image_path"];
					$img_media_id = $product_info_result[0]["img_media_id"];
					if(!empty($image_path)){
       	                        	        $image_path = resizeImagePath($image_path,"87X65",$aModuleImageResize,$img_media_id);
        	                	        $image_path = $image_path ? CENTRAL_IMAGE_URL.$image_path : '';
					}
				}
			}

			$review_cnt = $userreview->arrGetUserReviewDetailsCount("","","","","","",$category_id,$product_info_id,"","1","","","");
	
			//start user rating
	        	$reviewsresult = $userreview->arrGetAdminOverallGrade($category_id,$brand_id,'0',$product_name_id);
	        	$reviewscnt = sizeof($reviewsresult);
		        $overallcnt = 0;
        		$overallavg = round($reviewsresult[0]['overallgrade']);
		        if($reviewscnt <= 0){	
        		        $reviewsresult = $userreview->arrGetOverallGrade($category_id,$brand_id,'0',$product_name_id);
	        	        $overallavg = round($reviewsresult[0]['overallavg']);
                		$overallcnt = $reviewsresult[0]['totaloverallcnt'] ? $reviewsresult[0]['totaloverallcnt'] : 0;
	        	        $totalcnt = $userreview->arrGetUserReviewDetailsCount("","","","","",$brand_id,$category_id,$product_name_id);
		        }
	
		        $product_brand_name = html_entity_decode($brand_name,ENT_QUOTES,'UTF-8');
        		$product_brand_name = removeSlashes($product_brand_name);
		        $product_brand_name = seo_title_replace($product_brand_name);
	
        		$product_link_name = $product_brand_name."-".$aProductInfoDetails[0]['product_info_name'];
	        	$product_link_name = html_entity_decode($product_link_name,ENT_QUOTES,'UTF-8');
	        	$product_link_name = removeSlashes($product_link_name);
		        $product_link_name = seo_title_replace($product_link_name);
        		unset($seoTitleArr); 
		        $seoTitleArr[] = SEO_WEB_URL;
        		$seoTitleArr[] = $product_brand_name."-cars";
	        	$seoTitleArr[] = $product_link_name;
	        	$seoTitleArr[] = "reviews-ratings";
		        $seoTitleArr[] = SEO_CARS_MODEL_REVIEWS;
        		$seoTitleArr[] = $product_info_name;
		        $seoTitleArr[] = $product_info_id;
        		$seo_model_url = implode("/",$seoTitleArr);
	        	unset($seoTitleArr);
			$html = "";
		        for($grade=1;$grade<=5;$grade++){
                		if($grade <= $overallavg){
        	        	        $html .= '<img src="'.IMAGE_URL.'spacer.gif" class="vsblStr"/>';
		                }else{
        	                	$html .= '<img src="'.IMAGE_URL.'spacer.gif" class="dsblStr"/>';
                		}
	        	}
		        //End user rating

			$display_name = $brand_name." ".$product_info_name;
			array_push($product_info_arr,$product_info_id);
			if($j >= "11"){
				$recently_reviewed_html .='<div class="ResSml">';
        	                $recently_reviewed_html .='<a href="'.$seo_model_url.'">';
                	        if(!empty($image_path)){
                        	        $recently_reviewed_html .='<img src="'.$image_path.'" alt="" width="98" height="56"/>';
	                        }else{
        	                        $recently_reviewed_html .='<img src="'.IMAGE_URL.'no-image.png" alt="" width="98" height="56"/>';
                	        }
                        	$recently_reviewed_html .='</a>';
	                        $recently_reviewed_html .='<div class="carNameSml"><a href="'.$seo_model_url.'">'.$display_name.'</a></div>';
        	                $recently_reviewed_html .='<div class="starRating"> <a href="'.$seo_model_url.'">'.$html.'</a> </div>';
                	        $recently_reviewed_html .='<div class="reviewsSml"><a href="'.$seo_model_url.'">'.$review_cnt.' Reviews</a></div>';
                        	$recently_reviewed_html .='<div class="clear"></div></div>';
			}
			$j++;
		}
	}
	echo $recently_reviewed_html;
	exit;
	//Recently Reviewed End
}

?>
