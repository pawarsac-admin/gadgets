function addNewCompareProduct(id){
	var brandid = document.getElementById('Brand_'+id).value; 
	if(!brandid){
		alert("Please select Brand.");
		return false;
	} else {
		var sBrandName	= $("#Brand_" + id +" :selected").text();
	}

	var modelid = document.getElementById('Model_'+id).value; 
	if(!modelid){
		alert("Please select Model.");
		return false;
	} else {
		var sModelName	= $("#Model_" + id +" :selected").text();
	}

	var sPhone	= $.trim(sBrandName) + " " + $.trim(sModelName) ;
	sPhone	= sPhone.replace(/\s\s+/g, ' ' );
	var productid = modelid;
	setCookieForCompare( productid , sPhone , 0 ,0);

	$("#compare_add_button_"+id).addClass('addButtonClick');
	$("#compare_add_button_"+id).attr('disabled', true);

	return false;
}

function QueryStr(sPhoneName,sPhoneId,catid,currenttab){
	var queryArr	= Array();
	var queryStr	= '';
	if(sPhoneId != ''){
		if(sPhoneName != ''){
			//queryArr.push('catid='+catid);
			queryArr.push(sPhoneName);
		}
		if(catid != ''){
			//queryArr.push('catid='+catid);
			queryArr.push(catid);
		}
		if(sPhoneId != ''){
			//queryArr.push('catid='+catid);
			queryArr.push(sPhoneId);
		}
		//queryArr.push('productids='+newCompareStr);                                
		if(currenttab != ''){
			//queryArr.push('fid='+currenttab);
			queryArr.push(currenttab);
		}                                
	}
	if(queryArr.length > 0){
		//queryStr = '?'+queryArr.join('&');
		queryStr = '/'+queryArr.join('/');
	}
	return queryStr;
}

function isArray(obj){
	if (obj.constructor.toString().indexOf("Array") == -1){
		return false;
	}else{
		return true;
	}
} 

function getModelByBrand(divId,surl,divname,param){
	var iBrndId=document.getElementById('Brand_'+divId).value;
	var str="action=model&brand_id="+iBrndId+"&Rand="+Math.random();
	var data = $.ajax({url: siteURL+surl,data: str,async: false}).responseText;		
	$('#Model_'+divId).html(data);	
	$('#ModelVariant_'+divId).empty().append('<option value="">--Select Variant--</option>');
	return true;
}

function getVariantByBrandModel(divId,surl,divname,param){
	var iBrndId=document.getElementById('Brand_'+divId).value;
	var iModelId=document.getElementById('Model_'+divId).value;
	var str="action=model&brand_id="+iBrndId+"&product_name_id="+iModelId+"&Rand="+Math.random();
	var data = $.ajax({url: siteURL+surl,data: str,async: false}).responseText;		
	$('#ModelVariant_'+divId).html(data);	
	return true;
}

/////////////////////////// For phone list page

var dbug = 0;

function d_a(ary) {
    var beg = next_entry(ary) - 1;
    for (var i = beg; i > -1; i--) {
        ary[i] = null;
    }
}
function init_array() {
    if (dbug) alert('init_cookie');
    var ary = new Array(null);
    return ary;
}
function set_cookie(name, value, expires) {
    if (dbug) alert('set_cookie');
    if (!expires) expires = new Date();
    document.cookie = name + '=' + escape(value) + '; expires=' + expires.toGMTString() + '; path=/';
}
function get_cookie(name) {
    if (dbug) alert('get_cookie');
    var dcookie = document.cookie;
    var cname = name + "=";
    var clen = dcookie.length;
    var cbegin = 0;
    while (cbegin < clen) {
        var vbegin = cbegin + cname.length;
        if (dcookie.substring(cbegin, vbegin) == cname) {
            var vend = dcookie.indexOf(";", vbegin);
            if (vend == -1) vend = clen;
            return unescape(dcookie.substring(vbegin, vend));
        }
        cbegin = dcookie.indexOf(" ", cbegin) + 1;
        if (cbegin == 0) break;
    }
    return null;
}
function del_cookie(name) {
    if (dbug) alert('del_cookie');
    document.cookie = name + '=' + '; expires=Thu, 01-Jan-70 00:00:01 GMT; path=/';
}
function get_array(name, ary) {
    if (dbug) alert('get_array');
    d_a(ary);
    var ent = get_cookie(name);
    if (ent) {
        i = 1;
        while (ent.indexOf('^') != '-1') {
            ary[i] = ent.substring(0, ent.indexOf('^'));
            i++;
            ent = ent.substring(ent.indexOf('^') + 1, ent.length);
        }
    }
}
function in_array(name, ary, needle) {
    if (dbug) alert('in_array');
    d_a(ary);
    var ent = get_cookie(name);
    if (ent) {
        i = 1;
        while (ent.indexOf('^') != '-1') {
            ary[i] = ent.substring(0, ent.indexOf('^'));
			if (ary[i] == needle ){
				return i;
			}
			i++;
            ent = ent.substring(ent.indexOf('^') + 1, ent.length);
        }
    }
	return -1;
}
function set_array(name, ary, expires) {
    if (dbug) alert('set_array');
    var value = '';
    for (var i = 1; ary[i]; i++) {
        value += ary[i] + '^';
    }
    set_cookie(name, value, expires);
}
function del_entry(name, ary, pos, expires) {
    if (dbug) alert('del_entry');
    var value = '';
    get_array(name, ary);
    for (var i = 1; i < pos; i++) {
        value += ary[i] + '^';
    }
    for (var j = pos + 1; ary[j]; j++) {
        value += ary[j] + '^';
    }
    set_cookie(name, value, expires);
}
function next_entry(ary) {
    if (dbug) alert('next_entry');
    var j = 0;
    for (var i = 1; ary[i]; i++) {
        j = i
    }
    return j + 1;
}

function debug_on() {
    dbug = 1;
}
function debug_off() {
    dbug = 0;
}
function dump_cookies() {
    if (document.cookie == '') document.write('No Cookies Found');
    else {
        thisCookie = document.cookie.split('; ');
        for (i = 0; i < thisCookie.length; i++) {
            document.write(thisCookie[i] + '<br \/>');
        }
    }
}

//// Initialization 
var maxLimit	= 4;
var timeToKeep	= 60000 * 60; // one minute * 60
var expires		= new Date();
expires.setTime(expires.getTime() + timeToKeep); 

var cookie_pid		= 'ck_pid'; // give the cookie a name
var cookie_pname	= 'ck_pname'; // give the cookie a name

//// Function to set Id in cookie
function setCookieForCompare( pid , pname , flag, unset ){

	
	var cIndex		= -1;
	var arrPID		= init_array();
	var arrPNAME	= init_array();
	get_array(cookie_pid, arrPID);
	get_array(cookie_pname, arrPNAME);
	var num = next_entry(arrPID);

	//alert( 'ExistingCookie : ' + arrPID + " & num : " + num );
	
	if( num - 1 > 0 ){
		cIndex	= in_array( cookie_pid, arrPID, pid );
		//alert('cIndex : ' + cIndex);
	}

	var xxxx	= false ;
	
	if( unset == 1 ){ /// Directly delete the cookie
	
	} else {
		if( flag == 1 ){ // from phone listing page
			xxxx = $("#compare_product_"+pid+"_"+pid).is(":checked");
		} else { // from compare page , so set the cookie
			xxxx = true;
		}
	}

	if( xxxx == true ){

		if( num > maxLimit ) { 
			alert("You can select maximum 4 Products to compare."); 
			$("#compare_product_"+pid+"_"+pid).attr('checked', false);
			return false;
		}

		//alert("In true");
		if( cIndex > -1 ){
			alert("This Phone is already in compare List");
		} else {
			//alert("Adding....");
			arrPID[num] = pid;
			set_array(cookie_pid, arrPID, expires);	

			arrPNAME[num] = pname;
			set_array(cookie_pname, arrPNAME, expires);
		
		}
	} else if( xxxx == false ){
		if( cIndex > -1 ){
			//alert("Deleting....");
			del_entry(cookie_pid, arrPID, cIndex, expires);
			del_entry(cookie_pname, arrPNAME, cIndex, expires);
			$("#compare_product_"+pid+"_"+pid).attr('checked', false);
		}
	}

	showSelectedProducts();
}

function showSelectedProducts(){
		$('#op').text('');
		var marray	= init_array();
		get_array(cookie_pname, marray);

		var marray1	= init_array();
		get_array(cookie_pid, marray1);

		$('#floating-box').empty();
		
		var maxCnt = next_entry(marray);

		if( maxCnt > 1 ){
			for (var i=1; i< maxCnt; i++) {
				$('#floating-box').append( "<span class='remProduct'><img class='pointer' width='12' height='12' src='/images/cross.png'  onclick='setCookieForCompare(" + marray1[i] + ",\"xyz\",0,1)' /> " + marray[i] + "<span>" );
			}
		
			if ($("#floating-box").is(":hidden")) {
				$("#floating-box").fadeIn("slow");
			}
		} else {
				$("#floating-box").fadeOut("slow");
		}
}

function compareIt(){
	var arrpid	=  Array();
	var arrpname =  Array();
	var marray_pid		= init_array();
	get_array(cookie_pid, marray_pid );

	for (var i=1; i< next_entry(marray_pid); i++) {
			arrpid[i-1] = marray_pid[i];
	}
	
	var marray_pname	= init_array();
	get_array(cookie_pname, marray_pname );

	for (var i=1; i< next_entry(marray_pname); i++) {
			arrpname[i-1] = marray_pname[i];
	}
	sPhoneName		= implode ("-Vs-", arrpname) ;
	sPhoneName		= sPhoneName.replace(/\s\s+/g, ' ' );
	sPhoneName		= sPhoneName.replace(/\s/g, '-' );
	//alert(sPhone);

	sPhoneId		= implode (",", arrpid) ;
	//alert(sPhoneId);
	var queryStr	= QueryStr(sPhoneName,sPhoneId,catid,currenttab)
	var redirectUrl = web_url+'Compare-mobiles'+queryStr; 
	setTimeout(function(){location.href = redirectUrl;},250);
	return true;
}

function implode (glue, pieces) {
    var i = '',
        retVal = '',
        tGlue = '';
    if (arguments.length === 1) {
        pieces = glue;
        glue = '';
    }
    if (typeof(pieces) === 'object') {
        if (Object.prototype.toString.call(pieces) === '[object Array]') {
            return pieces.join(glue);
        } 
        for (i in pieces) {
            retVal += tGlue + pieces[i];
            tGlue = glue;
        }
        return retVal;
    }
    return pieces;
}


function compareIt1(){
	var arrProduct		=  Array();
	var arrProductId	=  Array();
	var modelID	= productName = '';
	
	var j	= 0;
	for (var i=1; i< 5; i++) {
			modelId = $.trim($('#Model_'+i).val());
			//alert( "Model ID :" + i + ":"+modelId);
			
			if( modelId != '' ){
				
				strModel	= $('#Model_'+i+" option:selected").text();
				//alert("strModel :"+strModel);
				brandId		= $.trim($('#Brand_'+i).val());
				
				//alert("Brand ID :"+ i + ":"+brandId);
				if( brandId != ''  && brandId != 'undefined' ){
					strBrand =	$('#Brand_'+i +" option:selected").text();

					if($.trim(strBrand)==''){
						strBrand =	$('#Model_name_' + i ).val();
						productName = $.trim(strBrand) ;
					} else {
						productName = $.trim(strBrand) + '-' + $.trim(strModel) ;
					}
					//alert("productName :"+productName);
					
					arrProduct[j]		= productName ;
					arrProductId[j++]	= modelId;
				}
			}
	}

	sPhoneName		= implode ("-Vs-", arrProduct) ;
	sPhoneName		= sPhoneName.replace(/\s\s+/g, ' ' );
	sPhoneName		= sPhoneName.replace(/\s/g, '-' );
	//alert(sPhoneName);

	sPhoneId		= implode (",", arrProductId) ;
	//alert(sPhoneId);

	var queryStr	= QueryStr(sPhoneName,sPhoneId,catid,currenttab)
	var redirectUrl = siteURL +'Compare-mobiles'+queryStr; 
	setTimeout(function(){location.href = redirectUrl;},250);
	return true;
}