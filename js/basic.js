$(document).ready(function()
{
    $(".defaultText").focus(function(srcc)
    {
        if ( $.trim( $(this).val() ) == $(this)[0].title)
        {
            $(this).removeClass("defaultTextActive");
            $(this).val("");
        }
    });
    
    $(".defaultText").blur(function()
    {
        if ( $.trim( ( $(this).val() ) ) == "" )
        {
            $(this).addClass("defaultTextActive");
            $(this).val($(this)[0].title);
        }
    });
    
    $(".defaultText").blur();        
});

//Email Validator
function validateEmail(email) 
{ 
	var re = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
 	return email.match(re) ;
}

//Email Validator
function emailValidator(email){
	//var email = document.getElementById(id).value;
	var emailExp = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
	if(email.match(emailExp)){
		return true;
	}else{
		//alert(helperMsg);
		//document.getElementById(id).focus();
		return false;
	}
}

function submitSearch(){
	document.researchfrm.submit();
}

function showContainer(){
	jQuery('#loader-div').removeClass('show').addClass('hide');
}

// Header Serach Box
$(function(){
	$("#searchtab1,#searchtab2,#searchtab3,#searchtab4").click(function() {
	 $(".tab a").removeClass("active");
     $(this).addClass("active");
});

// Sidebar Top rated
$("#topRated").click(function() {
 $("#topRatedContent").show();
 $("#topSellContent").hide();
 $("#topSell").removeClass("active");
 $(this).addClass("active");
 });
$("#topSell").click(function() {
 $("#topSellContent").show();
 $("#topRatedContent").hide();
 $("#topRated").removeClass("active");
 $(this).addClass("active");
 });
});

function trim(sValue) {
	return sValue.replace(/^\s+|\s+$/g,"");
}
//function left trim
function ltrim(sValue) {
	return sValue.replace(/^\s+/,"");
}
//function right trim 
function rtrim(sValue){
	return sValue.replace(/\s+$/,"");
}