// onload calls

// window positions
var x_pos=($(window).width() - $('#loadingimg').width()) / 2;
var y_pos=($(window).height() - $('#loadingimg').height()) / 2;

function sortby(){
	//var sortby = document.getElementById('select_sort_by').value;
	//document.getElementById('sortproduct').value = sortby;
	var sortby = $('#select_sort_by').val();
	$('#sortproduct').val(sortby);
	loadBox();
	$('#findcars').submit();
}
// paging 
function getAjaxData(nextpage){
	$('#pageoffset').val(nextpage);
	var sortby = $('#select_sort_by').val();
	$('#sortproduct').val(sortby);
	loadBox();
	//$('#findcars').submit();
	setTimeout(function(){$('#findcars').submit();},0);
}
// loading box
function loadBox(){
	y_pos = y_pos + $(window).scrollTop();
	$('#loadingimg').show();
	$('#loadingimg').css('position', 'absolute');
	$('#loadingimg').css('margin-left', x_pos);
	$('#loadingimg').css('margin-top', y_pos);
	$('#loadingimg').css('z-index', 999);
}
function clearSearchCriteria(id,searchElement){
	if(document.getElementById(id).checked == true){
		document.getElementById(id).checked = false;
	}
	removeChecked(searchElement);
	loadBox();
	setTimeout(function(){$('#findcars').submit();},0);
	return true;
}
var sliderPriceIndexArr = new Array('1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','25','30','35','40','45','50','55','60','70','80','90','1.0','1.5','2.0','2.5','3.0','3.5','4.0','4.5','5.0');
function getPriceIndex(price){
	var len = sliderPriceIndexArr.length;
	for(i=0; i<len; i++){
		if(sliderPriceIndexArr[i] == price) break;
	}
	return i;
}
function getSliderValueByIndex(i){
	return sliderPriceIndexArr[i];
	if(index>=21) { return arr[index-1]; } else{if(index==0){return index=1;}else{return index;}}
}
function toggleMatchingProduct(sid,plusimgsrc,minusimgsrc){		
	var currentTime = new Date();
	var currentTime = currentTime.getSeconds();
	var sdiv = '#div_hide_'+sid;
	$(sdiv).slideToggle('slow');
	if(plusimgsrc == document.getElementById("displayplustree_"+sid).src){
		document.getElementById("displayplustree_"+sid).src = minusimgsrc;
	}else if(minusimgsrc == document.getElementById("displayplustree_"+sid).src){
		document.getElementById("displayplustree_"+sid).src = plusimgsrc;
	}	
	return true;
}
function handleSearchLoadingBox(){
	$("#findcars input:checkbox").click(function(){
		submitUrl(urlArr);
		loadBox();
		$('#findcars').submit();
	});	
}
function scrollTo(hash) {
    location.hash = "#" + hash; // used to add anchar on carfinder.xsl.
}

$('document').ready(function(){ // DOM ready
	
	//scrollTo('anchorbdy'); // used to add anchar on carfinder.xsl.

	// search result view more
	//toggleMatchingProduct(); // function is used hide/show matching products.
	
	//form customization
	/*
	$(function(){
		$("input, textarea, select, button").uniform();
	});
	*/	
	handleSearchLoadingBox(); // function is used to handle search criteria and loading box.

	/* Price range slider code */
	var min_price = document.getElementById('mnprice').value;
	var min_unit = document.getElementById('mnpriceunit').value;

	var max_price = document.getElementById('mxprice').value;	
	var max_unit = document.getElementById('mxpriceunit').value;

	var minValueIndex = getPriceIndex(min_price);
	var maxValueIndex = getPriceIndex(max_price);
	
	var minValue = 1;
	var maxValue = sliderPriceIndexArr.length;

	$("#slider").slider({
		range: true,
		min: minValue,
		max: maxValue,
		step: 1,
		values:[minValueIndex,maxValueIndex],
		slide: function(event, ui) {

			//start code to intialize slider default price range on change slider values.
			var changedMinIndex = ui.values[0];
			if(changedMinIndex == 1){
				changedMinIndex = 0;
			}
			var changedMaxIndex = ui.values[1];
			if(changedMaxIndex == sliderPriceIndexArr.length){
				changedMaxIndex = parseInt(changedMaxIndex)-parseInt(1);
			}
			
			var changedMinPrice = getSliderValueByIndex(changedMinIndex);
			$("#minRange").html(changedMinPrice);
			if(changedMinPrice.indexOf('.') == -1){				
				min_unit = "thousand";
			}else{
				min_unit = "lakh";
			}
			var changedMaxPrice = getSliderValueByIndex(changedMaxIndex);
			$("#maxRange").html(changedMaxPrice);
			if(changedMaxPrice.indexOf('.') == -1){
				max_unit = "thousand";
			}else{
				max_unit = "lakh";				
			}

			$("#mnprice").val(changedMinPrice);
			$("#minUnit").html('&nbsp;'+min_unit);
			$("#mnpriceunit").val(min_unit);
			
			$("#mxprice").val(changedMaxPrice);
			$("#maxUnit").html('&nbsp;'+max_unit);
			$("#mxpriceunit").val(max_unit);
		},
		change: function(event, ui) {
			//start code to intialize slider default price range on change slider values.
			var changedMinIndex = ui.values[0];
			if(changedMinIndex == 1){
				changedMinIndex = 0;
			}
			var changedMaxIndex = ui.values[1];
			if(changedMaxIndex == sliderPriceIndexArr.length){
				changedMaxIndex = parseInt(changedMaxIndex)-parseInt(1);
			}
			
			var changedMinPrice = getSliderValueByIndex(changedMinIndex);
			$("#minRange").html(changedMinPrice);
			if(changedMinPrice.indexOf('.') == -1){				
				min_unit = "thousand";
			}else{
				min_unit = "lakh";
			}
			var changedMaxPrice = getSliderValueByIndex(changedMaxIndex);
			$("#maxRange").html(changedMaxPrice);
			if(changedMaxPrice.indexOf('.') == -1){
				max_unit = "thousand";
			}else{
				max_unit = "lakh";				
			}

			$("#mnprice").val(changedMinPrice);
			$("#minUnit").html('&nbsp;'+min_unit);
			$("#mnpriceunit").val(min_unit);
			
			$("#mxprice").val(changedMaxPrice);
			$("#maxUnit").html('&nbsp;'+max_unit);
			$("#mxpriceunit").val(max_unit);
			if(submiturl == '1'){
				submitUrl(urlArr);
				loadBox();
				//addChecked('');
				$('#findcars').submit();
			}
		}
	});
	//start code to intialize slider default price range on page load.
	$("#minRange").html(min_price);
	$("#maxRange").html(max_price);
	$("#minUnit").html('&nbsp;'+min_unit);
	$("#maxUnit").html('&nbsp;'+max_unit);

	
	$("#mnprice").val(min_price);
	$("#mnpriceunit").val(min_unit);
			
	$("#mxprice").val(max_price);
	$("#mxpriceunit").val(max_unit);

	$('#clearall').click(function(){
		$("#findcars input:checkbox").parent().removeClass('checked');
		$("#findcars input:checkbox").removeAttr('checked');
		$('#sc').remove();
		loadBox();
		//location.href = web_url+seo_car_finder;	
		setTimeout(function(){location.href = web_url+seo_car_finder;},0);
	});

	$('#loadingimg').hide();
	// hide and show for left search criterion
	$('.hid').click(function(){ $(this).parent().parent().next().slideToggle('slow'); });
});
function getPriceArr(){

	var mnprice = document.getElementById('mnprice').value;
	var mnpriceunit = document.getElementById('mnpriceunit').value;

	if(mnpriceunit == 'lakh'){
		mnprice = parseFloat(mnprice)*parseInt(100000);				
	}else{
		mnprice = parseFloat(mnprice)*parseInt(1000);
	}
	var mxprice = document.getElementById('mxprice').value;
	var mxpriceunit = document.getElementById('mxpriceunit').value;

	if(mxpriceunit == 'lakh'){
		mxprice = parseFloat(mxprice)*parseInt(100000);
	}else{
		mxprice = parseFloat(mxprice)*parseInt(1000);
	}		
	var price = mnprice+'-'+mxprice;
	urlArr['price'] = price;
	return urlArr;	
}
function arrSearchReplace(searchElement){
	var len = urlArr.length;
	for(i=0;i<len;i++){
		if(urlArr[i] == searchElement){			
			urlArr.splice(i,1);
			return urlArr;
		}
	}	
	return urlArr;
}
function addBrandChecked(newElement,brand_id){	
	if(newElement){
		brandArr.push(newElement);
		var brandStr = brandArr.join('+.');
		urlArr['brand'] = brandStr;
	}	
	return false;
}
function removeBrandChecked(searchElement,brand_id,submiturl){	
	var len = brandArr.length;
	var brandStr;
	for(i=0;i<len;i++){
		if(brandArr[i] == searchElement){
			brandArr.splice(i,1);
		}
	}		
	if(brandArr.length > 0){
		brandStr = brandArr.join('+.');
		urlArr['brand'] = brandStr;
	}else{
		delete urlArr['brand'];
	}
	if(submiturl){
		var queryStr = submitUrl(urlArr);
		setTimeout(function(){location.href = queryStr;},0);
	}
	return true;
}
function removeAllBrands(){
	 delete urlArr['brand'];
}
function addFuelTypeFeature(feature_name){
	if(feature_name){
		fuelTypeArr.push(feature_name);	
		var fuelTypeStr = fuelTypeArr.join('+.');
		urlArr['fuel'] = fuelTypeStr;
	}
	return false;
}
function removeFuelTypeFeature(searchElement){
	var len = fuelTypeArr.length;
	var fuelTypeStr;
	for(i=0;i<len;i++){
		if(fuelTypeArr[i] == searchElement){
			fuelTypeArr.splice(i,1);
		}
	}	
	if(fuelTypeArr.length > 0){
		fuelTypeStr = fuelTypeArr.join('+.');
		urlArr['fuel'] = fuelTypeStr;
	}else{
		delete urlArr['fuel'];
	}
	return false;
}
function addImpFeature(feature_name){
	if(feature_name){
		impFeatureArr.push(feature_name);
		var impFeatureStr = impFeatureArr.join('+.');
		urlArr['impfeature'] = impFeatureStr;
	}
	return false;
}
function removeImpFeature(searchElement){
	var len = impFeatureArr.length;
	var impFeatureStr;
	for(i=0;i<len;i++){
		if(impFeatureArr[i] == searchElement){
			impFeatureArr.splice(i,1);
		}
	}	
	if(impFeatureArr.length > 0){
		impFeatureStr = impFeatureArr.join('+.');
		urlArr['impfeature'] = impFeatureStr;
	}else{
		delete urlArr['impfeature'];
	}
	return false;
}
function addBodyStyleFeature(feature_name){
	if(feature_name){
		bodyStyleArr.push(feature_name);
		var bodyStyleStr = bodyStyleArr.join('+.');
		urlArr['style'] = bodyStyleStr;
	}
	return false;
}
function removeBodyStyleFeature(searchElement){
	var len = bodyStyleArr.length;
	var bodyStyleStr;
	for(i=0;i<len;i++){
		if(bodyStyleArr[i] == searchElement){
			bodyStyleArr.splice(i,1);
		}
	}
	if(bodyStyleArr.length > 0){
		bodyStyleStr = bodyStyleArr.join('+.');
		urlArr['style'] = bodyStyleStr;
	}else{
		delete urlArr['style'];
	}
	return false;
}
function addTransmissionFeature(feature_name){
	if(feature_name){
		transmissionArr.push(feature_name);
		var transmissionTypeStr = transmissionArr.join('+.');
		urlArr['transmission'] = transmissionTypeStr;
	}
	return false;
}
function removeTransmissionFeature(searchElement){
	var len = transmissionArr.length;
	var transmissionTypeStr;
	for(i=0;i<len;i++){
		if(transmissionArr[i] == searchElement){
			transmissionArr.splice(i,1);
		}
	}
	if(transmissionArr.length > 0){
		transmissionTypeStr = transmissionArr.join('+.');
		urlArr['transmission'] = transmissionTypeStr;
	}else{
		delete urlArr['transmission'];
	}
	return false;
}
function addSeatingCapacityFeature(feature_name){
	if(feature_name){
		seatingCapcityArr.push(feature_name);
		var seatingCapacityStr = seatingCapcityArr.join('+.');
		urlArr['seating'] = seatingCapacityStr;
	}
	return false;
}
function removeSeatingCapacityFeature(searchElement){
	var len = seatingCapcityArr.length;
	var seatingCapacityStr;
	for(i=0;i<len;i++){
		if(seatingCapcityArr[i] == searchElement){
			seatingCapcityArr.splice(i,1);
		}
	}
	if(seatingCapcityArr.length > 0){
		seatingCapacityStr = seatingCapcityArr.join('+.');
		urlArr['seating'] = seatingCapacityStr;
	}else{
		delete urlArr['seating'];
	}
	return false;
}
function addFeatureChecked(feature_name,feature_group_name){
	if(feature_group_name == 'Fuel Type'){
		addFuelTypeFeature(feature_name);
	}else if(feature_group_name == 'Important features'){
		addImpFeature(feature_name);
	}else if(feature_group_name == 'Body Style'){
		addBodyStyleFeature(feature_name);
	}else if(feature_group_name == 'Transmission'){
		addTransmissionFeature(feature_name);
	}else if(feature_group_name == 'Seating Capacity'){
		addSeatingCapacityFeature(feature_name);
	}
}
function removeFeatureChecked(feature_name,feature_group_name,submiturl){
	if(feature_group_name == 'Fuel Type'){
		removeFuelTypeFeature(feature_name);
	}else if(feature_group_name == 'Important features'){
		removeImpFeature(feature_name);
	}else if(feature_group_name == 'Body Style'){
		removeBodyStyleFeature(feature_name);
	}else if(feature_group_name == 'Transmission'){
		removeTransmissionFeature(feature_name);
	}else if(feature_group_name == 'Seating Capacity'){
		removeSeatingCapacityFeature(feature_name);
	}
	if(submiturl){
		var queryStr = submitUrl(urlArr);
		setTimeout(function(){location.href = queryStr;},0);
	}
}
function submitUrl(urlArr){
	if(urlArr.length <=0){
		getPriceArr();
	}
	var newurlArr = Array();
	for(var key in urlArr) {		
		newurlArr.push(key+'-'+urlArr[key]);
	}
	var queryStr = web_url+seo_car_finder+'/'+newurlArr.join('/');	
	document.findcars.action = queryStr;
	return queryStr;
}
function togglePlusMinusImg(id){
	var plusimg = web_url+'images/plus.gif';
	var minusimg = web_url+'images/minus.gif';
	var currimg = document.getElementById(id).src;
	currimgIndex = parseInt(currimg.indexOf('minus'));
	if(currimgIndex > 0){
		document.getElementById(id).src = plusimg;
	}
	currimgIndex = parseInt(currimg.indexOf('plus'));
	if(currimgIndex > 0){
		document.getElementById(id).src = minusimg;
	}
	return true;
}
//Shadow Popup
function openFeatueDescriptionPop(id,desc){
document.getElementById('feature_desc_popup').innerHTML = desc;
var winW=0, winH=0;
if (parseInt(navigator.appVersion)>3) {
 if (navigator.appName=="Netscape" || navigator.appName=="Opera") {
  winW = document.body.clientWidth;
  winH = document.body.offsetHeight;
 }
 if (navigator.appName.indexOf("Microsoft")!=-1) {
  winW = document.body.offsetWidth;
  winH = document.body.offsetHeight;
 }
}
//alert(winW+"-------------------"+ winH);
	if(winH<600){
		document.getElementById('overLay').style.display="block";
		//document.getElementById('overLay').style.width=winW+'px';
		document.getElementById('overLay').style.height='600px';
		document.getElementById('logBx'+id).style.display="block";
	}
	else{
		document.getElementById('overLay').style.display="block";
		document.getElementById('overLay').style.width=winW+'px';
		document.getElementById('overLay').style.height=winH+'px';
		document.getElementById('logBx'+id).style.display="block";
	}
}	
