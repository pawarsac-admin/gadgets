function GetProductPrice(iProdId,surl,divname,param){
	var iCityId=document.getElementById('city').value;	//alert(iCityId);
	
	var str="action=price&product_id="+iProdId+"&city_id="+iCityId+"&Rand="+Math.random();
	//alert(siteURL+surl+"?"+str);
	$.ajax({
			url: siteURL+surl,
			data: str,
			success: function(data){
				//alert(data);
                document.getElementById(divname).innerHTML = data;
            },
            async:false
        });
}

function GetModelPrice(iProdId,surl,divname,param){
	var iCityId=document.getElementById('city').value;	//alert(iCityId);
	
	var str="action=price&product_name="+iProdId+"&city_id="+iCityId+"&Rand="+Math.random();
	//alert(siteURL+surl+"?"+str);
	$.ajax({
			url: siteURL+surl,
			data: str,
			success: function(data){
				//alert(data);
                document.getElementById(divname).innerHTML = data;
            },
            async:false
        });
}
function UpdateLike(video_id,type,type_id,surl){
	var str="action=videolike&video_id="+video_id+"&type="+type+"&type_id="+type_id+"&Rand="+Math.random();
	//alert(siteURL+surl+"?"+str);
	if(is_liked == '1'){
		alert("You've already rated!");
		return false;
	}
	$.ajax({
			url: siteURL+surl,
			data: str,
			success: function(data){
				//alert(data);
                document.getElementById('likediv').innerHTML = data;
				is_liked = '1';
            },
            async:false
        });
}

function DoHelpFul(review_id,flag,divname){
	_gaq.push(['_trackEvent', 'Userfullreviews', 'Voteforuserreview', '','_trackPageLoadTime']);
	var surl="do_rate_review.php?";
	var str="action=optionrate&review_id="+review_id+"&flag="+flag+"&Rand="+Math.random();
	//alert(siteURL+surl+"?"+str);
	$.ajax({
			url: siteURL+surl,
			data: str,
			success: function(data){
				//alert(data);
				if(data=='done'){
				  alert('Already Rated!');
				}else{
					document.getElementById(divname).innerHTML = data;
				}
            },
            async:false
        });
}



function getColorOption(iColorId,iProdId,surl,divname,param){
	
	//alert(iCityId);
	//siteURL="http://localhost/autos/";
	var str="action=color&product_id="+iProdId+"&color_id="+iColorId+"&Rand="+Math.random();
	//alert(siteURL+surl+"?"+str);
	$.ajax({
			url: siteURL+surl,
			data: str,
			success: function(data){
				//alert(data);
                document.getElementById(divname).innerHTML = data;
            },
            async:false
        });
}

function getModelByBrand(iModelId,surl,divname,param){
	var iBrndId=document.getElementById('Brand').value;
	if(iBrndId != ""){
		var str="action=model&brand_id="+iBrndId+"&product_name_id="+iModelId+"&Rand="+Math.random();
		var data = $.ajax({url: siteURL+surl,data: str,async: false}).responseText;		
		$('#Model').html(data);	
		$('#ModelVariant').empty().append('<option value="">--Select Variant--</option>');
		document.getElementById('Model').disabled=false;
	}else{
		$('#Model').empty().append('<option value="">All Models</option>');
		$('#ModelVariant').empty().append('<option value="">All Variants</option>');
	}
	
	/*
	if($.uniform.update){
		$.uniform.update("select#Model");
	}
	if($.uniform.update){
		$.uniform.update("select#ModelVariant");
	}
	*/
}

function getVariantByBrandModel(product_id,surl,divname,param){
	var iBrndId=document.getElementById('Brand').value;
	var iModelId=document.getElementById('Model').value;
	if(iModelId != ""){
		var str="action=model&brand_id="+iBrndId+"&product_name_id="+iModelId+"&product_id="+product_id+"&Rand="+Math.random();
		var data = $.ajax({url:  siteURL+surl,data: str,async: false}).responseText;		
		$('#ModelVariant').html(data);
		document.getElementById('ModelVariant').disabled=false;
	}else{
		$('#ModelVariant').empty().append('<option value="">--Select Variant--</option>');
	}	
	/*
	if($.uniform.update){
		$.uniform.update("select#ModelVariant");
	}
	*/
}

function getModelImage(product_id,divname,param,image_url){
        var iBrndId=document.getElementById('Brand').value;
        var iModelId=document.getElementById('Model').value;
	var data = "";
        if(iModelId != ""){
		var loading = "<div style='margin:40px 70px 0 0'><img src='"+image_url+"ajax-loader1.gif' height='20px' width='20px'/></div>";
                $('#model_image').html(loading);
                var str="action=model&brand_id="+iBrndId+"&product_name_id="+iModelId+"&Rand="+Math.random();
                data = $.ajax({url:  siteURL+"get_model_image.php",data: str,async: false}).responseText;
                $('#model_image').html(data);
        }else{
		$('#model_image').html(data);
	}
}

function submitVersion(){
	var seo_str;
	var productid = document.getElementById('ModelVariant').value;
	var model = document.getElementById('Model').value;
	var brand = document.getElementById('Brand').value;

	if(!brand && !model && !productid){
		seo_str = siteURL+seo_car_finder;
	}else if(brand != '' && model != '' && !productid){
		seo_str = get_model_page_url();
	}else if(brand != '' && model != '' && productid != ''){
		seo_str = get_variant_page_url();
	}else{
		seo_str = siteURL+seo_car_finder;
	}
	
	/*
	if(!brand){ 
		alert("Please select Brand");
		return false;
	}
	if(!model){ 
		alert("Please select Model");
		return false;
	}
	if(!productid){ 
		alert("Please select Model variant");
		return false;
	}
	*/

	//var Seostr=
	
	//alert(seo_str);
	//return false;
	setTimeout(function(){location.href = seo_str;},0);
	//window.location.href = seo_str;
	return true;
}
function submitModel(){
	
	var productid = document.getElementById('ModelVariant').value;
	var model = document.getElementById('Model').value;
	var brand = document.getElementById('Brand').value;
	
	
	if(!brand){ 
		alert("Please select Brand");
		return false;
	}
	if(!model){ 
		alert("Please select Model");
		return false;
	}
	if(!productid){ 
		alert("Please select Model variant");
		return false;
	}
	
	//var Seostr=
	
	//alert(seo_str);
	//return false;
	var seo_str = get_model_page_url();
	//window.location.href = seo_str;
	setTimeout(function(){location.href = seo_str;},0);
	return true;
}
function get_variant_page_url(){
	var productid = document.getElementById('ModelVariant').value;
	var selObjVar = document.getElementById('ModelVariant');
	var selIndex = selObjVar.selectedIndex;
	var Variant = selObjVar.options[selIndex].text;
	
	Variant = Variant.replace(" ","-");
	

	var selObjModel = document.getElementById('Model');
	var selIndex = selObjModel.selectedIndex;
	var Model = selObjModel.options[selIndex].text;
	
	Model = Model.replace(" ","-");
	
	var selObjBrand = document.getElementById('Brand');
	var selIndex = selObjBrand.selectedIndex;
	var Brand = selObjBrand.options[selIndex].text;
	
	Brand = Brand.replace(" ","-");
	
	var seo_str= seo_web_url+"/"+Brand+"-cars/"+Brand+"-"+Model+"/"+Variant+"/Overviews/"+productid;
	return seo_str;
}
function get_model_page_url(){
	var model = document.getElementById('Model').value;
	var brand = document.getElementById('Brand').value;
	
	var selObjModel = document.getElementById('Model');
	var selIndex = selObjModel.selectedIndex;
	var Model = selObjModel.options[selIndex].text;
	//Model = customEscape(Model);
	//Model = encodeURIComponent(Model);
	Model = Model.replace(" ","-");
	
	var selObjBrand = document.getElementById('Brand');
	var selIndex = selObjBrand.selectedIndex;
	var Brand = selObjBrand.options[selIndex].text;
	
	//Brand = customEscape(Brand);
	//Brand = encodeURIComponent(Brand);
	Brand = Brand.replace(" ","-");

	//Model/Nano
	var seo_str= seo_web_url +"/"+Brand+"-cars/"+Brand+"-"+Model+"/Model/"+Model+"/"+model;
	return seo_str;
}

function customEscape(eurl) {
    eurl = escape(eurl);
	//alert(eurl);
    eurl = eurl.replace(/-/g, '+.');
	///alert(eurl);
    return eurl;
}


function getOnRoadPrice(){
	_gaq.push(['_trackEvent', 'On Road Price', 'OnRoadPrice', '','_trackPageLoadTime']);	
	var productid = document.getElementById('ModelVariant').value;
	var model = document.getElementById('Model').value;
	var brand = document.getElementById('Brand').value;
	var ierror ="";var serror=''; var cntErr=0;
	if(!brand){ 
		//alert("Please select Brand");
		serror +="Please select Brand\n\r";
		cntErr++; //return false;
	}
	if(!model){ 
		//alert("Please select Model");
		serror +="Please select a Model for the selected Car Brand\n\r";
		cntErr++; //return false;
	}
	if(!productid){ 
		//alert("Please select Model variant");
		serror +="Please select a Variant for the selected Car Model\n\r";
		cntErr++; //return false;
	}
	if(document.getElementById('Select_City').value=='0'){
		//alert("please select city");
		serror +="Please select city\n\r";
		cntErr++; //return false;
	}
	if(document.getElementById('select_period').value=='0'){
		//alert("please select period");
		serror +="Please select period\n\r";
		cntErr++; //return false;
	}
	//if(ierror >0){
	//	alert(serror);
	//	return false;
	//}
	//var Seostr=
	var selObjVar = document.getElementById('ModelVariant');
	var selIndex = selObjVar.selectedIndex;
	var Variant = selObjVar.options[selIndex].text;

	var Variantstr=Variant.split(" ");
	var varLen=Variantstr.length;
	var variantFormat='';
	if(varLen >0){
		for(var i=0;i<varLen;i++){
			if(variantFormat==''){
				variantFormat += Variantstr[i];
			}else{
				variantFormat +="-"+Variantstr[i];
			}
		}
	}else{ variantFormat= Variantstr[0];}


	var selObjModel = document.getElementById('Model');
	var selIndex = selObjModel.selectedIndex;
	var Model = selObjModel.options[selIndex].text;

	var Modelstr=Model.split(" ");
	var ModelLen=Modelstr.length;
	var ModelFormat='';
	if(ModelLen >0){
		for(var i=0;i<ModelLen;i++){
			if(ModelFormat==''){
				ModelFormat += Modelstr[i];
			}else{
				ModelFormat +="-"+Modelstr[i];
			}
		}
	}else{ ModelFormat= Modelstr[0];}

	var selObjBrand = document.getElementById('Brand');
	var selIndex = selObjBrand.selectedIndex;
	var Brand = selObjBrand.options[selIndex].text;

	var Brandstr=Brand.split(" ");
	var BrandLen=Brandstr.length;
	var BrandFormat='';
	if(BrandLen >0){
		for(var i=0;i<BrandLen;i++){
			if(BrandFormat==''){
				BrandFormat += Brandstr[i];
			}else{
				BrandFormat +="-"+Brandstr[i];
			}
		}
	}else{ BrandFormat= Brandstr[0];}

	var seo_str= seo_web_url +"/"+BrandFormat+"-cars/"+BrandFormat+"-"+ModelFormat+"/"+variantFormat+"/"+view_on_road_price_seourl+"/";

	
	var filter=/^.+@.+\..{2,3}$/;
	var ck_username = /^[A-Za-z0-9_ ]{3,20}$/;
	if(trim(document.getElementById('name').value)==''){
		//alert("please enter your name");
		serror +="Please enter your Name\n\r";
		cntErr++;
	}
	if(trim(document.getElementById('name').value)!=''){
		if (!ck_username.test(document.getElementById('name').value)) {
			//alert("please enter valid user name");
			serror +="Please enter valid User Name\n\r";
			cntErr++;
		}
	}

	if(trim(document.getElementById('email').value)==''){
		//alert("please enter your email");
		serror +="Please enter your Email\n\r";
		cntErr++;
	}else if(document.getElementById('email').value!=''){
		if(emailValidator(document.getElementById('email').value)==false){
			serror +="Please enter valid Email Id\n\r";
			//alert("please enter valid email address");
			cntErr++;
		}
	}
	if(document.getElementById('mobile').value!=''){
		var mLength=trim(document.getElementById('mobile').value).length;
		if(isNaN(trim(document.getElementById('mobile').value))){
			//alert("please enter valid mobile number");
			serror +="Please enter valid Mobile No.\n\r";
			cntErr++;
		}else if(mLength!=10){
			///alert("please enter 10 digit mobile number");
			serror +="please enter 10 digit mobile number\n\r";
			cntErr++;
		}
	}

	if(trim(document.getElementById('mobile').value)=='' &&  trim(document.getElementById('contact_no').value)==''){
		//alert("please enter your either mobile or contact number");
		serror +="Please enter your either Mobile or Contact Number\n\r";
		cntErr++;
	}
	if(trim(document.getElementById('contact_no').value)!=''){
		if(isNaN(trim(document.getElementById('contact_no').value))){
			//alert("please enter valid contact number");
			serror +="Please enter valid Contact Number\n\r";
			cntErr++;
		}
		//alert("asasasas"+document.getElementById('std_code').value+"------");
		var sLength = trim(document.getElementById('std_code').value).length;
		if(trim(document.getElementById('std_code').value)==""){
			//alert("please enter valid std code");
			serror +="Please enter valid std code\n\r";
			cntErr++;
		}else if(sLength>3){
			//alert("please enter 3 digit std code");
			serror +="Please enter valid std code\n\r";
			cntErr++;
		}
	}
	if(document.getElementById('std_code').value!=''){
		var sLength = trim(document.getElementById('std_code').value).length;
		if(isNaN(trim(document.getElementById('std_code').value))){
			//alert("please enter valid std code");
			serror +="Please enter valid std code\n\r";
			cntErr++;
		}else if(sLength>3){
			//alert("please enter 3 digit std code");
			serror +="Please enter 3 digit std code\n\r";
			cntErr++;
		}
	}
	if(cntErr>0){
		alert(serror);
		return false;
	}
	else{
		document.mymodel.action=seo_str;
		setTimeout(function(){document.mymodel.submit();}, 0);
		//window.location.href = seo_str;
		return true;
	}
	return true;
}

function submitVersionWidget(){
	
	var productid = document.getElementById('ModelVariant').value;
	var model = document.getElementById('Model').value;
	var brand = document.getElementById('Brand').value;
	
	
	if(!brand){ 
		alert("Please select Brand");
		return false;
	}
	if(!model){ 
		alert("Please select Model");
		return false;
	}
	if(!productid){ 
		alert("Please select Model variant");
		return false;
	}
	
	//var Seostr=
	
	//alert(seo_str);
	//return false;
	var seo_str = get_variant_page_url();
	//parent.location.href = seo_str;
	setTimeout(function(){parent.location.href = seo_str;},0);
	return true;
}

var testresults

function checkemail(str){
 var str=str;
 var filter=/^.+@.+\..{2,3}$/

 if (filter.test(str))
    testresults=true
 else {
    
    testresults=false
}
 return (testresults)
}

 function emailValidator(email){
	//var email = document.getElementById(id).value;
	var emailExp = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
	if(email.match(emailExp)){
		return true;
	}else{
		//alert(helperMsg);
		//document.getElementById(id).focus();
		return false;
	}
}


function trim(str)
{
    //if(!str || typeof str != 'string')
   //  return null;
    return str.replace(/^[\s]+/,'').replace(/[\s]+$/,'').replace(/[\s]{2,}/,' ');
}


function sFeatuerdNewsPagination(page,startlimit,cnt,filename,divid,category_id){
	//alert(page+","+startlimit+","+cnt+","+filename+","+divid+","+category_id);
	if(divid == ""){ return false; }
		var url = siteURL+filename;
		$.ajax({
			url: url,
			data: 'catid='+category_id+'&page='+page+'&startlimit='+startlimit+'&cnt='+cnt,
			success: function(data){
				//(data);
                document.getElementById(divid).innerHTML = data;
                document.getElementById(divid).style.display="block";
               },
            async:false
        });
	return true;
}

function sFeatuerdVideosPagination(page,startlimit,cnt,filename,divid,category_id){
	//alert(page+","+startlimit+","+cnt+","+filename+","+divid+","+category_id);
	if(divid == ""){ return false; }
		var url = siteURL+filename;
		$.ajax({
			url: url,
			data: 'catid='+category_id+'&page='+page+'&startlimit='+startlimit+'&cnt='+cnt,
			success: function(data){
				//(data);
                document.getElementById(divid).innerHTML = data;
                document.getElementById(divid).style.display="block";
               },
            async:false
        });
	return true;
}

function sFeatuerdDataPagination(page,startlimit,cnt,filename,divid,category_id,type_id){
	//alert(page+","+startlimit+","+cnt+","+filename+","+divid+","+category_id);
	if(divid == ""){ return false; }
		var url = siteURL+filename;
		$.ajax({
			url: url,
			data: 'catid='+category_id+'&page='+page+'&startlimit='+startlimit+'&cnt='+cnt+'&type_id='+type_id,
			success: function(data){
				//(data);
                document.getElementById(divid).innerHTML = data;
                document.getElementById(divid).style.display="block";
               },
            async:false
        });
	return true;
}

function validateAskExpert(){
	_gaq.push(['_trackEvent', 'Ask Expert Widget', 'AskExpertWidget', '','_trackPageLoadTime']);
	var cntErr=0;
        var filter=/^.+@.+\..{2,3}$/;
        var ck_username = /^[A-Za-z0-9_]{3,20}$/;
        if(trim(document.getElementById('uname').value)==''){
                alert("please enter your name");
                cntErr++;
		return false;
        }
        /*if(trim(document.getElementById('uname').value)!=''){
                if (!ck_username.test(document.getElementById('uname').value)) {
                        alert("please enter valid user name");
                        cntErr++;
                }
        }*/

        if(trim(document.getElementById('emailid').value)==''){
                alert("please enter your email");
                cntErr++;
		return false;
        }else if(document.getElementById('emailid').value!=''){
                if(emailValidator(document.getElementById('emailid').value)==false){
                        alert("please enter valid email address");
                        cntErr++;
			return false;
                }
        }
	if(trim(document.getElementById('question').value)==''){
                alert("please enter question");
                cntErr++;
		return false;
        }
	if(cntErr>0){ 
		return false;
	}else{
	        return true;
        }
}

function submitSeeAnotherCar(){
	var modelUrlArr = Array();
	var seo_str; 
	var brand_name = $("#Brand option:selected").text();
	var brand_id = document.getElementById('Brand').value;
	var product_id = document.getElementById('Model').value;
	var variant_id = document.getElementById('ModelVariant').value;

	if(variant_id != '' && product_id != '' && brand_id != ''){
	seo_str = get_variant_page_url();
	}else if(variant_id == '' && brand_id != '' && product_id != ''){
	seo_str = get_model_page_url();
	}else if(variant_id == '' && product_id == '' && brand_id != ''){
	modelUrlArr.push(brand_name);  
	seo_str = submitResearchParamCar(modelUrlArr);
	
	var html = '<form action="'+seo_str+'" name="submittocarfinder" method="post">';
	html += '<input type="checkbox" name="branddetails[]" value="'+brand_id+'" checked="true">';
	html += '</form>'; 
	
	document.getElementById("submithiddenfrm").innerHTML = html;
	document.submittocarfinder.target="_parent";
	document.submittocarfinder.submit();
	return true;
	}else{
	seo_str = siteURL+seo_car_finder;
	}
	//parent.window.location.href = seo_str;
	setTimeout(function(){parent.window.location.href = seo_str;},0);
	return true;
} 
function submitResearchParamCar(newUrlArr){
	var queryStr = siteURL+seo_car_finder+'/'+newUrlArr.join('/');
	return queryStr;
	document.mymodel.action = queryStr;
	document.mymodel.submit();
	return true;
}

function submitLocateDealers(){
        var iCityId = document.getElementById('city').value;
        var iBrandId = document.getElementById('Brand').value;
	if(iCityId == "0" && iBrandId == "0"){
		alert("Please Select a City or a Brand To Locate The Dealer.");
		return false;
	}
	var city_name="";
	var brand_name="";
	if(iCityId != "0"){
                var url = siteURL+"get_city_brand_name.php";
                $.ajax({
                        url: url,
                        data: 'action=get_city_name&city_id='+iCityId,
                        success: function(data){
                                //alert(data);
                                city_name = data;
                        },
                        async:false
                });
	}
	if(iBrandId != "0"){
		var url = siteURL+"get_city_brand_name.php";
                $.ajax({
                        url: url,
                        data: 'action=get_brand_name&brand_id='+iBrandId,
                        success: function(data){
                                //alert(data);
                                brand_name = data;
                        },
                        async:false
                });
	}
	if((iCityId != "0") && (iBrandId == "0")){
		location.href=siteURL+dealer_url+"/City-"+city_name+"/"+iCityId;
	}else if((iBrandId != "0") && (iCityId == "0")){
		location.href=siteURL+dealer_url+"/Brand-"+brand_name+"/"+iBrandId;
	}else if((iCityId != "0") && (iBrandId != "0")){
		location.href=siteURL+dealer_url+"/Brand-"+brand_name+"/City-"+city_name+"/"+iBrandId+"/"+iCityId;
	}
        return true;
}
