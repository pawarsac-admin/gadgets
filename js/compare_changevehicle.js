function changeVehicle(pos,productid){                            
	newCompareArr = removeArrayElement(comparedIds,productid);                           
	comparedIds = newCompareArr.join(',');
	if(document.getElementById('selected_compare_name_'+pos)){
		document.getElementById('selected_compare_name_'+pos).style.display="none";
	}
	if(document.getElementById('selected_compare_img_'+pos)){
		document.getElementById('selected_compare_img_'+pos).style.display="none";
	}
	if(document.getElementById('change_vehicle_lnk_'+pos)){
		document.getElementById('change_vehicle_lnk_'+pos).style.display="none";
	}
	if(document.getElementById('closeDiv_'+pos)){
		document.getElementById('closeDiv_'+pos).style.display="none";
	}
	if(document.getElementById('add_button_'+pos)){
		document.getElementById('add_button_'+pos).style.display="block";
	}
	if(document.getElementById('sel_div_'+pos)){
		document.getElementById('sel_div_'+pos).style.display="block";
	}
	if(document.getElementById('display_feature_value_'+pos)){
		document.getElementById('display_feature_value_'+pos).style.display="block";
	}
	removeFirstComparedProduct();
	return true;
}
function removeFirstComparedProduct(){
	var sub_group_cnt = document.getElementById('sub_group_cnt_'+currtabid).value;	
	//alert(sub_group_cnt);
	if(document.getElementById('price_cnt_1')){
		document.getElementById('price_cnt_1').style.display="none";
	}
	if(document.getElementById('rating_cnt_1')){
		document.getElementById('rating_cnt_1').style.display="none";
	}
	for(group=1;group<=sub_group_cnt;group++){		
		
		var feature_group_cnt = document.getElementById('feature_group_cnt_'+group+'_'+currtabid).value;	
		//alert('feature_group_cnt = '+feature_group_cnt); //return false;
		for(feature=1;feature<=feature_group_cnt;feature++){
			//alert('display_feature_value_'+group+'_'+feature+'_1');
			if(document.getElementById('display_feature_value_'+group+'_'+feature+'_1_'+currtabid)){
				document.getElementById('display_feature_value_'+group+'_'+feature+'_1_'+currtabid).style.display="none";
			}
		}
	}
	return true;
}
function GetProductPrice(iProdId,surl,divname,param){
	var iCityId=document.getElementById('city').value;	//alert(iCityId);

	var str="action=price&product_id="+iProdId+"&city_id="+iCityId+"&Rand="+Math.random();
	//alert(siteURL+surl+"?"+str);
	$.ajax({
	url: siteURL+surl,
	data: str,
	success: function(data){
	//alert(data);
	document.getElementById(divname).innerHTML = data;
	},
	async:false
	});
}

function getColorOption(iColorId,iProdId,surl,divname,param){

	//alert(iCityId);
	//siteURL="http://localhost/autos/";
	var str="action=color&product_id="+iProdId+"&color_id="+iColorId+"&Rand="+Math.random();
	//alert(siteURL+surl+"?"+str);
	$.ajax({
	url: siteURL+surl,
	data: str,
	success: function(data){
	//alert(data);
	document.getElementById(divname).innerHTML = data;
	},
	async:false
	});
}

function getModelByBrand(divId,surl,divname,param){
	var iBrndId=document.getElementById('Brand_'+divId).value;
	var str="action=model&brand_id="+iBrndId+"&Rand="+Math.random();
	$('#ModelVariant_'+divId).empty().append('<option value="">--Select Variant--</option>');
	$.ajax({
	url: siteURL+surl,
	data: str,
	success: function(data){
	$('#Model_'+divId).empty().append(data);

	},
	async:false
	});
}

function getVariantByBrandModel(divId,surl,divname,param){

	var iBrndId=document.getElementById('Brand_'+divId).value;
	var iModelId=document.getElementById('Model_'+divId).value;
	var str="action=model&brand_id="+iBrndId+"&product_name_id="+iModelId+"&Rand="+Math.random();
	$.ajax({
	url: siteURL+surl,
	data: str,
	success: function(data){
	$('#ModelVariant_'+divId).empty().append(data);
	},
	async:false
	});
}