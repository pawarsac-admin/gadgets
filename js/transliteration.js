// Load the Google Transliterate API
google.load("elements", "1", {
	packages: "transliteration"
  });

function onLoad() {
if(sLanguage=="hindi"){
	var options = {
		sourceLanguage:
			google.elements.transliteration.LanguageCode.ENGLISH,
		destinationLanguage:
			[google.elements.transliteration.LanguageCode.HINDI],
		shortcutKey: 'ctrl+g',
		transliterationEnabled: true
	};
}
// Create an instance on TransliterationControl with the required
// options.
var control =
	new google.elements.transliteration.TransliterationControl(options);

// Enable transliteration in the textbox with id
// 'transliterateTextarea'.
var ids = sTransLiterationElements;
control.makeTransliteratable(ids);
}
if(sLanguage !="english"){
	google.setOnLoadCallback(onLoad);
}		