<?php
		require_once('./include/config.php');
		require_once(CLASSPATH.'DbConn.php');
		require_once(CLASSPATH.'user_review.class.php');
		require_once(CLASSPATH.'brand.class.php');
		require_once(CLASSPATH.'category.class.php');
		require_once(CLASSPATH.'product.class.php');

		$dbconn = new DbConn;
		$userreview = new USERREVIEW;
		$brand = new BrandManagement;
		$category = new CategoryManagement;
		$product = new ProductManagement;

		$category_id = $_REQUEST['catid'] ? $_REQUEST['catid'] : SITE_CATEGORY_ID;		
		$totalquestion = $_REQUEST['totalquestion'];
		$uname = trim($_REQUEST['uname']);
		$brand_id = $_REQUEST['brand_id'];
		$model_id = $_REQUEST['model_id'];
		$product_id = $_REQUEST['product_id'];
		$user_review_id = $_REQUEST['user_review_id'];
		$startlimit = $_REQUEST['startlimit'];
		$cnt = $_REQUEST['cnt'];

		$result = $userreview->arrGetUserReviewDetails("","","","","",$brand_id,$category_id,$model_id,$product_id,"1",$startlimit,$cnt);
		
		$cnt = sizeof($result);
		
		$xml = "<USER_REVIEW_MASTER>";
		$xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
		for($i=0;$i<$cnt;$i++){
			$user_review_id = $result[$i]['user_review_id'];
			$brand_id = $result[$i]['brand_id'];
			$model_id = $result[$i]['product_info_id'];
			$product_id = $result[$i]['product_id'];
			$category_id = $result[$i]['category_id'];
			$create_date = $result[$i]['create_date'];
			$result[$i]['create_date'] = date('d/m/Y',strtotime($create_date));
			if(!empty($brand_id)){
				$brand_result = $brand->arrGetBrandDetails($brand_id,$category_id);
				$brand_name = $brand_result[0]['brand_name'];				
				$modelArr[] = $brand_name;
			}
			$result[$i]['brand_name'] = $brand_name;
			if(!empty($model_id)){
				$product_result = $product->arrGetProductNameInfo($model_id);
				$model_name = $product_result[0]['product_info_name'];
				$modelArr[] = $model_name;
			}			
			$result[$i]['model_name'] = $model_name;
			$result[$i]['brand_model_name'] = implode(" ",$modelArr);
			if(!empty($product_id)){
				$product_result = $product->arrGetProductDetails($product_id);
				$variant = $product_result[0]['variant'];
				$modelArr[] = $variant_name;
			}
			$result[$i]['brand_model_variant_name'] = implode(" ",$modelArr);
			unset($modelArr);
			
			$result[$i]['variant'] = $variant;
			$xml .= "<USER_REVIEW_MASTER_DATA>";

			$ratingresult = $userreview->arrGetUserQnA('','',$user_review_id,"1");
			$ratingcnt = sizeof($ratingresult);
			$xml .= "<USER_RATING_MASTER>";
			for($rating=0;$rating<$ratingcnt;$rating++){
				$que_id = $ratingresult[$rating]['que_id'];
				$que_result = $userreview->arrGetQuestions($que_id);				
				$ratingresult[$rating]['quename'] = $que_result[0]['quename'];
				$answer = $ratingresult[$rating]['answer'];
				$ansArr = explode(",",$answer);
				$gradeCnt = $ratingresult[$rating]['grade'];
				$html = "";
				for($grade=1;$grade<=5;$grade++){
					if($grade <= $gradeCnt){
						$html .= '<img class="vsblStr mlr1" />';
					}else{
						$html .= '<img class="dsblStr mlr1" />';
					}
				}
				$ratingresult[$rating]['grade'] = $html;				
				$xml .= "<USER_RATING_MASTER_DATA>";
				$ratingresult[$rating] = array_change_key_case($ratingresult[$rating],CASE_UPPER);
				foreach($ratingresult[$rating] as $k=>$v){
					$xml .= "<$k><![CDATA[$v]]></$k>";
				}
				$xml .= "</USER_RATING_MASTER_DATA>";
			}
			$xml .= "</USER_RATING_MASTER>";
			
			$reviewresult = $userreview->arrGetUserQnA('','',$user_review_id,"0","1","0","1"); // for comment
			$reviewcnt = sizeof($reviewresult);
			$xml .= "<USER_REVIEW_COMMENT_ANSWER_MASTER>";
			for($review=0;$review<$reviewcnt;$review++){
				$que_id = $reviewresult[$review]['que_id'];
				$answer = $reviewresult[$review]['answer'];
				$answer = removeSlashes($answer);                
                $answer = html_entity_decode($answer,ENT_QUOTES);
                if(strlen($answer)>100){ $answer = getCompactString($answer, 95).' ...'; }
				$reviewresult[$review]['answer'] = $answer;
				$que_result = $userreview->arrGetQuestions($que_id);			
				$reviewresult[$review]['quename'] = $que_result[0]['quename'];

				$reviewresult[$review] = array_change_key_case($reviewresult[$review],CASE_UPPER);
				$xml .= "<USER_REVIEW_COMMENT_ANSWER_MASTER_DATA>";
				foreach($reviewresult[$review] as $k=>$v){
					$xml .= "<$k><![CDATA[$v]]></$k>";
				}
				$xml .= "</USER_REVIEW_COMMENT_ANSWER_MASTER_DATA>";
			}
			$xml .= "</USER_REVIEW_COMMENT_ANSWER_MASTER>";

			$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
			
			foreach($result[$i] as $k=>$v){
				$xml .= "<$k><![CDATA[$v]]></$k>";
			}
			$xml .= "</USER_REVIEW_MASTER_DATA>";

		}
		$xml .= "</USER_REVIEW_MASTER>";
		
		//used to check admin rating.
		$result = $userreview->arrGetAdminOverallGrade($category_id,$brand_id,$product_id,$model_id);
		$cnt = sizeof($result);
		$overallcnt = 0;
		$overallavg = round($result[0]['overallgrade']);
		
		if($cnt <= 0){
			$result = $userreview->arrGetOverallGrade($category_id,$brand_id,$product_id,$model_id);
			$overallcnt = $result[0]['totaloverallcnt'];
			$overallavg = round($result[0]['overallavg']);
		}
		
		$html = "";
		for($grade=1;$grade<=5;$grade++){
			if($grade <= $overallavg){
				$html .= '<img class="vsblStr mlr1"/>';
			}else{
				$html .= '<img class="dsblStr mlr1"/>';
			}
		}
		$xml .= "<OVERALL_AVG_HTML><![CDATA[$html]]></OVERALL_AVG_HTML>";
		$xml .= "<OVERALL_AVG_CNT><![CDATA[$overallavg]]></OVERALL_AVG_CNT>";
		$xml .= "<OVERALL_TOTAL_CNT><![CDATA[$overallcnt]]></OVERALL_TOTAL_CNT>";
		
		$config_details = get_config_details();

        $strXML .= "<XML>";
        $strXML .= $config_details;
		$strXML .= "<MSG><![CDATA[$msg]]></MSG>";
        $strXML .= "<SEO_URL><![CDATA[$seo_url]]></SEO_URL>";
        $strXML .= "<SEO_WEB_URL><![CDATA[".SEO_WEB_URL."]]></SEO_WEB_URL>";
        $strXML .= "<SEO_TITLE><![CDATA[$seo_title]]></SEO_TITLE>";
        $strXML .= "<SEO_TAGS><![CDATA[$seo_keywords]]></SEO_TAGS>";
        $strXML .= "<SEO_DESC><![CDATA[$seo_desc]]></SEO_DESC>";
        $strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$category_id]]></SELECTED_CATEGORY_ID>";        
	$strXML.= $xml;
	$strXML .= "<OC_ROS_BOTTOM_NORTH_728x90><![CDATA[1]]></OC_ROS_BOTTOM_NORTH_728x90>";
	$strXML .= "<OC_ROS_TOP_RHS_LREC_300x250_1><![CDATA[1]]></OC_ROS_TOP_RHS_LREC_300x250_1>";
	$strXML.= "</XML>";

		$strXML = mb_convert_encoding($strXML, "UTF-8");
        
		if($_REQUEST['debug']==1){ header('Content-type: text/xml');echo $strXML;exit; }

        $doc = new DOMDocument();
        $doc->loadXML($strXML);
        $doc->saveXML();
	
	    $xslt = new xsltProcessor;
        $xsl = DOMDocument::load('xsl/model_page_user_review.xsl');
        $xslt->importStylesheet($xsl);
        print $xslt->transformToXML($doc);
?>
