<?php
	require_once('include/config.php');
	require_once(CLASSPATH.'DbConn.php');
	require_once(CLASSPATH.'topstories.class.php');
	
	$dbconn			 = new DbConn;
	$oTopStories	 = new TopStories;
	$group_id		 = 1;// Top Rated Phones	

	$config_details	 = get_config_details();
	$topStoriesXML	 = $oTopStories->getStoriesByGroupXML( $group_id , $dbconn );
	$bgrFavoritesXML = $oTopStories->getStoriesByGroupXML( 2 , $dbconn ); // BGR Favorites

	$strXML			 = "<XML>";
	$strXML			.= $config_details;
	$strXML			.= "<BGR_FAVORITES>".$bgrFavoritesXML."</BGR_FAVORITES>";
	$strXML			.= "</XML>";



	if( $_GET['debug'] == 1 ){
		header('content-type:text/xml');
		echo $strXML;
		die;
	}

	$doc = new DOMDocument();
	$doc->loadXML($strXML);
	$doc->saveXML();
	$xslt = new xsltProcessor;
	$xsl = DOMDocument::load('xsl/get_top_stories.xsl');
	$xslt->importStylesheet($xsl);
	print $xslt->transformToXML($doc);
?>