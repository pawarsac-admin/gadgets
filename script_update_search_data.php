<?php

$BASEPATH	= (defined('BASEPATH'))? BASEPATH : '';

require_once($BASEPATH.'include/config.php');
require_once($BASEPATH.'include/config_search.php');
require_once(CLASSPATH.'DbConn.php');
require_once(CLASSPATH.'DbOp.php');

$dbconn         = new DbConn;
$DbOperation    = new DbOperation;

$patterns		= '/[ ]+/';
$replacements	= '';

if($_REQUEST['debug']==1){
	echo "\n<br/>SCRIPT START<br\>\n";
	echo "\n<br/><b>/////////////////////////  UPDATING 2G NETWORK TYPE /////////////////////////////</b>";
}
/*
define('NET2G',109);
$arr2GNetwork[NET2G][1]	= 'GSM';
$arr2GNetwork[NET2G][2]	= 'CDMA';
$arr2GNetwork[NET2G][3]	= 'GSM & CDMA';
*/
foreach($arr2GNetwork[NET2G] as $key => $value ){
	$value	= str_replace('&', '%', $value);
	$value	= preg_replace($patterns, $replacements, $value);
	$sql	= "UPDATE PRODUCT_FEATURE SET feature_numeric_value=$key WHERE feature_id= ".NET2G." AND feature_value LIKE '%$value%'";
	
	if($_REQUEST['debug']==1) echo "\n<br/>",$sql;
    
	$data           = $DbOperation->update($sql);
	
	if($_REQUEST['debug']==1) echo "\n<br/>Rows Updated : " , $data;
}

if($_REQUEST['debug']==1)
echo "\n<br/><b>/////////////////////////  UPDATING 3G NETWORK TYPE /////////////////////////////</b>";
/*
define('NET3G',6);
$arr3GNetwork[NET3G][1]	= 'CDMA';
$arr3GNetwork[NET3G][2]	= 'UMTS';
$arr3GNetwork[3GNET][3]	= 'HSDPA';
*/
foreach($arr3GNetwork[NET3G] as $key => $value ){
	$value	= preg_replace($patterns, $replacements, $value);
	$sql	= "UPDATE PRODUCT_FEATURE SET feature_numeric_value=$key WHERE feature_id= ".NET3G." AND feature_value LIKE '%$value%'";
	if($_REQUEST['debug']==1)	echo "\n<br/>",$sql;
    $data           = $DbOperation->update($sql);
	if($_REQUEST['debug']==1)	echo "\n<br/>Rows Updated : " , $data;

}

if($_REQUEST['debug']==1)
echo "\n<br/><b>/////////////////////////  UPDATING FORM FACTOR /////////////////////////////</b>";
/*
define('FORMFACTOR',86);
$arrFormFactor[FORMFACTOR][1]	= 'Bar';
$arrFormFactor[FORMFACTOR][3]	= 'Flip';
$arrFormFactor[FORMFACTOR][4]	= 'Slide';
$arrFormFactor[FORMFACTOR][5]	= 'Tablet';
$arrFormFactor[FORMFACTOR][7]	= 'Folder';
$arrFormFactor[FORMFACTOR][8]	= 'Swivel';
$arrFormFactor[FORMFACTOR][2]	= 'Candy Bar';
$arrFormFactor[FORMFACTOR][6]	= 'QWERTY Slider';
$arrFormFactor[FORMFACTOR][9]	= 'Mixed';
*/
foreach($arrFormFactor[FORMFACTOR] as $key => $value ){
	$sql	= "UPDATE PRODUCT_FEATURE SET feature_numeric_value=$key WHERE feature_id= ".FORMFACTOR." AND feature_value LIKE '%$value%'";
	if($_REQUEST['debug']==1) echo "\n<br/>",$sql;
    $data           = $DbOperation->update($sql);
	if($_REQUEST['debug']==1) echo "\n<br/>Rows Updated : " , $data;

}


if($_REQUEST['debug']==1)
echo "\n<br/><b>/////////////////////////  UPDATING PHONE TYPE /////////////////////////////</b>";
/*
define('PHONETYPE',79);
$arrPhoneType[PHONETYPE][1]	= 'Smart Phone';
$arrPhoneType[PHONETYPE][2]	= 'Feature Phone';
$arrPhoneType[PHONETYPE][3]	= 'Basic Phone';
$arrPhoneType[PHONETYPE][4]	= 'Tablet';
*/
foreach($arrPhoneType[PHONETYPE] as $key => $value ){
	$sql	= "UPDATE PRODUCT_FEATURE SET feature_numeric_value=$key WHERE feature_id= ".PHONETYPE." AND feature_value LIKE '%$value%'";
	if($_REQUEST['debug']==1) echo "\n<br/>",$sql;
    $data           = $DbOperation->update($sql);
	if($_REQUEST['debug']==1) echo "\n<br/>Rows Updated : " , $data;

}

if($_REQUEST['debug']==1)
echo "\n<br/><b>/////////////////////////  UPDATING INPUT MECHANISM /////////////////////////////</b>";
/*
define('INPUTMECH',97);
$arrInputMech[INPUTMECH][1]	= 'AlphaNum';
$arrInputMech[INPUTMECH][2]	= 'QWERTY';
$arrInputMech[INPUTMECH][3]	= 'Touchpad';
$arrInputMech[INPUTMECH][4]	= 'AlphaNum & QWERTY';
$arrInputMech[INPUTMECH][5]	= 'Touchpad & AlphaNum';
$arrInputMech[INPUTMECH][6]	= 'Touchpad & QWERTY';
$arrInputMech[INPUTMECH][7]	= 'AlphaNum & QWERTY & Touchpad';
*/
foreach($arrInputMech[INPUTMECH] as $key => $value ){
	$sql	= "UPDATE PRODUCT_FEATURE SET feature_numeric_value=$key WHERE feature_id= ".INPUTMECH ." AND feature_value LIKE '%$value%'";
	if($_REQUEST['debug']==1) echo "\n<br/>",$sql;
    $data           = $DbOperation->update($sql);
	if($_REQUEST['debug']==1) echo "\n<br/>Rows Updated : " , $data;

}

if($_REQUEST['debug']==1)
echo "\n<br/><b>/////////////////////////  UPDATING DUAL SIM FEATURE /////////////////////////////</b>";
/*
define('DUALSIM',112);
$arrDualSIM[DUALSIM][1]	= 'YES';
$arrDualSIM[DUALSIM][2]	= 'NO';
*/

foreach($arrDualSIM[DUALSIM] as $key => $value ){
	$sql	= "UPDATE PRODUCT_FEATURE SET feature_numeric_value=$key WHERE feature_id= ".DUALSIM." AND feature_value LIKE '%$value%'";
	if($_REQUEST['debug']==1) echo "\n<br/>",$sql;
    $data           = $DbOperation->update($sql);
	if($_REQUEST['debug']==1) echo "\n<br/>Rows Updated : " , $data;
}
///////////////

if($_REQUEST['debug']==1)
echo "\n<br/><b>/////////////////////////  UPDATING GPS FEATURE /////////////////////////////</b>";
/*
	define('GPS',114);
	$arrGPS[GPS][1]	= 'YES';
	$arrGPS[GPS][2]	= 'NO';
*/

foreach($arrGPS[GPS] as $key => $value ){
	$sql	= "UPDATE PRODUCT_FEATURE SET feature_numeric_value=$key WHERE feature_id= ".GPS." AND feature_value LIKE '%$value%'";
	if($_REQUEST['debug']==1) echo "\n<br/>",$sql;
    $data           = $DbOperation->update($sql);
	if($_REQUEST['debug']==1) echo "\n<br/>Rows Updated : " , $data;
}

if($_REQUEST['debug']==1)
echo "\n<br/><b>/////////////////////////  UPDATING WIFI FEATURE /////////////////////////////</b>";
/*
	define('WIFI',94);
	$arrWIFI[WIFI][1]	= 'YES';
	$arrWIFI[WIFI][2]	= 'NO';
*/

foreach($arrWIFI[WIFI] as $key => $value ){
	$sql	= "UPDATE PRODUCT_FEATURE SET feature_numeric_value=$key WHERE feature_id= ".WIFI." AND feature_value LIKE '%$value%'";
	if($_REQUEST['debug']==1) echo "\n<br/>",$sql;
    $data           = $DbOperation->update($sql);
	if($_REQUEST['debug']==1) echo "\n<br/>Rows Updated : " , $data;
}
////

if($_REQUEST['debug']==1)
echo "\n<br/><b>/////////////////////////  UPDATING OPERATING SYSTEM /////////////////////////////</b>";
/*
define('OS',106);
$arrOS[OS][1]	= 'Android';
$arrOS[OS][2]	= 'iOS';
$arrOS[OS][3]	= 'Symbian';
$arrOS[OS][4]	= 'Windows Phone';
$arrOS[OS][5]	= 'Blackberry';
$arrOS[OS][6]	= 'Bada';
$arrOS[OS][7]	= 'Others';
*/

foreach($arrOS[OS] as $key => $value ){
	$sql	= "UPDATE PRODUCT_FEATURE SET feature_numeric_value=$key WHERE feature_id= ". OS ." AND feature_value LIKE '%$value%'";
	if($_REQUEST['debug']==1) echo "\n<br/>",$sql;
    $data           = $DbOperation->update($sql);
	if($_REQUEST['debug']==1) echo "\n<br/>Rows Updated : " , $data;
}

if($_REQUEST['debug']==1)
echo "\n<br/><b>/////////////////////////  UPDATING RAM /////////////////////////////</b>";
/*
define('RAM',111);
$arrRAM[RAM][1]	= 256;
$arrRAM[RAM][2]	= 512;
$arrRAM[RAM][3]	= 1024;
*/
//arsort($arrRAM[$feature_id]);

foreach($arrRAM[RAM] as $key => $value ){
	$sql	= "UPDATE PRODUCT_FEATURE SET feature_numeric_value=$key WHERE feature_id = ".RAM." AND feature_value >= $value";
	if($_REQUEST['debug']==1) echo "\n<br/>",$sql;
    $data           = $DbOperation->update($sql);
	if($_REQUEST['debug']==1) echo "\n<br/>Rows Updated : " , $data;
}

if($_REQUEST['debug']==1)
echo "\n<br/><b>/////////////////////////  UPDATING WEIGHT /////////////////////////////</b>";

/*
define('WEIGHT',34);
$arrWeight[WEIGHT][1]	= '0-100';
$arrWeight[WEIGHT][2]	= '101-150';
$arrWeight[WEIGHT][3]	= '151-1000';
*/

foreach($arrWeight[WEIGHT] as $key => $value ){
	$arrT	= explode('-',$value);
	$sql	= "UPDATE PRODUCT_FEATURE SET feature_numeric_value=$key WHERE feature_id= ".WEIGHT." AND feature_value BETWEEN $arrT[0] AND $arrT[1]";
	if($_REQUEST['debug']==1) echo "\n<br/>",$sql;
    $data           = $DbOperation->update($sql);
	if($_REQUEST['debug']==1) echo "\n<br/>Rows Updated : " , $data;
}

if($_REQUEST['debug']==1)
echo "\n<br/><b>/////////////////////////  UPDATING Length /////////////////////////////</b>";
/*
define('LENGTH',30);
$arrLength[LENGTH][1]	= '0-50';
$arrLength[LENGTH][2]	= '51-100';
$arrLength[LENGTH][3]	= '101-150';
*/

foreach($arrLength[LENGTH] as $key => $value ){
	$arrT	= explode('-',$value);
	$sql	= "UPDATE PRODUCT_FEATURE SET feature_numeric_value=$key WHERE feature_id= ".LENGTH." AND feature_value BETWEEN $arrT[0] AND $arrT[1]";
	if($_REQUEST['debug']==1) echo "\n<br/>",$sql;
    $data           = $DbOperation->update($sql);
	if($_REQUEST['debug']==1) echo "\n<br/>Rows Updated : " , $data;
}

if($_REQUEST['debug']==1)
echo "\n<br/><b>/////////////////////////  UPDATING Breadth /////////////////////////////</b>";
/*
define('BREADTH',31);
$arrBreadth[BREADTH][1]	= '0-30';
$arrBreadth[BREADTH][2]	= '31-60';
$arrBreadth[BREADTH][3]	= '61-100';
*/

foreach($arrBreadth[BREADTH] as $key => $value ){
	$arrT	= explode('-',$value);
	$sql	= "UPDATE PRODUCT_FEATURE SET feature_numeric_value=$key WHERE feature_id= ".BREADTH." AND feature_value BETWEEN $arrT[0] AND $arrT[1]";
	if($_REQUEST['debug']==1) echo "\n<br/>",$sql;
    $data           = $DbOperation->update($sql);
	if($_REQUEST['debug']==1) echo "\n<br/>Rows Updated : " , $data;
}

if($_REQUEST['debug']==1)
echo "\n<br/><b>/////////////////////////  UPDATING Thickness /////////////////////////////</b>";
/*
define('THICK',32);
$arrThickness[THICK][1]	= '0-10';
$arrThickness[THICK][2]	= '11-20';
$arrThickness[THICK][3]	= '21-30';
*/

foreach($arrThickness[THICK] as $key => $value ){
	$arrT	= explode('-',$value);
	$sql	= "UPDATE PRODUCT_FEATURE SET feature_numeric_value=$key WHERE feature_id= ".THICK." AND feature_value BETWEEN $arrT[0] AND $arrT[1]";
	if($_REQUEST['debug']==1) echo "\n<br/>",$sql;
    $data           = $DbOperation->update($sql);
	if($_REQUEST['debug']==1) echo "\n<br/>Rows Updated : " , $data;
}


if($_REQUEST['debug']==1){
echo "\n<br/><b>/////////////////////////  UPDATING PRODUCT full name for sorting ///////////////////////////</b>";
	$sql	= "UPDATE PRODUCT_MASTER P,BRAND_MASTER B set product_full_name = CONCAT(B.brand_name,' ',P.product_name,' ',P.variant)  WHERE P.brand_id=B.brand_id";
	if($_REQUEST['debug']==1) echo "\n<br/>",$sql;
    $data           = $DbOperation->update($sql);
	if($_REQUEST['debug']==1) echo "\n<br/>Rows Updated : " , $data;
}
//////////////////


if($_REQUEST['debug']==1)
echo "\n<br/><b>/////////////////////////  UPDATING CAMERA RESOLUTION /////////////////////////////</b>";
/*
	define('CAMRESOLUTION',110);
	$arrCamResolution[CAMRESOLUTION][0]	= '0';
	$arrCamResolution[CAMRESOLUTION][1]	= '0.1-1.9';
	$arrCamResolution[CAMRESOLUTION][2]	= '2';
	$arrCamResolution[CAMRESOLUTION][3]	= '3-4';
	$arrCamResolution[CAMRESOLUTION][5]	= '5';
	$arrCamResolution[CAMRESOLUTION][8]	= '8';
	$arrCamResolution[CAMRESOLUTION][12]= '12';

*/

$sql	= "UPDATE PRODUCT_FEATURE SET feature_numeric_value=0 WHERE feature_id = ".CAMRESOLUTION." AND feature_value = ''";

if($_REQUEST['debug']==1) echo "\n<br/>",$sql;
$data           = $DbOperation->update($sql);
if($_REQUEST['debug']==1) echo "\n<br/>Rows Updated : " , $data;

foreach($arrCamResolution[CAMRESOLUTION] as $key => $value ){

	if( strpos($value, '-') )  {
		$arrT	= explode('-',$value);
		$sql	= "UPDATE PRODUCT_FEATURE SET feature_numeric_value=$key WHERE feature_id= ".CAMRESOLUTION." AND feature_value BETWEEN $arrT[0] AND $arrT[1]";
	} else {
		$sql	= "UPDATE PRODUCT_FEATURE SET feature_numeric_value=$key WHERE feature_id = ".CAMRESOLUTION." AND feature_value = $value";
	}

	if($_REQUEST['debug']==1) echo "\n<br/>",$sql;
    $data           = $DbOperation->update($sql);
	if($_REQUEST['debug']==1) echo "\n<br/>Rows Updated : " , $data;
}

if($_REQUEST['debug']==1) echo "\n<br/>SCRIPT END<br\>\n";

?>