<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Zend_View_Helper_TruncateText extends Zend_View_Helper_Abstract{
    
        /**
         * Truncate string to complete word
         * @since 2011-06-07 Sandip Gaikwad
         */
        public function truncateText($string, $max = 20)
        {	

                $string = strip_tags($string);
                $string = preg_replace('/\[(.*)\]/',' ',$string);
                $replacement = "&#133;";
                // Cut off at the end of a word
                $arr = explode(' ', $string);
                $out = '';
                $count = 0;
                $plus = 1;
                foreach( $arr as $str ){                    
                    if($arr[count($arr)-1] == $str){$plus = 0;}
                    $count += ( strlen($str) + $plus); // +1 is for the space we removed
                    if ($count > $max)
                            break;
                    $out .= $str . ' ';
                }
                // Send something back....
                if (!$out)
                        $out = $arr[0];

                // Do we have to add the ellipses?
                if(strlen($string) > $max){
                    //Trim to remove last spache
                    $out = trim($out);
                    $out .= $replacement;
                }
                   
                //$out = balanceTags($out, true);
                return $out;
        } 
        
        
}
?>
