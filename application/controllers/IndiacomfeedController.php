<?php

class IndiacomfeedController extends Zend_Controller_Action
{
    public function init()
    {
        /* Initialize action controller here */
        $this->_helper->layout->disableLayout();
    }
    
    public function windowsappAction()
    {
        $request = $this->getRequest();
        $ids = trim($request->getParam('id'));//comma separated widget ids
        $feedarr = array();
        
        //if widget id is given, get stories for those widgets only
        if(!empty($ids)){
            $idArr = explode(',', $ids);
            foreach($idArr as $wid)
            {
                $obj1 = new Application_Model_DbTable_Tbwidget();
                $widDetails = $obj1->_getwidgetname($wid);
                foreach($widDetails as $widget){
                    $wname = $widget['name'];
                    $wslug = $widget['slug'];
                    break;
                }
                $obj = new Application_Model_DbTable_Tbpublishedwidgetstory();
                $storiesArr = $obj->_getWidgetStories($wid); 
                $stories = $this->_updatestories($storiesArr);
                
                if($stories){
                     $feedarr[$wid] = array(
                            'name' => $wname,
                            'slug' => $wslug,
                            'stories' => $stories        
                     );
                }
            }
        }
        else //else get stories for all widgets
        {
            $obj1 = new Application_Model_DbTable_Tbwidget();
            $allwid = $obj1->_getallwidget(false);//false => no user permission check

            foreach($allwid as $k=>$v){
                $name = $v['name'];
                $slug = $v['slug'];
                $widid = $v['id'];
                $obj = new Application_Model_DbTable_Tbpublishedwidgetstory();
                $storiesArr = $obj->_getWidgetStories($widid);
                $stories = $this->_updatestories($storiesArr);
                
                if($stories){
                     $feedarr[$widid] = array(
                            'name' => $name,
                            'slug' => $slug,
                            'stories' => $stories        
                     );
                }
            }
        }
        
        $this->view->assign('jsonfeed',str_replace('\r\n','',json_encode($feedarr)));
        $this->_helper->layout->disableLayout();
        
    } 
    
    public function _updatestories($stories)
    {
        foreach ($stories as $story)
        {
            //add full image path for each story
            $story->image = $_SERVER['CDN_IMAGE_URL'].$story->image;
            if(!empty($story->image1))$story->image1 = $_SERVER['CDN_IMAGE_URL'].$story->image1;
            if(!empty($story->image2))$story->image2 = $_SERVER['CDN_IMAGE_URL'].$story->image2;
        }
        return $stories;
    }
}
?>
