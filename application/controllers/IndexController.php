<?php

class IndexController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
                        $this->_helper->layout->disableLayout();
    }

    public function indexAction()
    {
        $request = $this->getRequest();
                         $d = $request->getParam('design'); 
                         
                         if(empty($d) || $d == "") $d =1;
        $admin = new Application_Model_Admin();
        
                         
        /* Top story widget*/                 
        $fetuobj = new Application_Model_DbTable_Tbpublishedwidgetstory();
        $fetresult = $fetuobj->_getwidgetStories(1);
        $this->view->assign('topstories',$fetresult);
        /* End */
        
        /* Top story widget*/                 
        $newsresult_all = $fetuobj->_getwidgetStories(14);
        $newsresult = array_slice($newsresult_all, 0, 5);
        $this->view->assign('news',$newsresult);
        /* End */
        
        /* Top story widget*/                 
        
        $bollyresult_all = $fetuobj->_getwidgetStories(3);
        $bollyresult = array_slice($bollyresult_all, 0, 5);
        $this->view->assign('bollywood',$bollyresult);
        /* End */
        
        /* Top story widget*/                 
        
        $cricresult_all = $fetuobj->_getwidgetStories(15);
        $cricresult = array_slice($cricresult_all, 0, 5);
        $this->view->assign('cricket',$cricresult);
        /* End */
        
        /* Top story widget*/                 
        
        $othresult_all = $fetuobj->_getwidgetStories(16);
        $othresult = array_slice($othresult_all, 0, 5);
        $this->view->assign('otherstories',$othresult);
        /* End */        
                         
        $smap = new Application_Model_DbTable_Tbsectionmapping();

        if($_SERVER['USEMEMCACHE']){
        /* Memcache code*/
                $frontendOpts = array(
                    'caching' => true,
                    'lifetime' => 3600,
                    'automatic_serialization' => true
                );

                $backendOpts = array(
                    'servers' =>array(
                        array(
                        'host'   => $_SERVER['MEMSERVER'],
                        'port'   => $_SERVER['MEMPORT'],
                        'weight' => 1
                        )
                    ),
                    'compression' => false
                );
                $cache = Zend_Cache::factory('Core', 'Memcached', $frontendOpts, $backendOpts);

        /* End*/
        }
        
        if($_SERVER['USEMEMCACHE']){
            if(!$htm = $cache->load('htmlayoutdesign'.$d)){
                $htm = $smap->_getLayoutDesign($d);
                $cache->save($htm,'htmlayoutdesign'.$d);               
            }else  echo "<!-- In memcache for layoutdesign -->"; 
        }else{
            $htm = $smap->_getLayoutDesign($d);
        }
        
        if($_SERVER['USEMEMCACHE']){
        if(!$sidehtm = $cache->load('sidebarlayoutdesign'.$d)){
                $sidehtm = $smap->_getSidebarDesign($d);
                $cache->save($sidehtm,'sidebarlayoutdesign'.$d);                
            }else { echo "<!-- In memcache for sidebarlayoutdesign -->";  }
        }else{
            $sidehtm = $smap->_getSidebarDesign($d);
        }
        
        $this->view->assign('html',$htm);
        $this->view->assign('laynum',$d);
        $this->view->assign('sidehtml',$sidehtm);  
       
        /* Top header ad 728*/                 
        $obj = new Application_Model_DbTable_Tbadminsectionhtml();
        $htm = $obj->_getHtmlForParentChild(6, 1);
        $this->view->assign('headertopad',$htm); 
        /* End top header ad*/
       
        /*LOGO*/
        $obj = new Application_Model_DbTable_Tbadminsectionhtml();
        $htm = $obj->_getHtmlForParentChild(7, 1);
        $this->view->assign('headerlogo',$htm); 
        /*ENd*/
        
        /*Breaking News*/
        
        $bobj1 = new Application_Model_DbTable_Tbwidget();
        $widflag = $bobj1->_isWidgetActive(17);
        
        if($widflag){
            $bnewsname = $bobj1->_getWidgetName(17);
            foreach($bnewsname as $b){
                $bname = $b['name'];
                break;
            }
            $this->view->assign('bnewsname',$bname);     
            
            $bobj = new Application_Model_DbTable_Tbpublishedwidgetstory();
            if($_SERVER['USEMEMCACHE']){
                if(!$brekingnews = $cache->load('brekingnews'.$d)){
                    $brekingnews = $bobj->_getwidgetStories(17);
                    $cache->save($brekingnews,'htmlsidebar'.$d);                
                }else  echo "<!-- In memcache for sidebarlayoutdesign -->";
            }else{
                $brekingnews = $bobj->_getwidgetStories(17);
            }
            $this->view->assign('breakingnews',$brekingnews);            
        }
        /*End*/
        
        /*Returning User Message*/
        $storyObj = new Application_Model_DbTable_Tbstory();  
        $msgArr = $storyObj->_getStoryFromTitle('returning_user_msg'); 
        
        $this->view->assign('returnmsg',$msgArr->description);
        /*End*/
        
        $this->_helper->layout->disableLayout();    
    }

    public function userloginAction()
    {
        // action body
    }

    public function editAction()
    {
        // action body
    }

    public function headerAction()
    {
        // action body
        
    }

    public function footerAction()
    {
        // action body
    }
    
    public function storyfeedAction()
    {
        // action body

        $obj1 = new Application_Model_DbTable_Tbwidget();
        $allwid = $obj1->_getallwidget(true);
        
        $feedarr = array();

        foreach($allwid as $k=>$v){       

            $name = $v['name'];
            $slug = $v['slug'];
            $widid = $v['id'];
            $obj = new Application_Model_DbTable_Tbpublishedwidgetstory();
            $stories = $obj->_getWidgetStories($widid);            
            if($stories){
                 $feedarr[$widid] = array(
                        'name' => $name,
                        'slug' => $slug,
                        'stories' => $stories        
                 );
            }
        }
        
        $this->view->assign('jsonfeed',json_encode($feedarr));
        $this->_helper->layout->disableLayout();
        
    }
    
    public function getfeedfromurlAction()
    {
        $url = $this->getRequest()->getParam('url'); 
        if(!empty($url))
        {
            //Initialize memcache
            if($_SERVER['USEMEMCACHE']){
            /* Memcache code*/
                    $frontendOpts = array(
                        'caching' => true,
                        'lifetime' => 3600,
                        'automatic_serialization' => true
                    );

                    $backendOpts = array(
                        'servers' =>array(
                            array(
                            'host'   => $_SERVER['MEMSERVER'],
                            'port'   => $_SERVER['MEMPORT'],
                            'weight' => 1
                            )
                        ),
                        'compression' => false
                    );
                    $cache = Zend_Cache::factory('Core', 'Memcached', $frontendOpts, $backendOpts);

            /* End*/
            }
            
            $encoded_url = md5($url);
            $json_cache_data = '';
            if($_SERVER['USEMEMCACHE']){$json_cache_data = $cache->load($encoded_url);}
            if(empty($json_cache_data)){            
                $query = urlencode('url:"'.$url.'"');
                $curl_url = 'http://172.16.1.103:8984/solr/collection1/select?q='.$query.'&wt=json&indent=true';
                $curl = curl_init($curl_url);

                curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_ANY);                    
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);                          
                curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);                           
                curl_setopt($curl, CURLOPT_USERAGENT, 'Homepage Excerpt Feed');

                $response = curl_exec($curl); 
                $responseArr = json_decode($response, true);
                
                if($responseArr['response']['numFound'] == 0)
                {
                    echo 'nofeed';
                }
                else{
                    $json_data = json_encode($responseArr['response']['docs'][0]);
                    if($_SERVER['USEMEMCACHE']){$cache->save($json_data,$encoded_url);}

                    echo $json_data;
                }
            }
            else
            {
                echo $json_cache_data;
            }
        }
        else
        {//if no url provided
            echo 'nofeed';
        }
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);        
    }
}









