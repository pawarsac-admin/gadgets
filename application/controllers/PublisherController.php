<?php

class PublisherController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
                        $obj = new Application_Model_Admin();
                        $obj->_is_logged_in();
    }

    public function indexAction()
    {
        // action body
    }

    public function registeruserAction()
    {
        // action body
                                        $request = $this->getRequest();
                                        $error = $request->getParam('error');
                                       
                                
                                        if($error) $this->view->assign('error',$error);
                                
                                        $this->view->assign('action',"process");
                                        $this->view->assign('url',$url);
                                        $this->view->assign('action',"process");
                                        $this->view->assign('title','Member Registration');
                                        $this->view->assign('label_fname','First Name');
                                        $this->view->assign('label_lname','Last Name');	
                                        $this->view->assign('label_uname','User Name');	
                                        $this->view->assign('label_email','User Email');	
                                        $this->view->assign('label_pass','Password');
                                        $this->view->assign('label_submit','Register');		
                                        $this->view->assign('description','Please enter this form completely:');
    }

    public function processAction()
    {
        $request = $this->getRequest();
                        
                        $oldurl = $request->getParam('url');
                                
                                        $data = array('first_name' => $request->getParam('first_name'),
                                          'last_name' => $request->getParam('last_name'),
                                                      'user_name' => $request->getParam('user_name'),
                                                      'email' => $request->getParam('email'),
                                                      'password' => md5($request->getParam('password'))
                                          );
                                        $user = new Application_Model_DbTable_Tbusers();  
                                
                                        $flag = $user->_adduser($data);
                                
                                        if($flag === true){
                                            if(empty($oldurl) )   $this->_redirect('/publisher/userlogin');
                                            else $this->_redirect($oldurl);
                                        }else{ $this->_redirect('/publisher/registeruser/error/'.$flag); }
    }

    public function userloginAction()
    {
        $request = $this->getRequest();
                         $url = $request->getParam('url');
                        $this->view->assign('url',$url);
                        
                                        $this->view->assign('action', $request->getBaseURL()."/publisher/auth");  
                                        $this->view->assign('title', 'Login Form');
                                        $this->view->assign('username', 'User Name');	
                                        $this->view->assign('password', 'Password');
    }

    public function authAction()
    {
        $request = $this->getRequest(); 
                        $oldurl = $request->getParam('url');
                        
                                        $data = array('username' => $request->getParam('username'),
                                                      'password' => $request->getParam('password')
                                        );                        
                                
                                        $user = new Application_Model_DbTable_Tbusers();  
                                        $flag = $user->_authenticate($data); 
                                
                                        if($flag === true){
                                            if(empty($oldurl) )   $this->_redirect('/publisher/userlogin');
                                            else $this->_redirect($oldurl);
                                        }                        else  $this->_redirect('/publisher/userlogin');
    }

    public function addstoryAction()
    {
        $this->view->assign('action',"addstoryprocess");
    }

    public function addstoryprocessAction()
    {
        $uploads_dir = $_SERVER['UPLOAD_IMAGE_PATH'];
                                        $request 	= $this->getRequest();
                                        
                                        $v = $request->getParam('video');
                                        if($v == "" || $v == null ) $v =0;
                                
                                        $name = time()."_".$_FILES['fileField']['name'];
                                        move_uploaded_file($_FILES['fileField']['tmp_name'], $uploads_dir.$name);
                                        
                                        $name1 = time()."_".$_FILES['fileField1']['name'];
                                        move_uploaded_file($_FILES['fileField1']['tmp_name'], $uploads_dir.$name1);
                                        
                                        $name2 = time()."_".$_FILES['fileField2']['name'];
                                        move_uploaded_file($_FILES['fileField2']['tmp_name'], $uploads_dir.$name2);
                                
                                        $publish_date = date('Y-m-d h:i:s');
                                        
                                        $data = array('title' => $request->getParam('title'),
                                        'link' => $request->getParam('link'),
                                        'description' => $request->getParam('description'),
                                        'imgalt' => $request->getParam('imgalt'),
                                        'video' => $v,    
                                        'image' => $name,
                                        'image1' => $name1,
                                        'image2' => $name2,
                                        'publish_date' => $publish_date
                                        );                
                                
                                        $story = new Application_Model_DbTable_Tbstory();  
                                        $flag = $story->_addstory($data);
                                
                                        if($flag)   $this->_redirect('/publisher/medialibrary');
    }

    public function medialibraryAction()
    {
        $story = new Application_Model_DbTable_Tbstory();
                                        $re = $story->_getallstories();
                                
                                        $this->view->assign('title','Member List');
                                        $this->view->assign('description','Below, our members:');
                                        $this->view->assign('datas',$re);
                                        $wid = new Application_Model_DbTable_Tbwidget();
                                        $where = "1=1";
                                        $this->view->assign('widlist',$wid->_getallwidget($where));
    }

    public function addwidgetAction()
    {
        $request = $this->getRequest();
                                        $this->view->assign('action',"processwidget");
                                        $this->view->assign('title','Create Widget');
                                        $this->view->assign('w_name','Widget Name');
                                        $this->view->assign('w_slug','Widget Slug');	
                                        $this->view->assign('w_mlink','More Link');	
                                        $this->view->assign('image_h','Image Height');	
                                        $this->view->assign('image_w','Image Width');
                                        $this->view->assign('w_type','Widget Type');	
                                        $this->view->assign('label_submit','Create Widget');	
                                        $this->view->assign('description','Please enter this form completely:');
    }

    public function processwidgetAction()
    {
        $request 	= $this->getRequest();
                                        $data = array('name' => $request->getParam('w_name'),
                                          'slug' => $request->getParam('w_slug'),
                                                      'morelink' => $request->getParam('m_link'),
                                                      'image_height' => $request->getParam('image_h'),
                                                      'image_width' => $request->getParam('image_w'),
                                                      'type' => $request->getParam('w_type')					  
                                        );  
                                        $wid = new Application_Model_DbTable_Tbwidget();
                                        $wid->_addwidget($data);
                                        $this->view->assign('widlist',$wid->_getallwidget());
                                        $this->_redirect('/publisher/medialibrary');
    }

    public function mapstoryAction()
    {
        $request = $this->getRequest();
                                        $sid = $request->getParam('storyid');
                                        $wid = $request->getParam('widid');
                                        $imgname = $request->getParam('imgname');
                                        $imgnamethumb = $request->getParam('imgname');
                                
                                        $this->view->assign('sid',$sid);
                                        $this->view->assign('wid',$wid);
                                
                                        $data = array('widget_id' => $wid,
                                                      'story_id' => $sid					  
                                        ); 
                                        
                                /* image maunpulation*/
                                
                                        $widgetobj = new Application_Model_DbTable_Tbwidget();
                                        $result = $widgetobj->_getDymentions($wid);
                                
                                        $new = new Application_Model_Admin();
                                        if($result['image_height'] && $result['image_width']){
                                                $h = $result['image_height'];
                                                $w = $result['image_width'];
                                                $newimgname = $new->getResizedImagePath($imgname,$w."X".$h);
                                                $thumbname = $new->getResizedImageThumbPath($imgnamethumb,'90X50');
                                                $thumbname = $new->getResizedImageThumbPath($imgnamethumb,'74X42');
                                                $thumbname = $new->getResizedImageThumbPath($imgnamethumb,'65X37');
                                        }
                                        
                                        
                                        
                                        $data = array('widget_id' => $wid,
                                                      'story_id' => $sid					  
                                        );				
                                
                                        if(!empty($newimgname)){
                                                $data['imgname'] = $newimgname;
                                        }   
                                        
                                /*End*/        
                                        
                                        $widstory = new Application_Model_DbTable_Tbwidgetstory();
                                        $widstory->_addwidstory($data);
    }

    public function editAction()
    {
        $wid = 0;
        $request 	= $this->getRequest();
        $id = trim($request->getParam('id'));
        $wid = trim($request->getParam('widid'));
        if(!empty($wid))
        {
            $this->view->assign('action',"editpublishedstoryprocess");
        }
        else
        {
            $this->view->assign('action',"editaddstoryprocess");
        }

        $story = new Application_Model_DbTable_Tbstory();  
        $result = $story->_getStory($id);

        $this->view->assign('title',$result[$id]['title']);
        $this->view->assign('description',$result[$id]['description']);
        $this->view->assign('link',$result[$id]['link']);	
        $this->view->assign('imgalt',$result[$id]['imgalt']);
        $this->view->assign('video',$result[$id]['video']);
        $this->view->assign('id',$id);
        $this->view->assign('wid', $wid);
        $this->_helper->layout->disableLayout();
    }
    
    public function editpublishedstoryprocessAction()
    {
        $request 	= $this->getRequest();
                                
        $sid = trim($request->getParam('id'));
        $v = $request->getParam('video');
        if($v == "" || $v == null ) $v =0;                                        
        
        /* update story*/
        $uploads_dir = $_SERVER['UPLOAD_IMAGE_PATH'];

        $data = array('title' => $request->getParam('title'),
                        'link' => $request->getParam('link'),
                        'description' => $request->getParam('description'),
                        'video' => $v,    
                        'imgalt' => $request->getParam('imgalt'),					  
        );

        if($_FILES['fileField']['name']){
                $imgname = time()."_".$_FILES['fileField']['name'];
                move_uploaded_file($_FILES['fileField']['tmp_name'], $uploads_dir.$imgname);
                $data['image'] = $imgname;
        }
        
        if($_FILES['fileField1']['name']){
                $imgname1 = time()."_".$_FILES['fileField1']['name'];
                move_uploaded_file($_FILES['fileField1']['tmp_name'], $uploads_dir.$imgname1);
                $data['image1'] = $imgname1;
        }
        
        if($_FILES['fileField2']['name']){
                $imgname2 = time()."_".$_FILES['fileField2']['name'];
                move_uploaded_file($_FILES['fileField2']['tmp_name'], $uploads_dir.$imgname2);
                $data['image2'] = $imgname2;
        }
        
        $data['publish_date'] = date('Y-m-d h:i:s');
        $story = new Application_Model_DbTable_Tbstory();  
        $result = $story->_updateStory($data, $sid);
        /*update story end*/
        
        $wid = trim($request->getParam('widid'));
        
        /*if image uploaded*/
        if(!empty($imgname))
        {
            /*create thumb*/
            $widgetobj = new Application_Model_DbTable_Tbwidget();
            $result = $widgetobj->_getDymentions($wid);

            if($result['image_height'] && $result['image_width']){
                $h = $result['image_height'];
                $w = $result['image_width'];
                $new = new Application_Model_Admin();
                $newimgname = $new->getResizedImagePath($imgname,$w."X".$h);
                $thumbname90X50 = $new->getResizedImageThumbPath($imgname,'90X50');
                $thumbname74X42 = $new->getResizedImageThumbPath($imgname,'74X42');
                $thumbname65X37 = $new->getResizedImageThumbPath($imgname,'65X37');
            }

            $datas = array('widget_id' => $wid,
                          'story_id' => $sid					  
            );				

            if(!empty($newimgname)){
                $datas['imgname'] = $newimgname;
            }
            /*create thumb end*/
            
            /*update widget-story*/
            $widstory = new Application_Model_DbTable_Tbwidgetstory();
            $widstory->_updatewidstory($datas, $wid, $sid);
            /*update widget-story end*/
        }

        //store the widget id in session
        $widget = new Zend_Session_Namespace('widgetDetails');
        $widget->id = $wid;

        echo "<script type='text/javascript'>
                    var url = opener.window.location;
                        opener.window.location = url;
                        window.close();
                 </script>";

        exit();
    }

    public function editaddstoryprocessAction()
    {
        $request 	= $this->getRequest();
                                
                                        $id = trim($request->getParam('id'));
                                        $v = $request->getParam('video');
                                        if($v == "" || $v == null ) $v =0;                                        
                                
                                        $uploads_dir = $_SERVER['UPLOAD_IMAGE_PATH'];
                                
                                        $data = array('title' => $request->getParam('title'),
                                                      'link' => $request->getParam('link'),
                                                                  'description' => $request->getParam('description'),
                                                                  'video' => $v,    
                                                                  'imgalt' => $request->getParam('imgalt'),					  
                                        );
                                
                                        if($_FILES['fileField']['name']){
                                                $name = time()."_".$_FILES['fileField']['name'];
                                                move_uploaded_file($_FILES['fileField']['tmp_name'], $uploads_dir.$name);
                                                $data['image'] = $name;
                                        }
                                        
                                        if($_FILES['fileField1']['name']){
                                                $name1 = time()."_".$_FILES['fileField1']['name'];
                                                move_uploaded_file($_FILES['fileField1']['tmp_name'], $uploads_dir.$name1);
                                                $data['image1'] = $name1;
                                        }
                                        
                                        if($_FILES['fileField2']['name']){
                                                $name2 = time()."_".$_FILES['fileField2']['name'];
                                                move_uploaded_file($_FILES['fileField2']['tmp_name'], $uploads_dir.$name2);
                                                $data['image2'] = $name2;
                                        }
                                        
                                        $data['publish_date'] = date('Y-m-d h:i:s');
                                        $story = new Application_Model_DbTable_Tbstory();  
                                        $result = $story->_updateStory($data, $id);
                                
                                        echo "<script type='text/javascript'>
                                                        url = opener.window.location;
                                                        opener.window.location = url;
                                                        window.close();
                                                 </script>";
                                
                                        exit();
    }

    public function publishwidgetAction()
    {
        $story = new Application_Model_DbTable_Tbstory();
                                        $re = $story->_getallstories();
                                
                                        $this->view->assign('title','Member List');
                                        $this->view->assign('description','Below, our members:');
                                        $this->view->assign('datas',$re);
                                        $wid = new Application_Model_DbTable_Tbwidget();
                                        $where = "1=1";
                                        $this->view->assign('widlist',$wid->_getallwidget($where));		
                                        $registry = Zend_Registry::getInstance(); 
                                        $DB = $registry['DB'];
                                
                                        $sql = "SELECT * FROM widget";
                                        $result1 = $DB->fetchAssoc($sql);
                                
                                        //get the widget id from session
                                        $widget = new Zend_Session_Namespace('widgetDetails');
                                        if(isset($widget->id)){
                                            $widID = $widget->id;
                                        } else {
                                            $widID = 'noval';
                                        }
                                
                                        //unset the session variable after retriving widget id
                                        unset($widget->id);
                                        
                                        $this->view->assign('wid',$widID);
                                        $this->view->assign('data',$result1);
    }

    public function getwidgetstoriesAction()
    {
        $request 	= $this->getRequest();
                                        $wid = $request->getParam('widid');
                                
                                        $widstory = new Application_Model_DbTable_Tbwidgetstory();
                                        $result = $widstory->_getwidgetStories($wid);
                                
                                        $widget = new Application_Model_DbTable_Tbwidget();
                                        $type = $widget->_getwidgettype($wid);
                                
                                        $this->view->assign('data',$result);
                                        $this->view->assign('type',$type);
                                        $this->view->assign('widid',$wid);
                                
                                        $this->_helper->layout->disableLayout();
    }

    public function updatewidgetstoriesAction()
    {
        $request 	= $this->getRequest();
                                        $wid = $request->getParam('widid');
                                        $storyorder = $request->getParam('storyorder');
                                
                                        $widget = new Application_Model_DbTable_Tbwidget();
                                        $type = $widget->_getwidgettype($wid);   
                                
                                        $storyorder = explode("-",$storyorder);
                                                        		
                                        if(!empty($storyorder)){
                                                $i=1;
                                                $widstory = new Application_Model_DbTable_Tbwidgetstory();
                                
                                                foreach($storyorder as $v){
                                                        if($type == 1){
                                                            $data = array('widget_id' => $wid,
                                                                          'story_id' => $v,
                                                                                      'order' => $i++					  
                                                            );                                            
                                                        }else{
                                                            $data = array('widget_id' => $wid,
                                                                          'story_id' => $v,
                                                                                      'order' => $i++					  
                                                            );
                                                        }
                                
                                                        $widstory->_updatewidstory($data, $wid, $v);
                                                }
                                        }        
    }

    public function deletestoryfromwidgetAction()
    {
        $request = $this->getRequest();
                                                    $sid = $request->getParam('sid');
                                                    $wid = $request->getParam('widid');	
                                                    
                                                    $widstory = new Application_Model_DbTable_Tbwidgetstory();
                                                    $widstory->_deleteStory($wid, $sid);
                                                    exit();
    }

    public function publishwidtofrontendAction()
    {
        $request = $this->getRequest();
                                         $wid = $request->getParam('widid');       
                                         
                                         /* Code to update stories for Live widget in database*/
                                         $widstory = new Application_Model_DbTable_Tbwidgetstory();
                                         $widstory->_liveUpdate($wid);
                                         
                                         /* Code to create HTML for Published widget*/
                                         $obj = new Application_Model_Templatizewidget();
                                         $htm = $obj->_get_html($wid);
                                         /* Update html to database to fetch for Frontend*/
                                         
                                         
                                         if(!empty($htm)){
                                            $obj1 = new Application_Model_DbTable_Tbadminsectionhtml();
                                            $obj1->_updateWidgetHtml($wid, $htm);
                                         }
                                         
                                         $this->_helper->layout->disableLayout();
    }

    public function logoutAction()
    {
        $auth = Zend_Auth::getInstance();
        $auth->clearIdentity();
        $this->_redirect('/admin');
    }

    public function flushmedialibraryAction()
    {
        // action body
        $obj = new Application_Model_DbTable_Tbstory();
        $obj->_flushmedialib();
    }

    public function adtagsAction()
    {
        // action body
        /*Check level 2 permission */
        $obj = new Application_Model_Admin();
        $obj->_check_level_permission(3);
        /*End code*/
        
        $bobj1 = new Application_Model_DbTable_Tbwidget();
        $widflag = $bobj1->_isWidgetActive(17);
        
        $storyObj = new Application_Model_DbTable_Tbstory();  
        $msgArr = $storyObj->_getStoryFromTitle('returning_user_msg');
        
        if(empty($msgArr))
        {
            $msg = "";
        }
        else
        {
            $msg = $msgArr->description;
        }
        
        $this->view->assign('action',"updateadtags");
        $this->view->assign('bnewsflag',$widflag);
        $this->view->assign('returnusermsg', $msg);
        
        
    }

    public function updateadtagsAction()
    {
        // action body
        $request = $this->getRequest();
        $indialogo = $request->getParam('indialogo');
        $header728 = $request->getParam('header728');
        $middle468 = $request->getParam('middle468');
        $middle160 = $request->getParam('middle160');
        $sidetop300 = $request->getParam('sidetop300');
        $sidemiddle300 = $request->getParam('sidemiddle300');
        $bnews = $request->getParam('bnews');
        $bnewstitle = $request->getParam('bnewstitle');
        
        $obj1 = new Application_Model_DbTable_Tbadminsectionhtml();
        
        if(!empty($middle468)){
            $obj = new Application_Model_Templatizewidget();
            $tem_htm = $obj->_get_type_html('middle468');
            $htm = str_replace('%%tag%%', $middle468, $tem_htm);
            /* Update HTML for $p parent and $c child*/
            $obj1->_updateAdTag(2, 1, $htm);
        }
        
        if(!empty($middle160)){
            $obj = new Application_Model_Templatizewidget();
            $tem_htm = $obj->_get_type_html('middle160');
            $htm = str_replace('%%tag%%', $middle160, $tem_htm);
            /* Update HTML for $p parent and $c child*/
            $obj1->_updateAdTag(2, 2, $htm);
        } 
        
        if(!empty($sidetop300)){
            $obj = new Application_Model_Templatizewidget();
            $tem_htm = $obj->_get_type_html('sidetop300');
            $htm = str_replace('%%tag%%', $sidetop300, $tem_htm);
            /* Update HTML for $p parent and $c child*/
            $obj1->_updateAdTag(5, 2, $htm);
        }  
        
        if(!empty($sidemiddle300)){
            $obj = new Application_Model_Templatizewidget();
            $tem_htm = $obj->_get_type_html('sidemiddle300');
            $htm = str_replace('%%tag%%', $sidemiddle300, $tem_htm);
            /* Update HTML for $p parent and $c child*/
            $obj1->_updateAdTag(5, 4, $htm);
        }
        
        if(!empty($header728)){
            $obj = new Application_Model_Templatizewidget();
            $tem_htm = $obj->_get_type_html('header728');
            
            $htm = str_replace('%%tag%%', $header728, $tem_htm);

            /* Update HTML for $p parent and $c child*/
            $obj1->_updateAdTag(6, 1, $htm);            
        }
        
        if(!empty($_FILES['fileField']['name'])){
            $uploads_dir = $_SERVER['UPLOAD_IMAGE_PATH'];
            $name = time()."_".$_FILES['fileField']['name'];
            move_uploaded_file($_FILES['fileField']['tmp_name'], $uploads_dir.$name);
            
            $indialogo = $_SERVER['UPLOAD_IMAGE_URL'].$name;
            
            $obj = new Application_Model_Templatizewidget();
            $tem_htm = $obj->_get_type_html('indialogo');
            
            $htm = str_replace('%%tag%%', $indialogo, $tem_htm);

            /* Update HTML for $p parent and $c child*/
            $obj1->_updateAdTag(7, 1, $htm);             
        }

        $obj2 = new Application_Model_DbTable_Tbwidget();        
        if(!empty($bnews)){
            $obj2->_setBreakingNews(1);
        }else{
            $obj2->_setBreakingNews(0);            
        }
        
        if($bnewstitle){
            $obj2->_updateWidgetName(17, $bnewstitle);
        }
        
        //the returning user message is stored in story table under the title returning_user_msg
        $data = array('title' => 'returning_user_msg',
            'description' => $request->getParam('returnusermsg'),
            'status' => 0
            );
        
        $storyObj = new Application_Model_DbTable_Tbstory();  
        $msgArr = $storyObj->_getStoryFromTitle('returning_user_msg');
        if(empty($msgArr))
        {
            $storyObj->_addstory($data);
        }
        else
        {
            $storyObj->_updateStory($data, $msgArr->id);
        }
    }

    public function clearmemcacheAction()
    {
        // action body
        $request = $this->getRequest();
        $wid = $request->getParam('wid');
        $layout = $request->getParam('layout');
        if($_SERVER['USEMEMCACHE']){
            /* Delete Memcache when widget is published*/
            /* Memcache code*/
                    $frontendOpts = array(
                        'caching' => true,
                        'lifetime' => 3600,
                        'automatic_serialization' => true
                    );

                    $backendOpts = array(
                        'servers' =>array(
                            array(
                            'host'   => $_SERVER['MEMSERVER'],
                            'port'   => $_SERVER['MEMPORT'],
                            'weight' => 1
                            )
                        ),
                        'compression' => false
                    );
                    $cache = Zend_Cache::factory('Core', 'Memcached', $frontendOpts, $backendOpts);

            /* End*/
            
                $cache->remove('Widgetcache'.$wid);
                $layout = 0;
                if($layout == 0){
                    for($n=1; $n < 6; $n++){
                        $cache->remove('htmlayoutdesign'.$n);
                        $cache->remove('sidebarlayoutdesign'.$n);                    
                    }    
                }else{
                    $cache->remove('htmlayoutdesign'.$layout);
                    $cache->remove('sidebarlayoutdesign'.$layout);
                }    
               
            /* Code End*/ 
            /*Delete editors pick when any widget is published*/
            $cache->remove('editorpicksnewcachearray');
        }
        
    }

    public function headerAction()
    {
        // action body
        $this->view->assign('logtitle',"hell");
    }



}

