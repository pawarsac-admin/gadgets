<?php
class WidgetController extends Zend_Controller_Action
{
    public function init()
    {
        /* Initialize action controller here */
        $this->_helper->layout->disableLayout();
    }
    
    public function travelAction()
    {
        $parent_url = $_SERVER['HTTP_REFERER'];
        $parent_site = "";
        if(strpos($parent_url,'dnaindia.com')){
            $parent_site = 'dna';
        }
        $widObj = new Application_Model_DbTable_Tbwidget();
        $wid = $widObj->_getWidgetIdFromName('Travel');
        $na = $widObj->_getwidgetname($wid);
        
        $obj = new Application_Model_DbTable_Tbpublishedwidgetstory();
        $result = $obj->_getWidgetStories($wid);

        foreach($na as $k=>$n){
            $name = $n['name'];
            $morelink = $n['morelink'];
        }
        
        $this->view->assign('result', $result);
        $this->view->assign('name', $name);
        $this->view->assign('morelink', $morelink);
        $this->view->assign('site', $parent_site);
        
        $this->_helper->layout->disableLayout();
    }
    
    public function bgrAction()
    {
        
        $parent_url = $_SERVER['HTTP_REFERER'];
        $parent_site = "";
        if(strpos($parent_url,'dnaindia.com')){
            $parent_site = 'dna';
        }
        //get the request param
        $request = $this->getRequest();
        $cat = trim($request->getParam('cat'));        
        if(empty($cat)){$cat = 'news';}
        
        //get the widget url from db
        $widObj = new Application_Model_DbTable_Tbwidget();
        $wid = $widObj->_getWidgetIdFromName('Mobile');
        $na = $widObj->_getwidgetname($wid);
        foreach($na as $k=>$n){
            $morelink = $n['morelink'];
        }
        
        $feed_uri = "http://www.bgr.in/category/".$cat."/feed/";        
        $xml_source = file_get_contents($feed_uri);
        $x = simplexml_load_string($xml_source);
        $dataArr = $x->channel->item;
        
        $this->view->assign('morelink', $morelink);
        $this->view->assign('dataArr', $dataArr);
        $this->view->assign('site', $parent_site);
        
        $this->_helper->layout->disableLayout();
    }
    
    public function healthAction()
    {
        $parent_url = $_SERVER['HTTP_REFERER'];
        $parent_site = "";
        if(strpos($parent_url,'dnaindia.com')){
            $parent_site = 'dna';
        }
        $widObj = new Application_Model_DbTable_Tbwidget();
        $wid = $widObj->_getWidgetIdFromName('Health');
        $na = $widObj->_getwidgetname($wid);
        
        $obj = new Application_Model_DbTable_Tbpublishedwidgetstory();
        $result = $obj->_getWidgetStories($wid);

        foreach($na as $k=>$n){
            $name = $n['name'];
            $morelink = $n['morelink'];
        }
        
        $this->view->assign('result', $result);
        $this->view->assign('name', $name);
        $this->view->assign('morelink', $morelink);
        $this->view->assign('site', $parent_site);
        
        $this->_helper->layout->disableLayout();
    }
}
?>
