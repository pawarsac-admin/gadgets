<?php

class OutlineController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
        $obj = new Application_Model_Admin();
        $obj->_is_logged_in();
        
        $obj->_check_level_permission(2);
        
    }

    public function indexAction()
    {
        $request = $this->getRequest();
                         $d = $request->getParam('design'); 
                         
                         if(empty($d) || $d == "") $d =1;
                         
                         $smap = new Application_Model_DbTable_Tbsectionmapping();
                         $htm = $smap->_getAdminLayoutDesign($d);
                         
                         $sidehtm = $smap->_getAdminSidebarDesign($d);
                         
                         $this->view->assign('html',$htm);
                         $this->view->assign('laynum',$d);
                         $this->view->assign('sidehtml',$sidehtm);
                         
    }

    public function layoutAction()
    {
        $request = $this->getRequest();
                         $d = $request->getParam('design'); 
                         
                         if(empty($d) || $d == "") $d =1;
                         
                         $smap = new Application_Model_DbTable_Tbsectionmapping();
                         $htm = $smap->_getAdminLayoutDesign($d);
                         
                         $sidehtm = $smap->_getAdminSidebarDesign($d);
                         
                         $this->view->assign('html',$htm);
                         $this->view->assign('laynum',$d);
                         $this->view->assign('sidehtml',$sidehtm);
    }

    public function updatesecmappingAction()
    {
        $request = $this->getRequest();
                         $secmap = $request->getParam('secmap');
                         $layout = $request->getParam('layout');
                
                         $sec = explode("-", $secmap);
                         $secarr = array();
                         foreach($sec as $s){
                             $a = explode("sec", $s);
                             $secarr[]=$a[1];
                         }
                
                         $data = array('id' => $layout,
                                'layout' => serialize($secarr)
                         );
                
                         $upmap = new Application_Model_DbTable_Tbupdatesecmapping();
                         $flag = $upmap->_updateLayout($data, $layout);
    }

    public function sectionmappingAction()
    {
        $request = $this->getRequest();
                         $secmap = $request->getParam('secmap');
                         $layout = $request->getParam('layout');       
                         
                         $sec = explode("-", $secmap);
                         $section = "";
                         $subsec = array();
                         foreach($sec as $s){        
                             $a = explode("sec", $s);
                             $b = explode("d", $a[1]);
                             $section = $b[0];
                             $subsec[] = $b[1];
                         }
                         $smap = new Application_Model_DbTable_Tbsectionmapping();
                         $smap->_deleteparent($section, $layout);
                         
                         foreach($subsec as $sub){
                             $data = array('id' => '',
                                    'layout' => $layout,
                                    'parent' => $section,
                                    'child' => $sub
                             );  
                             
                             
                             $smap->_updatesecmapping($section, $layout, $data); }
    }

    public function sidebarmappingAction()
    {
        $request = $this->getRequest();
                         $secmap = $request->getParam('secmap');
                         $layout = $request->getParam('layout'); 
                         
                         $sec = explode("-", $secmap);
                         $section = 5;
                         $smap = new Application_Model_DbTable_Tbsectionmapping();
                         $smap->_deleteparent($section, $layout);                 
                         
                         foreach($sec as $s){
                             $a = explode("sd", $s);
                             $sdwidid = $a[1];
                             $data = array('id' => '',
                                    'layout' => $layout,
                                    'parent' => $section,
                                    'child' => $sdwidid
                             );  
                             
                             
                             $smap->_updatesecmapping($section, $layout, $data); 
                             
                         }
    }

    public function previewAction()
    {
        $request = $this->getRequest();
                         $d = $request->getParam('design'); 
                         
                         if(empty($d) || $d == "") $d =1;
        $admin = new Application_Model_Admin();
        
                         
        /* Top story widget*/                 
        $fetuobj = new Application_Model_DbTable_Tbpublishedwidgetstory();
        $fetresult = $fetuobj->_getwidgetStories(1);
        $this->view->assign('topstories',$fetresult);
        /* End */
        
        /* Top story widget*/                 
        $newsresult_all = $fetuobj->_getwidgetStories(14);
        $newsresult = array_slice($newsresult_all, 0, 5);
        $this->view->assign('news',$newsresult);
        /* End */
        
        /* Top story widget*/                 
        
        $bollyresult_all = $fetuobj->_getwidgetStories(3);
        $bollyresult = array_slice($bollyresult_all, 0, 5);
        $this->view->assign('bollywood',$bollyresult);
        /* End */
        
        /* Top story widget*/                 
        
        $cricresult_all = $fetuobj->_getwidgetStories(15);
        $cricresult = array_slice($cricresult_all, 0, 5);
        $this->view->assign('cricket',$cricresult);
        /* End */
        
        /* Top story widget*/                 
        
        $othresult_all = $fetuobj->_getwidgetStories(16);
        $othresult = array_slice($othresult_all, 0, 5);
        $this->view->assign('otherstories',$othresult);
        /* End */        
                         
        $smap = new Application_Model_DbTable_Tbsectionmapping();

        if($_SERVER['USEMEMCACHE']){
        /* Memcache code*/
                $frontendOpts = array(
                    'caching' => true,
                    'lifetime' => 3600,
                    'automatic_serialization' => true
                );

                $backendOpts = array(
                    'servers' =>array(
                        array(
                        'host'   => $_SERVER['MEMSERVER'],
                        'port'   => $_SERVER['MEMPORT'],
                        'weight' => 1
                        )
                    ),
                    'compression' => false
                );
                $cache = Zend_Cache::factory('Core', 'Memcached', $frontendOpts, $backendOpts);

        /* End*/
        }
        
        if($_SERVER['USEMEMCACHE']){
            if(!$htm = $cache->load('htmlayoutdesign'.$d)){
                $htm = $smap->_getLayoutDesign($d);
                $cache->save($htm,'htmlayoutdesign'.$d);               
            }else  echo "<!-- In memcache for layoutdesign -->"; 
        }else{
            $htm = $smap->_getLayoutDesign($d);
        }
        
        if($_SERVER['USEMEMCACHE']){
        if(!$sidehtm = $cache->load('sidebarlayoutdesign'.$d)){
                $sidehtm = $smap->_getSidebarDesign($d);
                $cache->save($sidehtm,'sidebarlayoutdesign'.$d);                
            }else { echo "<!-- In memcache for sidebarlayoutdesign -->";  }
        }else{
            $sidehtm = $smap->_getSidebarDesign($d);
        }
        
        $this->view->assign('html',$htm);
        $this->view->assign('laynum',$d);
        $this->view->assign('sidehtml',$sidehtm);  
       
        /* Top header ad 728*/                 
        $obj = new Application_Model_DbTable_Tbadminsectionhtml();
        $htm = $obj->_getHtmlForParentChild(6, 1);
        $this->view->assign('headertopad',$htm); 
        /* End top header ad*/
       
        /*LOGO*/
        $obj = new Application_Model_DbTable_Tbadminsectionhtml();
        $htm = $obj->_getHtmlForParentChild(7, 1);
        $this->view->assign('headerlogo',$htm); 
        /*ENd*/
        
        /*Breaking News*/
        
        $bobj1 = new Application_Model_DbTable_Tbwidget();
        $widflag = $bobj1->_isWidgetActive(17);
        
        if($widflag){
            $bnewsname = $bobj1->_getWidgetName(17);
            foreach($bnewsname as $b){
                $bname = $b['name'];
                break;
            }
            $this->view->assign('bnewsname',$bname);     
            
            $bobj = new Application_Model_DbTable_Tbpublishedwidgetstory();
            if($_SERVER['USEMEMCACHE']){
                if(!$brekingnews = $cache->load('brekingnews'.$d)){
                    $brekingnews = $bobj->_getwidgetStories(17);
                    $cache->save($brekingnews,'htmlsidebar'.$d);                
                }else  echo "<!-- In memcache for sidebarlayoutdesign -->";
            }else{
                $brekingnews = $bobj->_getwidgetStories(17);
            }
            $this->view->assign('breakingnews',$brekingnews);            
        }
        /*End*/
        
        
        $this->_helper->layout->disableLayout();
    }

}











