<?php
class Application_Model_Templatizewidget
{

    public function _get_type_html($temp_type, $wid=0){
        if($wid != 0){
            $temp_type = "Template".$temp_type;
            $temp_type = trim($temp_type);

            $obj = new Application_Model_DbTable_Tbpublishedwidgetstory();
            $result = $obj->_getWidgetStories($wid);

            $obj1 = new Application_Model_DbTable_Tbwidget();
            $na = $obj1->_getwidgetname($wid);

            foreach($na as $k=>$n){
                $name = $n['name'];
                $slug = $n['slug'];
                $morelink = $n['morelink'];
                $arr = explode("/", $morelink);
                $readmoretitle = ucfirst(str_replace('www.', '', $arr[2]));
                //var_dump($readmoretitle);
            }
        }
        $htm = "";  
        switch ($temp_type){
            case 'Template1':
                    /* Sports - Health - Enter - MObile - Autos - Business */
                    $htm = '<li id="sec1d1" class="lftblk br5"><article id="secblk"  class="lftblk '.$slug.'">';
                    $htm .=  '<h2><span><a href="'.$morelink.'" target="_blank" onClick="_gaq.push([\'_trackEvent\', \'New Layout1 - '.$name.'\', \'Clicked\',\'Widget Title\']);">'.$name.'</a></span></h2>';
                    $i=1;
                    foreach($result as $r){
                        if($i > 4) break;
                        if($r->video == 1){
                            $vflagclass = 'class="vidlnk"';
                            $vflagcode = '<span class="iconplay-big"></span>';
                        }else{
                            $vflagclass = '';
                            $vflagcode = '';                            
                        } 
                        if($i++ == 1){
                        $htm .='<figure>
                            <a onClick="_gaq.push([\'_trackEvent\', \'New Layout1 - '.$name.'\', \'Clicked\',\'Article '.($i-1).' - Image Link\']);"  '.$vflagclass.' target="_blank" href="'.$r->link.'"><img alt="'.$r->imgalt.'" title="'.$r->title.'" src="'.$_SERVER['CDN_IMAGE_URL'].$r->imgname.'" />
                            '.$vflagcode.'    
                            </a>
                            <figcaption><a onClick="_gaq.push([\'_trackEvent\', \'New Layout1 - '.$name.'\', \'Clicked\',\'Article '.($i-1).' - title Link\']);"   target="_blank" href="'.$r->link.'">'.$this->truncateText($r->title,30).'</a> </figcaption>
                        </figure>
                        <ul>';
                        }else{ 
                            $htm .='<li><a onClick="_gaq.push([\'_trackEvent\', \'New Layout1 - '.$name.'\', \'Clicked\',\'Article '.($i-1).' - title Link\']);"  target="_blank" href="'.$r->link.'">'.$this->truncateText($r->title,70).'</a></li>';
                        }    
                        

                    }        
                    $htm .='                        </ul>
                        <a  title="Read More on '.$name.' at '.$readmoretitle.'"  onClick="_gaq.push([\'_trackEvent\', \'New Layout1 - '.$name.'\', \'Clicked\',\'Read More\']);"  class="readmore" target="_blank" href="'.$morelink.'">Read more</a>
                    </article></li>';  
                    break;
            case 'Template2':
                    /* Education */
                    $i=1;
                    foreach($result as $r){
                        if($r->video == 1){
                            $vflagclass = 'class="vidlnk crrht-img"';
                            $vflagcode = '<span class="iconplay-big"></span>';
                            $vflagcodesmall = '<span class="iconplay"></span>';
                        }else{
                            $vflagclass = 'crrht-img';
                            $vflagcode = ''; 
                            $vflagcodesmall ='';
                        }
                        
                        $thumimg = str_replace('350X197', '90X50', $r->imgname);
                        $upload_path = $_SERVER['UPLOAD_IMAGE_PATH'];
                        if(!file_exists($upload_path.$thumimg)) $thumimg = $r->imgname;
                        
                        if($i++ == 1){                
                    $htm = '<figure>
                                <a onClick="_gaq.push([\'_trackEvent\', \'New Layout1 - '.$name.'\', \'Clicked\',\'Article '.($i-1).' - Image Link\']);"  '.$vflagclass.' target="_blank" href="'.$r->link.'"><img  alt="'.$r->imgalt.'" title="'.$r->title.'"  src="'.$_SERVER['CDN_IMAGE_URL'].$r->imgname.'" />'.$vflagcode.'</a>
                                <figcaption>
                               	<a onClick="_gaq.push([\'_trackEvent\', \'New Layout1 - '.$name.'\', \'Clicked\',\'Article '.($i-1).' - title Link\']);"  '.$vflagclass.' target="_blank" href="'.$r->link.'">'.$r->title.'</a>	
                                </figcaption>    
                            </figure>
                        </article>
                        <aside class="careerght">';
                        }else{
                        $htm .='    <figure>
                                <a  target="_blank" onClick="_gaq.push([\'_trackEvent\', \'New Layout1 - '.$name.'\', \'Clicked\',\'Article '.($i-1).' - Image Link\']);" '.$vflagclass.' class="crrht-img" href="'.$r->link.'"><img  alt="'.$r->imgalt.'" title="'.$r->title.'"   width="91px" src="'.$_SERVER['CDN_IMAGE_URL'].$thumimg.'" alt="" />'.$vflagcodesmall.'</a>
                                <figcaption>
                                    <a  target="_blank" onClick="_gaq.push([\'_trackEvent\', \'New Layout1 - '.$name.'\', \'Clicked\',\'Article '.($i-1).' - title Link\']);"  target="_blank" href="'.$r->link.'">'.$this->truncateText($r->title,50).'</a>
                                    <p>'.$this->truncateText($r->description,70).'</p>
                                </figcaption>
                                <div class="clear"></div>
                            </figure>';
                        }
                    }
                    $htm .= '<a onClick="_gaq.push([\'_trackEvent\', \'New Layout1 - '.$name.'\', \'Clicked\',\'Read More\']);"  href="'.$morelink.'" target="_blank" title="Read More on '.$name.' at '.$readmoretitle.'" class="readmore">Read more</a>';
                    break;
            case 'Template3':
                    /* Travel*/
                    $htm = '            <li id="sd3">
              <article class="trvlft">
                  <h2><span><a href="'.$morelink.'" target="_blank" onClick="_gaq.push([\'_trackEvent\', \'New Layout1 - '.$n['name'].'\', \'Clicked\',\'Widget - Title\']);">'.$n['name'].'</a></span></h2>
                  <div class="trvlcarousel">
                    <div class="mid">';
                    $i =1;
                    $imgcnt = 1;
                    foreach($result as $r){
                        if($r->video == 1){
                            $vflagclass = 'class="vidlnk"';
                            $vflagcode = '<span class="iconplay-big"></span>';
                            $vflagcodesmall = '<span class="iconplay"></span>';
                        }else{
                            $vflagclass = '';
                            $vflagcode = ''; 
                            $vflagcodesmall ='';
                        } 
                        
                      if($imgcnt++ < 2){
                        $htm .= '<figure> <a onClick="_gaq.push([\'_trackEvent\', \'New Layout1 - '.$name.'\', \'Clicked\',\'Article '.$i.' - Image Link\']);"  '.$vflagclass.' target="_blank" href="'.$r->link.'"> <img  alt="'.$r->imgalt.'" title="'.$r->title.'"  src="'.$_SERVER['CDN_IMAGE_URL'].$r->imgname.'" rel="'.$i.'" />'.$vflagcode.'</a>
                        <figcaption> <a onClick="_gaq.push([\'_trackEvent\', \'New Layout1 - '.$name.'\', \'Clicked\',\'Article '.$i++.' - title Link\']);"  target="_blank" href="'.$r->link.'">'.$this->truncateText($r->title,50).'</a> '.$this->truncateText($r->description,170).' </figcaption>
                      </figure>';
                      }else{
                        $htm .= '<figure> <a onClick="_gaq.push([\'_trackEvent\', \'New Layout1 - '.$name.'\', \'Clicked\',\'Article '.$i.' - Image Link\']);"  '.$vflagclass.' target="_blank" href="'.$r->link.'"><img  alt="'.$r->imgalt.'" title="'.$r->title.'"  data-href="'.$_SERVER['CDN_IMAGE_URL'].$r->imgname.'" rel="'.$i.'" />'.$vflagcode.'</a>
                        <figcaption> <a onClick="_gaq.push([\'_trackEvent\', \'New Layout1 - '.$name.'\', \'Clicked\',\'Article '.$i++.' - title Link\']);"  target="_blank" href="'.$r->link.'">'.$this->truncateText($r->title,50).'</a> '.$this->truncateText($r->description,170).' </figcaption>
                      </figure>';                          
                      }  
                    }  
                      
                    $htm .= '</div>
                    <div class="jthumb-bar">
                      <a href="javascript:void(0)" class="sprit-icon prev"></a>
                      <a href="javascript:void(0)" class="sprit-icon next"></a>
                      <div class="jCarouselLite thumbwrap">
                        <ul>';
                        $i =1;
                        $imgcnt = 1;
                        foreach($result as $r){   
                            
                            $thumimg = str_replace('300X169', '65X37', $r->imgname);
                            $upload_path = $_SERVER['UPLOAD_IMAGE_PATH'];
                            if(!file_exists($upload_path.$thumimg)) $thumimg = $r->imgname;                            
                            
                            if($imgcnt++ < 2){
                                $htm .= '      <li><img width="65"  alt="'.$r->imgalt.'" title="'.$r->title.'"  src="'.$_SERVER['CDN_IMAGE_URL'].$thumimg.'" rel="'.$i++.'" /></li>';
                            }else{
                                $htm .= '      <li><img width="65"  alt="'.$r->imgalt.'" title="'.$r->title.'"  data-href="'.$_SERVER['CDN_IMAGE_URL'].$thumimg.'" rel="'.$i++.'" /></li>';
                            }
                        }
                            
                          $htm .= '<div class="clear"></div>
                        </ul>
                      </div>
                    </div>
                    <div class="clear"></div>
                  </div>
                  <div class="clear"></div>
              </article>
            </li>';
                          
                    break;
            /* Ad tags */        
            case 'middle468':
                    $htm = '<li id="sec2d1" class="adbanner468 lftblk">%%tag%%</li>';
                    break;
            case 'middle160':
                    $htm = '<li id="sec2d2" class="adbanner160 lftblk">%%tag%%</li>';
                    break;
            case 'sidetop300':
                    $htm = '<li id="sd2"><div class="adbanner300">%%tag%%</div></li>';
                    break;
            case 'sidemiddle300':
                    $htm = '<li id="sd4"><div class="adbanner300">%%tag%%</div></li>';
                    break;
            case 'header728':
                    $htm = '<div class="topad728">%%tag%%</div>';
                    break;
            /* Ad tag Ends */
            /* India.com LOGO*/
            case 'indialogo':
                    $htm = '<div class="logo"><a href="'.$_SERVER['CDN_DOMAIN'].'"><img title="India | Latest India News | Get Free India.com Email | Live Cricket and Entertainment News at India.Com" alt="India | Latest India News | Get Free India.com Email | Live Cricket and Entertainment News at India.Com" src="%%tag%%"></a></div>';
                    break;
            /* End*/
            case 'Template4':
            /* Food section*/
                if($result[0]->video == 1){
                    $vflagclass0 = 'class="vidlnk"';
                    $vflagcode0 = '<span class="iconplay-big"></span>';
                    $vflagcodesmall0 = '<span class="iconplay"></span>';
                }else{
                    $vflagclass0 = '';
                    $vflagcode0 = ''; 
                    $vflagcodesmall0 ='';
                }
                if($result[1]->video == 1){
                    $vflagclass1 = 'class="vidlnk ld1"';
                    $vflagcode1 = '<span class="iconplay-big"></span>';
                    $vflagcodesmall1 = '<span class="iconplay"></span>';
                }else{
                    $vflagclass1 = 'class="ld1"';
                    $vflagcode1 = ''; 
                    $vflagcodesmall1 ='';
                } 
                
                $thumimg = str_replace('300X170', '90X50', $result[1]->imgname);
                $upload_path = $_SERVER['UPLOAD_IMAGE_PATH'];
                if(!file_exists($upload_path.$thumimg)) $thumimg = $result[1]->imgname;
                        
                        
                            $htm = '<li id="sd5"><article class="food">
                <h2><span><a href="'.$n['morelink'].'" target="_blank" onClick="_gaq.push([\'_trackEvent\', \'New Layout1 - '.$name.'\', \'Clicked\',\'Widget - Title\']);">'.$name.'</a></span></h2>
                <figure class="fdld"> <a onClick="_gaq.push([\'_trackEvent\', \'New Layout1 - '.$name.'\', \'Clicked\',\'Article 1 - Image Link\']);"  '.$vflagclass0.' target="_blank" href="'.$result[0]->link.'"><img alt="'.$result[0]->imgalt.'" title="'.$result[0]->title.'" src="'.$_SERVER['CDN_IMAGE_URL'].$result[0]->imgname.'">'.$vflagcode0.'</a>
                    <figcaption> <a onClick="_gaq.push([\'_trackEvent\', \'New Layout1 - '.$name.'\', \'Clicked\',\'Article 1 - title Link\']);"  target="_blank" href="'.$result[0]->link.'">'.$this->truncateText($result[0]->title,50).'</a>
                    <p>'.$this->truncateText($result[0]->description,170).'</p>
                </figcaption>
                </figure>
                <figure class="fdld1"> <a onClick="_gaq.push([\'_trackEvent\', \'New Layout1 - '.$name.'\', \'Clicked\',\'Article 2 - Image Link\']);"  '.$vflagclass1.' target="_blank" href="'.$result[1]->link.'" class="ld1"><img  height="50px" alt="'.$result[1]->imgalt.'" title="'.$result[1]->title.'"  src="'.$_SERVER['CDN_IMAGE_URL'].$thumimg.'">'.$vflagcodesmall1.'</a>
                    <figcaption> <a onClick="_gaq.push([\'_trackEvent\', \'New Layout1 - '.$name.'\', \'Clicked\',\'Article 2 - title Link\']);"  target="_blank" href="'.$result[1]->link.'">'.$this->truncateText($result[1]->title,35).'</a>
                    <p>'.$this->truncateText($result[1]->description,70).'</p>
                </figcaption>
                </figure>
                <div class="clear"></div>
            </article></li>';
            break;
            case 'Template5':
                /* Gallery*/
                $htm = '            <li id="sd1"><article id="picts">
                <figure class="lftld">
                	<a onClick="_gaq.push([\'_trackEvent\', \'New Layout1 - '.$name.'\', \'Clicked\',\'Article 1 - Image Link\']);"  target="_blank" href="'.$result[0]->link.'"><img alt="'.$result[0]->imgalt.'" title="'.$result[0]->title.'" src="'.$_SERVER['CDN_IMAGE_URL'].$result[0]->imgname.'">
                        <p class="galttl">GALLERY</p>    
                        </a>
                </figure>
                <figure class="rthld">
                    <a onClick="_gaq.push([\'_trackEvent\', \'New Layout1 - '.$name.'\', \'Clicked\',\'Article 2 - Image Link\']);"  target="_blank" href="'.$result[1]->link.'"><img alt="'.$result[1]->imgalt.'" title="'.$result[1]->title.'" src="'.$_SERVER['CDN_IMAGE_URL'].$result[1]->imgname.'"></a>
                    <a onClick="_gaq.push([\'_trackEvent\', \'New Layout1 - '.$name.'\', \'Clicked\',\'Article 3 - Image Link\']);"  target="_blank" href="'.$result[2]->link.'"><img alt="'.$result[2]->imgalt.'"  title="'.$result[2]->title.'" src="'.$_SERVER['CDN_IMAGE_URL'].$result[2]->imgname.'"></a>
                </figure>
                <div class="clear"></div>
            </article></li>';
             break;   
            /* End*/    
            default:
                    $htm = "";
        }
        return  $htm;
    }
    
    public function _get_html($widget_id){
        $obj = new Application_Model_DbTable_Tbwidget();
        $temp_type = $obj->_getWidgetTemplateType($widget_id);
        
        $temp_type = trim($temp_type);
        
        $htm = $this->_get_type_html($temp_type, $widget_id);
        
        return $htm;
    }
    
        public function truncateText($string, $max = 20)
        {	

                $string = strip_tags($string);
                $string = preg_replace('/\[(.*)\]/',' ',$string);
                $replacement = "&#133;";
                // Cut off at the end of a word
                $arr = explode(' ', $string);
                $out = '';
                $count = 0;
                $plus = 1;
                foreach( $arr as $str ){
                    if($arr[count($arr)-1] == $str){$plus = 0;}
                    $count += ( strlen($str) + $plus); // +1 is for the space we removed
                    if ($count > $max)
                            break;
                    $out .= $str . ' ';
                }
                // Send something back....
                if (!$out)
                        $out = $arr[0];

                // Do we have to add the ellipses?
                if(strlen($string) > $max){
                    //Trim to remove last spache
                    $out = trim($out);                    
                    $out .= $replacement;
                }
                        
                   
                //$out = balanceTags($out, true);
                return $out;
        }     
}

