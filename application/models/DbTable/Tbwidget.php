<?php
class Application_Model_DbTable_Tbwidget extends Zend_Db_Table_Abstract
{

    protected $_name = 'widget';
    public function __construct(){
          $registry = Zend_Registry::getInstance(); 
	  $this->DB = $registry['DB'];
          $this->auth = Zend_Auth::getInstance();
    }
    
    public function _addwidget($args){
         $this->DB->insert('widget', $args);
    }

    /*argument $whr
     * true: check for logged in user permissions, return only widgets the user has permissions for
     * false: no check for user permissions, return all widgets
     */
    public function _getallwidget($whr = true){
        $authAdapter = new Zend_Auth_Adapter_DbTable($this->DB);

        if($whr)
        {
            $userid = $this->auth->getIdentity()->id;

            if($userid == "" || empty($userid)) return false;
            $obj = new Application_Model_Admin();
            $flag = $obj->_check_level_permission(2,false);

            if($flag){
                $sql = "SELECT * FROM widget  order by name";
            }else{
                $sql = "SELECT * FROM widget where id IN (select widget_id from user_widget where user_id = ".$userid.") order by name";
            }
        }
        else
        {
            $sql = "SELECT * FROM widget  order by name";
        }

        $result = $this->DB->fetchAssoc($sql);        
        return $result;        
    }
    
    public function _getwidgettype($wid){
        $sql = "SELECT type from widget where id=".$wid;
	$result = $this->DB->fetchOne($sql);        
        return $result;             
    }
    
    public function _getwidgetname($wid){
        $sql = "SELECT name,slug,morelink from widget where id=".$wid;
	$result = $this->DB->fetchAssoc($sql);        
        return $result;             
    }    
    
    public function _getDymentions($wid){
        $sql = "SELECT * FROM widget where id=".$wid;
	$resultarr = $this->DB->fetchAssoc($sql);
		
	return $result = $resultarr[$wid];
    }
    
    public function _getWidgetTemplateType($wid){
        $sql = "select template_id from ".$this->_name." where id=".$wid;
        $v = $this->DB->fetchOne($sql);
        return $v;
    } 
    
    public function _setBreakingNews($v){
        if(!$v==1) $v=0;
        
        $data = array(
            'active'=>$v    
        );
        $this->DB->update($this->_name, $data,'id=17');
    }
    
    public function _isWidgetActive($wid){
        $sql = "select active from ".$this->_name." where id=".$wid;
        $v = $this->DB->fetchOne($sql);
        return $v;        
    }
    
    public function _updateWidgetName($wid,$name){
        $data = array(
            'name'=>$name
        );
        $this->DB->update($this->_name,$data,'id='.$wid);
    }
    
    public function _getWidgetIdFromName($name)
    {
        $sql = "select id from ".$this->_name." where name='".$name."'";
        $v = $this->DB->fetchOne($sql);
        return $v;
    }
}
