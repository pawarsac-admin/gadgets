<?php
class Application_Model_DbTable_Tbusers extends Zend_Db_Table_Abstract
{
    protected $_name = 'users';
    protected $DB;
    public function __construct(){
          $registry = Zend_Registry::getInstance(); 
	  $this->DB = $registry['DB'];
          $this->auth = Zend_Auth::getInstance();
    }
    public function _adduser($args){
        try{
            $flag = $this->DB->insert($this->_name, $args);
        }catch(Exception $e){
            if($e->getCode() == 23000){
                return "Dublicate Entry!!";                
            }
        }
        if($flag){ return true; }else { return "Invalid Entry!!"; }        
    }
    
    public function _authenticate($args){
	$authAdapter = new Zend_Auth_Adapter_DbTable($this->DB);
        $authAdapter->setIdentity($args['username']);
	$authAdapter->setCredential(md5($args['password']));        
	$authAdapter->setTableName($this->_name)->setIdentityColumn('user_name')->setCredentialColumn('password');    
        
        $result = $this->auth->authenticate($authAdapter);
        
        if($result->isValid()){
          $data = $authAdapter->getResultRowObject(null,'password');
          $this->auth->getStorage()->write($data);
          return true;
        }else{
          return false;
        }        
        
    }
    
    public function _getUserLevel($uid){
        $sql = "select level from ".$this->_name." where id=".$uid;
        return $this->DB->fetchOne($sql);        
    }

}

