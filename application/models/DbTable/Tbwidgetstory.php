<?php
class Application_Model_DbTable_Tbwidgetstory extends Zend_Db_Table_Abstract
{
    protected $_name = 'widget_story';
    public function __construct(){
          $registry = Zend_Registry::getInstance(); 
	  $this->DB = $registry['DB'];
    }
    
    public function _addwidstory($args){
        try{
            $flag = $this->DB->insert($this->_name,$args);
        }catch(Exception $e){
            if($e->getCode() == 23000){
                return "Dublicate Entry!!";                
            }
        }
        if($flag){ return true; }else { return "Invalid Entry!!"; }           
    }
    
    public function _getwidgetStories($wid){
        $sql = 'SELECT w.order, s . *,w.imgname
                        FROM story s
                        INNER JOIN widget_story w ON s.id = w.story_id
                        AND w.widget_id ='.trim($wid).'
                        ORDER BY w.order';

        $result = $this->DB->fetchAll($sql);   
        return $result;
    }
    
    public function _updatewidstory($data,$wid,$v){
        $this->DB->update($this->_name, $data,'widget_id='.$wid." and story_id=".$v);				
    }
    
    public function _deleteStory($wid,$sid){
        $flag = $this->DB->delete('widget_story','widget_id='.$wid." and story_id=".$sid);
    }
    
    public function _liveUpdate($wid){
        $sql = "select * from widget_story as w where widget_id=".$wid." ORDER BY w.order";
        $result = $this->DB->fetchAll($sql);
        
        
        $this->DB->delete('published_widget_story','widget_id='.$wid);
        foreach($result as $k=>$r){
            $new_r =array();
            foreach($r as $k1=>$v1){
                $new_r[$k1] = $v1; 
            }
            $flag = $this->DB->insert('published_widget_story',$new_r);
        }
        
        
    }

}

