<?php

class Application_Model_DbTable_Tbpublishedwidgetstory extends Zend_Db_Table_Abstract
{

    protected $_name = 'published_widget_story';
    public function __construct(){
          $registry = Zend_Registry::getInstance(); 
	  $this->DB = $registry['DB'];
    } 
    
    public function _getWidgetStories($wid){
        $sql = "SELECT *
                FROM published_widget_story pws
                INNER JOIN story s ON s.id = pws.story_id
                AND pws.widget_id =".$wid."
                ORDER BY pws.order ASC";
        
        if($_SERVER['USEMEMCACHE']){
        /* Memcache code*/
                $frontendOpts = array(
                    'caching' => true,
                    'lifetime' => 3600,
                    'automatic_serialization' => true
                );

                $backendOpts = array(
                    'servers' =>array(
                        array(
                        'host'   => $_SERVER['MEMSERVER'],
                        'port'   => $_SERVER['MEMPORT'],
                        'weight' => 1
                        )
                    ),
                    'compression' => false
                );
                $cache = Zend_Cache::factory('Core', 'Memcached', $frontendOpts, $backendOpts);

        /* End*/
        }
        
        if($_SERVER['USEMEMCACHE']){
            if(!$rearray = $cache->load('Widgetcache'.$wid)){
                //echo "<!-- In not using memcache for widget id ".$wid." -->"; 
                $rearray = $this->DB->fetchAll($sql);        
                $cache->save(serialize($rearray), 'Widgetcache'.$wid);                 
            }else{ 
                //echo "<!-- In memcache for widget id ".$wid." -->";   
                $rearray = unserialize($rearray);                
            }
        }else {
            $rearray = $this->DB->fetchAll($sql);        
        }   

        return $rearray;
    }


}

