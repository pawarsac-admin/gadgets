<?php

class Application_Model_DbTable_Tbstory extends Zend_Db_Table_Abstract
{

    protected $_name = 'story';
    public function __construct(){
          $registry = Zend_Registry::getInstance(); 
	  $this->DB = $registry['DB'];
    }    

    public function _addstory($args){
        try{
            $flag = $this->DB->insert($this->_name, $args);
        }catch(Exception $e){
            if($e->getCode() == 23000){
                return "Dublicate Entry!!";                
            }
        }
        if($flag){ return true; }else { return "Invalid Entry!!"; }        
    } 
    
    public function _getallstories(){
	$sql = "SELECT * FROM story where status=1 order by id desc";
	$result = $this->DB->fetchAssoc($sql);        
        return $result;
    }
    
    public function _getStory($id){
        $sql = "SELECT * FROM story where id=".$id;
	$result = $this->DB->fetchAssoc($sql);
        return $result;
    }
    
    public function _updateStory($data,$id){
        $flag = $this->DB->update($this->_name, $data,'id='.$id);
    }
    
    public function _flushmedialib(){
        $data = array(
            'status'=>0
        );
        $flag = $this->DB->update($this->_name, $data,'status=1');
    }
    
    public function _getStoryFromTitle($title){
        $sql = "SELECT * FROM story where title='".$title."'";
        $result = $this->DB->fetchRow($sql);
        
        return $result;
    }
}

