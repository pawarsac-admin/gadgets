<?php

class Application_Model_DbTable_Tbadminsectionhtml extends Zend_Db_Table_Abstract
{

    protected $_name = 'admin_section_html';
    public function __construct(){
          $registry = Zend_Registry::getInstance(); 
	  $this->DB = $registry['DB'];
    }    

    public function _getWidgetType($wid){
        $sql = "select template_id from ".$this->_name." where widget_id=".$wid;
        $v = $this->DB->fetchOne($sql);
        return $v;
    }
    
    public function _updateWidgetHtml($wid, $htm){
        $sql = "select * from ".$this->_name." where widget_id=".$wid;
        $r = $this->DB->fetchAssoc($sql);
        $u = array();
        foreach($r as $rr){
            $u = $rr;
            $u['layouthtml'] = $htm;
        }
        //var_dump($u);
        $flag = $this->DB->update($this->_name, $u,'widget_id='.$wid); 
        
        //var_dump($flag);
    }
    
    public function _updateAdTag($p,$c,$htm){
        $u = array();
        $u['layouthtml'] = $htm;
        $flag = $this->DB->update($this->_name, $u,'parent='.$p." AND child=".$c);
        var_dump($flag);
    }
    
    public function _getHtmlForParentChild($p,$c){
        $sql = "select layouthtml from ".$this->_name." where parent=".$p." AND child=".$c;
        return $this->DB->fetchOne($sql);
    }
}

