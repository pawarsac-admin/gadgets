<?php

class Application_Model_DbTable_Tbsectionmapping extends Zend_Db_Table_Abstract
{
    protected $_name = 'section_mapping';
    protected $DB;
    public function __construct(){
          $registry = Zend_Registry::getInstance(); 
	  $this->DB = $registry['DB'];
          $this->auth = Zend_Auth::getInstance();
    }
    
    public function _deleteparent($parent,$layout){
        $this->DB->delete($this->_name,'parent='.$parent." AND layout=".$layout);
    }
    
    public function _updatesecmapping($parent, $layout, $data){
        $this->DB->insert($this->_name,$data);
    }
    
    public function _getAdminLayoutDesign($layout){
        $sql = "select layout from section_layout where id=".$layout;
        $layarr = $this->DB->fetchOne($sql);
        $layarr = unserialize($layarr);
        
        //var_dump($layarr);
        
        $htm = "";
        
        foreach($layarr as $l){
            /* Get Start end html*/   
            $start = "";
            $end = "";
            $sql = "select * from admin_section_start_end where parent=".$l;
            $result = $this->DB->fetchAll($sql);
            if(empty($result)){
               return false;
            }
            
            foreach($result as $k=>$r){
                $start = $r->start;
                $end = $r->end;
            }
            
            /*Get child html*/
            
            $sql = "SELECT sm.child, html
                    FROM admin_section_html adhtml
                    INNER JOIN section_mapping sm ON adhtml.parent = sm.parent
                    AND adhtml.child = sm.child
                    AND sm.parent =$l
                    AND sm.layout =$layout order by sm.id";
            
            $result2 = $this->DB->fetchAll($sql);
            
            //var_dump($result2);
            
            foreach ($result2 as $re){
                $start = $start.$re->html;
            }
            
            $start = $start . $end;
            $htm = $htm.$start;
        }
        
        return $htm;
    }
    
    public function _getAdminSidebarDesign($d){
           /* Get Start end html*/   
            $start = "";
            $end = "";
            $sql = "select * from admin_section_start_end where parent=5";
            $result = $this->DB->fetchAll($sql);
            if(empty($result)){
               return false;
            }
            
            foreach($result as $k=>$r){
                $start = $r->start;
                $end = $r->end;
            }
            
            /*Get child html*/
            
            $sql = "SELECT sm.child, html
                    FROM admin_section_html adhtml
                    INNER JOIN section_mapping sm ON adhtml.parent = sm.parent
                    AND adhtml.child = sm.child
                    AND sm.parent =5
                    AND sm.layout =$d order by sm.id";
            
            $result2 = $this->DB->fetchAll($sql);
            
            //var_dump($result2);
            
            foreach ($result2 as $re){
                $start = $start.$re->html;
            }
            
            $start = $start . $end;
            
            return $start;
            
    }
    
    public function _getLayoutDesign($layout){
        $sql = "select layout from section_layout where id=".$layout;
        $layarr = $this->DB->fetchOne($sql);
        $layarr = unserialize($layarr);
        
        
        $htm = "";
        
        foreach($layarr as $l){
            /* Get Start end html*/   
            $start = "";
            $end = "";
            $sql = "select * from admin_section_start_end where parent=".$l;
            $result = $this->DB->fetchAll($sql);
            if(empty($result)){
               return false;
            }
            
            foreach($result as $k=>$r){
                $start = $r->layoutstart;
                $end = $r->layoutend;
            }
            
            /*Get child html*/
            
            $sql = "SELECT sm.child, layouthtml
                    FROM admin_section_html adhtml
                    INNER JOIN section_mapping sm ON adhtml.parent = sm.parent
                    AND adhtml.child = sm.child
                    AND sm.parent =$l
                    AND sm.layout =$layout order by sm.id";
            
            $result2 = $this->DB->fetchAll($sql);
            
            if(empty($result2)){
            $sql = "SELECT layouthtml
                    FROM admin_section_html where
                    parent =$l
                    AND child =1";
            
            $result2 = $this->DB->fetchAll($sql);                
            }
            
            foreach ($result2 as $re){
                $start = $start.$re->layouthtml;
            }
            
            $start = $start . $end;
            $htm = $htm.$start;
        }
        
        return $htm;
    } 
    
    public function _getSidebarDesign($d){
           /* Get Start end html*/   
            $start = "";
            $end = "";
            $sql = "select * from admin_section_start_end where parent=5";
            $result = $this->DB->fetchAll($sql);
            if(empty($result)){
               return false;
            }
            
            foreach($result as $k=>$r){
                $start = $r->layoutstart;
                $end = $r->layoutend;
            }
            
            /*Get child html*/
            
            $sql = "SELECT sm.child, layouthtml
                    FROM admin_section_html adhtml
                    INNER JOIN section_mapping sm ON adhtml.parent = sm.parent
                    AND adhtml.child = sm.child
                    AND sm.parent =5
                    AND sm.layout =$d order by sm.id";
            
            $feedobj = new Application_Model_Admin();
            if($feedobj->_is_crickfeed_is_up()){
                $start .= $feedobj->_is_crickfeed_is_up();
            
                /* SQL to not to show gallery when Cricket scorecard feed is up*/
                $sql = "SELECT sm.child, layouthtml
                        FROM admin_section_html adhtml
                        INNER JOIN section_mapping sm ON adhtml.parent = sm.parent
                        AND adhtml.child = sm.child
                        AND sm.parent =5 AND sm.child !=1
                        AND sm.layout =$d order by sm.id";          
                /* End*/            
            }
            
            $result2 = $this->DB->fetchAll($sql);
            
            //var_dump($result2);
            
            foreach ($result2 as $re){
                $start = $start.$re->layouthtml;
            }
            
            $start = $start . $end;
            
            return $start;
            
    }          
}

