<?php

class Application_Model_Admin
{

	public static function getResizedImagePath($sImgName,$sSize){
		$upload_path = $_SERVER['UPLOAD_IMAGE_PATH'];
		$sImageName=basename($sImgName);
		$temp=explode(".",$sImageName);
		$sImageName = $upload_path."$sImgName";
		
		$sNewImageName= "";
		
		if(strlen($sSize)>0){
			$sNewImageName=$temp[sizeof($temp)-2].'_'.$sSize.".".$temp[sizeof($temp)-1];
			if(!file_exists($sNewImageName) && file_exists($sImageName)){
				// Initialise / load image
				$resizeObj = new Application_Model_Resizeimage($sImageName);
				$aSize = explode('X',$sSize);
				$iWidth = $aSize[0];
				$iHeight = $aSize[1];
				//Save image
				$resizeObj->resizeImage($iWidth, $iHeight, 'auto');
				$sResizePath =$upload_path.$sNewImageName;
				$resizeObj->saveImage($sResizePath, 85);
			}
			$aNewImages[]=$upload_path.$sNewImageName;
		}
		return $sNewImageName;
	}
        
	public static function getResizedImageThumbPath($sImgName,$sSize){
		$upload_path = $_SERVER['UPLOAD_IMAGE_PATH'];
		$sImageName=basename($sImgName);
		$temp=explode(".",$sImageName);
		$sImageName = $upload_path."$sImgName";
		
		$sNewImageName= "";
		
		if(strlen($sSize)>0){
			$sNewImageName=$temp[sizeof($temp)-2].'_'.$sSize.".".$temp[sizeof($temp)-1];
			if(!file_exists($sNewImageName) && file_exists($sImageName)){
				// Initialise / load image
				$resizeObj = new Application_Model_Resizeimage($sImageName);
				$aSize = explode('X',$sSize);
				$iWidth = $aSize[0];
				$iHeight = $aSize[1];
				//Save image
				$resizeObj->resizeImage($iWidth, $iHeight, 'auto');
				$sResizePath =$upload_path.$sNewImageName;
				$resizeObj->saveImage($sResizePath, 100);
			}
			$aNewImages[]=$upload_path.$sNewImageName;
		}
		return $sNewImageName;
	}        
        
        /**
         * Converts a number of special characters into their HTML entities.
         *
         * Specifically deals with: &, <, >, ", and '.
         *
         */
        function _handle_specialchars( $string, $quote_style = ENT_QUOTES ) {
                $string = (string) $string;

                if ( 0 === strlen( $string ) ) {
                        return '';
                }

                // Don't bother if there are no specialchars - saves some processing
                if ( !preg_match( '/[&<>"\']/', $string ) ) {
                        return $string;
                }

                // Account for the previous behaviour of the function when the $quote_style is not an accepted value
                if ( empty( $quote_style ) ) {
                        $quote_style = ENT_NOQUOTES;
                } elseif ( !in_array( $quote_style, array( 0, 2, 3, 'single', 'double' ), true ) ) {
                        $quote_style = ENT_QUOTES;
                }

                $charset = 'UTF-8';

                $_quote_style = $quote_style;

                if ( $quote_style === 'double' ) {
                        $quote_style = ENT_COMPAT;
                        $_quote_style = ENT_COMPAT;
                } elseif ( $quote_style === 'single' ) {
                        $quote_style = ENT_NOQUOTES;
                }


                $string = $this->handle_specialchars_decode( $string, $_quote_style );


                // Now proceed with custom double-encoding silliness
                $string = preg_replace( '/&(#?x?[0-9a-z]+);/i', '|wp_entity|$1|/wp_entity|', $string );

                $string = @htmlspecialchars( $string, $quote_style, $charset );

                $string = str_replace( array( '|wp_entity|', '|/wp_entity|' ), array( '&', ';' ), $string );

                // Backwards compatibility
                if ( 'single' === $_quote_style ) {
                        $string = str_replace( "'", '&#039;', $string );
                }

                return $string;
        }

        /**
         * Converts a number of HTML entities into their special characters.
         *
         * Specifically deals with: &, <, >, ", and '.
         *
         * $quote_style can be set to ENT_COMPAT to decode " entities,
         */
        function handle_specialchars_decode( $string, $quote_style = ENT_QUOTES ) {
                $string = (string) $string;

                if ( 0 === strlen( $string ) ) {
                        return '';
                }

                // Don't bother if there are no entities - saves a lot of processing
                if ( strpos( $string, '&' ) === false ) {
                        return $string;
                }

                // Match the previous behaviour of _wp_specialchars() when the $quote_style is not an accepted value
                if ( empty( $quote_style ) ) {
                        $quote_style = ENT_NOQUOTES;
                } elseif ( !in_array( $quote_style, array( 0, 2, 3, 'single', 'double' ), true ) ) {
                        $quote_style = ENT_QUOTES;
                }

                // More complete than get_html_translation_table( HTML_SPECIALCHARS )
                $single = array( '&#039;'  => '\'', '&#x27;' => '\'' );
                $single_preg = array( '/&#0*39;/'  => '&#039;', '/&#x0*27;/i' => '&#x27;' );
                $double = array( '&quot;' => '"', '&#034;'  => '"', '&#x22;' => '"' );
                $double_preg = array( '/&#0*34;/'  => '&#034;', '/&#x0*22;/i' => '&#x22;' );
                $others = array( '&lt;'   => '<', '&#060;'  => '<', '&gt;'   => '>', '&#062;'  => '>', '&amp;'  => '&', '&#038;'  => '&', '&#x26;' => '&' );
                $others_preg = array( '/&#0*60;/'  => '&#060;', '/&#0*62;/'  => '&#062;', '/&#0*38;/'  => '&#038;', '/&#x0*26;/i' => '&#x26;' );

                if ( $quote_style === ENT_QUOTES ) {
                        $translation = array_merge( $single, $double, $others );
                        $translation_preg = array_merge( $single_preg, $double_preg, $others_preg );
                } elseif ( $quote_style === ENT_COMPAT || $quote_style === 'double' ) {
                        $translation = array_merge( $double, $others );
                        $translation_preg = array_merge( $double_preg, $others_preg );
                } elseif ( $quote_style === 'single' ) {
                        $translation = array_merge( $single, $others );
                        $translation_preg = array_merge( $single_preg, $others_preg );
                } elseif ( $quote_style === ENT_NOQUOTES ) {
                        $translation = $others;
                        $translation_preg = $others_preg;
                }

                // Remove zero padding on numeric entities
                $string = preg_replace( array_keys( $translation_preg ), array_values( $translation_preg ), $string );

                // Replace characters according to translation table
                return strtr( $string, $translation );
        }
        
        public function _is_logged_in(){
            $match = $match1 = $match2 = 0;
            $match = preg_match('/^\/publisher\/userlogin\/*/', $_SERVER['REQUEST_URI']);            
            $match1 = preg_match('/^\/publisher\/registeruser\/*/', $_SERVER['REQUEST_URI']);            
            $match2 = preg_match('/^\/publisher\/addwidget\/*/', $_SERVER['REQUEST_URI']);

            if($match === 1 || $match1 === 1 || $match2 === 1 ){

            }else{
                $auth = Zend_Auth::getInstance(); 
                if(!$auth->hasIdentity()){
                  header('Location:/publisher/userlogin?url='.$_SERVER['REQUEST_URI']); 
                }else{
                    return true;
                }
            }            
        }
        
        public function _is_crickfeed_is_up(){
	//return false;
            $curl_url = 'http://www.cricketcountry.com/india_scoreboard.php';
            $curl = curl_init($curl_url);

            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);                         
            curl_setopt($curl, CURLOPT_USERPWD, 'admin:Admin@2012');
            curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_ANY);                    
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);                          
            curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);                           
            curl_setopt($curl, CURLOPT_USERAGENT, 'Homepage Feed');

            $response = curl_exec($curl);                                          
            $resultStatus = curl_getinfo($curl);                                   

            if($resultStatus['http_code'] == 200) {
                $response = trim($response);
                if($response == null || $response == "") return false;
                return '<iframe src="'.$curl_url.'" frameborder="0"></iframe>';
            } else {
                return false;
            }
            
        }
        
        public function _check_level_permission($l,$flag=true){
                    
            $auth = Zend_Auth::getInstance();        
            $level = $auth->getIdentity()->level;

            if(!($level >= $l) && $flag ){ echo "No Permission. Contact site admin!";  die(); }        
            if(!($level >= $l) && !$flag ){ return false; }
            
            return true;
        }

}

