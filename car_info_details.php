<?php
	require_once('./include/config.php');
	require_once(CLASSPATH.'DbConn.php');
	require_once(CLASSPATH.'DbConn.php');
	require_once(CLASSPATH.'brand.class.php');
	require_once(CLASSPATH.'category.class.php');
	require_once(CLASSPATH.'pivot.class.php');
	require_once(CLASSPATH.'feature.class.php');
	require_once(CLASSPATH.'product.class.php');
	require_once(CLASSPATH.'user.class.php');
	require_once(CLASSPATH.'price.class.php');
	require_once(CLASSPATH.'user_review.class.php');
	require_once(CLASSPATH."citystate.class.php");
	require_once(CLASSPATH.'Utility.php');
	require_once(CLASSPATH.'videos.class.php');


	$dbconn = new DbConn;
	$brand = new BrandManagement;
	$category = new CategoryManagement;
	$pivot = new PivotManagement;
	$feature = new FeatureManagement;
	$oProduct = new ProductManagement;
	$oUser = new User;
	$price = new price;
	$userreview = new USERREVIEW;
	$oCityState = new citystate;
	$videoGallery = new videos();
	$domain=DOMAIN;
	

	//print_r($_POST); //die();

	//$getAddUsrInfo = $_REQUEST['chkusrinfo'];
	$iSelBrandId = $_POST['Brand'];
	$iSelModel = $_POST['Model'];
	$iProductId = $_POST['ModelVariant'] ? $_POST['ModelVariant'] : $_POST['pid'];
	$iSelCity  = $_POST['Select_City'];
	//$sPeriod = $_POST['Select_a_period'];
	$category_id = $_REQUEST['catid'] ? $_REQUEST['catid'] : SITE_CATEGORY_ID;

	$sName = $_POST['name'];
	$sEmail = $_POST['email'];
	$sMobile = $_POST['mobile'];
	$StdCode = $_POST['std_code'];
	$ContactNo = $_POST['contact_no'];
	$period=$_POST['select_period'];
	$chkuserid=$_REQUEST['chkuserid'];
	//echo "<br>".$iSelBrandId."---".$iSelModel."---".$iProductId."---".$iSelCity."---".$sName."<br>";
	unset($seoTitleArr);
	if($iSelBrandId== '' || $iSelModel=='' || $iProductId=='' || $iSelCity=='' || $sName==''){
	   	$seoTitleArr[] = SEO_WEB_URL;
		$seoTitleArr[] = SEO_GET_ON_ROAD_PRICE; //"Get-Car-Info";
		$seo_url = implode("/",$seoTitleArr);
		header('Location:'.$seo_url."/");exit;
	
	}

	//if(empty($iProductId)){ 
		if(!empty($iProductId)){
			$queryArr[] = "pid=$iProductId";
		}
		if(!empty($pname)){
			$queryArr[] = "pname=$pname";
		}
		if(sizeof($queryArr) > 0){
			$queryStr = "?".implode("&",$queryArr);
		}
		if(!empty($iProductId)){
			$product_result = $oProduct->arrGetProductDetails($iProductId,$category_id,"",'1',"","","","","","","","",$iSelCity);
			//print_r($product_result); echo "ddsdsds"; 
			$icnt = sizeof($product_result);
			if(!empty($product_result)){
				//echo "SDSDSDSDSD";
				$product_name = $product_result[0]['product_name'];
				$product_variant = $product_result[0]['variant'];
				$brand_id = $product_result[0]['brand_id'];
				$brand_result=$brand->arrGetBrandDetails($brand_id,$category_id);
				$brand_name=$brand_result[0]['brand_name'];
				$product_link_name=$brand_name."-".$product_name;
				
				$product_brand_name = html_entity_decode($brand_name,ENT_QUOTES,'UTF-8');
				$product_brand_name = removeSlashes($product_brand_name);
				$product_brand_name = seo_title_replace($product_brand_name);
				$video_photo_brand_name=$product_brand_name;

				$product_link_name = html_entity_decode($product_link_name,ENT_QUOTES,'UTF-8');
				$product_link_name = removeSlashes($product_link_name);
				$product_link_name = seo_title_replace($product_link_name);
				$video_photo_link_name=$product_link_name;

				$product_variant_name = html_entity_decode($product_variant,ENT_QUOTES,'UTF-8');
				$product_variant_name = removeSlashes($product_variant_name);
				$product_variant_name = seo_title_replace($product_variant_name);
				$video_photo_variant_name=$product_variant_name;
			}
		}

	//die();

	if(!preg_match("/^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/",$sEmail,$matches)){
		//echo "error";exit;
		unset($seoTitleArr);
		$seoTitleArr[] = SEO_WEB_URL;
		$seoTitleArr[] = $product_brand_name."-cars";
		$seoTitleArr[] = $product_link_name;
		$seoTitleArr[] = $product_variant_name;
		$seoTitleArr[] = SEO_GET_ON_ROAD_PRICE; //"Get-Car-Info";
		$seoTitleArr[] = $iProductId;
		$seo_url = implode("/",$seoTitleArr);
		/////////header('Location:'.$seo_url);exit;
	}
	
	if(!preg_match("/^[0-9]{10}$/",$sMobile,$matches) and (!preg_match("/^[0-9]{3}$/",$StdCode,$matches) and !preg_match("/^[0-9]{8}$/",$ContactNo,$matches))){
		//echo "error";exit;
		unset($seoTitleArr);
		$seoTitleArr[] = SEO_WEB_URL;
		$seoTitleArr[] = $product_brand_name."-cars";
		$seoTitleArr[] = $product_link_name;
		$seoTitleArr[] = $product_variant_name;
		$seoTitleArr[] = SEO_GET_ON_ROAD_PRICE; //"Get-Car-Info";
		$seoTitleArr[] = $iProductId;
		$seo_url = implode("/",$seoTitleArr);
		////////////header('Location:'.$seo_url);exit;
	}
//die();
	

	$city_name="";
        if(!empty($iSelCity)){
                $city_result = $oCityState->arrGetCityDetails($iSelCity,"","1");
                $cnt = sizeof($city_result);
                if(!empty($cnt)){
                        $city_name = $city_result["0"]["city_name"];
                }
        }
	$selected_brand_name="";
	if(!empty($iSelBrandId)){
                $brand_result = $brand->arrGetBrandDetails($iSelBrandId,$category_id,"1");
                $cnt = sizeof($brand_result);
                if(!empty($cnt)){
                        $selected_brand_name = $brand_result["0"]["brand_name"];
                }
        }
	$select_model_name="";
	if(!empty($iSelModel)){
                $model_result = $oProduct->arrGetProductNameInfo($iSelModel,$category_id,"","","1","","");
                $cnt = sizeof($model_result);
                if(!empty($cnt)){
                        $selected_model_name = $model_result["0"]["product_info_name"];
                }
        }
	
	if(empty($iProductId)){
		unset($seoTitleArr);
		$seoTitleArr[] = SEO_WEB_URL;
		$seoTitleArr[] = SEO_GET_ON_ROAD_PRICE; //"Get-Car-Info";
		$seo_url = implode("/",$seoTitleArr);
		header('Location:'.$seo_url."/");exit;
	}

	/*if(!empty($getAddUsrInfo)){
		$msg = "<div class=\"p10 ac\"><div class='noRstTxt'>Thank you for contacting us. You will shortly receive an email with the requested on-road price.<div class=\"marAuto w100px pt10\"><div class=\"mrBtn fr\"><ul class=\"org\"><li><a href='".WEB_URL.SEO_CAR_FINDER."' class=\"current\"><span>More cars</span></a></li></ul></div><div class=\"cb\"></div></div></div></div>";
	}
	*/
	$price_result = $price->arrGetPriceDetails('',$iProductId,$category_id,"","",$iSelCity,"1","","","");

	//print"<pre>";print_r($price_result);print"</pre>";
	
	$variant_result = $price->arrGetVariantDetailCount($category_id);
	//print"<pre>";print_r($variant_result);print"</pre>";
	$variantcnt = $variant_result[0]['cnt'];
	$cnt = sizeof($price_result);
	if($variantcnt == $cnt){
		$msg ='';
		$price_replyed = 1;
	
	}else{
		$msg = "<div class=\"p10 ac\"><div class='noRstTxt'>Thank you for contacting us. You will shortly receive an email with the requested on-road price.<div class=\"marAuto w100px pt10\"><div class=\"mrBtn fr\"><ul class=\"org\"><li><a href='".WEB_URL.SEO_CAR_FINDER."' class=\"current\"><span>More cars</span></a></li></ul></div><div class=\"cb\"></div></div></div></div>";
		$price_replyed = 0;
	}

	//if($_REQUEST['name']!="" && strlen($_REQUEST['name'])>0 && empty($getAddUsrInfo)){
	if($_POST['name']!="" && strlen($_POST['name'])>0){
		$aParameter=array("profile_name"=>$sName,"email"=>$sEmail,"mobile"=>$sMobile,"std_code"=>$StdCode,"contact_no"=>$ContactNo,"period"=>$period,"create_date"=>'now()',"brand_id"=>$iSelBrandId,"product_info_id"=>$iSelModel,"product_id"=>$iProductId,"city_id"=>$iSelCity,"price_replyed"=>$price_replyed,"request_type"=>1);
		$iUserDetail=$oUser->intInsertUserDetail($aParameter);
		if($iUserDetail!=''){ 
			//setcookie('chkusrinfo','1');
			setcookie('chkusrinfo','1', time()+3600*24,'/',$domain);
			setcookie('chkuserid',$iUserDetail, time()+3600*24,'/',$domain);
			setcookie('rev_username',$sName, time()+3600*24,'/',$domain);
			setcookie('rev_emailid',$sEmail, time()+3600*24,'/',$domain);

			setcookie('rev_mobileno',$sMobile, time()+3600*24,'/',$domain);
			setcookie('rev_stdcode',$StdCode, time()+3600*24,'/',$domain);
			setcookie('rev_contactno',$ContactNo, time()+3600*24,'/',$domain);

			$getAddUsrInfo = 1;
		}
	}


	//echo $iProductId; echo $category_id ; die();
	$getAddUsrInfo = 1;
	/*if(empty($getAddUsrInfo)){ 
		if(!empty($iProductId)){
			$queryArr[] = "pid=$iProductId";
		}
		if(!empty($pname)){
			$queryArr[] = "pname=$pname";
		}
		if(sizeof($queryArr) > 0){
			$queryStr = "?".implode("&",$queryArr);
		}
		$product_result = $oProduct->arrGetProductDetails($iProductId,$category_id,"",'1',"","","","","","","","",$iSelCity);
		//print_r($product_result ); die();

		$icnt = sizeof($product_result);
		$product_name = $product_result[0]['product_name'];
		$product_variant = $product_result[0]['variant'];
		$brand_id = $product_result[0]['brand_id'];
		$brand_result=$brand->arrGetBrandDetails($brand_id,$category_id);
		$brand_name=$brand_result[0]['brand_name'];
		$product_link_name=$brand_name."-".$product_name;
		
		$product_brand_name = html_entity_decode($brand_name,ENT_QUOTES,'UTF-8');
		$product_brand_name = removeSlashes($product_brand_name);
		$product_brand_name = seo_title_replace($product_brand_name);
		$video_photo_brand_name=$product_brand_name;

		$product_link_name = html_entity_decode($product_link_name,ENT_QUOTES,'UTF-8');
		$product_link_name = removeSlashes($product_link_name);
		$product_link_name = seo_title_replace($product_link_name);
		$video_photo_link_name=$product_link_name;

		$product_variant_name = html_entity_decode($product_variant,ENT_QUOTES,'UTF-8');
		$product_variant_name = removeSlashes($product_variant_name);
		$product_variant_name = seo_title_replace($product_variant_name);
		$video_photo_variant_name=$product_variant_name;

		unset($seoTitleArr);
		$seoTitleArr[] = SEO_WEB_URL;
		$seoTitleArr[] = $product_brand_name."-cars";
		$seoTitleArr[] = $product_link_name;
		$seoTitleArr[] = $product_variant_name;
		$seoTitleArr[] = SEO_GET_ON_ROAD_PRICE; //"Get-Car-Info";
		$seoTitleArr[] = $iProductId;
		$seo_url = implode("/",$seoTitleArr);
		header('Location:'.$seo_url);exit;
	}*/
	$aProductDetail=$oProduct->arrGetProductDetails($iProductId,$category_id,"",'1',"","","1","","","","","",$iSelCity);
	
	//	print "<pre>"; print_r($aProductDetail); die();
	if(is_array($aProductDetail)){
		$product_name = $aProductDetail[0]['product_name'];
		$product_variant = $aProductDetail[0]['variant'];
		$brand_id = $aProductDetail[0]['brand_id'];
		$selected_brand_id = $brand_id;
		$brand_result=$brand->arrGetBrandDetails($brand_id,$category_id);
		$brand_name=$brand_result[0]['brand_name'];
		$product_link_name=$brand_name."-".$product_name;
		unset($result);
		$result = $oProduct->arrGetProductNameInfo("",$category_id,$brand_id,$product_name,"1","","");
		$product_name_id = $result[0]["product_name_id"];
		$selected_product_name_id = $product_name_id;
		
		$product_brand_name = html_entity_decode($brand_name,ENT_QUOTES,'UTF-8');
		$product_brand_name = removeSlashes($product_brand_name);
		$product_brand_name = seo_title_replace($product_brand_name);
		$video_photo_brand_name=$product_brand_name;

		$product_link_name = html_entity_decode($product_link_name,ENT_QUOTES,'UTF-8');
		$product_link_name = removeSlashes($product_link_name);
		$product_link_name = seo_title_replace($product_link_name);
		$video_photo_link_name=$product_link_name;

		$product_variant_name = html_entity_decode($product_variant,ENT_QUOTES,'UTF-8');
		$product_variant_name = removeSlashes($product_variant_name);
		$product_variant_name = seo_title_replace($product_variant_name);
		$video_photo_variant_name=$product_variant_name;
		
		$result_count=$userreview->arrGetUserReviewDetailsCount("","","","","","",$category_id,$product_name_id);

		$result = $userreview->arrGetAdminOverallGrade($category_id,$brand_id,'0',$model_id);
		$cnt = sizeof($result);
		$overallcnt = 0;
		$overallavg = round($result[0]['overallgrade']);
		if($cnt <= 0){
		        $result = $userreview->arrGetOverallGrade($category_id,$brand_id,'0',$product_name_id);
		        $overallcnt = $result[0]['totaloverallcnt'];
		        $overallavg = round($result[0]['overallavg']);
		}
		$html = "";
		for($grade=1;$grade<=5;$grade++){
		        if($grade <= $overallavg){
                		$html .= '<img src="'.IMAGE_URL.'spacer.gif" class="vsblStr"/>';
		        }else{
                		$html .= '<img src="'.IMAGE_URL.'spacer.gif" class="dsblStr"/>';
		        }
		}
		$rev="";
		$rev .= "<MODEL_USER_REVIEW_DATA>";
		$rev .= "<OVERALL_TOTAL_CNT><![CDATA[$overallcnt]]></OVERALL_TOTAL_CNT>";
		$rev .= "<OVERALL_AVG_HTML><![CDATA[$html]]></OVERALL_AVG_HTML>";
		$rev .= "<OVERALL_AVG_CNT><![CDATA[$overallavg]]></OVERALL_AVG_CNT>";
		$rev .= "<OVERALL_TOTAL_CNT><![CDATA[$overallcnt]]></OVERALL_TOTAL_CNT>";
		$rev .= "<REVCOUNT><![CDATA[".$result_count."]]></REVCOUNT>";
		$rev .= "</MODEL_USER_REVIEW_DATA>";

		//seo user review
                unset($seoTitleArr);
                $seoTitleArr[] = SEO_WEB_URL;
                $seoTitleArr[] = str_replace(" ","-",$brand_name)."-cars";
                $seoTitleArr[] = str_replace(" ","-",$brand_name)."-".str_replace(" ","-",$product_name); 
                $seoTitleArr[] = str_replace(" ","-",$product_variant_name);
                $seoTitleArr[] = "Tech-specification";
                $seoTitleArr[] = $iProductId;
                $seoTitleArr[] = "3";
                $tech_spec_url = implode("/",$seoTitleArr);

		unset($seoTitleArr);
                $seoTitleArr[] = SEO_WEB_URL;
                $seoTitleArr[] = str_replace(" ","-",$brand_name)."-cars";
                $seoTitleArr[] = str_replace(" ","-",$brand_name)."-".str_replace(" ","-",$product_name);
                $seoTitleArr[] = str_replace(" ","-",$product_variant_name);
                $seoTitleArr[] = "Features";
                $seoTitleArr[] = $iProductId;
                $seoTitleArr[] = "2";
                $features_url = implode("/",$seoTitleArr);

		                unset($seoTitleArr);
                $seoTitleArr[] = SEO_WEB_URL;
                $seoTitleArr[] = str_replace(" ","-",$brand_name)."-cars";
                $seoTitleArr[] = str_replace(" ","-",$brand_name)."-".str_replace(" ","-",$product_name);
                $seoTitleArr[] = "reviews-ratings";
                $seoTitleArr[] = "model-user-reviews";
                $seoTitleArr[] = str_replace(" ","-",$product_name);
                $seoTitleArr[] = $product_name_id;
                $review_seo_url = implode("/",$seoTitleArr);
                $reviewseourl = $review_seo_url;
	
	}
	
	$aProductWithPriceDetail=$oProduct->constantProductDetails($aProductDetail,$category_id,$iSelCity,$price_other_param,$variantcnt);
	//print_r($aProductWithPriceDetail);die();
	if(is_array($aProductWithPriceDetail)){
		$icnt=sizeof($aProductWithPriceDetail);
		$sProductDetXml = "<PRODUCT_DETAIL>";
		for($i=0;$i<$icnt;$i++){
			$sImagePath=$aProductWithPriceDetail[$i]['image_path'];
			if(!empty($sImagePath)){
				$sImagePath = CENTRAL_IMAGE_URL.$sImagePath;
			}
			$aProductWithPriceDetail[$i]['image_path'] = $sImagePath;
			$brand_id=$aProductWithPriceDetail[$i]['brand_id'];
			$product_id=$aProductWithPriceDetail[$i]['product_id'];
			
			$aBrandDetail=$brand->arrGetBrandDetails($brand_id,$category_id);
			$brand_name=$aBrandDetail['0']['brand_name'];
			$aProductWithPriceDetail[$i]['brand_name']=$brand_name;
			
			$sProductName = $aProductWithPriceDetail[$i]['product_name'];
			$sProductDispName = $aProductWithPriceDetail[$i]['product_name'];
			$sVariant=$aProductWithPriceDetail[$i]['variant'];
			
			$aPriceDetails=$aProductWithPriceDetail[$i]['price_details'];
			if(is_array($aPriceDetails)){
				foreach($aPriceDetails as $iKey=>$aPriceDetail){
					$aPriceDetail['variant_value'] = priceFormat($aPriceDetail["variant_value"]);
					$aProductPriceData[$aPriceDetail['variant_id']]=$aPriceDetail;
					$sCityName=$aPriceDetail['city_name'];
				}
				ksort($aProductPriceData);
				foreach($aProductPriceData as $isKey=>$aProductPriceDetail){
					$aSortedProductPriceDetail[]=$aProductPriceDetail;
				}
			}
			//print "<pre>"; print_r($aSortedProductPriceDetail);print"</pre>";

			
			$sProductPrice=arraytoxml($aSortedProductPriceDetail,"PRICE_DETAIL_DATA");
			$sProductPriceXml ="<PRICE_DETAIL>".$sProductPrice."</PRICE_DETAIL>";

			
			$aSimilarProductDetails=$aProductWithPriceDetail[$i]['similar_product'];
			$isim_cnt=sizeof($aSimilarProductDetails);
			$sSimilarProductDetXml = "<SIMILAR_PRODUCT_DETAIL>";
			for($j=0;$j<$isim_cnt;$j++){
				$product_id=$aSimilarProductDetails[$j]['product_id'];
				unset($price_result);unset($variant_result);
				$price_result = $price->arrGetPriceDetails('',$product_id,$category_id,"","",$iSelCity,"1","","","");
				$variant_result = $price->arrGetVariantDetailCount($category_id);
				//print "<pre>"; print_r($price_result);print"</pre>";
				//print "<pre>"; print_r($variant_result);print"</pre>";
				$variantcnt = $variant_result[0]['cnt'];
				$cnt = sizeof($price_result);
				if($variantcnt == $cnt){
					$brand_id=$aSimilarProductDetails[$j]['brand_id'];
					$product_name=$aSimilarProductDetails[$j]['product_name'];
					$variant=$aSimilarProductDetails[$j]['variant'];

					$aBrandDetail=$brand->arrGetBrandDetails($brand_id,$category_id);
					$brand_name=$aBrandDetail['0']['brand_name'];
					
					$product_link_name=$brand_name."-".$product_name;

					$product_brand_name = html_entity_decode($brand_name,ENT_QUOTES,'UTF-8');
					$product_brand_name = removeSlashes($product_brand_name);
					$product_brand_name = seo_title_replace($product_brand_name);

					$product_link_name = html_entity_decode($product_link_name,ENT_QUOTES,'UTF-8');
					$product_link_name = removeSlashes($product_link_name);
					$product_link_name = seo_title_replace($product_link_name);

					$product_variant_name = html_entity_decode($variant,ENT_QUOTES,'UTF-8');
					$product_variant_name = removeSlashes($product_variant_name);
					$product_variant_name = seo_title_replace($product_variant_name);

					unset($seoTitleArr);
					$seoTitleArr[] = SEO_WEB_URL;
					$seoTitleArr[] = $product_brand_name."-cars";
					$seoTitleArr[] = $product_link_name;
					$seoTitleArr[] = $product_variant_name;
					$seoTitleArr[] = SEO_VIEW_ON_ROAD_PRICE; //"On-Road-Price";
					$seoTitleArr[] = $product_id;
					$seoTitleArr[] = $iSelCity;
					$seo_url = implode("/",$seoTitleArr);
					$aSimilarProductDetails[$j]['seo_url']=$seo_url;

					$aSimilarProductDetails[$j]['On_Road_Price'] = priceFormat($aSimilarProductDetails[$j]['On_Road_Price']);
					$aSimilarProductDetails[$j]['compare_price_difference'] = priceFormat($aSimilarProductDetails[$j]['compare_price_difference']);
					$aSimilarProductDetails[$j]['exshowroomprice'] = priceFormat($aSimilarProductDetails[$j]['exshowroomprice']);
					$aSimilarProductDetails[$j] = array_change_key_case($aSimilarProductDetails[$j],CASE_UPPER);
					$sSimilarProductDetXml .= "<SIMILAR_PRODUCT_DATA>";
					foreach($aSimilarProductDetails[$j] as $k => $v){
						$sSimilarProductDetXml .= "<$k><![CDATA[$v]]></$k>";
					}
					$sSimilarProductDetXml .= "</SIMILAR_PRODUCT_DATA>";
				}
			}
			$sSimilarProductDetXml .= "</SIMILAR_PRODUCT_DETAIL>";

	
				$product_link_name=$brand_name."-".$sProductName;
				
				$product_brand_name = html_entity_decode($brand_name,ENT_QUOTES,'UTF-8');
				$product_brand_name = removeSlashes($product_brand_name);
				$product_brand_name = seo_title_replace($product_brand_name);

				$product_link_name = html_entity_decode($product_link_name,ENT_QUOTES,'UTF-8');
				$product_link_name = removeSlashes($product_link_name);
				$product_link_name = seo_title_replace($product_link_name);

				$product_variant_name = html_entity_decode($sVariant,ENT_QUOTES,'UTF-8');
				$product_variant_name = removeSlashes($product_variant_name);
				$product_variant_name = seo_title_replace($product_variant_name);

				$result = $feature->arrGetFeatureMainGroupDetails("",$category_id,"",$startlimit,$limitcnt);
				$cnt=sizeof($result);
				$sSeoUrlDetXml = "<SEO_URL_DETAIL>";
				for($j=0;$j<$cnt;$j++){
					unset($seoTitleArr);
					$seoTitleArr[] = SEO_WEB_URL;
					$seoTitleArr[] = $product_brand_name."-cars";
					$seoTitleArr[] = $product_link_name;
					$seoTitleArr[] = $product_variant_name;
					$group_id=$result[$j]['group_id'];
					if(!empty($group_id)){
						if($group_id==2){
							$seoTitleArr[] = 'Features';
						}
						if($group_id==3){
							$seoTitleArr[] = 'Tech-specification';
						}
					}
					$seoTitleArr[] = $iProductId;
					if($group_id!=1){
						$seoTitleArr[] = $group_id;
					}
					$seo_url = implode("/",$seoTitleArr);
					$main_group_name=$result[$j]['main_group_name'];
					if($main_group_name!='Overview'){
						$sSeoUrlDetXml .= "<SEO_URL_DATA>";
						$sSeoUrlDetXml .= "<MAIN_GROUP_NAME><![CDATA[$main_group_name]]></MAIN_GROUP_NAME>";
						$sSeoUrlDetXml .= "<SEO_URL><![CDATA[$seo_url]]></SEO_URL>";
						$sSeoUrlDetXml .= "</SEO_URL_DATA>";
					}
				}
				$sSeoUrlDetXml .= "</SEO_URL_DETAIL>";
		
			$aProductWithPriceDetail[$i]['On_Road_Price'] = priceFormat($aProductWithPriceDetail[$i]['On_Road_Price']);
			//print "<pre>"; print_r($aProductWithPriceDetail);print"</pre>";
			$aProductWithPriceDetail[$i] = array_change_key_case($aProductWithPriceDetail[$i],CASE_UPPER);
			
			$sProductDetXml .= "<PRODUCT_DETAIL_DATA>";
			foreach($aProductWithPriceDetail[$i] as $k => $v){
				$sProductDetXml .= "<$k><![CDATA[$v]]></$k>";
			}
			$sProductDetXml .=$sSeoUrlReviewsDetXml;
			$sProductDetXml .= "</PRODUCT_DETAIL_DATA>";
		}
		$sProductDetXml .= "</PRODUCT_DETAIL>";
	}

	$aBrandDetail=$brand->arrGetBrandDetails("",$category_id);
        $sBrandDataDet=arraytoxml($aBrandDetail,"BRAND_DETAIL_DATA");
        $sBrandDataDetXML ="<BRAND_DETAIL>".$sBrandDataDet."</BRAND_DETAIL>";
        if(is_array($aBrandDetail)){
                foreach($aBrandDetail as $ibKey=>$aBrandData){
                        $aBrandDetailName[$aBrandData['brand_id']][]=$aBrandData['brand_name'];
                }
        }

	$aProductCompetitorDetail=$oProduct->arrGetProdCompetitorDetails("","0",$iSelModel,'',$category_id,"1","0","3");
        //print"<pre>";print_r($aProductCompetitorDetail);print"</pre>";
	if(is_array($aProductCompetitorDetail)){
                foreach($aProductCompetitorDetail as $iKey=>$aValue){

                        $aproduct_info_ids=$aValue['product_info_ids'];
                        $aProductInfoDetail=$oProduct->arrGetProductNameInfo($aproduct_info_ids,$category_id,"","","1","","");
                        $aproduct_info_name=$aProductInfoDetail[0]['product_info_name'];
                        //echo "TEST--";  print_r($aProductInfoDetail);
                        $aProductDataDetail[$iKey]=$aProductInfoDetail[0];

                        $aProductName = $oProduct->arrGetProductByName($aproduct_info_name,"","","1","","");
                        if(is_array($aProductName)){
                                foreach($aProductName as $ilKey=>$pValue){
                                        $aproduct_ids[]=$pValue['product_id'];
                                        $aTopCompt[$pValue['product_id']][]=$pValue;
                                }
                        }
                        $sProductIds=implode(",",$aproduct_ids);
                        unset($aproduct_ids);
                        $aPriceVariantData=$price->arrGetVariantValueDetail("","1",$sProductIds,$category_id,"","","","1",$startlimit,$cnt);
                        if(is_array($aPriceVariantData)){
                                foreach($aPriceVariantData as $iprKey=>$aPriceVariantDet){
                                        $aProductPriceInfo[]=$aPriceVariantDet['variant_value'];
                                }
                        }
                        $aProductDataDetail[$iKey]['price_val']=$aProductPriceInfo;
                        unset($aProductPriceInfo);
                }
        }
	
	$sProductIds='';
        $aCompProductData=array();

	if(is_array($aProductDataDetail)){
        foreach($aProductDataDetail as $ipKey=>$aProductValueData){
				$iProductID=$aProductValueData['product_name_id'];
                $sProductName=$aProductValueData['product_info_name'];
                $icBrandId=$aProductValueData['brand_id'];
                //$variant=$aProductValueData['variant'];
                $aCompProductData[$ipKey]=$aProductValueData;
                $sImagePath=$aProductValueData['image_path'];
				$img_media_id = $aProductValueData['img_media_id'];
                if(!empty($sImagePath)){
                        //$sImagePath = resizeImagePath($sImagePath,"555X416",$aModuleImageResize,$img_media_id);
                        $sImagePath = $sImagePath ? CENTRAL_IMAGE_URL.$sImagePath : "";
                }
                $aCompProductData[$ipKey]['image_path']=$sImagePath;
                if(is_array($aBrandDetailName) && isset($aBrandDetailName[$icBrandId])){
                        $sBrandName=$aBrandDetailName[$icBrandId][0];
                        $sDisplayName=$sBrandName." ".$sProductName;
                        $aCompProductData[$ipKey]['display_name']=$sDisplayName;
                }
                $aProductPriceInfo=$aProductValueData['price_val'];
                if(is_array($aProductPriceInfo)){
                        $aPriceVariantValue = $aProductPriceInfo;
                        sort($aPriceVariantValue);
                        $EndCountprice=(count($aPriceVariantValue)-1);
                        if(count($aPriceVariantValue)>1){$endCount=$EndCountprice;}else{$endCount=1;}
                        $aCompProductData[$ipKey]['low_price_range_orginal'] = $aPriceVariantValue[0];
                        $aCompProductData[$ipKey]['low_price_range'] = $aPriceVariantValue[0] ? priceFormat($aPriceVariantValue[0]) : '';
                        $aCompProductData[$ipKey]['high_price_range_orginal'] = $aPriceVariantValue[$endCount];
                        $aCompProductData[$ipKey]['high_price_range'] = $aPriceVariantValue[$endCount] ? priceFormat($aPriceVariantValue[$endCount]) : '';
                }
				$sim_brand_name = html_entity_decode($sBrandName,ENT_QUOTES,'UTF-8');
                $sim_brand_name = removeSlashes($sim_brand_name);
                $sim_brand_name = seo_title_replace($sim_brand_name);

                $sim_link_name=$sim_brand_name."-".$sProductName;
                $sim_link_name = html_entity_decode($sim_link_name,ENT_QUOTES,'UTF-8');
                $sim_link_name = removeSlashes($sim_link_name);
                $sim_link_name = seo_title_replace($sim_link_name);
                unset($seoTitleArr);
                $seoTitleArr[] = SEO_WEB_URL;
                $seoTitleArr[] = trim($sim_brand_name)."-cars";
                $seoTitleArr[] = trim($sim_link_name);
                $seoTitleArr[] = 'Model';
                $seoTitleArr[] = trim($sProductName);
                $seoTitleArr[] = $iProductID;
                $seo_url = implode("/",$seoTitleArr);
                $aCompProductData[$ipKey]['seo_url'] = trim($seo_url);
                unset($seoTitleArr);
                $seoTitleArr[] = SEO_WEB_URL;
                $seoTitleArr[] = trim($sim_brand_name)."-cars";
                $seoTitleArr[] = trim($sim_link_name);
                $seoTitleArr[] = SEO_GET_ON_ROAD_PRICE ; //"Get-On-Road-Price";
                $seoTitleArr[] = trim($sProductName);
                $seo_url1 = implode("/",$seoTitleArr);
                $aCompProductData[$ipKey]['seo_url1'] = trim($seo_url1);
                $aCompProductData[$ipKey] = array_change_key_case($aCompProductData[$ipKey],CASE_UPPER);
                }
                //print_r($aCompProductData);
        $sComptProductDet=arraytoxml($aCompProductData,"COMPETITOR_PRODUCT_DETAIL_DATA");
        $sTopCopmetitorsListing ="<COMPETITOR_PRODUCT_DETAIL>".$sComptProductDet."</COMPETITOR_PRODUCT_DETAIL>";
}

	
	//SEO details
	unset($seoTitleArr);
	$seoTitleArr[] = $sProductDispName;
	$seoTitleArr[] = $sVariant;
	$seoTitleArr[] = "- On Road Price in";
	$seoTitleArr[] = $sCityName;
	$seoTitleArr[] = ' | Get price of all variant of ';
	$seoTitleArr[] = $sProductDispName;
	$seoTitleArr[] = ' in india at '.SEO_DOMAIN;
	$seo_title=implode(" ",$seoTitleArr);

	unset($seoTitleArr);
	$seoTitleArr[] ='Get price of ';
	$seoTitleArr[] = $sProductDispName;
	$seoTitleArr[] = $sVariant;
	$seoTitleArr[] = ' in ';
	$seoTitleArr[] = $sCityName;
	$seoTitleArr[] = ' You can get On road mobile prices in various cities of India at '.SEO_DOMAIN;
	$seo_desc=implode(" ",$seoTitleArr);

	unset($seoTitleArr);
	$seoTitleArr[] ='Price of ';
	$seoTitleArr[] = $sProductDispName;
	$seoTitleArr[] = ', price of ';
	$seoTitleArr[] = $sVariant;
	$seoTitleArr[] = ', on road price of ';
	$seoTitleArr[] = $sProductDispName;
	$seoTitleArr[] = $sVariant;
	$seoTitleArr[] = ',car prices, mobile price, auto price, on road price in india, on road mobile prices, on road mobile price.';
	$seo_keywords=implode(" ",$seoTitleArr);

	
	unset($seoTitleArr);
	$product_info_name = seo_title_replace($product_info_name);
	$seoTitleArr[] = SEO_WEB_URL;
	$seoTitleArr[] = $video_photo_brand_name."-cars";
	$seoTitleArr[] = $video_photo_link_name;
	$seoTitleArr[] = $video_photo_variant_name;
	$seoTitleArr[] = "Car-Photos-videos"; //"interior"
	$seoTitleArr[] = $iProductId;
	$seoTitleArr[] = "3"; //"interior"
	$seo_photo_tab_url= implode("/",$seoTitleArr); 

	unset($seoTitleArr);
	$product_info_name = seo_title_replace($product_info_name);
	$seoTitleArr[] = SEO_WEB_URL;
	$seoTitleArr[] = $video_photo_brand_name."-cars";
	$seoTitleArr[] = $video_photo_link_name;
	$seoTitleArr[] = $video_photo_variant_name;
	$seoTitleArr[] = "Videos"; //"interior"
	$seoTitleArr[] = $iProductId;
	$seoTitleArr[] = "1"; //"interior"
	$seo_video_tab_url= implode("/",$seoTitleArr); 

	$breadcrumb = CATEGORY_HOME." <a href='".WEB_URL."Car-Search'>New Mobile search</a> > <a href='".WEB_URL.SEO_GET_ON_ROAD_PRICE."'>Get Price</a> > ".$brand_name." ".$product_name." ".$sVariant;


	$config_details = get_config_details();
	$strXML = "<?xml version='1.0' encoding='iso-8859-1'?>";
	$strXML .= "<XML>";
        $strXML .= "<BREAD_CRUMB><![CDATA[$breadcrumb]]></BREAD_CRUMB>";
	$strXML .= "<MSG><![CDATA[$msg]]></MSG>";
	$strXML .= "<STARTLIMIT><![CDATA[$startlimit]]></STARTLIMIT>";
	$strXML .= "<CNT><![CDATA[$limitcnt]]></CNT>";
	$strXML .= "<SEO_WEB_URL><![CDATA[".SEO_WEB_URL."]]></SEO_WEB_URL>";
	$strXML .= "<SEO_TITLE><![CDATA[$seo_title]]></SEO_TITLE>";
	$strXML .= "<SEO_TAGS><![CDATA[$seo_keywords]]></SEO_TAGS>";
	$strXML .= "<SEO_DESC><![CDATA[$seo_desc]]></SEO_DESC>";
	$strXML .= "<SEO_PHOTO_TAB_URL><![CDATA[$seo_photo_tab_url]]></SEO_PHOTO_TAB_URL>";
	$strXML .= "<SEO_VIDEO_TAB_URL><![CDATA[$seo_video_tab_url]]></SEO_VIDEO_TAB_URL>";
	$strXML .= "<BRAND_ID><![CDATA[$selected_brand_id]]></BRAND_ID>";
	$strXML .= "<PRODUCT_NAME_ID><![CDATA[$selected_product_name_id]]></PRODUCT_NAME_ID>";
	$strXML .= "<MODEL_BRAND_NAME><![CDATA[$selected_brand_name]]></MODEL_BRAND_NAME>";
        $strXML .= "<MODEL_NAME><![CDATA[$selected_model_name]]></MODEL_NAME>";
	$strXML .= $config_details;
	$strXML .= $sProductDetXml;
	$strXML .= $sProductPriceXml;
	$strXML .= $sSimilarProductDetXml;
	$strXML .= $sSeoUrlDetXml;
	$strXML .= $sTopCopmetitorsListing;
	$strXML .= $rev;
	$strXML .= "<CITY_NAME><![CDATA[".$city_name."]]></CITY_NAME>";
	$strXML .= "<SELECTED_CITY_ID><![CDATA[".$iSelCity."]]></SELECTED_CITY_ID>";
	$strXML .= "<TECH_SPEC_URL><![CDATA[".$tech_spec_url."]]></TECH_SPEC_URL>";
	$strXML .= "<FEATURES_URL><![CDATA[".$features_url."]]></FEATURES_URL>";
	$strXML .= "<REVIEW_SEO_URL><![CDATA[".$reviewseourl."]]></REVIEW_SEO_URL>";
	$strXML .= "<OC_ROS_BOTTOM_NORTH_728x90><![CDATA[OC_ROS_Bottom_North_728x90]]></OC_ROS_BOTTOM_NORTH_728x90>";
	$strXML .= "<OC_ROS_TOP_RHS_LREC_300x250_1><![CDATA[OC_ROS_Top_RHS_Lrec_300x250_1]]></OC_ROS_TOP_RHS_LREC_300x250_1>";
	$strXML .= "<OC_ROS_MIDDLE_HB_468X60><![CDATA[OC_ROS_Middle_HB_468x60]]></OC_ROS_MIDDLE_HB_468X60>";
	$most_recent_one_result_list = $videoGallery->getarrMostRecentVideosHeaderTabLink($category_id,"0","1",$array_result);
	$strXML.="<MOST_RECENT_VIDEO_HEADER_LINK><![CDATA[".$most_recent_one_result_list."]]></MOST_RECENT_VIDEO_HEADER_LINK>";
	$strXML .= "</XML>";
	$strXML = mb_convert_encoding($strXML, "UTF-8");
	if($_GET['debug']==1) { header('Content-type: text/xml');echo $strXML;exit; }
	$doc = new DOMDocument();
	$doc->loadXML($strXML);
	$doc->saveXML();

	$xslt = new xsltProcessor;
	$xsl = DOMDocument::load('xsl/car_info_details.xsl');

	$xslt->importStylesheet($xsl);
	print $xslt->transformToXML($doc);
?>
