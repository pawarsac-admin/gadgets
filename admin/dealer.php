<?php
require_once("../include/config.php");
require_once(CLASSPATH.'DbConn.php');
require_once(CLASSPATH."category.class.php");
require_once(CLASSPATH."brand.class.php");
require_once(CLASSPATH."citystate.class.php");
////require_once(CLASSPATH."product.class.php");
require_once(CLASSPATH."dealer.class.php");

$dbconn 	= new DbConn;
$oCategory	= new CategoryManagement;
$oBrand		= new BrandManagement;
///$oProduct	= new ProductManagement;
$oCityState = new citystate;
$oDealer	= new Dealer;


$actiontype = $_REQUEST['actiontype'];
$startlimit = $_REQUEST['startlimit'];
$limitcnt = $_REQUEST['cnt'];
///if($_POST){ print_r($_REQUEST); } die();
$dealer_id=$_REQUEST['dealer_id'];
unset($request_param);
$category_id = $_REQUEST['selected_category_id'] ? $_REQUEST['selected_category_id'] : $_REQUEST['catid'];
$category_id = ($category_id != "") ? $category_id : SITE_CATEGORY_ID;
if($actiontype == 'insert' || $actiontype== 'Update'){
	$location = trim($_REQUEST['location']);
	if(!empty($location)){
		$request_param['location'] = htmlentities($location,ENT_QUOTES,'UTF-8');
	}
	$dealer_name = trim($_REQUEST['dealer_name']);
	if(!empty($dealer_name)){ $request_param['dealer_name'] = htmlentities($dealer_name,ENT_QUOTES,'UTF-8');}

	$dealer_address = trim($_REQUEST['dealer_address']);
	if(!empty($dealer_address)){ $request_param['dealer_address'] = htmlentities($dealer_address,ENT_QUOTES,'UTF-8');}
	
	$mobile_address = trim($_REQUEST['mobile_address']);
	if(!empty($mobile_address)){ $request_param['mobile_address'] = htmlentities($mobile_address,ENT_QUOTES,'UTF-8');}

	$country_id = trim($_REQUEST['select_country_id']);
	if(!empty($country_id)){ $request_param['country_id']=$country_id;}
	
	$state_id = trim($_REQUEST['select_state_id']);
	if(!empty($state_id)){ $request_param['state_id']=$state_id;}

	$city_id = trim($_REQUEST['city_id']);
	if(!empty($city_id)){ $request_param['city_id']=$city_id;}

	$pin_code = trim($_REQUEST['pin_code']);
	if(!empty($pin_code)){ $request_param['pin_code']=$pin_code;}

	$contact_number = trim($_REQUEST['contact_number']);
	if(!empty($contact_number)){ $request_param['contactno']=$contact_number;}

	$fax_number = trim($_REQUEST['fax_number']);
	if(!empty($fax_number)){ $request_param['fax_number']=$fax_number;}

	$email_address = trim($_REQUEST['email_address']);
	if(!empty($email_address)){ $request_param['email']=$email_address;}

	$contact_person = trim($_REQUEST['contact_person']);
	if(!empty($contact_person)){ $request_param['contact_person']=$contact_person;}

	$website = trim($_REQUEST['website']);
	if(!empty($website)){ $request_param['website']=$website;}

	$discount_offer = trim($_REQUEST['discount_offer']);
	if(!empty($discount_offer)){ $request_param['discount_offer']=$discount_offer;}

	$status = trim($_REQUEST['dealer_status']);
	if($status!=''){ $request_param['status']=$status;}

	$dealer_id = $_REQUEST['dealer_id'] ? $_REQUEST['dealer_id'] : $dealer_id ;
	if(!empty($dealer_id)){ $request_param['dealer_id']=$dealer_id;}

	$dealer_id=$oDealer->addUpdDealerDetails($request_param,"DEALER");
	unset($request_param);
	$dealer_id = $_REQUEST['dealer_id'] ? $_REQUEST['dealer_id'] : $dealer_id ;
	if(!empty($dealer_id)){ $request_param['dealer_id']=$dealer_id;}

	$dealer_product_id = trim($_REQUEST['dealer_product_id']);
	if(!empty($dealer_product_id)){ $request_param['dealer_product_id']=$dealer_product_id;}

	$brand_id = trim($_REQUEST['select_brand_id']);
	if(!empty($brand_id)){ $request_param['brand_id']=$brand_id;}

	$category_id = trim($category_id);
	if(!empty($category_id)){ $request_param['category_id']=$category_id;}

	$iResId=$oDealer->addUpdDealerDetails($request_param,"DEALER_PRODUCT");

}
if($actiontype=='Delete' && !empty($dealer_id)){
		$dresult = $oDealer->boolDeleteDealer($dealer_id);
		$msg = 'Dealer Data deleted successfully.';
}
$config_details = get_config_details();
$strXML = "<XML>";
$strXML .= "<MSG><![CDATA[$msg]]></MSG>";
$strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$category_id]]></SELECTED_CATEGORY_ID>";
$strXML .= "<STARTLIMIT><![CDATA[$startlimit]]></STARTLIMIT>";
$strXML .= "<CNT><![CDATA[$limitcnt]]></CNT>";
$strXML .= $config_details;
$strXML .= $xml;
$strXML .= "</XML>";

//header('Content-type: text/xml');echo $strXML;exit;

$doc = new DOMDocument();
$doc->loadXML($strXML);
$doc->saveXML();

$xslt = new xsltProcessor;
$xsl = DOMDocument::load('xsl/dealer.xsl');

$xslt->importStylesheet($xsl);
print $xslt->transformToXML($doc);
?>
