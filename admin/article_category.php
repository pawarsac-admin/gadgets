<?php
        require_once('../include/config.php');
        require_once(CLASSPATH.'DbConn.php');
	require_once(CLASSPATH.'article.class.php');
	
        $dbconn = new DbConn;
	$article = new article; 
	
	//////if($_POST){ print_r($_REQUEST);} die();
	$actiontype = $_REQUEST['actiontype'];
	$startlimit = $_REQUEST['startlimit'];
	$limitcnt = $_REQUEST['cnt'];
	$request_param=$_REQUEST;
	$category_id = $_REQUEST['selected_category_id'];
	$category_id = ($category_id != "") ? $category_id : SITE_CATEGORY_ID;
	$selected_article_type_id = $_REQUEST["select_section_id"] ? $_REQUEST['select_section_id'] : $_REQUEST['hd_view_section_id'];

	if($actiontype == 'Insert'|| $actiontype== 'Update'){
		unset($request_param);
		$article_sub_type_arr = Array();
		$article_sub_type_arr = $_REQUEST["select_article_sub_type_id"];
		$cnt = sizeof($article_sub_type_arr);
		for($i=0;$i<$cnt;$i++){	
			$article_sub_type_id = $article_sub_type_arr[$i];
			$hd_view_section_id = $_REQUEST["hd_view_section_id"];
			$select_article_id =  $_REQUEST["select_article_id"];
			$select_section_id = $_REQUEST["select_section_id"] ? $_REQUEST["select_section_id"] : $hd_view_section_id;
			$status = $_REQUEST["status"];
			$request_param['article_type_id'] = $select_section_id;
			$request_param['article_sub_type_id'] = $article_sub_type_id;
			$request_param['category_id'] = $category_id;
			$request_param['article_id'] = $select_article_id;
			$request_param['status'] = $status;
		        //print"<pre>";print_r($request_param);print"</pre>";
			if($actiontype == 'Insert'){
			        $result = $article->intInsertArticleCategoryBase($request_param);
                        }elseif($actiontype == 'Update'){
                        	if($i == 0){
                                        $id = $_REQUEST['hd_id'];
                                        //print"<pre>";print_r($request_param);print"</pre>";
                                        $result = $article->boolUpdateArticleCategoryBase($id,$request_param);
                                }else{
                                	$result = $article->intInsertArticleCategoryBase($request_param);
                                }
			}	
		}
		if($sresult>0){
                        if($actiontype == 'Insert'){
                                $msg = 'article category added successfully.';
                        }else{
                                $msg = 'article category updated successfully.';
                        }
                }
	}

	if($actiontype == 'Delete'){
		$id = $_REQUEST["hd_id"];
        	if($id!=''){
                	$result = $article->booldeleteArticleCategory($id,"ARTICLE_CATEGORYBASE");
	                $msg = 'article category deleted successfully.';
        	}
	}

	$config_details = get_config_details();

        $strXML = "<XML>";
        $strXML .= "<MSG><![CDATA[$msg]]></MSG>";
        $strXML .= "<SELECTED_MENU_ID><![CDATA[$menu_level]]></SELECTED_MENU_ID>";
        $strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$category_id]]></SELECTED_CATEGORY_ID>";
	$strXML .= "<SELECTED_ACTION_TYPE><![CDATA[$actiontype]]></SELECTED_ACTION_TYPE>";
	$strXML .= "<VIEWSECTION><![CDATA[$selected_article_type_id]]></VIEWSECTION>";
        $strXML .= "<STARTLIMIT><![CDATA[$startlimit]]></STARTLIMIT>";
        $strXML .= "<CNT><![CDATA[$limitcnt]]></CNT>";
        $strXML .= $config_details;
        $strXML .= $xml;
        $strXML .= "</XML>";

	if($_GET['debug']==1) { header('Content-type: text/xml');echo $strXML;exit; }

        $doc = new DOMDocument();
        $doc->loadXML($strXML);
        $doc->saveXML();

        $xslt = new xsltProcessor;
        $xsl = DOMDocument::load('xsl/article_category.xsl');

        $xslt->importStylesheet($xsl);
        print $xslt->transformToXML($doc);

?>

