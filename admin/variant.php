<?php
	require_once('../include/config.php');
	require_once(CLASSPATH.'DbConn.php');
	require_once(CLASSPATH.'price.class.php');
	$dbconn = new DbConn;
	$oPrice = new price;
	$variant_level = $_REQUEST['selected_category_id'];
	$variant = $_REQUEST['variant'];
	$status = $_REQUEST['status'];
	$variant_id = $_REQUEST['variant_id'];
	$actiontype = $_REQUEST['actiontype'];
	$startlimit = $_REQUEST['startlimit'];
    $limitcnt = $_REQUEST['cnt'];

	$actiontype	=$_REQUEST['actiontype'] ? $_REQUEST['actiontype'] :"Insert";
	
	if(!empty($variant)){
		$request_param['variant'] = htmlentities($variant,ENT_QUOTES);
	}
	if($status != ''){
		$request_param['status'] = $status;
	}
	if($variant_level != ''){
		$request_param['category_id'] = $variant_level;
	}
	
	if($actiontype == 'Insert'){
		$result = $oPrice->intInsertUpdateVariantDetail($request_param);
		$msg ='variant added successfully.';
	}
	else if($actiontype=="Update"){
		if($variant_id != ''){
			$request_param['variant_id'] = $variant_id;
		}
		$result = $oPrice->intInsertUpdateVariantDetail($request_param);
		$msg ='variant updated successfully.';
	}
	else if($actiontype=="Delete"){
		$result = $oPrice->boolDeleteVariantDetail($variant_id);
	    $msg = 'variant deleted successfully.';
	}
	
	$config_details = get_config_details();
	$strXML = "<XML>";
	$strXML .= "<MSG><![CDATA[$msg]]></MSG>";
	$strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$variant_level]]></SELECTED_CATEGORY_ID>";
	$strXML .= "<SELECTED_ACTION_TYPE><![CDATA[$actiontype]]></SELECTED_ACTION_TYPE>";
	$strXML .= "<STARTLIMIT><![CDATA[$startlimit]]></STARTLIMIT>";
	$strXML .= "<CNT><![CDATA[$limitcnt]]></CNT>";
	$strXML .= $config_details;
	$strXML .= $xml;
	$strXML .= "</XML>";
	
	$strXML = mb_convert_encoding($strXML, "UTF-8");
	if($_GET['debug']==1) { header('Content-type: text/xml');echo $strXML;exit; }
	
	$doc = new DOMDocument();
	$doc->loadXML($strXML);
	$doc->saveXML();
	
	$xslt = new xsltProcessor;
	$xsl = DOMDocument::load('xsl/variant.xsl');

	$xslt->importStylesheet($xsl);
	print $xslt->transformToXML($doc);
?>