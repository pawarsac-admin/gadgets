<?php
require_once('../include/config.php');
require_once(CLASSPATH.'DbConn.php');
require_once(CLASSPATH.'user_feedback.class.php');

$dbconn = new DbConn;
$oFeedbackuser = new FEEDBACKUSER;

$actiontype = $_REQUEST['actiontype'] ? $_REQUEST['actiontype'] : "Insert";
$startlimit = $_REQUEST['startlimit'];
$limitcnt = $_REQUEST['cnt'];
$category_id = $_REQUEST['selected_category_id'];

//print "<pre>"; print_r($_REQUEST); 
//die();

if($actiontype== 'Update'){
	unset($request_param);

	$feedback_id = $_REQUEST['feedback_id'];
	if($feedback_id!=''){ $request_param['feedback_id'] = $feedback_id;}
	$status = $_REQUEST['status'];
        if($status!=''){ $request_param['status'] = $status;}
			
	$result = $oFeedbackuser->intInsertFeedback($request_param);
			
	if($sresult>0){
                $msg = 'Feedback updated successfully.';
        }	
}

if($actiontype == 'Delete'){
        $feedback_id = $_REQUEST['feedback_id'];
        if($feedback_id!=''){
                $result = $oFeedbackuser->booldeleteFeedback($feedback_id);
                $msg = 'Feedback deleted successfully.';
        }
}

$config_details = get_config_details();
$strXML = "<XML>";
$strXML .= "<MSG><![CDATA[$msg]]></MSG>";
$strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$category_id]]></SELECTED_CATEGORY_ID>";
$strXML .= "<STARTLIMIT><![CDATA[$startlimit]]></STARTLIMIT>";
$strXML .= "<SELECTED_ACTION_TYPE><![CDATA[$actiontype]]></SELECTED_ACTION_TYPE>";
$strXML .= "<CNT><![CDATA[$limitcnt]]></CNT>";
$strXML .= $config_details;
$strXML .= $xml;
$strXML .= "</XML>";

$strXML = mb_convert_encoding($strXML, "UTF-8");
if($_GET['debug']==1) { header('Content-type: text/xml');echo $strXML;exit; }

$doc = new DOMDocument();
$doc->loadXML($strXML);
$doc->saveXML();

$xslt = new xsltProcessor;
$xsl = DOMDocument::load('xsl/add_feedback.xsl');

$xslt->importStylesheet($xsl);
print $xslt->transformToXML($doc);
?>
