<?php
require_once('../include/config.php');
require_once(CLASSPATH.'DbConn.php');
require_once(CLASSPATH.'article.class.php');

$dbconn = new DbConn;
$oArticle = new article;

//if($_POST){ print_r($_REQUEST);} //die();
$actiontype = $_REQUEST['actiontype'] ? $_REQUEST['actiontype'] : "Insert";
$startlimit = $_REQUEST['startlimit'];
$limitcnt = $_REQUEST['cnt'];
$request_param=$_REQUEST;
$aNewsSection=array("1"=>"upcoming","2"=>"scoops","3"=>"concepts");
$view_section_id=$_REQUEST['view_section_id'] ? $_REQUEST['view_section_id'] :"1"; 
$section_id=$_REQUEST['section_article_id'] ? $_REQUEST['section_article_id'] :"1";
$iAId=$_REQUEST['section_article_id'];
$category_id=$_REQUEST['selected_category_id'];
$category_id = ($category_id != "") ? $category_id : SITE_CATEGORY_ID;

if($_POST['select_article_id']!='' && strlen($_POST['select_article_id'])>0){
	if($status != ''){$request_param['status'] = $status;}
	if($category_id!=''){
		$request_param['category_id'] = $category_id;
	}
	if($_REQUEST['select_section_id']!=''){
		if($_REQUEST['select_section_id']=='upcoming'){
		   $article_type = 1 ;
		}else if($_REQUEST['select_section_id']=='scoops'){
		   $article_type = 2 ;
		}else if($_REQUEST['select_section_id']=='concepts'){
		   $article_type = 3 ;
		}
	}
$aParameters = array("article_id"=>addslashes($request_param['select_article_id']),"category_id"=>$_REQUEST['selected_category_id'],"status"=>$_REQUEST['status'],"position"=>"0","article_type"=>$article_type);
#print "<pre>";print_r($aParameters);
	//$iAId==0 ? $aParameters['cdate']=date("Y-m-d H:i:s") : $aParameters['udate']=date("Y-m-d H:i:s"); 
	if($iAId>0) $aParameters['section_article_id']=$iAId;
	if($_REQUEST['select_section_id']!=""){
		 $aTableName=$_REQUEST['select_section_id'];
		//echo "TEST";
		$iResId=$oArticle->addUpdNewsDetails($aParameters,"CATEGORY_NEWS");
	}
	
	if($iAId==0) {
		$iAId=$iResId;
		$msg="News detail added successfully.";
	}else {
		$msg="News detail updated successfully.";
	}
}
$hd_view_section_id=$_REQUEST['hd_view_section_id'];
//echo $actiontype."-----".$hd_view_section_id."===".$iAId;
if($actiontype == 'Delete' && $hd_view_section_id!=''){
	if($hd_view_section_id!=''){$tablename=$hd_view_section_id;}
	$result = $oArticle->deleteCategoryNews($iAId,"CATEGORY_NEWS");
	$msg = 'News deleted successfully.';
}
$startlimit = $_REQUEST['startlimit'];
$limitcnt = $_REQUEST['cnt'];

$config_details = get_config_details();

$strXML = "<XML>";
$strXML .= "<MSG><![CDATA[$msg]]></MSG>";
$strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$category_id]]></SELECTED_CATEGORY_ID>";
$strXML .= "<STARTLIMIT><![CDATA[$startlimit]]></STARTLIMIT>";
$strXML .= "<CNT><![CDATA[$limitcnt]]></CNT>";
$strXML .= "<SELECTED_ACTION_TYPE><![CDATA[$actiontype]]></SELECTED_ACTION_TYPE>"; 
$strXML .= $config_details;
$strXML .= $xml;
$strXML .= "</XML>";

//header('Content-type: text/xml');echo $strXML;exit;
$doc = new DOMDocument();
$doc->loadXML($strXML);
$doc->saveXML();

$xslt = new xsltProcessor;
$xsl = DOMDocument::load('xsl/related_cat_news.xsl');

$xslt->importStylesheet($xsl);
print $xslt->transformToXML($doc);
?>
