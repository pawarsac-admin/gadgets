<?php
	require_once('../include/config.php');
	require_once(CLASSPATH.'DbConn.php');
	require_once(CLASSPATH.'user.class.php');
	require_once(CLASSPATH.'category.class.php');
	require_once(CLASSPATH.'product.class.php');
	require_once(CLASSPATH.'brand.class.php');
	require_once(CLASSPATH.'citystate.class.php');
	require_once(CLASSPATH.'pager.class.php');
	
	$dbconn = new DbConn;
	$oUser = new user;
	$category = new CategoryManagement;
	$product = new ProductManagement;
	$brand = new BrandManagement;
	$citystate = new citystate;
	$oPager = new Pager;
	
	//print "<pre>"; print_r($_REQUEST);
	$startlimit = $_REQUEST['startlimit'];
	$limitcnt = $_REQUEST['cnt'];	
	$category_id = $_REQUEST['selected_category_id'] ? $_REQUEST['selected_category_id'] : SITE_CATEGORY_ID;

	$selected_status = $_REQUEST['user_stat'] ;
	$selected_status = ($selected_status!='') ? $selected_status : '1';

	$category_id = $_REQUEST['catid'] ? $_REQUEST['catid'] : SITE_CATEGORY_ID;
        $user_profile_id = $_REQUEST['user_profile_id'];
	$user_profile_id = ($user_profile_id!='') ? $user_profile_id : $_REQUEST['hd_user_profile_id'];
	$selected_brand_id = $_REQUEST['select_brand_id'] ;
	$selected_model_id = $_REQUEST['Model'] ;
	$selected_variant_id = $_REQUEST['Variant'] ;
    $action = $_REQUEST['action'];
	$select_city_id = $_REQUEST['select_city_id'] ;
	


	switch ($action){
		case "updateadminreply":
			$update_param["user_profile_id"]=$user_profile_id;
	                $update_param["admin_reply"]=1;
        	        $update_param["category_id"]=$category_id;
                	$result = $oUser->intUpdateUserDetail($user_profile_id,$update_param);
			break;
		case "updateuserstatus":
			$update_param["user_profile_id"]=$user_profile_id;
                        $update_param["status"]=$_REQUEST['hd_update_status'];
                        $result = $oUser->intUpdateUserDetail($user_profile_id,$update_param);
                        break;
		default:
	}
	//echo $selected_brand_id."TEST";
	$iUserInfoCount = $oUser->getUserInfoCount("",$category_id,$selected_brand_id,$selected_model_id,$selected_variant_id,$select_city_id,"",$selected_status,"","");
	if($iUserInfoCount != 0){
                $page = $_REQUEST['page'] ? $_REQUEST['page'] : 1;
                $perpage = 30;
                $start  = $oPager->findStart($perpage);
                $recordcount = $iUserInfoCount;
                $sExtraParam = "user_info.php,DivUserInfo,$category_id,$selected_status,$selected_brand_id,$selected_model_id,$selected_variant_id,$select_city_id";
                $jsparams = $start.",".$perpage.",".$sExtraParam;
                $pages = $oPager->findPages($recordcount,$perpage);
                if($pages > 1 ){
                        $pagelist = $oPager->jsPageNumNextPrev($page,$pages,"sArticlePagination",$jsparams,"text");
                        $nodesPaging .= "<Pages><![CDATA[".$pagelist."]]></Pages>";
                        $nodesPaging .= "<Page><![CDATA[".$page."]]></Page>";
                        $nodesPaging .= "<Perpage><![CDATA[".$perpage."]]></Perpage>";
                }
		$result = $oUser->arrGetUserInfoDetails("",$category_id,$selected_brand_id,$selected_model_id,$selected_variant_id,$select_city_id,"",$selected_status,$start,$perpage);
	}

	//print"<pre>";print_r($result);print"</pre>";

	$cnt = sizeof($result);
	$xml = "<USER_INFO_MASTER>";
	$xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
	for($i=0;$i<$cnt;$i++){
		$result[$i]['user_profile_id'] = $result[$i]['user_profile_id'];
		$status = $result[$i]['status'];
                $categoryid = $result[$i]['category_id'];
                if(!empty($categoryid)){ 
                        $category_result = $category->arrGetCategoryDetails($categoryid,"","","","");
                }
                $category_name = $category_result[0]['category_name'];
                $result[$i]['js_category_name'] = $category_name;
                $result[$i]['category_name'] = html_entity_decode($category_name,ENT_QUOTES);
                //$result[$i]['status'] = ($status == 1) ? 'Active' : 'InActive';
                $result[$i]['create_date'] = date('d-m-Y',strtotime($result[$i]['create_date']));
                        
                $brand_id = $result[$i]['brand_id'];
                $result[$i]['brand_id'] = $brand_id;
                if(!empty($brand_id)){
                        $brand_result = $brand->arrGetBrandDetails($brand_id,$category_id,"","","");
                }
                $brand_name = $brand_result[0]['brand_name'];
                $result[$i]['js_brand_name'] = $brand_name;
                $result[$i]['brand_name'] = html_entity_decode($brand_name,ENT_QUOTES);
                if($result[$i]['product_id']!=''){
                        $product_id = $result[$i]['product_id'];
                        $result[$i]['product_id'] = $product_id;
                        if(!empty($product_id)){
                                $product_result = $product->arrGetProductDetails($product_id,$category_id,"","","","","","","","","");
                        }
                        $product_name = $product_result[0]['product_name'];
			//$brand_name.=" ".$product_name;
                        $product_variant = $product_result[0]['variant'];
                        $result[$i]['js_product_name'] = $product_name;
                        $result[$i]['product_name'] = html_entity_decode($product_name,ENT_QUOTES);
                        $result[$i]['product_variant'] = html_entity_decode($product_variant,ENT_QUOTES);
                }
		if($result[$i]['product_info_id']!=''){
                        $product_info_id = $result[$i]['product_info_id'];
                        $result[$i]['product_info_id'] = $product_info_id;
                        if(!empty($product_info_id)){
                                $product_info_result = $product->arrGetProductNameInfo($product_info_id,$category_id,"","","","","");
                                $product_info_name = $product_info_result[0]['product_info_name'];
                        }
			//$brand_name.=" ".$product_info_name;
                        $result[$i]['js_product_name'] = $product_info_name;
                        $result[$i]['product_name'] = html_entity_decode($product_name,ENT_QUOTES);
                }
		$brand_name = $brand_name." ".$product_name." ".$product_variant;
		$result[$i]['brand_model_variant'] = $brand_name;
		$city_id=$result[$i]['city_id'];
		$city_name = "";
		if(!empty($city_id)){
			$city_res = $citystate->arrGetCityDetails($city_id,"","","","");
			$city_name = $city_res[0]["city_name"];
		}
		$result[$i]['city_id'] = $result[$i]['city_id'];
		$result[$i]['city_name'] = $city_name;
		$result[$i]['profile_name'] = $result[$i]['profile_name'];
		$result[$i]['profile_name'] = html_entity_decode($result[$i]['profile_name'],ENT_QUOTES);
		$result[$i]['email'] = $result[$i]['email'];
		$result[$i]['mobile'] = $result[$i]['mobile'];
		$result[$i]['std_code'] = $result[$i]['std_code'];
		$std_code = $result[$i]['std_code'];
		$contact_no = $result[$i]['contact_no'];
		if($contact_no != ""){
			$result[$i]['contact_no'] = $std_code."-".$contact_no;
		}
		$result[$i]['contact_no'] = $result[$i]['contact_no'];
		$result[$i]['period'] = $result[$i]['period'];
		$result[$i]['status'] = $result[$i]['status'];
		if(!empty($result[$i]['email'])){$email = $result[$i]['email'];}


		$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
		$xml .= "<USER_INFO_MASTER_DATA>";
		foreach($result[$i] as $k=>$v){
			$xml .= "<$k><![CDATA[$v]]></$k>";
		}
		$xml .= "</USER_INFO_MASTER_DATA>";
		$product_info_name=""; $product_variant=""; $product_name="";
		unset($brand_result);
	        unset($product_result);
        	unset($product_info_result);
	}
	$xml .= "</USER_INFO_MASTER>";

	unset($result);
	if(!empty($category_id)){
        	$result = $brand->arrGetBrandDetails("",$category_id,"","","");
	}
	$cnt = sizeof($result);
	$xml .= "<BRAND_MASTER>";
	$xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
	for($i=0;$i<$cnt;$i++){
        	$status = $result[$i]['status'];
	        $categoryid = $result[$i]['category_id'];
        	if(!empty($categoryid)){ 
                	$category_result = $category->arrGetCategoryDetails($categoryid,"","","","");
	        }
        	$category_name = $category_result[0]['category_name'];
	        $result[$i]['js_category_name'] = $category_name;
        	$result[$i]['category_name'] = html_entity_decode($category_name,ENT_QUOTES);
	        $result[$i]['brand_status'] = ($status == 1) ? 'Active' : 'InActive';
        	$result[$i]['create_date'] = date('d-m-Y',strtotime($result[$i]['create_date']));
	        $result[$i]['js_brand_name'] = $result[$i]['brand_name'];
        	$result[$i]['brand_name'] = html_entity_decode($result[$i]['brand_name'],ENT_QUOTES);
	        $result[$i] = array_change_key_case($result[$i],CASE_UPPER);
        	$xml .= "<BRAND_MASTER_DATA>";
	        foreach($result[$i] as $k=>$v){
        	        $xml .= "<$k><![CDATA[$v]]></$k>";
	        }
        	$xml .= "</BRAND_MASTER_DATA>";
	}
	$xml .= "</BRAND_MASTER>";

	unset($result);
	if(!empty($category_id)){
        	$result = $citystate->arrGetCityDetails();
	}
	//print_r($result);
	$cnt = sizeof($result);
	$xml .= "<CITY_MASTER>";
	$xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
	for($i=0;$i<$cnt;$i++){
        	$status = $result[$i]['status'];
	        $categoryid = $result[$i]['category_id'];
        	if(!empty($categoryid)){ 
                	$category_result = $category->arrGetCategoryDetails($categoryid,"","","","");
	        }
        	$category_name = $category_result[0]['category_name'];
	        $result[$i]['js_category_name'] = $category_name;
        	$result[$i]['category_name'] = html_entity_decode($category_name,ENT_QUOTES);
	        $result[$i]['status'] = ($status == 1) ? 'Active' : 'InActive';
        	$result[$i]['create_date'] = date('d-m-Y',strtotime($result[$i]['create_date']));
	        $result[$i]['js_city_name'] = $result[$i]['city_name'];
        	$result[$i]['city_name'] = html_entity_decode($result[$i]['city_name'],ENT_QUOTES);
	        $result[$i] = array_change_key_case($result[$i],CASE_UPPER);
        	$xml .= "<CITY_MASTER_DATA>";
	        foreach($result[$i] as $k=>$v){
        	        $xml .= "<$k><![CDATA[$v]]></$k>";
	        }
        	$xml .= "</CITY_MASTER_DATA>";
	}
	$xml .= "</CITY_MASTER>";
	

	$config_details = get_config_details();
	$strXML = "<XML>";
	$strXML .= "<MSG><![CDATA[$msg]]></MSG>";
	$strXML .= "<STARTLIMIT><![CDATA[$startlimit]]></STARTLIMIT>";
	$strXML .= "<CNT><![CDATA[$limitcnt]]></CNT>";
	$strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$category_id]]></SELECTED_CATEGORY_ID>";
	$strXML .= "<SELECTED_STATUS><![CDATA[$selected_status]]></SELECTED_STATUS>";
	$strXML .= "<SELECTED_BRAND_ID><![CDATA[$selected_brand_id]]></SELECTED_BRAND_ID>";
	$strXML .= "<SELECTED_MODEL_ID><![CDATA[$selected_model_id]]></SELECTED_MODEL_ID>";
	$strXML .= "<SELECTED_VARIANT_ID><![CDATA[$selected_variant_id]]></SELECTED_VARIANT_ID>";
	$strXML .= "<SELECTED_CITY_ID><![CDATA[$select_city_id]]></SELECTED_CITY_ID>";
	
	$strXML .= $config_details;
	$strXML .= $xml;
	$strXML .= $sStateXml;
	$strXML .= $nodesPaging;
	$strXML .= "</XML>";

	$strXML = mb_convert_encoding($strXML, "UTF-8");
	if($_GET['debug']==1) { header('Content-type: text/xml');echo $strXML;exit; }

	$doc = new DOMDocument();
	$doc->loadXML($strXML);
	$doc->saveXML();

	$xslt = new xsltProcessor;
	$xsl = DOMDocument::load('xsl/user_info.xsl');

	$xslt->importStylesheet($xsl);
	print $xslt->transformToXML($doc);
?>

