<?php
require_once('../include/config.php');
require_once(CLASSPATH.'DbConn.php');
require_once(CLASSPATH.'product.class.php');

$dbconn = new DbConn;
$oProduct = new ProductManagement;

//if($_POST){ print"<pre>";print_r($_REQUEST);}  //die();
$actiontype = $_REQUEST['actiontype'];
$startlimit = $_REQUEST['startlimit'];
$limitcnt = $_REQUEST['cnt'];

$oncars_compare_id=$_REQUEST['oncars_compare_id'];
if(!empty($oncars_compare_id)){
	$request_param['oncars_compare_id']=$oncars_compare_id;
}
$category_id=$_REQUEST['selected_category_id'];
$category_id = ($category_id != "") ? $category_id : SITE_CATEGORY_ID;
if(!empty($category_id)){
	$request_param['category_id']=$category_id;
}
$title=$_REQUEST['title'];
if($title != ""){
	$request_param['title'] = htmlentities($title,ENT_QUOTES);
}
$description=$_REQUEST['description'];
if($description!=""){
        $request_param['description']=htmlentities($description,ENT_QUOTES);
}
$request_param['position'] = $_REQUEST['position'];
$status=$_REQUEST['status'];
if($status!=""){
	$request_param['status']=$status;
}
$compare_set_img_id=$_REQUEST['compare_set_img_id'];
if($compare_set_img_id!=""){
	$request_param['compare_set_img_id']=$compare_set_img_id;
}
$compare_set_img=$_REQUEST['img_upload'];
if($compare_set_img!=""){
	$request_param['compare_set_image_path']=$compare_set_img;
}

$display_rows = $_REQUEST['display_rows'];
if(($display_rows != "") && ($display_rows > '0')){
	$comp_set = "";
	for($i=1; $i<=$display_rows; $i++){
		if($_REQUEST['select_comp_variant_id_'.$i]!='' && strlen($_REQUEST['select_comp_variant_id_'.$i])>0){
		        $comp_set .=$_REQUEST['select_comp_variant_id_'.$i].",";
		}	
	}
	$comp_set = substr($comp_set, 0, -1);
       	$request_param['compare_set']=$comp_set;
}
if($actiontype=="Insert"){
	if($request_param!=''){
		$iResId=$oProduct->addUpdOncarsCompareSetDetails($request_param);
		$msg="Oncars Compare set detail added successfully.";
	}
}
else if($actiontype=="Update"){
	if($request_param!=''){
		$iResId=$oProduct->addUpdOncarsCompareSetDetails($request_param);
		$msg="Oncars Compare set detail updatded successfully.";
	}

}
else if($actiontype=="Delete"){
	$result = $oProduct->boolDeleteOncarsCompareSetDetail($oncars_compare_id);
	$msg = 'Oncars Compare set Detail deleted successfully.';
}

$config_details = get_config_details();
$strXML = "<?xml version='1.0' encoding='iso-8859-1'?>";
$strXML .= "<XML>";
$strXML .= "<MSG><![CDATA[$msg]]></MSG>";
$strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$category_id]]></SELECTED_CATEGORY_ID>";
$strXML .= "<SELECTED_ACTION_TYPE><![CDATA[$actiontype]]></SELECTED_ACTION_TYPE>";
$strXML .= "<STARTLIMIT><![CDATA[$startlimit]]></STARTLIMIT>";
$strXML .= "<CNT><![CDATA[$limitcnt]]></CNT>";
$strXML .= $config_details;
$strXML .= $xml;
$strXML .= "</XML>";

$strXML = mb_convert_encoding($strXML, "UTF-8");
if($_GET['debug']==1) { header('Content-type: text/xml');echo $strXML;exit; }

$doc = new DOMDocument();
$doc->loadXML($strXML);
$doc->saveXML();

$xslt = new xsltProcessor;
$xsl = DOMDocument::load('xsl/oncars_comparison.xsl');

$xslt->importStylesheet($xsl);
print $xslt->transformToXML($doc);
?>
