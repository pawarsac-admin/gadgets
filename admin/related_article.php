<?php
require_once('../include/config.php');
require_once(CLASSPATH.'DbConn.php');
require_once(CLASSPATH.'article.class.php');

$dbconn = new DbConn;
$oArticle = new article;

//if($_POST){ print_r($_REQUEST);} //die();
$actiontype = $_REQUEST['actiontype'];
$startlimit = $_REQUEST['startlimit'];
$limitcnt = $_REQUEST['cnt'];
$request_param=$_REQUEST;
$_REQUEST['product_id'] !=0 ? $_REQUEST['product_id'] :0;
$_REQUEST['select_model_id'] !=0 ? $_REQUEST['select_model_id'] :0;  

$aArticleSection=array("LATEST_ARTICLE"=>"LATEST_ARTICLE","2"=>"FEATURED_ARTICLE","3"=>"RELATED_ARTICLE");
$view_section_id=$_REQUEST['view_section_id'] ? $_REQUEST['view_section_id'] :"LATEST_ARTICLE"; 

$section_id=$_REQUEST['section_article_id'] ? $_REQUEST['section_article_id'] :"1";
$iAId=$_REQUEST['section_article_id'];
if($_POST['select_article_id']!='' && strlen($_POST['select_article_id'])>0){
	
	if($status != ''){$request_param['status'] = $status;}
	$category_id=$_REQUEST['selected_category_id'];
	if($category_id!=''){
		$request_param['category_id'] = $category_id;
	}
	$aParameters = array("article_id"=>addslashes($request_param['select_article_id']),"category_id"=>$request_param['category_id'],"status"=>$_REQUEST['status'],"article_type"=>$_REQUEST['select_aritcle_type_id'],"position"=>"0");
	//$iAId==0 ? $aParameters['cdate']=date("Y-m-d H:i:s") : $aParameters['udate']=date("Y-m-d H:i:s"); 
	if($iAId>0) $aParameters['section_article_id']=$iAId;
	//print "<pre>"; print_r($aParameters);
	if($_REQUEST['select_section_id']!=""){
		$aTableName=$_REQUEST['select_section_id'];
		$iResId=$oArticle->addUpdArticleDetails($aParameters,$aTableName);
	}
	
	if($iAId==0) {
		$iAId=$iResId;
		$msg="Article detail added successfully.";
	}else {
		$msg="Article detail updated successfully.";
	}
}
$hd_view_section_id=$_REQUEST['hd_view_section_id'];
if($actiontype == 'Delete' && $hd_view_section_id!=''){
	if($hd_view_section_id!=''){$tablename=$hd_view_section_id;}
	$result = $oArticle->deleteRelatedArticle($iAId,$tablename);
	$msg = 'Article deleted successfully.';
}
$startlimit = $_REQUEST['startlimit'];
$limitcnt = $_REQUEST['cnt'];

$config_details = get_config_details();

$strXML = "<XML>";
$strXML .= "<MSG><![CDATA[$msg]]></MSG>";
$strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$category_id]]></SELECTED_CATEGORY_ID>";
$strXML .= "<STARTLIMIT><![CDATA[$startlimit]]></STARTLIMIT>";
$strXML .= "<CNT><![CDATA[$limitcnt]]></CNT>";
$strXML .= $config_details;
$strXML .= $xml;
$strXML .= "</XML>";

$strXML = mb_convert_encoding($strXML, "UTF-8");
if($_GET['debug']==1) { header('Content-type: text/xml');echo $strXML;exit; }

$doc = new DOMDocument();
$doc->loadXML($strXML);
$doc->saveXML();

$xslt = new xsltProcessor;
$xsl = DOMDocument::load('xsl/related_article.xsl');

$xslt->importStylesheet($xsl);
print $xslt->transformToXML($doc);
?>