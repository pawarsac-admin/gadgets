<?php
        require_once('../include/config.php');
        require_once(CLASSPATH.'DbConn.php');
	require_once(CLASSPATH.'user.class.php');
	
        $dbconn = new DbConn;
	$obj_user = new user; 
	
	//if($_POST){ print_r($_REQUEST);} //die();

	$actiontype = ($_REQUEST['actiontype'] != "") ? $_REQUEST['actiontype'] : "";
	$startlimit = $_REQUEST['startlimit'];
	$limitcnt = $_REQUEST['cnt'];
	$category_id = $_REQUEST['selected_category_id'];

	if($actiontype == 'Insert'|| $actiontype == 'Update'){
		unset($request_param);
		$editor_id = $_REQUEST["editor_id"];
		$editor_name= $_REQUEST["editor_name"];
		$designation = $_REQUEST["designation"];
		$email_id = $_REQUEST["email_id"];
		$phone_no = $_REQUEST["phone_no"];
		$short_desc = $_REQUEST["short_desc"];
		$long_desc = $_REQUEST["long_desc"];
		$abstract_img_path = $_REQUEST["abstract_img_path"];
		$status = $_REQUEST["status"];

	
		if($editor_id != ""){$request_param['editor_id'] = $editor_id;}
		if($editor_name != ""){$request_param['editor_name'] = htmlentities($editor_name,ENT_QUOTES);}
		$request_param['designation'] = htmlentities($designation,ENT_QUOTES);
		if($email_id != ""){$request_param['email_id'] = $email_id;}
		if($phone_no != ""){$request_param['phone_no'] = $phone_no;}
		$request_param['short_desc'] = htmlentities($short_desc,ENT_QUOTES);
		$request_param['long_desc'] = htmlentities($long_desc,ENT_QUOTES);
		if($abstract_img_path != ""){$request_param['profile_img'] = $abstract_img_path;}
		if($status != ""){$request_param['status'] = $status;}

		if($_FILES["uploadedfile"]["name"] != ""){
			$target_path = BASEPATH."images/";
			$name = $_FILES['uploadedfile']['name'];
			$name = strtolower(str_replace(' ', '_', trim($name)));

			$target_path = $target_path.$name; 
			rename($_FILES['uploadedfile']['tmp_name'], $target_path);
			$request_param['profile_img'] = $name;
		}
		//print"<pre>";print_r($request_param); die();

	        if($actiontype == 'Insert'){
		       $result = $obj_user->intInsertEditorDetails($request_param);
        	       if($sresult>0){$msg = 'Editor added successfully.';}
		}elseif($actiontype == 'Update'){
			$result = $obj_user->boolUpdateEditorDetails($request_param);
			//print"<pre>";print_r($result);
        		if($sresult>0){$msg = 'Editor updated successfully.';}
		}
		
	}


	if($actiontype == 'Delete'){
		$editor_id = $_REQUEST["editor_id"];
        	if($editor_id!=''){
                	$result = $obj_user->booldeleteEditorInfo($editor_id);
	                $msg = 'Editor Details deleted successfully.';
        	}
	}

	$config_details = get_config_details();

        $strXML = "<XML>";
        $strXML .= "<MSG><![CDATA[$msg]]></MSG>";
        $strXML .= "<SELECTED_MENU_ID><![CDATA[$menu_level]]></SELECTED_MENU_ID>";
        $strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$category_id]]></SELECTED_CATEGORY_ID>";
	$strXML .= "<SELECTED_ACTION_TYPE><![CDATA[$actiontype]]></SELECTED_ACTION_TYPE>";
        $strXML .= "<STARTLIMIT><![CDATA[$startlimit]]></STARTLIMIT>";
        $strXML .= "<CNT><![CDATA[$limitcnt]]></CNT>";
        $strXML .= $config_details;
        $strXML .= $xml;
        $strXML .= "</XML>";

	if($_GET['debug']==1) { header('Content-type: text/xml');echo $strXML;exit; }

        $doc = new DOMDocument();
        $doc->loadXML($strXML);
        $doc->saveXML();

        $xslt = new xsltProcessor;
        $xsl = DOMDocument::load('xsl/editorinfo.xsl');

        $xslt->importStylesheet($xsl);
        print $xslt->transformToXML($doc);

?>

