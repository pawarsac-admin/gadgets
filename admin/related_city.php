<?php
        require_once('../include/config.php');
        require_once(CLASSPATH.'DbConn.php');
	require_once(CLASSPATH.'citystate.class.php');
	
        $dbconn = new DbConn;
	$oCityState = new citystate;
	
	//if($_POST){ print_r($_REQUEST);} die();
	$actiontype = $_REQUEST['actiontype'];
	$startlimit = $_REQUEST['startlimit'];
	$limitcnt = $_REQUEST['cnt'];
	$request_param=$_REQUEST;
	$category_id = $_REQUEST['selected_category_id'];
	$category_id = ($category_id != "") ? $category_id : SITE_CATEGORY_ID;
	$selected_city_id = $_REQUEST['select_city_id'] ? $_REQUEST['select_city_id'] : $_REQUEST['selected_city_id'] ;

	if($actiontype == 'Insert' || $actiontype == 'Update'){
                $city_id_arr =Array();
                $city_id_arr = $_REQUEST['select_related_city_id'];
                $cnt = sizeof($city_id_arr);
                if($cnt > 0){
                	for($i=0;$i<$cnt;$i++){
                        	unset($request_param);
				$related_id = $_REQUEST["related_id"];
				$city_id =  $_REQUEST["select_city_id"];
				$related_city_id = $city_id_arr[$i];
                        	if(!empty($city_id)){ $request_param['city_id'] = $city_id;}
                        	if(!empty($related_city_id)){ $request_param['related_city_id'] = $related_city_id;}
                        	$status = $_REQUEST['status'];
	                        if($status!=''){ $request_param['status'] = $status;}
				$request_param['category_id'] = $category_id;
	        		if($actiontype == 'Insert'){
		        		$result = $oCityState->intInsertUpdateRelatedCity($request_param);
				}elseif($actiontype == 'Update'){
					if($i == 0){
						$request_param['related_id'] = $related_id;
						$result = $oCityState->intInsertUpdateRelatedCity($request_param);
					}else{
						$result = $oCityState->intInsertUpdateRelatedCity($request_param);
					}
				}
			}
		
        	        if($sresult>0){
				if($actiontype == 'Insert'){
					$msg = 'related city added successfully.';
				}else{
					$msg = 'related city updated successfully.';
				}
			}
		}
		
	}


	if($actiontype == 'Delete'){
		$related_id = $_REQUEST["related_id"];
        	if($related_id != ''){
                	$result = $oCityState->boolDeleteRelatedCity($related_id);
	                $msg = 'related city deleted successfully.';
        	}
	}

	$config_details = get_config_details();

        $strXML = "<XML>";
        $strXML .= "<MSG><![CDATA[$msg]]></MSG>";
        $strXML .= "<SELECTED_MENU_ID><![CDATA[$menu_level]]></SELECTED_MENU_ID>";
        $strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$category_id]]></SELECTED_CATEGORY_ID>";
	$strXML .= "<SELECTED_ACTION_TYPE><![CDATA[$actiontype]]></SELECTED_ACTION_TYPE>";
	$strXML .= "<SELECTED_CITY_ID><![CDATA[$selected_city_id]]></SELECTED_CITY_ID>";
        $strXML .= "<STARTLIMIT><![CDATA[$startlimit]]></STARTLIMIT>";
        $strXML .= "<CNT><![CDATA[$limitcnt]]></CNT>";
        $strXML .= $config_details;
        $strXML .= $xml;
        $strXML .= "</XML>";

	if($_GET['debug']==1) { header('Content-type: text/xml');echo $strXML;exit; }

        $doc = new DOMDocument();
        $doc->loadXML($strXML);
        $doc->saveXML();

        $xslt = new xsltProcessor;
        $xsl = DOMDocument::load('xsl/related_city.xsl');

        $xslt->importStylesheet($xsl);
        print $xslt->transformToXML($doc);

?>

