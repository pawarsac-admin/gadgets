<?php
require_once('../include/config.php');
require_once(CLASSPATH.'DbConn.php');
require_once(CLASSPATH.'user_review.class.php');
require_once(CLASSPATH.'brand.class.php');
require_once(CLASSPATH.'category.class.php');
require_once(CLASSPATH.'product.class.php');

$dbconn = new DbConn;
$userreview = new USERREVIEW;
$brand = new BrandManagement;
$category = new CategoryManagement;
$product = new ProductManagement;


//print "<pre>"; print_r($_REQUEST);print"<pre>";
//die();
$actiontype = $_REQUEST['submit'];
$user_review_id = $_REQUEST['user_review_id'];
$category_id = $_REQUEST['selected_category_id'];
$commentcnt = $_REQUEST['comment_cnt'];
$title = $_REQUEST['title'];
for($i=1;$i<=$commentcnt;$i++){
        $usr_review_ans_id = $_REQUEST['usr_review_ans_id_'.$i];
        $update_param['answer'] = htmlentities($_REQUEST['comment_'.$i],ENT_QUOTES);
        $isUpdateComment = $userreview->intUpdateUserReviewAnswer($update_param,$usr_review_ans_id);
}
if($actiontype == "Approve"){
        unset($request_param);

        if(!empty($user_review_id)){ $request_param['user_review_id'] = $user_review_id;}
        $request_param['status'] = "1";

        if(!empty($title)){ $request_param['title'] = $title;}

        $review_result_id = $userreview->addUpdUserReviewsDetails($request_param,"USER_REVIEW");

        if($review_result_id>0){
                $msg = 'User Review Approved.';
        }

}elseif($actiontype == "Reject"){
        unset($request_param);

        if(!empty($user_review_id)){ $request_param['user_review_id'] = $user_review_id;}
        $request_param['status'] = "0";

        $review_result_id=$userreview->addUpdUserReviewsDetails($request_param,"USER_REVIEW");

        if($review_result_id>0){
                $msg = 'User Review Rejected.';
        }
}


$config_details = get_config_details();
$strXML = "<XML>";
$strXML .= "<MSG><![CDATA[$msg]]></MSG>";
$strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$category_id]]></SELECTED_CATEGORY_ID>";
$strXML .= "<STARTLIMIT><![CDATA[$startlimit]]></STARTLIMIT>";
$strXML .= "<SELECTED_ACTION_TYPE><![CDATA[$actiontype]]></SELECTED_ACTION_TYPE>";
$strXML .= "<CNT><![CDATA[$limitcnt]]></CNT>";
$strXML .= $config_details;
$strXML .= $xml;
$strXML .= "</XML>";

$strXML = mb_convert_encoding($strXML, "UTF-8");
if($_GET['debug']==1) { header('Content-type: text/xml');echo $strXML;exit; }

$doc = new DOMDocument();
$doc->loadXML($strXML);
$doc->saveXML();

$xslt = new xsltProcessor;
$xsl = DOMDocument::load('xsl/user_review.xsl');

$xslt->importStylesheet($xsl);
print $xslt->transformToXML($doc);
?>

