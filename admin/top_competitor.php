<?php
require_once('../include/config.php');
require_once(CLASSPATH.'DbConn.php');
require_once(CLASSPATH.'product.class.php');

$dbconn = new DbConn;
$oProduct = new ProductManagement;

//if($_POST){ print_r($_REQUEST);}  //die();
$startlimit = $_REQUEST['startlimit'];
$limitcnt = $_REQUEST['cnt'];
$actiontype = $_REQUEST['actiontype'] ? $_REQUEST['actiontype'] : "Insert";
if($actiontype=="Insert" || $actiontype=="Update"){
	if($_REQUEST['display_rows']>0){
		$dataCount=$_REQUEST['display_rows'];
		for($i=1;$i<=$dataCount;$i++){
			unset($request_param);
			$competitor_product_id = "";
			if($i == "1"){
				$competitor_product_id = $_REQUEST['competitor_product_id'];
			}
			if(!empty($competitor_product_id)){
                                $request_param['competitor_product_id']=$competitor_product_id;
                        }
			$category_id=$_REQUEST['selected_category_id'];
			if(!empty($category_id)){
				$request_param['category_id']=$category_id;
			}
			$brand_id=$_REQUEST['select_brand_id'];
			if(!empty($brand_id)){
				$request_param['brand_id']=$brand_id;
			}
			$product_info_id=$_REQUEST['select_model_id']; 
			$request_param['product_info_id']=$product_info_id;
			$product_id=$_REQUEST['product_id']; 
			$request_param['product_id']=$product_id;
			$competitor_brand_id = $_REQUEST['select_comp_brand_id_'.$i];
			$competitor_model_id=$_REQUEST['select_comp_model_id_'.$i];
			$competitor_variant_id=$_REQUEST['select_comp_variant_id_'.$i]; 
                        $request_param['brand_ids']=$competitor_brand_id;
                        $request_param['product_info_ids']=$competitor_model_id;
			$request_param['product_ids']=$competitor_variant_id;
			$status=$_REQUEST['status']; 
			if($status!=""){
				$request_param['status']=$status;
			}
			//print"<pre>";print_r($request_param);print"</pre>";
			if($request_param!=""){
				$iResId=$oProduct->addUpdTopCompetitorDetails($request_param);
				if($actiontype=="Insert"){
					$msg="Top competitor detail added successfully.";
				}else if($actiontype=="Update"){
					$msg="Top competitor detail updated successfully.";
				}
			}	
		}
	}
}else if($actiontype=="Delete"){
	$competitor_product_id = $_REQUEST['competitor_product_id'];
	$result = $oProduct->boolDeleteTopCompetitorDetail($competitor_product_id);
	$msg = 'Top competitor Detail deleted successfully.';
}

$config_details = get_config_details();
$strXML = "<?xml version='1.0' encoding='iso-8859-1'?>";
$strXML .= "<XML>";
$strXML .= "<MSG><![CDATA[$msg]]></MSG>";
$strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$category_id]]></SELECTED_CATEGORY_ID>";
$strXML .= "<SELECTED_ACTION_TYPE><![CDATA[$actiontype]]></SELECTED_ACTION_TYPE>";
$strXML .= "<STARTLIMIT><![CDATA[$startlimit]]></STARTLIMIT>";
$strXML .= "<CNT><![CDATA[$limitcnt]]></CNT>";
$strXML .= $config_details;
$strXML .= $xml;
$strXML .= "</XML>";

$strXML = mb_convert_encoding($strXML, "UTF-8");
if($_GET['debug']==1) { header('Content-type: text/xml');echo $strXML;exit; }

$doc = new DOMDocument();
$doc->loadXML($strXML);
$doc->saveXML();

$xslt = new xsltProcessor;
$xsl = DOMDocument::load('xsl/top_competitor.xsl');

$xslt->importStylesheet($xsl);
print $xslt->transformToXML($doc);
?>
