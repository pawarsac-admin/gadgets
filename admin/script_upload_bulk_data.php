<?php
	require_once('../include/config.php');
	require_once(CLASSPATH.'DbConn.php');
	$dbconn		= new DbConn;

	$action		= $_REQUEST['action'];
	$debug		= $_REQUEST['debug'];

	if(empty($debug)){
		unset($_REQUEST['debug']);
	}

	if($action=='update'){
		require_once('../script_update_search_data.php');
		require_once('../script_generate_autocomplete_js.php');
	    $msg .= 'Search Data Updated successfully.';
	
	} else if( $action=='upload' ) { 

		if(!empty($_FILES["filename"]["name"])){
			if ($_FILES["filename"]["error"] > 0) {
				$msg .= "Return Code: " . $_FILES["filename"]["error"] . "<br />";
			} else 	{
				$msg .= "Upload: " . $_FILES["filename"]["name"] . "<br />";
				$msg .="Type: " . $_FILES["filename"]["type"] . "<br />";
				$msg .="Size: " . ($_FILES["filename"]["size"] / 1024) . " Kb<br />";
				$msg .="Temp file: " . $_FILES["filename"]["tmp_name"] . "<br />";

				if (file_exists("../upload/" . $_FILES["filename"]["name"])){
					$msg .= $_FILES["filename"]["name"] . " already exists. ". "<br />";
				} else {
					move_uploaded_file($_FILES["filename"]["tmp_name"],
					"../upload/" . $_FILES["filename"]["name"]);
					$msg .="Stored in: " . "/upload/" . $_FILES["filename"]["name"]. "<br />";

					// START UPLOADING 
					define('FILE_PATH','../upload/');
					require_once('../script_upload_xls.php');
					require_once('../script_update_search_data.php');
					require_once('../script_generate_autocomplete_js.php');
				   $msg .= 'Data Uploaded successfully.';
				}
			}

		}

	}
	$config_details = get_config_details();
	$strXML = "<XML>";
	$strXML .= "<ACTION><![CDATA[$action]]></ACTION>";
	$strXML .= "<MSG><![CDATA[$msg]]></MSG>";
	$strXML .= $config_details;
	$strXML .= "</XML>";
	//header('Content-type: text/xml');echo $strXML;exit;
	$doc = new DOMDocument();
	$doc->loadXML($strXML);
	$doc->saveXML();
	$xslt = new xsltProcessor;
	$xsl = DOMDocument::load('xsl/script_upload_bulk_data.xsl');
	$xslt->importStylesheet($xsl);
	print $xslt->transformToXML($doc);
?>