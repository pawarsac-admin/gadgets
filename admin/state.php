<?php
	require_once('../include/config.php');
	require_once(CLASSPATH.'DbConn.php');
	require_once(CLASSPATH.'citystate.class.php');

	$dbconn = new DbConn;
	$oCityState = new citystate;

	$state_id = $_REQUEST['state_id'];
	$state_name = trim($_REQUEST['state_name']);
	$status = $_REQUEST['state_status'];
	$actiontype = $_REQUEST['actiontype'];
	$startlimit = $_REQUEST['startlimit'];
	$limitcnt = $_REQUEST['cnt'];

	$country_id = $_REQUEST['country_id'] ? $_REQUEST['country_id'] : COUNTRYID ;
	if(!empty($state_name)){
		$request_param['state_name'] = htmlentities($state_name,ENT_QUOTES);
	}
	if(!empty($country_id)){
		$request_param['country_id'] = $country_id;
	}
	if($status != ''){
		$request_param['state_status'] = $status;
	}

	if($actiontype == 'Insert'){
		$result = $oCityState->intInsertUpdateState($request_param);
		$msg ='State added successfully.';
	}elseif($actiontype== 'Update'){
		$result = $oCityState->intUpdateState($state_id,$request_param);
		$msg ='State added successfully.';
	}elseif($actiontype == 'Delete'){
		$result = $oCityState->boolDeleteState($state_id);
		$msg = 'State deleted successfully.';
	}

	$result = $oCityState->arrGetStateDetails("","","",$startlimit,$limitcnt);
	$cnt = sizeof($result);
	$xml = "<STATE_MASTER>";
	$xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
	for($i=0;$i<$cnt;$i++){
		$status = $result[$i]['state_status'];
		$result[$i]['state_status'] = ($status == 1) ? 'Active' : 'InActive';
		$result[$i]['create_date'] = date('d-m-Y',strtotime($result[$i]['create_date']));
		$result[$i]['js_state_name'] = $result[$i]['state_name'];
		$result[$i]['state_name'] = html_entity_decode($result[$i]['state_name'],ENT_QUOTES);
		$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
		$xml .= "<STATE_MASTER_DATA>";
		
		foreach($result[$i] as $k=>$v){
			$xml .= "<$k><![CDATA[$v]]></$k>";
		}
		$xml .= "</STATE_MASTER_DATA>";
	}
	$xml .= "</STATE_MASTER>";

	$config_details = get_config_details();
	$strXML = "<XML>";
	$strXML .= "<MSG><![CDATA[$msg]]></MSG>";
	$strXML .= "<STARTLIMIT><![CDATA[$startlimit]]></STARTLIMIT>";
	$strXML .= "<CNT><![CDATA[$limitcnt]]></CNT>";
	$strXML .= $config_details;
	$strXML .= $xml;
	$strXML .= "</XML>";

	$strXML = mb_convert_encoding($strXML, "UTF-8");
	if($_GET['debug']==1) { header('Content-type: text/xml');echo $strXML;exit; }

	$doc = new DOMDocument();
	$doc->loadXML($strXML);
	$doc->saveXML();

	$xslt = new xsltProcessor;
	$xsl = DOMDocument::load('xsl/state.xsl');

	$xslt->importStylesheet($xsl);
	print $xslt->transformToXML($doc);
?>