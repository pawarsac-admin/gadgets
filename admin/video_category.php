<?php
        require_once('../include/config.php');
        require_once(CLASSPATH.'DbConn.php');
	require_once(CLASSPATH.'videos.class.php');
	
        $dbconn = new DbConn;
	$videos = new videos; 
	
	//if($_POST){ print_r($_REQUEST);} //die();
	$actiontype = $_REQUEST['actiontype'];
	$startlimit = $_REQUEST['startlimit'];
	$limitcnt = $_REQUEST['cnt'];
	$request_param=$_REQUEST;
	$category_id = $_REQUEST['selected_category_id'];
	$category_id = ($category_id != "") ? $category_id : SITE_CATEGORY_ID;

	if($actiontype == 'Insert'|| $actiontype== 'Update'){
		unset($request_param);
		
		$tab_name = $_REQUEST["video_cat_name"];
		if(!empty($tab_name)){
			$tab_name = htmlentities($tab_name,ENT_QUOTES);
		}
		$order_tab = $_REQUEST["order_tab"];
		$status = $_REQUEST["status"];
		$hd_video_tab_id = $_REQUEST["hd_video_tab_id"];

		$request_param['category_id'] = $category_id;
		$request_param['tab_name'] = $tab_name;
		$request_param['order_tab'] = $order_tab;
		$request_param['status'] = $status;
		
                //print"<pre>";print_r($request_param);print"</pre>";
		if($actiontype == 'Insert'){
		       $result = $videos->intInsertVideoTab($request_param);
                }elseif($actiontype == 'Update'){
				$id = $hd_video_tab_id;
				$request_param['tab_id'] = $id;
                                //print"<pre>";print_r($request_param);print"</pre>";
                                $result = $videos->intInsertVideoTab($request_param);
		}	
		if($sresult>0){
                        if($actiontype == 'Insert'){
                                $msg = 'video category added successfully.';
                        }else{
                                $msg = 'video category updated successfully.';
                        }
                }
	}

	if($actiontype == 'Delete'){
		$id = $_REQUEST["hd_video_tab_id"];
        	if($id!=''){
                	$result = $videos->booldeleteVideoTab($id);
	                $msg = 'video category deleted successfully.';
        	}
	}

	$config_details = get_config_details();

        $strXML = "<XML>";
        $strXML .= "<MSG><![CDATA[$msg]]></MSG>";
        $strXML .= "<SELECTED_MENU_ID><![CDATA[$menu_level]]></SELECTED_MENU_ID>";
        $strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$category_id]]></SELECTED_CATEGORY_ID>";
	$strXML .= "<SELECTED_ACTION_TYPE><![CDATA[$actiontype]]></SELECTED_ACTION_TYPE>";
	$strXML .= "<VIEWSECTION><![CDATA[$selected_video_type_id]]></VIEWSECTION>";
        $strXML .= "<STARTLIMIT><![CDATA[$startlimit]]></STARTLIMIT>";
        $strXML .= "<CNT><![CDATA[$limitcnt]]></CNT>";
        $strXML .= $config_details;
        $strXML .= $xml;
        $strXML .= "</XML>";

	if($_GET['debug']==1) { header('Content-type: text/xml');echo $strXML;exit; }

        $doc = new DOMDocument();
        $doc->loadXML($strXML);
        $doc->saveXML();

        $xslt = new xsltProcessor;
        $xsl = DOMDocument::load('xsl/video_category.xsl');

        $xslt->importStylesheet($xsl);
        print $xslt->transformToXML($doc);

?>

