<?php
	require_once('../include/config.php');
	require_once(CLASSPATH.'DbConn.php');
	require_once(CLASSPATH.'reviews.class.php');
	
	$dbconn = new DbConn;
	$oReview = new reviews;
	
	//print "<pre>"; print_r($_REQUEST);
	$startlimit = $_REQUEST['startlimit'];
	$limitcnt = $_REQUEST['cnt'];	
	$question_id = $_REQUEST['q_id'];	
	
	
	$result = $oReview->arrGetQuestionDetails("","","","","create_date");
	
	$cnt = sizeof($result);
	$xml = "<QUESTION_MASTER>";
	$xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
	for($i=0;$i<$cnt;$i++){
		$result[$i]['question_id'] = $result[$i]['question_id'];
		$result[$i]['name'] = $result[$i]['name'];
		$result[$i]['name'] = html_entity_decode($result[$i]['name'],ENT_QUOTES);
		$result[$i]['email_id'] = $result[$i]['email_id'];
		$result[$i]['question'] = html_entity_decode($result[$i]['question'],ENT_QUOTES);
		$result[$i]['create_date'] = date('d-m-Y',strtotime($result[$i]['create_date']));
		if(!empty($result[$i]['email_id'])){$email = $result[$i]['email_id'];}

		$send_mail = "mailto:".$email."?subject=Expert's Reply&body=Thanks";
		$result[$i]['send_mail_to'] = $send_mail;

		$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
		$xml .= "<QUESTION_MASTER_DATA>";
		foreach($result[$i] as $k=>$v){
			$xml .= "<$k><![CDATA[$v]]></$k>";
		}
		$xml .= "</QUESTION_MASTER_DATA>";
	}
	$xml .= "</QUESTION_MASTER>";
	

	$config_details = get_config_details();
	$strXML = "<XML>";
	$strXML .= "<MSG><![CDATA[$msg]]></MSG>";
	$strXML .= "<STARTLIMIT><![CDATA[$startlimit]]></STARTLIMIT>";
	$strXML .= "<CNT><![CDATA[$limitcnt]]></CNT>";
	$strXML .= $config_details;
	$strXML .= $xml;
	$strXML .= $sStateXml;
	$strXML .= "</XML>";

	$strXML = mb_convert_encoding($strXML, "UTF-8");
	if($_GET['debug']==1) { header('Content-type: text/xml');echo $strXML;exit; }

	$doc = new DOMDocument();
	$doc->loadXML($strXML);
	$doc->saveXML();

	$xslt = new xsltProcessor;
	$xsl = DOMDocument::load('xsl/ask_expert.xsl');

	$xslt->importStylesheet($xsl);
	print $xslt->transformToXML($doc);
?>
