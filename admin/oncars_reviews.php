<?php
require_once('../include/config.php');
require_once(CLASSPATH.'DbConn.php');
require_once(CLASSPATH.'reviews.class.php');

$dbconn = new DbConn;
$oReviews = new reviews;

if($_POST){ 
	//print_r($_REQUEST); 
	//die();
}  
$editor_id = $_REQUEST['editor_id'];
$publish_time = $_REQUEST['publish_time'];
$actiontype = $_REQUEST['actiontype'] ? $_REQUEST['actiontype'] : "Insert";
$startlimit = $_REQUEST['startlimit'];
$limitcnt = $_REQUEST['cnt'];
$category_id = $_REQUEST['selected_category_id'] ? $_REQUEST['selected_category_id'] : $_REQUEST['catid'];
$category_id = ($category_id != "") ? $category_id : SITE_CATEGORY_ID;

$category_ids = $category_id;

$iRId=$_REQUEST['review_id'];

if($actiontype == 'Insert'|| $actiontype== 'Update'){
	$sTitle = trim($_REQUEST['review_title']);
	$sTitle = translatechars($sTitle);	
	if(!empty($sTitle)){ $request_param['title'] = htmlentities($sTitle,ENT_QUOTES);}
	
	$sDescription = translatechars($_REQUEST['review_description']);	
	if(!empty($sDescription)){ $request_param['content'] = htmlentities($sDescription,ENT_QUOTES);}
	
	$sAbstract = $_REQUEST['review_abstract']  ? $_REQUEST['review_abstract']  : $_REQUEST['mce_review_abstract'];
	$sAbstract = translatechars($sAbstract);

	 $request_param['abstract'] = htmlentities($sAbstract,ENT_QUOTES);
	
	if(!empty($iRId)){ $request_param['review_id']=$iRId;}
	
	$sStatus=$_REQUEST['review_status'];
	if($sStatus!=''){ $request_param['status']=$sStatus;}

	$sMediaId=$_REQUEST['media_id'];
	if(!empty($sMediaId)){ $request_param['media_id']=$sMediaId;}

	$sImgUpload1=trim($_REQUEST['media_upload_1']);
	if(!empty($sImgUpload1)){ $request_param['video_path']=$sImgUpload1;}	

	$sImgMediaId=$_REQUEST['abstract_img_id'];
	if(!empty($sImgMediaId)){ $request_param['img_media_id']=$sImgMediaId;}

	$sImgThmUpload=trim($_REQUEST['abstract_img_path']);
	if(!empty($sImgThmUpload)){ $request_param['image_path']=$sImgThmUpload;}		
	
	$sTags=trim($_REQUEST['review_tags']);
	if(!empty($sTags)){ $request_param['tags']=$sTags;}	
	
	$sSource=trim($_REQUEST['review_source']);
	if(!empty($sSource)){ $request_param['source']=$sSource;}

	$sSourceUrl=trim($_REQUEST['review_source_url']);
	if(!empty($sSourceUrl)){ $request_param['source_url']=$sSourceUrl;}		
	
	$sTags=trim($_REQUEST['review_tags']);
	if(!empty($sTags)){ $request_param['tags']=$sTags;}	

	$sArtType=$_REQUEST['select_aritcle_type_id'];
	$request_param['uid'] = $editor_id;
	$request_param['publish_time'] = $publish_time;
	if($sArtType!=''){ $request_param['review_type']=$sArtType;}		
	//print "<pre>"; print_r($request_param);
	if($sTitle!=''){
		$iResId=$oReviews->addUpdReviewsDetails($request_param,"REVIEWS");
	}
	if($iRId==0){$iRId=$iResId;}
	if($iRId!=''){
		$iPrdRId=$_REQUEST['product_review_id'];
		if(!empty($iPrdRId)){ $request_param1['product_review_id']=$iPrdRId;}
		
		if(!empty($iRId)){ $request_param1['review_id']=$iRId;}
		
		$iGroupId=$_REQUEST['select_aritcle_group_id'];
		if($iGroupId!=''){ $request_param1['group_id']=$iGroupId;}
		
		$iCategoryId=$category_id;
		if(!empty($iCategoryId)){ $request_param1['category_id']=$iCategoryId;}
		
		$iBrandId=$_REQUEST['select_brand_id'];
		if($iBrandId!=''){ $request_param1['brand_id']=$iBrandId;}
		
		$iProductId=$_REQUEST['product_id'];
		if($iProductId!=''){ $request_param1['product_id']=$iProductId;}
		
		$iModelId=$_REQUEST['select_model_id'];
		if($iModelId!=''){ $request_param1['product_info_id']=$iModelId;}
		//print "<pre>"; print_r($request_param1);
		if($sTitle!=''){
			$iProdArtResId=$oReviews->addUpdReviewsDetails($request_param1,"PRODUCT_REVIEWS");
		}
		$iPrdRId==0 ? $iPrdRId=$iProdArtResId : $iPrdRId=$_REQUEST['product_review_id'];
		if($iPrdRId!=''){
		
			if($_REQUEST['display_rows']>0){
				unset($request_param);
				$dataCount=$_REQUEST['display_rows'];
				for($i=1;$i<=$dataCount;$i++){
					$upload_media_id = $_REQUEST['upload_media_id_'.$i];
					if(!empty($upload_media_id)){
						$request_param['upload_media_id'] = $upload_media_id;
					}
					$request_param['product_review_id'] = $iPrdRId;
					$media_id = $_REQUEST['media_id_'.$i];
					$request_param['media_id'] = $media_id;	

					$image_title = $_REQUEST['image_title_'.$i];
					$image_title=htmlentities($image_title,ENT_QUOTES);
				//	if(!empty($image_title)){ 
						$request_param['image_title'] = $image_title;	
				//	}

					$media_path = trim($_REQUEST['media_upload_1_'.$i]);


					$media_img_id = $_REQUEST['img_media_id_'.$i];
					$request_param['video_img_id'] = $media_img_id;

					$media_img_path = trim($_REQUEST['img_upload_id_thm_'.$i]);
					$request_param['video_img_path'] = $media_img_path;

					$content = $_REQUEST['review_description_'.$i] ? $_REQUEST['review_description_'.$i] : $_REQUEST['mce_review_description_'.$i];
					$content = translatechars($content);
					$request_param["content"] = htmlentities($content,ENT_QUOTES,'UTF-8');

					$content_type = !empty($_REQUEST['video_content_type_'.$i]) ? $_REQUEST['video_content_type_'.$i] : $_REQUEST['content_type_'.$i];
					$request_param['content_type'] = $content_type;
					
					if($content_type == 1){
						//for video
						if(!empty($media_img_path) && empty($media_path)){
							$request_param['media_path'] = $media_img_path;
							$request_param['is_media_process'] = 0;
						}

					}else if($content_type == 2){
						//for image
						if(!empty($media_path)){
							$request_param['media_path'] = $media_path;
							$request_param['video_img_path'] = $media_path;
						}
						if(!empty($media_id)){
							$request_param['video_img_id'] = $media_id;
						}
						$request_param['is_media_process'] = 1;
					}else if($content_type == 3){
						//for audio
						if(!empty($media_img_path) && empty($media_path)){
							$request_param['media_path'] = $media_img_path;
							$request_param['is_media_process'] = 0;
						}
					}
					$check_flag = $_REQUEST['check_flag_'.$i];
					if($check_flag == 1){
						$request_param['media_id'] = "";
						$request_param['media_path'] = "";
						$request_param['video_img_id']="";
						$request_param['video_img_path'] = "";
						$request_param['content_type'] = "";
						$request_param['is_media_process'] = "";
					}
					
					//if(!empty($media_id)){
						$iProdArtMediaId=$oReviews->addUpdReviewsDetails($request_param,"UPLOAD_MEDIA_REVIEWS");
						unset($request_param);
					//}
				}
			}
		}
	}
	if($iRId==0) {
		$iRId=$iResId;
		$msg="Review detail added successfully.";
	}else {
		$msg="Review detail updated successfully.";
	}
	//$actiontype = "Select";

}
if($actiontype == 'Delete'){
	$result = $oReviews->booldeleteReviews($iRId);
	$msg = 'Review deleted successfully.';
}


if(!empty($_REQUEST['review_id']) && $_REQUEST['act']="update"){
		
		$act=$_REQUEST['act'];
		$review_id=$_REQUEST['review_id'];
		//echo "ENETER--".$review_id;
		$review_result = $oReviews->getReviewsDetails($review_id,"","","","",$category_ids,"","1");
		//print_r($review_result);
		
		$iCnt = sizeof($review_result);
		if(is_array($review_result)){
			$review_id = $review_result['0']['review_id'];
			$brand_id = $review_result['0']['brand_id'];
			$product_id = $review_result['0']['product_id'];
			$product_info_id = $review_result['0']['product_info_id'];
			$product_article_id = $review_result['0']['product_review_id'];
			$review_type = $review_result['0']['review_type'];
			unset($review_result);
		}
	}
	


$config_details = get_config_details();
$strXML = "<XML>";
$strXML .= "<MSG><![CDATA[$msg]]></MSG>";
$strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$category_ids]]></SELECTED_CATEGORY_ID>";
$strXML .= "<STARTLIMIT><![CDATA[$startlimit]]></STARTLIMIT>";
$strXML .= "<SELECTED_ACTION_TYPE><![CDATA[$actiontype]]></SELECTED_ACTION_TYPE>";
$strXML .= "<CNT><![CDATA[$limitcnt]]></CNT>";
$strXML .= $config_details;
$strXML .= $xml;
$strXML .= "<ACT><![CDATA[$act]]></ACT>";

if($_REQUEST['act']="update"){
	//echo "SACHIHN";
	$strXML .= "<REVIEW_ID><![CDATA[$review_id]]></REVIEW_ID>";
	$strXML .= "<BRAND_ID><![CDATA[$brand_id]]></BRAND_ID>";
	$strXML .= "<PRODUCT_ID><![CDATA[$product_id]]></PRODUCT_ID>";
	$strXML .= "<PRODUCT_INFO_ID><![CDATA[$product_info_id]]></PRODUCT_INFO_ID>";
	$strXML .= "<PRODUCT_REVIEW_ID><![CDATA[$product_review_id]]></PRODUCT_REVIEW_ID>";
	$strXML .= "<REVIEW_TYPE><![CDATA[$review_type]]></REVIEW_TYPE>";
}


$strXML .= "</XML>";

$strXML = mb_convert_encoding($strXML, "UTF-8");
if($_GET['debug']==1) { header('Content-type: text/xml');echo $strXML;exit; }

$doc = new DOMDocument();
$doc->loadXML($strXML);
$doc->saveXML();

$xslt = new xsltProcessor;
$xsl = DOMDocument::load('xsl/oncars_reviews.xsl');

$xslt->importStylesheet($xsl);
print $xslt->transformToXML($doc);
?>
