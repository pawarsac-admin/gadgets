<?php
	require_once('../include/config.php');
	require_once(CLASSPATH.'DbConn.php');
	require_once(CLASSPATH.'overview.class.php');
	require_once(CLASSPATH.'feature.class.php');
	$dbconn = new DbConn;
	$overview = new OverviewManagement;
	$feature = new FeatureManagement;
	//print_r($_REQUEST);
	$category_id = $_REQUEST['selected_category_id'];
	$main_group_id =  $_REQUEST['sel_main_group'] ?  $_REQUEST['sel_main_group'] : $_REQUEST['main_group'];

	if($category_id != ''){
		$request_param['category_id'] = $category_id;
		$result = $feature->arrGetFeatureDetails($feature_id);
		if(sizeof($result) > 0){
			$request_param['overview_sub_group_id'] = $result[0]['feature_group'];
		}
	}
	$feature_id = $_REQUEST['feature_id'];
	
	$status = $_REQUEST['feature_overview_status'];
		
	$overview_id = $_REQUEST['overview_id'];
	$actiontype = $_REQUEST['actiontype'];
	$startlimit = $_REQUEST['startlimit'];
    	$limitcnt = $_REQUEST['cnt'];	
	
	
	//print_r($_REQUEST);
	if($actiontype == 'Delete'){
	   $result = $overview->boolDeleteCompareFeatureOverview($overview_id);
	   $msg = 'Feature Overview deleted successfully.';
	}elseif($actiontype == 'Update'){
	   //$result = $brand->boolUpdateBrand($brand_id,$request_param);
	  // $msg = 'Brand updated successfully.';
	}elseif($actiontype == 'insert'){
	   $result = $overview->intInsertCompareFeatureOverview($feature_id);	 
	  // echo "overview id = $result";exit;
	   $msg = ($result == 'exists') ? 'Feature Compare Overview already exists.' : 'Feature Compare Overview added successfully.';
	}
	$startlimit = $_REQUEST['startlimit'];
	$limitcnt = $_REQUEST['cnt'];

	$config_details = get_config_details();
	$strXML = "<XML>";
	$strXML .= "<MSG><![CDATA[$msg]]></MSG>";
	$strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$category_id]]></SELECTED_CATEGORY_ID>";
	$strXML .= "<SELECTED_MAIN_GROUP_ID><![CDATA[$main_group_id]]></SELECTED_MAIN_GROUP_ID>";
	$strXML .= "<STARTLIMIT><![CDATA[$startlimit]]></STARTLIMIT>";
	$strXML .= "<CNT><![CDATA[$limitcnt]]></CNT>";
	$strXML .= $config_details;
	$strXML .= $xml;
	$strXML .= "</XML>";
	//header('Content-type: text/xml');echo $strXML;exit;
	$doc = new DOMDocument();
	$doc->loadXML($strXML);
	$doc->saveXML();
	$xslt = new xsltProcessor;
	$xsl = DOMDocument::load('xsl/compare_feature_overview.xsl');
	$xslt->importStylesheet($xsl);
	print $xslt->transformToXML($doc);
?>
