<?php
	require_once('../include/config.php');
	require_once(CLASSPATH.'DbConn.php');
	require_once(CLASSPATH.'user_review.class.php');

	$dbconn = new DbConn;
	$userreview = new USERREVIEW;
	
	$category_id = $_REQUEST['selected_category_id'];
	if($category_id != ''){
		$request_param['category_id'] = $category_id;
	}

	$product_id = $_REQUEST['product_id'];
	if($product_id != ''){
		$request_param['product_id'] = $product_id;
	}

	$brand_id = $_REQUEST['select_brand_id'];
	if($brand_id != ''){
		$request_param['brand_id'] = $brand_id;
	}

	$select_model_id = $_REQUEST['select_model_id'];
	if($select_model_id != ''){
		$request_param['product_info_id'] = $select_model_id;
	}
	$status = $_REQUEST['overall_grade_status'];
	if($status != ''){
		$request_param['status'] = $status;
	}

	$overallgrade = $_REQUEST['overallgrade'];
	if($overallgrade != ''){
		$request_param['overallgrade'] = $overallgrade;
	}
	
	$actiontype = $_REQUEST['actiontype'];
	$admin_rating_id = $_REQUEST['admin_rating_id'];

    if($actiontype == 'Delete'){
	   $result = $userreview->boolAdminOverallRating($admin_rating_id);
	   $msg = 'Overall rating deleted successfully.';
	}elseif($actiontype == 'Update'){
	   $result = $userreview->boolUpdateAdminOverallRating($admin_rating_id,$request_param);
	   $msg = 'Overall rating  updated successfully.';
	}elseif($actiontype == 'insert'){
	   $result = $userreview->intInsertAdminOverallRating($request_param);
	   $msg = ($result == 'exists') ? 'Overall rating already exists.' : 'Overall rating added successfully.';
	}

	$config_details = get_config_details();
	$strXML = "<XML>";
	$strXML .= "<MSG><![CDATA[$msg]]></MSG>";
	$strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$category_id]]></SELECTED_CATEGORY_ID>";
	$strXML .= "<STARTLIMIT><![CDATA[$startlimit]]></STARTLIMIT>";
	$strXML .= "<CNT><![CDATA[$limitcnt]]></CNT>";
	$strXML .= $config_details;
	$strXML .= $xml;
	$strXML .= "</XML>";
	//header('Content-type: text/xml');echo $strXML;exit;
	$doc = new DOMDocument();
	$doc->loadXML($strXML);
	$doc->saveXML();
	$xslt = new xsltProcessor;
	$xsl = DOMDocument::load('xsl/overall_rating.xsl');
	$xslt->importStylesheet($xsl);
	print $xslt->transformToXML($doc);
?>