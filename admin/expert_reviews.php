<?php
require_once('../include/config.php');
require_once(CLASSPATH.'DbConn.php');
require_once(CLASSPATH.'reviews.class.php');

$dbconn = new DbConn;
$oReviews = new reviews;

//if($_POST){ print_r($_REQUEST);} ////die();
$actiontype = $_REQUEST['actiontype'];
$startlimit = $_REQUEST['startlimit'];
$limitcnt = $_REQUEST['cnt'];
$category_id = $_REQUEST['selected_category_id'];

$_REQUEST['product_id'] !=0 ? $_REQUEST['product_id'] :0;
$_REQUEST['select_model_id'] !=0 ? $_REQUEST['select_model_id'] :0;  

$iRId=$_REQUEST['review_id'];

if($actiontype == 'Insert'|| $actiontype== 'Update'){
	$sTitle = trim($_REQUEST['review_title']);
	$sTitle = translatechars($sTitle);	
	if(!empty($sTitle)){ $request_param['title'] = htmlentities($sTitle,ENT_QUOTES);}
	
	$sDescription = translatechars($_REQUEST['review_description']);	
	if(!empty($sDescription)){ $request_param['content'] = htmlentities($sDescription,ENT_QUOTES);}
	
	$sAbstract = translatechars($_REQUEST['review_abstract']);	
	if(!empty($sAbstract)){ $request_param['abstract'] = htmlentities($sAbstract,ENT_QUOTES);}
	
	if(!empty($iRId)){ $request_param['review_id']=$iRId;}
	
	$sStatus=$_REQUEST['review_status'];
	if($sStatus!=''){ $request_param['status']=$sStatus;}

	$sMediaId=$_REQUEST['media_id'];
	if(!empty($sMediaId)){ $request_param['media_id']=$sMediaId;}

	$sImgUpload1=trim($_REQUEST['img_upload_1']);
	if(!empty($sImgUpload1)){ $request_param['video_path']=$sImgUpload1;}	

	$sImgMediaId=$_REQUEST['img_media_id'];
	if(!empty($sImgMediaId)){ $request_param['img_media_id']=$sImgMediaId;}

	$sImgThmUpload=trim($_REQUEST['img_upload_thm']);
	if(!empty($sImgThmUpload)){ $request_param['image_path']=$sImgThmUpload;}		
	
	$sTags=trim($_REQUEST['review_tags']);
	if(!empty($sTags)){ $request_param['tags']=$sTags;}	
	
	$sSource=trim($_REQUEST['review_source']);
	if(!empty($sSource)){ $request_param['source']=$sSource;}

	$sSourceUrl=trim($_REQUEST['review_source_url']);
	if(!empty($sSourceUrl)){ $request_param['source_url']=$sSourceUrl;}		
	
	$sTags=trim($_REQUEST['review_tags']);
	if(!empty($sTags)){ $request_param['tags']=$sTags;}	

	$sArtType=$_REQUEST['select_aritcle_type_id'];
	if($sArtType!=''){ $request_param['review_type']=$sArtType;}		
	//print "<pre>"; print_r($request_param);
	$iResId=$oReviews->addUpdReviewsDetails($request_param,"EXPERT_REVIEWS");
	if($iRId==0){$iRId=$iResId;}
	if($iRId!=''){
		$iPrdRId=$_REQUEST['product_review_id'];
		if(!empty($iPrdRId)){ $request_param1['product_review_id']=$iPrdRId;}
		
		if(!empty($iRId)){ $request_param1['review_id']=$iRId;}
		
		$iGroupId=$_REQUEST['select_aritcle_group_id'];
		if($iGroupId!=''){ $request_param1['group_id']=$iGroupId;}
		
		$iCategoryId=$_REQUEST['selected_category_id'];
		if(!empty($iCategoryId)){ $request_param1['category_id']=$iCategoryId;}
		
		$iBrandId=$_REQUEST['select_brand_id'];
		if($iBrandId!=''){ $request_param1['brand_id']=$iBrandId;}
		
		$iProductId=$_REQUEST['product_id'];
		if($iProductId!=''){ $request_param1['product_id']=$iProductId;}
		
		$iModelId=$_REQUEST['select_model_id'];
		if($iModelId!=''){ $request_param1['product_info_id']=$iModelId;}
		//print "<pre>"; print_r($request_param1);
		
		$iProdArtResId=$oReviews->addUpdReviewsDetails($request_param1,"PRODUCT_EXPERT_REVIEWS");
		$iPrdRId==0 ? $iPrdRId=$iProdArtResId : $iPrdRId=$_REQUEST['product_review_id'];
		
		
	}
	if($iRId==0) {
		$iRId=$iResId;
		$msg="Review detail added successfully.";
	}else {
		$msg="Review detail updated successfully.";
	}
}
if($actiontype == 'Delete'){
	$result = $oReviews->booldeleteExpertReviews($iRId);
	$msg = 'Review deleted successfully.';
}
$config_details = get_config_details();
$strXML = "<XML>";
$strXML .= "<MSG><![CDATA[$msg]]></MSG>";
$strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$category_id]]></SELECTED_CATEGORY_ID>";
$strXML .= "<STARTLIMIT><![CDATA[$startlimit]]></STARTLIMIT>";
$strXML .= "<CNT><![CDATA[$limitcnt]]></CNT>";
$strXML .= $config_details;
$strXML .= $xml;
$strXML .= "</XML>";

$strXML = mb_convert_encoding($strXML, "UTF-8");
if($_GET['debug']==1) { header('Content-type: text/xml');echo $strXML;exit; }

$doc = new DOMDocument();
$doc->loadXML($strXML);
$doc->saveXML();

$xslt = new xsltProcessor;
$xsl = DOMDocument::load('xsl/expert_reviews.xsl');

$xslt->importStylesheet($xsl);
print $xslt->transformToXML($doc);
?>
