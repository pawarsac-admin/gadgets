/**
* @note function is used to update the menu details.
* @param integer menuid.
* @param string menuname.
* @param integer menustatus.
* @pre menuid must be valid,non-empty,non-zero integer.menuname and catstatus must be valid,non-emtpy string.
* @post a form.
* return submit form
*/
function updateMenu(menuid,menuname,menupage,menutitle,menustatus,menu_level){
	menuname = menuname.replace(/%26/,"&");
	menuname = menuname.replace(/&#039;/,"'");
	menuname = menuname.replace(/%2C/,",");
	menuname = menuname.replace(/%2F/,"/");

	menupage = menupage.replace(/%26/,"&");
	menupage = menupage.replace(/&#039;/,"'");
	menupage = menupage.replace(/%2C/,",");
	menupage = menupage.replace(/%2F/,"/");

	menutitle = menutitle.replace(/%26/,"&");
	menutitle = menutitle.replace(/&#039;/,"'");
	menutitle = menutitle.replace(/%2C/,",");
	menutitle = menutitle.replace(/%2F/,"/");

	document.getElementById('menu_name').value = menuname;
	document.getElementById('menu_page').value = menupage;
	document.getElementById('menu_title').value = menutitle;
	document.getElementById('menu_status').value = menustatus;
	document.getElementById('menu_id').value = menuid;
	document.getElementById('actiontype').value = 'Update';
	return true;
}
/**
* @note function is used to delete the menu.
* @param integer menuid.
* @pre menuid must be valid,non-empty,non-zero integer.
* @post a form.
* return submit form
*/
function deleteMenu(menuid,menuname){
	menuname = menuname.replace(/%26/,"&");
	menuname = menuname.replace(/&#039;/,"'");
	menuname = menuname.replace(/%2C/,",");
	menuname = menuname.replace(/%2F/,"/");

        document.getElementById('menu_name').value = menuname;

	document.getElementById('menu_id').value = menuid;
        document.getElementById('actiontype').value = 'Delete';
	var answer = confirm ("Are you sure.Wants to delete menu '"+menuname+"'?")
	if (answer){
		document.menu_action.submit();
		return true;
	}else{
		return false;
	}
}
function validateMenu(){
	var err = "";
	if(document.getElementById('menu_name')){
		if(document.getElementById('menu_name').value == ''){
			//alert('Menu must not be empty');
			err+="Menu name must not be empty. \n";
			document.getElementById('menu_name').focus();
		}
	}
	/*if(document.getElementById('menu_page')){
                if(document.getElementById('menu_page').value == ''){
                        //alert('Menu page not be empty');
			err+="Menu page must not be empty. \n";
                        document.getElementById('menu_page').focus();
                }
        }*/
	if(document.getElementById('menu_title')){
                if(document.getElementById('menu_title').value == ''){
                        //alert('Menu title not be empty');
			err+="Menu title must not be empty.";
                        document.getElementById('menu_title').focus();
                }
        }

	if(err != ""){
		alert(err);
		return false;
	}
	return true;

}
