function getVariantDashboard(divid,ajaxloaderid,category_id,startlimit,cnt){
	if(category_id == ''){
		var category_id = document.getElementById('selected_category_id').value;
	
		if(isCategorySelected() == false){
			alert("Please select the category.");
			return false;
		}
		if(isLastLvlCategory() == false){
			alert("Please select last level category.");
			return false;
		}
	}
	document.getElementById(ajaxloaderid).style.display = "block";
        if(divid == ""){ return false; }
        var url = admin_web_url+'ajax/variant_dashboard.php';
        $.ajax({
         url: url,
         data: 'catid='+category_id+'&startlimit='+startlimit+'&cnt='+cnt,
         success: function(data){
                         document.getElementById(divid).innerHTML = data;
                         document.getElementById(divid).style.display="block";
                         document.getElementById(ajaxloaderid).style.display = "none";
                 },
                 async:false
        });
	return true;
}
function updateVariant(variantid,variantname,variantstatus){
	variantname = variantname.replace(/%26/,"&");
	variantname = variantname.replace(/&#039;/,"'");
	variantname = variantname.replace(/%2C/,",");
	variantname = variantname.replace(/%2F/,"/");
	if(variantstatus=="Active"){var status=1;}else{var status=0;}
	document.getElementById('status').value = status;
	document.getElementById('variant').value = variantname;
	document.getElementById('variant_id').value = variantid;
	document.getElementById('actiontype').value = 'Update';
	return false;
}
function deleteVariant(variantid,variantname){
	variantname = variantname.replace(/%26/,"&");
	variantname = variantname.replace(/&#039;/,"'");
	variantname = variantname.replace(/%2C/,",");
	variantname = variantname.replace(/%2F/,"/");
	document.getElementById('variant_id').value = variantid;
    	document.getElementById('actiontype').value = 'Delete';
    	var answer = confirm ("Are you sure.Want to delete variant '"+variantname+"'?")
    	if (answer){
         	document.variant_action.submit();
        	return true;
     	}
    	return false;
}
function validateVariant(){
	if(isCategorySelected() == false){
                alert("Please select the category.");
                return false;
        }
        if(isLastLvlCategory() == false){
                alert("Please select last level category.");
                return false;
        }
	if(document.getElementById('variant').value == ''){
		alert("Please add the variant");
		document.getElementById('variant').focus();
		return false;
	}
	//document.getElementById('actiontype').value = 'Insert';
	return true;
}
