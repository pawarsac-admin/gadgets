function updateCity(cityid,cityname,citystatus,stateid){
	//alert(cityid+"---"+cityname+"---"+citystatus+"---"+stateid);
	cityname = cityname.replace(/%26/,"&");
	cityname = cityname.replace(/&#039;/,"'");
	cityname = cityname.replace(/%2C/,",");
	cityname = cityname.replace(/%2F/,"/");

	//document.city_action.select_state_id.options[document.city_action.select_state_id.selectedIndex].value=stateid; 
	select_state_id = document.city_action.select_state_id;
	
	for (i=0; i<select_state_id.options.length; i++) {
		if (select_state_id.options[i].value == stateid) {
			select_state_id.selectedIndex = i;
		}
	}

	city_status = document.city_action.city_status;
	
	for (ii=0; ii<city_status.options.length; ii++) {
		if (city_status.options[ii].text == citystatus) {
			city_status.selectedIndex = ii;
		}
	}

	//document.getElementById('city_status').value = citystatus;
	document.getElementById('city_name').value = cityname;
	document.getElementById('city_id').value = cityid;
	document.getElementById('actiontype').value = 'Update';
	return false;
}
function deleteCity(cityid,cityname){
	cityname = cityname.replace(/%26/,"&");
	cityname = cityname.replace(/&#039;/,"'");
	cityname = cityname.replace(/%2C/,",");
	cityname = cityname.replace(/%2F/,"/");
	document.getElementById('city_id').value = cityid;
	document.getElementById('actiontype').value = 'Delete';
	var answer = confirm ("Are you sure.Want to delete  city '"+cityname+"'?")
    	if (answer){
         	document.city_action.submit();
         	return true;
     	}
	return false;
}



function validateCity(){
	if(document.getElementById('city_name').value == ''){
		alert("Please add the city");
		document.getElementById('city_name').focus();
		return false;
	}
	document.getElementById('actiontype').value = 'Insert';
	return true;
}

function getProductByBrand(ajaxloaderid,cityid){
	
	var brand_id = document.getElementById('select_state_id').value;
	if(brand_id == '' ||  brand_id == 0){return false;}
	
	document.getElementById(ajaxloaderid).style.display = "block";
	var url = admin_web_url+'ajax/select_state.php';
	var html = $.ajax({ url: url, data: 'state_id='+state_id+'&city_id='+cityid, success: function(data){ document.getElementById(ajaxloaderid).style.display = "none";}, async: false}).responseText;
	
	var table = document.getElementById("Update");
	var rowCount = 1;


	var rowId='product_row_id_'+rowCount;
	if(document.getElementById(rowId)){
		removeTr(rowId);
	}
	var row = table.insertRow(rowCount);
	
	row.id = rowId;
	
	var product_name = row.insertCell(0);

    	product_name.innerHTML = 'State Name';

	var product_name_value = row.insertCell(1);
	product_name_value.colSpan = 10;
	product_name_value.innerHTML = html;
	return true;
}

function getCity(){
	if(document.getElementById('actiontype')){
		document.getElementById('actiontype').value = 'Select';
	}
	var show_stateid=document.getElementById('select_state_id_dash').options[document.getElementById('select_state_id_dash').selectedIndex].value;
	if(show_stateid!=0){
		document.getElementById("state_id").value=show_stateid;
		document.city_action.submit();
		return true;
	}else{
		alert("please select state");
		return false;
	}

}
