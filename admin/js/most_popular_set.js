function getProductMostPopularSetDashboard(divid,ajaxloaderid,category_id,startlimit,cnt){
	if(category_id == ''){
		var category_id = document.getElementById('selected_category_id').value;
		if(isCategorySelected() == false){
			alert("Please select the category.");
			return false;
		}
		if(isLastLvlCategory() == false){
			alert("Please select last level category.");
			return false;
		}
	}
	document.getElementById(ajaxloaderid).style.display = "block";
    if(divid == ""){ return false; }
		var url = admin_web_url+'ajax/most_popular_set_dashboard.php';
		//alert(url+'?catid='+category_id+'&startlimit='+startlimit+'&cnt='+cnt)
        $.ajax({
			url: url,
			data: 'catid='+category_id+'&startlimit='+startlimit+'&cnt='+cnt,
			success: function(data){
				//alert(data);
                document.getElementById(divid).innerHTML = data;
                document.getElementById(divid).style.display="block";
                document.getElementById(ajaxloaderid).style.display = "none";
            },
            async:false
        });
	return true;
}

function getModelByBrand(ajaxloaderid,product_name_id){
	var brand_id = document.getElementById('select_brand_id').value;
	if(brand_id == '' ||  brand_id == 0){return false;}
	var category_id = document.getElementById('selected_category_id').value;
	if(category_id == '' ||  category_id == 0){return false;}
	document.getElementById(ajaxloaderid).style.display = "block";
	var url = admin_web_url+'ajax/select_model.php';
	var html = $.ajax({ url: url, data: 'category_id='+category_id+'&brand_id='+brand_id+'&product_name_id='+product_name_id, success: function(data){ document.getElementById(ajaxloaderid).style.display = "none";}, async: false}).responseText;
	var table = document.getElementById("Update");
	var rowCount = 1;
	var rowId='product_row_id_'+rowCount;
	if(document.getElementById(rowId)){
		removeTr(rowId);
	}
	var row = table.insertRow(rowCount);
	row.id = rowId;
	var product_name = row.insertCell(0);
   	product_name.innerHTML = 'Model Name';
	var product_name_value = row.insertCell(1);
	product_name_value.colSpan = 10;
	product_name_value.innerHTML = html;
	return true;
}

function getVariantByModel(ajaxloaderid,product_id){
	var brand_id = document.getElementById('select_brand_id').value;
	if(brand_id == '' ||  brand_id == 0){return false;}
	var product_name_id = document.getElementById('select_model_id').value;
	if(product_name_id == '' ||  product_name_id == 0){return false;}
	var category_id = document.getElementById('selected_category_id').value;
	if(category_id == '' ||  category_id == 0){return false;}
	document.getElementById(ajaxloaderid).style.display = "block";
	var url = admin_web_url+'ajax/select_variant.php';
	var html = $.ajax({ url: url, data: 'category_id='+category_id+'&brand_id='+brand_id+'&product_id='+product_id+'&product_name_id='+product_name_id, success: function(data){ document.getElementById(ajaxloaderid).style.display = "none";}, async: false}).responseText;
	var table = document.getElementById("Update");
	var rowCount = 2;
	var rowId='product_row_id_'+rowCount;
	if(document.getElementById(rowId)){
		removeTr(rowId);
	}
	var row = table.insertRow(rowCount);
	row.id = rowId;
	var product_name = row.insertCell(0);
   	product_name.innerHTML = 'Product Name';
	var product_name_value = row.insertCell(1);
	product_name_value.colSpan = 10;
	product_name_value.innerHTML = html;
	return true;
}

function updateProductCompetitor(divid,ajaxloaderid,compare_id,productid,categoryid,brandid,comp_brand_id_1,comp_model_id_1,comp_product_id_1,comp_brand_id_2,comp_model_id_2,comp_product_id_2){
	if(category_id == ''){
		var category_id = document.getElementById('selected_category_id').value;
		if(isCategorySelected() == false){
			alert("Please select the category.");
			return false;
		}
		if(isLastLvlCategory() == false){
			alert("Please select last level category.");
			return false;
		}
	}
	document.getElementById(ajaxloaderid).style.display = "block";
    if(divid == ""){ return false; }
		var url = admin_web_url+'ajax/most_popular_set_dashboard.php';
        $.ajax({
			url: url,
			data: 'act=update&catid='+categoryid+'&compare_id='+compare_id+'&product_id='+productid,
			success: function(data){
				//alert(data);
                document.getElementById(divid).innerHTML = data;
                document.getElementById(divid).style.display="block";
                document.getElementById(ajaxloaderid).style.display = "none";
            },
            async:false
        });
	
	document.getElementById('actiontype').value = 'Update';
	getModelByBrandCompareSet1('ajaxloader1',comp_model_id_1);
	getVariantByModelCompareSet1('ajaxloadervariant1',comp_product_id_1);
	getModelByBrandCompareSet2('ajaxloader2',comp_model_id_2);
	getVariantByModelCompareSet2('ajaxloadervariant2',comp_product_id_2);
	
	return true;
}

function deleteProductCompetitor(compare_id){
	   	document.getElementById('actiontype').value = 'Delete';
		document.getElementById('compare_id').value = compare_id;
    	var answer = confirm ("Are you sure.Want to delete compare set?")
    	if (answer){
         	document.product_manage.submit();
         	return true;
     	}
	return false;
}


function getModelByBrandCompareSet1(ajaxloaderid,product_name_id){
	var brand_id = document.getElementById('select_comp_brand_id_1').value;
	if(brand_id == '' ||  brand_id == 0){return false;}
	var category_id = document.getElementById('selected_category_id').value;
	if(category_id == '' ||  category_id == 0){return false;}
	document.getElementById(ajaxloaderid).style.display = "block";
	var url = admin_web_url+'ajax/select_most_popular_model.php';
	var html = $.ajax({ url: url, data: 'category_id='+category_id+'&brand_id='+brand_id+'&product_name_id='+product_name_id, success: function(data){ document.getElementById(ajaxloaderid).style.display = "none";}, async: false}).responseText;
	var table = document.getElementById("Update");
	var rowCount = 2;
	var rowId='product_row_id_'+rowCount;
	if(document.getElementById(rowId)){
		removeTr(rowId);
	}
	var row = table.insertRow(rowCount);
	row.id = rowId;
	var product_name = row.insertCell(0);
   	product_name.innerHTML = 'Compare Model Name 1';
	var product_name_value = row.insertCell(1);
	product_name_value.colSpan = 10;
	product_name_value.innerHTML = html;
	return true;
}
function getModelByBrandCompareSet2(ajaxloaderid,product_name_id){
	var brand_id = document.getElementById('select_comp_brand_id_2').value;
	if(brand_id == '' ||  brand_id == 0){return false;}
	var category_id = document.getElementById('selected_category_id').value;
	if(category_id == '' ||  category_id == 0){return false;}
	document.getElementById(ajaxloaderid).style.display = "block";
	var url = admin_web_url+'ajax/select_most_popular_model_1.php';
	var html = $.ajax({ url: url, data: 'category_id='+category_id+'&brand_id='+brand_id+'&product_name_id='+product_name_id, success: function(data){ document.getElementById(ajaxloaderid).style.display = "none";}, async: false}).responseText;
	var table = document.getElementById("Update");
	var rowCount = 5;
	var rowId='product_row_id_'+rowCount;
	if(document.getElementById(rowId)){
		removeTr(rowId);
	}
	var row = table.insertRow(rowCount);
	row.id = rowId;
	var product_name = row.insertCell(0);
   	product_name.innerHTML = 'Compare Model Name 2';
	var product_name_value = row.insertCell(1);
	product_name_value.colSpan = 10;
	product_name_value.innerHTML = html;
	return true;
}

function getVariantByModelCompareSet1(ajaxloaderid,product_id){
	var brand_id = document.getElementById('select_comp_brand_id_1').value;
	if(brand_id == '' ||  brand_id == 0){return false;}
	var product_name_id = document.getElementById('select_comp_model_id_1').value;
	if(product_name_id == '' ||  product_name_id == 0){return false;}
	var category_id = document.getElementById('selected_category_id').value;
	if(category_id == '' ||  category_id == 0){return false;}
	document.getElementById(ajaxloaderid).style.display = "block";
	
	var url = admin_web_url+'ajax/select_most_popular_variant.php';
	var html = $.ajax({ url: url, data: 'category_id='+category_id+'&brand_id='+brand_id+'&product_id='+product_id+'&product_name_id='+product_name_id, success: function(data){ document.getElementById(ajaxloaderid).style.display = "none";}, async: false}).responseText;
	var table = document.getElementById("Update");
	var rowCount = 3;
	var rowId='product_row_id_'+rowCount;
	if(document.getElementById(rowId)){
		removeTr(rowId);
	}
	var row = table.insertRow(rowCount);
	row.id = rowId;
	var product_name = row.insertCell(0);
   	product_name.innerHTML = 'Compare Product Name 1';
	var product_name_value = row.insertCell(1);
	product_name_value.colSpan = 10;
	product_name_value.innerHTML = html;
	return true;
}
function getVariantByModelCompareSet2(ajaxloaderid,product_id){
	var brand_id = document.getElementById('select_comp_brand_id_2').value;
	if(brand_id == '' ||  brand_id == 0){return false;}
	var product_name_id = document.getElementById('select_comp_model_id_2').value;
	if(product_name_id == '' ||  product_name_id == 0){return false;}
	var category_id = document.getElementById('selected_category_id').value;
	if(category_id == '' ||  category_id == 0){return false;}
	document.getElementById(ajaxloaderid).style.display = "block";
	var url = admin_web_url+'ajax/select_most_popular_variant_1.php';
	var html = $.ajax({ url: url, data: 'category_id='+category_id+'&brand_id='+brand_id+'&product_id='+product_id+'&product_name_id='+product_name_id, success: function(data){ document.getElementById(ajaxloaderid).style.display = "none";}, async: false}).responseText;
	var table = document.getElementById("Update");
	var rowCount = 6;
	var rowId='product_row_id_'+rowCount;
	if(document.getElementById(rowId)){
		removeTr(rowId);
	}
	var row = table.insertRow(rowCount);
	row.id = rowId;
	var product_name = row.insertCell(0);
   	product_name.innerHTML = 'Compare Product Name 2';
	var product_name_value = row.insertCell(1);
	product_name_value.colSpan = 10;
	product_name_value.innerHTML = html;
	return true;
}

function validateProduct(){
	if(document.getElementById('select_comp_brand_id_1').value == ''){
		alert("Please select first brand for compare set product.");
		return false;
	}
	if(document.getElementById('select_comp_brand_id_1').value != ''){
		if(document.getElementById('select_comp_model_id_1').value == ''){
			alert("Please select first model for compare set.");
			return false;
		}
		if(document.getElementById('select_comp_model_id_1').value != ''){
			if(document.getElementById('comp_product_id_1').value == ''){
				alert("Please select first variant for compare set.");
				return false;
			}
		}
	}
	if(document.getElementById('select_comp_brand_id_2').value == ''){
		alert("Please select second brand for compare set product");
		return false;
	}
	
	if(document.getElementById('select_comp_brand_id_2').value != ''){
		if(document.getElementById('select_comp_model_id_2').value == ''){
			alert("Please select second model for compare set.");
			return false;
		}
		if(document.getElementById('select_comp_model_id_2').value != ''){
			if(document.getElementById('comp_product_id_2').value == ''){
				alert("Please select second variant for compare set.");
				return false;
			}
		}
	}
		
	
	return true;
}

function getProductByBrand(ajaxloaderid,productid){
	var brand_id = document.getElementById('select_brand_id').value;
	if(brand_id == '' ||  brand_id == 0){return false;}
	var category_id = document.getElementById('selected_category_id').value;
	if(category_id == '' ||  category_id == 0){return false;}
	document.getElementById(ajaxloaderid).style.display = "block";
	var url = admin_web_url+'ajax/select_product.php';
	var html = $.ajax({ url: url, data: 'category_id='+category_id+'&brand_id='+brand_id+'&productid='+productid, success: function(data){ document.getElementById(ajaxloaderid).style.display = "none";}, async: false}).responseText;
	var table = document.getElementById("Update");
	var rowCount = 1;
	var rowId='product_row_id_'+rowCount;
	if(document.getElementById(rowId)){
		removeTr(rowId);
	}
	var row = table.insertRow(rowCount);
	row.id = rowId;
	var product_name = row.insertCell(0);
   	product_name.innerHTML = 'Product Name';
	var product_name_value = row.insertCell(1);
	product_name_value.colSpan = 10;
	product_name_value.innerHTML = html;
	return true;
}

function removeTr(rowId){
    var row = document.getElementById(rowId);
	if(row.parentElement){
	       	row.parentElement.removeChild(row);
	}else if(row.parentNode){
		row.parentNode.removeChild(row);
	}
	
	return false;
}
function remove_product_row(rowCount){	
	if(rowCount == 0){return false;}
	if(document.getElementById('product_remove_linkrow_id_'+rowCount)){
		removeTr('product_remove_linkrow_id_'+rowCount);
    	}
	if(document.getElementById('product_status_row_id_'+rowCount)){
		removeTr('product_status_row_id_'+rowCount);
	}
	if(document.getElementById('product_style_row_id_'+rowCount)){
		removeTr('product_style_row_id_'+rowCount);
	}
	if(document.getElementById('product_style_row_id_'+rowCount)){
		removeTr('product_style_row_id_'+rowCount);
	}
	if(document.getElementById('product_desc_row_id_'+rowCount)){
		removeTr('product_desc_row_id_'+rowCount);
	}
	if(document.getElementById('product_group_row_id_'+rowCount)){
		removeTr('product_group_row_id_'+rowCount);
	}
	if(document.getElementById('product_name_row_id_'+rowCount)){
		removeTr('product_name_row_id_'+rowCount);
	}
	return false;
}

function getUploadData (sFrm,sTitle,sId,sPath,mType,sImageCat){
	window.open('get_upload.php?rfrm='+sFrm+'&rtitle='+sTitle+'&rpath='+sPath+'&rid='+sId+'&rtype='+mType+'&rimgcat='+sImageCat,'mywindow','width=600,height=300,left=300,top=300');
}
function getUploadedDataList (sFrm,sTitle,sId,sPath,mType,sImageCat){
	window.open('search_store.php?rfrm='+sFrm+'&rtitle='+sTitle+'&rpath='+sPath+'&rid='+sId+'&rtype='+mType+'&rimgcat='+sImageCat,'mywindow','width=600,height=400,left=300,top=300');
}
