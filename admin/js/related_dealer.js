function getRelatedDealerDashboard(divid,ajaxloaderid,category_id,startlimit,cnt,view_section_id){
        if(category_id == ''){
                var category_id = document.getElementById('selected_category_id').value;

                if(isCategorySelected() == false){
                        alert("Please select the category.");
                        return false;
                }
                if(isLastLvlCategory() == false){
                        alert("Please select last level category.");
                        return false;
                }
        }

        //alert(divid+','+ajaxloaderid+','+category_id);
        document.getElementById(ajaxloaderid).style.display = "block";
	if(divid == ""){ return false; }
	if(view_section_id == ''){
                if(document.getElementById('view_section_id')){
                        view_section_id = document.getElementById('view_section_id').value;
                }
        }
        var url = admin_web_url+'ajax/related_dealer_dashboard.php';
        //alert(url+'?catid='+category_id+'&startlimit='+startlimit+'&cnt='+cnt+'&view_section_id='+view_section_id);return false;
        $.ajax({
        	url: url,
                data: 'catid='+category_id+'&startlimit='+startlimit+'&cnt='+cnt+'&view_section_id='+view_section_id,
                success: function(data){
                	//alert(data);
		        document.getElementById(divid).innerHTML = data;
	        	document.getElementById(divid).style.display="block";
                	document.getElementById(ajaxloaderid).style.display = "none";
		},
	        async:false
        });
        return true;
}

function getRelatedDealerDashboardByType(divid,ajaxloaderid,category_id,startlimit,cnt){
        if(category_id == ''){
                var category_id = document.getElementById('selected_category_id').value;

                if(isCategorySelected() == false){
                        alert("Please select the category.");
                        return false;
                }
                if(isLastLvlCategory() == false){
                        alert("Please select last level category.");
                        return false;
                }
        }
	if(document.getElementById('view_section_id')!="undefined"){
                var view_section_id = document.getElementById('view_section_id').value;
        }
        document.getElementById(ajaxloaderid).style.display = "block";
	if(divid == ""){ return false; }
        var url = admin_web_url+'ajax/related_dealer_dashboard.php';
        //alert(url+'?catid='+category_id+'&startlimit='+startlimit+'&cnt='+cnt+'&view_section_id='+view_section_id);
        $.ajax({
		url: url,
                data: 'catid='+category_id+'&startlimit='+startlimit+'&cnt='+cnt+'&view_section_id='+view_section_id,
                success: function(data){
                	//alert(data);
	                document.getElementById(divid).innerHTML = data;
        	        document.getElementById(divid).style.display="block";
                	document.getElementById(ajaxloaderid).style.display = "none";
    		},
		async:false
        });
        return true;
}

function validateProduct(){
	if(document.getElementById('select_section_id')!="undefined"){
                if((document.getElementById('select_section_id').value) == "0"){
                        alert("Please select dealer section.");
                        return false;
                }
        }
       	if(document.getElementById('select_dealer_id').value == "0"){
                alert("Please select the dealer from the dealer list.");
                return false;
        }
	if(document.getElementById('view_section_id')!="undefined"){
                var view_section_id = document.getElementById('view_section_id').value;
                document.getElementById('hd_view_section_id').value=view_section_id;
        }
        if(document.getElementById('actiontype')){
                if(document.getElementById('actiontype').value == ""){
                        document.getElementById('actiontype').value = 'Insert';
                }
        } 
        return true;
}

function updateRelatedDealer(divid,ajaxloaderid,dealer_id,category_id){
        if(category_id == ''){
        	var category_id = document.getElementById('selected_category_id').value;
                if(isCategorySelected() == false){
                        alert("Please select the category.");
                        return false;
                }
                if(isLastLvlCategory() == false){
                        alert("Please select last level category.");
                        return false;
                }
        }
     	if(document.getElementById('view_section_id')!="undefined"){
                var view_section_id = document.getElementById('view_section_id').value;
                document.getElementById('hd_view_section_id').value=view_section_id;
        } 
        //alert(divid+','+ajaxloaderid+','+category_id);
        document.getElementById(ajaxloaderid).style.display = "block";
	if(divid == ""){ return false; }
        var url = admin_web_url+'ajax/related_dealer_dashboard.php';
	//alert(url+'?act=update&dealer_id='+dealer_id+'&catid='+category_id+'&view_section_id='+view_section_id);
	$.ajax({
        	url: url,
                data: 'act=update&dealer_id='+dealer_id+'&catid='+category_id+'&view_section_id='+view_section_id,
                success: function(data){
                //alert(data);
                document.getElementById(divid).innerHTML = data;
                document.getElementById(divid).style.display="block";
                document.getElementById(ajaxloaderid).style.display = "none";
		
            },
            async:false
        });
	document.getElementById('actiontype').value = 'Update';
        return true;
}

function deleteRelatedDealer(dealer_id){
      	if(document.getElementById('view_section_id')!="undefined"){
                var view_section_id = document.getElementById('view_section_id').value;
                document.getElementById('hd_view_section_id').value=view_section_id;
        } 
        document.getElementById('actiontype').value = 'Delete';
	document.getElementById('hd_dealer_id').value = dealer_id;
        var answer = confirm ("Are you sure.Want to delete this dealer?")
        if (answer){
                document.product_manage.submit();
                return true;
        }
        return false;
}
	
