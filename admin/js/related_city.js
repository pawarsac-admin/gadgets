function getRelatedCityDashboard(divid,ajaxloaderid,category_id,startlimit,cnt,city_id){
	if(category_id == ''){
                var category_id = document.getElementById('selected_category_id').value;

                if(isCategorySelected() == false){
                        alert("Please select the category.");
                        return false;
                }
                if(isLastLvlCategory() == false){
                        alert("Please select last level category.");
                        return false;
                }
        }
        //alert(divid+','+ajaxloaderid+','+category_id);
        document.getElementById(ajaxloaderid).style.display = "block";
        if(divid == ""){ return false; }

	if(city_id == ''){
		if(document.getElementById("city_id")){
			city_id = document.getElementById("city_id").value;
		}
	}
	
        var url = admin_web_url+'ajax/related_city_dashboard.php';
        //alert(url+'?catid='+category_id+'&startlimit='+startlimit+'&cnt='+cnt+'&selected_city_id='+city_id);
        $.ajax({
                url: url,
                data: 'catid='+category_id+'&startlimit='+startlimit+'&cnt='+cnt+'&selected_city_id='+city_id,
                success: function(data){
                        //alert(data);
                        document.getElementById(divid).innerHTML = data;
                        document.getElementById(divid).style.display="block";
                        document.getElementById(ajaxloaderid).style.display = "none";
                },
                async:false
        });
        return true;
}

function updateRelatedCity(divid,ajaxloaderid,related_id,city_id,related_city_id,category_id,selected_city_id){
	var startlimit = "";
	var cnt = "";
	if(category_id == ''){
                var category_id = document.getElementById('selected_category_id').value;
                if(category_id == ''){
                        alert("Please select category.");
                        return false;
                }
        }
        document.getElementById(ajaxloaderid).style.display = "block";
        if(divid == ""){ return false; }
        var url = admin_web_url+'ajax/related_city_dashboard.php';
	if(selected_city_id == ""){selected_city_id = city_id;}
        //alert(url+'?act=update&related_id='+related_id+'&catid='+category_id+'&startlimit='+startlimit+'&cnt='+cnt+'&selected_city_id='+selected_city_id);
        $.ajax({
                url: url,
                data: 'act=update&related_id='+related_id+'&catid='+category_id+'&startlimit='+startlimit+'&cnt='+cnt+'&selected_city_id='+selected_city_id,

                success: function(data){
                //alert(data);
                document.getElementById(divid).innerHTML = data;
                document.getElementById(divid).style.display="block";
                document.getElementById(ajaxloaderid).style.display = "none";
            },
            async:false
        });
	if(document.getElementById('related_id')){
                document.getElementById('related_id').value = related_id;
        }
        if(document.getElementById('actiontype')){
                document.getElementById('actiontype').value = 'Update';
        }
        return true;
}

function deleteRelatedCity(related_id,city_id){
	document.getElementById('related_id').value = related_id;
	document.getElementById('selected_city_id').value = city_id;
        var answer = confirm ("Are you sure.Want to delete this related city?")
	if(document.getElementById('actiontype')){
		document.getElementById('actiontype').value = 'Delete';
        }
        if (answer){
                document.product_manage.submit();
                return true;
        }
        return false;
}

function validateProduct(){
	if(document.getElementById('actiontype')){
		if(document.getElementById('actiontype').value != 'Update'){
	                document.getElementById('actiontype').value = 'Insert';
		}
        }
	if(document.getElementById('select_city_id').value == ''){
                alert("Please Select City.");
                return false;
        }
       	if((document.getElementById('select_related_city_id').value == "") || (document.getElementById('select_related_city_id').value == "0")){
        	alert("Please Select Related City");
	        return false;
 	}	
        return true;
}
function getModelByBrand(ajaxloaderid,product_name_id,divid){
        if(document.getElementById('select_brand_id')){
                var brand_id = document.getElementById('select_brand_id').value;
        }
        if(brand_id == '' ||  brand_id == 0){
		if(document.getElementById(divid)){
			document.getElementById(divid).innerHTML = "<option value=''>---Select Brand---</option>";
		}
		return false;
	}


        var category_id = document.getElementById('selected_category_id').value;
        if(category_id == '' ||  category_id == 0){return false;}

        //if(productid!=''){}

        document.getElementById(ajaxloaderid).style.display = "block";
        var url = admin_web_url+'ajax/select_popular_model.php';
        var html = $.ajax({url: url,data: 'category_id='+category_id+'&brand_id='+brand_id+'&product_name_id='+product_name_id, success: function(data)
{document.getElementById(ajaxloaderid).style.display = "none";},async: false}).responseText;
	if(document.getElementById(divid)){
		document.getElementById(divid).innerHTML = html;
	}
        return true;
}

