function getPopularFeatureCarsDashboard(divid,ajaxloaderid,category_id,startlimit,cnt,pivot_group,pivot_id){
	if(category_id == ''){
                var category_id = document.getElementById('selected_category_id').value;

                if(isCategorySelected() == false){
                        alert("Please select the category.");
                        return false;
                }
                if(isLastLvlCategory() == false){
                        alert("Please select last level category.");
                        return false;
                }
        }
        //alert(divid+','+ajaxloaderid+','+category_id);
        document.getElementById(ajaxloaderid).style.display = "block";
        if(divid == ""){ return false; }

	if(pivot_group == ''){
		if(document.getElementById("pivot_group")){
			pivot_group = document.getElementById("pivot_group").value;
		}
	}
	if(pivot_id == ''){
		if(document.getElementById("pivot_id")){
        	        pivot_id = document.getElementById("pivot_id").value;
        	}
	}
        var url = admin_web_url+'ajax/popular_feature_cars_dashboard.php';
        //alert(url+'?catid='+category_id+'&startlimit='+startlimit+'&cnt='+cnt+'&pivot_group='+pivot_group+'&pivot_id='+pivot_id);
        $.ajax({
                url: url,
                data: 'catid='+category_id+'&startlimit='+startlimit+'&cnt='+cnt+'&pivot_group='+pivot_group+'&pivot_id='+pivot_id,
                success: function(data){
                        //alert(data);
                        document.getElementById(divid).innerHTML = data;
                        document.getElementById(divid).style.display="block";
                        document.getElementById(ajaxloaderid).style.display = "none";
                },
                async:false
        });
        return true;
}
function fillPivots(pivot_group_id,fieldid,selected_pivot_id){
	document.getElementById("select_product_id").innerHTML = "<option value='0'>---Select Product---</option>";
	var category_id="";
	if(category_id == ''){
                var category_id = document.getElementById('selected_category_id').value;

                if(isCategorySelected() == false){
                        alert("Please select the category.");
                        return false;
                }
                if(isLastLvlCategory() == false){
                        alert("Please select last level category.");
                        return false;
                }
        }
	var pivot_group = "";
        if(document.getElementById(pivot_group_id)){
                pivot_group = document.getElementById(pivot_group_id).value;
        }
	if(pivot_group != "0"){	
		var url = admin_web_url+'ajax/get_pivots.php';
	        //alert(url+'?act=fill_pivots&catid='+category_id+'&pivot_group='+pivot_group+'&selected_pivot_id='+selected_pivot_id);
        	$.ajax({
                	url: url,
	                data: 'act=fill_pivots&catid='+category_id+'&pivot_group='+pivot_group+'&pivot_id='+selected_pivot_id,
        	        success: function(data){
                	        //alert(data);
				if(data == "<option value='0' selected>---Select Pivot---</option>"){
					if(fieldid == "select_pivot_id"){
                 			       document.getElementById("select_product_id").innerHTML = "<option value='0'>---Select Product---</option>";
			                }
				}
                        	document.getElementById(fieldid).innerHTML = data;
	                },
        	        async:false
        	});
	}else{
		document.getElementById(fieldid).innerHTML = "<option value='0'>---Select Pivot---</option>";
		if(fieldid == "select_pivot_id"){
			document.getElementById("select_product_id").innerHTML = "<option value='0'>---Select Product---</option>";
		}
	}
        return true;
}

function fillPoductName(pivot_group_id, pivot_name, fieldid){
	var category_id="";
	if(category_id == ''){
                var category_id = document.getElementById('selected_category_id').value;

                if(isCategorySelected() == false){
                        alert("Please select the category.");
                        return false;
                }
                if(isLastLvlCategory() == false){
                        alert("Please select last level category.");
                        return false;
                }
        }
        var pivot_group = "";
        if(document.getElementById(pivot_group_id)){
                pivot_group = document.getElementById(pivot_group_id).value;
        }
	var pivot_id = "";
	if(document.getElementById(pivot_name)){
                pivot_id = document.getElementById(pivot_name).value;
        }
	if(pivot_group != "0" && pivot_id != "0"){
                var url = admin_web_url+'ajax/get_product_name.php';
                //alert(url+'?act=fill_productname&catid='+category_id+'&pivot_group='+pivot_group+'&pivot_id='+pivot_id);
                $.ajax({
                        url: url,
                        data: 'act=fill_productname&catid='+category_id+'&pivot_group='+pivot_group+'&pivot_id='+pivot_id,
                        success: function(data){
                                //alert(data);
                                document.getElementById(fieldid).innerHTML = data;
                        },
                        async:false
                });
        }else{
                document.getElementById(fieldid).innerHTML = "<option value='0' selected>---Select Product---</option>";
        }
        return true;
	
}

function updatePopularFeatureCars(divid,ajaxloaderid,popular_feature_id,category_id,selected_pivot_group,selected_pivot_id,selected_product_id){
	var startlimit = "";
	var cnt = "";
	if(category_id == ''){
                var category_id = document.getElementById('selected_category_id').value;
                if(category_id == ''){
                        alert("Please select category.");
                        return false;
                }
        }
        document.getElementById(ajaxloaderid).style.display = "block";
        if(divid == ""){ return false; }
        var url = admin_web_url+'ajax/popular_feature_cars_dashboard.php';
        //alert(url+'?act=update&popular_feature_id='+popular_feature_id+'&catid='+category_id+'&startlimit='+startlimit+'&cnt='+cnt+'&pivot_group='+selected_pivot_group+'&pivot_id='+selected_pivot_id+'&product_id='+selected_product_id);
        $.ajax({
                url: url,
                data: 'act=update&popular_feature_id='+popular_feature_id+'&catid='+category_id+'&startlimit='+startlimit+'&cnt='+cnt+'&pivot_group='+selected_pivot_group+'&pivot_id='+selected_pivot_id+'&product_id='+selected_product_id,

                success: function(data){
                //alert(data);
                document.getElementById(divid).innerHTML = data;
                document.getElementById(divid).style.display="block";
                document.getElementById(ajaxloaderid).style.display = "none";
            },
            async:false
        });
	if(document.getElementById('popular_feature_id')){
                document.getElementById('popular_feature_id').value = popular_feature_id;
        }
        if(document.getElementById('actiontype')){
                document.getElementById('actiontype').value = 'Update';
        }
        return true;
}

function deletePopularBrand(popular_feature_id,pivot_group,pivot_id){
	document.getElementById('popular_feature_id').value = popular_feature_id;
	document.getElementById('selected_pivot_group').value = pivot_group;
	document.getElementById('selected_pivot_id').value = pivot_id;
        var answer = confirm ("Are you sure.Want to delete this?")
	if(document.getElementById('actiontype')){
		document.getElementById('actiontype').value = 'Delete';
        }
        if (answer){
                document.product_manage.submit();
                return true;
        }
        return false;
}

function validateProduct(){
	if(document.getElementById('actiontype')){
		if(document.getElementById('actiontype').value != 'Update'){
	                document.getElementById('actiontype').value = 'Insert';
		}
        }
	if(document.getElementById('select_pivot_group').value == '0'){
                alert("Please Select Pivot group.");
                return false;
        }
	if(document.getElementById('select_pivot_id').value == '0'){
                alert("Please Select Pivot Name.");
                return false;
        }
	if(document.getElementById('select_product_id').value == '0'){
                alert("Please Select Product Name.");
                return false;
        }
        return true;
}

