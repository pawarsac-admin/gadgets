function getArticleSubTypesDashboard(divid,ajaxloaderid,category_id,startlimit,cnt,view_section_id){
	if(category_id == ''){
		var category_id = document.getElementById('selected_category_id').value;

		if(isCategorySelected() == false){
			alert("Please select the category.");
			return false;
		}
		if(isLastLvlCategory() == false){
			alert("Please select last level category.");
			return false;
		}
	}
	
	//alert(divid+','+ajaxloaderid+','+category_id);
	document.getElementById(ajaxloaderid).style.display = "block";
	if(divid == ""){ return false; }
	if(view_section_id == ''){
		if(document.getElementById('view_section_id')){
        	        view_section_id = document.getElementById('view_section_id').value;
	        }
	}
	var url = admin_web_url+'ajax/article_sub_type_dashboard.php';
	//alert(url+'?catid='+category_id+'&startlimit='+startlimit+'&cnt='+cnt+'&view_section_id='+view_section_id)
        $.ajax({
		url: url,
		data: 'catid='+category_id+'&startlimit='+startlimit+'&cnt='+cnt+'&view_section_id='+view_section_id,
		success: function(data){
		//alert(data);
                document.getElementById(divid).innerHTML = data;
                document.getElementById(divid).style.display="block";
                document.getElementById(ajaxloaderid).style.display = "none";
            },
            async:false
        });
	return true;
}
function updateArticleSubType(divid,ajaxloaderid,article_type_id,article_sub_type_id,category_id){

        if(category_id == ''){
                var category_id = document.getElementById('selected_category_id').value;
		if(category_id == ''){
	                alert("Please select category.");
        	        return false;
		}
        }
	var view_section_id = document.getElementById('view_section_id').value;
        document.getElementById(ajaxloaderid).style.display = "block";
	if(divid == ""){ return false; }
        var url = admin_web_url+'ajax/article_sub_type_dashboard.php';
	//alert(url+'?act=update&catid='+category_id+'&article_type_id='+article_type_id+'&article_sub_type_id='+article_sub_type_id+'&view_section_id='+view_section_id);
        $.ajax({
                url: url,
                data: 'act=update&catid='+category_id+'&article_type_id='+article_type_id+'&article_sub_type_id='+article_sub_type_id+'&view_section_id='+view_section_id,

                success: function(data){
                //alert(data);
                document.getElementById(divid).innerHTML = data;
                document.getElementById(divid).style.display="block";
                document.getElementById(ajaxloaderid).style.display = "none";
            },
            async:false
        });
	if(document.getElementById('view_section_id')){
                document.getElementById('hd_view_section_id').value = document.getElementById('view_section_id').value;
        }
	if(document.getElementById('hd_article_sub_type_id')){
                document.getElementById('hd_article_sub_type_id').value = article_sub_type_id;
        }
	if(document.getElementById('actiontype')){
                document.getElementById('actiontype').value = 'Update';
        }
        //alert(document.getElementById('actiontype').value);
        return true;
}
function deleteArticleSubType(article_sub_type_id,article_type_id){
       document.getElementById('actiontype').value = 'Delete';

        document.getElementById('hd_article_sub_type_id').value = article_sub_type_id;
        document.getElementById('hd_view_section_id').value = article_type_id;
        var answer = confirm ("Are you sure.Want to delete this article sub type?")
        if (answer){
                document.product_manage.submit();
                return true;
        }
        return false;
}

function validateArticleSubType(){
	if(document.getElementById('actiontype')){
		if(document.getElementById('actiontype').value == ""){
	                document.getElementById('actiontype').value = 'Insert';
		}
        }
	if(document.getElementById('select_section_id').value == '0'){
                alert("Please Select Article Type.");
                return false;
        }
	var article_sub_type_id = "";
	if(document.getElementById('select_article_sub_type')){
		article_sub_type_id = document.getElementById('select_article_sub_type').value;
	}
	if(article_sub_type_id == ""){
                alert("Please Enter Article Sub Type.");
                return false;
        }
	if(document.getElementById('view_section_id')){
                document.getElementById('hd_view_section_id').value = document.getElementById('view_section_id').value;
        }	
        return true;
}
