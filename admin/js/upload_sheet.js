function getStateByCountry(ajaxloaderid,stateid){
	var country_id = document.getElementById('select_country_id').value;
	if(country_id == '' ||  country_id == 0){return false;}
	document.getElementById(ajaxloaderid).style.display = "block";
	var url = admin_web_url+'ajax/select_state.php';
	//alert(url);
	var html1 = $.ajax({ 
						url: url,
						data: 'country_id='+country_id+'&state_id='+stateid,
						success:
						function(data){
							//alert(data);
							document.getElementById(ajaxloaderid).style.display = "none";
						},
						async: false
					}).responseText;
	var table = document.getElementById("Update");
	var rowCount = 1;
	var rowId='state_row_id_'+rowCount;
	if(document.getElementById(rowId)){
		removeTr(rowId);
	}
	var row = table.insertRow(rowCount);
	row.id = rowId;
	var state_name = row.insertCell(0);
	state_name.innerHTML = 'State Name';
	var state_name_value = row.insertCell(1);
	state_name_value.colSpan = 10;
	state_name_value.innerHTML = html1;
	return true;
}

function getCityByState(ajaxloaderid,cityid){
	var state_id = document.getElementById('select_state_id').value;
	if(state_id == '' ||  state_id == 0){return false;}
	document.getElementById(ajaxloaderid).style.display = "block";
	var url = admin_web_url+'ajax/select_city.php';
	//alert(url);
	var html = $.ajax({ 
						url: url,
						data: 'state_id='+state_id+'&city_id='+cityid,
						success:
						function(data){
							//alert(data);
							document.getElementById(ajaxloaderid).style.display = "none";
						},
						async: false
					}).responseText;
	var table = document.getElementById("Update");
	var rowCount = 2;
	var rowId='city_row_id_'+rowCount;
	if(document.getElementById(rowId)){
		removeTr(rowId);
	}
	var row = table.insertRow(rowCount);
	row.id = rowId;
	var city_name = row.insertCell(0);
	city_name.innerHTML = 'City Name';
	var city_name_value = row.insertCell(1);
	city_name_value.colSpan = 10;
	city_name_value.innerHTML = html;
	return true;
}


function city_details(ajaxloaderid){
	var state_id = document.getElementById('state_id').value;
	if(state_id == '' ||  state_id == 0){return false;}

	
	document.getElementById(ajaxloaderid).style.display = "block";
	var url = admin_web_url+'ajax/city_ajax.php';
	var html = $.ajax({ url: url, data: 'state_id='+state_id, success: function(data){ document.getElementById(ajaxloaderid).style.display = "none";}, async: false}).responseText;
	var table = document.getElementById("Update");
	var rowCount = 3;	
	var row = table.insertRow(rowCount);
	row.id = 'city_row_id_'+rowCount;

	var city_name = row.insertCell(0);

    	city_name.innerHTML = 'Show room city'+rowCount;

	var city_name_value = row.insertCell(1);
	city_name_value.colSpan = 10;
	city_name_value.innerHTML = html;
	return true;
}
