function updateAdminReply(user_profile_id){
	document.getElementById('hd_user_profile_id').value=user_profile_id;
	document.getElementById('action').value="updateadminreply";
	document.product_manage.submit();
        return true;
}

function updateUserStatus(user_profile_id, status_val){
	document.getElementById('hd_user_profile_id').value=user_profile_id;
        document.getElementById('hd_update_status').value=status_val;
        document.getElementById('action').value="updateuserstatus";
        document.product_manage.submit();
        return true;
}

function getUserInfo(){
	var stat_val = document.getElementById('user_stat').value;
	if(stat_val == ""){
		alert("Please Select Status.");
		return false;
	}
        document.getElementById('hd_status').value=stat_val;

	document.product_manage.submit();
        return true;
}


function getModelByBrand(iModelId,surl,divname,param){
        var iBrndId=document.getElementById('select_brand_id').value;
        //alert(iBrndId);
        if(iBrndId == ""){
                $('#Model').empty().append('<option value="">--Select Model--</option>');
        	$('#Variant').empty().append('<option value="">--Select Variant--</option>');
                return false; 
        }
        var str="action=model&brand_id="+iBrndId+"&product_name_id="+iModelId+"&Rand="+Math.random();
        $('#ModelVariant').empty().append('<option value="">--Select Variant--</option>');
        //alert(siteURL+surl+"?"+str);
        $.ajax({
		url: siteadminURL+"ajax/"+surl,
                data: str,
                success: function(data){
	                $('#Model').empty().append(data);
		},
            	async:false
        });
        $('#Variant').empty().append('<option value="">All Variants</option>');
}

function getVariantByBrandModel(product_id,surl,divname,param){
        var iBrndId=document.getElementById('select_brand_id').value;
        var iModelId=document.getElementById('Model').value;
	if(iModelId == ""){
                $('#Variant').empty().append('<option value="">--Select Variant--</option>');
                return false;
        }
        //alert(iBrndId);
        var str="action=model&brand_id="+iBrndId+"&product_name_id="+iModelId+"&product_id="+product_id+"&Rand="+Math.random();
        $.ajax({
            url: siteadminURL+"ajax/"+surl,
            data: str,
            success: function(data){
            	$('#Variant').empty().append(data);
            },
            async:false
        });
}


function submitResearchForm(){
	var iBrndId=document.getElementById('select_brand_id').value;
        var iModelId=document.getElementById('Model').value;
	var iVariantId=document.getElementById('Variant').value;

	/*if(iBrndId == ""){
		alert("Please Select Brand.");
		return false;
	}*/	

	document.product_manage.submit();
        return true;
}

function sArticlePagination(page,startlimit,cnt,filename,divid,category_id,selected_status,selected_brand_id,selected_model_id,selected_variant_id,selected_city_id){

        if(category_id == ''){
                var category_id = document.getElementById('selected_category_id').value;

                if(isCategorySelected() == false){
                        alert("Please select the category.");
                        return false;
                }
                if(isLastLvlCategory() == false){
                        alert("Please select last level category.");
                        return false;
                }
        }

        if(divid == ""){ return false; }
                var url = admin_web_url+filename;

                $.ajax({
                        url: url,
                        data: 'catid='+category_id+'&page='+page+'&startlimit='+startlimit+'&cnt='+cnt+'&user_stat='+selected_status+'&select_brand_id='+selected_brand_id+'&Model='+selected_model_id+'&Variant='+selected_variant_id+'&select_city_id='+selected_city_id,
                        success: function(data){
                                //alert(data);
                document.getElementById(divid).innerHTML = data;
                document.getElementById(divid).style.display="block";
               },
            async:false
        });
      	if(selected_brand_id != ""){
			getModelByBrand('','get_model_detail.php','Model','');
		}
		if((selected_brand_id != "") && (selected_model_id != "")){
			
			document.getElementById("Model").value = selected_model_id;
			getVariantByBrandModel('','get_variant_detail.php','Variant','');
		}
		if(selected_variant_id != ""){
			document.getElementById("Variant").value = selected_variant_id;
		}
	return true;
}

