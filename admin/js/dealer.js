function getDealerDashboard(divid,ajaxloaderid,category_id,startlimit,cnt){
	if(category_id == ''){
		var category_id = document.getElementById('selected_category_id').value;
		if(isCategorySelected() == false){
			alert("Please select the category.");
			return false;
		}
		if(isLastLvlCategory() == false){
			alert("Please select last level category.");
			return false;
		}
	}
	//alert(divid+','+ajaxloaderid+','+category_id);
	document.getElementById(ajaxloaderid).style.display = "block";
	if(divid == ""){ return false; }
	var url = admin_web_url+'ajax/dealer_dashboard.php';
	//alert(url+'?catid='+category_id+'&startlimit='+startlimit+'&cnt='+cnt)
	$.ajax({
		url: url,
		data: 'catid='+category_id+'&startlimit='+startlimit+'&cnt='+cnt,
		success: function(data){
			//alert(data);
			document.getElementById(divid).innerHTML = data;
			document.getElementById(divid).style.display="block";
			document.getElementById(ajaxloaderid).style.display = "none";
		},
		async:false
	});
	return true;
}

function updateDealer(divid,ajaxloaderid,dealerid,categoryid,brandid,productid,countryid,stateid,cityid,startlimit,cnt){
	//alert(dealerid+"------"+categoryid+"------"+brandid+"------"+productid+"------"+countryid+"------"+stateid+"------"+cityid);
	if(category_id == ''){
		var category_id = document.getElementById('selected_category_id').value;
		if(isCategorySelected() == false){
			alert("Please select the category.");
			return false;
		}
		if(isLastLvlCategory() == false){
			alert("Please select last level category.");
			return false;
		}
	}
	document.getElementById(ajaxloaderid).style.display = "block";
    if(divid == ""){ return false; }
	
		var url = admin_web_url+'ajax/dealer_dashboard.php';
		//alert(url);
        $.ajax({
			url: url,
			data: 'act=update&dealerid='+dealerid+'&catid='+categoryid+'&bid='+brandid+'&pid='+productid+'&startlimit='+startlimit+'&cnt='+cnt,
				
			success: function(data){
				//alert(data);
                document.getElementById(divid).innerHTML = data;
                document.getElementById(divid).style.display="block";
                document.getElementById(ajaxloaderid).style.display = "none";
            },
            async:false
        });
		//alert(productid);
		//getProductByBrand('ajaxloaderbrand',productid);
		//alert('dddd');
		getStateByCountry('ajaxloadercountry',stateid);
		//alert('llll');
		getCityByState('ajaxloaderstate',cityid);
		document.getElementById('actiontype').value = 'Update';
	
	return true;
}


function deleteDealer(dealerid,prdid){
	   	document.getElementById('actiontype').value = 'Delete';
		document.getElementById('dealer_id').value = dealerid;
		document.getElementById('product_id').value = prdid;
    	var answer = confirm ("Are you sure.Want to delete dealer detail?")
    	if (answer){
         	document.dealer_manage.submit();
         	return true;
     	}
	return false;
}


function validateDealer(){
	if(document.getElementById('select_brand_id').value == ''){
		alert("Please select the brand.");
		return false;
	}
	if(document.getElementById('product_name').value == ''){
		alert("Please add the product");
		return false;
	}	
	var exshowroom_mrp = document.getElementById('product_mrp_ex_showroom').value;
	var state_id = document.getElementById('state_id').value;
	if(exshowroom_mrp != '' && state_id == ''){
		alert("Please select the state for ex-show room price.");
		return false;
	}
	var city_id = document.getElementById('city_id').value;
	if(state_id != '' && city_id == ''){
		alert("Please select the city for ex-show room price.");
		return false;
	}
	return true;
}


function getProductByBrand(ajaxloaderid,productid){
	var brand_id = document.getElementById('select_brand_id').value;
	if(brand_id == '' ||  brand_id == 0){return false;}
	var category_id = document.getElementById('selected_category_id').value;
	if(category_id == '' ||  category_id == 0){return false;}
	document.getElementById(ajaxloaderid).style.display = "block";
	
	var url = admin_web_url+'ajax/select_product.php';
	//alert(url);
	var html = $.ajax({ 
						url: url,
						data: 'category_id='+category_id+'&brand_id='+brand_id+'&productid='+productid,
						success:
						function(data){
							//alert(data);
							document.getElementById(ajaxloaderid).style.display = "none";
						},
						async: false
					}).responseText;
	var table = document.getElementById("Update");
	//alert(productid);
	if(productid!=0){	var rowCount = 7;}else{var rowCount = 9;}
	var rowId='product_row_id_'+rowCount;
	if(document.getElementById(rowId)){
		removeTr(rowId);
	}
	var row = table.insertRow(rowCount);
	row.id = rowId;
	var product_name = row.insertCell(0);
	product_name.innerHTML = 'Product Name';
	var product_name_value = row.insertCell(1);
	product_name_value.colSpan = 10;
	//alert(html);
	product_name_value.innerHTML = html;
	return true;
}


function getStateByCountry(ajaxloaderid,stateid){
	var country_id = document.getElementById('select_country_id').value;
	if(country_id == '' ||  country_id == 0){return false;}
	document.getElementById(ajaxloaderid).style.display = "block";
	var url = admin_web_url+'ajax/select_state.php';
	//alert(url);
	var html1 = $.ajax({ 
						url: url,
						data: 'country_id='+country_id+'&state_id='+stateid,
						success:
						function(data){
							//alert(data);
							document.getElementById(ajaxloaderid).style.display = "none";
						},
						async: false
					}).responseText;
	var table = document.getElementById("Update");
	var rowCount = 2;
	var rowId='state_row_id_'+rowCount;
	if(document.getElementById(rowId)){
		removeTr(rowId);
	}
	var row = table.insertRow(rowCount);
	row.id = rowId;
	var state_name = row.insertCell(0);
	state_name.innerHTML = 'State Name';
	var state_name_value = row.insertCell(1);
	state_name_value.colSpan = 10;
	state_name_value.innerHTML = html1;
	return true;
}

function getCityByState(ajaxloaderid,cityid){
	var state_id = document.getElementById('select_state_id').value;
	if(state_id == '' ||  state_id == 0){return false;}
	document.getElementById(ajaxloaderid).style.display = "block";
	var url = admin_web_url+'ajax/select_city.php';
	//alert(url);
	var html = $.ajax({ 
						url: url,
						data: 'state_id='+state_id+'&city_id='+cityid,
						success:
						function(data){
							//alert(data);
							document.getElementById(ajaxloaderid).style.display = "none";
						},
						async: false
					}).responseText;
	var table = document.getElementById("Update");
	var rowCount = 3;
	var rowId='city_row_id_'+rowCount;
	if(document.getElementById(rowId)){
		removeTr(rowId);
	}
	var row = table.insertRow(rowCount);
	row.id = rowId;
	var city_name = row.insertCell(0);
	city_name.innerHTML = 'City Name';
	var city_name_value = row.insertCell(1);
	city_name_value.colSpan = 10;
	city_name_value.innerHTML = html;
	return true;
}


function city_details(ajaxloaderid){
	var state_id = document.getElementById('state_id').value;
	if(state_id == '' ||  state_id == 0){return false;}

	
	document.getElementById(ajaxloaderid).style.display = "block";
	var url = admin_web_url+'ajax/city_ajax.php';
	var html = $.ajax({ url: url, data: 'state_id='+state_id, success: function(data){ document.getElementById(ajaxloaderid).style.display = "none";}, async: false}).responseText;
	var table = document.getElementById("Update");
	var rowCount = 6;	
	var row = table.insertRow(rowCount);
	row.id = 'city_row_id_'+rowCount;

	var city_name = row.insertCell(0);

    	city_name.innerHTML = 'Show room city'+rowCount;

	var city_name_value = row.insertCell(1);
	city_name_value.colSpan = 10;
	city_name_value.innerHTML = html;
	return true;
}
function removeTr(rowId){
    var row = document.getElementById(rowId);
	if(row.parentElement){
	       	row.parentElement.removeChild(row);
	}else if(row.parentNode){
		row.parentNode.removeChild(row);
	}
	
	return false;
}
function remove_product_row(rowCount){	
	if(rowCount == 0){return false;}
	if(document.getElementById('product_remove_linkrow_id_'+rowCount)){
		removeTr('product_remove_linkrow_id_'+rowCount);
    	}
	if(document.getElementById('product_status_row_id_'+rowCount)){
		removeTr('product_status_row_id_'+rowCount);
	}
	if(document.getElementById('product_style_row_id_'+rowCount)){
		removeTr('product_style_row_id_'+rowCount);
	}
	if(document.getElementById('product_style_row_id_'+rowCount)){
		removeTr('product_style_row_id_'+rowCount);
	}
	if(document.getElementById('product_desc_row_id_'+rowCount)){
		removeTr('product_desc_row_id_'+rowCount);
	}
	if(document.getElementById('product_group_row_id_'+rowCount)){
		removeTr('product_group_row_id_'+rowCount);
	}
	if(document.getElementById('product_name_row_id_'+rowCount)){
		removeTr('product_name_row_id_'+rowCount);
	}
	return false;
}


function getUploadData (sFrm,sTitle,sId,mType,sImageCat){
	window.open('get_upload.php?rfrm='+sFrm+'&rtitle='+sTitle+'&rid='+sId+'&rtype='+mType+'&rimgcat='+sImageCat,'mywindow','width=600,height=300,left=300,top=300');
}
function getUploadedDataList (sFrm,sTitle,sId,mType,sImageCat){
	window.open('search_store.php?rfrm='+sFrm+'&rtitle='+sTitle+'&rid='+sId+'&rtype='+mType+'&rimgcat='+sImageCat,'mywindow','width=600,height=400,left=300,top=300');
}
