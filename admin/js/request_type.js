function getRequestTypeDashboard(divid,ajaxloaderid,category_id,startlimit,cnt){
        if(category_id == ''){
                var category_id = document.getElementById('selected_category_id').value;

                if(isCategorySelected() == false){
                        alert("Please select the category.");
                        return false;
                }
                if(isLastLvlCategory() == false){
                        alert("Please select last level category.");
                        return false;
                }
        }

        //alert(divid+','+ajaxloaderid+','+category_id);
        document.getElementById(ajaxloaderid).style.display = "block";
        if(divid == ""){ return false; }
                var url = admin_web_url+'ajax/request_type_dashboard.php';
                //alert(url+'?catid='+category_id+'&startlimit='+startlimit+'&cnt='+cnt)
                $.ajax({
                        url: url,
                        data: 'catid='+category_id+'&startlimit='+startlimit+'&cnt='+cnt,
                        success: function(data){
                        //alert(data);
                        document.getElementById(divid).innerHTML = data;
                        document.getElementById(divid).style.display="block";
                        document.getElementById(ajaxloaderid).style.display = "none";
                },
                async:false
        });
        //tiny();
        return true;
}
function updateRequestType(divid,ajaxloaderid,request_type_id,category_id){

        //alert(divid+','+ajaxloaderid+','+request_type_id+','+category_id);
        document.getElementById(ajaxloaderid).style.display = "block";
    	if(divid == ""){ return false; }
       	var url = admin_web_url+'ajax/request_type_dashboard.php';
        $.ajax({
                        url: url,
                        data: 'act=update&request_type_id='+request_type_id+'&catid='+category_id+'&actiontype=Update',

                        success: function(data){
                                //alert(data);
                document.getElementById(divid).innerHTML = data;
                document.getElementById(divid).style.display="block";
                document.getElementById(ajaxloaderid).style.display = "none";
            },
            async:false
        });
	if(document.getElementById('actiontype')){
                document.getElementById('actiontype').value = 'Update';
        }
        //alert(document.getElementById('actiontype').value);
        return true;
}

function deleteRequestType(request_type_id){
	document.getElementById('actiontype').value = 'Delete';
        document.getElementById('request_type_id').value = request_type_id;
        var answer = confirm ("Are you sure.Want to delete this request type?")
        if (answer){
                document.product_manage.submit();
                return true;
        }
        return false;
}
function validateProduct(){
	if(document.getElementById('actiontype')){
                document.getElementById('actiontype').value = 'Insert';
        }
        var iTotalRowsCurrent = document.getElementById('display_rows').value;
        for(i=1;i<=iTotalRowsCurrent;i++){
        	if(document.getElementById('request_type_'+i).value == ''){
                	alert("Please enter request type "+i);
	                return false;
        	}
        }
        return true;
}
