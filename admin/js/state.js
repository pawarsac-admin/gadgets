function updateState(stateid,statename,statestatus){
	statename = statename.replace(/%26/,"&");
	statename = statename.replace(/&#039;/,"'");
	statename = statename.replace(/%2C/,",");
	statename = statename.replace(/%2F/,"/");
	if(statestatus=='Active'){
		document.getElementById('state_status').value = 1;
	}else{
		document.getElementById('state_status').value = 0;
	}
	
	document.getElementById('state_name').value = statename;
	document.getElementById('state_id').value = stateid;
	document.getElementById('actiontype').value = 'Update';
	return false;
}
function deleteState(stateid,statename){
	statename = statename.replace(/%26/,"&");
	statename = statename.replace(/&#039;/,"'");
	statename = statename.replace(/%2C/,",");
	statename = statename.replace(/%2F/,"/");
	document.getElementById('state_id').value = stateid;
	document.getElementById('actiontype').value = 'Delete';
	var answer = confirm ("Are you sure.Want to delete  state '"+statename+"'?")
    	if (answer){
         	document.state_action.submit();
         	return true;
     	}
	return false;
}
function validateState(){
	if(document.getElementById('state_name').value == ''){
		alert("Please add the state");
		document.getElementById('state_name').focus();
		return false;
	}
	return true;
}