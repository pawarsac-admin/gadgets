function getBrandDashboard(divid,ajaxloaderid,category_id,startlimit,cnt){
	if(category_id == ''){
		var category_id = document.getElementById('selected_category_id').value;
	
		if(isCategorySelected() == false){
			alert("Please select the category.");
			return false;
		}
		if(isLastLvlCategory() == false){
			alert("Please select last level category.");
			return false;
		}
	}
	//alert(divid+','+ajaxloaderid+','+category_id);
	document.getElementById(ajaxloaderid).style.display = "block";
        if(divid == ""){ return false; }
        var url = admin_web_url+'ajax/overall_rating_dashboard.php';
        $.ajax({
         url: url,
         data: 'catid='+category_id+'&startlimit='+startlimit+'&cnt='+cnt,
         success: function(data){
                         //alert(data);
                         document.getElementById(divid).innerHTML = data;
                         document.getElementById(divid).style.display="block";
                         document.getElementById(ajaxloaderid).style.display = "none";
                 },
                 async:false
        });
	return true;
}
function updateOverallRating(admin_rating_id,brand_id,product_info_id,productid,overallgrade,status,ajaxloaderid){
	document.getElementById('select_brand_id').value = brand_id;
	if(document.getElementById('select_brand_id')!='0'){
		if(product_info_id!=''){
			getModelByBrand(ajaxloaderid,product_info_id);
		}
		if(document.getElementById('select_model_id')!='0'){
			if(productid!=''){
				getVariantByModel(ajaxloaderid,productid);
			}
		}
	}
	document.getElementById('overallgrade').value = overallgrade;
	document.getElementById('overall_grade_status').value = status;
	document.getElementById('admin_rating_id').value = admin_rating_id;
	document.getElementById('actiontype').value = 'Update';
	return false;
}
function deleteBrand(admin_rating_id,product_name){
	product_name = product_name.replace(/%26/,"&");
	product_name = product_name.replace(/&#039;/,"'");
	product_name = product_name.replace(/%2C/,",");
	product_name = product_name.replace(/%2F/,"/");
	document.getElementById('admin_rating_id').value = admin_rating_id;
	document.getElementById('actiontype').value = 'Delete';
	var answer = confirm ("Are you sure.Want to delete overall rating for '"+product_name+"'?")
	if (answer){
		document.brand_action.submit();
		return true;
	}
	return false;
}
function validateRating(){
	if(isCategorySelected() == false){
			alert("Please select the category.");
			return false;
	}
	if(isLastLvlCategory() == false){
			alert("Please select last level category.");
			return false;
	}
	if(document.getElementById('select_brand_id').value == ''){
		alert("Please select brand");
		document.getElementById('select_brand_id').focus();
		return false;
	}
	if(document.getElementById('select_model_id').value == ''){
		alert("Please select model");
		document.getElementById('select_model_id').focus();
		return false;
	}
	/*
	if(document.getElementById('product_id').value == ''){
		alert("Please select product");
		document.getElementById('product_id').focus();
		return false;
	}
	*/
	if(document.getElementById('overallgrade').value == ''){
		alert("Please select grade");
		document.getElementById('overallgrade').focus();
		return false;
	}
	
	return true;
}
function getModelByBrand(ajaxloaderid,product_name_id){
	var brand_id = document.getElementById('select_brand_id').value;
	if(brand_id == '' ||  brand_id == 0){return false;}
	var category_id = document.getElementById('selected_category_id').value;
	if(category_id == '' ||  category_id == 0){return false;}
	document.getElementById(ajaxloaderid).style.display = "block";
	var url = admin_web_url+'ajax/select_model.php';
	var html = $.ajax({ url: url, data: 'category_id='+category_id+'&brand_id='+brand_id+'&product_name_id='+product_name_id, success: function(data){ document.getElementById(ajaxloaderid).style.display = "none";}, async: false}).responseText;
	var table = document.getElementById("Update");
	var rowCount = 1;
	var rowId='product_row_id_'+rowCount;
	
	if(document.getElementById(rowId)){
		removeTr(rowId);
	}
	

	var row = table.insertRow(rowCount);
	row.id = rowId;
	var product_name = row.insertCell(0);
	product_name.innerHTML = 'Model Name';
	var product_name_value = row.insertCell(1);
	product_name_value.colSpan = 10;
	product_name_value.innerHTML = html;

	var rowCount1 = 2;
	var rowId1='product_row_id_'+rowCount1;
	//alert(rowId1)
	if(document.getElementById(rowId1)){
	removeTr(rowId1);
	}
	return true;
}

function getVariantByModel(ajaxloaderid,product_id){
	var brand_id = document.getElementById('select_brand_id').value;
	if(brand_id == '' ||  brand_id == 0){return false;}
	var product_name_id = document.getElementById('select_model_id').value;
	if(product_name_id == '' ||  product_name_id == 0){return false;}
	var category_id = document.getElementById('selected_category_id').value;
	if(category_id == '' ||  category_id == 0){return false;}
	document.getElementById(ajaxloaderid).style.display = "block";
	var url = admin_web_url+'ajax/select_variant.php';
	var html = $.ajax({ url: url, data: 'category_id='+category_id+'&brand_id='+brand_id+'&product_id='+product_id+'&product_name_id='+product_name_id, success: function(data){ document.getElementById(ajaxloaderid).style.display = "none";}, async: false}).responseText;
	var table = document.getElementById("Update");
	var rowCount = 2;
	var rowId='product_row_id_'+rowCount;
	if(document.getElementById(rowId)){
	removeTr(rowId);
	}
	var row = table.insertRow(rowCount);
	row.id = rowId;
	var product_name = row.insertCell(0);
	product_name.innerHTML = 'Product Name';
	var product_name_value = row.insertCell(1);
	product_name_value.colSpan = 10;
	product_name_value.innerHTML = html;
	return true;
}