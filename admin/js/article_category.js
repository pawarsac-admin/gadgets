function getArticleCategoryDashboard(divid,ajaxloaderid,category_id,startlimit,cnt,view_section_id){
	if(category_id == ''){
		var category_id = document.getElementById('selected_category_id').value;

		if(isCategorySelected() == false){
			alert("Please select the category.");
			return false;
		}
		if(isLastLvlCategory() == false){
			alert("Please select last level category.");
			return false;
		}
	}
	
	//alert(divid+','+ajaxloaderid+','+category_id);
	document.getElementById(ajaxloaderid).style.display = "block";
	if(divid == ""){ return false; }
	if(view_section_id == ''){
		if(document.getElementById('view_section_id')){
        	        var view_section_id = document.getElementById('view_section_id').value;
	        }	
	}
	var url = admin_web_url+'ajax/article_category_dashboard.php';
	//alert(url+'?catid='+category_id+'&startlimit='+startlimit+'&cnt='+cnt+'&view_section_id='+view_section_id)
        $.ajax({
		url: url,
		data: 'catid='+category_id+'&startlimit='+startlimit+'&cnt='+cnt+'&view_section_id='+view_section_id,
		success: function(data){
		//alert(data);
                document.getElementById(divid).innerHTML = data;
                document.getElementById(divid).style.display="block";
                document.getElementById(ajaxloaderid).style.display = "none";
            },
            async:false
        });
	return true;
}
function getArticleList(selected_section_id,divid,category_id){
	if(category_id == ''){
                var category_id = document.getElementById('selected_category_id').value;

                if(isCategorySelected() == false){
                        alert("Please select the category.");
                        return false;
                }
                if(isLastLvlCategory() == false){
                        alert("Please select last level category.");
                        return false;
                }
        }

        if(divid == ""){ return false; }
	var selected_section = "";
        if(document.getElementById(selected_section_id)){
                selected_section = document.getElementById(selected_section_id).value;
        }
        if(selected_section != "0" && selected_section!=""){
		var url = admin_web_url+'ajax/get_article_list.php';
        	//alert(url+'?act=fill_articles&catid='+category_id+'&article_type_id='+selected_section)
	        $.ajax({
                	url: url,
        	        data: 'act=fill_articles&catid='+category_id+'&article_type_id='+selected_section,
	                success: function(data){
                	//alert(data);
        	        document.getElementById(divid).innerHTML = data;
	            },
        	    async:false
	        });
		var url1 = admin_web_url+'ajax/get_article_sub_type_list.php';
                //alert(url1+'?act=fill_articles&catid='+category_id+'&article_type_id='+selected_section)
                $.ajax({
                        url: url1,
                        data: 'act=fill_article_sub_type&catid='+category_id+'&article_type_id='+selected_section,
                        success: function(html){
                        //alert(data);
                        document.getElementById("select_article_sub_type_id").innerHTML = html;
                    },
                    async:false
                });
        }else{
                document.getElementById(divid).innerHTML = "<option value='0'>---Select Article---</option>";
                document.getElementById("select_article_sub_type_id").innerHTML = "<option value='0'>---Select Article Sub Type---</option>";
        }
        return true;
}
function updateArticleCategory(divid,ajaxloaderid,id,category_id,article_type_id,article_sub_type_id){

        if(category_id == ''){
                var category_id = document.getElementById('selected_category_id').value;
		if(category_id == ''){
	                alert("Please select category.");
        	        return false;
		}
        }
	var view_section_id = document.getElementById('view_section_id').value;
        document.getElementById(ajaxloaderid).style.display = "block";
	if(divid == ""){ return false; }
        var url = admin_web_url+'ajax/article_category_dashboard.php';
	//alert(url+'?act=update&id='+id+'&catid='+category_id+'&article_type_id='+article_type_id+'&article_sub_type_id='+article_sub_type_id+'&view_section_id='+view_section_id);
        $.ajax({
                url: url,
                data: 'act=update&id='+id+'&catid='+category_id+'&article_type_id='+article_type_id+'&article_sub_type_id='+article_sub_type_id+'&view_section_id='+view_section_id,

                success: function(data){
                //alert(data);
                document.getElementById(divid).innerHTML = data;
                document.getElementById(divid).style.display="block";
                document.getElementById(ajaxloaderid).style.display = "none";
            },
            async:false
        });
	if(document.getElementById('view_section_id')){
                document.getElementById('hd_view_section_id').value = document.getElementById('view_section_id').value;
        }
	if(document.getElementById('hd_id')){
                document.getElementById('hd_id').value = id;
        }
	if(document.getElementById('actiontype')){
                document.getElementById('actiontype').value = 'Update';
        }
        //alert(document.getElementById('actiontype').value);
        return true;
}
function deleteArticleCategory(id,article_type_id){
       document.getElementById('actiontype').value = 'Delete';

        document.getElementById('hd_id').value = id;
        document.getElementById('hd_view_section_id').value = article_type_id;
        var answer = confirm ("Are you sure.Want to delete this article category?")
        if (answer){
                document.product_manage.submit();
                return true;
        }
        return false;
}

function validateArticleCategory(){
	if(document.getElementById('actiontype')){
		if(document.getElementById('actiontype').value == ""){
	                document.getElementById('actiontype').value = 'Insert';
		}
        }
	if(document.getElementById('select_section_id').value == '0'){
                alert("Please Select Article Type.");
                return false;
        }
	if(document.getElementById("select_article_id").value == '0'){
                alert("Please Select Article.");
                return false;
        }
	var article_sub_type_id = "";
	if(document.getElementById('select_article_sub_type_id')){
		article_sub_type_id = document.getElementById('select_article_sub_type_id').value;
	}
	if(article_sub_type_id == '0' || article_sub_type_id == ""){
                alert("Please Select At Least One Article Sub Type.");
                return false;
        }
	if(document.getElementById('view_section_id')){
                document.getElementById('hd_view_section_id').value = document.getElementById('view_section_id').value;
        }	
        return true;
}

/*function soVideosPagination(page,startlimit,cnt,filename,divid,category_id,type_selected){
	if(category_id == ''){
		var category_id = document.getElementById('selected_category_id').value;

		if(isCategorySelected() == false){
			alert("Please select the category.");
			return false;
		}
		if(isLastLvlCategory() == false){
			alert("Please select last level category.");
			return false;
		}
	}
		
	if(divid == ""){ return false; }
		var url = admin_web_url+filename;
		
		$.ajax({
			url: url,
			data: 'catid='+category_id+'&page='+page+'&startlimit='+startlimit+'&cnt='+cnt+'&video_type_id='+type_selected,
			success: function(data){
				//alert(data);
                document.getElementById(divid).innerHTML = data;
                document.getElementById(divid).style.display="block";
               },
            async:false
        });
	
	return true;
}*/
