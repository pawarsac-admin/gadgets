function getPremiumDealerDashboard(divid,ajaxloaderid,category_id,startlimit,cnt){

        if(category_id == ''){
                var category_id = document.getElementById('selected_category_id').value;

                if(isCategorySelected() == false){
                        alert("Please select the category.");
                        return false;
                }
                if(isLastLvlCategory() == false){
                        alert("Please select last level category.");
                        return false;
                }
        }

        //alert(divid+','+ajaxloaderid+','+category_id);
        document.getElementById(ajaxloaderid).style.display = "block";
	if(divid == ""){ return false; }
        var url = admin_web_url+'ajax/premium_dealer_dashboard.php';
        //alert(url+'?catid='+category_id+'&startlimit='+startlimit+'&cnt='+cnt);return false;
        $.ajax({
        	url: url,
                data: 'catid='+category_id+'&startlimit='+startlimit+'&cnt='+cnt,
                success: function(data){
                	//alert(data);
		        document.getElementById(divid).innerHTML = data;
	        	document.getElementById(divid).style.display="block";
                	document.getElementById(ajaxloaderid).style.display = "none";
		},
	        async:false
        });
        return true;
}

function validateProduct(){
	if(document.getElementById('select_dealer_id').value == "0"){
		alert("Please select the dealer.");
		return false;
	}
	if(document.getElementById('actiontype')){
		if(document.getElementById('actiontype').value == ""){
	                document.getElementById('actiontype').value = 'Insert';
		}
        }        
        return true;
}

function updatePremiumDealer(divid,ajaxloaderid,premium_dealer_id,category_id){
        if(category_id == ''){
        	var category_id = document.getElementById('selected_category_id').value;
                if(isCategorySelected() == false){
                        alert("Please select the category.");
                        return false;
                }
                if(isLastLvlCategory() == false){
                        alert("Please select last level category.");
                        return false;
                }
        }
      
        //alert(divid+','+ajaxloaderid+','+category_id);
        var categoryid='';
        document.getElementById(ajaxloaderid).style.display = "block";
	if(divid == ""){ return false; }
        var url = admin_web_url+'ajax/premium_dealer_dashboard.php';
	//alert(url+'?act=update&premium_dealer_id='+premium_dealer_id+'&catid='+category_id);
	$.ajax({
        	url: url,
                data: 'act=update&premium_dealer_id='+premium_dealer_id+'&catid='+category_id,
                success: function(data){
                //alert(data);
                document.getElementById(divid).innerHTML = data;
                document.getElementById(divid).style.display="block";
                document.getElementById(ajaxloaderid).style.display = "none";
		
            },
            async:false
        });
	if(document.getElementById('actiontype')){
                document.getElementById('actiontype').value = 'Update';
        }
        return true;
}

function deletePremiumDealer(premium_dealer_id){
       
        document.getElementById('actiontype').value = 'Delete';
	document.getElementById('premium_dealer_id').value = premium_dealer_id;
        var answer = confirm ("Are you sure.Want to delete premium dealer?")
        if (answer){
                document.product_manage.submit();
                return true;
        }
        return false;
}
	
