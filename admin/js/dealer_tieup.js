function getDealerTieupDashboard(divid,ajaxloaderid,category_id,startlimit,cnt){
        if(category_id == ''){
                var category_id = document.getElementById('selected_category_id').value;

                if(isCategorySelected() == false){
                        alert("Please select the category.");
                        return false;
                }
                if(isLastLvlCategory() == false){
                        alert("Please select last level category.");
                        return false;
                }
        }

        //alert(divid+','+ajaxloaderid+','+category_id);
        document.getElementById(ajaxloaderid).style.display = "block";
	if(divid == ""){ return false; }
        var url = admin_web_url+'ajax/dealer_tieup_dashboard.php';
        //alert(url+'?catid='+category_id+'&startlimit='+startlimit+'&cnt='+cnt);return false;
        $.ajax({
        	url: url,
                data: 'catid='+category_id+'&startlimit='+startlimit+'&cnt='+cnt,
                success: function(data){
                	//alert(data);
		        document.getElementById(divid).innerHTML = data;
	        	document.getElementById(divid).style.display="block";
                	document.getElementById(ajaxloaderid).style.display = "none";
		},
	        async:false
        });
        return true;
}

function validateProduct(){
       	if(document.getElementById('select_dealer_id').value == "0"){
                alert("Please select the dealer.");
                return false;
        }
        if(document.getElementById('actiontype')){
                if(document.getElementById('actiontype').value == ""){
                        document.getElementById('actiontype').value = 'Insert';
                }
        } 
        return true;
}

function updateDealerTieup(divid,ajaxloaderid,dealer_tieup_id,category_id){
        if(category_id == ''){
        	var category_id = document.getElementById('selected_category_id').value;
                if(isCategorySelected() == false){
                        alert("Please select the category.");
                        return false;
                }
                if(isLastLvlCategory() == false){
                        alert("Please select last level category.");
                        return false;
                }
        }
      
        //alert(divid+','+ajaxloaderid+','+category_id);
        var categoryid='';
        document.getElementById(ajaxloaderid).style.display = "block";
	if(divid == ""){ return false; }
        var url = admin_web_url+'ajax/dealer_tieup_dashboard.php';
	//alert(url+'?act=update&dealer_tieup_id='+dealer_tieup_id+'&catid='+category_id);
	$.ajax({
        	url: url,
                data: 'act=update&dealer_tieup_id='+dealer_tieup_id+'&catid='+category_id,
                success: function(data){
                //alert(data);
                document.getElementById(divid).innerHTML = data;
                document.getElementById(divid).style.display="block";
                document.getElementById(ajaxloaderid).style.display = "none";
		
            },
            async:false
        });
	document.getElementById('actiontype').value = 'Update';
        return true;
}

function deleteDealerTieup(dealer_tieup_id){
       
        document.getElementById('actiontype').value = 'Delete';
	document.getElementById('dealer_tieup_id').value = dealer_tieup_id;
        var answer = confirm ("Are you sure.Want to delete this dealer tieup?")
        if (answer){
                document.product_manage.submit();
                return true;
        }
        return false;
}
	
