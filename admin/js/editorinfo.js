function tiny(){
	tinyMCE.init({
		// General options
		mode : "textareas",
		theme : "advanced",
		plugins : "spellchecker,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",

		// Theme options
		theme_advanced_buttons1 : "bold,italic,underline,link,unlink",
		theme_advanced_buttons2 : "",
		theme_advanced_buttons3 : "",
		theme_advanced_buttons4 : "",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : true,

		// Skin options
		skin : "o2k7",
		skin_variant : "silver",

		// Example content CSS (should be your site CSS)
		content_css : "css/example.css",

		// Drop lists for link/image/media/template dialogs
		template_external_list_url : "js/template_list.js",
		external_link_list_url : "js/link_list.js",
		external_image_list_url : "js/image_list.js",
		media_external_list_url : "js/media_list.js",

		// Replace values for the template plugin
		template_replace_values : {
		username : "Some User",
		staffid : "991234"
		}
	});
}
function geteditorDashboard(divid,ajaxloaderid,category_id,startlimit,cnt){
	if(category_id == ''){
		var category_id = document.getElementById('selected_category_id').value;

		if(isCategorySelected() == false){
			alert("Please select the category.");
			return false;
		}
		if(isLastLvlCategory() == false){
			alert("Please select last level category.");
			return false;
		}
	}
	
	//alert(divid+','+ajaxloaderid+','+category_id);
	document.getElementById(ajaxloaderid).style.display = "block";
	if(divid == ""){ return false; }
		var url = admin_web_url+'ajax/editorinfo_dashboard.php';
		//alert(url+'?catid='+category_id+'&startlimit='+startlimit+'&cnt='+cnt)
	        $.ajax({
			url: url,
			data: 'catid='+category_id+'&startlimit='+startlimit+'&cnt='+cnt,
			success: function(data){
				//alert(data);
                document.getElementById(divid).innerHTML = data;
                document.getElementById(divid).style.display="block";
                document.getElementById(ajaxloaderid).style.display = "none";
            },
            async:false
        });
	//tiny();
	return true;
}
function updateEditorInfo(divid,ajaxloaderid,editor_id,categoryid,startlimit,cnt){
	if(category_id == ''){
		var category_id = document.getElementById('selected_category_id').value;

		if(isCategorySelected() == false){
			alert("Please select the category.");
			return false;
		}
	}
	
	//alert(divid+','+ajaxloaderid+','+category_id);
	document.getElementById(ajaxloaderid).style.display = "block";
    	if(divid == ""){ return false; }
	var url = admin_web_url+'ajax/editorinfo_dashboard.php';
	//alert(url+'?act=update&editor_id='+editor_id+'&catid='+categoryid);
        $.ajax({
		url: url,
		data: 'act=update&editor_id='+editor_id+'&catid='+categoryid,
				
		success: function(data){
		//alert(data);
                document.getElementById(divid).innerHTML = data;
                document.getElementById(divid).style.display="block";
                document.getElementById(ajaxloaderid).style.display = "none";
            },
            async:false
        });
	if(document.getElementById('editor_id')){
                document.getElementById('editor_id').value = editor_id;
        }
	if(document.getElementById('actiontype')){
		document.getElementById('actiontype').value = 'Update';
	}
	return true;
}

function deleteEditorInfo(editor_id){
	document.getElementById('actiontype').value = 'Delete';
	document.getElementById('editor_id').value = editor_id;
	
    	var answer = confirm ("Are you sure.Want to delete Editor?")
    	if (answer){
         	document.product_manage.submit();
         	return true;
     	}
	return false;
}
function validateProduct(){
        if(document.getElementById('actiontype')){
		if(document.getElementById('actiontype').value == ""){
	                document.getElementById('actiontype').value = 'Insert';
		}
        }
        if(document.getElementById('editor_name').value == ''){
                alert("Please enter Editor Name");
                return false;
        }
	if(document.getElementById('email_id').value != ''){
		if(emailValidator(document.getElementById('email_id').value)==false){
                        alert("please enter valid email address");
                        document.getElementById('email_id').focus();
                        return false;
                }

	}
	if(document.getElementById('phone_no').value != ''){
		if(ToCellValidate(document.getElementById('phone_no').value)==false){
                        alert("please enter Correct Phone no");
                        document.getElementById('phone_no').focus();
                        return false;
                }
        }
        return true;
}

function emailValidator(email){
        var emailExp = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
        if(email.match(emailExp)){
                return true;
        }else{
                return false;
        }
}

function ToCellValidate(objMobileNo){
	if(objMobileNo!=""){
		var incomingString=objMobileNo;
		if(trim(incomingString).length > 10 || trim(incomingString).length < 10 || incomingString.search(/[^0-9\-()+]/g) != -1 ){
			return false;
		}else{
			return true; 
		}
	}
}
function trim(stringToTrim) {
	return stringToTrim.replace(/^\s+|\s+$/g,"");
}

function getUploadData (sFrm,sTitle,sId,sPath,mType,sImageCat){
	window.open('get_upload.php?rfrm='+sFrm+'&rtitle='+sTitle+'&rpath='+sPath+'&rid='+sId+'&rtype='+mType+'&rimgcat='+sImageCat,'mywindow','width=600,height=300,left=300,top=300');
}
/*function getUploadedDataList (sFrm,sTitle,sId,sPath,mType,sImageCat){
	window.open('search_store.php?rfrm='+sFrm+'&rtitle='+sTitle+'&rpath='+sPath+'&rid='+sId+'&rtype='+mType+'&rimgcat='+sImageCat,'mywindow','width=600,height=400,left=300,top=300');
}*/
