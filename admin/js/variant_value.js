function getVariantValueDashboard(divid,ajaxloaderid,category_id,startlimit,cnt,selected_brand_id,selected_model_id,selected_variant_id,city_id){
	
	if(selected_brand_id!=0){selected_brand_id =selected_brand_id}else{selected_brand_id="";}
	if(selected_model_id!=0){selected_model_id =selected_model_id}else{selected_model_id="";}
	if(selected_variant_id!=0){selected_variant_id =selected_variant_id}else{selected_variant_id="";}
	if(city_id!=0){city_id =city_id}else{city_id="";}

	if(category_id == ''){
		var category_id = document.getElementById('selected_category_id').value;
		if(isCategorySelected() == false){
			alert("Please select the category.");
			return false;
		}
		if(isLastLvlCategory() == false){
			alert("Please select last level category.");
			return false;
		}
	}
	document.getElementById(ajaxloaderid).style.display = "block";
	if(divid == ""){ return false; }
	var url = admin_web_url+'ajax/variant_value_dashboard.php';
	$.ajax({
		url: url,
		data: 'catid='+category_id+'&startlimit='+startlimit+'&cnt='+cnt+'&selected_brand_id='+selected_brand_id+'&selected_model_id='+selected_model_id+'&selected_variant_id='+selected_variant_id+'&city_id='+city_id,
		success: function(data){
			document.getElementById(divid).innerHTML = data;
			document.getElementById(divid).style.display="block";
			document.getElementById(ajaxloaderid).style.display = "none";
		},
		async:false
	});
		if(selected_model_id!='' ){
			getModelByBrandDashboard(selected_model_id,'get_model_detail.php','Model','');
		}
		if(selected_variant_id!='' ){
			getVariantByBrandModelDashboard(selected_variant_id,'get_variants.php','Variant','');
		}
	return true;
}

function sPricePagination(page,startlimit,cnt,filename,divid,category_id,selected_brand_id,selected_model_id,selected_variant_id,city_id){
	//alert(selected_brand_id+","+selected_model_id+","+selected_variant_id+","+city_id);
	if(selected_brand_id!=0){selected_brand_id =selected_brand_id}else{selected_brand_id="";}
	if(selected_model_id!=0){selected_model_id =selected_model_id}else{selected_model_id="";}
	if(selected_variant_id!=0){selected_variant_id =selected_variant_id}else{selected_variant_id="";}
	if(city_id!=0){city_id =city_id}else{city_id="";}
	if(category_id == ''){
		var category_id = document.getElementById('selected_category_id').value;

		if(isCategorySelected() == false){
			alert("Please select the category.");
			return false;
		}
		if(isLastLvlCategory() == false){
			alert("Please select last level category.");
			return false;
		}
	}
	if(divid == ""){ return false; }
		var url = admin_web_url+filename;

		$.ajax({
			url: url,
			data: 'catid='+category_id+'&page='+page+'&startlimit='+startlimit+'&cnt='+cnt+'&selected_brand_id='+selected_brand_id+'&selected_model_id='+selected_model_id+'&selected_variant_id='+selected_variant_id+'&city_id='+city_id,
			success: function(data){
				//alert(data);
                document.getElementById(divid).innerHTML = data;
                document.getElementById(divid).style.display="block";
               },
            async:false
        });
		if(selected_model_id!='' ){
			getModelByBrandDashboard(selected_model_id,'get_model_detail.php','Model','');
		}
		if(selected_variant_id!='' ){
			getVariantByBrandModelDashboard(selected_variant_id,'get_variants.php','Variant','');
		}
		
	return true;
}



function updateVariantValue(divid,ajaxloaderid,price_variant,categoryid,brandid,productid,countryid,stateid,cityid,startlimit,cnt){
	if(category_id == ''){
		var category_id = document.getElementById('selected_category_id').value;
		if(isCategorySelected() == false){
			alert("Please select the category.");
			return false;
		}
		if(isLastLvlCategory() == false){
			alert("Please select last level category.");
			return false;
		}
	}
	document.getElementById(ajaxloaderid).style.display = "block";
    if(divid == ""){ return false; }
	
		var url = admin_web_url+'ajax/variant_value_dashboard.php';
		//alert(url+'?act=update&price_variant='+price_variant+'&catid='+categoryid+'&bid='+brandid+'&pid='+productid+'&startlimit='+startlimit+'&cnt='+cnt);
        $.ajax({
			url: url,
			data: 'act=update&price_variant='+price_variant+'&catid='+categoryid+'&bid='+brandid+'&pid='+productid+'&startlimit='+startlimit+'&cnt='+cnt,
				
			success: function(data){
				//alert(data);
				document.getElementById(divid).innerHTML = data;
                document.getElementById(divid).style.display="block";
                document.getElementById(ajaxloaderid).style.display = "none";
            },
            async:false
        });
		getProductByBrand('ajaxloaderbrand',productid);
		getStateByCountry('ajaxloadercountry',stateid);
		getCityByState('ajaxloaderstate',cityid);
		document.getElementById('actiontype').value ='Update';
	
	return true;
}


function deleteVariantValue(price_variant,prdid){
	   	document.getElementById('actiontype').value = 'Delete';
		document.getElementById('price_variant').value = price_variant;
		document.getElementById('product_id').value = prdid;
    	var answer = confirm ("Are you sure.Want to delete price variant detail?")
    	if (answer){
         	document.variant_manage.submit();
         	return true;
     	}
	return false;
}


function validateVariant(){
	if(document.getElementById('select_brand_id').value == ''){
		alert("Please select the brand.");
		return false;
	}
	if(document.getElementById('product_name').value == ''){
		alert("Please add the product");
		return false;
	}	
	var exshowroom_mrp = document.getElementById('product_mrp_ex_showroom').value;
	var state_id = document.getElementById('state_id').value;
	if(exshowroom_mrp != '' && state_id == ''){
		alert("Please select the state for ex-show room price.");
		return false;
	}
	var city_id = document.getElementById('city_id').value;
	if(state_id != '' && city_id == ''){
		alert("Please select the city for ex-show room price.");
		return false;
	}
	return true;
}


function getProductByBrand(ajaxloaderid,productid){
	var brand_id = document.getElementById('select_brand_id').value;
	if(brand_id == '' ||  brand_id == 0){return false;}
	var category_id = document.getElementById('selected_category_id').value;
	if(category_id == '' ||  category_id == 0){return false;}
	document.getElementById(ajaxloaderid).style.display = "block";
	
	var url = admin_web_url+'ajax/select_product.php';
	//alert(url+'?category_id='+category_id+'&brand_id='+brand_id+'&productid='+productid);
	var html = $.ajax({ 
						url: url,
						data: 'category_id='+category_id+'&brand_id='+brand_id+'&productid='+productid,
						success:
						function(data){
							//alert(data);
							document.getElementById(ajaxloaderid).style.display = "none";
						},
						async: false
					}).responseText;
	var table = document.getElementById("Update");
	//alert(productid);
	if(productid!=0){	var rowCount = 4;}else{var rowCount = 6;}
	var rowId='product_row_id_'+rowCount;
	if(document.getElementById(rowId)){
		removeTr(rowId);
	}
	var row = table.insertRow(rowCount);
	row.id = rowId;
	var product_name = row.insertCell(0);
	product_name.innerHTML = 'Product Name';
	var product_name_value = row.insertCell(1);
	product_name_value.colSpan = 10;
	//alert(html);
	product_name_value.innerHTML = html;
	return true;
}


function getStateByCountry(ajaxloaderid,stateid){
	var country_id = document.getElementById('select_country_id').value;
	if(country_id == '' ||  country_id == 0){return false;}
	document.getElementById(ajaxloaderid).style.display = "block";
	var url = admin_web_url+'ajax/select_state.php';
	//alert(url);
	var html1 = $.ajax({ 
						url: url,
						data: 'country_id='+country_id+'&state_id='+stateid,
						success:
						function(data){
							//alert(data);
							document.getElementById(ajaxloaderid).style.display = "none";
						},
						async: false
					}).responseText;
	var table = document.getElementById("Update");
	var rowCount = 3;
	var rowId='state_row_id_'+rowCount;
	if(document.getElementById(rowId)){
		removeTr(rowId);
	}
	var row = table.insertRow(rowCount);
	row.id = rowId;
	var state_name = row.insertCell(0);
	state_name.innerHTML = 'State Name';
	var state_name_value = row.insertCell(1);
	state_name_value.colSpan = 10;
	state_name_value.innerHTML = html1;
	return true;
}

function getCityByState(ajaxloaderid,cityid){
	var state_id = document.getElementById('select_state_id').value;
	if(state_id == '' ||  state_id == 0){return false;}
	document.getElementById(ajaxloaderid).style.display = "block";
	var url = admin_web_url+'ajax/select_city.php';
	//alert(url);
	var html = $.ajax({ 
						url: url,
						data: 'state_id='+state_id+'&city_id='+cityid,
						success:
						function(data){
							//alert(data);
							document.getElementById(ajaxloaderid).style.display = "none";
						},
						async: false
					}).responseText;
	var table = document.getElementById("Update");
	var rowCount = 4;
	var rowId='city_row_id_'+rowCount;
	if(document.getElementById(rowId)){
		removeTr(rowId);
	}
	var row = table.insertRow(rowCount);
	row.id = rowId;
	var city_name = row.insertCell(0);
	city_name.innerHTML = 'City Name';
	var city_name_value = row.insertCell(1);
	city_name_value.colSpan = 10;
	city_name_value.innerHTML = html;
	return true;
}


function city_details(ajaxloaderid){
	var state_id = document.getElementById('state_id').value;
	if(state_id == '' ||  state_id == 0){return false;}

	
	document.getElementById(ajaxloaderid).style.display = "block";
	var url = admin_web_url+'ajax/city_ajax.php';
	var html = $.ajax({ url: url, data: 'state_id='+state_id, success: function(data){ document.getElementById(ajaxloaderid).style.display = "none";}, async: false}).responseText;
	var table = document.getElementById("Update");
	var rowCount = 6;	
	var row = table.insertRow(rowCount);
	row.id = 'city_row_id_'+rowCount;

	var city_name = row.insertCell(0);

    	city_name.innerHTML = 'Show room city'+rowCount;

	var city_name_value = row.insertCell(1);
	city_name_value.colSpan = 10;
	city_name_value.innerHTML = html;
	return true;
}
function removeTr(rowId){
    var row = document.getElementById(rowId);
	if(row.parentElement){
	       	row.parentElement.removeChild(row);
	}else if(row.parentNode){
		row.parentNode.removeChild(row);
	}
	
	return false;
}
function remove_product_row(rowCount){	
	if(rowCount == 0){return false;}
	if(document.getElementById('product_remove_linkrow_id_'+rowCount)){
		removeTr('product_remove_linkrow_id_'+rowCount);
    	}
	if(document.getElementById('product_status_row_id_'+rowCount)){
		removeTr('product_status_row_id_'+rowCount);
	}
	if(document.getElementById('product_style_row_id_'+rowCount)){
		removeTr('product_style_row_id_'+rowCount);
	}
	if(document.getElementById('product_style_row_id_'+rowCount)){
		removeTr('product_style_row_id_'+rowCount);
	}
	if(document.getElementById('product_desc_row_id_'+rowCount)){
		removeTr('product_desc_row_id_'+rowCount);
	}
	if(document.getElementById('product_group_row_id_'+rowCount)){
		removeTr('product_group_row_id_'+rowCount);
	}
	if(document.getElementById('product_name_row_id_'+rowCount)){
		removeTr('product_name_row_id_'+rowCount);
	}
	return false;
}


function getUploadData (sFrm,sTitle,sId,mType,sImageCat){
	window.open('get_upload.php?rfrm='+sFrm+'&rtitle='+sTitle+'&rid='+sId+'&rtype='+mType+'&rimgcat='+sImageCat,'mywindow','width=600,height=300,left=300,top=300');
}
function getUploadedDataList (sFrm,sTitle,sId,mType,sImageCat){
	window.open('search_store.php?rfrm='+sFrm+'&rtitle='+sTitle+'&rid='+sId+'&rtype='+mType+'&rimgcat='+sImageCat,'mywindow','width=600,height=400,left=300,top=300');
}

function getModelByBrandDashboard(iModelId,surl,divname,param){
        var iBrndId=document.getElementById('select_brand').value;
        //alert(iBrndId);
        if(iBrndId == ""){
                $('#Model').empty().append('<option value="">--All Models--</option>');
        	$('#Variant').empty().append('<option value="">--All Variants--</option>');
                return false;
        }
        var str="action=model&brand_id="+iBrndId+"&product_name_id="+iModelId+"&Rand="+Math.random();
        $('#Variant').empty().append('<option value="">--All Variants--</option>');
	var url = admin_web_url+'ajax/'+surl;
	//alert("url==="+url);
        $.ajax({
                url: url,
                data: str,
                success: function(data){
                        $('#Model').empty().append(data);
                },
                async:false
        });
        $('#Variant').empty().append('<option value="">--All Variants--</option>');
}

function getVariantByBrandModelDashboard(product_id,surl,divname,param){
        var iBrndId=document.getElementById('select_brand').value;
        var iModelId=document.getElementById('Model').value;
        //alert(iBrndId);
	//alert(admin_web_url+'ajax/'+surl+"?action=model&brand_id="+iBrndId+"&product_name_id="+iModelId+"&product_id="+product_id+"&Rand="+Math.random());
        var str="action=model&brand_id="+iBrndId+"&product_name_id="+iModelId+"&product_id="+product_id+"&Rand="+Math.random();
	var url = admin_web_url+'ajax/'+surl;
	//alert("url==="+url);
        $.ajax({
            url: url,
            data: str,
            success: function(data){
                $('#Variant').empty().append(data);
            },
            async:false
        });
}

function submitResearchForm(){
        var iBrndId=document.getElementById('select_brand').value;
        var iModelId=document.getElementById('Model').value;
        var iVariantId=document.getElementById('Variant').value;

        /*if(iBrndId == ""){
                alert("Please Select Brand.");
                return false;
        }*/

        document.product_manage_dashboard.submit();
        return true;
}
