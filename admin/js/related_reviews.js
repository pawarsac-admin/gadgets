function getProductReviewsDashboard(divid,ajaxloaderid,category_id,startlimit,cnt){
	
	if(category_id == ''){
		var category_id = document.getElementById('selected_category_id').value;
		if(isCategorySelected() == false){
			alert("Please select the category.");
			return false;
		}
		if(isLastLvlCategory() == false){
			alert("Please select last level category.");
			return false;
		}
	}
	document.getElementById(ajaxloaderid).style.display = "block";
	if(divid == ""){ return false; }
	var url = admin_web_url+'ajax/related_reviews_dashboard.php';
	//alert(url+'?catid='+category_id+'&startlimit='+startlimit+'&cnt='+cnt)
	$.ajax({
		url: url,
		data: 'catid='+category_id+'&startlimit='+startlimit+'&cnt='+cnt,
		success: function(data){
		//alert(data);
		document.getElementById(divid).innerHTML = data;
		document.getElementById(divid).style.display="block";
		document.getElementById(ajaxloaderid).style.display = "none";
		},
		async:false
	});
	return true;
}

function getProductReviewsDashboardByType(divid,ajaxloaderid,category_id,startlimit,cnt){
	document.getElementById('actiontype').value = 'Select';
	if(category_id == ''){
		var category_id = document.getElementById('selected_category_id').value;
		if(isCategorySelected() == false){
			alert("Please select the category.");
			return false;
		}
		if(isLastLvlCategory() == false){
			alert("Please select last level category.");
			return false;
		}
	}
	if(document.getElementById('view_section_id')!="undefined"){
		var view_section_id = document.getElementById('view_section_id').value;
	}
	document.getElementById(ajaxloaderid).style.display = "block";
	if(divid == ""){ return false; }
	var url = admin_web_url+'ajax/related_reviews_dashboard.php';
	//alert(url+'?catid='+category_id+'&startlimit='+startlimit+'&cnt='+cnt+'&view_section_id='+view_section_id);
	$.ajax({
		url: url,
		data: 'catid='+category_id+'&startlimit='+startlimit+'&cnt='+cnt+'&view_section_id='+view_section_id,
		success: function(data){
		//alert(data);
		document.getElementById(divid).innerHTML = data;
		document.getElementById(divid).style.display="block";
		document.getElementById(ajaxloaderid).style.display = "none";
		},
		async:false
	});
	return true;
}

function updateProductReviews(divid,ajaxloaderid,sectionid,reviewid,article_type_id,category_id){
	document.getElementById('actiontype').value = 'Update';
	if(category_id == ''){
		var category_id = document.getElementById('selected_category_id').value;
		if(isCategorySelected() == false){
			alert("Please select the category.");
			return false;
		}
		if(isLastLvlCategory() == false){
			alert("Please select last level category.");
			return false;
		}
	}
	//alert(PRODUCT_NAME_INFO);
	if(document.getElementById('view_section_id')!="undefined"){
		var view_section_id = document.getElementById('view_section_id').value;
	}
	//var categoryid='';
	document.getElementById(ajaxloaderid).style.display = "block";
	if(divid == ""){ return false; }
	var url = admin_web_url+'ajax/related_reviews_dashboard.php';

	$.ajax({
		url: url,
		data: 'act=update&rid='+reviewid+'&catid='+category_id+'&article_type_id='+article_type_id+'&section_review_id='+sectionid+'&view_section_id='+view_section_id,
		success: function(data){
		document.getElementById(divid).innerHTML = data;
		document.getElementById(divid).style.display="block";
		document.getElementById(ajaxloaderid).style.display = "none";
		},
		async:false
	});
	//getTypeData('ajaxloaderType',reviewid,article_type_id);
	return true;
}

function deleteProductReviews(secreviewid){
	if(document.getElementById('view_section_id')!="undefined"){
		var view_section_id = document.getElementById('view_section_id').value;
		document.getElementById('hd_view_section_id').value=view_section_id;
	}
	document.getElementById('actiontype').value = 'Delete';
	document.getElementById('section_review_id').value = secreviewid;
	var answer = confirm ("Are you sure.Want to delete review?")
	if (answer){
		document.product_manage.submit();
		return true;
	}
	return false;
}

function getModelByBrand(ajaxloaderid,product_name_id){
	var brand_id = document.getElementById('select_brand_id').value;
	if(brand_id == '' ||  brand_id == 0){return false;}
	var category_id = document.getElementById('selected_category_id').value;
	if(category_id == '' ||  category_id == 0){return false;}
	document.getElementById(ajaxloaderid).style.display = "block";
	var url = admin_web_url+'ajax/select_model.php';
	var html = $.ajax({ url: url, data: 'category_id='+category_id+'&brand_id='+brand_id+'&product_name_id='+product_name_id, success: function(data){ document.getElementById(ajaxloaderid).style.display = "none";}, async: false}).responseText;
	var table = document.getElementById("Update");
	var rowCount = 1;
	var rowId='product_row_id_'+rowCount;
	if(document.getElementById(rowId)){
	removeTr(rowId);
	}
	var row = table.insertRow(rowCount);
	row.id = rowId;
	var product_name = row.insertCell(0);
	product_name.innerHTML = 'Model Name';
	var product_name_value = row.insertCell(1);
	product_name_value.colSpan = 10;
	product_name_value.innerHTML = html;
	return true;
}

function getTypeData(ajaxloaderid,reviewid,article_type_id){
	document.getElementById('actiontype').value = 'Select';
	var type_id = document.getElementById('select_aritcle_type_id').value;
	if(article_type_id!='0'){
		type_id=article_type_id;
	}
	//alert(article_type_id);
	var category_id = document.getElementById('selected_category_id').value;
	if(category_id == '' ||  category_id == 0){return false;}
	document.getElementById(ajaxloaderid).style.display = "block";
	var url = admin_web_url+'ajax/select_reviews.php';
	//alert(url+'?category_id='+category_id+'&article_type_id='+article_type_id+'&review_id='+reviewid);
	var html = $.ajax({ url: url, data: 'category_id='+category_id+'&review_type_id='+type_id+'&review_id='+reviewid, success: function(data){
	document.getElementById(ajaxloaderid).style.display = "none";
	}, async: false}).responseText;
	var table = document.getElementById("Update");
	var rowCount = 2;
	var rowId='product_row_id_'+rowCount;
	if(document.getElementById(rowId)){
		removeTr(rowId);
	}
	var row = table.insertRow(rowCount);
	row.id = rowId;
	var product_name = row.insertCell(0);
	product_name.innerHTML = 'Review Title';
	var product_name_value = row.insertCell(1);
	product_name_value.colSpan = 10;
	product_name_value.innerHTML = html;
	return true;
}

function getVariantByModel(ajaxloaderid,product_id){
	
	var brand_id = document.getElementById('select_brand_id').value;
	if(brand_id == '' ||  brand_id == 0){return false;}

	var product_name_id = document.getElementById('select_model_id').value;
	if(product_name_id == '' ||  product_name_id == 0){return false;}
	
	var category_id = document.getElementById('selected_category_id').value;
	if(category_id == '' ||  category_id == 0){return false;}

	//if(productid!=''){}
	
	document.getElementById(ajaxloaderid).style.display = "block";
	var url = admin_web_url+'ajax/select_variant.php';
	var html = $.ajax({ url: url, data: 'category_id='+category_id+'&brand_id='+brand_id+'&product_id='+product_id+'&product_name_id='+product_name_id, success: function(data){ document.getElementById(ajaxloaderid).style.display = "none";}, async: false}).responseText;
	
	var table = document.getElementById("Update");
	var rowCount = 2;


	var rowId='product_row_id_'+rowCount;
	if(document.getElementById(rowId)){
		removeTr(rowId);
	}
	var row = table.insertRow(rowCount);
	
	row.id = rowId;
	
	var product_name = row.insertCell(0);

    	product_name.innerHTML = 'Product Name';

	var product_name_value = row.insertCell(1);
	product_name_value.colSpan = 10;
	product_name_value.innerHTML = html;
	return true;
}



function validateProduct(){
	document.getElementById('actiontype').value = 'Insert';
	if(document.getElementById('select_aritcle_type_id').value == '0'){
		alert("Please select the type.");
		return false;
	}
	if(document.getElementById('select_aritcle_type_id').value != '0'){
		if(document.getElementById('select_review_id').value == ''){
			alert("Please add the review");
			return false;
		}
	}
	return true;
}


function getProductByBrand(ajaxloaderid,productid){
	
	var brand_id = document.getElementById('select_brand_id').value;
	if(brand_id == '' ||  brand_id == 0){return false;}


	var category_id = document.getElementById('selected_category_id').value;
	if(category_id == '' ||  category_id == 0){return false;}

	//if(productid!=''){}
	
	document.getElementById(ajaxloaderid).style.display = "block";
	var url = admin_web_url+'ajax/select_product.php';
	var html = $.ajax({ url: url, data: 'category_id='+category_id+'&brand_id='+brand_id+'&productid='+productid, success: function(data){ document.getElementById(ajaxloaderid).style.display = "none";}, async: false}).responseText;
	
	var table = document.getElementById("Update");
	var rowCount = 1;


	var rowId='product_row_id_'+rowCount;
	if(document.getElementById(rowId)){
		removeTr(rowId);
	}
	var row = table.insertRow(rowCount);
	
	row.id = rowId;
	
	var product_name = row.insertCell(0);

    	product_name.innerHTML = 'Product Name';

	var product_name_value = row.insertCell(1);
	product_name_value.colSpan = 10;
	product_name_value.innerHTML = html;
	return true;
}



function removeTr(rowId){
    var row = document.getElementById(rowId);
	if(row.parentElement){
	       	row.parentElement.removeChild(row);
	}else if(row.parentNode){
		row.parentNode.removeChild(row);
	}
	
	return false;
}
function remove_product_row(rowCount){	
	if(rowCount == 0){return false;}
	if(document.getElementById('product_remove_linkrow_id_'+rowCount)){
		removeTr('product_remove_linkrow_id_'+rowCount);
    	}
	if(document.getElementById('product_status_row_id_'+rowCount)){
		removeTr('product_status_row_id_'+rowCount);
	}
	if(document.getElementById('product_style_row_id_'+rowCount)){
		removeTr('product_style_row_id_'+rowCount);
	}
	if(document.getElementById('product_style_row_id_'+rowCount)){
		removeTr('product_style_row_id_'+rowCount);
	}
	if(document.getElementById('product_desc_row_id_'+rowCount)){
		removeTr('product_desc_row_id_'+rowCount);
	}
	if(document.getElementById('product_group_row_id_'+rowCount)){
		removeTr('product_group_row_id_'+rowCount);
	}
	if(document.getElementById('product_name_row_id_'+rowCount)){
		removeTr('product_name_row_id_'+rowCount);
	}
	return false;
}

function getUploadData (sFrm,sTitle,sId,sPath,mType,sImageCat){
	window.open('get_upload.php?rfrm='+sFrm+'&rtitle='+sTitle+'&rpath='+sPath+'&rid='+sId+'&rtype='+mType+'&rimgcat='+sImageCat,'mywindow','width=600,height=300,left=300,top=300');
}
function getUploadedDataList (sFrm,sTitle,sId,sPath,mType,sImageCat){
	window.open('search_store.php?rfrm='+sFrm+'&rtitle='+sTitle+'&rpath='+sPath+'&rid='+sId+'&rtype='+mType+'&rimgcat='+sImageCat,'mywindow','width=600,height=400,left=300,top=300');
}