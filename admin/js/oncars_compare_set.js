function getOncarsCompareSetDashboard(divid,ajaxloaderid,category_id,startlimit,cnt){
	if(category_id == ''){
		var category_id = document.getElementById('selected_category_id').value;
		if(isCategorySelected() == false){
			alert("Please select the category.");
			return false;
		}
		if(isLastLvlCategory() == false){
			alert("Please select last level category.");
			return false;
		}
	}
	document.getElementById(ajaxloaderid).style.display = "block";
    if(divid == ""){ return false; }
		var url = admin_web_url+'ajax/oncars_comparison_dashboard.php';
		//alert(url+'?catid='+category_id+'&startlimit='+startlimit+'&cnt='+cnt)
        $.ajax({
			url: url,
			data: 'catid='+category_id+'&startlimit='+startlimit+'&cnt='+cnt,
			success: function(data){
				//alert(data);
                document.getElementById(divid).innerHTML = data;
                document.getElementById(divid).style.display="block";
                document.getElementById(ajaxloaderid).style.display = "none";
            },
            async:false
        });
	return true;
}

function updateOncarsComparison(divid,ajaxloaderid,oncars_compare_id,categoryid){
	if(category_id == ''){
		var category_id = document.getElementById('selected_category_id').value;
		if(isCategorySelected() == false){
			alert("Please select the category.");
			return false;
		}
		if(isLastLvlCategory() == false){
			alert("Please select last level category.");
			return false;
		}
	}
	document.getElementById(ajaxloaderid).style.display = "block";
	if(divid == ""){ return false; }
	var url = admin_web_url+'ajax/oncars_comparison_dashboard.php';
	//alert(url+'?act=update&catid='+categoryid+'&oncars_compare_id='+oncars_compare_id);
        $.ajax({
		url: url,
		data: 'act=update&catid='+categoryid+'&oncars_compare_id='+oncars_compare_id,
		success: function(data){
		//alert(data);
                document.getElementById(divid).innerHTML = data;
                document.getElementById(divid).style.display="block";
                document.getElementById(ajaxloaderid).style.display = "none";
            },
            async:false
        });
	var iTotalRowsCurrent = document.getElementById('display_rows').value;
	if(iTotalRowsCurrent >= 3){
        	document.getElementById('add').style.visibility="hidden";
                document.getElementById('remove').style.visibility="hidden";
        }
	if(document.getElementById('actiontype')){
		document.getElementById('actiontype').value = 'Update';
	}
	
	return true;
}

function deleteOncarsComparison(oncars_compare_id){
   	document.getElementById('actiontype').value = 'Delete';
	document.getElementById('oncars_compare_id').value = oncars_compare_id;
    	var answer = confirm ("Are you sure.Want to delete ithis oncars compare set?")
    	if (answer){
         	document.product_manage.submit();
         	return true;
     	}
	return false;
}

function getModelByBrand(ajaxloaderid,product_name_id,position){
        //alert(position);
        var select_variant_id = "select_comp_variant_id_"+position;
        document.getElementById(select_variant_id).innerHTML = '<option value="">---Select variant---</option>';
        var comp_brand_id = "select_comp_brand_id_"+position;
        var brand_id = document.getElementById(comp_brand_id).value;
	var select_model_id = "select_model_id_"+position;
        if(brand_id == '' ||  brand_id == 0){
		document.getElementById(select_model_id).innerHTML = "<select name='select_comp_model_id_"+position+"' id='select_comp_model_id_"+position+"'><option value=''>---Select Model---</option>";
		return false;
	}
        var category_id = document.getElementById('selected_category_id').value;
        if(category_id == '' ||  category_id == 0){return false;}
        var url = admin_web_url+'ajax/select_top_competitor_model.php';
        var html = $.ajax({ url: url, data: 'category_id='+category_id+'&brand_id='+brand_id+'&product_name_id='+product_name_id+'&position='+position, success: function(data){ }, async: false}).responseText;
        var table = document.getElementById("Update");

       	//alert("id=="+select_model_id+"==html=="+html);
	if(html == ""){
	        document.getElementById(select_model_id).innerHTML = "<select name='select_comp_model_id_"+position+"' id='select_comp_model_id_"+position+"'><option value=''>---Select Model---</option>";
	}else{
	        document.getElementById(select_model_id).innerHTML = html;
	}
        return true;
}

function getVariantByModelCompetitor(ajaxloaderid,product_id,position){

        var comp_brand_id = "select_comp_brand_id_"+position;
        var brand_id = document.getElementById(comp_brand_id).value;

	var select_variant_id = "select_comp_variant_id_"+position;

        if(brand_id == '' ||  brand_id == 0){
		return false;
	}

        var comp_model_id = "select_comp_model_id_"+position;
        if(document.getElementById(comp_model_id)){
                var product_name_id = document.getElementById(comp_model_id).value;
                //alert("comp_model_id=="+comp_model_id);
                if(product_name_id == '' ||  product_name_id == 0){
        		document.getElementById(select_variant_id).innerHTML = '<option value="">---Select variant---</option>';
			return false;
		}

                var category_id = document.getElementById('selected_category_id').value;
                if(category_id == '' ||  category_id == 0){return false;}

		if(ajaxloaderid != ""){
	                document.getElementById(ajaxloaderid).style.display = "block";
		}

                var url = admin_web_url+'ajax/select_oncars_compare_variant.php';
		//alert(url+'?category_id='+category_id+'&brand_id='+brand_id+'&product_id='+product_id+'&product_name_id='+product_name_id+'&position='+position);

                var html = $.ajax({ url: url, data: 'category_id='+category_id+'&brand_id='+brand_id+'&product_id='+product_id+'&product_name_id='+product_name_id+'&position='+position, success: function(data){ if(ajaxloaderid != ""){document.getElementById(ajaxloaderid).style.display = "none";}}, async: false}).responseText;
                var table = document.getElementById("Update");

                var select_variant_id = "select_variant_id_"+position;

                document.getElementById(select_variant_id).innerHTML = html;
        }

        return true;
}

function validateProduct(){
	if(document.getElementById('actiontype')){
                if(document.getElementById('actiontype').value == ""){
                        document.getElementById('actiontype').value = 'Insert';
                }
        }
	if(document.getElementById('title')){
		if(document.getElementById('title').value == ''){
			alert("Please enter title.");
			return false;
		}	
	}
	if(document.getElementById('description')){
		if(document.getElementById('description').value == ''){
			alert("Please enter description.");
			return false;
		}	
	}
	var iTotalRowsCurrent = document.getElementById('display_rows').value;
	if(iTotalRowsCurrent < '3'){
		alert("Add 3 products for comparison");
		return false;
	}
        for(i=1;i<=iTotalRowsCurrent;i++){
		if(document.getElementById('select_comp_brand_id_'+i)){
			if(document.getElementById('select_comp_brand_id_'+i).value == ''){
				alert("Please select brand "+i+".");
				return false;
			}	
		}
		if(document.getElementById('select_comp_model_id_'+i)){
			if(document.getElementById('select_comp_model_id_'+i).value == ''){
				alert("Please select model "+i+".");
				return false;
			}
		}
		if(document.getElementById('select_comp_variant_id_'+i)){
			if(document.getElementById('select_comp_variant_id_'+i).value == ''){
				alert("Please select variant "+i+".");
				return false;
			}
		}
	}
	if(document.getElementById('position')){
                if(document.getElementById('position').value == ''){
                        alert("Please select position.");
                        return false;
                }
        }
	
	return true;
}

function getUploadData (sFrm,sTitle,sId,sPath,mType,sImageCat){
	window.open('get_upload.php?rfrm='+sFrm+'&rtitle='+sTitle+'&rpath='+sPath+'&rid='+sId+'&rtype='+mType+'&rimgcat='+sImageCat,'mywindow','width=600,height=300,left=300,top=300');
}
function getUploadedDataList (sFrm,sTitle,sId,sPath,mType,sImageCat){
	window.open('search_store.php?rfrm='+sFrm+'&rtitle='+sTitle+'&rpath='+sPath+'&rid='+sId+'&rtype='+mType+'&rimgcat='+sImageCat,'mywindow','width=600,height=400,left=300,top=300');
}

function sMostpopularPagination(page,startlimit,cnt,filename,divid,category_id){

	
	if(category_id == ''){
		var category_id = document.getElementById('selected_category_id').value;

		if(isCategorySelected() == false){
			alert("Please select the category.");
			return false;
		}
		if(isLastLvlCategory() == false){
			alert("Please select last level category.");
			return false;
		}
	}
		
	if(divid == ""){ return false; }
		var url = admin_web_url+filename;

		$.ajax({
			url: url,
			data: 'catid='+category_id+'&page='+page+'&startlimit='+startlimit+'&cnt='+cnt,
			success: function(data){
				//alert(data);
                document.getElementById(divid).innerHTML = data;
                document.getElementById(divid).style.display="block";
               },
            async:false
        });
	
	return true;
}
