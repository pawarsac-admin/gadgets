function getBlackListNumbersDashboard(divid,ajaxloaderid,category_id,startlimit,cnt,view_section_id){
        if(category_id == ''){
                var category_id = document.getElementById('selected_category_id').value;

                if(isCategorySelected() == false){
                        alert("Please select the category.");
                        return false;
                }
                if(isLastLvlCategory() == false){
                        alert("Please select last level category.");
                        return false;
                }
        }

        //alert(divid+','+ajaxloaderid+','+category_id);
        document.getElementById(ajaxloaderid).style.display = "block";
        if(divid == ""){ return false; }
        if(view_section_id == ''){
                if(document.getElementById('view_section_id')){
                        view_section_id = document.getElementById('view_section_id').value;
                }
        }
                var url = admin_web_url+'ajax/black_listed_numbers_dashboard.php';
                //alert(url+'?catid='+category_id+'&startlimit='+startlimit+'&cnt='+cnt+'&view_section_id='+view_section_id)
                $.ajax({
                        url: url,
                        data: 'catid='+category_id+'&startlimit='+startlimit+'&cnt='+cnt+'&view_section_id='+view_section_id,
                        success: function(data){
                        //alert(data);
                        document.getElementById(divid).innerHTML = data;
                        document.getElementById(divid).style.display="block";
                        document.getElementById(ajaxloaderid).style.display = "none";
                },
                async:false
        });
        //tiny();
        return true;
}
function updateBlackListedNumber(divid,ajaxloaderid,black_list_number_id,category_id){
	if(category_id == ''){
                var category_id = document.getElementById('selected_category_id').value;
                if(isCategorySelected() == false){
                        alert("Please select the category.");
                        return false;
                }
                if(isLastLvlCategory() == false){
                        alert("Please select last level category.");
                        return false;
                }
        }
        if(document.getElementById('view_section_id')!="undefined"){
                var view_section_id = document.getElementById('view_section_id').value;
                document.getElementById('hd_view_section_id').value=view_section_id;
        }
        //alert(divid+','+ajaxloaderid+','+black_list_number_id+','+category_id+'&view_section_id='+view_section_id);
        document.getElementById(ajaxloaderid).style.display = "block";
    	if(divid == ""){ return false; }
       	var url = admin_web_url+'ajax/black_listed_numbers_dashboard.php';
        $.ajax({
                        url: url,
                        data: 'actiontype=update&black_list_number_id='+black_list_number_id+'&catid='+category_id+'&view_section_id='+view_section_id,

                        success: function(data){
                                //alert(data);
                document.getElementById(divid).innerHTML = data;
                document.getElementById(divid).style.display="block";
                document.getElementById(ajaxloaderid).style.display = "none";
            },
            async:false
        });
	if(document.getElementById('actiontype')){
                document.getElementById('actiontype').value = 'Update';
        }
        return true;
}

function deleteBlackListedNumber(black_list_number_id){
	document.getElementById('actiontype').value = 'Delete';
	if(document.getElementById('view_section_id')!="undefined"){
                var view_section_id = document.getElementById('view_section_id').value;
                document.getElementById('hd_view_section_id').value=view_section_id;
        }
        document.getElementById('black_list_number_id').value = black_list_number_id;
        var answer = confirm ("Are you sure.Want to delete this black list number?")
        if (answer){
                document.product_manage.submit();
                return true;
        }
        return false;
}
function validateProduct(){
	if(document.getElementById('actiontype')){
		if(document.getElementById('actiontype').value == ""){
	                document.getElementById('actiontype').value = 'Insert';
		}
        }
	if(document.getElementById('select_section_id')!="undefined"){
                if((document.getElementById('select_section_id').value) == "0"){
                        alert("Please select black list number section.");
                        return false;
                }
        }
        if(document.getElementById('view_section_id')!="undefined"){
                var view_section_id = document.getElementById('view_section_id').value;
                document.getElementById('hd_view_section_id').value=view_section_id;
        }
        var iTotalRowsCurrent = document.getElementById('display_rows').value;
        for(i=1;i<=iTotalRowsCurrent;i++){
        	if(document.getElementById('black_list_number_'+i).value == ''){
                	alert("Please enter black list number "+i);
	                return false;
        	}
        }
        return true;
}
