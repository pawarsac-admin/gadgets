function getProductSlidesDashboard(divid,ajaxloaderid,category_id,startlimit,cnt){

        if(category_id == ''){
                var category_id = document.getElementById('selected_category_id').value;

                if(isCategorySelected() == false){
                        alert("Please select the category.");
                        return false;
                }
                if(isLastLvlCategory() == false){
                        alert("Please select last level category.");
                        return false;
                }
        }

        //alert(divid+','+ajaxloaderid+','+category_id);
        document.getElementById(ajaxloaderid).style.display = "block";
	if(divid == ""){ return false; }
        var url = admin_web_url+'ajax/clinck_slideshow_dashboard.php';
        //alert(url+'?catid='+category_id+'&startlimit='+startlimit+'&cnt='+cnt);return false;
        $.ajax({
        	url: url,
                data: 'catid='+category_id+'&startlimit='+startlimit+'&cnt='+cnt,
                success: function(data){
                	//alert(data);
		        document.getElementById(divid).innerHTML = data;
	        	document.getElementById(divid).style.display="block";
                	document.getElementById(ajaxloaderid).style.display = "none";
		},
	        async:false
        });
        return true;
}

function getProductSlideDashboardByType(divid,ajaxloaderid,category_id,startlimit,cnt){
        //alert("dsdsdsdsds");
        if(category_id == ''){
                var category_id = document.getElementById('selected_category_id').value;

                if(isCategorySelected() == false){
                        alert("Please select the category.");
                        return false;
                }
                if(isLastLvlCategory() == false){
                        alert("Please select last level category.");
                        return false;
                }
        }
        //alert(divid+','+ajaxloaderid+','+category_id);
        document.getElementById(ajaxloaderid).style.display = "block";
	if(divid == ""){ return false; }
        var url = admin_web_url+'ajax/clinck_slideshow_dashboard.php';
       // alert(url+'?catid='+category_id+'&startlimit='+startlimit+'&cnt='+cnt);
        $.ajax({
		url: url,
                data: 'catid='+category_id+'&startlimit='+startlimit+'&cnt='+cnt,
                success: function(data){
                	//alert(data);
	                document.getElementById(divid).innerHTML = data;
        	        document.getElementById(divid).style.display="block";
                	document.getElementById(ajaxloaderid).style.display = "none";
    		},
		async:false
        });
        return true;
}

function validateProduct(){
	if(document.getElementById('actiontype')){
                document.getElementById('actiontype').value = 'Insert';
        }
        if(document.getElementById('select_product_id')!="undefined"){
		if((document.getElementById('select_product_id').value) == "0"){
                        alert("Please select slide from slide list.");
                        return false;
		}
	}
        return true;
}

function updateProductSlide(divid,ajaxloaderid,product_slide_id,category_id){
        if(category_id == ''){
        	var category_id = document.getElementById('selected_category_id').value;
                if(isCategorySelected() == false){
                        alert("Please select the category.");
                        return false;
                }
                if(isLastLvlCategory() == false){
                        alert("Please select last level category.");
                        return false;
                }
        }
        //alert(divid+','+ajaxloaderid+','+category_id);
        var categoryid='';
        document.getElementById(ajaxloaderid).style.display = "block";
	if(divid == ""){ return false; }
        var url = admin_web_url+'ajax/clinck_slideshow_dashboard.php';
	//alert(url+'?act=update&product_slide_id='+product_slide_id+'&catid='+category_id);
	$.ajax({
        	url: url,
                data: 'act=update&product_slide_id='+product_slide_id+'&catid='+category_id,
                success: function(data){
                //alert(data);
                document.getElementById(divid).innerHTML = data;
                document.getElementById(divid).style.display="block";
                document.getElementById(ajaxloaderid).style.display = "none";
		
            },
            async:false
        });
	document.getElementById('actiontype').value = 'Update';
        return true;
}

function deleteProductSlide(section_slide_id){
        document.getElementById('actiontype').value = 'Delete';
	document.getElementById('hd_section_slide_id').value = section_slide_id;
        var answer = confirm ("Are you sure.Want to delete this clinck slide?")
        if (answer){
                document.product_manage.submit();
                return true;
        }
        return false;
}
	
