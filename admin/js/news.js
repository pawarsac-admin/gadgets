function tiny(){
	tinyMCE.init({
		// General options
		mode : "textareas",
		theme : "advanced",
		plugins : "spellchecker,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",

		// Theme options
		theme_advanced_buttons1 : "bold,italic,underline,link,unlink",
		theme_advanced_buttons2 : "",
		theme_advanced_buttons3 : "",
		theme_advanced_buttons4 : "",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : true,

		// Skin options
		skin : "o2k7",
		skin_variant : "silver",

		// Example content CSS (should be your site CSS)
		content_css : "css/example.css",

		// Drop lists for link/image/media/template dialogs
		template_external_list_url : "js/template_list.js",
		external_link_list_url : "js/link_list.js",
		external_image_list_url : "js/image_list.js",
		media_external_list_url : "js/media_list.js",

		// Replace values for the template plugin
		template_replace_values : {
		username : "Some User",
		staffid : "991234"
		}
	});
}
function getProductArticleDashboard(divid,ajaxloaderid,category_id,startlimit,cnt){
	if(category_id == ''){
		var category_id = document.getElementById('selected_category_id').value;
		if(isCategorySelected() == false){
			alert("Please select the category.");
			return false;
		}
		if(isLastLvlCategory() == false){
			alert("Please select last level category.");
			return false;
		}
	}
	document.getElementById(ajaxloaderid).style.display = "block";
	if(divid == ""){ return false; }
	var url = admin_web_url+'ajax/prod_news_dashboard.php';
	//alert(url+'?catid='+category_id+'&startlimit='+startlimit+'&cnt='+cnt)
	$.ajax({
		url: url,
		data: 'catid='+category_id+'&startlimit='+startlimit+'&cnt='+cnt,
		success: function(data){
		//alert(data);
		document.getElementById(divid).innerHTML = data;
		document.getElementById(divid).style.display="block";
		document.getElementById(ajaxloaderid).style.display = "none";
		},
		async:false
	});
	//tiny();
	return true;
}

function getArticleDetail(article_id,category_id){
	if(category_id == ''){
		var category_id = document.getElementById('selected_category_id').value;

		if(isCategorySelected() == false){
			alert("Please select the category.");
			return false;
		}
		if(isLastLvlCategory() == false){
			alert("Please select last level category.");
			return false;
		}
	}
	window.location.href=admin_web_url+"news.php?act=update&article_id="+article_id+'&catid='+category_id;
	return true;
}

function updateProductArticle(divid,ajaxloaderid,articleid,productid,product_info_id,categoryid,brandid,startlimit,cnt){
	if(category_id == ''){
		var category_id = document.getElementById('selected_category_id').value;
		if(isCategorySelected() == false){
			alert("Please select the category.");
			return false;
		}
		if(isLastLvlCategory() == false){
			alert("Please select last level category.");
			return false;
		}
	}
	
	//alert(divid+','+ajaxloaderid+','+category_id);
	document.getElementById(ajaxloaderid).style.display = "block";
	if(divid == ""){ return false; }
	var url = admin_web_url+'ajax/prod_news_dashboard.php';
	$.ajax({
		url: url,
		data: 'act=update&aid='+articleid+'&catid='+categoryid+'&bid='+brandid+'&pid='+productid+'&startlimit='+startlimit+'&cnt='+cnt,
		success: function(data){
		document.getElementById(divid).innerHTML = data;
		document.getElementById(divid).style.display="block";
		document.getElementById(ajaxloaderid).style.display = "none";
		},
		async:false
	});
	//tiny();
	document.getElementById('actiontype').value = 'Update';
	document.getElementById('select_brand_id').disabled="disabled";
	if(document.getElementById('select_brand_id')!='0'){
	if(product_info_id!=''){
		getModelByBrand(ajaxloaderid,product_info_id);
		if(document.getElementById('select_model_id')){
			document.getElementById('select_model_id').disabled="disabled";
		}
	}
		if(document.getElementById('select_model_id')!='0'){
			if(productid!=''){
				getVariantByModel(ajaxloaderid,productid);
				if(document.getElementById('product_id')){
					document.getElementById('product_id').disabled="disabled";	
				}
			}
		}
	}	
	return true;
}


function deleteProductArticle(articleid){
	document.getElementById('actiontype').value = 'Delete';
	document.getElementById('article_id').value = articleid;
	var answer = confirm ("Are you sure.Want to delete news?")
	if (answer){
	document.product_manage.submit();
	return true;
	}
	return false;
}

function validateProduct(){
	document.getElementById('actiontype').value = 'Insert';
	if(document.getElementById('selected_category_id').value == ''){
	alert("Please select the category.");
	return false;
	}
	if(document.getElementById('article_title').value == ''){
		alert("Please enter title");
		return false;
	}
	
	var iTotalRowsCurrent = document.getElementById('display_rows').value;
	var articleabstract = tinyMCE.get('mce_article_abstract').getContent();
	//alert(articleabstract);
	document.getElementById('article_abstract').value = articleabstract;
	for(i=1;i<=iTotalRowsCurrent;i++){
		document.getElementById('article_description_'+i).value = tinyMCE.get('mce_article_description_'+i).getContent();  
		var chk_flag = document.getElementById('box_'+i).checked;
                if(chk_flag == true){
                        document.getElementById('check_flag_'+i).value = "1";
                }else{
                        document.getElementById('check_flag_'+i).value = "0";
                }
	}
	
	return true;
}


function getModelByBrand(ajaxloaderid,product_name_id){
	var brand_id = document.getElementById('select_brand_id').value;
	if(brand_id == '' ||  brand_id == 0){return false;}
	var category_id = document.getElementById('selected_category_id').value;
	if(category_id == '' ||  category_id == 0){return false;}
	document.getElementById(ajaxloaderid).style.display = "block";
	var url = admin_web_url+'ajax/select_model.php';
	var html = $.ajax({ url: url, data: 'category_id='+category_id+'&brand_id='+brand_id+'&product_name_id='+product_name_id, success: function(data){ document.getElementById(ajaxloaderid).style.display = "none";}, async: false}).responseText;
	var table = document.getElementById("Update");
	var rowCount = 1;
	var rowId='product_row_id_'+rowCount;
	
	if(document.getElementById(rowId)){
		removeTr(rowId);
	}
	

	var row = table.insertRow(rowCount);
	row.id = rowId;
	var product_name = row.insertCell(0);
	product_name.innerHTML = 'Model Name';
	var product_name_value = row.insertCell(1);
	product_name_value.colSpan = 10;
	product_name_value.innerHTML = html;

	var rowCount1 = 2;
	var rowId1='product_row_id_'+rowCount1;
	//alert(rowId1)
	if(document.getElementById(rowId1)){
	removeTr(rowId1);
	}
	return true;
}

function getVariantByModel(ajaxloaderid,product_id){
	var brand_id = document.getElementById('select_brand_id').value;
	if(brand_id == '' ||  brand_id == 0){return false;}
	var product_name_id="";
	if(document.getElementById('select_model_id')){	
		product_name_id = document.getElementById('select_model_id').value;
	}
	if(product_name_id == '' ||  product_name_id == 0){return false;}
	var category_id = document.getElementById('selected_category_id').value;
	if(category_id == '' ||  category_id == 0){return false;}
	document.getElementById(ajaxloaderid).style.display = "block";
	var url = admin_web_url+'ajax/select_variant.php';
	var html = $.ajax({ url: url, data: 'category_id='+category_id+'&brand_id='+brand_id+'&product_id='+product_id+'&product_name_id='+product_name_id, success: function(data){ document.getElementById(ajaxloaderid).style.display = "none";}, async: false}).responseText;
	var table = document.getElementById("Update");
	var rowCount = 2;
	var rowId='product_row_id_'+rowCount;
	if(document.getElementById(rowId)){
	removeTr(rowId);
	}
	var row = table.insertRow(rowCount);
	row.id = rowId;
	var product_name = row.insertCell(0);
	product_name.innerHTML = 'Product Name';
	var product_name_value = row.insertCell(1);
	product_name_value.colSpan = 10;
	product_name_value.innerHTML = html;
	return true;
}

function remove_product_row(rowCount){	
	if(rowCount == 0){return false;}
	if(document.getElementById('product_remove_linkrow_id_'+rowCount)){
		removeTr('product_remove_linkrow_id_'+rowCount);
	}
	if(document.getElementById('product_status_row_id_'+rowCount)){
		removeTr('product_status_row_id_'+rowCount);
	}
	if(document.getElementById('product_style_row_id_'+rowCount)){
		removeTr('product_style_row_id_'+rowCount);
	}
	if(document.getElementById('product_style_row_id_'+rowCount)){
		removeTr('product_style_row_id_'+rowCount);
	}
	if(document.getElementById('product_desc_row_id_'+rowCount)){
		removeTr('product_desc_row_id_'+rowCount);
	}
	if(document.getElementById('product_group_row_id_'+rowCount)){
		removeTr('product_group_row_id_'+rowCount);
	}
	if(document.getElementById('product_name_row_id_'+rowCount)){
		removeTr('product_name_row_id_'+rowCount);
	}
	return false;
}

function getUploadData (sFrm,sTitle,sId,sPath,mType,sImageCat){
	window.open('get_upload.php?rfrm='+sFrm+'&rtitle='+sTitle+'&rpath='+sPath+'&rid='+sId+'&rtype='+mType+'&rimgcat='+sImageCat,'mywindow','width=600,height=300,left=300,top=300');
}
function getUploadedDataList (sFrm,sTitle,sId,sPath,mType,sImageCat){
	window.open('search_store.php?rfrm='+sFrm+'&rtitle='+sTitle+'&rpath='+sPath+'&rid='+sId+'&rtype='+mType+'&rimgcat='+sImageCat,'mywindow','width=600,height=400,left=300,top=300');
}

function sArticlePagination(page,startlimit,cnt,filename,divid,category_id){
	
	if(category_id == ''){
		var category_id = document.getElementById('selected_category_id').value;

		if(isCategorySelected() == false){
			alert("Please select the category.");
			return false;
		}
		if(isLastLvlCategory() == false){
			alert("Please select last level category.");
			return false;
		}
	}
	if(divid == ""){ return false; }
		var url = admin_web_url+filename;

		$.ajax({
			url: url,
			data: 'catid='+category_id+'&page='+page+'&startlimit='+startlimit+'&cnt='+cnt,
			success: function(data){
				//alert(data);
                document.getElementById(divid).innerHTML = data;
                document.getElementById(divid).style.display="block";
               },
            async:false
        });
	return true;
}

function getProductArticle(category_id){
	if(category_id == ''){
		var category_id = document.getElementById('selected_category_id').value;

		if(isCategorySelected() == false){
			alert("Please select the category.");
			return false;
		}
		if(isLastLvlCategory() == false){
			alert("Please select last level category.");
			return false;
		}
	}
	window.location.href=admin_web_url+"news.php?catid="+category_id;
	return true;
}
