function getVideoCategoryDashboard(divid,ajaxloaderid,category_id,startlimit,cnt){
	if(category_id == ''){
		var category_id = document.getElementById('selected_category_id').value;

		if(isCategorySelected() == false){
			alert("Please select the category.");
			return false;
		}
		if(isLastLvlCategory() == false){
			alert("Please select last level category.");
			return false;
		}
	}
	
	//alert(divid+','+ajaxloaderid+','+category_id);
	document.getElementById(ajaxloaderid).style.display = "block";
	if(divid == ""){ return false; }
	var url = admin_web_url+'ajax/video_category_dashboard.php';
	//alert(url+'?catid='+category_id+'&startlimit='+startlimit+'&cnt='+cnt)
        $.ajax({
		url: url,
		data: 'catid='+category_id+'&startlimit='+startlimit+'&cnt='+cnt,
		success: function(data){
		//alert(data);
                document.getElementById(divid).innerHTML = data;
                document.getElementById(divid).style.display="block";
                document.getElementById(ajaxloaderid).style.display = "none";
            },
            async:false
        });
	return true;
}
function updateVideoTab(divid,ajaxloaderid,id,category_id){

        if(category_id == ''){
                var category_id = document.getElementById('selected_category_id').value;
		if(category_id == ''){
	                alert("Please select category.");
        	        return false;
		}
        }
        document.getElementById(ajaxloaderid).style.display = "block";
	if(divid == ""){ return false; }
        var url = admin_web_url+'ajax/video_category_dashboard.php';
	//alert(url+'?act=update&id='+id+'&catid='+category_id);
        $.ajax({
                url: url,
                data: 'act=update&id='+id+'&catid='+category_id,

                success: function(data){
                //alert(data);
                document.getElementById(divid).innerHTML = data;
                document.getElementById(divid).style.display="block";
                document.getElementById(ajaxloaderid).style.display = "none";
            },
            async:false
        });
	if(document.getElementById('hd_video_tab_id')){
                document.getElementById('hd_video_tab_id').value = id;
        }
	if(document.getElementById('actiontype')){
                document.getElementById('actiontype').value = 'Update';
        }
        //alert(document.getElementById('actiontype').value);
        return true;
}
function deleteVideoTab(id){
       document.getElementById('actiontype').value = 'Delete';

        document.getElementById('hd_video_tab_id').value = id;
        var answer = confirm ("Are you sure.Want to delete this video category?")
        if (answer){
                document.product_manage.submit();
                return true;
        }
        return false;
}

function validateVideoTab(){
	if(document.getElementById('actiontype')){
		if(document.getElementById('actiontype').value == ""){
	                document.getElementById('actiontype').value = 'Insert';
		}
        }
	if(document.getElementById("video_cat_name").value == ""){
                alert("Please Enter Video Category Name.");
                return false;
        }
	if(document.getElementById("order_tab").value == ""){
                alert("Please Select Ordering.");
                return false;
        }
        return true;
}

/*function soVideosPagination(page,startlimit,cnt,filename,divid,category_id,type_selected){
	if(category_id == ''){
		var category_id = document.getElementById('selected_category_id').value;

		if(isCategorySelected() == false){
			alert("Please select the category.");
			return false;
		}
		if(isLastLvlCategory() == false){
			alert("Please select last level category.");
			return false;
		}
	}
		
	if(divid == ""){ return false; }
		var url = admin_web_url+filename;
		
		$.ajax({
			url: url,
			data: 'catid='+category_id+'&page='+page+'&startlimit='+startlimit+'&cnt='+cnt+'&video_type_id='+type_selected,
			success: function(data){
				//alert(data);
                document.getElementById(divid).innerHTML = data;
                document.getElementById(divid).style.display="block";
               },
            async:false
        });
	
	return true;
}*/
