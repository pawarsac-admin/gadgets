function getProductModelDashboard(divid,ajaxloaderid,category_id,startlimit,cnt){
	
	if(category_id == ''){
		var category_id = document.getElementById('selected_category_id').value;

		if(isCategorySelected() == false){
			alert("Please select the category.");
			return false;
		}
		if(isLastLvlCategory() == false){
			alert("Please select last level category.");
			return false;
		}
	}
	
	//alert(divid+','+ajaxloaderid+','+category_id);
	document.getElementById(ajaxloaderid).style.display = "block";
    if(divid == ""){ return false; }
		var url = admin_web_url+'ajax/model_dashboard.php';
		//alert(url+'?catid='+category_id+'&startlimit='+startlimit+'&cnt='+cnt)
        $.ajax({
			url: url,
			data: 'catid='+category_id+'&startlimit='+startlimit+'&cnt='+cnt,
			success: function(data){
				//alert(data);
                document.getElementById(divid).innerHTML = data;
				
                document.getElementById(divid).style.display="block";
                document.getElementById(ajaxloaderid).style.display = "none";
            },
            async:false
        });
	tiny();
	return true;
}

function updateProductModel(divid,ajaxloaderid,mid,categoryid,brandid,startlimit,cnt){

	if(category_id == ''){
		var category_id = document.getElementById('selected_category_id').value;

		if(isCategorySelected() == false){
			alert("Please select the category.");
			return false;
		}
		if(isLastLvlCategory() == false){
			alert("Please select last level category.");
			return false;
		}
	}
	//alert(divid+','+ajaxloaderid+','+category_id);
	document.getElementById(ajaxloaderid).style.display = "block";
    if(divid == ""){ return false; }
		var url = admin_web_url+'ajax/model_dashboard.php';
        $.ajax({
			url: url,
			data: 'act=update&product_name_id='+mid+'&catid='+categoryid+'&startlimit='+startlimit+'&cnt='+cnt+'&actiontype=Update',
				
			success: function(data){
				//alert(data);
                document.getElementById(divid).innerHTML = data;
                document.getElementById(divid).style.display="block";
                document.getElementById(ajaxloaderid).style.display = "none";
            },
            async:false
        });
		tiny();
	//getProductByBrand('ajaxloader',productid);
	//getModelByBrand(ajaxloaderid,product_info_id);
	//getVariantByModel(ajaxloaderid,productid);
	return true;
}



function deleteProductModel(mid){
	   	document.getElementById('actiontype').value = 'Delete';
		document.getElementById('product_name_id').value = mid;
		//alert(document.getElementById('product_name_id').value);return false;
    	var answer = confirm ("Are you sure.Want to delete model?")
    	if (answer){
         	document.product_manage.submit();
         	return true;
     	}
	return false;
}
function validateProduct(){
	if(document.getElementById('select_brand_id').value == ''){
		alert("Please select the brand.");
		return false;
	}
	if(document.getElementById('model_title').value == ''){
		alert("Please add the model");
		return false;
	}		
	return true;
}
function getUploadData (sFrm,sTitle,sId,sPath,mType,sImageCat){
	window.open('get_upload.php?rfrm='+sFrm+'&rtitle='+sTitle+'&rpath='+sPath+'&rid='+sId+'&rtype='+mType+'&rimgcat='+sImageCat,'mywindow','width=600,height=300,left=300,top=300');
}
function getUploadedDataList (sFrm,sTitle,sId,sPath,mType,sImageCat){
	window.open('search_store.php?rfrm='+sFrm+'&rtitle='+sTitle+'&rpath='+sPath+'&rid='+sId+'&rtype='+mType+'&rimgcat='+sImageCat,'mywindow','width=600,height=400,left=300,top=300');
}

function sOProduOverPagination(page,startlimit,cnt,filename,divid,category_id){

 
  if(category_id == ''){
		var category_id = document.getElementById('selected_category_id').value;

		if(isCategorySelected() == false){
			alert("Please select the category.");
			return false;
		}
		if(isLastLvlCategory() == false){
			alert("Please select last level category.");
			return false;
		}
	}
		
	if(divid == ""){ return false; }
		var url = admin_web_url+filename;

		$.ajax({
			url: url,
			data: 'catid='+category_id+'&page='+page+'&startlimit='+startlimit+'&cnt='+cnt,
			success: function(data){
				//alert(data);
                document.getElementById(divid).innerHTML = data;
                document.getElementById(divid).style.display="block";
               },
            async:false
        });
	
	return true;
}