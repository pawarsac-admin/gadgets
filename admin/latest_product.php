<?php
	require_once('../include/config.php');
	//require_once(CLASSPATH.'category.class.php');
	require_once(CLASSPATH.'DbConn.php');
	require_once(CLASSPATH.'product.class.php');
	$dbconn = new DbConn;
	
	$oProduct = new ProductManagement;
	
	$actiontype = $_REQUEST['actiontype'];
	$startlimit = $_REQUEST['startlimit'];
	$limitcnt = $_REQUEST['cnt'];
	$request_param=$_REQUEST;
	$cayegory_id=$_REQUEST['selected_category_id'];
	$latest_product_id=$_REQUEST['latest_product_id'];
	if($_POST['product_id']!='' && strlen($_POST['product_id'])>0){
		$aParameters = array("category_id"=>$_REQUEST['selected_category_id'],"brand_id"=>$_REQUEST['select_brand_id'],"product_id"=>$_REQUEST['product_id'],"product_position"=>$_REQUEST['product_position'],"status"=>$_REQUEST['product_status'] );
		$latest_product_id==0 ? $aParameters['create_date']=date("Y-m-d H:i:s") : $aParameters['update_date']=date("Y-m-d H:i:s"); 
		if($latest_product_id>0) $aParameters['latest_product_id']=$latest_product_id;
		$iResId=$oProduct->intInsertLatestProduct($aParameters);
	 	$latest_product_id=$iResId;
		
		if($latest_product_id==0) {
			$latest_product_id=$iResId;
			$msg="Product detail added successfully.";
		}else {
			$msg="Product detail updated successfully.";
		}
	
	}
	if($actiontype == 'Delete'){
	   $result = $oProduct->boolDeleteLatestProduct($latest_product_id);
	   $msg = 'Product deleted successfully.';
	}
	
$startlimit = $_REQUEST['startlimit'];
$limitcnt = $_REQUEST['cnt'];
$config_details = get_config_details();

$strXML = "<XML>";
$strXML .= "<MSG><![CDATA[$msg]]></MSG>";
$strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$cayegory_id]]></SELECTED_CATEGORY_ID>";
$strXML .= "<STARTLIMIT><![CDATA[$startlimit]]></STARTLIMIT>";
$strXML .= "<CNT><![CDATA[$limitcnt]]></CNT>";
$strXML .= $config_details;
$strXML .= $xml;
$strXML .= "</XML>";
//header('Content-type: text/xml');echo $strXML;exit;
$doc = new DOMDocument();
$doc->loadXML($strXML);
$doc->saveXML();
$xslt = new xsltProcessor;
$xsl = DOMDocument::load('xsl/latest_product.xsl');
$xslt->importStylesheet($xsl);
print $xslt->transformToXML($doc);
?>
