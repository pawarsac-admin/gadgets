<?php
require_once("../include/config.php");
require_once(CLASSPATH.'DbConn.php');
require_once(CLASSPATH."category.class.php");
require_once(CLASSPATH."brand.class.php");
require_once(CLASSPATH."citystate.class.php");
require_once(CLASSPATH."product.class.php");
require_once(CLASSPATH."price.class.php");

$dbconn 	= new DbConn;
$oCategory	= new CategoryManagement;
$oBrand		= new BrandManagement;
$oProduct	= new ProductManagement;
$oCityState = new citystate;
$oPrice	= new price;

//if($_POST){ print_r($_REQUEST);  } //die();

$actiontype = $_REQUEST['actiontype'] ? $_REQUEST['actiontype'] : "Insert";
$startlimit = $_REQUEST['startlimit'];
$limitcnt = $_REQUEST['cnt'];
$price_variant=$_REQUEST['price_variant'];
if(!empty($price_variant)){
	$request_param['price_variant'] = trim($price_variant);
}
$variant_id=$_REQUEST['variant_id'];
if(!empty($variant_id)){
	$request_param['variant_id'] = $variant_id;
}
$variant_value=$_REQUEST['variant_value'];
if($variant_value !=''){
	$request_param['variant_value'] = trim($variant_value);
}
$category_id=$_REQUEST['selected_category_id']? $_REQUEST['selected_category_id'] : SITE_CATEGORY_ID;;
if(!empty($category_id)){
	$request_param['category_id'] = $category_id;
}
$brand_id=$_REQUEST['select_brand_id'];
if(!empty($brand_id)){
	$request_param['brand_id'] = $brand_id;
}
$product_id=$_REQUEST['product_id'];
if(!empty($product_id)){
	$request_param['product_id'] = $product_id;
}
$country_id=$_REQUEST['select_country_id'];
if(!empty($country_id)){
	$request_param['country_id'] = $country_id;
}
$state_id=$_REQUEST['select_state_id'];
if(!empty($state_id)){
	$request_param['state_id'] = $state_id;
}
$city_id=$_REQUEST['city_id'];
if(!empty($city_id)){
	$request_param['city_id'] = $city_id;
}
$status=$_REQUEST['status'];
if($status!=''){
	$request_param['status'] = $status;
}
$default_city=$_REQUEST['default_city'];
if($status!=''){
	$request_param['default_city'] = $default_city;
}
$r_product_id = $_REQUEST['product_id'];

if($actiontype=="Insert"){
	$iResId=$oPrice->intInsertVariantValueDetail($request_param);
	$msg="Price Variant detail added successfully.";
}
else if($actiontype=="Update"){
	$iResId=$oPrice->intInsertVariantValueDetail($request_param);
	$msg="Price Variant updated successfully.";
}
else if ($actiontype=="Delete"){
	$dresult = $oPrice->boolDeleteVariantValueDetail($price_variant,$r_product_id);
	$msg = 'Price variant Data deleted successfully.';
}

$selected_brand_id = $_REQUEST['select_brand'] ? $_REQUEST['select_brand'] : "0"  ;
$selected_model_id = $_REQUEST['Model'] ? $_REQUEST['Model'] : "0" ;
$selected_variant_id = $_REQUEST['Variant'] ? $_REQUEST['Variant'] : "0" ;
$city_id = $_REQUEST['city_id'] ? $_REQUEST['city_id']: "0";

$state_id = $_REQUEST['state_id'];

$config_details = get_config_details();
$strXML = "<XML>";
$strXML .= "<MSG><![CDATA[$msg]]></MSG>";
$strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$category_id]]></SELECTED_CATEGORY_ID>";
$strXML .= "<SELECTED_ACTION_TYPE><![CDATA[$actiontype]]></SELECTED_ACTION_TYPE>";
$strXML .= "<STARTLIMIT><![CDATA[$startlimit]]></STARTLIMIT>";
$strXML .= "<CNT><![CDATA[$limitcnt]]></CNT>";
$strXML .= "<SELECTED_BRAND_ID><![CDATA[$selected_brand_id]]></SELECTED_BRAND_ID>";
$strXML .= "<SELECTED_MODEL_ID><![CDATA[$selected_model_id]]></SELECTED_MODEL_ID>";
$strXML .= "<SELECTED_VARIANT_ID><![CDATA[$selected_variant_id]]></SELECTED_VARIANT_ID>";
$strXML .= "<SELECTED_CITY_ID><![CDATA[$city_id]]></SELECTED_CITY_ID>";

$strXML .= $config_details;
$strXML .= $xml;
$strXML .= "</XML>";

$strXML = mb_convert_encoding($strXML, "UTF-8");
if($_GET['debug']==1) { header('Content-type: text/xml');echo $strXML;exit; }

$doc = new DOMDocument();
$doc->loadXML($strXML);
$doc->saveXML();

$xslt = new xsltProcessor;
$xsl = DOMDocument::load('xsl/variant_value.xsl');

$xslt->importStylesheet($xsl);
print $xslt->transformToXML($doc);
?>