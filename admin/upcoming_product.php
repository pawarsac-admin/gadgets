<?php
	require_once('../include/config.php');
	//require_once(CLASSPATH.'category.class.php');
	require_once(CLASSPATH.'DbConn.php');
	require_once(CLASSPATH.'product.class.php');
	$dbconn = new DbConn;
	
	$oProduct = new ProductManagement;
	$actiontype = $_REQUEST['actiontype'];
	$startlimit = $_REQUEST['startlimit'];
    $limitcnt = $_REQUEST['cnt'];
	$request_param=$_REQUEST;
	$cayegory_id=$_REQUEST['selected_category_id'];
	$upcoming_product_id=$_REQUEST['upcoming_product_id'];
	if($_POST['product_id']!='' && strlen($_POST['product_id'])>0){
	
		$aParameters = array("category_id"=>$_REQUEST['selected_category_id'],"brand_id"=>$_REQUEST['select_brand_id'],"product_id"=>$_REQUEST['product_id'],"product_position"=>$_REQUEST['product_position'],"status"=>$_REQUEST['product_status'] );
		$latest_product_id==0 ? $aParameters['create_date']=date("Y-m-d H:i:s") : $aParameters['update_date']=date("Y-m-d H:i:s"); 
		if($upcoming_product_id>0) $aParameters['upcoming_product_id']=$upcoming_product_id;
		$iResId=$oProduct->intInsertUpComingProduct($aParameters);
	 	$upcoming_product_id=$iResId;
		
		if($upcoming_product_id==0) {
			$upcoming_product_id=$iResId;
			$msg="Product detail added successfully.";
		}else {
			$msg="Product detail updated successfully.";
		}
	
	}

	if($actiontype == 'Delete'){
		$result = $oProduct->boolDeleteUpComingProduct($upcoming_product_id);
	   $msg = 'Product deleted successfully.';
	}
	
$startlimit = $_REQUEST['startlimit'];
$limitcnt = $_REQUEST['cnt'];
$config_details = get_config_details();

$strXML = "<XML>";
$strXML .= "<MSG><![CDATA[$msg]]></MSG>";
$strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$category_id]]></SELECTED_CATEGORY_ID>";
$strXML .= "<STARTLIMIT><![CDATA[$startlimit]]></STARTLIMIT>";
$strXML .= "<CNT><![CDATA[$limitcnt]]></CNT>";
$strXML .= $config_details;
$strXML .= $xml;
$strXML .= "</XML>";

$doc = new DOMDocument();
$doc->loadXML($strXML);
$doc->saveXML();

$xslt = new xsltProcessor;
$xsl = DOMDocument::load('xsl/upcoming_product.xsl');

$xslt->importStylesheet($xsl);
print $xslt->transformToXML($doc);
?>