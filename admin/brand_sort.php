<?php
require_once('../include/config.php');
require_once(CLASSPATH.'DbConn.php');
require_once(CLASSPATH.'brand.class.php');

$dbconn			= new DbConn;
$oBrand			= new BrandManagement;

$action			= $_REQUEST['action'];
$brands			= $_REQUEST['brands'];

$arrBrand		= explode(',',$brands);
$arrBrand		= array_filter($arrBrand);

//print_r($arrBrand);

if($action){
	if($action=='rearrange'){
		$op		= $oBrand->updatePosition( $arrBrand, $dbconn);
		$msg	= date("Y-m-d H:i:s") . ': Brands Rearranged .';
		echo $msg; die;
	}
}
?>