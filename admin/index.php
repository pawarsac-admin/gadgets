<?php
	error_reporting(E_ALL);
	require_once('../include/config.php');
	/*
	require_once(CLASSPATH.'DbConn.php');
	require_once(CLASSPATH.'category.class.php');

	$dbconn = new DbConn;
	$category = new CategoryManagement;
*/

	$config_details = get_config_details();
	$strXML = "<XML>";
	$strXML .= $config_details;
	$strXML .= "</XML>";
	if($_GET['debug']==2){
		header('content-type:text/xml');
		echo $strXML;
		die;
	}
	$doc = new DOMDocument();
	$doc->loadXML($strXML);
	$doc->saveXML();
	$xslt = new xsltProcessor;
	$xsl = DOMDocument::load('xsl/index.xsl');
	$xslt->importStylesheet($xsl);
	print $xslt->transformToXML($doc);
?>
