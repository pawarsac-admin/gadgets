<?php
require_once('../include/config.php');
require_once(CLASSPATH.'DbConn.php');
require_once(CLASSPATH.'topstories.class.php');
require_once(CLASSPATH.'product.class.php');

$dbconn		 = new DbConn;
$oTopStories = new TopStories;
$oProduct	= new ProductManagement;

$group_type	= 2;
$action		= $_REQUEST['action'];
$group_id	= ($_REQUEST['group_id'])? $_REQUEST['group_id'] : 0;

$category_id			= $_REQUEST['selected_category_id'] ? $_REQUEST['selected_category_id'] : $_REQUEST['catid'];
$selected_brand_id		= $_REQUEST['select_brand'] ;
$selected_model_id		= $_REQUEST['Model'] ;
$selected_variant_id	= $_REQUEST['Variant'] ;

if($action){
	if($action=='rearrange'){
		$op	= $oTopStories->updatePosition( $group_id, $_REQUEST['listItem'],$dbconn);
		$msg = date("Y-m-d H:i:s") . ': Stories Rearranged .';
		echo $msg; die;
	} else if($action== 'update'){
		$op	= $oTopStories->updateStories( $group_id, $_REQUEST,$dbconn);
		$msg = date("Y-m-d H:i:s") . ': Stories Updated .';
	} else if($action== 'createnewgroup'){
		$grouptitle		= $_REQUEST['grouptitle'];
		$noofstories	= 0;//$_REQUEST['noofstories'];
		if(empty($grouptitle) OR empty($grouptitle) ) {
			$msg = date("Y-m-d H:i:s") . ':\'Group title\' and/or \'No. Of Stories\' Should Not Be Empty .';
		} else {
			$op	= $oTopStories->createGroup( $group_type, $grouptitle, $noofstories, $dbconn);
			if( is_numeric($op) AND $op > 0 ) $group_id	= $op;
			$msg = date("Y-m-d H:i:s") . ': Group Created .';
		}
	}
}

if( $group_id==0 )	$group_id = $oTopStories->getLatestGroupId( $group_type, $dbconn );

$config_details = get_config_details();

$storyGroupXML	= $oTopStories->getStoryGroupsXML( $group_type, $dbconn );
$storyXML		= $oTopStories->getStoriesByGroupXML( $group_id, $dbconn);

$productsXML	= $oProduct->arrGetProductDetailsXML('','','',1,'','',1,'','',1,'ORDER BY PRODUCT_MASTER.product_id DESC');

$strXML = "<XML>";
$strXML .= "<MSG><![CDATA[$msg]]></MSG>";
$strXML .= "<GROUP_ID><![CDATA[$group_id]]></GROUP_ID>";
$strXML .= "<STARTLIMIT><![CDATA[$startlimit]]></STARTLIMIT>";
$strXML .= "<SELECTED_ACTION_TYPE><![CDATA[$actiontype]]></SELECTED_ACTION_TYPE>";
$strXML .= "<CNT><![CDATA[$limitcnt]]></CNT>";

$strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$category_id]]></SELECTED_CATEGORY_ID>"; 
$strXML .= "<SELECTED_BRAND_ID><![CDATA[$selected_brand_id]]></SELECTED_BRAND_ID>";
$strXML .= "<SELECTED_MODEL_ID><![CDATA[$selected_model_id]]></SELECTED_MODEL_ID>";
$strXML .= "<SELECTED_VARIANT_ID><![CDATA[$selected_variant_id]]></SELECTED_VARIANT_ID>";

$strXML .= $config_details;
$strXML .= $storyGroupXML;
$strXML .= $storyXML;
$strXML .= $productsXML;
$strXML .= "</XML>";

$strXML = mb_convert_encoding($strXML, "UTF-8");
if($_GET['debug']==1) { header('Content-type: text/xml');echo $strXML;exit; }

$doc = new DOMDocument();
$doc->loadXML($strXML);
$doc->saveXML();

$xslt = new xsltProcessor;
$xsl = DOMDocument::load('xsl/custom_top_products.xsl');

$xslt->importStylesheet($xsl);
print $xslt->transformToXML($doc);

?>

