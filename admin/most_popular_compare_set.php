<?php
require_once('../include/config.php');
require_once(CLASSPATH.'DbConn.php');
require_once(CLASSPATH.'product.class.php');

$dbconn = new DbConn;
$oProduct = new ProductManagement;

//if($_POST){ print_r($_REQUEST);}  die();
$actiontype = $_REQUEST['actiontype'];
$startlimit = $_REQUEST['startlimit'];
$limitcnt = $_REQUEST['cnt'];
$request_param=$_REQUEST;
$_REQUEST['product_id'] !=0 ? $_REQUEST['product_id'] :0;
$iCmptId=$_REQUEST['competitor_product_id'];

if($_POST['comp_product_id']!='' && strlen($_POST['comp_product_id'])>0){
	if($status != ''){$request_param['status'] = $status;}
	$aParameters = array("category_id"=>$_REQUEST['selected_category_id'],"brand_id"=>$_REQUEST['select_brand_id'],"product_id"=>$_REQUEST['product_id'],"product_info_id"=>$_REQUEST['select_model_id'],"product_ids"=>$_REQUEST['comp_product_id'],"status"=>$_REQUEST['status']);;
	$iCmptId==0 ? $aParameters['create_date']=date("Y-m-d H:i:s") : $aParameters['update_date']=date("Y-m-d H:i:s"); 
	if($iCmptId>0) $aParameters['competitor_product_id']=$iCmptId;
	$iResId=$oProduct->addUpdCompareTopCompetitorDetails($aParameters);
	if($iCmptId==0){$iCmptId=$iResId;}
	if($iCmptId==0) {
		$iCmptId=$iResId;
		$msg="Top competitor detail added successfully.";
	}else {
		$msg="Top competitor detail updated successfully.";
	}
}
if($actiontype == 'Delete'){
	$result = $oProduct->boolDeleteCompareTopCompetitorDetail($iCmptId);
	$msg = 'Top competitor Detail deleted successfully.';
}

$config_details = get_config_details();
$strXML = "<?xml version='1.0' encoding='iso-8859-1'?>";
$strXML .= "<XML>";
$strXML .= "<MSG><![CDATA[$msg]]></MSG>";
$strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$brand_level]]></SELECTED_CATEGORY_ID>";
$strXML .= "<STARTLIMIT><![CDATA[$startlimit]]></STARTLIMIT>";
$strXML .= "<CNT><![CDATA[$limitcnt]]></CNT>";
$strXML .= $config_details;
$strXML .= $xml;
$strXML .= "</XML>";
$strXML = mb_convert_encoding($strXML, "UTF-8");
if($_GET['debug']==1) { header('Content-type: text/xml');echo $strXML;exit; }
$doc = new DOMDocument();
$doc->loadXML($strXML);
$doc->saveXML();

$xslt = new xsltProcessor;
$xsl = DOMDocument::load('xsl/top_competitor.xsl');

$xslt->importStylesheet($xsl);
print $xslt->transformToXML($doc);
?>
