<?php
	require_once('../include/config.php');
	require_once(CLASSPATH.'DbConn.php');
	require_once(CLASSPATH.'DbOp.php');

	$action			= $_REQUEST['action'];
	$table			= $_REQUEST['table'];
	$fieldname		= $_REQUEST['fieldname'];
	$primaryfield	= $_REQUEST['primaryfield'];
	$conditionparam = $_REQUEST['conditionparam'];
	$conditionval	= $_REQUEST['conditionval'];

	$debug			= $_REQUEST['debug'];

	if(empty($action) || empty($table) || empty($fieldname) ) {
		echo "<b>Usage :</b> " . $_SERVER["PHP_SELF"]  . '?action=clean&debug=1&table=&fieldname=&primaryfield=&conditionparam=&conditionval='. "<br/><br/>";	
		die("No action/table/fieldname specified.");
	}

	set_time_limit (0);
	define("SPECIAL_CHAR_PATTERN",'[^ a-zA-Z0-9,\.\/\':;"\[\]{}`~@#\$%^&*()_+-=<>\?!]');

	$pattern='/'.SPECIAL_CHAR_PATTERN.'/';

	$dbconn			= new DbConn;
	$DbOperation	= new DbOperation;
	
	if( !empty($conditionpara) && !empty($conditionval) ){
		$condition		= " WHERE $conditionparam = $conditionval ";
	} else {
		$condition		= " WHERE 1=1 ";
	}

	if( !empty($primaryfield)) {
		$primefield		= " $primaryfield , ";
	}

	$sql    = "SELECT $primefield $fieldname FROM $table $condition";
	
	if($_REQUEST['debug']==1) echo "\n<br/>- SQL : ", $sql;

	$op		= $DbOperation->select($sql);
	//if($_REQUEST['debug']==1) print_r($op);
	
	if($op) {

		foreach( $op as $data ){
			$val	= $data[$fieldname];
			if(!empty($val)){
				$val	= preg_replace($pattern, '', $val);

				if( !empty($primaryfield)) {
					$condition1	= $condition . " AND $primaryfield = ' " . $data[$primaryfield] ."' ";
				}

				$sql    = "UPDATE $table SET $fieldname = '$val' $condition1";
				if($_REQUEST['debug']==1) echo "<br/> SQL: " , $sql;
				$id     = $DbOperation->update($sql);
			}
		}

	}

	if($_REQUEST['debug']==1) echo "<br/>Operation Finished.";
?>