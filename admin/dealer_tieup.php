<?php
require_once("../include/config.php");
require_once(CLASSPATH.'DbConn.php');
require_once(CLASSPATH."category.class.php");
require_once(CLASSPATH."brand.class.php");
require_once(CLASSPATH."citystate.class.php");
require_once(CLASSPATH."dealer.class.php");

$dbconn 	= new DbConn;
$oCategory	= new CategoryManagement;
$oBrand		= new BrandManagement;
$oCityState = new citystate;
$oDealer	= new Dealer;

//if($_POST){ print_r($_REQUEST);} ///die();

$actiontype = $_REQUEST['actiontype'];
$startlimit = $_REQUEST['startlimit'];
$limitcnt = $_REQUEST['cnt'];
unset($request_param);
$category_id = $_REQUEST['selected_category_id'] ? $_REQUEST['selected_category_id'] : $_REQUEST['catid'];
if($actiontype == 'Insert'|| $actiontype== 'Update'){
	$status = trim($_REQUEST['status']);
	if($status!=''){ $request_param['status']=$status;}

	$dealer_id = $_REQUEST['select_dealer_id'] ? $_REQUEST['select_dealer_id'] : $dealer_id ;
	if(!empty($dealer_id)){ $request_param['dealer_id']=$dealer_id;}

	if(!empty($category_id)){ $request_param['category_id']=$category_id;}

	$dealer_tieup_id = $_REQUEST['dealer_tieup_id'] ? $_REQUEST['dealer_tieup_id'] : $dealer_tieup_id ;
	if(!empty($dealer_tieup_id)){ $request_param['dealer_tieup_id']=$dealer_tieup_id;}
	//print "<pre>"; print_r($request_param);
	$table_name="DEALERS_TIEUP";
	if($dealer_id!=''){
		$result = $oDealer->addUpdDealerTieupDetails($request_param,$table_name);
	}
	if($actiontype == 'Insert'){
		if($sresult>0){$msg = 'Dealer Tieup added successfully.';}
	}elseif($actiontype == 'Update'){
		if($sresult>0){$msg = 'Dealer Tieup updated successfully.';}
	}
}

if($actiontype == 'Delete'){
	$dealer_tieup_id = $_REQUEST["dealer_tieup_id"];
	$table_name="DEALERS_TIEUP";
	if($dealer_tieup_id!=''){
		$result = $oDealer->boolDeleteDealerTieup($dealer_tieup_id,$table_name);
		$msg = 'Dealer Tieup deleted successfully.';
	}
}

$config_details = get_config_details();

$strXML = "<XML>";
$strXML .= "<MSG><![CDATA[$msg]]></MSG>";
$strXML .= "<SELECTED_MENU_ID><![CDATA[$menu_level]]></SELECTED_MENU_ID>";
$strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$category_id]]></SELECTED_CATEGORY_ID>";
$strXML .= "<SELECTED_ACTION_TYPE><![CDATA[$actiontype]]></SELECTED_ACTION_TYPE>";
$strXML .= "<STARTLIMIT><![CDATA[$startlimit]]></STARTLIMIT>";
$strXML .= "<CNT><![CDATA[$limitcnt]]></CNT>";
$strXML .= $config_details;
$strXML .= $xml;
$strXML .= "</XML>";

if($_GET['debug']==1) { header('Content-type: text/xml');echo $strXML;exit; }

$doc = new DOMDocument();
$doc->loadXML($strXML);
$doc->saveXML();

$xslt = new xsltProcessor;
$xsl = DOMDocument::load('xsl/dealer_tieup.xsl');

$xslt->importStylesheet($xsl);
print $xslt->transformToXML($doc);
?>
