<?php
        require_once('../include/config.php');
        require_once(CLASSPATH.'DbConn.php');
        require_once(CLASSPATH.'videos.class.php');

        $dbconn = new DbConn;
        $videos = new videos; 

        //if($_POST){ print_r($_REQUEST);} //die();
        $actiontype = $_REQUEST['actiontype'];
        $startlimit = $_REQUEST['startlimit'];
        $limitcnt = $_REQUEST['cnt'];
        $request_param=$_REQUEST;
        $category_id = $_REQUEST['selected_category_id'];
        $category_id = ($category_id != "") ? $category_id : SITE_CATEGORY_ID;
        $selected_video_type_id = $_REQUEST["select_section_id"] ? $_REQUEST['select_section_id'] : $_REQUEST['hd_view_section_id'];

        if($actiontype == 'Insert'|| $actiontype== 'Update'){
                unset($request_param);

                $status = $_REQUEST["status"];
                $video_type_id = $_REQUEST["select_section_id"];
                $video_sub_type_name = trim($_REQUEST["select_video_sub_type"]);
                $request_param['video_type_id'] = $video_type_id;
                $request_param['category_id'] = $category_id;
                if(!empty($video_sub_type_name)){
                        $request_param['sub_type_name'] = htmlentities($video_sub_type_name,ENT_QUOTES);
                }
                $request_param['status'] = $status;
                //print"<pre>";print_r($request_param);print"</pre>";
                if($actiontype == 'Insert'){
                        $result = $videos->intInsertVideoSubType($request_param);
                        if($sresult>0){
                                $msg = 'video type added successfully.';
                        }
                }elseif($actiontype == 'Update'){
                        $video_sub_type_id = $_REQUEST['hd_video_sub_type_id'];
                        //print"<pre>";print_r($request_param);print"</pre>";
                        $result = $videos->boolUpdateVideoSubType($video_sub_type_id,$request_param);
                        if($sresult>0){
                                $msg = 'video type updated successfully.';
                        }
                }
        }

        if($actiontype == 'Delete'){
                $video_sub_type_id = $_REQUEST["hd_video_sub_type_id"];
                if($video_sub_type_id!=''){
                        $result = $videos->booldeleteVideoSubType($video_sub_type_id,"VIDEO_SUB_TYPE");
                        $msg = 'video sub type deleted successfully.';
                }
        }

        $config_details = get_config_details();

        $strXML = "<XML>";
        $strXML .= "<MSG><![CDATA[$msg]]></MSG>";
        $strXML .= "<SELECTED_MENU_ID><![CDATA[$menu_level]]></SELECTED_MENU_ID>";
        $strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$category_id]]></SELECTED_CATEGORY_ID>";
        $strXML .= "<SELECTED_ACTION_TYPE><![CDATA[$actiontype]]></SELECTED_ACTION_TYPE>";
        $strXML .= "<VIEWSECTION><![CDATA[$selected_video_type_id]]></VIEWSECTION>";
        $strXML .= "<STARTLIMIT><![CDATA[$startlimit]]></STARTLIMIT>";
        $strXML .= "<CNT><![CDATA[$limitcnt]]></CNT>";
        $strXML .= $config_details;
        $strXML .= $xml;
        $strXML .= "</XML>";

        if($_GET['debug']==1) { header('Content-type: text/xml');echo $strXML;exit; }

        $doc = new DOMDocument();
        $doc->loadXML($strXML);
        $doc->saveXML();

        $xslt = new xsltProcessor;
        $xsl = DOMDocument::load('xsl/video_sub_type.xsl');

        $xslt->importStylesheet($xsl);
        print $xslt->transformToXML($doc);

?>

