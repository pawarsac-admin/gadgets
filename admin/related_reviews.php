<?php
require_once('../include/config.php');
require_once(CLASSPATH.'DbConn.php');
require_once(CLASSPATH.'reviews.class.php');

$dbconn = new DbConn;
$oReviews = new reviews;

//if($_POST){ print_r($_REQUEST);} //die();
$actiontype = $_REQUEST['actiontype'];
$startlimit = $_REQUEST['startlimit'];
$limitcnt = $_REQUEST['cnt'];

$_REQUEST['product_id'] !=0 ? $_REQUEST['product_id'] :0;
$_REQUEST['select_model_id'] !=0 ? $_REQUEST['select_model_id'] :0;  
$view_section_id=$_REQUEST['view_section_id'] ? $_REQUEST['view_section_id'] :"LATEST_ARTICLE"; 
$section_id=$_REQUEST['section_review_id'] ? $_REQUEST['section_review_id'] :"1";
$iRId=$_REQUEST['select_review_id'];

if($actiontype == 'Insert'|| $actiontype== 'Update'){
	
	$status=$_REQUEST['status'];
	if($status!=''){
		$request_param['status'] = $status;
	}
	
	$review_id=$_REQUEST['review_id'];
	if($review_id!=''){
		$request_param['review_id'] = $review_id;
	}
	
	$review_id=$_REQUEST['select_review_id'];
	if($review_id!=''){
		$request_param['review_id'] = $review_id;
	}
	
	$category_id=$_REQUEST['selected_category_id'];
	if($category_id!=''){
		$request_param['category_id'] = $category_id;
	}
	
	$review_type=$_REQUEST['select_aritcle_type_id'];
	if($review_type!=''){
		$request_param['review_type'] = $review_type;
	}
	$section_review_id=$_REQUEST['section_review_id'];
	if($section_review_id!=''){
		$request_param['section_review_id'] = $section_review_id;
	}
	if($_REQUEST['select_section_id']!=""){
		$aTableName=$_REQUEST['select_section_id'];
		$iResId=$oReviews->addUpdReviewsDetails($request_param,$aTableName);
	}
	if($iRId==0) {
		$iRId=$iResId;
		$msg="Review detail added successfully.";
	}else {
		$msg="Review detail updated successfully.";
	}
}

$hd_view_section_id=$_REQUEST['hd_view_section_id'];
if($actiontype == 'Delete' && $hd_view_section_id!=''){
	if($hd_view_section_id!=''){$tablename=$hd_view_section_id;}
	$result = $oReviews->booldeleteRelatedReviews($section_id,$tablename);
	$msg = 'Review deleted successfully.';
}

$config_details = get_config_details();

$strXML = "<XML>";
$strXML .= "<MSG><![CDATA[$msg]]></MSG>";
$strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$category_id]]></SELECTED_CATEGORY_ID>";
$strXML .= "<STARTLIMIT><![CDATA[$startlimit]]></STARTLIMIT>";
$strXML .= "<CNT><![CDATA[$limitcnt]]></CNT>";
$strXML .= $config_details;
$strXML .= $xml;
$strXML .= "</XML>";

$strXML = mb_convert_encoding($strXML, "UTF-8");
if($_GET['debug']==1) { header('Content-type: text/xml');echo $strXML;exit; }

$doc = new DOMDocument();
$doc->loadXML($strXML);
$doc->saveXML();

$xslt = new xsltProcessor;
$xsl = DOMDocument::load('xsl/related_reviews.xsl');

$xslt->importStylesheet($xsl);
print $xslt->transformToXML($doc);
?>
