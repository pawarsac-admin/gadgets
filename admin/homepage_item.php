<?php
require_once('../include/config.php');
require_once(CLASSPATH.'DbConn.php');
require_once(CLASSPATH.'videos.class.php');

$dbconn = new DbConn;
$videoGallery = new videos();

//print_r($_REQUEST); die();

$actiontype = $_REQUEST['actiontype'];
$startlimit = $_REQUEST['startlimit'];
$limitcnt = $_REQUEST['cnt'];
$request_param=$_REQUEST;
$category_id = $_REQUEST['selected_category_id'] ? $_REQUEST['selected_category_id'] : $_REQUEST['catid'];
$category_id = ($category_id != "") ? $category_id : SITE_CATEGORY_ID;

if($actiontype == 'Insert'|| $actiontype== 'Update'){
	unset($request_param);
	$hd_view_section_id = $_REQUEST["hd_view_section_id"];
	$hd_type_id = $_REQUEST["hd_type_id"];
	$select_item_id =  $_REQUEST["select_item_id"];
	$select_section_id = $_REQUEST["select_section_id"] ? $_REQUEST["select_section_id"] : $hd_view_section_id;
	$select_type_id = $_REQUEST["select_type_id"] ? $_REQUEST["select_type_id"] : $hd_type_id;
	$status = $_REQUEST["status"];
	$selected_category_id = $category_id;
	$request_param['category_id'] = $selected_category_id;

	if($select_item_id != ""){$request_param['item_id'] = $select_item_id;}
	if($select_type_id != ""){$request_param['content_type_id'] = $select_type_id;}

	//if($select_section_id != ""){$request_param['tbl_type_id'] = $select_section_id;}
	//echo "TEST".$select_section_id;

	if($select_section_id == "VIDEOS"){
		$request_param['tbl_type_id'] = "1";
	}else if($select_section_id == "REVIEWS"){
		$request_param['tbl_type_id'] = "2";
	}else if($select_section_id == "ARTICLES"){
		$request_param['tbl_type_id'] = "3";
	}else if($select_section_id == "NEWS"){
		$request_param['tbl_type_id'] = "4";
	}

	$img_id = $_REQUEST['img_id'];
	if(!empty($img_id)){
        	$request_param['img_id'] = $img_id;
	}

	$img_path = $_REQUEST['img_upload_id_thm'];
	if(!empty($img_path)){
        	$request_param['img_path'] = $img_path;
	}
	//print "<pre>"; print_r($request_param); die();

	if($status != ""){$request_param['status'] = $status;}
	
	if($actiontype == 'Insert'){
		$result = $videoGallery->addUpdVideosDetails($request_param,"ONCARS_INDIA_HOMEPAGE_ITEMS");
	if($sresult>0){$msg = 'item added successfully.';}
	}elseif($actiontype == 'Update'){
		$result = $videoGallery->addUpdVideosDetails($request_param,$sTableName);
	if($sresult>0){$msg = 'item updated successfully.';}
	}

}
if($actiontype == 'Delete'){
	$id = $_REQUEST["hd_id"];
	if($id!=''){
		$result = $videoGallery->booldeleteItemData($id,"ONCARS_INDIA_HOMEPAGE_ITEMS");
		$msg = 'item deleted successfully.';
	}
}

$config_details = get_config_details();

$strXML = "<XML>";
$strXML .= "<MSG><![CDATA[$msg]]></MSG>";
$strXML .= "<SELECTED_MENU_ID><![CDATA[$menu_level]]></SELECTED_MENU_ID>";
$strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$category_id]]></SELECTED_CATEGORY_ID>";
$strXML .= "<SELECTED_ACTION_TYPE><![CDATA[$actiontype]]></SELECTED_ACTION_TYPE>";
$strXML .= "<STARTLIMIT><![CDATA[$startlimit]]></STARTLIMIT>";
$strXML .= "<CNT><![CDATA[$limitcnt]]></CNT>";
$strXML .= $config_details;
$strXML .= $xml;
$strXML .= "</XML>";

if($_GET['debug']==1) { header('Content-type: text/xml');echo $strXML;exit; }

$doc = new DOMDocument();
$doc->loadXML($strXML);
$doc->saveXML();

$xslt = new xsltProcessor;
$xsl = DOMDocument::load('xsl/homepage_item.xsl');

$xslt->importStylesheet($xsl);
print $xslt->transformToXML($doc);
?>
