<?php
	require_once('../include/config.php');
	require_once(CLASSPATH.'DbConn.php');
	require_once(CLASSPATH.'product.class.php');
	require_once(CLASSPATH.'media.class.php');

	$dbconn = new DbConn;
	$product = new ProductManagement;
	$media = new media;	

	//print_r($_REQUEST);

	$category_id = $_REQUEST['selected_category_id'];
	$featurecnt = $_REQUEST['featureboxcnt'];	
	$product_name = trim($_REQUEST['product_name']);
	$brand_id = $_REQUEST['select_brand_id'];
	$varient = trim($_REQUEST['varient']);
	$product_desc = translatechars($_REQUEST['product_description']);
	$ex_showroom_mrp = $_REQUEST['product_mrp_ex_showroom'];
	$onroad_mrp = $_REQUEST['product_mrp_on_road'];
	$city_id = $_REQUEST['city_id'];
	$state_id = $_REQUEST['state_id'];

	$is_upcomimg = $_REQUEST['is_upcomimg'];
	$is_latest = $_REQUEST['is_latest'];
	$status = $_REQUEST['product_status'];
	$actiontype = $_REQUEST['actiontype'];
	
	$product_id = $_REQUEST['product_id'];
	
	$fileinfo = $_FILES['img_upload_1'];
	$tmp_path = $fileinfo['tmp_name'];
	$filename = $fileinfo['name'];
	rename($tmp_path,UPLOAD_TMP_PATH.$filename);
	
	$request_param['category_id'] = $category_id;
	$request_param['brand_id'] = $brand_id;	
	$request_param['product_name'] = $product_name;
	$request_param['variant'] = $varient;
	/*
	$request_param['product_mrp_ex_showroom'] = $ex_showroom_mrp;	
	$request_param['product_mrp_on_road'] = $onroad_mrp;
	$request_param['is_upcoming'] = $is_upcomimg;	
	$request_param['is_latest'] = $is_latest;	
	$request_param['showroom_city_id'] = $city_id;
	$request_param['showroom_state_id'] = $state_id;
	*/
	$request_param['product_desc'] = $product_desc;	
	$request_param['status'] = $status;	
	$request_param['uid'] = $uid;
	if(!empty($_REQUEST['media_id']))
	{
		$request_param['media_id'] = $_REQUEST['media_id'];
		$request_param['video_path'] = $_REQUEST['img_upload_1'];
		$request_param['img_media_id'] = $_REQUEST['img_media_id'];
		$request_param['image_path'] = $_REQUEST['img_upload_thm'];
	}
	

	
	if($actiontype == 'insert'){
		if(!empty($product_name) && !empty($brand_id)){
			$product_id = $product->intInsertProduct($request_param);
			if(!empty($product_id) && $product_id != 'exists'){
				$groupmastercount=$_REQUEST['groupmastercnt'];
				for($i=1;$i<=$groupmastercount;$i++){
					$subgroupmastercount=$_REQUEST['subgroupmastercnt_'.$i];
					for($j=1;$j<=$subgroupmastercount;$j++){
						$subgroupmasterdatacount=$_REQUEST['subgroupmaster_data_cnt_'.$i.'_'.$j];
						for($k=1;$k<=$subgroupmasterdatacount;$k++){
							 $feature_value= $_REQUEST['feature_value_'.$i.'_'.$j.'_'.$k];
							 $feature_id= $_REQUEST['feature_id_'.$i.'_'.$j.'_'.$k];
							 //echo "TEST---".$feature_id."----".$feature_value."<br>";
							 if(!empty($feature_value)){
								//insert product feature.
								$feature_param['feature_value'] = $feature_value;
								$feature_param['feature_id'] = $feature_id;
								$feature_param['product_id'] = $product_id;
								$product_feature_id = $product->intInsertProductFeature($feature_param);
							}	
						}
					}
				}







				/*
				
				for($i=1;$i<=$featurecnt;$i++){
				$feature_value = $_REQUEST['feature_value_'.$i];
				$feature_id = $_REQUEST['feature_id_'.$i];
					if(!empty($feature_value)){
						//insert product feature.
						$feature_param['feature_value'] = $feature_value;
						$feature_param['feature_id'] = $feature_id;
						$feature_param['product_id'] = $product_id;
						$product_feature_id = $product->intInsertProductFeature($feature_param);
					}
				}*/



			}
		}
		
		$msg = ($product_id == 'exists') ? 'Product already exists.' : 'Product added successfully.';
	}elseif($actiontype == 'update'){
		 if(!empty($product_name) && !empty($brand_id)){
			$result = $product->boolUpdateProduct($product_id,$request_param);
			$groupmastercount=$_REQUEST['groupmastercnt'];
				for($i=1;$i<=$groupmastercount;$i++){
					$subgroupmastercount=$_REQUEST['subgroupmastercnt_'.$i];
					for($j=1;$j<=$subgroupmastercount;$j++){
						$subgroupmasterdatacount=$_REQUEST['subgroupmaster_data_cnt_'.$i.'_'.$j];
						for($k=1;$k<=$subgroupmasterdatacount;$k++){
							//echo "groupmastercount==".$groupmastercount."======="."subgroupmastercount==".$subgroupmastercount."+++++"."subgroupmasterdatacount==".$subgroupmasterdatacount."<br>";
							 $feature_value= $_REQUEST['feature_value_'.$i.'_'.$j.'_'.$k];
							 $feature_id= $_REQUEST['feature_id_'.$i.'_'.$j.'_'.$k];
							 //echo 'feature_value_'.$i.'_'.$j.'_'.$k."===TEST---".$feature_id."----".$feature_value."<br>";
							 if(!empty($feature_value)){
								//insert product feature.
								$feature_param['feature_value'] = $feature_value;
								$feature_param['feature_id'] = $feature_id;
								$feature_param['product_id'] = $product_id;
								$product_feature_id = $product->intInsertProductFeature($feature_param);
							}	
						}
					}
				}
			$msg = "Product updated successfully.";
		}
	}elseif($actiontype == 'Delete'){
		$result = $product->boolDeleteProduct($product_id);
	    $msg = 'Product deleted successfully.';
   	}
	unlink(UPLOAD_TMP_PATH.$filename);	
	$config_details = get_config_details();

	$strXML = "<XML>";
	$strXML .= $config_details;
	$strXML .= "<MSG><![CDATA[$msg]]></MSG>";
    	$strXML .= "<STARTLIMIT><![CDATA[$startlimit]]></STARTLIMIT>";
    	$strXML .= "<CNT><![CDATA[$limitcnt]]></CNT>";
	$strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$category_id]]></SELECTED_CATEGORY_ID>"; 
	$strXML .= $xml;
	$strXML .= "</XML>";
	//header('Content-type: text/xml');echo $strXML;exit;
	$doc = new DOMDocument();
	$doc->loadXML($strXML);
	$doc->saveXML();
	$xslt = new xsltProcessor;
	$xsl = DOMDocument::load('xsl/productinfo.xsl');
	$xslt->importStylesheet($xsl);
	print $xslt->transformToXML($doc);
?>
