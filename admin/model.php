<?php
	require_once('../include/config.php');
	require_once(CLASSPATH.'DbConn.php');
	require_once(CLASSPATH.'product.class.php');

	$dbconn = new DbConn;
	$oProduct = new ProductManagement;
	//print_r($_REQUEST);//exit;
	$actiontype = $_REQUEST['actiontype'];
	$startlimit = $_REQUEST['startlimit'];
	$limitcnt = $_REQUEST['cnt'];

	$product_info_name = $_REQUEST['model_title'];
	if(!empty($product_info_name)){
		$request_param['product_info_name'] = htmlentities(trim($product_info_name),ENT_QUOTES,'UTF-8');
	}
	$category_id = $_REQUEST['selected_category_id'];
	$category_id = ($category_id != "") ? $category_id : SITE_CATEGORY_ID;
	if(!empty($category_id)){
		$request_param['category_id'] = $category_id;
	}
	$brand_id=$_REQUEST['select_brand_id'];
	if(!empty($brand_id)){
		$request_param['brand_id'] = $brand_id;
	}
	$model_description = $_REQUEST['model_description'];
	//if(!empty($model_description)){
		$request_param['product_name_desc'] = htmlentities($model_description,ENT_QUOTES,'UTF-8');
	//}
	$model_abstract = $_REQUEST['model_abstract'];
	if(!empty($model_abstract)){
		$request_param['abstract'] = htmlentities($model_abstract,ENT_QUOTES,'UTF-8');
	}
	$model_tags = trim($_REQUEST['model_tags']);
	if(!empty($model_tags)){
		$request_param['tags'] = htmlentities($model_tags,ENT_QUOTES,'UTF-8');
	}
	$media_id = $_REQUEST['media_id'];
	if(!empty($media_id)){
		$request_param['media_id'] = $media_id;
	}
	$video_path = trim($_REQUEST['img_upload_1']);
	if(!empty($video_path)){
		$request_param['video_path'] = $video_path;
	}
	$img_media_id = $_REQUEST['img_media_id'];
	if(!empty($img_media_id)){
		$request_param['img_media_id'] = $img_media_id;
	}
	$image_path = trim($_REQUEST['img_upload_thm']);
	if(!empty($image_path)){
		$request_param['image_path'] = $image_path;
	}
	$status = $_REQUEST['model_status'];
	$request_param['status'] = $status;

	$product_name_id = $_REQUEST['product_name_id'];
	
	if(!empty($product_name_id)){
		$product_result_name = $oProduct->arrGetProductNameInfo($product_name_id);
		$old_product_name = strtolower($product_result_name[0]['product_info_name']);	
	}
	if($actiontype == 'Insert'){
		$product_name_id = $oProduct->addUpdProductInfoDetails($request_param,"PRODUCT_NAME_INFO");
		$msg="Model detail added successfully.";
	}else if($actiontype == 'Update'){
		$request_param['product_name_id'] = $product_name_id;
		$product_name_id = $oProduct->addUpdProductInfoDetails($request_param,"PRODUCT_NAME_INFO");
		$result = $oProduct->boolUdateProductName($product_info_name,$old_product_name,1);
		$msg="Model detail updated successfully.";
	}else if($actiontype == 'Delete'){
		$result = $oProduct->deleteModel($product_name_id);
		$result = $oProduct->boolUdateProductName($product_info_name,$old_product_name,0);
		$msg = 'Model deleted successfully.';
	}

	$config_details = get_config_details();

	$strXML = "<XML>";
	$strXML .= "<MSG><![CDATA[$msg]]></MSG>";
	$strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$category_id]]></SELECTED_CATEGORY_ID>";
	$strXML .= "<STARTLIMIT><![CDATA[$startlimit]]></STARTLIMIT>";
	$strXML .= "<CNT><![CDATA[$limitcnt]]></CNT>";
	$strXML .= $config_details;
	$strXML .= $xml;
	$strXML .= "</XML>";

	//header('Content-type: text/xml');echo $strXML;exit;
	$doc = new DOMDocument();
	$doc->loadXML($strXML);
	$doc->saveXML();

	$xslt = new xsltProcessor;
	$xsl = DOMDocument::load('xsl/model.xsl');

	$xslt->importStylesheet($xsl);
	print $xslt->transformToXML($doc);
?>
