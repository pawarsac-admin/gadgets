<?php
require_once('../include/config.php');
require_once(CLASSPATH.'DbConn.php');
require_once(CLASSPATH.'dealer.class.php');

$dbconn = new DbConn;
$oDealer = new Dealer;

$actiontype = $_REQUEST['actiontype'];
$startlimit = $_REQUEST['startlimit'];
$limitcnt = $_REQUEST['cnt'];
$category_id = $_REQUEST['selected_category_id'];

//print "<pre>"; print_r($_REQUEST); 
//die();

if($actiontype == 'Insert'|| $actiontype== 'Update'){
        if($_REQUEST['display_rows']>0){
                unset($request_param);
                $dataCount=$_REQUEST['display_rows'];
                for($i=1;$i<=$dataCount;$i++){
			unset($request_param);
                        $request_type = $_REQUEST['request_type_'.$i];
                        $request_type = trim($request_type);
                        if(!empty($request_type)){ $request_param['request_type'] = htmlentities($request_type,ENT_QUOTES);}
			
			$request_type_id = $_REQUEST['request_type_id_'.$i];
                        if(!empty($request_type_id)){ $request_param['request_type_id'] = $request_type_id;}

			$status = $_REQUEST['status'];
                        if($status!=''){ $request_param['status'] = $status;}
			
			$result = $oDealer->addUpdRequestTypeDetails($request_param,"REQUEST_TYPE");
			
			if($sresult>0 && $actiontype == 'Insert'){
		                $msg = 'Request Type added successfully.';
		        }	
			if($sresult>0 && $actiontype == 'Update'){
		                $msg = 'Request Type updated successfully.';
		        }	
		}
	}
}

if($actiontype == 'Delete'){
        $request_type_id = $_REQUEST['request_type_id'];
        if($request_type_id!=''){
                $result = $oDealer->boolDeleteRequestType($request_type_id,"REQUEST_TYPE");
                $msg = 'Request Type deleted successfully.';
        }
}

$config_details = get_config_details();
$strXML = "<XML>";
$strXML .= "<MSG><![CDATA[$msg]]></MSG>";
$strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$category_id]]></SELECTED_CATEGORY_ID>";
$strXML .= "<STARTLIMIT><![CDATA[$startlimit]]></STARTLIMIT>";
$strXML .= "<SELECTED_ACTION_TYPE><![CDATA[$actiontype]]></SELECTED_ACTION_TYPE>";
$strXML .= "<CNT><![CDATA[$limitcnt]]></CNT>";
$strXML .= $config_details;
$strXML .= $xml;
$strXML .= "</XML>";

$strXML = mb_convert_encoding($strXML, "UTF-8");
if($_GET['debug']==1) { header('Content-type: text/xml');echo $strXML;exit; }

$doc = new DOMDocument();
$doc->loadXML($strXML);
$doc->saveXML();

$xslt = new xsltProcessor;
$xsl = DOMDocument::load('xsl/request_type.xsl');

$xslt->importStylesheet($xsl);
print $xslt->transformToXML($doc);
?>
