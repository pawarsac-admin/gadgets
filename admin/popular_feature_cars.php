<?php
    require_once('../include/config.php');
    require_once(CLASSPATH.'DbConn.php');
	require_once(CLASSPATH.'brand.class.php');
	require_once(CLASSPATH.'feature.class.php');
	require_once(CLASSPATH.'pivot.class.php');
	require_once(CLASSPATH.'product.class.php');
	
    $dbconn = new DbConn;
	$brand = new BrandManagement;
	$feature = new FeatureManagement;
	$pivot = new PivotManagement;
	$product = new ProductManagement;
	
	//if($_POST){ print_r($_REQUEST);} //die();
	$actiontype = $_REQUEST['actiontype'];
	$startlimit = $_REQUEST['startlimit'];
	$limitcnt = $_REQUEST['cnt'];
	$request_param=$_REQUEST;
	$category_id = $_REQUEST['selected_category_id'];
	$category_id = ($category_id != "") ? $category_id : SITE_CATEGORY_ID;
	$selected_pivot_group = $_REQUEST['select_pivot_group'] ? $_REQUEST['select_pivot_group'] : $_REQUEST['selected_pivot_group'];
	$selected_pivot_id = $_REQUEST['select_pivot_id'] ? $_REQUEST['select_pivot_id'] : $_REQUEST['selected_pivot_id'];

	if($actiontype == 'Insert' || $actiontype == 'Update'){
		$pivot_group = $_REQUEST['select_pivot_group'];
		$pivot_id = $_REQUEST['select_pivot_id'];
		$pivotresult = $pivot->arrGetPivotDetails($pivot_id);
		
		$feature_id = $pivotresult[0]['feature_id'] ? $pivotresult[0]['feature_id'] : 0;
		
    	$status = $_REQUEST['status'];
		$product_id_arr =Array();
		$product_id_arr = $_REQUEST['select_product_id'];
		$cnt = sizeof($product_id_arr);
		if($cnt > 0){
                	for($i=0;$i<$cnt;$i++){
		                unset($request_param);
				$model_id="";$brand_id="";
				$request_param['pivot_group']=$pivot_group;
				$request_param['pivot_id']=$pivot_id;
				$request_param['feature_id'] = $feature_id;
				$request_param['status']=$status;
				$request_param['category_id']=$category_id;
				$product_id = $product_id_arr[$i];
				if(!empty($product_id)){
                 		       $product_result=$product->arrGetProductDetails($product_id,$category_id,"","","","","","","","","","");
		                       $product_name = $product_result[0]['product_name'];
                		       $variant_name = $product_result[0]['variant'];
		                       $brand_id = $product_result[0]['brand_id'];
                		       if(!empty($product_name)){
						$product_name_arr = $product->arrGetProductNameInfo("",$category_id,$brand_id,$product_info_name);
						$model_id = $product_name_arr[0]["product_name_id"];
		                       }
                        	}
				$request_param['brand_id']=$brand_id;
				$request_param['model_id']=$model_id;
				$request_param['product_id']=$product_id;
	
				//print"<pre>";print_r($request_param);print"</pre>";
				if($actiontype == 'Insert'){
                                        $result = $feature->intInsertPopularFeatureCars($request_param);
                                }elseif($actiontype == 'Update'){
                                        if($i == 0){
                                                $popular_feature_id = $_REQUEST['popular_feature_id'];
						//print"<pre>";print_r($request_param);print"</pre>";
                                                $result = $feature->boolUpdatePopularFeatureCars($popular_feature_id,$request_param);
                                        }else{
                                                $result = $feature->intInsertPopularFeatureCars($request_param);
                                        }
                                }
				
			}
		}

        	if($sresult>0){
			if($actiontype == 'Insert'){
				$msg = 'popular feature cars added successfully.';
			}else{
				$msg = 'popular feature cars updated successfully.';
			}
		}
	}
		


	if($actiontype == 'Delete'){
		$popular_feature_id = $_REQUEST["popular_feature_id"];
        	if($popular_feature_id != ''){
                	$result = $feature->boolDeletePopularFeatureCars($popular_feature_id);
	                $msg = 'popular feature cars deleted successfully.';
        	}
	}

	$config_details = get_config_details();

        $strXML = "<XML>";
        $strXML .= "<MSG><![CDATA[$msg]]></MSG>";
        $strXML .= "<SELECTED_MENU_ID><![CDATA[$menu_level]]></SELECTED_MENU_ID>";
        $strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$category_id]]></SELECTED_CATEGORY_ID>";
	$strXML .= "<SELECTED_ACTION_TYPE><![CDATA[$actiontype]]></SELECTED_ACTION_TYPE>";
	$strXML .= "<SELECTED_PIVOT_GROUP><![CDATA[$selected_pivot_group]]></SELECTED_PIVOT_GROUP>";
	$strXML .= "<SELECTED_PIVOT_ID><![CDATA[$selected_pivot_id]]></SELECTED_PIVOT_ID>";
        $strXML .= "<STARTLIMIT><![CDATA[$startlimit]]></STARTLIMIT>";
        $strXML .= "<CNT><![CDATA[$limitcnt]]></CNT>";
        $strXML .= $config_details;
        $strXML .= $xml;
        $strXML .= "</XML>";

	if($_GET['debug']==1) { header('Content-type: text/xml');echo $strXML;exit; }

        $doc = new DOMDocument();
        $doc->loadXML($strXML);
        $doc->saveXML();

        $xslt = new xsltProcessor;
        $xsl = DOMDocument::load('xsl/popular_feature_cars.xsl');

        $xslt->importStylesheet($xsl);
        print $xslt->transformToXML($doc);
?>

