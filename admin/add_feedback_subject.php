<?php
require_once('../include/config.php');
require_once(CLASSPATH.'DbConn.php');
require_once(CLASSPATH.'user_feedback.class.php');

$dbconn = new DbConn;
$oFeedbackuser = new FEEDBACKUSER;

$actiontype = $_REQUEST['actiontype'] ? $_REQUEST['actiontype'] : "Insert";
$startlimit = $_REQUEST['startlimit'];
$limitcnt = $_REQUEST['cnt'];
$category_id = $_REQUEST['selected_category_id'];

//print "<pre>"; print_r($_REQUEST); 
//die();

if($actiontype == 'Insert'|| $actiontype== 'Update'){
        if($_REQUEST['display_rows']>0){
                unset($request_param);
                $dataCount=$_REQUEST['display_rows'];
                for($i=1;$i<=$dataCount;$i++){
			unset($request_param);
                        $subject = $_REQUEST['subject_'.$i];
                        $subject = trim($subject);
                        if(!empty($subject)){ $request_param['subject'] = htmlentities($subject,ENT_QUOTES);}
			
			$subject_id = $_REQUEST['subject_id_'.$i];
                        if(!empty($subject_id)){ $request_param['subject_id'] = $subject_id;}

			$status = $_REQUEST['status'];
                        if($status!=''){ $request_param['status'] = $status;}
			
			$result = $oFeedbackuser->intInsertFeedbackSubject($request_param);
			
			if($sresult>0 && $actiontype == 'Insert'){
		                $msg = 'video added successfully.';
		        }	
			if($sresult>0 && $actiontype == 'Update'){
		                $msg = 'video updated successfully.';
		        }	
		}
	}
}

if($actiontype == 'Delete'){
        $subject_id = $_REQUEST['subject_id'];
        if($subject_id!=''){
                $result = $oFeedbackuser->booldeleteFeedbackSubject($subject_id);
                $msg = 'Feedback Subject deleted successfully.';
        }
}

$config_details = get_config_details();
$strXML = "<XML>";
$strXML .= "<MSG><![CDATA[$msg]]></MSG>";
$strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$category_id]]></SELECTED_CATEGORY_ID>";
$strXML .= "<STARTLIMIT><![CDATA[$startlimit]]></STARTLIMIT>";
$strXML .= "<SELECTED_ACTION_TYPE><![CDATA[$actiontype]]></SELECTED_ACTION_TYPE>";
$strXML .= "<CNT><![CDATA[$limitcnt]]></CNT>";
$strXML .= $config_details;
$strXML .= $xml;
$strXML .= "</XML>";

$strXML = mb_convert_encoding($strXML, "UTF-8");
if($_GET['debug']==1) { header('Content-type: text/xml');echo $strXML;exit; }

$doc = new DOMDocument();
$doc->loadXML($strXML);
$doc->saveXML();

$xslt = new xsltProcessor;
$xsl = DOMDocument::load('xsl/add_feedback_subject.xsl');

$xslt->importStylesheet($xsl);
print $xslt->transformToXML($doc);
?>
