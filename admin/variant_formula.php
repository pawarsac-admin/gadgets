<?php
require_once("../include/config.php");
require_once(CLASSPATH.'DbConn.php');
require_once(CLASSPATH."category.class.php");
require_once(CLASSPATH."price.class.php");

$dbconn = new DbConn;
$oCategory	= new CategoryManagement;
$oPrice	= new price;

$actiontype = $_REQUEST['actiontype'] ? $_REQUEST['actiontype'] : "Insert";
$startlimit = $_REQUEST['startlimit'];
$limitcnt = $_REQUEST['cnt'];
$variant_count= $_REQUEST['variant_count'];

$variant_formula_id=$_REQUEST['variant_formula_id'];
if($variant_formula_id!=''){
	$request_param['variant_formula_id']=$variant_formula_id;
}
$category_id=$_REQUEST['selected_category_id'];
if($category_id!=''){
	$request_param['category_id']=$category_id;
}
$status=$_REQUEST['status'];
if($status!=''){
	$request_param['status']=$status;
}

if($variant_count>0){
	for($n=1;$n<=$variant_count;$n++){
		$variant_id=$_REQUEST['variant_id_'.$n];
		$variant_opt=$_REQUEST['variant_operator_'.$n];
		$sVariantForm .=$variant_id.$variant_opt;
	}
	$request_param['formula']=$sVariantForm;
}

if($actiontype=="Insert"){
	$iResId=$oPrice->intInsertUpdateVariantFormula($request_param);
	$msg="Price Variant formula detail added successfully.";
}
else if($actiontype=="Update"){
	$iResId=$oPrice->intInsertUpdateVariantFormula($request_param);
	$msg="Price Variant formula updated successfully.";
}
else if($actiontype=="Delete"){
	$dresult = $oPrice->boolDeleteVariantFormula($variant_formula_id);
	$msg = 'Price variant formula Data deleted successfully.';
}

$config_details = get_config_details();
$strXML = "<XML>";
$strXML .= "<MSG><![CDATA[$msg]]></MSG>";
$strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$category_id]]></SELECTED_CATEGORY_ID>";
$strXML .= "<SELECTED_ACTION_TYPE><![CDATA[$actiontype]]></SELECTED_ACTION_TYPE>";
$strXML .= "<STARTLIMIT><![CDATA[$startlimit]]></STARTLIMIT>";
$strXML .= "<CNT><![CDATA[$limitcnt]]></CNT>";
$strXML .= $config_details;
$strXML .= $xml;
$strXML .= "</XML>";

$strXML = mb_convert_encoding($strXML, "UTF-8");
if($_GET['debug']==1) { header('Content-type: text/xml');echo $strXML;exit; }

$doc = new DOMDocument();
$doc->loadXML($strXML);
$doc->saveXML();

$xslt = new xsltProcessor;
$xsl = DOMDocument::load('xsl/variant_formula.xsl');

$xslt->importStylesheet($xsl);
print $xslt->transformToXML($doc);
?>