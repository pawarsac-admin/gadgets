<?php
require_once('../include/config.php');
require_once(CLASSPATH.'DbConn.php');
require_once(CLASSPATH.'wallpaper.class.php');

$dbconn = new DbConn;
$oWallpapers = new Wallpapers;

$actiontype = $_REQUEST['actiontype'] ? $_REQUEST['actiontype'] : "Insert";
$startlimit = $_REQUEST['startlimit'];
$limitcnt = $_REQUEST['cnt'];
$category_id = $_REQUEST['selected_category_id'];
$wallpaper_id = $_REQUEST['wp_id'];

//print "<pre>"; print_r($_REQUEST); die();
//echo "<br>=============================<br>";
if($actiontype == 'Insert'|| $actiontype== 'Update'){
	if($_REQUEST['display_rows']>0){
		unset($request_param);
		$dataCount=$_REQUEST['display_rows'];
		for($i=1;$i<=$dataCount;$i++){
			unset($request_param);
			$title = $_REQUEST['title_'.$i];	
			$title = trim($title);
			if(!empty($title)){ $request_param['title'] = htmlentities($title,ENT_QUOTES);}
				
			$tags = $_REQUEST['tags_'.$i];
			$tags = trim($tags);
			if(!empty($tags)){ $request_param['tags'] = $tags;}	

			$meta_description = $_REQUEST['meta_description_'.$i];
			if(!empty($meta_description)){ $request_param['meta_description'] = $meta_description;}	
		
			$publish_time = $_REQUEST['publish_time']; 
	                if($publish_time != ''){ $request_param['publish_time'] = $publish_time;}

			//$status = $_REQUEST['status_'.$i];
			$status = $_REQUEST['status'];
			if($status!=''){ $request_param['status'] = $status;}


			$wp_id = $_REQUEST['wp_id_'.$i];
			if(!empty($wallpaper_id)){ $request_param['wallpaper_id'] = $wp_id;}


			$upload_media_id = $_REQUEST['upload_media_id_'.$i];
			if(!empty($upload_media_id)){
				$request_param['upload_media_id'] = $upload_media_id;
			}

			$media_id = $_REQUEST['media_id_'.$i];
			if(!empty($media_id)){
				$request_param['media_id'] = $media_id;	
			}

			$media_path = $_REQUEST['media_upload_1_'.$i];
			if(!empty($media_path)){
				$request_param['media_path'] = $media_path;
			}

			$media_img_id = $_REQUEST['img_media_id_'.$i];
			if(!empty($media_img_id)){	
				$request_param['video_img_id'] = $media_img_id;
			}

			$media_img_path = $_REQUEST['img_upload_id_thm_'.$i];
			if(!empty($media_img_path)){	
				$request_param['video_img_path'] = $media_img_path;
			}

			$content_type = !empty($_REQUEST['wp_content_type_'.$i]) ? $_REQUEST['wp_content_type_'.$i] : $_REQUEST['content_type_'.$i];
			$request_param['content_type'] = $content_type;

			$request_param['is_media_process'] = 1;

			//print "<pre>"; print_r($request_param);
			$wp_result_id=$oWallpapers->intInsertWallpapers($request_param,"WALLPAPER");

			$wp_id = $wp_result_id ? $wp_result_id : $_REQUEST['wp_id_'.$i];
			unset($request_param);
				
			if(!empty($wp_id)){ $request_param['wallpaper_id'] = $wp_id;}
	
			$category_id=$_REQUEST['selected_category_id'];
			if(!empty($category_id)){ $request_param['category_id']=$category_id;}

			$brand_id=$_REQUEST['select_brand_id'] ? $_REQUEST['select_brand_id'] : $_REQUEST['hd_select_brand_id'];
			if($brand_id!=''){ $request_param['brand_id']=$brand_id;}

			$product_id=$_REQUEST['product_id'] ? $_REQUEST['product_id'] : $_REQUEST['hd_product_id'];
			if($product_id!=''){ $request_param['product_id']=$product_id;}

			$product_info_id=$_REQUEST['select_model_id'] ? $_REQUEST['select_model_id'] : $_REQUEST['hd_select_model_id'];
			if($product_info_id!=''){ $request_param['product_info_id']=$product_info_id;}

			//$group_id=$_REQUEST['select_group_id'];
			//if($group_id!=''){ $request_param['group_id']=$group_id;}

			$product_wp_id=$_REQUEST['product_wp_id_'.$i];
			if(!empty($product_wp_id)){ $request_param['product_wallpaper_id'] = $product_wp_id;}
			//print "<pre>"; print_r($request_param);
			$product_wp_result_id=$oWallpapers->intInsertWallpapers($request_param,"PRODUCT_WALLPAPERS");
			if($wp_result_id!=''){ $sresult++;}
	
		}
	}
	if($sresult>0){
		$msg = 'Wallpaper added successfully.';
	}
}

//print "<pre>"; print_r($_REQUEST);
if($actiontype == 'Delete'){
	$wp_id = $_REQUEST['wp_id'];
	if($wp_id!=''){
		$result = $oWallpapers->boolDeleteWallpaper($wp_id);
		$msg = 'wallpaper deleted successfully.';
	}
}
$config_details = get_config_details();
$strXML = "<XML>";
$strXML .= "<MSG><![CDATA[$msg]]></MSG>";
$strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$category_id]]></SELECTED_CATEGORY_ID>";
$strXML .= "<STARTLIMIT><![CDATA[$startlimit]]></STARTLIMIT>";
$strXML .= "<SELECTED_ACTION_TYPE><![CDATA[$actiontype]]></SELECTED_ACTION_TYPE>";
$strXML .= "<CNT><![CDATA[$limitcnt]]></CNT>";
$strXML .= $config_details;
$strXML .= $xml;
$strXML .= "</XML>";

$strXML = mb_convert_encoding($strXML, "UTF-8");
if($_GET['debug']==1) { header('Content-type: text/xml');echo $strXML;exit; }

$doc = new DOMDocument();
$doc->loadXML($strXML);
$doc->saveXML();

$xslt = new xsltProcessor;
$xsl = DOMDocument::load('xsl/wallpaper.xsl');

$xslt->importStylesheet($xsl);
print $xslt->transformToXML($doc);
?>
