<?php
	require_once('../include/config.php');
	require_once(CLASSPATH.'DbConn.php');
	require_once(CLASSPATH.'menu.class.php');
	$dbconn = new DbConn;
	$menu = new MenuManagement;
	//print_r($_REQUEST);
	$menu_level = $_REQUEST['selected_menu_id'];
	$menu_name = trim($_REQUEST['menu_name']);
	$menu_page = trim($_REQUEST['menu_page']);
	$menu_title = trim($_REQUEST['menu_title']);
	$status = $_REQUEST['menu_status'];
	$menu_id = $_REQUEST['menu_id'];
	$category_id = $_REQUEST['category_id'] ? $_REQUEST['category_id'] : $_REQUEST['selected_category_id'];
	$actiontype = $_REQUEST['actiontype'];
	//$request_param = array('menu_name'=>$menu_name,'status'=>$status,'menu_level'=>$menu_level);
	if(!empty($menu_name)){
		$request_param['menu_name'] = htmlentities($menu_name,ENT_QUOTES);
	}
	if(!empty($menu_page)){
		$request_param['menu_page'] = htmlentities($menu_page,ENT_QUOTES);
	}
	if(!empty($menu_title)){
		$request_param['title'] = htmlentities($menu_title,ENT_QUOTES);
	}
	if(!empty($category_id)){
		$request_param['category_id'] = $category_id;
	}
	if($actiontype != 'Update' && $menu_level != ''){
		$request_param['menu_level'] = $menu_level;
	}
	if($actiontype == 'Delete'){
	   $result = $menu->boolDeleteHeaderMenu($menu_id);
	   $msg = 'Menu deleted successfully.';
	}elseif($actiontype == 'Update'){
	   $result = $menu->boolUpdateHeaderMenu($menu_id,$request_param);
	   $msg = 'Menu updated successfully.';
	}elseif($actiontype == 'insert'){
	   $result = $menu->intInsertHeaderMenu($request_param);
	   $msg = ($result == 'exists') ? 'Menu already exists.' : 'Menu added successfully.';
	}
	$startlimit = $_REQUEST['startlimit'];
	$limitcnt = $_REQUEST['cnt'];
	$result = $menu->arrGetHeaderMenuDetails("","",$category_id,"","","",$startlimit,$limitcnt);
	$cnt = sizeof($result);
	$xml = "<HEADER_MENU_MASTER>";
	$xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
	for($i=0;$i<$cnt;$i++){
		$status = $result[$i]['status'];
		$result[$i]['menu_status'] = ($status == 1) ? 'Active' : 'InActive';
		$result[$i]['create_date'] = date('d-m-Y',strtotime($result[$i]['create_date']));
		$result[$i]['js_menu_name'] = $result[$i]['menu_name'];
		$result[$i]['js_menu_page'] = $result[$i]['menu_page'];
		$result[$i]['js_title'] = $result[$i]['title'];
		$result[$i]['menu_name'] = html_entity_decode($result[$i]['menu_name'],ENT_QUOTES);
		$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
		$xml .= "<HEADER_MENU_MASTER_DATA>";
		foreach($result[$i] as $k=>$v){
			$xml .= "<$k><![CDATA[$v]]></$k>";
		}
		$xml .= "</HEADER_MENU_MASTER_DATA>";
	}
	$xml .= "</HEADER_MENU_MASTER>";

	$config_details = get_config_details();
	$strXML = "<XML>";
	$strXML .= "<MSG><![CDATA[$msg]]></MSG>";
	$strXML .= "<SELECTED_MENU_ID><![CDATA[$menu_level]]></SELECTED_MENU_ID>";
	$strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$category_id]]></SELECTED_CATEGORY_ID>";
	$strXML .= "<STARTLIMIT><![CDATA[$startlimit]]></STARTLIMIT>";
	$strXML .= "<CNT><![CDATA[$limitcnt]]></CNT>";
	$strXML .= $config_details;
	$strXML .= $xml;
	$strXML .= "</XML>";
	//header('Content-type: text/xml');echo $strXML;exit;
	$doc = new DOMDocument();
	$doc->loadXML($strXML);
	$doc->saveXML();
	$xslt = new xsltProcessor;
	$xsl = DOMDocument::load('xsl/header_menu.xsl');
	$xslt->importStylesheet($xsl);
	print $xslt->transformToXML($doc);
?>
