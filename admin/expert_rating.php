<?php
	require_once('../include/config.php');
	require_once(CLASSPATH.'DbConn.php');
	require_once(CLASSPATH.'user_review.class.php');

	$dbconn = new DbConn;
	$userreview = new USERREVIEW;
	
	$category_id = $_REQUEST['selected_category_id'];
        $category_id = ($category_id != "") ? $category_id : SITE_CATEGORY_ID;
	if($category_id != ''){
		$request_param['category_id'] = $category_id;
	}

	$product_id = $_REQUEST['product_id'];
	if($product_id != ''){
		$request_param['product_id'] = $product_id;
	}

	$brand_id = $_REQUEST['select_brand_id'];
	if($brand_id != ''){
		$request_param['brand_id'] = $brand_id;
	}

	$select_model_id = $_REQUEST['select_model_id'];
	if($select_model_id != ''){
		$request_param['product_info_id'] = $select_model_id;
	}
	$status = $_REQUEST['overall_grade_status'];
	if($status != ''){
		$request_param['status'] = $status;
	}

	$user_rating = $_REQUEST['user_rating'];
	if($user_rating != ''){
		$request_param['user_rating'] = $user_rating;
	}
	$performance_rating = $_REQUEST['performance_rating'];
        if($performance_rating != ''){
                $request_param['performance_rating'] = $performance_rating;
        }
	$design_rating = $_REQUEST['design_rating'];
        if($design_rating != ''){
                $request_param['design_rating'] = $design_rating;
        }	
	$actiontype = $_REQUEST['actiontype'];
	$expert_rating_id = $_REQUEST['expert_rating_id'];

    if($actiontype == 'Delete'){
	   $result = $userreview->boolDeleteAdminExpertRating($expert_rating_id);
	   $msg = 'Overall rating deleted successfully.';
	}elseif($actiontype == 'Update'){
	   $result = $userreview->boolUpdateAdminExpertRating($expert_rating_id,$request_param);
	   $msg = 'Overall rating  updated successfully.';
	}elseif($actiontype == 'insert'){
	   $result = $userreview->intInsertAdminExpertRating($request_param);
	   $msg = ($result == 'exists') ? 'Expert Overall rating already exists.' : 'Expert Overall rating added successfully.';
	}

	$config_details = get_config_details();
	$strXML = "<XML>";
	$strXML .= "<MSG><![CDATA[$msg]]></MSG>";
	$strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$category_id]]></SELECTED_CATEGORY_ID>";
	$strXML .= "<STARTLIMIT><![CDATA[$startlimit]]></STARTLIMIT>";
	$strXML .= "<CNT><![CDATA[$limitcnt]]></CNT>";
	$strXML .= $config_details;
	$strXML .= $xml;
	$strXML .= "</XML>";
	//header('Content-type: text/xml');echo $strXML;exit;
	$doc = new DOMDocument();
	$doc->loadXML($strXML);
	$doc->saveXML();
	$xslt = new xsltProcessor;
	$xsl = DOMDocument::load('xsl/expert_rating.xsl');
	$xslt->importStylesheet($xsl);
	print $xslt->transformToXML($doc);
?>
