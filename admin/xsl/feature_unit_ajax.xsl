<?xml version="1.0" ?>
<!DOCTYPE xsl:stylesheet  [
  <!ENTITY nbsp   "&#160;">
  <!ENTITY copy   "&#169;">
  <!ENTITY reg    "&#174;">
  <!ENTITY trade  "&#8482;">
  <!ENTITY mdash  "&#8212;">
  <!ENTITY ldquo  "&#8220;">
  <!ENTITY rdquo  "&#8221;">
  <!ENTITY pound  "&#163;">
  <!ENTITY yen    "&#165;">
  <!ENTITY euro   "&#8364;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:output method="html" version="4.0" encoding="UTF-8" indent="yes"/>
  <xsl:include href="inc_header.xsl" /><!-- include header-->
  <xsl:include href="inc_footer.xsl" /><!-- include footer-->
  <xsl:template match="/">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>:: Ajax - Admin Feature Unit Dashboard Management ::</title>
        <link rel="stylesheet" type="text/css" href="{/XML/CSS_URL}main.css" />
        <script LANGUAGE="JavaScript" SRC="{XML/ADMIN_JS_URL}common.js"></script>
        <script LANGUAGE="JavaScript" SRC="{XML/ADMIN_JS_URL}category.js"></script>
        <script LANGUAGE="JavaScript" SRC="{XML/ADMIN_JS_URL}feature_unit.js"></script>
      </head>
      <body>
        <table width="100%" border="1" id="feature_unit_dashboard">
          <tr>
            <td colspan="6">
              <div align="center">
                <h3>Dashboard</h3>
              </div>
            </td>
          </tr>
          <tr>
            <td colspan="6">
              <div align="right">pagination goes here</div>
            </td>
          </tr>
          <tr>
            <td width="15%">Sr no.</td>
            <td width="37%">Unit Name</td>
            <td width="12%">Status</td>
            <td width="12%">Create Date</td>
            <td colspan="2">Action</td>
          </tr>
          <xsl:choose>
            <xsl:when test="/XML/UNIT_MASTER/COUNT&lt;=0">
              <tr>
                <td colspan="6">
                  <div align="center">Zero Result Found.</div>
                </td>
              </tr>
            </xsl:when>
            <xsl:otherwise>
              <xsl:for-each select="/XML/UNIT_MASTER/UNIT_MASTER_DATA">
                <tr>
                  <td>
                    <xsl:value-of select="position()" diseable-output-esacaping="yes"/>
                  </td>
                  <td>
                    <xsl:value-of select="UNIT_NAME" diseable-output-esacaping="yes"/>
                  </td>
                  <td>
                    <xsl:value-of select="UNIT_STATUS" diseable-output-esacaping="yes"/>
                  </td>
                  <td>
                    <xsl:value-of select="CREATE_DATE" diseable-output-esacaping="yes"/>
                  </td>
                  <td width="7%">
                    <a href="#Update" onclick="updateFeatureUnit('{UNIT_ID}','{JS_UNIT_NAME}','{STATUS}');">Update</a>
                  </td>
                  <td width="17%">
                    <a href="javascript:undefined;" onclick="deleteFeatureUnit('{UNIT_ID}','{JS_UNIT_NAME}');">Delete</a>
                  </td>
                </tr>
              </xsl:for-each>
            </xsl:otherwise>
          </xsl:choose>
          <tr>
            <td colspan="6">
              <div align="right">pagination goes here</div>
            </td>
          </tr>
        </table>
        <table width="100%" border="0">
          <tr>
            <td colspan="6" width="100%">
              <h3>Add a new feature unit</h3>
            </td>
          </tr>
        </table>
        <table width="100%" id="Update" border="1">
          <tr>
            <td width="13%">Feature Unit Name</td>
            <td colspan="2">
              <input type="text" name="unit_name" id="unit_name"/>
            </td>
          </tr>
          <tr>
            <td>Status</td>
            <td colspan="2">
              <select id="unit_status" name="unit_status">
                <option value="1">Active</option>
                <option value="0">InActive</option>
              </select>
            </td>
          </tr>
          <tr>
            <td>
              <input type="hidden" name="unit_id" id="unit_id" value=""/>
            </td>
            <td colspan="2">
              <input type="hidden" name="actiontype" id="actiontype" value="insert"/>
            </td>
          </tr>
          <tr>
            <td>
              <input type="hidden" name="startlimit" id="startlimit" value="{/XML/STARTLIMIT}"/>
            </td>
            <td colspan="2">
              <input type="hidden" name="cnt" id="cnt" value="{/XML/CNT}"/>
            </td>
          </tr>
          <tr>
            <td height="26">&nbsp;</td>
            <td width="7%">
              <input type="submit" name="featureunitsubmit" id="featureunitsubmit" value="Add/Update"/>
            </td>
            <td width="80%">
              <input type="button" name="cancel" id="cancel" value="Cancel" onclick="javascript:this.form.reset();"/>
            </td>
          </tr>
        </table>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
