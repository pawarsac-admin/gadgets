<?xml version="1.0" ?>
<!DOCTYPE xsl:stylesheet  [
  <!ENTITY nbsp   "&#160;">
  <!ENTITY copy   "&#169;">
  <!ENTITY reg    "&#174;">
  <!ENTITY trade  "&#8482;">
  <!ENTITY mdash  "&#8212;">
  <!ENTITY ldquo  "&#8220;">
  <!ENTITY rdquo  "&#8221;">
  <!ENTITY pound  "&#163;">
  <!ENTITY yen    "&#165;">
  <!ENTITY euro   "&#8364;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html" version="4.0" encoding="UTF-8" indent="yes"/>
    <xsl:include href="../xsl/inc_header.xsl" /><!-- include header-->
    <xsl:include href="../xsl/inc_footer.xsl" /><!-- include footer-->
    <xsl:include href="../xsl/inc_leftnavigation.xsl" /><!-- include left navigation-->
    <xsl:include href="../xsl/inc_rightnavigation.xsl" /><!-- include right navigation-->
    <xsl:template match="/">
        <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title>Mobile : Admin Onroad Price User Management</title>
                <link rel="stylesheet" type="text/css" href="{XML/WEB_URL}css/main.css" />
		<script LANGUAGE="JavaScript" SRC="{XML/ADMIN_JS_URL}user.js"></script>
            </head>
            <body>
	    <div id="DivUserInfo">
                <table align="center" width="100%" border="0" cellpadding="2" cellspacing="2">
                    <!-- call Header -->
                    <tr>
                        <td colspan="3">
                            <xsl:call-template name="incHeader"/>
                        </td>
                    </tr>
                    <tr>
                        <!-- call Left Navigation -->
                        <td width="10%" valign="top">
                            <xsl:call-template name="incLeftNavigation"/>
                        </td>
                        <!-- main area -->
                        <td>
                            <div align="center">
                                <table width="100%" border="0">
                                    <tr>
                                        <td>
                                            <h2>Onroad Price User Management</h2>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <a href="{XML/ADMIN_WEB_URL}index.php">Main</a>
>  Onroad Price User Management Dashboard</td>
                                    </tr>
                                </table>
                            </div>
			   <form enctype="multipart/form-data" method="post" action="{XML/ADMIN_WEB_URL}user_info.php" name="product_manage" id="product_manage">
                            <table width="100%" border="0" id="feature_unit_dashboard" style="border: 1px solid rgb(213, 213, 213);">
				<tr>
				<td>
					<b>Search By City</b>
					<select name="select_city_id" id="select_city_id">
	                                        <option value="">---Select City---</option>
        	                                        <xsl:for-each select="/XML/CITY_MASTER/CITY_MASTER_DATA">
                	                                        <xsl:choose>
                        	                                        <xsl:when test="/XML/SELECTED_CITY_ID=CITY_ID">
                                	                                        <option value="{CITY_ID}" selected='yes'>
                                        	                                        <xsl:value-of select="CITY_NAME"/>
                                                                                </option>
                                                                        </xsl:when>
                                                                        <xsl:otherwise>
                                                 	                       <option value="{CITY_ID}">
                                                        	                       <xsl:value-of select="CITY_NAME"/>
                                                                               </option>
                                                                        </xsl:otherwise>
                                                                 </xsl:choose>
                                                          </xsl:for-each>
                                          </select>
					  

				</td>
                                    <td colspan="9" align="right">
					<b>Search By:-</b>
					<select name="select_brand_id" id="select_brand_id" onchange="getModelByBrand('','get_model_detail.php','Model','')">
	                                        <option value="">---Select Brand---</option>
        	                                        <xsl:for-each select="/XML/BRAND_MASTER/BRAND_MASTER_DATA">
                	                                        <xsl:choose>
                        	                                        <xsl:when test="/XML/SELECTED_BRAND_ID=BRAND_ID">
                                	                                        <option value="{BRAND_ID}" selected='yes'>
                                        	                                        <xsl:value-of select="BRAND_NAME"/>
                                                                                </option>
                                                                        </xsl:when>
                                                                        <xsl:otherwise>
                                                 	                       <option value="{BRAND_ID}">
                                                        	                       <xsl:value-of select="BRAND_NAME"/>
                                                                               </option>
                                                                        </xsl:otherwise>
                                                                 </xsl:choose>
                                                          </xsl:for-each>
                                          </select>	
					  <select name="Model" id="Model" onchange="getVariantByBrandModel('','get_variant_detail.php','Variant','')">
	                                          <option value="">--All Models--</option>
                                          </select>
					  <select name="Variant" id="Variant">
                                                   <option value="">--All Variants--</option>
                                          </select>
					  <input type="submit" onclick="javascript:submitResearchForm();" value="Search"/>
				    </td>
				    <td align="right" colspan="5">					
					<b>Filter By:-</b>
					<select name="user_stat" id="user_stat" onChange="getUserInfo();">
						<option value="">--Select Status--</option>
						<xsl:choose>
                                                    <xsl:when test="/XML/SELECTED_STATUS=1">
							<option value="1" selected='yes'>Active</option>
                                                    </xsl:when>
                                                    <xsl:otherwise>
							<option value="1">Active</option>
                                                    </xsl:otherwise>
                                               </xsl:choose>
						<xsl:choose>
                                                    <xsl:when test="/XML/SELECTED_STATUS=0">
                                                        <option value="0" selected='yes'>Inactive</option>
                                                    </xsl:when>
                                                    <xsl:otherwise>
							<option value="0">Inactive</option>
                                                    </xsl:otherwise>
                                               </xsl:choose>
					</select>
                                    </td>
                                </tr>
				<tr style="border: 1px solid #d5d5d5;">
                                        <td colspan="14" style="border: 1px solid #d5d5d5;">
                                            <div align="right"><xsl:value-of select="XML/Pages" disable-output-escaping="yes"/></div>
                                        </td>
                                </tr>
                                <tr class="row0" style="padding-left:2px;">
                                    <td width="5%">SL</td>
                                    <td width="5%">Request Id</td>
                                    <td width="15%">Brand</td>
				    <td width="15%">Model</td>
				    <td width="15%">Variant</td>
                                    <td width="10%">City</td>
                                    <td width="10%">Name</td>
                                    <td width="15%">Email</td>
                                    <td width="10%">Mobile</td>
                                    <td width="10%">Landline</td>
                                    <td width="10%">Period</td>
                                    <td width="5%">Status</td>
                                    <td width="10%">Date</td>
                                    <td width="5%">Action</td>
                                </tr>
                                <xsl:choose>
                                    <xsl:when test="/XML/USER_INFO_MASTER/COUNT&lt;=0">
                                        <tr>
                                            <td colspan="13">
                                                <div align="center">Zero Result Found.</div>
                                            </td>
                                        </tr>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:for-each select="/XML/USER_INFO_MASTER/USER_INFO_MASTER_DATA">
                                            <tr class="row1">
                                                <td>
                                                    <xsl:value-of select="position()" diseable-output-esacaping="yes"/>
                                                </td>
                                                <td>
                                                    <xsl:value-of select="USER_PROFILE_ID" diseable-output-esacaping="yes"/>
                                                </td>
                                                <td>
                                                    <xsl:value-of select="BRAND_NAME" diseable-output-esacaping="yes"/>
                                                </td>
						<td>
                                                    <xsl:value-of select="PRODUCT_NAME" diseable-output-esacaping="yes"/>
                                                </td>
						<td>
                                                    <xsl:value-of select="PRODUCT_VARIANT" diseable-output-esacaping="yes"/>
                                                </td>
                                                <td>
                                                    <xsl:value-of select="CITY_NAME" diseable-output-esacaping="yes"/>
                                                </td>
                                                <td>
                                                    <xsl:value-of select="PROFILE_NAME" diseable-output-esacaping="yes"/>
                                                </td>
                                                <td>
                                                    <xsl:value-of select="EMAIL" diseable-output-esacaping="yes"/>
						  <!--<A HREF="mailto:{EMAIL}" target="_blank"><xsl:value-of select="EMAIL" diseable-output-esacaping="yes"/></A>-->
                                                </td>
                                                <td>
                                                    <xsl:value-of select="MOBILE" diseable-output-esacaping="yes"/>
                                                </td>
                                                <td>
                                                    <xsl:value-of select="CONTACT_NO" diseable-output-esacaping="yes"/>
                                                </td>
                                                <td>
                                                    <xsl:value-of select="PERIOD" diseable-output-esacaping="yes"/>
                                                </td>
                                                <td>
						   <xsl:choose>
                                                            <xsl:when test="STATUS=1">
                                                                <a href="javascript:void(0);" onClick="updateUserStatus({USER_PROFILE_ID},0);">Active</a>
                                                            </xsl:when>
                                                            <xsl:otherwise>
								<a href="javascript:void(0);" onClick="updateUserStatus({USER_PROFILE_ID},1);">Inactive</a>
                                                            </xsl:otherwise>
                                                        </xsl:choose>
                                                </td>
                                                <td>
                                                    <xsl:value-of select="CREATE_DATE" diseable-output-esacaping="yes"/>
                                                </td>
                                                <td>
							<xsl:choose>
			                                    <xsl:when test="ADMIN_REPLY=1">
                                                    		Replied
                                    			    </xsl:when>
                                                            <xsl:otherwise>
                                                    		<a href="javascript:void(0);" onClick="updateAdminReply({USER_PROFILE_ID});">Reply</a>
                                                            </xsl:otherwise>
							</xsl:choose>
                                                </td>
                                            </tr>
                                        </xsl:for-each>
                                    </xsl:otherwise>
                                </xsl:choose>
				<tr style="border: 1px solid #d5d5d5;">
                                        <td colspan="14" style="border: 1px solid #d5d5d5;">
                                            <div align="right"><xsl:value-of select="XML/Pages" disable-output-escaping="yes"/></div>
                                        </td>
                                </tr>
				<tr>
                                    <td colspan="14">
                                      <input type="hidden" name="selected_category_id" id="selected_category_id" value="{/XML/SELECTED_CATEGORY_ID}"/>
                                      <input type="hidden" name="hd_status" id="hd_status" value=""/>
                                      <input type="hidden" name="hd_update_status" id="hd_update_status" value=""/>
                                      <input type="hidden" name="hd_user_profile_id" id="hd_user_profile_id" value=""/>
                                      <input type="hidden" name="action" id="action" value=""/>
                                     </td>
                                 </tr>
                            </table>
			    </form>
                        </td>
                        <!-- main area  END -->
                        <!-- call Right Navigation -->
                        <td width="15%"  valign="top">
                            <xsl:call-template name="incRightNavigation"/>
                        </td>
                    </tr>
                    <!-- call footer -->
                    <tr>
                        <td colspan="3">
                            <xsl:call-template name="incFooter"/>
                        </td>
                    </tr>
                </table>
	    </div>
            </body>
	   <script>
		var siteURL = '<xsl:value-of select="/XML/WEB_URL" disable-output-escaping="yes"/>';
		var siteadminURL = '<xsl:value-of select="/XML/ADMIN_WEB_URL" disable-output-escaping="yes"/>';
	   </script>
	   <script type="text/javascript" charset="utf-8">
		var selected_brand_id = '<xsl:value-of select="/XML/SELECTED_BRAND_ID" disable-output-escaping="yes"/>';
		var selected_model_id = '<xsl:value-of select="/XML/SELECTED_MODEL_ID" disable-output-escaping="yes"/>';
		var selected_variant_id = '<xsl:value-of select="/XML/SELECTED_VARIANT_ID" disable-output-escaping="yes"/>';
		<xsl:text disable-output-escaping="yes">
		<![CDATA[
			if(selected_brand_id != ""){
				getModelByBrand('','get_model_detail.php','Model','');
			}
			if((selected_brand_id != "") && (selected_model_id != "")){
				document.getElementById("Model").value = selected_model_id;
				getVariantByBrandModel('','get_variant_detail.php','Variant','');
			}
			if(selected_variant_id != ""){
				document.getElementById("Variant").value = selected_variant_id;
			}
		]]>
		</xsl:text>
	</script>
        </html>
    </xsl:template>
</xsl:stylesheet>

