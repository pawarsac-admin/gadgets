<?xml version="1.0" ?>
<!DOCTYPE xsl:stylesheet  [
  <!ENTITY nbsp   "&#160;">
  <!ENTITY copy   "&#169;">
  <!ENTITY reg    "&#174;">
  <!ENTITY trade  "&#8482;">
  <!ENTITY mdash  "&#8212;">
  <!ENTITY ldquo  "&#8220;">
  <!ENTITY rdquo  "&#8221;">
  <!ENTITY pound  "&#163;">
  <!ENTITY yen    "&#165;">
  <!ENTITY euro   "&#8364;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html" version="4.0" encoding="UTF-8" indent="yes"/>
    <xsl:include href="../xsl/inc_header.xsl" /><!-- include header-->
    <xsl:include href="../xsl/inc_footer.xsl" /><!-- include footer-->
    <xsl:include href="../xsl/inc_leftnavigation.xsl" /><!-- include left navigation-->
    <xsl:include href="../xsl/inc_rightnavigation.xsl" /><!-- include right navigation-->
    <xsl:template match="/">
        <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title>Mobile : Admin Custom Top Products Management</title>
                <link rel="stylesheet" type="text/css" href="{XML/WEB_URL}css/main.css" />
		<link rel="stylesheet" href="{XML/WEB_URL}admin/css/styles.css" type="text/css" media="all" />
		<link rel="stylesheet" href="{XML/WEB_URL}admin/css/autocomplete.css" type="text/css" media="all" />
		<script> var arrProducts= Array();</script>
	    </head>
            <body>
		<style>
			#myPopUp{
				position:absolute;top:668px;left:155px;width:970px;height:550px;background:#ccc;overflow-x:hidden;overflow-y:auto;
				display:none;
			}
		</style>
		<div id="myPopUp">
			here data will appear
		</div>
                <table align="center" width="100%" border="0" cellpadding="2" cellspacing="2">
                    <!-- call Header -->
                    <tr>
                        <td colspan="3">
                            <xsl:call-template name="incHeader"/>
                        </td>
                    </tr>
                    <tr>
                        <!-- call Left Navigation -->
                        <td width="10%" valign="top">
                            <xsl:call-template name="incLeftNavigation"/>
                        </td>
                        <!-- main area -->
                        <td>
                            <div align="center">
                                <table width="100%" border="0">
                                    <tr>
                                        <td>
                                            <h2>Custom Top Products Management</h2>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <a href="{XML/ADMIN_WEB_URL}index.php">Main</a> > Custom Top Products
                                        </td>
                                    </tr>
                                </table>
                            </div>
			    <br/>
			    <div style="height:50px;" class="info">
				<div id="infox" class="info floatl">
					<span class="bold">Product Group : </span><select name="grouplist" id="grouplist">
					<xsl:for-each select="/XML/TOP_STORIES_GROUP/GROUP">
						<option value="{GROUP_ID}"><xsl:value-of select="TITLE" diseable-output-esacaping="yes"/></option>
					</xsl:for-each>
					</select>	
				</div>
				<div id="infoy" class="info floatr">
				<span class="bold">Create New Product Group : </span><form method="post" > Title : <input type="text" name="grouptitle"  size="30"/> <!-- No of Products : <input type="text" name="noofstories" size="2"/> --> <span class="spacer">&nbsp;</span><input type="hidden" name="action"  value="createnewgroup" /><span class="spacer">&nbsp;</span><input type="submit" value=" Do It " class="groovybutton"/></form></div>
			    </div>



			    <table>
                                <tr>
                                    <td>
                                        <div id="ajaxloader" style="display:none;">
                                            <div align="center">
                                                <img src="{/XML/IMAGE_URL}ajax-loader.gif"/>
                                            </div>
                                        </div>
                                        <div id="category_ajax" style="display:block;"></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div align="right">
                                            <input type="button" name="select_box" value="Select" id="select_box_id" class="formbtn" onclick="javascript:getProductList('product_dashboard','productajaxloader','','{/XML/STARTLIMIT}','{/XML/CNT}','{/XML/SELECTED_BRAND_ID}','{/XML/SELECTED_MODEL_ID}','{/XML/SELECTED_VARIANT_ID}');"/>

                                        </div>
                                    </td>
                                </tr>
                            </table>
                            <div id="productajaxloader" style="display:none;">
                                <div align="center">
                                    <img src="{/XML/IMAGE_URL}ajax-loader.gif"/>
                                </div>
                            </div>
                            <div id="product_dashboard" style="display:block;">
                                        	Please select category for product information.
                            </div>


                            <!--start product ajax data place-->
                <br/><hr/><br/>
			    <pre><h3 id="groupHeader"></h3>  <span id="info" class="info message bold">
			    <xsl:value-of select="/XML/MSG" diseable-output-esacaping="yes"/>
			    </span></pre>
				<form method="post">
				<div class="info">
				<ul class="ui-sortable" id="test-list">
				  
		<xsl:for-each select="/XML/STORIES/STORY">
			
			<li id="listItem_{STORY_ID}"><img src="/admin/images/arrow.png" alt="move" class="handle" height="16" width="16" />
			
				<span class="bold">Product : </span>

				<input type="hidden" id="product_id_{position()}" name="product_id_{position()}" value="{PRODUCT_ID}" size="6" />&nbsp;&nbsp;

				<input type="text" id="product_name_{position()}" name="product_name_{position()}" value="{PRODUCT_NAME}" readonly="readonly" />

				<span class="spacer">&nbsp;</span>
				<input type="hidden" name="story_id_{position()}" value="{STORY_ID}" />
				<!--Title : <input type="text" name="title_{position()}" value="{TITLE}" size="50"/> -->

				<span class="spacer">&nbsp;</span><span class="spacer">&nbsp;</span>
				<a href="javascript:undefined;" onclick="removeFromListProduct({PRODUCT_ID},{STORY_ID});"><img src="{/XML/ADMIN_IMAGE_URL}delete.png" border="0" alt="Remove from list"/></a>
				<script> arrProducts[ <xsl:value-of select="position()" />]= '<xsl:value-of select="PRODUCT_ID" />' </script>
			</li>
		</xsl:for-each>
				 
				</ul>
				<input type="hidden" name="group_id" value="{/XML/GROUP_ID}" />
				<input type="hidden" name="action" value="update" />
				<!--span style="float:right"><input type="submit" value="Save It" class="groovybutton" /></span-->
				</div>
				</form>

                            <!--end product ajax data place-->
                        </td>
                        <!-- main area  END -->
                        <!-- call Right Navigation -->
                        <td width="15%"  valign="top">
                            <xsl:call-template name="incRightNavigation"/>
                        </td>
                    </tr>
                    <!-- call footer -->
                    <tr>
                        <td colspan="3">
                            <xsl:call-template name="incFooter"/>
                        </td>
                    </tr>
                </table>

	    
<script type="text/javascript" src="{XML/WEB_URL}admin/js/jquery.ui.sortable.js"></script>
<script type="text/javascript" src="{XML/WEB_URL}js/autocomplete1.js"></script>

<script type="text/javascript">
var group_id = '<xsl:value-of select="/XML/GROUP_ID" diseable-output-esacaping="yes"/>';
  // When the document is ready set up our sortable with it's inherant function(s)
  $(document).ready(function() {
	$("#test-list").sortable({
	handle : '.handle',
	update : function () {
		var order = $('#test-list').sortable('serialize');
		//alert(order);
		$("#info").load("/admin/custom_top_products.php?action=rearrange&amp;group_id="+ group_id +"&amp;"+order);
		//$("#info").fadeOut("slow");
		YellowFade("#info");

	}
	});
	$('#grouplist').change(function() {
		window.location = "/admin/custom_top_products.php?group_id=" + $("#grouplist").val();
	});
	$('#grouplist').val(group_id);
	$('#groupHeader').html( $('#grouplist option:selected').text() + " : ");
});

function YellowFade(selector){
   $(selector)
      .css('opacity', 0)
      .css('visibility','visible')
      .animate({ backgroundColor: '#FCFC74', opacity: 1.0 }, 1000)
      .animate({ backgroundColor: '#FCFC74', opacity: 1.0 }, 2000)
      .animate({ backgroundColor: '#ffffff', opacity: 0.0}, 750 );
}
YellowFade("#info");
</script>

<script LANGUAGE="JavaScript" SRC="{XML/ADMIN_JS_URL}common.js"></script>
<script LANGUAGE="JavaScript" SRC="{XML/ADMIN_JS_URL}category.js"></script>
<script language="javascript" src="{XML/ADMIN_JS_URL}product.js"></script>
<script>

var siteURL		= '<xsl:value-of select="/XML/WEB_URL" disable-output-escaping="yes"/>';
var image_url	= '<xsl:value-of select="/XML/ADMIN_IMAGE_URL" />';
var n			= '<xsl:value-of select="count(/XML/STORIES/STORY)" />';

$(document).ready(function() {
category_details('<xsl:value-of select="/XML/SELECTED_CATEGORY_ID" diseable-output-escaping="yes"/>','category_ajax','ajaxloader');
<xsl:if test="/XML/SELECTED_CATEGORY_ID!=''">
	getProductList('product_dashboard','productajaxloader','<xsl:value-of select="/XML/SELECTED_CATEGORY_ID" diseable-output-escaping="yes"/>','<xsl:value-of select="/XML/STARTLIMIT" diseable-output-escaping="yes"/>','<xsl:value-of select="/XML/CNT" diseable-output-escaping="yes"/>','<xsl:value-of select="/XML/SELECTED_BRAND_ID" diseable-output-escaping="yes"/>','<xsl:value-of select="/XML/SELECTED_MODEL_ID" diseable-output-escaping="yes"/>','<xsl:value-of select="/XML/SELECTED_VARIANT_ID" diseable-output-escaping="yes"/>');
</xsl:if>
});

<![CDATA[
function getProductList(divid,ajaxloaderid,category_id,startlimit,cnt,selected_brand_id,selected_model_id,selected_variant_id){
	if(category_id == ''){
		var category_id = document.getElementById('selected_category_id').value;

		if(isCategorySelected() == false){
			alert("Please select the category.");
			return false;
		}
		if(isLastLvlCategory() == false){
			alert("Please select last level category.");
			return false;
		}
	}
	document.getElementById(ajaxloaderid).style.display = "block";
    if(divid == ""){ return false; }
		var url = admin_web_url+'ajax/product_list.php';
        $.ajax({
			url: url,
			data: 'catid='+category_id+'&startlimit='+startlimit+'&cnt='+cnt+'&selected_brand_id='+selected_brand_id+'&selected_model_id='+selected_model_id+'&selected_variant_id='+selected_variant_id,
			success: function(data){
                document.getElementById(divid).innerHTML = data;
                document.getElementById(divid).style.display="block";
                document.getElementById(ajaxloaderid).style.display = "none";
				document.getElementById('grp_id').value = group_id;
				var cnt = arrProducts.length;
				for(tCnt = 1 ; tCnt <= cnt ; tCnt++ ){
					//alert("img_"+arrProducts[tCnt]+":"+image_url + 'yes.gif');
					$("#img_"+arrProducts[tCnt]).attr('src', image_url + 'yes.gif')
				}
				
            },
            async:false
        });
	if(selected_brand_id != ""){
                getModelByBrandDashboard(selected_model_id,'get_model_detail.php','Model','');
        }
        if((selected_brand_id != "") && (selected_model_id != "")){
                document.getElementById("Model").value = selected_model_id;
                getVariantByBrandModelDashboard(selected_variant_id,'get_variants.php','Variant','');
        }
        if(selected_variant_id != ""){
                document.getElementById("Variant").value = selected_variant_id;
        }
	return true;
}
function addToListProduct(a,b,c){ // a: product id     b:product name     c: position id  

	var d = 999999; // d : new row id

	$.ajax({
	  url: "/admin/ajax/ajax.php?action=addtolist&group_id="+group_id+"&product_id="+a+"&product_name="+b,
	  context: document.body,
	  success: function(data){
		//alert("done :" + data);
		d = data;
			$('#test-list').append('<li id="listItem_'+d+'"><img width="16" height="16" class="handle" alt="move" src="/admin/images/arrow.png"><span class="bold">Product : </span><input type="hidden" size="6" value="'+a+'" name="product_id_'+n+'" id="product_id_'+n+'"><input type="text" readonly="" value="'+b+'" name="product_name_'+n+'" id="product_name_'+n+'"><span class="spacer"></span><input type="hidden" value="'+d+'" name="story_id_'+n+'"><span class="spacer"></span><span class="spacer"></span><a onclick="removeFromListProduct(' + a + ',' + d + ');" href="javascript:undefined;"><img border="0" alt="Remove from list" src="http://mobilebuyguide-dev.int.india.com/admin/images/delete.png" /></a></li>');
			$('#p_'+c).html('<img border="0" alt="In list" src="http://mobilebuyguide-dev.int.india.com/admin/images/yes.gif" />');
			n= n+1;

			$("#info").html("Product added to list.");
			YellowFade("#info");
			var cnt = arrProducts.length + 1 ;
			arrProducts[cnt] = a;
	  }
	});
}

function removeByElement(arrayName,arrayElement)
 {
  for(var i=0; i<arrayName.length;i++ )
   { 
  if(arrayName[i]==arrayElement)
  arrayName.splice(i,1); 
  } 
}

function removeFromListProduct(a,c){ // a: product id     c: position id  
	$.ajax({
	  url: "/admin/ajax/ajax.php?action=removefromlist&story_id=" + c ,
	  context: document.body,
	  success: function(){
		$('#listItem_'+c).remove();
		$("#info").html("Product removed from list.");
		removeByElement(arrProducts,a)
		YellowFade("#info");
	  }
	});
}

function sProductPagination_new(page,startlimit,cnt,filename,divid,category_id){
	
	if(category_id == ''){
		var category_id = document.getElementById('selected_category_id').value;

		if(isCategorySelected() == false){
			alert("Please select the category.");
			return false;
		}
		if(isLastLvlCategory() == false){
			alert("Please select last level category.");
			return false;
		}
	}
	
	var selected_brand_id="";var selected_model_id = "";var selected_variant_id="";
	selected_brand_id = document.getElementById('select_brand').value;
	selected_model_id = document.getElementById('Model').value;
	selected_variant_id = document.getElementById('Variant').value;
	if(divid == ""){ return false; }
		var url = admin_web_url+filename;

		$.ajax({
			url: url,
			data: 'catid='+category_id+'&page='+page+'&startlimit='+startlimit+'&cnt='+cnt+'&selected_brand_id='+selected_brand_id+'&selected_model_id='+selected_model_id+'&selected_variant_id='+selected_variant_id,
			success: function(data){
				//alert(data);
                document.getElementById(divid).innerHTML = data;
                document.getElementById(divid).style.display="block";

					var cnt = arrProducts.length;
					for(tCnt = 1 ; tCnt < cnt ; tCnt++ ){
						//alert("img_"+arrProducts[tCnt]+":"+image_url + 'yes.gif');
						$("#img_"+arrProducts[tCnt]).attr('src', image_url + 'yes.gif')
					}

               },
            async:false
        });
	if(selected_brand_id != ""){
                getModelByBrandDashboard(selected_model_id,'get_model_detail.php','Model','');
        }
        if((selected_brand_id != "") && (selected_model_id != "")){
                document.getElementById("Model").value = selected_model_id;
                getVariantByBrandModelDashboard(selected_variant_id,'get_variants.php','Variant','');
        }
        if(selected_variant_id != ""){
                document.getElementById("Variant").value = selected_variant_id;
        }
	return true;
}

]]>
</script>
</body>
        </html>
    </xsl:template>
</xsl:stylesheet>