<?xml version="1.0" ?>
<!DOCTYPE xsl:stylesheet  [
  <!ENTITY nbsp   "&#160;">
  <!ENTITY copy   "&#169;">
  <!ENTITY reg    "&#174;">
  <!ENTITY trade  "&#8482;">
  <!ENTITY mdash  "&#8212;">
  <!ENTITY ldquo  "&#8220;">
  <!ENTITY rdquo  "&#8221;">
  <!ENTITY pound  "&#163;">
  <!ENTITY yen    "&#165;">
  <!ENTITY euro   "&#8364;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html" version="4.0" encoding="UTF-8" indent="yes"/>
    <xsl:include href="../xsl/inc_header.xsl" /><!-- include header-->
    <xsl:include href="../xsl/inc_footer.xsl" /><!-- include footer-->
    <xsl:include href="../xsl/inc_leftnavigation.xsl" /><!-- include left navigation-->
    <xsl:include href="../xsl/inc_rightnavigation.xsl" /><!-- include right navigation-->
    <xsl:template match="/">
        <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title>Mobile : Admin City Management</title>
                <link rel="stylesheet" type="text/css" href="{XML/WEB_URL}css/main.css" />
            </head>
            <body>
                <table align="center" width="100%" border="0" cellpadding="2" cellspacing="2">
                    <!-- call Header -->
                    <tr>
                        <td colspan="3">
                            <xsl:call-template name="incHeader"/>
                        </td>
                    </tr>
                    <tr>
                        <!-- call Left Navigation -->
                        <td width="10%" valign="top">
                            <xsl:call-template name="incLeftNavigation"/>
                        </td>
                        <!-- main area -->
                        <td>
                            <div align="center">
                                <table width="100%" border="0">
                                    <tr>
                                        <td>
                                            <h2>City Management</h2>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <a href="{XML/ADMIN_WEB_URL}index.php">Main</a>
>  City Dashboard</td>
                                    </tr>
                                </table>
                            </div>
                            <table width="100%" border="1" id="feature_unit_dashboard">
                                <tr>
                                    <td colspan="6">
                                        <div align="center">
                                            <h3>Dashboard</h3>
                                        </div>
					State:
					<select id="select_state_id_dash" name="select_state_id_dash" onchange="getCity();">
					<option value="0">---select state----</option>
					<xsl:value-of select="XML/SSTATE" disable-output-escaping="yes"/>
					</select>	
                                    </td>
				    
				</tr>

                                <xsl:if test="/XML/MSG!=''">
                                    <tr>
                                        <td colspan="6" bgcolor="#98AFC7">
                                            <div align="center">
                                                <xsl:value-of select="/XML/MSG" diseable-output-esacaping="yes"/>
                                            </div>
                                        </td>
                                    </tr>
                                </xsl:if>
                                <tr>
                                    <td colspan="6">
                                        <div align="right">pagination goes here</div>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15%">Sr no.</td>
                                    <td width="37%">City Name</td>
                                    <td width="12%">Status</td>
                                    <td width="12%">Create Date</td>
                                    <td colspan="2">Action</td>
                                </tr>
                                <xsl:choose>
                                    <xsl:when test="/XML/CITY_MASTER/COUNT&lt;=0">
                                        <tr>
                                            <td colspan="6">
                                                <div align="center">Zero Result Found.</div>
                                            </td>
                                        </tr>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:for-each select="/XML/CITY_MASTER/CITY_MASTER_DATA">
                                            <tr>
                                                <td>
                                                    <xsl:value-of select="position()" diseable-output-esacaping="yes"/>
                                                </td>
                                                <td>
                                                    <xsl:value-of select="CITY_NAME" diseable-output-esacaping="yes"/>
                                                </td>
                                                <td>
                                                    <xsl:value-of select="CITY_STATUS" diseable-output-esacaping="yes"/>
                                                </td>
                                                <td>
                                                    <xsl:value-of select="CREATE_DATE" diseable-output-esacaping="yes"/>
                                                </td>
                                                <td width="7%">
                                                    <a href="#Update" onclick="updateCity('{CITY_ID}','{JS_CITY_NAME}','{CITY_STATUS}','{STATE_ID}');">Update</a>
                                                </td>
                                                <!--<td width="17%">
                                                    <a href="javascript:undefined;" onclick="deleteCity('{CITY_ID}','{JS_CITY_NAME}');">Delete</a>
                                                </td>-->
                                            </tr>
                                        </xsl:for-each>
                                    </xsl:otherwise>
                                </xsl:choose>
                                <tr>
                                    <td colspan="6">
                                        <div align="right">pagination goes here</div>
                                    </td>
                                </tr>
                            </table>
                            <table width="100%" border="0">
                                <tr>
                                    <td colspan="6" width="100%">
                                        <h3>Add a new State</h3>
                                    </td>
                                </tr>
                            </table>
                            <form action="{/XML/ADMIN_WEB_URL}city.php" method="post" name="city_action" id="city_action" onsubmit="return validateCity();">
                                <table width="100%" id="Update" border="1">
				 <tr>
                                        <td>State</td>
                                        <td colspan="2">
                                            <select id="select_state_id" name="select_state_id">
                                               <xsl:value-of select="XML/SSTATE" disable-output-escaping="yes"/>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="13%">City Name</td>
                                        <td colspan="2">
                                            <input type="text" name="city_name" id="city_name"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Status</td>
                                        <td colspan="2">
                                            <select id="city_status" name="city_status">
                                                <option value="1" selected="yes">Active</option>
                                                <option value="0">InActive</option>
                                            </select>


						
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input type="hidden" name="state_id" id="state_id" value=""/>
											 <input type="hidden" name="city_id" id="city_id" value=""/>
                                        </td>
                                        <td colspan="2">
                                            <input type="hidden" name="actiontype" id="actiontype" value="select"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input type="hidden" name="startlimit" id="startlimit" value="{/XML/STARTLIMIT}"/>
                                        </td>
                                        <td colspan="2">
                                            <input type="hidden" name="cnt" id="cnt" value="{/XML/CNT}"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="26">&nbsp;</td>
                                        <td width="7%">
                                            <input type="submit" name="statesubmit" id="statesubmit" value="Add/Update"/>
                                        </td>
                                        <td width="80%">
                                            <input type="button" name="cancel" id="cancel" value="Cancel" onclick="javascript:this.form.reset();"/>
                                        </td>
                                    </tr>
                                </table>
                            </form>
                        </td>
                        <!-- main area  END -->
                        <!-- call Right Navigation -->
                        <td width="15%"  valign="top">
                            <xsl:call-template name="incRightNavigation"/>
                        </td>
                    </tr>
                    <!-- call footer -->
                    <tr>
                        <td colspan="3">
                            <xsl:call-template name="incFooter"/>
                        </td>
                    </tr>
                </table>
            </body>
            <script LANGUAGE="JavaScript" SRC="{XML/ADMIN_JS_URL}city.js"></script>
        </html>
    </xsl:template>
</xsl:stylesheet>
