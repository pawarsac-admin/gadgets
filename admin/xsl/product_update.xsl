<?xml version="1.0" ?>
<!DOCTYPE xsl:stylesheet  [
  <!ENTITY nbsp   "&#160;">
  <!ENTITY copy   "&#169;">
  <!ENTITY reg    "&#174;">
  <!ENTITY trade  "&#8482;">
  <!ENTITY mdash  "&#8212;">
  <!ENTITY ldquo  "&#8220;">
  <!ENTITY rdquo  "&#8221;">
  <!ENTITY pound  "&#163;">
  <!ENTITY yen    "&#165;">
  <!ENTITY euro   "&#8364;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html" version="4.0" encoding="UTF-8" indent="yes"/>
    <xsl:include href="inc_header.xsl" /><!-- include header-->
    <xsl:include href="inc_footer.xsl" /><!-- include footer-->
    <xsl:template match="/">
        <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title>:: Ajax - Admin Product Dashboard Management ::</title>
                <link rel="stylesheet" type="text/css" href="{/XML/CSS_URL}main.css" />
                <script>
		        var admin_web_url = '<xsl:value-of select="/XML/ADMIN_WEB_URL" disable-output-escaping="yes"/>';
           </script>
            </head>
            <body>
                <!--start code to add product form -->
                                <form enctype="multipart/form-data" method="post" action="{XML/ADMIN_WEB_URL}product.php" name="product_manage" id="product_manage" onsubmit="return validateProduct();">
                                    <table width="100%" border="0" id="add_product_table">                                        
                                        <tr>
                                            <td align="right">                                                
                                                <table width="100%" border="0">
                                                    <tr>
                                                        <td>
                                                            <input type="hidden" name="startlimit" id="startlimit" value="{/XML/STARTLIMIT}"/>
                                                        </td>
                                                        <td>
                                                            <input type="hidden" name="cnt" id="cnt" value="{/XML/CNT}"/>
                                                        </td>
                                                        <td>
                                                            <input type="hidden" name="product_id" id="product_id"/>
                                                        </td>
                                                        <td>
                                                            <input type="hidden" name="actiontype" id="actiontype" value="insert"/>
                                                        </td>
                                                        <td>
                                                            <input type="hidden" name="featureboxcnt" id="featureboxcnt" value="{/XML/FEATURE_MASTER/COUNT}"/>
                                                        </td>
                                                        <td></td>
                                                        <td colspan="2"></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                </table>
                                                <table width="100%" id="Update" border="1">
                                                    <tr>
                                                        <td>Brand Name</td>
                                                        <td colspan="10">
                                                            <select name="select_brand_id" id="select_brand_id">
                                                                <option value="">---Select Brand---</option>
                                                            <xsl:for-each select="/XML/BRAND_MASTER/BRAND_MASTER_DATA">
                                                                <option value="{BRAND_ID}">
                                                                    <xsl:value-of select="BRAND_NAME"/>
                                                                </option>
                                                            </xsl:for-each>
                                                            </select>
                                                        </td>
                                                    </tr>
						    <tr>
							<td>Product Name</td>
                                                        <td colspan="10">
								<input type="text" name="product_name" id="product_name" size="50"/>
							</td>
						    </tr>
						    <tr>
                                                	<td>Varient</td>
	                                                <td colspan="10">
        	                                            <input type="text" name="varient" id="varient" size="40"/>
                	                                </td>
                        	                    </tr>
						    <tr>
                                                	<td>Product Description</td>
                                                	<td colspan="10">
                                                    		<textarea name="product_description" id="product_description" cols="30"></textarea>
                                                	</td>
                                            	   </tr>
						   <tr>
                                                	<td>Product MRP(ex-showroom)</td>
	                                                <td colspan="10">
        	                                            <input type="text" name="product_mrp_ex_showroom" id="product_mrp_ex_showroom" size="40" />
        	                                        </td>
                	                            </tr>
												<tr>
                                                	<td>Show room state</td>
	                                                <td colspan="10">
														<select name="state_id" id="state_id" onchange="javascript:city_details('ajaxloader');">
															<option value="">---Select State---</option>
															<xsl:for-each select="/XML/STATE_MASTER/STATE_MASTER_DATA">
																<option value="{STATE_ID}"><xsl:value-of select="STATE_NAME" disable-output-escaping="yes"/></option>
															</xsl:for-each>
							<div id="ajaxloader" style="display:none;">
                                                <div align="center">
                                                    <img src="{/XML/IMAGE_URL}ajax-loader.gif"/>
                                                </div>
                                            </div>							</select>
        	                                        </td>
                	                            </tr>
												<tr>
                                                	<td>Product MRP(On-Road)</td>
	                                                <td colspan="10">
        	                                            <input type="text" name="product_mrp_on_road" id="product_mrp_on_road" size="40" />
        	                                        </td>
                	                            </tr>
						    <tr>
                                                	<td>Upcoming Product</td>
	                                                <td colspan="10">
        	                                            <select name="is_upcomimg" id="is_upcomimg">
								<option value="0">No</option>
                                                                <option value="1">Yes</option>                                                                
                                                            </select>
        	                                        </td>
                	                            </tr>
						    <tr>
                                                	<td>Latest Product</td>
	                                                <td colspan="10">
        	                                            <select name="is_latest" id="is_latest">
								<option value="0">No</option>
                                                                <option value="1">Yes</option>                                                                
                                                            </select>
        	                                        </td>
                	                            </tr>
						    <tr>
                                                	<td>Upload Creative 1:</td>
                                                	<td colspan="10">
                                                    		<input name="img_upload_1" type="file" size="40" id="img_upload_id_1"/>
                                                	</td>
                                            	    </tr>
                                                    <tr>
                                                        <td>Product Status#1</td>
                                                        <td colspan="14">
                                                            <select name="product_status" id="product_status">
                                                                <option value="1">Active</option>
                                                                <option value="0">InActive</option>
                                                            </select>
                                                        </td>
                                                    </tr>
						    <xsl:for-each select="XML/FEATURE_MASTER/FEATURE_MASTER_DATA">
                                                <tr>
                                                    <xsl:choose>
                                                        <!--start condition is used to check complusary product feature-->
                                                        <xsl:when test="PIVOT_FEATURE_ID=FEATURE_ID">
                                                            <xsl:choose>
                                                                <xsl:when test="FEATURE_STATUS='1'">
                                                                    <!--start condition is used to check active product-->
                                                                    <td bgcolor="gray">
									here
                                                                        <xsl:value-of select="FEATURE_NAME" disable-output-esacaping="yes"/>
                                                                    </td>
                                                                    <td colspan="10" bgcolor="gray">
                                                                        <input type="text" name="feature_value_{position()}" id="feature_value_id_{PIVOT_FEATURE_ID}" size="40" />
                                                                        <input type="hidden" name="feature_id_{position()}" id="feature_id_{position()}" size="40" value="{FEATURE_ID}"/>
                                                                    </td>
                                                                </xsl:when>
                                                                <xsl:otherwise>
                                                                    <!--start condition is used to check inactive product-->
                                                                    <td bgcolor="#adadad">
                                                                        <xsl:value-of select="FEATURE_NAME" disable-output-esacaping="yes"/>(<xsl:value-of select="FEATURE_STATUS" disable-output-esacaping="yes"/>)
								    </td>
                                                                    <td colspan="10" bgcolor="#adadad">
                                                                        <input type="text" name="feature_value_{position()}" id="feature_value_id_{PIVOT_FEATURE_ID}" size="40" />
                                                                        <input type="hidden" name="feature_id_{position()}" id="feature_id_{position()}" size="40" value="{FEATURE_ID}"/>
                                                                    </td>
                                                                </xsl:otherwise>
                                                            </xsl:choose>
                                                        </xsl:when>
                                                        <!--start condition is used to check not complusary product feature-->
                                                        <xsl:otherwise>
                                                            <xsl:choose>
                                                                <xsl:when test="STATUS='1'">
                                                                    <!--start condition is used to check active product-->
                                                                    <td bgcolor="gray">
                                                                        <xsl:value-of select="FEATURE_NAME" disable-output-esacaping="yes"/>
                                                                    </td>
                                                                    <td colspan="10" bgcolor="gray">
                                                                        <input type="text" name="feature_value_{position()}" id="feature_value_id_{position()}" size="40" />
                                                                        <input type="hidden" name="feature_id_{position()}" id="feature_id_{position()}" size="40" value="{FEATURE_ID}"/>
                                                                    </td>
                                                                </xsl:when>
                                                                <xsl:otherwise>
                                                                    <!--start condition is used to check inactive product-->
                                                                    <td bgcolor="#adadad">
                                                                        <xsl:value-of select="FEATURE_NAME" disable-output-esacaping="yes"/>(<xsl:value-of select="FEATURE_STATUS" disable-output-esacaping="yes"/>)
								    </td>
                                                                    <td colspan="10" bgcolor="#adadad">
                                                                        <input type="text" name="feature_value_{position()}" id="feature_value_id_{position()}" size="40" />
                                                                        <input type="hidden" name="feature_id_{position()}" id="feature_id_{position()}" size="40" value="{FEATURE_ID}"/>
                                                                    </td>
                                                                </xsl:otherwise>
                                                            </xsl:choose>
                                                        </xsl:otherwise>
                                                    </xsl:choose>
                                                </tr>
                                            </xsl:for-each>
                                                    <tr>						
                                                        <td colspan="11"><input type="hidden" name="selected_category_id" id="selected_category_id" value="{/XML/SELECTED_CATEGORY_ID}"/></td>
						     </tr>
						    <tr>
							<td colspan="9">&nbsp;</td>
                                                        <td>
                                                            <div align="center">
                                                                <input type="submit" name="save" value="Save" class="formbtn"/>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div align="center">
                                                                <input type="button" name="cancel" value="Cancel" class="formbtn" onclick="javascript:this.form.reset();"/>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </form>
                                <!--end code to add product form -->
                            </div>
                        </td>
                        <!-- main area  END -->
                    </tr>
                </table>
            </body>
	    

        </html>
    </xsl:template>
</xsl:stylesheet>
