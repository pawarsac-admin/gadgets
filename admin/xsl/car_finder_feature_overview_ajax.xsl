<?xml version="1.0" ?>
<!DOCTYPE xsl:stylesheet  [
  <!ENTITY nbsp   "&#160;">
  <!ENTITY copy   "&#169;">
  <!ENTITY reg    "&#174;">
  <!ENTITY trade  "&#8482;">
  <!ENTITY mdash  "&#8212;">
  <!ENTITY ldquo  "&#8220;">
  <!ENTITY rdquo  "&#8221;">
  <!ENTITY pound  "&#163;">
  <!ENTITY yen    "&#165;">
  <!ENTITY euro   "&#8364;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:output method="html" version="4.0" encoding="UTF-8" indent="yes"/>
  <xsl:include href="inc_header.xsl" /><!-- include header-->
  <xsl:include href="inc_footer.xsl" /><!-- include footer-->
  <xsl:template match="/">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>:: Ajax - Admin Car Finder Feature Overview Dashboard Management ::</title>
        <link rel="stylesheet" type="text/css" href="{/XML/CSS_URL}main.css" />
        <script LANGUAGE="JavaScript" SRC="{XML/ADMIN_JS_URL}common.js"></script>
        <script LANGUAGE="JavaScript" SRC="{XML/ADMIN_JS_URL}category.js"></script>
        <script LANGUAGE="JavaScript" SRC="{XML/ADMIN_JS_URL}car_finder_feature_overview.js"></script>
      </head>
      <body>
              
             
        <div id="sFeatureOverDiv">
        <table align="center" width="100%" border="0" cellpadding="2" cellspacing="2">
          <tr>
            <td>
              <div align="center">
                               

                <table width="100%" border="1" id="feature_overview_dashboard">
                  <tr>
                    <td colspan="12">
                      <div align="center">
                        <h3>Dashboard</h3>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td colspan="12">
                      <div align="right"><xsl:value-of select="XML/Pages" disable-output-escaping="yes"/></div>
                    </td>
                  </tr>
                  <tr>
                    <td width="15%">Sr no.</td>
                    <td width="37%">Category Name</td>
                    <td width="37%">Feature Name</td>
                    <td width="37%">Feature Title</td>
                    <td width="37%">Feature Display Unit</td>
                    <td width="12%">Status</td>
                    <td width="12%">Create Date</td>
                    <td colspan="3">&nbsp;</td>
                    <td colspan="2">Action</td>
                  </tr>
                  <xsl:choose>
                    <xsl:when test="/XML/FEATURE_OVERVIEW_MASTER/COUNT&lt;=0">
                      <tr>
                        <td colspan="7">
                          <div align="center">Zero result found.</div>
                        </td>
                      </tr>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:for-each select="/XML/FEATURE_OVERVIEW_MASTER/FEATURE_OVERVIEW_MASTER_DATA">
                        <tr>
                          <td>
                            <xsl:value-of select="position()" diseable-output-esacaping="yes"/>
                          </td>
                          <td>
                            <xsl:value-of select="CATEGORY_NAME" diseable-output-esacaping="yes"/>
                          </td>
                          <td>
                            <xsl:value-of select="FEATURE_NAME" diseable-output-esacaping="yes"/>
                          </td>
                          <td>
                            <xsl:value-of select="TITLE" diseable-output-esacaping="yes"/>
                          </td>
                          <td>
                            <xsl:value-of select="ABBREVIATION" diseable-output-esacaping="yes"/>
                          </td>
                          <td>
                            <xsl:value-of select="FEATURE_OVERVIEW_STATUS" diseable-output-esacaping="yes"/>
                          </td>
                          <td>
                            <xsl:value-of select="CREATE_DATE" diseable-output-esacaping="yes"/>
                          </td>
                          <td>Up</td>
                          <td colspan="2">Down</td>
                          <!--<td width="7%">
                              <a href="#Update" onclick="updateBrand('{BRAND_ID}','{JS_BRAND_NAME}','{STATUS}');">Update</a>
                          </td>-->
                          <td width="17%">
                            <a href="javascript:undefined;" onclick="deleteCarFinderFeatureOverview('{OVERVIEW_ID}','{JS_FEATURE_NAME}');">Delete</a>
                          </td>
                        </tr>
                      </xsl:for-each>
                    </xsl:otherwise>
                  </xsl:choose>
                  <tr>
                    <td colspan="12">
                      <div align="right"><xsl:value-of select="XML/Pages" disable-output-escaping="yes"/></div>
                    </td>
                  </tr>
                </table>
                               

              </div>
            </td>
            <!-- main area  END -->
          </tr>
        </table>
	
        <table align="center" width="100%" border="1" cellpadding="2" cellspacing="2">
          <form method="post" name="feature_main_group_frm" action="{/XML/ADMIN_WEB_URL}car_finder_feature_overview.php">
            <input type="hidden" name="selected_category_id" id="selected_category_id" value="{/XML/SELECTED_CATEGORY_ID}"/>
	    <input type="hidden" name="actiontype" id="actiontype" value="insert"/>
	    <input type="hidden" name="overview_id" id="overview_id" value=""/>
            <tr>
              <td>Select Main Group</td>
              <td>
                <select name="sel_main_group" id="sel_main_group" onchange="javascript:this.form.submit();">
                  <option value="">--select Feature Group--</option>
                  <xsl:for-each select="/XML/FEATURE_GROUP_MASTER/FEATURE_GROUP_MASTER_DATA">
                    <xsl:if test="MAIN_GROUP_NAME!=''">
                      <xsl:choose>
                        <xsl:when test="/XML/SELECTED_MAIN_GROUP_ID=GROUP_ID">
                          <option value="{GROUP_ID}" selected="yes">
                            <xsl:value-of select="MAIN_GROUP_NAME" disable-output-escaping="yes"/>
                          </option>
                        </xsl:when>
                        <xsl:otherwise>
                          <option value="{GROUP_ID}">
                            <xsl:value-of select="MAIN_GROUP_NAME" disable-output-escaping="yes"/>
                          </option>
                        </xsl:otherwise>
                      </xsl:choose>
                    </xsl:if>
                  </xsl:for-each>
                </select>
              </td>
            </tr>
          </form>
        </table>
	
        <xsl:choose>
          <xsl:when test="/XML/SELECTED_MAIN_GROUP_ID=''">
                                        Please select Main Feature Group to add new overview.
          </xsl:when>
          <xsl:otherwise>
            <table width="100%" border="0">
              <tr>
                <td colspan="6" width="100%">
                  <h3>Add a new Car Finder Feature Overview</h3>
                </td>
              </tr>
              <xsl:if test="/XML/MSG!=''">
                <tr>
                  <td colspan="6" bgcolor="#98AFC7">
                    <div align="center">
                      <xsl:value-of select="/XML/MSG" diseable-output-esacaping="yes"/>
                    </div>
                  </td>
                </tr>
              </xsl:if>
            </table>
            <table width="100%" id="Update" border="1">
              <tr>
                <td width="13%">Select Feature</td>
                <td colspan="2">
                  <select name="feature_id" id="feature_id">
                    <option value="">--Select Feature--</option>
                    <xsl:for-each select="/XML/FEATURE_MASTER/FEATURE_MASTER_DATA">
                      <option value="{FEATURE_ID}">
                        <xsl:value-of select="FEATURE_NAME" disable-output-escaping="yes"/>
                      </option>
                    </xsl:for-each>
                  </select>
                </td>
              </tr>
              <tr>
                <td width="13%">Feature Title</td>
                <td colspan="2">
                  <input type="text" name="feature_title" id="feature_title"/>
                </td>
              </tr>
              <tr>
                <td width="13%">Feature Overview Display Unit</td>
                <td colspan="2">
                  <input type="text" name="feature_display_unit" id="feature_display_unit"/>
                </td>
              </tr>
              <tr>
                <td>Status</td>
                <td colspan="2">
                  <select id="feature_overview_status" name="feature_overview_status">
                    <option value="1">Active</option>
                    <option value="0">InActive</option>
                  </select>
                </td>
              </tr>
              <tr>
                <td><!--<input type="hidden" name="category_id" id="category_id" value=""/>--></td>
                <td colspan="2">
                  
                </td>
              </tr>
              <tr>
                <td>
                  <input type="hidden" name="startlimit" id="startlimit" value="{/XML/STARTLIMIT}"/>
                </td>
                <td colspan="2">
                  <input type="hidden" name="cnt" id="cnt" value="{/XML/CNT}"/>
                </td>
              </tr>
              <tr>
                <td height="26">
                  
                </td>
                <td width="7%">
                  <input type="submit" name="overviewsubmit" id="overviewsubmit" value="Add/Update"/>
                </td>
                <td width="80%">
                  <input type="button" name="cancel" id="cancel" value="Cancel" onclick="javascript:this.form.reset();"/>
                </td>
              </tr>
            </table>
          </xsl:otherwise>
        </xsl:choose>
	</div>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
