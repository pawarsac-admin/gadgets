<?xml version="1.0" ?>
<!DOCTYPE xsl:stylesheet  [
  <!ENTITY nbsp   "&#160;">
  <!ENTITY copy   "&#169;">
  <!ENTITY reg    "&#174;">
  <!ENTITY trade  "&#8482;">
  <!ENTITY mdash  "&#8212;">
  <!ENTITY ldquo  "&#8220;">
  <!ENTITY rdquo  "&#8221;">
  <!ENTITY pound  "&#163;">
  <!ENTITY yen    "&#165;">
  <!ENTITY euro   "&#8364;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html" version="4.0" encoding="UTF-8" indent="yes"/>
    <xsl:include href="../xsl/inc_header.xsl" /><!-- include header-->
    <xsl:include href="../xsl/inc_footer.xsl" /><!-- include footer-->
    <xsl:include href="../xsl/inc_leftnavigation.xsl" /><!-- include left navigation-->
    <xsl:include href="../xsl/inc_rightnavigation.xsl" /><!-- include right navigation-->
    <xsl:template match="/">
        <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title>Mobile : Admin Upload Price sheet Management</title>
                <link rel="stylesheet" type="text/css" href="{XML/WEB_URL}css/main.css" />
            </head>
            <body>
                <table align="center" width="100%" border="0" cellpadding="2" cellspacing="2">
                    <!-- call Header -->
                    <tr>
                        <td colspan="3">
                            <xsl:call-template name="incHeader"/>
                        </td>
                    </tr>
                    <tr>
                        <!-- call Left Navigation -->
                        <td width="10%" valign="top">
                            <xsl:call-template name="incLeftNavigation"/>
                        </td>
                        <!-- main area -->
                        <td>
                            <div align="center">
                                <table width="100%" border="0">
                                    <tr>
                                        <td>
                                            <h2>Upload Price sheet Management</h2>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <a href="{XML/ADMIN_WEB_URL}index.php">Main</a>
>  Upload Price sheet Dashboard</td>
                                    </tr>
                                </table>
                            </div>
                            <table width="100%" border="0">
                                <tr>
                                    <td colspan="6" width="100%">
                                        <h3>Add a new Upload Price sheet</h3>
                                    </td>
                                </tr>
                                <xsl:if test="/XML/MSG!=''">
                                    <tr>
                                        <td colspan="6" bgcolor="#98AFC7">
                                            <div align="center">
                                                <xsl:value-of select="/XML/MSG" diseable-output-esacaping="yes"/>
                                            </div>
                                        </td>
                                    </tr>
                                </xsl:if>
                            </table>
                                <table id="show_categorys">
                                    <tr>
                                        <td>
                                            <div id="ajaxloader" style="display:none;">
                                                <div align="center">
                                                    <img src="{/XML/IMAGE_URL}ajax-loader.gif"/>
                                                </div>
                                            </div>
                                            <div id="category_ajax" style="display:none;"></div>
                                            <div align="right">
                                               <!-- <input type="button" name="select_category" id="select_category" value="Select Category" onclick="javascript:getSheetDashboard('sheet_dashboard','sheetajaxloader','','{/XML/STARTLIMIT}','{/XML/CNT}');"/>-->
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <div id="sheetajaxloader" style="display:none;">
				  
                                    <div align="center">
                                        <img src="{/XML/IMAGE_URL}ajax-loader.gif"/>
                                    </div>
                                </div>
                                <div id="sheet_dashboard" style="display:block;">
				   <xsl:if test="/XML/MESSAGE!=''">
				      <div> <xsl:value-of select="XML/MESSAGE" disable-output-escaping="yes"/> </div>
				   </xsl:if>
					<form enctype="multipart/form-data" action="{/XML/ADMIN_WEB_URL}upload_sheet.php" method="post" name="sheet_action" id="sheet_action" onsubmit="return ValidateTest(this);">
			        <table width="100%" id="Update" border="1" >
                                   <tr>
								<td>Country Name</td>
								<td colspan="10">
									<select name="select_country_id" id="select_country_id" onchange="getStateByCountry('ajaxloadercountry','0');">
										<option value="">---Select Country---</option>
										<xsl:for-each select="/XML/COUNTRY_MASTER/COUNTRY_MASTER_DATA">
											<xsl:if test="/XML/VARIANT_VALUE_DATA/COUNTRY_ID=COUNTRY_ID">
											<option value="{COUNTRY_ID}" selected='yes'>
											<xsl:value-of select="COUNTRY_NAME"/>
											</option>
											</xsl:if>
											<xsl:if test="not(/XML/VARIANT_VALUE_DATA/COUNTRY_ID=COUNTRY_ID)">
											<option value="{COUNTRY_ID}">
											<xsl:value-of select="COUNTRY_NAME"/>
											</option>
											</xsl:if>
										</xsl:for-each>
									</select>
									<div id="ajaxloadercountry" style="display:none;">
										<div align="center">
											<img src="{/XML/IMAGE_URL}ajax-loader.gif"/>
										</div>
									</div>
								</td>
							</tr>
                                    <tr>
                                        <td>Upload Price Sheet</td>
                                        <td colspan="2">
                                                <input name="uploadedfile" type="file"/><xsl:value-of select="XML/BRAND_DETAIL/BRAND_DETAIL_DATA/BRAND_IMAGE"/>
						<img src="{XML/BRAND_DETAIL/BRAND_DETAIL_DATA/BRAND_IMAGE_PATH}" width="50px" height="50px" />
                                        </td>
                                    </tr>
				   
				    <tr>
                                        <td colspan="3">
                                            <input type="hidden" name="actiontype" id="actiontype" value="insert"/>
                                            <input type="hidden" name="startlimit" id="startlimit" value="{/XML/STARTLIMIT}"/>
                                            <input type="hidden" name="cnt" id="cnt" value="{/XML/CNT}"/>
                                            <input type="hidden" name="sheet_id" id="sheet_id" value=""/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" align="center">
                                            <input type="submit" name="sheetsubmit" id="sheetsubmit" value="Add/Update"/>
                                            <input type="button" name="cancel" id="cancel" value="Cancel" onclick="javascript:this.form.reset();"/>

                                        </td>
                                    </tr>
				    <tr>
					<td>
					  <xsl:if test="/XML/START_PROCESS=1">
					        <input type="hidden" name="city_id" id="city_id" value="{/XML/CITY_ID}"/>
						<input type="hidden" name="process" id="process" value="{/XML/START_PROCESS}"/>
						<input type="hidden" name="process_ins" id="process_ins" value=""/>
						<input type="hidden" name="file_name" id="file_name" value="{/XML/FILENAME}"/>
						<a href="javascript:void(0);" onclick="InsertInDb('{XML/FILENAME}','1');">Click to Insertion in db</a>
						
					  </xsl:if>
					</td>
				    </tr>
                                </table>
                            </form>
                                </div>
                        </td>
                        <!-- main area  END -->
                        <!-- call Right Navigation -->
                        <td width="15%"  valign="top">
                            <xsl:call-template name="incRightNavigation"/>
                        </td>
                    </tr>
                    <!-- call footer -->
                    <tr>
                        <td colspan="3">
                            <xsl:call-template name="incFooter"/>
                        </td>
                    </tr>
                </table>
            </body>
            <script LANGUAGE="JavaScript" SRC="{XML/ADMIN_JS_URL}common.js"></script>
            <script LANGUAGE="JavaScript" SRC="{XML/ADMIN_JS_URL}category.js"></script>
            <script LANGUAGE="JavaScript" SRC="{XML/ADMIN_JS_URL}upload_sheet.js"></script>
            <script>
                $(document).ready(function() {
                    category_details('<xsl:value-of select="/XML/SELECTED_CATEGORY_ID" diseable-output-escaping="yes"/>','category_ajax','ajaxloader');
               
                });

		function InsertInDb(){
		        
			document.getElementById('process_ins').value ="db_ins";
			document.sheet_action.submit();
         		return true;
		}
            </script>
	    <script type="text/javascript">
	                <xsl:text disable-output-escaping="yes">
            <![CDATA[
		var _validFileExtensions = [".csv"];

		function ValidateTest(oForm) {
		    var arrInputs = oForm.getElementsByTagName("input");
		    for (var i = 0; i < arrInputs.length; i++) {
			var oInput = arrInputs[i];
			if (oInput.type == "file") {
			    var sFileName = oInput.value;
			    if (sFileName.length > 0) {
				var blnValid = false;
				for (var j = 0; j < _validFileExtensions.length; j++) {
				    var sCurExtension = _validFileExtensions[j];
				    if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
					blnValid = true;
					break;
				    }
				}

				if (!blnValid) {
				    alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
				    return false;
				}
			    }
			}
		    }

		    return true;
		}
		   ]]>
          </xsl:text>
		</script>

        </html>
    </xsl:template>
</xsl:stylesheet>
