<?xml version="1.0" ?>
<!DOCTYPE xsl:stylesheet  [
  <!ENTITY nbsp   "&#160;">
  <!ENTITY copy   "&#169;">
  <!ENTITY reg    "&#174;">
  <!ENTITY trade  "&#8482;">
  <!ENTITY mdash  "&#8212;">
  <!ENTITY ldquo  "&#8220;">
  <!ENTITY rdquo  "&#8221;">
  <!ENTITY pound  "&#163;">
  <!ENTITY yen    "&#165;">
  <!ENTITY euro   "&#8364;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html" version="4.0" encoding="UTF-8" indent="yes"/>
    <xsl:include href="inc_header.xsl" /><!-- include header-->
    <xsl:include href="inc_footer.xsl" /><!-- include footer-->
    <xsl:template match="/">
        <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title>:: Ajax - Admin Feature Group Management ::</title>
                <link rel="stylesheet" type="text/css" href="{/XML/CSS_URL}main.css" />
                <script LANGUAGE="JavaScript" SRC="{XML/ADMIN_JS_URL}common.js"></script>
                <script LANGUAGE="JavaScript" SRC="{XML/ADMIN_JS_URL}category.js"></script>
                <script LANGUAGE="JavaScript" SRC="{XML/ADMIN_JS_URL}feature_group.js"></script>
            </head>
            <body>
                <table align="center" width="100%" border="0" cellpadding="2" cellspacing="2">
                    <tr>
                        <td>
                            <div align="center">
                                <table width="100%" border="1" id="feature_group_dashboard">
                                    <tr>
                                        <td colspan="7">
                                            <div align="center">
                                                <h3>Dashboard</h3>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="7">
                                            <div align="right">pagination goes here</div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="15%">Sr no.</td>
                                        <td width="37%">Feature Group name</td>
                                        <td width="37%">Category Name</td>
                                        <td width="12%">Status</td>
                                        <td width="12%">Create Date</td>
                                        <td colspan="2">Action</td>
                                    </tr>
                                    <xsl:choose>
                                        <xsl:when test="/XML/FEATURE_GROUP_MASTER/COUNT&lt;=0">
                                            <tr>
                                                <td colspan="7">
                                                    <div align="center">Zero result found.</div>
                                                </td>
                                            </tr>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:for-each select="/XML/FEATURE_GROUP_MASTER/FEATURE_GROUP_MASTER_DATA">
                                                <tr>
                                                    <td>
                                                        <xsl:value-of select="position()" diseable-output-esacaping="yes"/>
                                                    </td>
                                                    <td>
                                                        <xsl:value-of select="MAIN_GROUP_NAME" diseable-output-esacaping="yes"/>
                                                    </td>
                                                    <td>
                                                        <xsl:value-of select="CATEGORY_NAME" diseable-output-esacaping="yes"/>
                                                    </td>
                                                    <td>
                                                        <xsl:value-of select="MAIN_GROUP_STATUS" diseable-output-esacaping="yes"/>
                                                    </td>
                                                    <td>
                                                        <xsl:value-of select="CREATE_DATE" diseable-output-esacaping="yes"/>
                                                    </td>
                                                    <td width="7%">
                                                        <a href="#Update" onclick="updateFeatureGroup('{GROUP_ID}','{JS_MAIN_GROUP_NAME}','{JS_OVERVIEW_DISPLAY_NAME}','{STATUS}');">Update</a>
                                                    </td>
                                                    <td width="17%">
                                                        <a href="javascript:undefined;" onclick="deleteFeatureGroup('{GROUP_ID}','{JS_MAIN_GROUP_NAME}');">Delete</a>
                                                    </td>
                                                </tr>
                                            </xsl:for-each>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                    <tr>
                                        <td colspan="7">
                                            <div align="right">pagination goes here</div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                        <!-- main area  END -->
                    </tr>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>
