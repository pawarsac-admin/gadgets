<?xml version="1.0" ?>
<!DOCTYPE xsl:stylesheet  [
  <!ENTITY nbsp   "&#160;">
  <!ENTITY copy   "&#169;">
  <!ENTITY reg    "&#174;">
  <!ENTITY trade  "&#8482;">
  <!ENTITY mdash  "&#8212;">
  <!ENTITY ldquo  "&#8220;">
  <!ENTITY rdquo  "&#8221;">
  <!ENTITY pound  "&#163;">
  <!ENTITY yen    "&#165;">
  <!ENTITY euro   "&#8364;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html" version="4.0" encoding="UTF-8" indent="yes"/>
    <xsl:include href="inc_header.xsl" /><!-- include header-->
    <xsl:include href="inc_footer.xsl" /><!-- include footer-->
    <xsl:template match="/">
        <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title>:: Ajax - Admin  Dashboard Management ::</title>
				<link rel="stylesheet" type="text/css" href="{/XML/CSS_URL}main.css" />
		        <script>
					var admin_web_url = '<xsl:value-of select="/XML/ADMIN_WEB_URL" disable-output-escaping="yes"/>';
				</script>
	        </head>
            <body>
	      
                <table align="center" width="100%" border="0" cellpadding="2" cellspacing="2">
                    <tr>
                        <td>
                            <div align="center" id="DivArticle">
			    <form enctype="multipart/form-data" method="post" action="{XML/ADMIN_WEB_URL}variant_value.php" name="product_manage_dashboard" id="product_manage_dashboard">
                                <table width="100%" border="1" id="product_dashboard">
                                    <tr>
                                        <td colspan="18">
                                            <div align="center">
                                                <h3>Dashboard</h3>
                                            </div>
                                        </td>
                                    </tr>
				     <tr>
                    <td colspan="18" align="right">
			<b>Search By City</b>
					<select name="city_id" id="city_id">
	                                        <option value="">---Select City---</option>
        	                                        <xsl:for-each select="/XML/CITY_MASTER/CITY_MASTER_DATA">
                	                                        <xsl:choose>
                        	                                        <xsl:when test="/XML/SELECTED_CITY_ID=CITY_ID">
                                	                                        <option value="{CITY_ID}" selected='yes'>
                                        	                                        <xsl:value-of select="CITY_NAME"/>
                                                                                </option>
                                                                        </xsl:when>
                                                                        <xsl:otherwise>
                                                 	                       <option value="{CITY_ID}">
                                                        	                       <xsl:value-of select="CITY_NAME"/>
                                                                               </option>
                                                                        </xsl:otherwise>
                                                                 </xsl:choose>
                                                          </xsl:for-each>
                                          </select>
                        <b>Search By:-</b>
                        <select name="select_brand" id="select_brand" onchange="getModelByBrandDashboard('','get_model_detail.php','Model','')">
                                <option value="">---All Brands---</option>
                                        <xsl:for-each select="/XML/BRAND_MASTER/BRAND_MASTER_DATA">
                                                <xsl:choose>
                                                        <xsl:when test="/XML/SELECTED_BRAND_ID=BRAND_ID">
                                                                <option value="{BRAND_ID}" selected='yes'>
                                                                        <xsl:value-of select="BRAND_NAME"/>
                                                                </option>
                                                        </xsl:when>
                                                        <xsl:otherwise>
                                                               <option value="{BRAND_ID}">
                                                                       <xsl:value-of select="BRAND_NAME"/>
                                                               </option>
                                                        </xsl:otherwise>
                                                 </xsl:choose>
                                          </xsl:for-each>
                          </select>
                          <select name="Model" id="Model" onchange="getVariantByBrandModelDashboard('','get_variants.php','Variant','')">
                                  <option value="">--All Models--</option>
                          </select>
                          <select name="Variant" id="Variant">
                                   <option value="">--All Variants--</option>
                          </select>
                          <input type="submit" value="Search" onclick="javascript:submitResearchForm();"/>
                    </td>
		  </tr>
                                    <tr>
                                        <td colspan="18">
                                            <div align="right"><xsl:value-of select="XML/Pages" disable-output-escaping="yes"/></div>
                                        </td>
                                    </tr>
                                    <tr>
					<td>Sr.No</td>
					<td>Brand Name</td>
					<td>Product Name</td>
					<td>City Name</td>
					<td>Price Variant </td>
					<td>Product Price</td>
					<td>Status</td>
					<td>create date</td>
					<td colspan="4">Action</td>
                                    </tr>
                                    <xsl:choose>
                                        <xsl:when test="/XML/VARIANT_VALUE_MASTER/COUNT&lt;=0">
                                            <tr>
                                                <td colspan="12">
                                                    <div align="center">Zero result found.</div>
                                                </td>
                                            </tr>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:for-each select="/XML/VARIANT_VALUE_MASTER/VARIANT_VALUE_MASTER_DATA">
												<xsl:sort select="PRODUCT_NAME"/>
                                                <tr>
                                                    <td>
                                                        <xsl:value-of select="position()" diseable-output-esacaping="yes"/>
                                                    </td>
						    <td>
                                                        <xsl:value-of select="BRAND_NAME" diseable-output-esacaping="yes"/>
                                                    </td>
						    <td>
                                                        <xsl:value-of select="PRODUCT_NAME" diseable-output-esacaping="yes"/>
                                                    </td>
													<td>
                                                        <xsl:value-of select="CITY_NAME" diseable-output-esacaping="yes"/>
                                                    </td>
                                                    <td>
                                                        <xsl:value-of select="VARIANT" diseable-output-esacaping="yes"/>
                                                    </td>
						     <td>
                                                        <xsl:value-of select="VARIANT_VALUE" diseable-output-esacaping="yes"/>
                                                    </td>
													<td>
                                                        <xsl:value-of select="STATUS" diseable-output-esacaping="yes"/>
                                                    </td>
													<td>
                                                        <xsl:value-of select="CREATE_DATE" diseable-output-esacaping="yes"/>
                                                    </td>
													<!--	
                                                    <td>Up</td>
                                                    <td colspan="2">Down</td>
													-->
                                                    <td>
                                                        <a href="#Update" id="updateMe" onclick="updateVariantValue('variant_dashboard','variantajaxloader','{PRICE_VARIANT}','{CATEGORY_ID}','{BRAND_ID}','{PRODUCT_ID}','{COUNTRY_ID}','{STATE_ID}
','{CITY_ID}','','');">Update</a>
                                                    </td>
                                                    <td>
                                                        <a href="javascript:undefined;" onclick="deleteVariantValue('{PRICE_VARIANT}','{PRODUCT_ID}');">Delete</a>
                                                    </td>
                                                </tr>
                                            </xsl:for-each>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                    <tr>
                                        <td colspan="18">
                                            <div align="right"><xsl:value-of select="XML/Pages" disable-output-escaping="yes"/></div>
                                        </td>
                                    </tr>
                                </table>
				</form>
                                <!--start code to add dealer form -->
                                <form enctype="multipart/form-data" method="post" action="{XML/ADMIN_WEB_URL}variant_value.php" name="variant_manage" id="variant_manage" onsubmit="return validatevariant_manageVariantValue();">
                                    <table width="100%" border="0" id="add_product_table">                                        
                                        <tr>
                                            <td align="right">                                                
                                                <table width="100%" border="0">
                                                    <tr>
                                                        <td>
                                                            <input type="hidden" name="startlimit" id="startlimit" value="{/XML/STARTLIMIT}"/>
                                                        </td>
                                                        <td>
                                                            <input type="hidden" name="cnt" id="cnt" value="{/XML/CNT}"/>
                                                        </td>
                                                        <td>
														<input type="hidden" name="product_id" id="product_id" value="{XML/VARIANT_VALUE_DATA/PRODUCT_ID}"/>
														<input type="hidden" name="price_variant" id="price_variant" value="{XML/VARIANT_VALUE_DATA/PRICE_VARIANT}"/>
                                                        </td>
                                                        <td>
                                                            <input type="hidden" name="actiontype" id="actiontype" value="{/XML/SELECTED_ACTION_TYPE}"/>
                                                        </td>
                                                        <td></td>
                                                        <td colspan="2"></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                </table>
                                                <table width="100%" id="Update" border="1">
							<tr>
								<td>Variant</td>
								<td colspan="10">
									<select name="variant_id" id="variant_id">
										<option value="">---Select Variant---</option>
										<xsl:for-each select="/XML/VARIANT_MASTER/VARIANT_MASTER_DATA">sasa
											<xsl:if test="/XML/VARIANT_VALUE_DATA/VARIANT_ID=VARIANT_ID">
											<option value="{VARIANT_ID}" selected='yes'>
											<xsl:value-of select="VARIANT"/>
											</option>
											</xsl:if>
											<xsl:if test="not(/XML/VARIANT_VALUE_DATA/VARIANT_ID=VARIANT_ID)">
											<option value="{VARIANT_ID}">
											<xsl:value-of select="VARIANT"/>
											</option>
											</xsl:if>
										</xsl:for-each>
									</select>
									<div id="ajaxloadercountry" style="display:none;">
										<div align="center">
											<img src="{/XML/IMAGE_URL}ajax-loader.gif"/>
										</div>
									</div>
								</td>
							</tr>
							<tr>
								<td>Product Price</td>
								<td colspan="10">
									<input type="text" name="variant_value" id="variant_value" size="40" value="{XML/VARIANT_VALUE_DATA/VARIANT_VALUE}"/>
								</td>
							</tr>

							<tr>
								<td>Country Name</td>
								<td colspan="10">
									<select name="select_country_id" id="select_country_id" onchange="getStateByCountry('ajaxloadercountry','0');">
										<option value="">---Select Country---</option>
										<xsl:for-each select="/XML/COUNTRY_MASTER/COUNTRY_MASTER_DATA">
											<xsl:if test="/XML/VARIANT_VALUE_DATA/COUNTRY_ID=COUNTRY_ID">
											<option value="{COUNTRY_ID}" selected='yes'>
											<xsl:value-of select="COUNTRY_NAME"/>
											</option>
											</xsl:if>
											<xsl:if test="not(/XML/VARIANT_VALUE_DATA/COUNTRY_ID=COUNTRY_ID)">
											<option value="{COUNTRY_ID}">
											<xsl:value-of select="COUNTRY_NAME"/>
											</option>
											</xsl:if>
										</xsl:for-each>
									</select>
									<div id="ajaxloadercountry" style="display:none;">
										<div align="center">
											<img src="{/XML/IMAGE_URL}ajax-loader.gif"/>
										</div>
									</div>
								</td>
							</tr>
							<tr>
								<td>Brand Name</td>
								<td colspan="10">
									<select name="select_brand_id" id="select_brand_id" onchange="getProductByBrand('ajaxloaderbrand','0');">
										<option value="">---Select Brand---</option>
										<xsl:for-each select="/XML/BRAND_MASTER/BRAND_MASTER_DATA">
											<xsl:if test="/XML/VARIANT_VALUE_DATA/BRAND_ID=BRAND_ID">
												<option value="{BRAND_ID}" selected='yes'>
												<xsl:value-of select="BRAND_NAME"/>
												</option>
											</xsl:if>
											<xsl:if test="not(/XML/VARIANT_VALUE_DATA/BRAND_ID=BRAND_ID)">
												<option value="{BRAND_ID}">
												<xsl:value-of select="BRAND_NAME"/>
												</option>
											</xsl:if>
										</xsl:for-each>
									</select>
									<div id="ajaxloaderbrand" style="display:none;">
										<div align="center">
											<img src="{/XML/IMAGE_URL}ajax-loader.gif"/>
										</div>
									</div>
								</td>
							</tr>
                            <tr>
								<td>Set Default Price City#1</td>
								<td colspan="14">
								<select name="default_city" id="default_city">
								<xsl:if test="XML/VARIANT_VALUE_DATA/DEFAULT_CITY='1'">
								<option value="1" selected='yes'>Yes</option>
								<option value="0">No</option>
								</xsl:if>
								<xsl:if test="XML/VARIANT_VALUE_DATA/DEFAULT_CITY='0'">
								<option value="1" >Yes</option>
								<option value="0" selected='yes'>No</option>
								</xsl:if>
								<xsl:if test="not(XML/VARIANT_VALUE_DATA/DEFAULT_CITY)">
								<option value="1" >Yes</option>
								<option value="0" selected='yes'>No</option>
								</xsl:if>
								</select>
								</td>
							</tr>
							<tr>
								<td>Status#1</td>
								<td colspan="14">
								<select name="status" id="status">
								<xsl:choose>
									<xsl:when test="XML/VARIANT_VALUE_DATA/STATUS='Active'">
										<option value="1" selected='yes'>Active</option>
									</xsl:when>
									<xsl:otherwise>
										<option value="1">Active</option>
									</xsl:otherwise>
								</xsl:choose>
								<xsl:choose>
									<xsl:when test="XML/VARIANT_VALUE_DATA/STATUS='InActive'">
										<option value="0" selected='yes'>InActive</option>
									</xsl:when>
									<xsl:otherwise>
										<option value="0">InActive</option>
									</xsl:otherwise>
								</xsl:choose>
								
								</select>
								</td>
							</tr>
							<tr>						
								<td colspan="11"><input type="hidden" name="selected_category_id" id="selected_category_id" value="{/XML/SELECTED_CATEGORY_ID}"/></td>
							</tr>
							<tr>
								<td colspan="9">&nbsp;</td>
								<td>
								<div align="center">
								<input type="submit" name="save" value="Save" class="formbtn"/>
								</div>
								</td>
								<td>
								<div align="center">
								<input type="button" name="cancel" value="Cancel" class="formbtn" onclick="javascript:this.form.reset();"/>
								</div>
								</td>
						     </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </form>
                                <!--end code to add dealer form -->
                            </div>
                        </td>
                        <!-- main area  END -->
                    </tr>
                </table>
            </body>
	    </html>
    </xsl:template>
</xsl:stylesheet>
