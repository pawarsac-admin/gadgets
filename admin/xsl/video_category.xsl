<?xml version="1.0" ?>
<!DOCTYPE xsl:stylesheet  [
  <!ENTITY nbsp   "&#160;">
  <!ENTITY copy   "&#169;">
  <!ENTITY reg    "&#174;">
  <!ENTITY trade  "&#8482;">
  <!ENTITY mdash  "&#8212;">
  <!ENTITY ldquo  "&#8220;">
  <!ENTITY rdquo  "&#8221;">
  <!ENTITY pound  "&#163;">
  <!ENTITY yen    "&#165;">
  <!ENTITY euro   "&#8364;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html" version="4.0" encoding="UTF-8" indent="yes"/>
    <xsl:include href="../xsl/inc_header.xsl" /><!-- include header-->
    <xsl:include href="../xsl/inc_footer.xsl" /><!-- include footer-->
    <xsl:include href="../xsl/inc_leftnavigation.xsl" /><!-- include left navigation-->
    <xsl:include href="../xsl/inc_rightnavigation.xsl" /><!-- include right navigation-->
    <xsl:template match="/">
        <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title>: Admin Add Video Category Management</title>
                <link rel="stylesheet" type="text/css" href="{XML/WEB_URL}css/main.css" />
				
			<script language="javascript">
			<![CDATA[
				function isBlank(s){
					s = removeSpace(s);
					var len=s.length;
					var cnt;
					if(s.length==0){return true;}
					return false;
				}
				function removeSpace(s){
					return s.replace(/(^\s*)|(\s*$)/g, "");
				}
			]]>
			</script>

            </head>
            <body>
			<style>
				#myPopUp{
					position:absolute;top:668px;left:155px;width:970px;height:550px;background:#ccc;overflow-x:hidden;overflow-y:auto;
					display:none;
				}
			</style>
			<div id="myPopUp">
				here data will appear
			</div>
                <table align="center" width="100%" border="0" cellpadding="2" cellspacing="2">
                    <!-- call Header -->
                    <tr>
                        <td colspan="3">
                            <xsl:call-template name="incHeader"/>
                        </td>
                    </tr>
                    <tr>
                        <!-- call Left Navigation -->
                        <td width="10%" valign="top">
                            <xsl:call-template name="incLeftNavigation"/>
                        </td>
                        <!-- main area -->
                        <td>
                            <div align="center">
                                <table width="100%" border="0">
                                    <tr>
                                        <td>
                                            <h2> Video Category Management</h2>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <a href="{XML/ADMIN_WEB_URL}index.php">Main</a>> Add Video Category 
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <table width="100%" border="0">
                                <tr>
                                    <td colspan="6" width="100%">
                                        <h3>Add a new Video Category</h3>
                                    </td>
                                </tr>
                                <xsl:if test="/XML/MSG!=''">
                                    <tr>
                                        <td colspan="6" bgcolor="#98AFC7">
                                            <div align="center">
                                                <xsl:value-of select="/XML/MSG" diseable-output-esacaping="yes"/>
                                            </div>
                                        </td>
                                    </tr>
                                </xsl:if>
                            </table>
                            <!--start product ajax data place-->
                            <table>
                                <tr>
                                    <td>
                                        <div id="ajaxloader" style="display:none;">
                                            <div align="center">
                                                <img src="{/XML/IMAGE_URL}ajax-loader.gif"/>
                                            </div>
                                        </div>
                                        <div id="category_ajax" style="display:block;"></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div align="right">
                                            <input type="button" name="select_box" value="Select" id="select_box_id" class="formbtn" onclick="javascript: getVideoCategoryDashboard('prod_article_dashboard','productajaxloader','','{/XML/STARTLIMIT}','{/XML/CNT}');"/>

                                        </div>
                                    </td>
                                </tr>
                            </table>
                            <div id="productajaxloader" style="display:none;">
                                <div align="center">
                                    <img src="{/XML/IMAGE_URL}ajax-loader.gif"/>
                                </div>
                            </div>
                            <div id="prod_article_dashboard" style="display:block;">
                                        	Please select category for video category information.
                            </div>
                            <!--end product ajax data place-->
                        </td>
                        <!-- main area  END -->
                        <!-- call Right Navigation -->
                        <td width="15%"  valign="top">
                            <xsl:call-template name="incRightNavigation"/>
                        </td>
                    </tr>
                    <!-- call footer -->
                    <tr>
                        <td colspan="3">
                            <xsl:call-template name="incFooter"/>
                        </td>
                    </tr>
                </table>
            </body>
	    <script LANGUAGE="JavaScript" SRC="{XML/ADMIN_JS_URL}common.js"></script>
            <script LANGUAGE="JavaScript" SRC="{XML/ADMIN_JS_URL}category.js"></script>
            <script language="javascript" src="{XML/ADMIN_JS_URL}video_category.js"></script>
            <script language="javascript" src="{XML/ADMIN_JS_URL}tiny_mce/tiny_mce.js"></script>
	   
          
            <script>
            $(document).ready(function() {
                category_details('<xsl:value-of select="/XML/SELECTED_CATEGORY_ID" diseable-output-escaping="yes"/>','category_ajax','ajaxloader');
                <xsl:if test="/XML/SELECTED_CATEGORY_ID!=''">
                        getVideoCategoryDashboard('prod_article_dashboard','productajaxloader','<xsl:value-of select="/XML/SELECTED_CATEGORY_ID" diseable-output-escaping="yes"/>','<xsl:value-of select="/XML/STARTLIMIT" diseable-output-escaping="yes"/>','<xsl:value-of select="/XML/CNT" diseable-output-escaping="yes"/>');
                </xsl:if>
            });
				$("#updateMe").click(function(){
					alert("ok,ok,ok");
				});
            </script>
        </html>
    </xsl:template>
</xsl:stylesheet>
