<?xml version="1.0" ?>
<!DOCTYPE xsl:stylesheet  [
  <!ENTITY nbsp   "&#160;">
  <!ENTITY copy   "&#169;">
  <!ENTITY reg    "&#174;">
  <!ENTITY trade  "&#8482;">
  <!ENTITY mdash  "&#8212;">
  <!ENTITY ldquo  "&#8220;">
  <!ENTITY rdquo  "&#8221;">
  <!ENTITY pound  "&#163;">
  <!ENTITY yen    "&#165;">
  <!ENTITY euro   "&#8364;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html" version="4.0" encoding="UTF-8" indent="yes"/>
    <xsl:include href="../xsl/inc_header.xsl" /><!-- include header-->
    <xsl:include href="../xsl/inc_footer.xsl" /><!-- include footer-->
    <xsl:include href="../xsl/inc_leftnavigation.xsl" /><!-- include left navigation-->
    <xsl:include href="../xsl/inc_rightnavigation.xsl" /><!-- include right navigation-->
    <xsl:template match="/">
        <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title>Mobile : Admin Ask Expert Question Management</title>
                <link rel="stylesheet" type="text/css" href="{XML/WEB_URL}css/main.css" />
            </head>
            <body>
                <table align="center" width="100%" border="0" cellpadding="2" cellspacing="2">
                    <!-- call Header -->
                    <tr>
                        <td colspan="3">
                            <xsl:call-template name="incHeader"/>
                        </td>
                    </tr>
                    <tr>
                        <!-- call Left Navigation -->
                        <td width="10%" valign="top">
                            <xsl:call-template name="incLeftNavigation"/>
                        </td>
                        <!-- main area -->
                        <td>
                            <div align="center">
                                <table width="100%" border="0">
                                    <tr>
                                        <td>
                                            <h2>Ask Expert Question Management</h2>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <a href="{XML/ADMIN_WEB_URL}index.php">Main</a>
>  Question Dashboard</td>
                                    </tr>
                                </table>
                            </div>
                            <table width="100%" border="1" id="feature_unit_dashboard">
                                <tr>
                                    <td colspan="6">
                                        <div align="right">pagination goes here</div>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15%">Sr no.</td>
                                    <td width="37%">Name</td>
                                    <td width="12%">Email Id</td>
                                    <td width="12%">Question</td>
                                    <td width="12%">Create Date</td>
                                    <td colspan="2">Action</td>
                                </tr>
                                <xsl:choose>
                                    <xsl:when test="/XML/QUESTION_MASTER/COUNT&lt;=0">
                                        <tr>
                                            <td colspan="6">
                                                <div align="center">Zero Result Found.</div>
                                            </td>
                                        </tr>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:for-each select="/XML/QUESTION_MASTER/QUESTION_MASTER_DATA">
                                            <tr>
                                                <td>
                                                    <xsl:value-of select="position()" diseable-output-esacaping="yes"/>
                                                </td>
                                                <td>
                                                    <xsl:value-of select="NAME" diseable-output-esacaping="yes"/>
                                                </td>
                                                <td>
                                                    <xsl:value-of select="EMAIL_ID" diseable-output-esacaping="yes"/>
                                                </td>
                                                <td>
                                                    <xsl:value-of select="QUESTION" diseable-output-esacaping="yes"/>
                                                </td>
                                                <td>
                                                    <xsl:value-of select="CREATE_DATE" diseable-output-esacaping="yes"/>
                                                </td>
                                                <td width="7%">
                                                    <a href="{SEND_MAIL_TO}">Send Mail</a>
                                                </td>
                                            </tr>
                                        </xsl:for-each>
                                    </xsl:otherwise>
                                </xsl:choose>
                                <tr>
                                    <td colspan="6">
                                        <div align="right">pagination goes here</div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <!-- main area  END -->
                        <!-- call Right Navigation -->
                        <td width="15%"  valign="top">
                            <xsl:call-template name="incRightNavigation"/>
                        </td>
                    </tr>
                    <!-- call footer -->
                    <tr>
                        <td colspan="3">
                            <xsl:call-template name="incFooter"/>
                        </td>
                    </tr>
                </table>
            </body>
            <!--script LANGUAGE="JavaScript" SRC="{XML/ADMIN_JS_URL}city.js"></script-->
        </html>
    </xsl:template>
</xsl:stylesheet>
