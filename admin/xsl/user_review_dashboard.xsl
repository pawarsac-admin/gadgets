<?xml version="1.0" ?>
<!DOCTYPE xsl:stylesheet  [
  <!ENTITY nbsp   "&#160;">
  <!ENTITY copy   "&#169;">
  <!ENTITY reg    "&#174;">
  <!ENTITY trade  "&#8482;">
  <!ENTITY mdash  "&#8212;">
  <!ENTITY ldquo  "&#8220;">
  <!ENTITY rdquo  "&#8221;">
  <!ENTITY pound  "&#163;">
  <!ENTITY yen    "&#165;">
  <!ENTITY euro   "&#8364;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:output method="html" version="4.0" encoding="UTF-8" indent="yes"/>
  <xsl:include href="inc_header.xsl" /><!-- include header-->
  <xsl:include href="inc_footer.xsl" /><!-- include footer-->
  <xsl:template match="/">
	<table width="100%" border="1" id="product_dashboard">
                  <tr>
                    <td colspan="18">
                      <div align="center">
                        <h3>Dashboard</h3>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td colspan="18" align='right'>Pagination goes here</td>
                  </tr>
                  <tr>
                    <td>Sr.No</td>
		    <td>User Name</td>
                    <td>Email</td>
                    <td>Location</td>
                    <td>Brand</td>                    
                    <td>Model</td>                    
                    <td>Variant</td>                    
                    <td>Status</td>
                    <td>create date</td>
                    <td>Action</td>
                  </tr>
		<xsl:choose>
			<xsl:when test="/XML/USER_REVIEW_MASTER/COUNT&lt;=0">
			<tr>
                      	       <td colspan="12"><div align="center">Zero result found.</div></td>
			</tr>
			</xsl:when>
                	<xsl:otherwise>
				<xsl:for-each select="/XML/USER_REVIEW_MASTER/USER_REVIEW_MASTER_DATA">
				<tr>
					<td><xsl:value-of select="position()" diseable-output-esacaping="yes"/></td>
                			<td><xsl:value-of select="USER_NAME" diseable-output-esacaping="yes"/></td>
			                <td><xsl:value-of select="EMAIL" diseable-output-esacaping="yes"/></td>
        	        		<td><xsl:value-of select="LOCATION" diseable-output-esacaping="yes"/></td>
			                <td><xsl:value-of select="BRAND_NAME" diseable-output-esacaping="yes"/></td>
                			<td><xsl:value-of select="MODEL_NAME" diseable-output-esacaping="yes"/></td>
		                	<td><xsl:value-of select="VARIANT" diseable-output-esacaping="yes"/></td>
			                <td><xsl:value-of select="STATUS" diseable-output-esacaping="yes"/></td>
			                <td><xsl:value-of select="CREATE_DATE" diseable-output-esacaping="yes"/></td>
					<td><a href="#Update" id="Show Details" onclick="updateReview('{USER_REVIEW_ID}','{PRODUCT_INFO_ID}','{PRODUCT_ID}','{BRAND_ID}','{CATEGORY_ID}','','');">Update</a></td>
				</tr>
				</xsl:for-each>
        	        </xsl:otherwise>
                </xsl:choose>	
		<tr>
                <td colspan="18" align='right'>Pagination goes here.</td>
                 </tr>
                </table>
    </xsl:template>
</xsl:stylesheet>
