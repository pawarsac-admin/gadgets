<?xml version="1.0" ?>
<!DOCTYPE xsl:stylesheet  [
  <!ENTITY nbsp   "&#160;">
  <!ENTITY copy   "&#169;">
  <!ENTITY reg    "&#174;">
  <!ENTITY trade  "&#8482;">
  <!ENTITY mdash  "&#8212;">
  <!ENTITY ldquo  "&#8220;">
  <!ENTITY rdquo  "&#8221;">
  <!ENTITY pound  "&#163;">
  <!ENTITY yen    "&#165;">
  <!ENTITY euro   "&#8364;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html" version="4.0" encoding="UTF-8" indent="yes"/>
    <xsl:include href="../xsl/inc_header.xsl" /><!-- include header-->
    <xsl:include href="../xsl/inc_footer.xsl" /><!-- include footer-->
    <xsl:include href="../xsl/inc_leftnavigation.xsl" /><!-- include left navigation-->
    <xsl:include href="../xsl/inc_rightnavigation.xsl" /><!-- include right navigation-->
    <xsl:template match="/">
        <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
				<title>Mobile : Admin Brand Management</title>
                <link rel="stylesheet" type="text/css" href="{XML/ADMIN_CSS_URL}styles.css" />
				<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery.ui.all.css" />
				<script xmlns="" language="JavaScript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.3/jquery.min.js" type="text/javascript"></script>
	            <script>var admin_web_url = '<xsl:value-of select="/XML/ADMIN_WEB_URL" disable-output-escaping="yes"/>';</script>
				<script src="{/XML/ADMIN_WEB_URL}js/jquery.ui.core.js"></script>
				<script src="{/XML/ADMIN_WEB_URL}js/jquery.ui.widget.js"></script>
				<script src="{/XML/ADMIN_WEB_URL}js/jquery.ui.mouse.js"></script>
				<script src="{/XML/ADMIN_WEB_URL}js/jquery.ui.sortable.js"></script>

            </head>
            <body>
                <div id="container">
	<div id="header">
    	<h1><a href="/">established</a></h1>
        <h2>your website slogan here</h2>
    </div>
    
    <div id="navigation">
    	<ul>
        	<!--li><a href="index.html">Home</a></li>
            <li class="selected"><a href="examples.html">Examples</a></li>
            <li><a href="#">Products</a></li>
            <li><a href="#">Solutions</a></li>
            <li><a href="#">Logout</a></li-->
        </ul>
    </div>
    
    <div id="body">
    	
        <!-- <div class="sidebar sidebar-left">
            
        </div> -->
        
        <div id="content">
        	 <div align="center">
                                <table width="100%" border="0" cellspacing="0">
                                    <tr class="titlegray-bg">
                                        <td>
                                            <h2>Brand Management</h2>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <a href="{XML/ADMIN_WEB_URL}index.php">Main</a>
>  Brand Dashboard</td>
                                    </tr>
                                </table>
                            </div>
                            <table width="100%" border="0" cellspacing="0">
                                <tr class="titlegray-bg">
                                    <td colspan="6" width="100%">
                                        <h3>Add a new brand</h3>
                                    </td>
                                </tr>
                                <xsl:if test="/XML/MSG!=''">
                                    <tr>
                                        <td colspan="6" bgcolor="#98AFC7">
                                            <div align="center">
                                                <xsl:value-of select="/XML/MSG" diseable-output-esacaping="yes"/>
                                            </div>
                                        </td>
                                    </tr>
                                </xsl:if>
                            </table>
                                <table id="show_categorys" border="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <div id="ajaxloader" style="display:none;">
                                                <div align="center">
                                                    <img src="{/XML/ADMIN_IMAGE_URL}ajax-loader.gif"/>
                                                </div>
                                            </div>
                                            <div id="category_ajax" style="display:none;"></div>
                                            <div align="right">
                                                <input type="button" name="select_category" id="select_category" value="Select Category" onclick="javascript:getBrandDashboard('brand_dashboard','brandajaxloader','','{/XML/STARTLIMIT}','{/XML/CNT}');" class="formbutton"/>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <div id="brandajaxloader" style="display:none;">
                                    <div align="center">
                                        <img src="{/XML/ADMIN_IMAGE_URL}ajax-loader.gif"/>
                                    </div>
                                </div>
                                <div id="brand_dashboard" style="display:block;">
					Please select category for brand information.
                                </div>
			
        </div>
        
        <div class="sidebar sidebar-right">
            
        </div>
    
    	<div class="clear"></div>
    </div>

    <div id="footer">
    	<p>&copy; YourSite 2010. Design by <a href="http://www.spyka.net">Free CSS Templates</a> | <a href="http://www.justfreetemplates.com">Free Web Templates</a></p>
    </div>
</div>
            </body>
            <script LANGUAGE="JavaScript" SRC="{XML/ADMIN_JS_URL}common.js"></script>
            <script LANGUAGE="JavaScript" SRC="{XML/ADMIN_JS_URL}category.js"></script>
            <script LANGUAGE="JavaScript" SRC="{XML/ADMIN_JS_URL}brand.js"></script>
            <script>
                $(document).ready(function() {
                    category_details('<xsl:value-of select="/XML/SELECTED_CATEGORY_ID" diseable-output-escaping="yes"/>','category_ajax','ajaxloader');
                <xsl:if test="/XML/SELECTED_CATEGORY_ID!=''">
                    getBrandDashboard('brand_dashboard','brandajaxloader','<xsl:value-of select="/XML/SELECTED_CATEGORY_ID" diseable-output-escaping="yes"/>','<xsl:value-of select="/XML/STARTLIMIT" diseable-output-escaping="yes"/>','<xsl:value-of select="/XML/CNT" diseable-output-escaping="yes"/>');
                </xsl:if>
                });
            </script>

			<script>
				function LoadQuestion(){
					$(function() {
						$( "#sortable" ).sortable({
							placeholder: "ui-state-highlight",
							update : function () {
								//alert( $('input[type=hidden]').val() );
								var brands='';					
								$("#frmSort input[type='hidden']").each(function(){
									brands += "," + $(this).val() ;
								});

								//alert(brands);

								var order = $('#sortable').sortable('serialize');
								//alert(order);

								$("#info").load("/admin/brand_sort.php?action=rearrange&amp;brands="+brands);
								YellowFade("#info");
							}
						});
						$( "#sortable" ).disableSelection();
					});
					//alert("Done");
				}

				setTimeout(LoadQuestion,2000) ;

				function YellowFade(selector){
				   $(selector)
				  .css('opacity', 0)
				  .css('visibility','visible')
				  .animate({ backgroundColor: '#FCFC74', opacity: 1.0 }, 1000)
				  .animate({ backgroundColor: '#FCFC74', opacity: 1.0 }, 2000)
				  .animate({ backgroundColor: '#ffffff', opacity: 0.0}, 750 );
				}
			</script>

        </html>
    </xsl:template>
</xsl:stylesheet>
