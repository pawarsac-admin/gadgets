<?xml version="1.0" ?>
<!DOCTYPE xsl:stylesheet  [
  <!ENTITY nbsp   "&#160;">
  <!ENTITY copy   "&#169;">
  <!ENTITY reg    "&#174;">
  <!ENTITY trade  "&#8482;">
  <!ENTITY mdash  "&#8212;">
  <!ENTITY ldquo  "&#8220;">
  <!ENTITY rdquo  "&#8221;">
  <!ENTITY pound  "&#163;">
  <!ENTITY yen    "&#165;">
  <!ENTITY euro   "&#8364;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html" version="4.0" encoding="UTF-8" indent="yes"/>
    <xsl:include href="../xsl/inc_header.xsl" /><!-- include header-->
    <xsl:include href="../xsl/inc_footer.xsl" /><!-- include footer-->
    <xsl:include href="../xsl/inc_leftnavigation.xsl" /><!-- include left navigation-->
    <xsl:include href="../xsl/inc_rightnavigation.xsl" /><!-- include right navigation-->
    <xsl:template match="/">
        <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title>Mobile : Admin Car Finder Feature Overview Management</title>
                <link rel="stylesheet" type="text/css" href="{XML/WEB_URL}css/main.css" />
            </head>
            <body>
                <table align="center" width="100%" border="0" cellpadding="2" cellspacing="2">
                    <!-- call Header -->
                    <tr>
                        <td colspan="3">
                            <xsl:call-template name="incHeader"/>
                        </td>
                    </tr>
                    <tr>
                        <!-- call Left Navigation -->
                        <td width="10%" valign="top">
                            <xsl:call-template name="incLeftNavigation"/>
                        </td>
                        <!-- main area -->
                        <td>
                            <div align="center">
                                <table width="100%" border="0">
                                    <tr>
                                        <td>
                                            <h2>Car Finder Feature Overview Management</h2>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <a href="{XML/ADMIN_WEB_URL}index.php">Main</a>
>  Car Finder Feature Overview Dashboard</td>
                                    </tr>
                                </table>
                                <form action="{/XML/ADMIN_WEB_URL}car_finder_feature_overview.php" method="post" name="brand_action" id="brand_action" onsubmit="return validateCarFinderFeatureOverview();">
                                <table width="100%" id="show_categorys">
                                    <tr>
                                        <td>
                                            <div id="ajaxloader" style="display:none;">
                                                <div align="center">
                                                    <img src="{/XML/IMAGE_URL}ajax-loader.gif"/>
                                                </div>
                                            </div>
                                            <div id="category_ajax" style="display:none;"></div>
                                            <div align="right">
                                                <input type="button" name="select_category" id="select_category" value="Select Category" onclick="javascript:getCarFinderFeatureOverviewDashboard('feature_overview_dashboard','featureoverviewajaxloader','','{/XML/STARTLIMIT}','{/XML/CNT}');"/>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <div id="featureoverviewajaxloader" style="display:none;">
                                    <div align="center">
                                        <img src="{/XML/IMAGE_URL}ajax-loader.gif"/>
                                    </div>
                                </div>
                                <div id="feature_overview_dashboard" style="display:block;">
					              Please select category for Car Finder Feature Overview information.
                                </div>
                                </form>
                            </div>
                           
                            
                        </td>
                        <!-- main area  END -->
                        <!-- call Right Navigation -->
                        <td width="15%"  valign="top">
                            <xsl:call-template name="incRightNavigation"/>
                        </td>
                    </tr>
                    <!-- call footer -->
                    <tr>
                        <td colspan="3">
                            <xsl:call-template name="incFooter"/>
                        </td>
                    </tr>
                </table>
            </body>
            <script LANGUAGE="JavaScript" SRC="{XML/ADMIN_JS_URL}common.js"></script>
            <script LANGUAGE="JavaScript" SRC="{XML/ADMIN_JS_URL}category.js"></script>
            <script LANGUAGE="JavaScript" SRC="{XML/ADMIN_JS_URL}car_finder_feature_overview.js"></script>
            <script>
              var group_id = '<xsl:value-of select="/XML/SELECTED_MAIN_GROUP_ID" disable-output-escaping="yes"/>';
                $(document).ready(function() {                    
                    category_details('<xsl:value-of select="/XML/SELECTED_CATEGORY_ID" diseable-output-escaping="yes"/>','category_ajax','ajaxloader');
                <xsl:if test="/XML/SELECTED_CATEGORY_ID!=''">
                    getCarFinderFeatureOverviewDashboard('feature_overview_dashboard','featureoverviewajaxloader','<xsl:value-of select="/XML/SELECTED_CATEGORY_ID" diseable-output-escaping="yes"/>','<xsl:value-of select="/XML/STARTLIMIT" diseable-output-escaping="yes"/>','<xsl:value-of select="/XML/CNT" diseable-output-escaping="yes"/>');
                </xsl:if>
                });
            </script>
        </html>
    </xsl:template>
</xsl:stylesheet>
