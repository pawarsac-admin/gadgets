<?xml version="1.0" ?>
<!DOCTYPE xsl:stylesheet  [
  <!ENTITY nbsp   "&#160;">
  <!ENTITY copy   "&#169;">
  <!ENTITY reg    "&#174;">
  <!ENTITY trade  "&#8482;">
  <!ENTITY mdash  "&#8212;">
  <!ENTITY ldquo  "&#8220;">
  <!ENTITY rdquo  "&#8221;">
  <!ENTITY pound  "&#163;">
  <!ENTITY yen    "&#165;">
  <!ENTITY euro   "&#8364;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html" version="4.0" encoding="UTF-8" indent="yes"/>
    <xsl:include href="inc_header.xsl" /><!-- include header-->
    <xsl:include href="inc_footer.xsl" /><!-- include footer-->
    <xsl:template match="/">
        <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title>:: Ajax - Admin Brand Dashboard Management ::</title>
                <link rel="stylesheet" type="text/css" href="{/XML/ADMIN_CSS_URL}style.css" />
            </head>
            <body>
              <div id="sBrandDiv">
                <table align="center" width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <div align="center">

                                <table width="100%" border="0" id="brand_dashboard" cellspacing="0">
                                    <tr>
                                        <td colspan="10">
                                            <div align="center">
                                                <h3>Dashboard</h3>
                                            </div>
											<div id="info" class="info message bold"></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="10">
                                            <div align="right"><xsl:value-of select="XML/Pages" disable-output-escaping="yes"/></div>
                                        </td>
                                    </tr>
                                    <tr class="dashboard"><td>

										<div>

											<ul style="width:100%">
												<li>
													<span class="myspan w40 fbold tdleft">Sr no.</span>
													<span class="myspan w130 fbold">Brand name</span>  
													<span class="myspan w130 fbold">Category Name</span>  
													<!--span class="myspan w200 fbold">Description</span-->
													<span class="myspan w200 fbold">Logo</span> 
													<span class="myspan w130 fbold">Status</span> 
													<span class="myspan w130 fbold">Create Date</span> 
													<span class="myspan w200 fbold">Action</span> 
												</li>
											</ul>
										</div>
                                    </td></tr>
                                    <xsl:choose>
                                        <xsl:when test="/XML/BRAND_MASTER/COUNT&lt;=0">
                                            <tr class="dashboard">
                                                <td colspan="10">
                                                    <div align="center">Zero result found.</div>
                                                </td>
                                            </tr>
                                        </xsl:when>
                                        <xsl:otherwise>
										<tr><td>
										<div id="container"><form id="frmSort">
											<ul id="sortable">
											<xsl:for-each select="/XML/BRAND_MASTER/BRAND_MASTER_DATA">
												<!--xsl:sort select="BRAND_NAME"/-->
                                                <xsl:variable name="inactiverowtd"><xsl:if test="BRAND_STATUS='InActive'">inactive</xsl:if></xsl:variable>
												
												<li id="listItem_{position()}" >
													<input type="hidden" id="arrBrand_{BRAND_ID}" value="{BRAND_ID}" />
													<span class="myspan w40  tdleft">
														<xsl:value-of select="position()" diseable-output-esacaping="yes"/>
													</span>
													<span class="myspan w130 ">
														<xsl:value-of select="BRAND_NAME" diseable-output-esacaping="yes"/>
													</span>  
													<span class="myspan w130 ">
														<xsl:value-of select="CATEGORY_NAME" diseable-output-esacaping="yes"/>
													</span>  
													<!--span class="myspan w200 ">
														<xsl:value-of select="SHORT_DESC" diseable-output-esacaping="yes"/>
													</span-->
													<span class="myspan w200 ">
														<xsl:value-of select="BRAND_IMAGE" diseable-output-esacaping="yes"/>
													</span>
													<span class="myspan w130  {$inactiverowtd}">
														<xsl:value-of select="BRAND_STATUS" diseable-output-esacaping="yes"/>
													</span>
													<span class="myspan w130 ">
														<xsl:value-of select="CREATE_DATE" diseable-output-esacaping="yes"/>
													</span>
													<span class="myspan w200 ">
														<a href="#Update" onclick="updateBrand('brand_dashboard','brandajaxloader','{BRAND_ID}','{CATEGORY_ID}','{/XML/Page}','{/XML/Perpage}');">Update</a><span style="width:10px;">&#160;</span>
														<a href="javascript:undefined;" onclick="deleteBrand('{BRAND_ID}','{JS_BRAND_NAME}');">Delete</a>
													</span>
												</li>
                                            </xsl:for-each>
										</ul></form></div></td></tr>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                    <tr>
                                        <td colspan="10">
                                            <div align="right"><xsl:value-of select="XML/Pages" disable-output-escaping="yes"/></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="10">
                                            <div align="right"><a href="{/XML/EXCEL_DOWNLOAD_PATH}brand.xls">Download Excel Format</a></div>
                                        </td>
                                    </tr>
                                </table>
			<form><table width="100%" id="uploadsheet" class="dashboard_form">

            	<tr>
                    <td width="13%">Upload Excel Sheet:</td>
                    <td colspan="2">
                        <input type="file" name="brand_name_sheet" id="brand_name_sheet"/>
                    </td>
                    </tr>
                    <tr>
                    <td width="13%"></td>
                    <td colspan="2"><input type="submit" name="uploadsheet" value="Upload Excel Sheet" id="uploadsheet"/></td>
                </tr>
            </table></form>
				<form enctype="multipart/form-data" action="{/XML/ADMIN_WEB_URL}brand.php" method="post" name="brand_action" id="brand_action" onsubmit="return validateBrand();">
			        <table width="100%" id="Update" border="0" cellspacing="0" class="dashboard_form">
                    				<tr class="titlegray-bg">
                                       <td colspan="10"><div align="center"><b>OR</b></div></td>
                                    </tr>
                                    <tr>
                                        <td width="13%">Brand name</td>
                                        <td colspan="2">
                                            <input type="text" name="brand_name" id="brand_name" value="{XML/BRAND_DETAIL/BRAND_DETAIL_DATA/BRAND_NAME}"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Short Description</td>
                                        <td colspan="2">
						<textarea name="short_desc" id="short_desc" cols="60" rows="2" >
							<xsl:value-of select="XML/BRAND_DETAIL/BRAND_DETAIL_DATA/SHORT_DESC"/>
                                                </textarea>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Upload Car Finder Image:</td>
                                        <td colspan="2">
                                                <input name="uploadedfile" type="file"/><xsl:value-of select="XML/BRAND_DETAIL/BRAND_DETAIL_DATA/BRAND_IMAGE"/>
						<img src="{XML/BRAND_DETAIL/BRAND_DETAIL_DATA/BRAND_IMAGE_PATH}" width="50px" height="50px" />
                                        </td>
                                    </tr>
				    <tr>
                                        <td>Upload Research Image:</td>
                                        <td colspan="2">
                                                <input name="uploadedresearchimagefile" type="file" /><xsl:value-of select="XML/BRAND_DETAIL/BRAND_DETAIL_DATA/BRAND_RESEARCH_IMAGE"/><img src="{XML/BRAND_DETAIL/BRAND_DETAIL_DATA/BRAND_RESEARCH_IMAGE_PATH}" width="50px" height="50px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Status</td>
                                        <td colspan="2">
					    <select name="brand_status" id="brand_status">
			                            <xsl:choose>
                        			            <xsl:when test="XML/BRAND_DETAIL/BRAND_DETAIL_DATA/STATUS='Active'">
			                                            <option value="1" selected='yes'>Active</option>
                        			            </xsl:when>
			                                    <xsl:otherwise>
        	        		                            <option value="1">Active</option>
                	                		    </xsl:otherwise>
			                            </xsl:choose>
                        			    <xsl:choose>
                        			            <xsl:when test="XML/BRAND_DETAIL/BRAND_DETAIL_DATA/STATUS='InActive'">
			                                            <option value="0" selected='yes'>InActive</option>
                        			            </xsl:when>
			                                    <xsl:otherwise>
                        			                    <option value="0">InActive</option>
			                                    </xsl:otherwise>
                        			    </xsl:choose>
			                    </select>
                                        </td>
                                    </tr>
				    <tr>
                                        <td colspan="3">
                                            <input type="hidden" name="actiontype" id="actiontype" value="insert"/>
                                            <input type="hidden" name="startlimit" id="startlimit" value="{/XML/STARTLIMIT}"/>
                                            <input type="hidden" name="cnt" id="cnt" value="{/XML/CNT}"/>
                                            <input type="hidden" name="brand_id" id="brand_id" value=""/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" align="center">
                                            <input type="submit" name="brandsubmit" id="brandsubmit" value="Add/Update"/>
                                            <input type="button" name="cancel" id="cancel" value="Cancel" onclick="javascript:this.form.reset();"/>
                                        </td>
                                    </tr>
                                </table>
                            </form>
                            </div>
                        </td>
                        <!-- main area  END -->
                    </tr>
                </table>
		</div>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>
