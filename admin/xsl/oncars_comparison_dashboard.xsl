<?xml version="1.0" ?>
<!DOCTYPE xsl:stylesheet  [
  <!ENTITY nbsp   "&#160;">
  <!ENTITY copy   "&#169;">
  <!ENTITY reg    "&#174;">
  <!ENTITY trade  "&#8482;">
  <!ENTITY mdash  "&#8212;">
  <!ENTITY ldquo  "&#8220;">
  <!ENTITY rdquo  "&#8221;">
  <!ENTITY pound  "&#163;">
  <!ENTITY yen    "&#165;">
  <!ENTITY euro   "&#8364;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html" version="4.0" encoding="UTF-8" indent="yes"/>
    <xsl:include href="inc_header.xsl" /><!-- include header-->
    <xsl:include href="inc_footer.xsl" /><!-- include footer-->
    <xsl:template match="/">
        <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title>:: Ajax - Admin Oncars Comparison Dashboard Management ::</title>
		
                <link rel="stylesheet" type="text/css" href="{/XML/CSS_URL}main.css" />
		
                <script>
		        var admin_web_url = '<xsl:value-of select="/XML/ADMIN_WEB_URL" disable-output-escaping="yes"/>';
           </script>
	   
            </head>
            <body>
	     <div id="sMostpopularDiv">		
                <table align="center" width="100%" border="0" cellpadding="2" cellspacing="2">
					
                    <tr>
                        <td>
                            <div align="center" style="border: 1px solid #d5d5d5;">
                                <table width="100%" border="0" id="product_dashboard" colspacing="0" rowspacing="0">
                                    <tr style="border: 1px solid #d5d5d5;">
                                        <td colspan="18" style="border: 1px solid #d5d5d5;">
                                            <div align="center">
                                                <h3>Dashboard</h3>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr style="border: 1px solid #d5d5d5;">
                                        <td colspan="18" style="border: 1px solid #d5d5d5;">
                                            <div align="right"><xsl:value-of select="XML/Pages" disable-output-escaping="yes"/></div>
                                        </td>
                                    </tr>
                                    <tr class="row0">
                                        <td>Sr.No</td>
                                        <td>Category Name</td>
                                        <td>Title</td>
                                        <td>Description</td>
					<td>Compare set Names</td>
					<td>Position</td>
					<td>Status</td>
					<td>Create date</td>
					<td colspan="4">Action</td>
                                    </tr>
                                    <xsl:choose>
                                        <xsl:when test="/XML/ONCARS_COMPARE_SET_MASTER/COUNT&lt;=0">
                                            <tr>
                                                <td colspan="12">
                                                    <div align="center">Zero result found.</div>
                                                </td>
                                            </tr>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:for-each select="/XML/ONCARS_COMPARE_SET_MASTER/ONCARS_COMPARE_SET_MASTER_DATA">
												
                                                <tr class="row1">
                                                    <td>
                                                        <xsl:value-of select="position()" disable-output-escaping="yes"/>
                                                    </td>
                                                    <td>
                                                        <xsl:value-of select="CATEGORY_NAME" disable-output-esacaping="yes"/>
                                                    </td>
                                                    <td>
                                                        <xsl:value-of select="TITLE" disable-output-esacaping="yes"/>
                                                    </td>
                                                    <td>
                                                        <xsl:value-of select="DESCRIPTION" disable-output-esacaping="yes"/>
                                                    </td>
 						    <td>
                                                        <xsl:value-of select="PRODUCT_NAMES" disable-output-esacaping="yes"/>
                                                    </td>
 						    <td>
                                                        <xsl:value-of select="POSITION" disable-output-esacaping="yes"/>
                                                    </td>
						    <td>
                                                        <xsl:value-of select="STATUS" disable-output-esacaping="yes"/>
                                                    </td>
													<td>
                                                        <xsl:value-of select="CREATE_DATE" disable-output-esacaping="yes"/>
                                                    </td>
                                                    <td>
                                                        <a href="#Update" id="updateMe" onclick="updateOncarsComparison('prod_article_dashboard','productajaxloader','{ONCARS_COMPARE_ID}','{CATEGORY_ID}');">Update</a>
                                                    </td>
                                                    <td>
                                                        <a href="javascript:undefined;" onclick="deleteOncarsComparison('{ONCARS_COMPARE_ID}');">Delete</a>
                                                    </td>
                                                </tr>
                                            </xsl:for-each>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                    <tr style="border: 1px solid #d5d5d5;">
                                        <td colspan="18" style="border: 1px solid #d5d5d5;">
                                            <div align="right"><xsl:value-of select="XML/Pages" disable-output-escaping="yes"/></div>
                                        </td>
                                    </tr>
                                </table>
                                <!--start code to add product form -->
                                <form enctype="multipart/form-data" method="post" action="{XML/ADMIN_WEB_URL}oncars_comparison.php" name="product_manage" id="product_manage" onsubmit="return validateProduct();">

                                    <table width="100%" border="0" id="add_product_table">                                        
                                        <tr >
                                            <td align="right">                                                
                                                <table width="100%" border="0">
                                                    <tr class="row1">
                                                            <input type="hidden" name="startlimit" id="startlimit" value="{/XML/STARTLIMIT}"/>
                                                            <input type="hidden" name="cnt" id="cnt" value="{/XML/CNT}"/>
						            <input type="hidden" name="competitor_productids" id="competitor_productids" value="{XML/ONCARS_COMPARE_SET/ONCARS_COMPARE_SET_DATA/PRODUCT_IDS}"/>
                                                            <input type="hidden" name="actiontype" id="actiontype" value="{/XML/SELECTED_ACTION_TYPE}"/>
                                                            <input type="hidden" name="oncars_compare_id" id="oncars_compare_id" value="{/XML/ONCARS_COMPARE_SET/ONCARS_COMPARE_SET_DATA/ONCARS_COMPARE_ID}"/>
                                                    </tr>
                                                </table>
                                                <table width="100%" id="Update" border="0" class="row1" style="border:1px solid #d4d4d4;">
							<tbody id="slideshowtbody">
							<tr class="datarow1">
                                                        	<td>Title</td>
	                                                        <td colspan="10">
                                                                	<input type="text" name="title" id="title" length="150" size="80" value="{/XML/ONCARS_COMPARE_SET/ONCARS_COMPARE_SET_DATA/TITLE}"/>
                        	                                </td>
	                                                </tr>
							<tr class="datarow1">
                                                                <td>Description</td>
                                                                <td colspan="10">
									<textarea name="description" cols="60" rows="5" id="description">
                                                                		<xsl:value-of select="/XML/ONCARS_COMPARE_SET/ONCARS_COMPARE_SET_DATA/DESCRIPTION" disable-output-escaping="yes"/>
	                                                                </textarea>
                                                                </td>
                                                        </tr>
							<xsl:if test="(/XML/DISP_ROW_COUNT)=0">
							<tr class="datarow1">
                                                        <td> Product Name 1:</td>
                                                        <td>
                                                            <select name="select_comp_brand_id_1" id="select_comp_brand_id_1" onchange="getModelByBrand('ajaxloader1','0','1');">
                                                                <option value="">---Select Brand---</option>
									<xsl:for-each select="/XML/BRAND_MASTER/BRAND_MASTER_DATA">
										<xsl:if test="/XML/ONCARS_COMPARE_SET/ONCARS_COMPARE_SET_MODEL/BRAND_ID=BRAND_ID">
       			                                                         <option value="{BRAND_ID}" selected='yes'>
                        		                                            <xsl:value-of select="BRAND_NAME"/>
                                        		                        </option>
										</xsl:if>
										<xsl:if test="not(/XML/ONCARS_COMPARE_SET/ONCARS_COMPARE_SET_MODEL/BRAND_ID=BRAND_ID)">
                                                        		        <option value="{BRAND_ID}">
		                                                                    <xsl:value-of select="BRAND_NAME"/>
                		                                                </option>
										</xsl:if>
                                	                            </xsl:for-each>
                                                            </select>
							</td>
							<td>
                                        		<div id="select_model_id_1">
                                                	<select name="select_comp_model_id_1" id="select_comp_model_id_1">
                                                        	<option value="">---Select Model---</option>
	                                                </select>
        		                                </div>
							</td>
							<td colspan="8">
                        		                <div id="select_variant_id_1">
                                        	        <select name="select_comp_variant_id_1" id="select_comp_variant_id_1">
                                                	        <option value="">---Select variant---</option>
	                                                </select>
        		                                </div>
                                                	</td>
                                                </tr>
						</xsl:if>
						<xsl:if test="(/XML/DISP_ROW_COUNT)>0">	
						<xsl:for-each select="/XML/ONCARS_COMPARE_SET/ONCARS_COMPARE_SET_NAMES">
							<xsl:variable name="selected_brand_id"><xsl:value-of select="SELECTED_BRAND_ID"/></xsl:variable>
							<xsl:variable name="selected_model_id"><xsl:value-of select="SELECTED_MODEL_ID"/></xsl:variable>
							<xsl:variable name="selected_variant_id"><xsl:value-of select="SELECTED_VARIANT_ID"/></xsl:variable>
							<tr class="datarow1">
                                                        <td> Product Name <xsl:value-of select="position()"/>:</td>
                                                        <td>
                                                            <select name="select_comp_brand_id_{position()}" id="select_comp_brand_id_{position()}" onchange="getModelByBrand('ajaxloader1','0','{position()}');">
                                                                <option value="">---Select Brand---</option>
                                                                        <xsl:for-each select="ONCARS_BRAND_MASTER/ONCARS_BRAND_MASTER_DATA">
                                                                                <xsl:if test="$selected_brand_id=BRAND_ID">
                                                                                 <option value="{BRAND_ID}" selected='yes'>
                                                                                    <xsl:value-of select="BRAND_NAME"/>
                                                                                </option>
                                                                                </xsl:if>
                                                                                <xsl:if test="not($selected_brand_id=BRAND_ID)">
                                                                                <option value="{BRAND_ID}">
                                                                                    <xsl:value-of select="BRAND_NAME"/>
                                                                                </option>
                                                                                </xsl:if>
                                                                    </xsl:for-each>
                                                            </select>
                                                        </td>
                                                        <td>
                                                        <div id="select_model_id_{position()}">
                                                        <select name="select_comp_model_id_{position()}" id="select_comp_model_id_{position()}" onchange="getVariantByModelCompetitor('','0','{position()}');">
                                                                <option value="">---Select Model---</option>
								<xsl:for-each select="ONCARS_MODEL_MASTER/ONCARS_MODEL_MASTER_DATA">
                                                                                <xsl:if test="$selected_model_id=PRODUCT_NAME_ID">
                                                                                 <option value="{PRODUCT_NAME_ID}" selected='yes'>
                                                                                    <xsl:value-of select="PRODUCT_INFO_NAME"/>
                                                                                </option>
                                                                                </xsl:if>
                                                                                <xsl:if test="not($selected_model_id=PRODUCT_NAME_ID)">
                                                                                <option value="{PRODUCT_NAME_ID}">
                                                                                    <xsl:value-of select="PRODUCT_INFO_NAME"/>
                                                                                </option>
                                                                                </xsl:if>
                                                                    </xsl:for-each>
                                                        </select>
                                                        </div>
                                                        </td>
							<td colspan="8">
                                                        <div id="select_variant_id_{position()}">
                                                        <select name="select_comp_variant_id_{position()}" id="select_comp_variant_id_{position()}">
                                                                <option value="">---Select variant---</option>
								<xsl:for-each select="ONCARS_VARIANT_MASTER/ONCARS_VARIANT_MASTER_DATA">
                                                                                <xsl:if test="$selected_variant_id=PRODUCT_ID">
                                                                                 <option value="{PRODUCT_ID}" selected='yes'>
                                                                                    <xsl:value-of select="VARIANT"/>
                                                                                </option>
                                                                                </xsl:if>
                                                                                <xsl:if test="not($selected_variant_id=PRODUCT_ID)">
                                                                                <option value="{PRODUCT_ID}">
                                                                                    <xsl:value-of select="VARIANT"/>
                                                                                </option>
                                                                                </xsl:if>
                                                                    </xsl:for-each>
                                                        </select>
                                                        </div>
                                                        </td>
							</tr>
						</xsl:for-each>
						</xsl:if>
						<tr id="trAddRemove">
                                                        <td></td>
                                                        <td colspan="0" align="right">
                                                        <input id="add" value="Add More" onclick="addRemoveFileBrowseElements(1);" type="button" />&nbsp;
                                                        <input id="remove" value="Remove" onclick="addRemoveFileBrowseElements(0);" type="button" />
                                                        </td>
                                                </tr>
						<tr class="datarow1">
							<td>Upload Image:</td>
								<td colspan="10">
									<input name="compare_set_img_id" type="hidden"  id="compare_set_img_id" value="{XML/ONCARS_COMPARE_SET/ONCARS_COMPARE_SET_DATA/COMPARE_SET_IMG_ID}"/>
									<input name="img_upload" type="hidden"  id="img_upload" value="{XML/ONCARS_COMPARE_SET/ONCARS_COMPARE_SET_DATA/COMPARE_SET_IMAGE_PATH}"/>
									<input type="text" size="40" name="title_upload_file" id="title_upload_file" value="{XML/ONCARS_COMPARE_SET/ONCARS_COMPARE_SET_DATA/COMPARE_SET_IMAGE_PATH}" readonly="yes"/>
									<input type="hidden" name="content_type_1" id="content_type_1" value=""/>
									<input type="button" name="btn_get" id="btn_get" value="Image Upload" onclick="getUploadData('product_manage','title_upload_file','compare_set_img_id','img_upload','image','content_type_1');"/>
								</td>
                                         	    </tr>
						<tr class="datarow1">
                                                        <td>Position</td>
                                                        <td colspan="10" id="select_position">
                                                                <select name="position" id="position">
                                                                        <option value="">---Select Position---</option>
                                                                        <xsl:for-each select="/XML/ONCARS_COMPARE_SET_MASTER/COMPARISON_POSITION">
                                                                        <xsl:choose>
                                                                                <xsl:when test="/XML/ONCARS_COMPARE_SET/ONCARS_COMPARE_SET_DATA/POSITION=COMPARISON_POSITION_DATA">
                                                                                <option value="{COMPARISON_POSITION_DATA}" selected='yes'>
                                                                                        <xsl:value-of select="COMPARISON_POSITION_DATA"/>
                                                                                </option>
                                                                                </xsl:when>
                                                                                <xsl:otherwise>
                                                                                        <option value="{COMPARISON_POSITION_DATA}">
                                                                                                <xsl:value-of select="COMPARISON_POSITION_DATA"/>
                                                                                        </option>
                                                                                </xsl:otherwise>
                                                                        </xsl:choose>
                                                                        </xsl:for-each>
                                                                </select>
                                                                <div id="ajaxloaderType" style="display:none;">
                                                                <div align="center"><img src="{/XML/IMAGE_URL}ajax-loader.gif"/></div>
                                                                </div>
                                                        </td>
                                                </tr>
						<tr class="datarow1">
                                                        <td>Status</td>
                                                        <td colspan="14">
								<xsl:if test="/XML/WALLCNT!=0">
                                                                        <input name="display_rows" id="display_rows" value="{/XML/WALLCNT}" type="hidden" />
                                                                </xsl:if>
                                                                <xsl:if test="/XML/WALLCNT=0">
                                                                        <input name="display_rows" id="display_rows" value="1" type="hidden" />
                                                                </xsl:if>
								<select name="status" id="status">
									<xsl:if test="XML/ONCARS_COMPARE_SET/ONCARS_COMPARE_SET_DATA/STATUS='Active'">
										<option value="1" selected='yes'>Active</option>
										<option value="0">InActive</option>
									</xsl:if>
									<xsl:if test="XML/ONCARS_COMPARE_SET/ONCARS_COMPARE_SET_DATA/STATUS='InActive'">
										<option value="1" >Active</option>
										<option value="0" selected='yes'>InActive</option>
									</xsl:if>
									<xsl:if test="not(XML/ONCARS_COMPARE_SET/ONCARS_COMPARE_SET_DATA/STATUS)">
										<option value="1" selected='yes'>Active</option>
										<option value="0" >InActive</option>
									</xsl:if>
                                                            </select>
                                                        </td>
                                                    </tr>
						<tr>						
						<td colspan="11"><input type="hidden" name="selected_category_id" id="selected_category_id" value="{/XML/SELECTED_CATEGORY_ID}"/></td>
						</tr>
						<tr>
							<td colspan="9">&nbsp;</td>
                                                        <td>
                                                            <div align="center">
                                                                <input type="submit" name="save" value="Save" class="formbtn"/>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div align="center">
                                                                <input type="button" name="cancel" value="Cancel" class="formbtn" onclick="javascript:this.form.reset();"/>
                                                            </div>
                                                        </td>
                                                    </tr>
													</tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </form>
                                <!--end code to add product form -->
                            </div>
                        </td>
                        <!-- main area  END -->
                    </tr>
                </table>
	     </div>
            </body>
	    </html>
    </xsl:template>
</xsl:stylesheet>
