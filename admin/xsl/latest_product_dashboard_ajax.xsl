<?xml version="1.0" ?>
<!DOCTYPE xsl:stylesheet  [
  <!ENTITY nbsp   "&#160;">
  <!ENTITY copy   "&#169;">
  <!ENTITY reg    "&#174;">
  <!ENTITY trade  "&#8482;">
  <!ENTITY mdash  "&#8212;">
  <!ENTITY ldquo  "&#8220;">
  <!ENTITY rdquo  "&#8221;">
  <!ENTITY pound  "&#163;">
  <!ENTITY yen    "&#165;">
  <!ENTITY euro   "&#8364;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html" version="4.0" encoding="UTF-8" indent="yes"/>
    <xsl:include href="inc_header.xsl" /><!-- include header-->
    <xsl:include href="inc_footer.xsl" /><!-- include footer-->
    <xsl:template match="/">
        <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title>:: Ajax - Admin Product Dashboard Management ::</title>
		
                <link rel="stylesheet" type="text/css" href="{/XML/CSS_URL}main.css" />
		
                <script>
		        var admin_web_url = '<xsl:value-of select="/XML/ADMIN_WEB_URL" disable-output-escaping="yes"/>';
           </script>
	   
            </head>
            <body>
                <table align="center" width="100%" border="0" cellpadding="2" cellspacing="2">
                    <tr>
                        <td>
                            <div align="center">
                                <table width="100%" border="1" id="product_dashboard">
                                    <tr>
                                        <td colspan="18">
                                            <div align="center">
                                                <h3>Dashboard</h3>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="18">
                                            <div align="right">pagination goes here</div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Sr.No</td>
                                        <!--<td>Article name</td>-->
                                        <td>Category Name</td>
                                        <td>Brand Name</td>
					<td>Product Name</td>
					<td>Status</td>
                                        <td>create date</td>
					
					<td colspan="4">Action</td>
                                    </tr>
                                    <xsl:choose>
                                        <xsl:when test="/XML/PROUDCT_MASTER/COUNT&lt;=0">
                                            <tr>
                                                <td colspan="12">
                                                    <div align="center">Zero result found.</div>
                                                </td>
                                            </tr>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:for-each select="/XML/PRODUCT_MASTER/PRODUCT_MASTER_DATA">
                                                <tr>
                                                    <td>
                                                        <xsl:value-of select="position()" diseable-output-esacaping="yes"/>
                                                    </td>
                                                   <!-- <td>
                                                        <xsl:value-of select="TITLE" diseable-output-esacaping="yes"/>
                                                    </td>-->
						    
                                                    <td>
                                                        <xsl:value-of select="CATEGORY_NAME" diseable-output-esacaping="yes"/>
                                                    </td>
                                                    <td>
                                                        <xsl:value-of select="BRAND_NAME" diseable-output-esacaping="yes"/>
                                                    </td>
                                                    <td>
                                                        <xsl:value-of select="PRODUCT_NAME" diseable-output-esacaping="yes"/>
                                                    </td>
						    <td>
                                                        <xsl:value-of select="PRODUCT_STATUS" diseable-output-esacaping="yes"/>
                                                    </td>
						    <td>
                                                        <xsl:value-of select="CREATE_DATE" diseable-output-esacaping="yes"/>
                                                    </td>
						<!--	
                                                    <td>Up</td>
                                                    <td colspan="2">Down</td>
						    -->
                                                    <td>
                                                        <a href="#Update" id="updateMe" onclick="updateLatestProduct('prod_article_dashboard','productajaxloader','{LATEST_PRODUCT_ID}','{PRODUCT_ID}','{CATEGORY_ID}','{BRAND_ID}','','');">Update</a>
                                                    </td>
                                                    <td>
                                                        <a href="javascript:undefined;" onclick="deleteLatestProduct('{LATEST_PRODUCT_ID}');">Delete</a>
                                                    </td>
                                                </tr>
                                            </xsl:for-each>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                    <tr>
                                        <td colspan="18">
                                            <div align="right">pagination goes here</div>
                                        </td>
                                    </tr>
                                </table>
                                <!--start code to add product form -->
                                <form enctype="multipart/form-data" method="post" action="{XML/ADMIN_WEB_URL}latest_product.php" name="product_manage" id="product_manage" onsubmit="return validateProduct();">
                                    <table width="100%" border="0" id="add_product_table">                                        
                                        <tr>
                                            <td align="right">                                                
                                                <table width="100%" border="0">
                                                    <tr>
                                                        <td>
                                                            <input type="hidden" name="startlimit" id="startlimit" value="{/XML/STARTLIMIT}"/>
                                                        </td>
                                                        <td>
                                                            <input type="hidden" name="cnt" id="cnt" value="{/XML/CNT}"/>
                                                        </td>
                                                        <td>
                                                            <input type="hidden" name="product_id" id="product_id" value="{XML/PRODUCT_DATA/PRODUCT_ID}"/>
							    <input type="hidden" name="latest_product_id" id="latest_product_id" value="{XML/PRODUCT_DATA/LATEST_PRODUCT_ID}"/>
                                                        </td>
                                                        <td>
                                                            <input type="hidden" name="actiontype" id="actiontype" value="insert"/>
                                                        </td>
                                                        <td>
                                                            <input type="hidden" name="featureboxcnt" id="featureboxcnt" value="{/XML/FEATURE_MASTER/COUNT}"/>
                                                        </td>
                                                        <td></td>
                                                        <td colspan="2"></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                </table>
                                                <table width="100%" id="Update" border="1">
                                                    <tr>
                                                        <td>Brand Name</td>
                                                        <td colspan="10">
                                                            <select name="select_brand_id" id="select_brand_id" onchange="getProductByBrand('ajaxloader','0');">
                                                                <option value="">---Select Brand---</option>
                                                            <xsl:for-each select="/XML/BRAND_MASTER/BRAND_MASTER_DATA">
							     <xsl:if test="/XML/PRODUCT_DATA/BRAND_ID=BRAND_ID">
                                                                <option value="{BRAND_ID}" selected='yes'>
                                                                    <xsl:value-of select="BRAND_NAME"/>
                                                                </option>
							    </xsl:if>
							    <xsl:if test="not(/XML/PRODUCT_DATA/BRAND_ID=BRAND_ID)">
                                                                <option value="{BRAND_ID}">
                                                                    <xsl:value-of select="BRAND_NAME"/>
                                                                </option>
							    </xsl:if>
                                                            </xsl:for-each>
                                                            </select>
							    <div id="ajaxloader" style="display:none;">
								<div align="center">
									<img src="{/XML/IMAGE_URL}ajax-loader.gif"/>
								</div>
								</div>
                                                        </td>
                                                    </tr>
						    
						   
						    
						    
						   
						   
                                                    <tr>
                                                        <td>Latest Product Status#1</td>
                                                        <td colspan="14">
                                                            <select name="product_status" id="product_status">
																<xsl:choose>
																<xsl:when test="XML/PRODUCT_DATA/PRODUCT_STATUS='Active'">
																	<option value="1" selected='yes'>Active</option>
																</xsl:when>
																<xsl:otherwise>
																	<option value="1">Active</option>
																</xsl:otherwise>
																</xsl:choose>
																<xsl:choose>
																<xsl:when test="XML/PRODUCT_DATA/PRODUCT_STATUS='InActive'">
																	<option value="0" selected='yes'>InActive</option>
																</xsl:when>
																<xsl:otherwise>
																	<option value="0">InActive</option>
																</xsl:otherwise>
																</xsl:choose>

								
                                                            </select>
                                                        </td>
                                                    </tr>
						    <tr>						
                                                        <td colspan="11"><input type="hidden" name="selected_category_id" id="selected_category_id" value="{/XML/SELECTED_CATEGORY_ID}"/></td>
						     </tr>
						    <tr>
							<td colspan="9">&nbsp;</td>
                                                        <td>
                                                            <div align="center">
                                                                <input type="submit" name="save" value="Save" class="formbtn"/>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div align="center">
                                                                <input type="button" name="cancel" value="Cancel" class="formbtn" onclick="javascript:this.form.reset();"/>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </form>
                                <!--end code to add product form -->
                            </div>
                        </td>
                        <!-- main area  END -->
                    </tr>
                </table>
            </body>
	    
		
        </html>
    </xsl:template>
</xsl:stylesheet>
