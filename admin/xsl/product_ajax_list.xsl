<?xml version="1.0" ?>
<!DOCTYPE xsl:stylesheet  [
  <!ENTITY nbsp   "&#160;">
  <!ENTITY copy   "&#169;">
  <!ENTITY reg    "&#174;">
  <!ENTITY trade  "&#8482;">
  <!ENTITY mdash  "&#8212;">
  <!ENTITY ldquo  "&#8220;">
  <!ENTITY rdquo  "&#8221;">
  <!ENTITY pound  "&#163;">
  <!ENTITY yen    "&#165;">
  <!ENTITY euro   "&#8364;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:output method="html" version="4.0" encoding="UTF-8" indent="yes"/>
  <xsl:include href="inc_header.xsl" /><!-- include header-->
  <xsl:include href="inc_footer.xsl" /><!-- include footer-->
  <xsl:template match="/">
	<form enctype="multipart/form-data" method="post" action="{XML/ADMIN_WEB_URL}product.php" name="product_manage_dashboard" id="product_manage_dashboard">
	<table width="100%" border="1" id="product_dashboard">
                  <tr>
                    <td colspan="18">
                      <div align="center">
                        <h3>Dashboard</h3>
                      </div>
                    </td>
                  </tr>
		  <tr>
                    <td colspan="18" align="right">
                        <b>Search By:-</b>
                        <select name="select_brand" id="select_brand" onchange="getModelByBrandDashboard('','get_model_detail.php','Model','')">
                                <option value="">---All Brands---</option>
                                        <xsl:for-each select="/XML/BRAND_MASTER/BRAND_MASTER_DATA">
                                                <xsl:choose>
                                                        <xsl:when test="/XML/SELECTED_BRAND_ID=BRAND_ID">
                                                                <option value="{BRAND_ID}" selected='yes'>
                                                                        <xsl:value-of select="BRAND_NAME"/>
                                                                </option>
                                                        </xsl:when>
                                                        <xsl:otherwise>
                                                               <option value="{BRAND_ID}">
                                                                       <xsl:value-of select="BRAND_NAME"/>
                                                               </option>
                                                        </xsl:otherwise>
                                                 </xsl:choose>
                                          </xsl:for-each>
                          </select>
                          <select name="Model" id="Model" onchange="getVariantByBrandModelDashboard('','get_variants.php','Variant','')">
                                  <option value="">--All Models--</option>
                          </select>
                          <select name="Variant" id="Variant">
                                   <option value="">--All Variants--</option>
                          </select>
                          <input type="submit" value="Search" onclick="javascript:submitResearchForm();"/>
                    </td>
                  </tr>
                  <tr>
                    <td colspan="18">
                      <div align="right"><xsl:value-of select="XML/Pages" disable-output-escaping="yes"/></div>
                    </td>
                  </tr>
                  <tr>
                    <td>Sr.No</td>
					<td>Category Name</td>
                    <td>Brand Name</td>
                    <td>Product name</td>
                    <td>Variant</td>                    
                    <!--
                    <td>Product MRP</td>
                    <td>Upcoming Product</td>
                    <td>Latest Product</td>
                    -->
                    <td>Status</td>
                    <td>create date</td>
                    <!-- in next version
                    <td colspan="3">&nbsp;</td>
                    -->
                    <td colspan="4">Action</td>
                  </tr>
                  <xsl:choose>
                    <xsl:when test="/XML/PRODUCT_MASTER/COUNT&lt;=0">
                      <tr>
                        <td colspan="12">
                          <div align="center">Zero result found.</div>
                        </td>
                      </tr>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:for-each select="/XML/PRODUCT_MASTER/PRODUCT_MASTER_DATA">
                        <tr>
                          <td>
                            <xsl:value-of select="position()" diseable-output-esacaping="yes"/>
                          </td>
						  <td>
                            <xsl:value-of select="CATEGORY_NAME" diseable-output-esacaping="yes"/>
                          </td>
                          <td>
                            <xsl:value-of select="BRAND_NAME" diseable-output-esacaping="yes"/>
                          </td>
                          <td>
                            <xsl:value-of select="PRODUCT_NAME" diseable-output-esacaping="yes"/>
                          </td>
                          <td>
                            <xsl:value-of select="VARIANT" diseable-output-esacaping="yes"/>
                          </td>                          
<!--
                          <td>
                            <xsl:value-of select="PRODUCT_MRP" diseable-output-esacaping="yes"/>
                          </td>
                          <td>
                            <xsl:value-of select="IS_PRODUCT_UPCOMING" diseable-output-esacaping="yes"/>
                          </td>
                          <td>
                            <xsl:value-of select="IS_PRODUCT_LATEST" diseable-output-esacaping="yes"/>
                          </td>
                          -->
                          <td>
                            <xsl:value-of select="PRODUCT_STATUS" diseable-output-esacaping="yes"/>
                          </td>
                          <td>
                            <xsl:value-of select="CREATE_DATE" diseable-output-esacaping="yes"/>
                          </td>
                          <!-- in next version
                          <td>Up</td>
                          <td colspan="2">Down</td>
                          -->
                          <td>
                            <a href="#Update" id="updateMe" onclick="updateProduct('{PRODUCT_ID}','{BRAND_ID}','{CATEGORY_ID}','myPopUpAjaxloader','myPopUp');">Update</a>
                          </td>
                          <td>
                            <a href="javascript:undefined;" onclick="deleteProduct('{PRODUCT_ID}','{JS_PRODUCT_NAME}');">Delete</a>
                          </td>
                        </tr>
                      </xsl:for-each>
                    </xsl:otherwise>
                  </xsl:choose>
                  <tr>
                    <td colspan="18">
                      <div align="right"><xsl:value-of select="XML/Pages" disable-output-escaping="yes"/></div>
                    </td>
                  </tr>
		 <tr>
                  <td colspan="18">
                    <input type="hidden" name="selected_category_id" id="selected_category_id" value="{/XML/SELECTED_CATEGORY_ID}"/>
                  </td>
                 </tr>
                </table>
	</form>
    </xsl:template>
</xsl:stylesheet>
