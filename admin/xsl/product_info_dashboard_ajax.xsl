<?xml version="1.0" ?>
<!DOCTYPE xsl:stylesheet  [
  <!ENTITY nbsp   "&#160;">
  <!ENTITY copy   "&#169;">
  <!ENTITY reg    "&#174;">
  <!ENTITY trade  "&#8482;">
  <!ENTITY mdash  "&#8212;">
  <!ENTITY ldquo  "&#8220;">
  <!ENTITY rdquo  "&#8221;">
  <!ENTITY pound  "&#163;">
  <!ENTITY yen    "&#165;">
  <!ENTITY euro   "&#8364;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:output method="html" version="4.0" encoding="UTF-8" indent="yes"/>
  <xsl:include href="inc_header.xsl" /><!-- include header-->
  <xsl:include href="inc_footer.xsl" /><!-- include footer-->
  <xsl:template match="/">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>:: Ajax - Admin Product Dashboard Management ::</title>
        <link rel="stylesheet" type="text/css" href="{/XML/CSS_URL}main.css" />
        <script>
		        var admin_web_url = '<xsl:value-of select="/XML/ADMIN_WEB_URL" disable-output-escaping="yes"/>';
        </script>
      </head>
      <body>
        <table align="center" width="100%" border="0" cellpadding="2" cellspacing="2">
          <tr>
            <td>
              <div align="center">
                <table width="100%" border="1" id="product_dashboard">
                  <tr>
                    <td colspan="18">
                      <div align="center">
                        <h3>Dashboard</h3>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td colspan="18">
                      <div align="right">pagination goes here</div>
                    </td>
                  </tr>
                  <tr>
                    <td>Sr.No</td>
                    <td>Product name</td>
                    <td>Variant</td>
                    <td>Category Name</td>
                    <td>Brand Name</td>
                    <!--
                    <td>Product MRP</td>
                    <td>Upcoming Product</td>
                    <td>Latest Product</td>
                    -->
                    <td>Status</td>
                    <td>create date</td>
                    <!-- in next version
                    <td colspan="3">&nbsp;</td>
                    -->
                    <td colspan="4">Action</td>
                  </tr>
                  <xsl:choose>
                    <xsl:when test="/XML/PRODUCT_MASTER/COUNT&lt;=0">
                      <tr>
                        <td colspan="12">
                          <div align="center">Zero result found.</div>
                        </td>
                      </tr>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:for-each select="/XML/PRODUCT_MASTER/PRODUCT_MASTER_DATA">
                        <tr>
                          <td>
                            <xsl:value-of select="position()" diseable-output-esacaping="yes"/>
                          </td>
                          <td>
                            <xsl:value-of select="PRODUCT_NAME" diseable-output-esacaping="yes"/>
                          </td>
                          <td>
                            <xsl:value-of select="VARIANT" diseable-output-esacaping="yes"/>
                          </td>
                          <td>
                            <xsl:value-of select="CATEGORY_NAME" diseable-output-esacaping="yes"/>
                          </td>
                          <td>
                            <xsl:value-of select="BRAND_NAME" diseable-output-esacaping="yes"/>
                          </td>
<!--
                          <td>
                            <xsl:value-of select="PRODUCT_MRP" diseable-output-esacaping="yes"/>
                          </td>
                          <td>
                            <xsl:value-of select="IS_PRODUCT_UPCOMING" diseable-output-esacaping="yes"/>
                          </td>
                          <td>
                            <xsl:value-of select="IS_PRODUCT_LATEST" diseable-output-esacaping="yes"/>
                          </td>
                          -->
                          <td>
                            <xsl:value-of select="PRODUCT_STATUS" diseable-output-esacaping="yes"/>
                          </td>
                          <td>
                            <xsl:value-of select="CREATE_DATE" diseable-output-esacaping="yes"/>
                          </td>
                          <!-- in next version
                          <td>Up</td>
                          <td colspan="2">Down</td>
                          -->
                          <td>
                            <a href="#Update" id="updateMe" onclick="updateProduct('{PRODUCT_ID}','{BRAND_ID}','{CATEGORY_ID}','myPopUpAjaxloader','myPopUp');">Update</a>
                          </td>
                          <td>
                            <a href="javascript:undefined;" onclick="deleteProduct('{PRODUCT_ID}','{JS_PRODUCT_NAME}');">Delete</a>
                          </td>
                        </tr>
                      </xsl:for-each>
                    </xsl:otherwise>
                  </xsl:choose>
                  <tr>
                    <td colspan="18">
                      <div align="right">pagination goes here</div>
                    </td>
                  </tr>
                </table>
                <!--start code to add product form -->
                <form enctype="multipart/form-data" method="post" action="{XML/ADMIN_WEB_URL}product.php" name="product_manage" id="product_manage" onsubmit="return validateProduct();">
                  <table width="100%" border="0" id="add_product_table">
                    <tr>
                      <td align="right">
                        <table width="100%" border="0">
                          <tr>
                            <td>
                              <input type="hidden" name="startlimit" id="startlimit" value="{/XML/STARTLIMIT}"/>
                            </td>
                            <td>
                              <input type="hidden" name="cnt" id="cnt" value="{/XML/CNT}"/>
                            </td>
                            <td>
                              <input type="hidden" name="product_id" id="product_id"/>
                            </td>
                            <td>
                              <input type="hidden" name="actiontype" id="actiontype" value="insert"/>
                            </td>
                            <td>
                              <input type="hidden" name="featureboxcnt" id="featureboxcnt" value="{/XML/FEATURE_MASTER/COUNT}"/>
                            </td>
                            <td></td>
                            <td colspan="2"></td>
                            <td></td>
                            <td></td>
                          </tr>
                        </table>
                        <table width="100%" id="Update" border="1">
                          <tr>
                            <td>Brand Name</td>
                            <td colspan="10">
                              <select name="select_brand_id" id="select_brand_id">
                                <option value="">---Select Brand---</option>
                                <xsl:for-each select="/XML/BRAND_MASTER/BRAND_MASTER_DATA">
                                  <option value="{BRAND_ID}">
                                    <xsl:value-of select="BRAND_NAME"/>
                                  </option>
                                </xsl:for-each>
                              </select>
                            </td>
                          </tr>
                          <tr>
                            <td>Product Name</td>
                            <td colspan="10">
                              <input type="text" name="product_name" id="product_name" size="50"/>
                            </td>
                          </tr>
                          <tr>
                            <td>Varient</td>
                            <td colspan="10">
                              <input type="text" name="varient" id="varient" size="40"/>
                            </td>

                          </tr>
                          <tr>
                            <td>Product Description</td>
                            <td colspan="10">
                              <textarea name="product_description" id="product_description" cols="30"></textarea>
                            </td>
                          </tr>
                           
                             <!--needs to uncomment whenever the file system is up.
						 <tr>
                                                    <td>Upload Creative 1:</td>
                                                    <td colspan="10">
                                                          <input name="img_upload_1" type="file" size="40" id="img_upload_id_1"/>
                                                    </td>
                                                    </tr>
						    -->
			     <tr>
				<td>Upload Media:</td>
				<td colspan="10">
					<input name="media_id" type="hidden" size="40" id="media_id" value="{XML/ARTICLE_DATA/MEDIA_ID}"/>
					<input name="img_upload_1" type="hidden" size="40" id="img_upload_id_1" value="{XML/ARTICLE_DATA/VIDEO_PATH}"/>
					<input type="text" name="title_upload_file" id="title_upload_file" value="{XML/ARTICLE_DATA/VIDEO_PATH_TITLE}" readonly="yes"/>
					
					<input type="button" name="btn_get" id="btn_get" value="media upload" onclick="getUploadData('product_manage','title_upload_file','media_id','img_upload_id_1','image','main');"/>
					<input type="button" name="btn_search" id="btn_search" value="search" onclick="getUploadedDataList('product_manage','title_upload_file','media_id','img_upload_id_1','image','main');"/>	
				</td>
			    </tr>
			    <tr>
				<td>Upload Thumb:</td>
				<td colspan="10">
					<input name="img_media_id" type="hidden" size="40" id="img_media_id" value="{XML/ARTICLE_DATA/IMG_MEDIA_ID}"/>
					<input name="img_upload_thm" type="hidden" size="40" id="img_upload_id_thm" value="{XML/ARTICLE_DATA/IMAGE_PATH}"/>
					<input type="text" name="thumb_title" id="thumb_title" value="{XML/ARTICLE_DATA/IMAGE_PATH_TITLE}" readonly="yes"/> 	
					
					<input type="button" name="btn_get" id="btn_get" value="image upload" onclick="getUploadData('product_manage','thumb_title','img_media_id','img_upload_id_thm','image','thumb');"/>
					<input type="button" name="btn_search" id="btn_search" value="search" onclick="getUploadedDataList('product_manage','thumb_title','img_media_id','img_upload_id_thm','image','thumb');"/>			 
				</td>
			    </tr>
                            <tr>
                              <td>Product Status</td>
                              <td colspan="14">
                                <select name="product_status" id="product_status">
                                  <option value="1">Active</option>
                                  <option value="0">InActive</option>
                                </select>
                              </td>
                            </tr>
			    <input type="hidden" name="groupmastercnt" id="groupmastercnt" value="{count(/XML/GROUP_MASTER/GROUP_MASTER_DATA)}"/>
                            <xsl:for-each select="/XML/GROUP_MASTER/GROUP_MASTER_DATA">
				<xsl:variable name="groupmasterposition">
					<xsl:value-of select="position()"/>	
				</xsl:variable>
				<tr>
					<td colspan="11">
						<span style="color:#0000A0">
							<h3><xsl:value-of select="MAIN_GROUP_NAME" disable-output-escaping="yes"/></h3>
						</span>
					</td>
				</tr>
			    	<input type="hidden" name="subgroupmastercnt_{$groupmasterposition}" id="subgroupmastercnt_{$groupmasterposition}" value="{count(SUB_GROUP_MASTER)}"/>
				<xsl:for-each select="SUB_GROUP_MASTER">
					<xsl:variable name="subgroupmasterposition">
        	                                <xsl:value-of select="position()"/>
	                                </xsl:variable>
					<tr>
                                        	<td colspan="11">
                                                	<span style="color:#FF0000">
                                                        	<b><xsl:value-of select="SUB_GROUP_NAME" disable-output-escaping="yes"/></b>
                                                	</span>
                                        	</td>
                                	</tr>
			    		<input type="hidden" name="subgroupmaster_data_cnt_{$groupmasterposition}_{$subgroupmasterposition}" id="subgroupmaster_data_cnt_{$groupmasterposition}_{$subgroupmasterposition}" value="{count(SUB_GROUP_MASTER_DATA)}"/>
					<xsl:for-each select="SUB_GROUP_MASTER_DATA">
									<xsl:variable name="subgroupmaster_dataposition">
        	                                <xsl:value-of select="position()"/>
	                                </xsl:variable>
					<tr>
                                <xsl:choose>
                                  <!--start condition is used to check complusary product feature-->
                                  <xsl:when test="PIVOT_FEATURE_ID=FEATURE_ID">
                                    <xsl:choose>
                                      <xsl:when test="STATUS='1'">
                                        <!--start condition is used to check active product-->
                                        <td bgcolor="gray">
                                          <xsl:value-of select="FEATURE_NAME" disable-output-esacaping="yes"/>
                                          (<xsl:value-of select="FEATURE_STATUS" disable-output-esacaping="yes"/>)
                                        </td>
                                        <td colspan="10" bgcolor="gray">
<input type="text" name="feature_value_{$groupmasterposition}_{$subgroupmasterposition}_{$subgroupmaster_dataposition}" id="feature_value_id_{PIVOT_FEATURE_ID}" size="40" />
<input type="hidden" name="feature_id_{$groupmasterposition}_{$subgroupmasterposition}_{$subgroupmaster_dataposition}"  id="feature_id_{$groupmasterposition}_{$subgroupmasterposition}_{$subgroupmaster_dataposition}"  value="{FEATURE_ID}"/>
                                        </td>
                                      </xsl:when>
                                      <xsl:otherwise>
                                        <!--start condition is used to check inactive product-->
                                        <td bgcolor="#adadad">
                                          <xsl:value-of select="FEATURE_NAME" disable-output-esacaping="yes"/>
                                          (<xsl:value-of select="FEATURE_STATUS" disable-output-esacaping="yes"/>)
                                        </td>
                                        <td colspan="10" bgcolor="#adadad">
<input type="text" name="feature_value_{$groupmasterposition}_{$subgroupmasterposition}_{$subgroupmaster_dataposition}" id="feature_value_id_{PIVOT_FEATURE_ID}" size="40" />
<input type="hidden" name="feature_id_{$groupmasterposition}_{$subgroupmasterposition}_{$subgroupmaster_dataposition}"  id="feature_id_{$groupmasterposition}_{$subgroupmasterposition}_{$subgroupmaster_dataposition}"  value="{FEATURE_ID}"/>

                                        </td>
                                      </xsl:otherwise>
                                    </xsl:choose>
                                  </xsl:when>
                                  <!--start condition is used to check not complusary product feature-->
                                  <xsl:otherwise>
                                    <xsl:choose>
                                      <xsl:when test="STATUS='1'">
                                        <!--start condition is used to check active product-->
                                        <td bgcolor="gray">
                                          <xsl:value-of select="FEATURE_NAME" disable-output-esacaping="yes"/>
                                        </td>
                                        <td colspan="10" bgcolor="gray">
<input type="text" name="feature_value_{$groupmasterposition}_{$subgroupmasterposition}_{$subgroupmaster_dataposition}" id="feature_value_id_{PIVOT_FEATURE_ID}" size="40" />
<input type="hidden" name="feature_id_{$groupmasterposition}_{$subgroupmasterposition}_{$subgroupmaster_dataposition}"  id="feature_id_{$groupmasterposition}_{$subgroupmasterposition}_{$subgroupmaster_dataposition}"  value="{FEATURE_ID}"/>
                                        </td>
                                      </xsl:when>
                                      <xsl:otherwise>
                                        <!--start condition is used to check inactive product-->
                                        <td bgcolor="#adadad">
                                          <xsl:value-of select="FEATURE_NAME" disable-output-esacaping="yes"/>
(
                                          <xsl:value-of select="FEATURE_STATUS" disable-output-esacaping="yes"/>
)
                                        </td>
                                        <td colspan="10" bgcolor="#adadad">
<input type="text" name="feature_value_{$groupmasterposition}_{$subgroupmasterposition}_{$subgroupmaster_dataposition}" id="feature_value_id_{PIVOT_FEATURE_ID}" size="40" />
<input type="hidden" name="feature_id_{$groupmasterposition}_{$subgroupmasterposition}_{$subgroupmaster_dataposition}"  id="feature_id_{$groupmasterposition}_{$subgroupmasterposition}_{$subgroupmaster_dataposition}"  value="{FEATURE_ID}"/>
                                        </td>
                                      </xsl:otherwise>
                                    </xsl:choose>
                                  </xsl:otherwise>
                                </xsl:choose>
                              </tr>

					</xsl:for-each>
				</xsl:for-each>	
                                                          </xsl:for-each>
                            <tr>
                              <td colspan="11">
                                <input type="hidden" name="selected_category_id" id="selected_category_id" value="{/XML/SELECTED_CATEGORY_ID}"/>
                              </td>
                            </tr>
                            <tr>
                              <td colspan="9">&nbsp;</td>
                              <td>
                                <div align="center">
                                  <input type="submit" name="save" value="Save" class="formbtn"/>
                                </div>
                              </td>
                              <td>
                                <div align="center">
                                  <input type="button" name="cancel" value="Cancel" class="formbtn" onclick="javascript:this.form.reset();"/>
                                </div>
                              </td>
                            </tr>
                        </table>
                      </td>
                          </tr>
                  </table>
                </form>
                      <!--end code to add product form -->
              </div>
            </td>
                      <!-- main area  END -->
                    </tr>
        </table>
      </body>
	    








    </html>
  </xsl:template>
</xsl:stylesheet>
