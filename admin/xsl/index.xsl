<?xml version="1.0" ?>
<!DOCTYPE xsl:stylesheet  [
  <!ENTITY nbsp   "&#160;">
  <!ENTITY copy   "&#169;">
  <!ENTITY reg    "&#174;">
  <!ENTITY trade  "&#8482;">
  <!ENTITY mdash  "&#8212;">
  <!ENTITY ldquo  "&#8220;">
  <!ENTITY rdquo  "&#8221;">
  <!ENTITY pound  "&#163;">
  <!ENTITY yen    "&#165;">
  <!ENTITY euro   "&#8364;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:output method="html" version="4.0" encoding="UTF-8" indent="yes"/>
  <xsl:include href="../xsl/inc_header.xsl" /><!-- include header-->
  <xsl:include href="../xsl/inc_footer.xsl" /><!-- include footer-->
  <xsl:include href="../xsl/inc_leftnavigation.xsl" /><!-- include left navigation-->
  <xsl:include href="../xsl/inc_rightnavigation.xsl" /><!-- include right navigation-->
  <xsl:template match="/">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Mobile : Admin Add Feature Management</title>
        <link rel="stylesheet" type="text/css" href="{XML/WEB_URL}css/main.css" />
      </head>
      <body>
        <table align="center" width="100%" border="0" cellpadding="2" cellspacing="2">
          <!-- call Header -->
          <tr>
            <td colspan="3">
              <xsl:call-template name="incHeader"/>
            </td>
          </tr>
          <tr>
            <!-- call Left Navigation -->
            <td width="10%" valign="top">
              <xsl:call-template name="incLeftNavigation"/>
            </td>
            <!-- main area -->
            <td>
              <div align="center">
                <table width="100%" border="0">
                  <tr>
                    <td>
                      <h2>Administrator Management</h2>
                    </td>
                  </tr>
                  <tr>
                    <td align="left">
                      <a href="{XML/ADMIN_WEB_URL}index.php">Main</a>
> Administrator
                    </td>
                  </tr>
                </table>
              </div>
              <table width="100%" id="Update">
                <tr>
                  <td>
                    <ul>
                      <li>
                        <b>Category Management Module</b>
                      </li>
                      <ul>
                        <li>
                          <a href="{XML/ADMIN_WEB_URL}category.php">Category Management</a>
                        </li>
                      </ul>
                      <li>
                        <b>Brand Management Module</b>
                      </li>
                      <ul>
                        <li>
                          <a href="{XML/ADMIN_WEB_URL}brand.php">Brand Management</a>
                        </li>
                      </ul>
		      <!--ul>
                        <li>
                          <a href="{XML/ADMIN_WEB_URL}popular_brand.php">Popular Brand Management</a>
                        </li>
                      </ul-->
                      <li>
                        <b>Feature Management Module</b>
                      </li>
                      <ul>
                        <li>
                          <a href="{XML/ADMIN_WEB_URL}feature_unit.php">Feature Unit Management</a>
                        </li>
                        <li>
                          <a href="{XML/ADMIN_MAIN_URL}feature_group.php">Main Feature Group/Tab Management</a>
                        </li>
                        <li>
                          <a href="{XML/ADMIN_MAIN_URL}feature_sub_group.php">Feature Sub Group Management</a>
                        </li>
                        <li>
                          <a href="{XML/ADMIN_WEB_URL}feature.php">Feature Management</a>
                        </li>
                      </ul>
                      <!--li>
                        <b>Overview Management Module</b>
                      </li>
                      <ul>
                        <li>
                          <a href="{XML/ADMIN_MAIN_URL}feature_overview.php">Feature Overview Management</a>(<span style="color:red;">*Used to show overview Version Page and car finder page.</span>) 
                        </li>
                        <li>
                          <a href="{XML/ADMIN_MAIN_URL}compare_overview.php">Feature Compare Page Overview Management</a>(<span style="color:red;">*Used to show overview on Compare Page.</span>) 
                        </li>
			<li>
                          <a href="{XML/ADMIN_MAIN_URL}car_finder_feature_overview.php">Car finder Feature Overview Management</a>
                        </li>
			<li>
                          <a href="{XML/ADMIN_MAIN_URL}popular_feature_cars.php">Feature Based Popular Cars Management</a>
                        </li>
                      </ul-->
                      <li>
                        <b>Pivot Management Module</b>
                      </li>
                      <ul>
                        <li>
                          <a href="{XML/ADMIN_MAIN_URL}pivot_display_type.php">Pivot Display Management</a>
                        </li>
                        <li>
                          <a href="{XML/ADMIN_WEB_URL}pivot_sub_group.php">Pivot Sub Group Management</a>
                        </li>
                        <li>
                          <a href="{XML/ADMIN_MAIN_URL}pivot.php">Pivot Management</a>
                        </li>
                      </ul>
                      <!--li>
                        <b>Price Management Module</b>
                      </li>
                      <ul>
                        <li>
                          <a href="{XML/ADMIN_MAIN_URL}variant.php">Price Variant Management</a>
                        </li>
                        <li>
                          <a href="{XML/ADMIN_MAIN_URL}variant_formula.php">Price Variant Formula Management</a>
                        </li>
                      </ul-->
                      <li>
                        <b>Product Management Module</b>
                      </li>
                      <ul>
                        <li>
                          <a href="{XML/ADMIN_MAIN_URL}model.php">Product Model Management</a>
                        </li>
                        <li>
                          <a href="{XML/ADMIN_MAIN_URL}product.php">Product Management</a>
                        </li>
                        <li>
                          <a href="{XML/ADMIN_MAIN_URL}variant_value.php">Product Price Management</a>
                        </li>
			<!--li>
                          <a href="{XML/ADMIN_MAIN_URL}variant_percentage.php">Price Variant Percentage</a>
                        </li-->
                        <li>
                          <a href="{XML/ADMIN_MAIN_URL}latest_product.php">Latest Product Management</a>
                        </li>
                        <!--li>
                          <a href="{XML/ADMIN_MAIN_URL}upcoming_product.php">Upcoming Product Management</a>
                        </li-->
                        <li>
                          <a href="{XML/ADMIN_MAIN_URL}featured_product.php">Featured Product Management</a>
                        </li>
                      </ul>
                      <!--li>
                        <b>Article Management Module</b>
                      </li>
                      <ul>
                        <li>
                          <a href="{XML/ADMIN_MAIN_URL}article.php">Article Management</a>
                        </li>
                        <li>
                          <a href="{XML/ADMIN_MAIN_URL}related_article.php">Latest Article/Featured Article/Related Article Management</a>
                        </li>  
			<li>
                          <a href="{XML/ADMIN_MAIN_URL}article_category.php">Article Category Management</a>
                        </li>
                        <li>
                          <a href="{XML/ADMIN_MAIN_URL}article_sub_type.php">Article Sub Type Management</a>
                        </li>
                      </ul>
                      
		      <li>
                        <b>News Management Module</b>
                      </li>
                      <ul>
                        <li>
                          <a href="{XML/ADMIN_MAIN_URL}news.php">News Management</a>
                        </li>
                        <li>
                          <a href="{XML/ADMIN_MAIN_URL}related_news.php">Latest News/Featured News/Related News Management</a>
                        </li>
			<li>
                          <a href="{XML/ADMIN_MAIN_URL}related_cat_news.php">Category News Management</a>
                        </li>

                      </ul>

		      <li>
                        <b>Reviews Management Module</b>
                      </li>
                      <ul>
                        <li>
                          <a href="{XML/ADMIN_MAIN_URL}oncars_reviews.php">oncars Review Management</a>
                        </li>
                        <li>
                          <a href="{XML/ADMIN_MAIN_URL}related_reviews.php">Latest Reviews/Featured Reviews/Related Reviews Management</a>
                        </li>
                      </ul>
		       <li>
                        <b>Expert Reviews Management Module</b>
                      </li>
                      <ul>
                        <li>
                          <a href="{XML/ADMIN_MAIN_URL}expert_reviews.php">Expert Reviews Management</a>
                        </li>
                      </ul>
                      <li>
                        <b>Media Upload Management Module</b>
                      </li>
                      <ul>
                        <li>
                          <a href="{XML/ADMIN_MAIN_URL}upload_wallpaper.php">Wallpaper Management</a>
                        </li>
                      </ul>
                      <li>
                        <b>Top Competitor Management Module</b>
                      </li>
                      <ul>
                        <li>
                          <a href="{XML/ADMIN_MAIN_URL}top_competitor.php">Top Competitor Management</a>(<span style="color:red;">*Used to show on Version Page.</span>)
                        </li>
                        <li>
                          <a href="{XML/ADMIN_MAIN_URL}compare_top_competitor.php">Most popular Compare Management</a>(<span style="color:red;">*Used to show on Compare Page.</span>)
                        </li>
						 <li>
                          <a href="{XML/ADMIN_MAIN_URL}most_popular_set.php">Most popular Compare set Management</a>(<span style="color:red;">*Used to show on Compare Page.</span>)
                        </li>
                      </ul-->
                      <!--li>
                        <b>State City Management Module</b>
                      </li>
                      <ul>
                        <li>
                          <a href="{XML/ADMIN_MAIN_URL}state.php">State Management</a>
                        </li>
                        <li>
                          <a href="{XML/ADMIN_MAIN_URL}city.php">City Management</a>
                        </li>
                        <li>
                          <a href="{XML/ADMIN_MAIN_URL}related_city.php">Related City Management</a>
                        </li>
                      </ul>
		       
<li>
                        <b>Video Management Module</b>
                      </li>
                      <ul>
                        <li>
                          <a href="{XML/ADMIN_MAIN_URL}add_video.php">Video Management</a>
                        </li>
                       <li>
                          <a href="{XML/ADMIN_MAIN_URL}video.php">Related Video Management</a>
                        </li>
                       <li>
                          <a href="{XML/ADMIN_MAIN_URL}most_popular_video.php">Most Popular Video Management</a>
                        </li>
                       <li>
                          <a href="{XML/ADMIN_MAIN_URL}video_language.php">Language Video Management</a>
                        </li>
               <li>
                          <a href="{XML/ADMIN_MAIN_URL}video_category.php">Video Category Management</a>
                        </li>
                       <li>
                          <a href="{XML/ADMIN_MAIN_URL}video_type.php">Related Video Category Management</a>
                        </li>
            <li>
                          <a href="{XML/ADMIN_MAIN_URL}video_sub_type.php">Video Sub Category Management</a>
                        </li-->
            <!--<li>
                          <a href="{XML/ADMIN_MAIN_URL}home_videos.php">Home Video Management</a>
                        </li>-->
            <!--li>
                          <a href="{XML/ADMIN_MAIN_URL}video_landing.php">Pick Video Management</a>
                        </li>
            <li>
                          <a href="{XML/ADMIN_MAIN_URL}most_recent_videos.php">Most Recent Video Management</a>
                        </li>
           
                      </ul>





                     <li>
                        <b>Menu Management Module</b>
                      </li>
                      <ul>
                        <li>
                          <a href="{XML/ADMIN_MAIN_URL}header_menu.php">Header Menu Management</a>
                        </li>
			<li>
                          <a href="{XML/ADMIN_MAIN_URL}footer_menu.php">Footer Menu Management</a>
                        </li>
                      </ul> 
			<li>
                        <b>Slideshow Management Module</b>
                      </li>
                      <ul>
                        <li>
                          <a href="{XML/ADMIN_MAIN_URL}slideshow.php">Slideshow Management</a>
                        </li>
                        <li>
                          <a href="{XML/ADMIN_MAIN_URL}add_slides.php">Assign Slides Management</a>
                        </li>
                      </ul>
					  <li>
                        <b>Wallpaper Management Module</b>
                      </li>
                      <ul>
                        <li>
                          <a href="{XML/ADMIN_MAIN_URL}wallpaper.php">Wallpaper Management</a>
                        </li>
                        <li>
                          <a href="{XML/ADMIN_MAIN_URL}add_wallpaper.php">Assign Wallpaper Management</a>
                        </li>
                      </ul>
		 			  <li>
                        <b>Ask Expert Management Module</b>
                      </li>
                      <ul>
                        <li>
                          <a href="{XML/ADMIN_MAIN_URL}ask_expert.php">Ask Expert Management</a>
                        </li>
                      </ul>
					  <li>
                        <b>Rating &amp; Review Management Module</b>
                      </li>
                      <ul>
                        <li>
                          <a href="{XML/ADMIN_MAIN_URL}overall_rating.php">Overall Management</a>
                        </li>
                      </ul> -->
			<ul>
                        <li>
                          <a href="{XML/ADMIN_MAIN_URL}user_review.php">User Review Management</a>
                        </li>
			<li>
                          <a href="{XML/ADMIN_MAIN_URL}expert_rating.php">Expert Rating Management</a>
                        </li>
                      </ul>
			<!-- <li>
                        <b>Feedback Management Module</b>
                      </li>
                      <ul>
                        <li>
                          <a href="{XML/ADMIN_MAIN_URL}add_feedback_subject.php">Feedback Subject Management</a>
                        </li>
                      </ul>
                        <ul>
                        <li>
                          <a href="{XML/ADMIN_MAIN_URL}add_feedback.php">Feedbacks Management</a>
                        </li>
                      </ul>

		      
			<li>
                        <b>Clinck Management Module</b>
                      </li>
                      <ul>
                        <li>
                          <a href="{XML/ADMIN_MAIN_URL}clinck_videos.php">Clinck Videos Management</a>
                        </li>
                      </ul>
		      <ul>
                        <li>
                          <a href="{XML/ADMIN_MAIN_URL}clinck_slideshow.php">Clinck Slideshow Management</a>
                        </li>
                      </ul>
		      <li>
                        <b>Dealer Management Module</b>
                      </li>
                      <ul>
                        <li>
                          <a href="{XML/ADMIN_MAIN_URL}dealer.php">Dealer Management</a>
                        </li>
                      </ul-->
		      <!--ul>
                        <li>
                          <a href="{XML/ADMIN_MAIN_URL}featured_dealer.php">Featured Dealer Management</a>
                        </li>
                      </ul>
		      <ul>
                        <li>
                          <a href="{XML/ADMIN_MAIN_URL}premium_dealer.php">Premium Dealer Management</a>
                        </li>
                      </ul-->
		      <!--ul>
                        <li>
                          <a href="{XML/ADMIN_MAIN_URL}related_dealer.php">Featured Dealer/Premium Dealer Management</a>
                        </li>
                      </ul>
		      <ul>
                        <li>
                          <a href="{XML/ADMIN_MAIN_URL}request_type.php">Request Type Management</a>
                        </li>
                      </ul>
		     <li>
                        <b>User Information Management Module</b>
                      </li>
                      <ul>
                        <li>
                          <a href="{XML/ADMIN_MAIN_URL}user_info.php">User Info Management</a>
                        </li>
                      </ul>
		      <li>
                        <b>Top User Review Widget Management Module</b>
                      </li>
                      <ul>
                        <li>
                          <a href="{XML/ADMIN_MAIN_URL}user_review_widget.php">Top User Review Management</a>
                        </li>
                      </ul>
		      <li>
                        <b>Editor Info Management Module</b>
                      </li>
                      <ul>
                        <li>
                          <a href="{XML/ADMIN_MAIN_URL}editorinfo.php">Editor Info Management</a>
                        </li>
                      </ul-->
    
                          
			<!--li>
                        <b>Oncars Compare Management Module</b>
                      </li>
                      <ul>
                        <li>
                          <a href="{XML/ADMIN_MAIN_URL}oncars_comparison.php">Oncars Compare Set Management</a>
                        </li>
                      </ul>
                      <ul>
                        <li>
                          <a href="{XML/ADMIN_MAIN_URL}featured_oncars_comparison.php">Featured Oncars Compare Set Management</a>
                        </li>
                      </ul-->


			<!--li>
                        <b>India Home page Oncars Management Module</b>
                      </li>
                      <ul>
                        <li>
                          <a href="{XML/ADMIN_MAIN_URL}homepage_item.php">India Home Oncars Management</a>
                        </li>
                      </ul-->

		      <li>
                        <b>Custom Top Listing Management</b>
                      </li>
                      <ul>
                        <li>
                          <a href="{XML/ADMIN_MAIN_URL}custom_top_stories.php">Add/Modify Top Stories</a>
                        </li>
                        <li>
                          <a href="{XML/ADMIN_MAIN_URL}custom_top_products.php">Add/Modify Top Products</a>
                        </li>

                      </ul>


					<li>
                        <b>Upload Management</b>
                      </li>
                      <ul>
                        <li>
                          <a href="{XML/ADMIN_MAIN_URL}script_upload_bulk_data.php">Upload bulk data</a>
                        </li>
                        <li>
                          <a href="{XML/ADMIN_MAIN_URL}script_upload_bulk_data.php?action=update">Update search data</a>
                        </li>

                      </ul>

				    </ul>

                  </td>
                </tr>
              </table>
            </td>
            <!-- main area  END -->
            <!-- call Right Navigation -->
            <td width="15%"  valign="top">
              <xsl:call-template name="incRightNavigation"/>
            </td>
          </tr>
          <!-- call footer -->
          <tr>
            <td colspan="3">
              <xsl:call-template name="incFooter"/>
            </td>
          </tr>
        </table>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
