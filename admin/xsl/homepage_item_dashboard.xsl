<?xml version="1.0" ?>
<!DOCTYPE xsl:stylesheet  [
  <!ENTITY nbsp   "&#160;">
  <!ENTITY copy   "&#169;">
  <!ENTITY reg    "&#174;">
  <!ENTITY trade  "&#8482;">
  <!ENTITY mdash  "&#8212;">
  <!ENTITY ldquo  "&#8220;">
  <!ENTITY rdquo  "&#8221;">
  <!ENTITY pound  "&#163;">
  <!ENTITY yen    "&#165;">
  <!ENTITY euro   "&#8364;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html" version="4.0" encoding="UTF-8" indent="yes"/>
    <xsl:include href="inc_header.xsl" /><!-- include header-->
    <xsl:include href="inc_footer.xsl" /><!-- include footer-->
    <xsl:template match="/">
        <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title>:: Ajax - Admin Clinck Video Dashboard Management ::</title>
		
                <link rel="stylesheet" type="text/css" href="{/XML/CSS_URL}main.css" />
	        <script LANGUAGE="JavaScript" SRC="{XML/ADMIN_JS_URL}category.js"></script>
        	<script language="javascript" src="{XML/ADMIN_JS_URL}homepage_item.js"></script>
            	<script language="javascript" src="{XML/ADMIN_JS_URL}tiny_mce/tiny_mce.js"></script>
                <script>var admin_web_url = '<xsl:value-of select="/XML/ADMIN_WEB_URL" disable-output-escaping="yes"/>';</script>
	   
            </head>
            <body>
				
                <table align="center" width="100%" border="0" cellpadding="2" cellspacing="2">
                   	<tr>
                         <td>
                            <div align="center" style="border: 1px solid #d5d5d5;">
                                <table width="100%" border="0" id="product_dashboard" colspacing="0" rowspacing="0">
                                    <tr style="border: 1px solid #d5d5d5;">
                                        <td colspan="18" style="border: 1px solid #d5d5d5;">
                                            <div align="center">
                                                <h3>Dashboard</h3>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr style="border: 1px solid #d5d5d5;">
                                        <td colspan="18" style="border: 1px solid #d5d5d5;">
                                            <div align="right">pagination goes here</div>
                                        </td>
                                    </tr>
                                    <tr class="row0">
                                        <td class="b">Sr. No</td>
                                        <td class="b">Title</td>
                                        <td class="b">Type</td>
					<td class="b">Source Type</td>
                                        <td class="b">Status</td>
                                        <td class="b">Create date</td>
					<td colspan="4" class="b">Action</td>
                                    </tr>
                                    <xsl:choose>
                                        <xsl:when test="/XML/ITEM_MASTER/COUNT&lt;=0">
                                            <tr>
                                                <td colspan="12">
                                                    <div align="center">Zero result found.</div>
                                                </td>
                                            </tr>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:for-each select="/XML/ITEM_MASTER/ITEM_MASTER_DATA">
												
                                                <tr class="row1">
						    <td>
                                                        <xsl:value-of select="position()" diseable-output-esacaping="yes"/>
                                                    </td>
                                                    <td>
                                                        <xsl:value-of select="TITLE" diseable-output-esacaping="yes"/>
                                                    </td>
                                                    <td>
                                                        <xsl:value-of select="CONTENT_TYPE_DATA" diseable-output-esacaping="yes"/>
                                                    </td>
						     <td>
                                                        <xsl:value-of select="SECTION_TYPE" diseable-output-esacaping="yes"/>
                                                    </td>
						    <td>
                                                        <xsl:value-of select="STATUS" diseable-output-esacaping="yes"/>
                                                    </td>
						    <td>
                                                        <xsl:value-of select="CREATE_DATE" diseable-output-esacaping="yes"/>
                                                    </td>
                                                    <td>
							<a href="#Update" id="updateMe" onclick="updateProductItem('prod_article_dashboard','productajaxloader','{CATEGORY_ID}','{ID}','{ITEM_ID}','{CONTENT_TYPE_ID}','{TBL_TYPE_ID}');">Update</a>
                                                    </td>
                                                    <td><a href="javascript:undefined;" onclick="deleteProductVideo('{ID}');">Delete</a></td>
                                                </tr>
                                            </xsl:for-each>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                    <tr style="border: 1px solid #d5d5d5;">
                                        <td colspan="18" style="border: 1px solid #d5d5d5;">
                                            <div align="right">pagination goes here</div>
                                        </td>
                                    </tr>
                                </table>
                                <!--start code to add product form -->
                                <form enctype="multipart/form-data" method="post" action="{XML/ADMIN_WEB_URL}homepage_item.php" name="product_manage" id="product_manage" onsubmit="return validateProduct();">
                                    <table width="100%" border="0" id="add_product_table">                                        
                                        <tr >
                                            <td align="right">                                                
                                                <table width="100%" border="0">
                                                    <tr class="row1">
                                                        <td>
                                                            <input type="hidden" name="startlimit" id="startlimit" value="{/XML/STARTLIMIT}"/>
                                                        </td>
                                                        <td>
                                                            <input type="hidden" name="cnt" id="cnt" value="{/XML/CNT}"/>
                                                        </td>
							<td>
                                                            <input type="hidden" name="hd_view_section_id" id="hd_view_section_id" value=""/>
                                                            <input type="hidden" name="hd_item_id" id="hd_item_id" value=""/>
							    <input type="hidden" name="hd_id" id="hd_id" value=""/>
                                                        </td>
                                                        <td>
                                                            <input type="hidden" name="actiontype" id="actiontype" value=""/>
                                                        </td>
                                                        <td></td>
                                                        <td colspan="2"></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                </table>
                                                <table width="100%" id="Update" border="0" class="row1" style="border:1px solid #d4d4d4;">
						    <tbody id="slideshowtbody">
                                                    <tr class="datarow1">
                                                        <td>Item Type</td>
                                                        <td colspan="10">
                                                            <select name="select_type_id" id="select_type_id" onchange="getSectionData();">
                                                                <option value="0">---Select Type---</option>
								
									<xsl:choose>
								  	        <xsl:when test="/XML/CONTENT_TYPE_ID='1'">
											<option value="1" selected='yes'>Video</option>
										</xsl:when>
										<xsl:otherwise>
											<option value="1" >Video</option>
										</xsl:otherwise>
									</xsl:choose>
									<xsl:choose>
										<xsl:when test="/XML/CONTENT_TYPE_ID='2'">
											<option value="2" selected='yes'>Text</option>
										</xsl:when>		
										<xsl:otherwise>
											<option value="2" >Text</option>
										</xsl:otherwise>
									</xsl:choose>
								
                					</select>
                                                        </td>
                                                    </tr>
						     <tr class="datarow1">
                                                        <td>Item Source Section</td>
                                                        <td colspan="10">
                                                            <select name="select_section_id" id="select_section_id" onchange="javascript: getProductVideoDashboardByType('prod_article_dashboard','productajaxloader','{XML/SELECTED_CATEGORY_ID}','','');">
                                                                <option value="0">---Select Section---</option>
								<xsl:for-each select="/XML/ITEM_SECTION_MASTER/ITEM_SECTION">
									<xsl:choose>
										<xsl:when test="/XML/SELECT_SECTION_ID=SECTION_ID">
										<option value="{SECTION_ID}" selected='yes'>
											<xsl:value-of select="SECTION_NAME"/>
										</option>
										</xsl:when>
										<xsl:otherwise>
											<option value="{SECTION_ID}">
												<xsl:value-of select="SECTION_NAME"/>
											</option>
										</xsl:otherwise>	
									</xsl:choose>
								</xsl:for-each>	
                					</select>
                                                        </td>
                                                    </tr>
						    <tr class="datarow1">
							<td>Item List</td>
							<td colspan="10">
							        <select name="select_item_id" id="select_item_id">
							                <option value="0">---Select Item---</option>
							                <xsl:for-each select="/XML/ITEM_DETAILS_MASTER/ITEM_DETAILS_MASTER_DATA">
									<xsl:choose>
										<xsl:when test="/XML/ITEM_DATA/ITEM_ID=ITEM_ID">
							                		<option value="{ITEM_ID}" selected='yes'><xsl:value-of select="TITLE"/></option>
										</xsl:when>
										<xsl:otherwise>
									                <option value="{ITEM_ID}"><xsl:value-of select="TITLE"/></option>
										</xsl:otherwise>
									</xsl:choose>
							                </xsl:for-each>
							        </select>
							        <div id="ajaxloaderType" style="display:none;">
							        <div align="center"><img src="{/XML/IMAGE_URL}ajax-loader.gif"/></div>
								</div>
							</td>
                                                    </tr>
						    <tr class="datarow1">
							<td>Item Image</td>
							<td colspan="10">
								<input name="img_id" type="hidden" size="40" id="img_id" value="{/XML/ITEM_DATA/IMG_ID}"/>
                                                                <input name="img_upload_id_thm" type="hidden" size="40" id="img_upload_id_thm" value="{/XML/ITEM_DATA/IMG_PATH}"/>
                                                                <input type="text" name="thumb_title" id="thumb_title" value="{/XML/ITEM_DATA/IMG_PATH}" />
                                                                <input type="hidden" name="content_type" id="content_type" value="{/XML/ITEM_DATA/CONTENT_TYPE_ID}"/>
                                                                <input type="button" name="btn_get" id="btn_get" value="image upload" onclick="getUploadData('product_manage','thumb_title','img_id','img_upload_id_thm','image','content_type');"/>
                                                               (Select size as 148X113)
							</td>
						    </tr>
					            <tr>
                                                    <td> Status#1</td>
                                                        <td colspan="14">
                                        	                <select name="status" id="status">
									<xsl:choose>
								  	        <xsl:when test="/XML/ITEM_DATA/STATUS='Active'">
											<option value="1" selected='yes'>Active</option>
										</xsl:when>
										<xsl:otherwise>
											<option value="1" >Active</option>
										</xsl:otherwise>
									</xsl:choose>
									<xsl:choose>
										<xsl:when test="/XML/ITEM_DATA/STATUS='InActive'">
											<option value="0" selected='yes'>InActive</option>
										</xsl:when>		
										<xsl:otherwise>
											<option value="0" >InActive</option>
										</xsl:otherwise>
									</xsl:choose>
								</select>
                                                        </td>
                                                    </tr>
						<tr>						
						   <td colspan="11"><input type="hidden" name="selected_category_id" id="selected_category_id" value="{/XML/SELECTED_CATEGORY_ID}"/></td>
						</tr>
						<tr>
							<td colspan="9">&nbsp;</td>
        	                                        <td>
                	                                <div align="center">
                        		                        <input type="submit" name="save" value="Save" class="formbtn"/>
                                        	        </div>
                                                	</td>
                                                	<td>
	                                                <div align="center">
        		                                        <input type="button" name="cancel" value="Cancel" class="formbtn" onclick="javascript:this.form.reset();"/>
                        	                        </div>
                                	                </td>
                                                </tr>
						</tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </form>
                                <!--end code to add product form -->
                            </div>
                        </td>
                        <!-- main area  END -->
                    </tr>
                </table>
            </body>
	    </html>
    </xsl:template>
</xsl:stylesheet>
