<?xml version="1.0" ?>
<!DOCTYPE xsl:stylesheet  [
  <!ENTITY nbsp   "&#160;">
  <!ENTITY copy   "&#169;">
  <!ENTITY reg    "&#174;">
  <!ENTITY trade  "&#8482;">
  <!ENTITY mdash  "&#8212;">
  <!ENTITY ldquo  "&#8220;">
  <!ENTITY rdquo  "&#8221;">
  <!ENTITY pound  "&#163;">
  <!ENTITY yen    "&#165;">
  <!ENTITY euro   "&#8364;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html" version="4.0" encoding="UTF-8" indent="yes"/>
    <xsl:include href="../xsl/inc_header.xsl" /><!-- include header-->
    <xsl:include href="../xsl/inc_footer.xsl" /><!-- include footer-->
    <xsl:include href="../xsl/inc_leftnavigation.xsl" /><!-- include left navigation-->
    <xsl:include href="../xsl/inc_rightnavigation.xsl" /><!-- include right navigation-->
    <xsl:template match="/">
        <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title>Mobile : Admin Category Management</title>
                <link rel="stylesheet" type="text/css" href="{XML/WEB_URL}css/main.css" />
                <script LANGUAGE="JavaScript" SRC="{XML/ADMIN_JS_URL}common.js"></script>
                <script LANGUAGE="JavaScript" SRC="{XML/ADMIN_JS_URL}category.js"></script>
            </head>
            <body>
                <table align="center" width="100%" border="0" cellpadding="2" cellspacing="2">
                    <!-- call Header -->
                    <tr>
                        <td colspan="3">
                            <xsl:call-template name="incHeader"/>
                        </td>
                    </tr>
                    <tr>
                        <!-- call Left Navigation -->
                        <td width="10%" valign="top">
                            <xsl:call-template name="incLeftNavigation"/>
                        </td>
                        <!-- main area -->
                        <td>
                            <div align="center">
                                <table width="100%" border="0">
                                    <tr>
                                        <td>
                                            <h2>
											<xsl:choose>
												<xsl:when test="XML/ACTION='update'">
												  Refresh Search Data
												</xsl:when>
												<xsl:otherwise>
												  Upload Bulk Data
												</xsl:otherwise>
											  </xsl:choose>
											</h2>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <a href="{XML/ADMIN_WEB_URL}index.php">Main</a>
> Dashboard</td>
                                    </tr>
                                </table>
                            </div>
                            <table width="100%" border="1" id="category_dashboard">
                                <tr>
                                    <td colspan="6">
                                        <div align="center">
                                            <h3>Dashboard</h3>
                                        </div>
                                    </td>
                                </tr>
                                <xsl:if test="/XML/MSG!=''">
                                    <tr>
                                        <td colspan="6" bgcolor="#98AFC7">
                                            <div align="center">
                                                <xsl:value-of select="/XML/MSG" disable-output-escaping="yes" />
                                            </div>
                                        </td>
                                    </tr>
                                </xsl:if>
                                
                            </table>

							<xsl:choose>
								<xsl:when test="XML/ACTION='update'">
								  
								</xsl:when>
								<xsl:otherwise>
									<table width="100%" border="0">
										<tr>
											<td colspan="6" width="100%">
												<h3>Upload data file (xls)</h3>
											</td>
										</tr>
									</table>
									<table width="100%" id="show_categorys">
										<tr>
											<td>
												<td>
													<form action="{/XML/ADMIN_WEB_URL}script_upload_bulk_data.php" method="post" enctype="multipart/form-data">
													<input type="file" NAME="filename" size="50" /> 
													<input type="hidden" name="action" value="upload" />
													<input type="submit" name="submit" value="Upload" />
													</form>
												</td>
											</td>
										</tr>
									</table>
								</xsl:otherwise>
							  </xsl:choose>


                        </td>
                        <!-- main area  END -->
                        <!-- call Right Navigation -->
                        <td width="15%"  valign="top">
                            <xsl:call-template name="incRightNavigation"/>
                        </td>
                    </tr>
                    <!-- call footer -->
                    <tr>
                        <td colspan="3">
                            <xsl:call-template name="incFooter"/>
                        </td>
                    </tr>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>
