<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet  [
<!ENTITY nbsp   "&#160;">
<!ENTITY copy   "&#169;">
<!ENTITY reg    "&#174;">
<!ENTITY trade  "&#8482;">
<!ENTITY mdash  "&#8212;">
<!ENTITY ldquo  "&#8220;">
<!ENTITY rdquo  "&#8221;"> 
<!ENTITY pound  "&#163;">
<!ENTITY yen    "&#165;">
<!ENTITY euro   "&#8364;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="utf-8" doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"/>
<xsl:template match="/">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Upload</title>
<link href="{/XML/HOME}admin.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="{/HOME/ADMIN_JS_URL}tiny_mce/tiny_mce.js"></script>
<script type="text/javascript" src="{/HOME/ADMIN_JS_URL}common.js"></script>
<script>
var sform_name = '<xsl:value-of select="HOME/SFORMNAME"/>'
var stitle_text = '<xsl:value-of select="HOME/PARENT_TITLE"/>';
var media_hdtext = '<xsl:value-of select="HOME/PARENT_UPLOADHDTXT"/>';
var hdthumbtext = '<xsl:value-of select="HOME/PARENT_THUMBTXT"/>';
var sParPath = '<xsl:value-of select="HOME/PARENT_SPATH"/>';
var stype = '<xsl:value-of select="HOME/STYPE"/>';
var scontenttype = '<xsl:value-of select="HOME/PARENT_CONTENTPATH"/>';

var content_type = '<xsl:value-of select="/HOME/CONTENT_TYPE" disable-output-escaping="yes"/>';

function UpdateData(){
	var title = document.getElementById('media_title').value;
	var media_id = document.getElementById('media_id').value;
	var sPath = document.getElementById('spath').value;	
	window.opener.document.getElementById(stitle_text).value = sPath;
	window.opener.document.getElementById(media_hdtext).value = media_id;
	window.opener.document.getElementById(sParPath).value = sPath;
	window.opener.document.getElementById(scontenttype).value = content_type;
	window.close();
}
</script>
</head>
<body>
<!--start of main div-->
<div id="maindiv">
	<form name="frmupload" method="post" action="" enctype="multipart/form-data">
		<table width="100%" border="0" align="center" cellpadding="2" cellspacing="2" bgcolor="#ececec">
			<tr>
				<td align="center" valign="top" style="border:1px solid #000000;">
						<table width="80%" border="0" align="center" cellpadding="2" cellspacing="2" bgcolor="#ececec">
							<tr><td>New Upload</td></tr>
							<tr>
								<td align="left" bgcolor="#ececec" class="black_plain">Title:
								<input name="fld_title" type="text" id="fld_title" size="20" value=""/>
								<input name="media_id" type="hidden" id="media_id" size="20" value="{/HOME/MEDIAID}"/>
								<input name="media_title" type="hidden" id="media_title" size="20" value="{/HOME/MEDIATITLE}"/>
								<input name="thumb_image" type="hidden" id="thumb_image" size="20" value="{/HOME/MEDIATHUMBIMG}"/>
								<input name="spath" type="hidden" id="spath" size="20" value="{/HOME/MEDIAPATH}"/>
								<input name="scontentpath" type="hidden" id="scontentpath" size="20" value="{/HOME/PARENT_CONTENTPATH}"/>
								
								<div id="fld_title_div" class="rederr"></div>
								</td>
								</tr>
								<tr>
								<td align="left" bgcolor="#ececec" class="black_plain">Tags:
								<input name="fld_tags" type="text" id="fld_tags" size="20" value=""/>
								<div id="fld_tags_div" class="rederr"></div>
								</td>
								</tr>
								<tr>
								<td align="left" bgcolor="#ececec" class="black_plain">Description:
								
								<textarea name="fld_description" id="fld_description" cols="40" rows="2"></textarea>
								<div id="fld_description_div"></div>
								</td>
								<td><img src="{/HOME/CENTRAL_IMAGE_URL}{/HOME/MEDIATHUMBIMG}" id="img_name" name="img_name" widht="50" height="50"/></td>
							</tr>
							<tr>
								<td align="left" bgcolor="#ececec" class="black_plain"></td>
							</tr>

							<tr>
								<td align="left" bgcolor="#ececec" class="black_plain">Select Size
								<span class="gr">
								<select name="imagesize[]" size='2' multiple='yes'> <xsl:value-of select="HOME/IMAGE_SIZE_LIST" disable-output-escaping="yes"/></select>
								</span>
								</td>
							</tr>	

							<tr>
								<td align="left" bgcolor="#ececec" class="black_plain">Add File
								<span class="gr">
								<input type="file" name="upload_file" id="upload_file" value=""/>
								</span>
								</td>
							</tr>	
							<tr>
								<td align="right" bgcolor="#ececec" class="black_plain">
								<span class="gr">
								<input type="submit" name="btn_save" id="btn_save" value="upload"/>
								<input type="button" name="btn_done" id="btn_done" value="Done" onclick="UpdateData();"/>
								<input type="button" name="btn_close" id="btn_close" value="close without saving" onclick="window.close();"/>
								</span></td>
							</tr>
						</table>
					
				</td>
			</tr>
			
			
		</table>
	</form>
</div>
<!--end of main div-->
</body>
</html>
</xsl:template>
</xsl:stylesheet>
