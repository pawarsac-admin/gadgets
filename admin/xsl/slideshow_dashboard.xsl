<?xml version="1.0" ?>
<!DOCTYPE xsl:stylesheet  [
  <!ENTITY nbsp   "&#160;">
  <!ENTITY copy   "&#169;">
  <!ENTITY reg    "&#174;">
  <!ENTITY trade  "&#8482;">
  <!ENTITY mdash  "&#8212;">
  <!ENTITY ldquo  "&#8220;">
  <!ENTITY rdquo  "&#8221;">
  <!ENTITY pound  "&#163;">
  <!ENTITY yen    "&#165;">
  <!ENTITY euro   "&#8364;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html" version="4.0" encoding="UTF-8" indent="yes"/>
    <xsl:include href="inc_header.xsl" /><!-- include header-->
    <xsl:include href="inc_footer.xsl" /><!-- include footer-->
    <xsl:template match="/">
        <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title>:: Ajax - Admin Product Dashboard Management ::</title>
                <link rel="stylesheet" type="text/css" href="{/XML/CSS_URL}main.css" />		       
                <script LANGUAGE="JavaScript" SRC="{XML/ADMIN_JS_URL}common.js"></script>
                <script LANGUAGE="JavaScript" SRC="{XML/ADMIN_JS_URL}category.js"></script>
                <script language="javascript" src="{XML/ADMIN_JS_URL}slideshow.js"></script>
                 <script language="javascript" src="{XML/ADMIN_JS_URL}tiny_mce/tiny_mce.js"></script>
				
                <script>
                  
		        var admin_web_url = '<xsl:value-of select="/XML/ADMIN_WEB_URL" disable-output-escaping="yes"/>';
              </script>
	   
            </head>
            <body>
				
                <table align="center" width="100%" border="0" cellpadding="2" cellspacing="2">
					<tr>
                        <td>
                            <div align="center" style="border: 1px solid #d5d5d5;">
                                <table width="100%" border="0" id="product_dashboard" colspacing="0" rowspacing="0">
                                    <tr style="border: 1px solid #d5d5d5;">
                                        <td colspan="18" style="border: 1px solid #d5d5d5;">
                                            <div align="center">
                                                <h3>Dashboard</h3>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr style="border: 1px solid #d5d5d5;">
                                        <td colspan="18" style="border: 1px solid #d5d5d5;">
                                            <div align="right">pagination goes here</div>
                                        </td>
                                    </tr>
                                    <tr class="row0">
                                        <td>Sr.No</td>
                                        <td>Slideshow Title</td>
                                        <td>Category Name</td>
                                        <td>Brand Name</td>
                                        <td>Product Name</td>
                                        <td>Variant</td>
                                        <td>Status</td>
					<td>Create date</td>
					<td colspan="4">Action</td>
                                    </tr>
                                    <xsl:choose>
                                        <xsl:when test="/XML/SLIDESHOW_MASTER/COUNT&lt;=0">
                                            <tr>
                                                <td colspan="12">
                                                    <div align="center">Zero result found.</div>
                                                </td>
                                            </tr>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:for-each select="/XML/SLIDESHOW_MASTER/SLIDESHOW_MASTER_DATA">
												
                                                <tr class="row1">
                                                    <td>
                                                        <xsl:value-of select="position()" diseable-output-esacaping="yes"/>
                                                    </td>
						   <td>
                                                        <xsl:value-of select="TITLE" diseable-output-esacaping="yes"/>
                                                    </td>

                                                    <td>
                                                        <xsl:value-of select="CATEGORY_NAME" diseable-output-esacaping="yes"/>
                                                    </td>
                                                    <td>
                                                        <xsl:value-of select="BRAND_NAME" diseable-output-esacaping="yes"/>
                                                    </td>
                                                    <td>
                                                        <xsl:value-of select="PRODUCT_NAME" diseable-output-esacaping="yes"/>
                                                    </td>
                                                                                                        <td>
                                                        <xsl:value-of select="PRODUCT_VARIANT" diseable-output-esacaping="yes"/>
                                                    </td>
                                                                                                        <td>
                                                        <xsl:value-of select="STATUS" diseable-output-esacaping="yes"/>
                                                    </td> 
													
													<td>
                                                        <xsl:value-of select="CREATE_DATE" diseable-output-esacaping="yes"/>
                                                    </td>
                                                    <td>
                                                        <a href="javascript:void(0);" id="updateMe" onclick="updateProductSlideshow('prod_article_dashboard','productajaxloader','','{CATEGORY_ID}','{PRODUCT_ID}','{PRODUCT_INFO_ID}','{BRAND_ID}',{PRODUCT_SLIDE_ID},'','');">Update</a>
                                                    </td>
                                                    <td>
                                                        <a href="javascript:undefined;" onclick="deleteProductSlideshow('{PRODUCT_SLIDE_ID}');">Delete</a>
                                                    </td>
                                                </tr>
                                            </xsl:for-each>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                    <tr style="border: 1px solid #d5d5d5;">
                                        <td colspan="18" style="border: 1px solid #d5d5d5;">
                                            <div align="right">pagination goes here</div>
                                        </td>
                                    </tr>
                                </table>
                                <!--start code to add product form -->
                                <form enctype="multipart/form-data" method="post" action="{XML/ADMIN_WEB_URL}slideshow.php" name="product_manage" id="product_manage" onsubmit="return validateProduct();">
                                    <table width="100%" border="0" id="add_product_table">                                        
                                        <tr >
                                            <td align="right">                                                
                                                <table width="100%" border="0">
                                                    <tr class="row1">
                                                        <td>
                                                            <input type="hidden" name="startlimit" id="startlimit" value="{/XML/STARTLIMIT}"/>
                                                        </td>
                                                        <td>
                                                            <input type="hidden" name="cnt" id="cnt" value="{/XML/CNT}"/>
                                                        </td>
                                                        <td>
                                                            <!--<input type="hidden" name="product_id" id="product_id" value="{XML/ARTICLE_DATA/PRODUCT_ID}"/>-->
															
                                                        </td>
                                                        <td>
                                                            <input type="hidden" name="actiontype" id="actiontype" value="{/XML/SELECTED_ACTION_TYPE}"/>
						 	<input type="hidden" name="slideshow_id" id="slideshow_id" value="{/XML/SLIDESHOW_ID}"/>
						 	<input type="hidden" name="product_slideshow_id" id="product_slideshow_id" value=""/>
                                                        </td>
                                                        
                                                        <td></td>
                                                        <td colspan="2"></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                </table>
                                                <table width="100%" id="Update" border="0" class="row1" style="border:1px solid #d4d4d4;">
							<tbody id="slideshowtbody">
							<tr class="datarow1">
                                                       		<td>Brand Name</td>
	                                                        <td colspan="10">
        	                                                <select name="select_brand_id" id="select_brand_id" onchange="getModelByBrand('ajaxloader','0');">
                		                                        <option value="">---Select Brand---</option>
                                		                        <xsl:for-each select="/XML/BRAND_MASTER/BRAND_MASTER_DATA">
	                                                		        <xsl:choose>
        			                                                <xsl:when test="/XML/SLIDESHOW_DETAIL/SLIDESHOW_DETAIL_DATA/BRAND_ID=BRAND_ID">
                                				                        <option value="{BRAND_ID}" selected='yes'>
												<xsl:value-of select="BRAND_NAME"/>
				                                                        </option>
                                			                        </xsl:when>
										<xsl:otherwise>
				                                                        <option value="{BRAND_ID}">
                                					                        <xsl:value-of select="BRAND_NAME"/>
				                                                        </option>
                                			                        </xsl:otherwise>
                                                        			</xsl:choose>
		                                                        </xsl:for-each>
								</select>
	                	                                <div id="ajaxloader" style="display:none;">
                                		                        <div align="center"><img src="{/XML/IMAGE_URL}ajax-loader.gif"/></div>
                                                        	</div>
								</td>
                                                    	</tr>
                                                   	<tr class="datarow1"><td></td><td colspan="10"></td></tr>
							<tr class="datarow1"><td></td><td colspan="10"></td></tr>
							<tr class="datarow1">
								<td> Slideshow Title</td>
	                                                        <td colspan="10">
									<input type="text" name="slide_title" id="slide_title" value="{XML/SLIDESHOW_DETAIL/SLIDESHOW_DETAIL_DATA/PRODUCT_TITLE}"/>
									<input type="hidden" name="hd_product_slide_id" id="hd_product_slide_id" value="{XML/SLIDESHOW_DETAIL/SLIDESHOW_DETAIL_DATA/PRODUCT_SLIDE_ID}"/>
								</td>
							</tr>
							<tr class="datarow1">
                                                        	<td>Upload Slide Thumbnail Image:</td>
                                                                <td colspan="10">
                                                                	<input name="abstract_img_id" type="hidden" size="40" id="abstract_img_id" value="{XML/SLIDESHOW_DETAIL/SLIDESHOW_DETAIL_DATA/PRODUCT_SLIDE_MEDIA_ID}"/>
                                                                        <input name="abstract_img_path" type="hidden" size="40" id="abstract_img_path" value="{XML/SLIDESHOW_DETAIL/SLIDESHOW_DETAIL_DATA/PRODUCT_SLIDE_MEDIA_PATH}"/>
                                                                        <input type="hidden" name="content_type" id="content_type" value=""/>
                                                                        <input type="text" name="thumb_title" id="thumb_title" value="{XML/SLIDESHOW_DETAIL/SLIDESHOW_DETAIL_DATA/PRODUCT_SLIDE_MEDIA_PATH}" readonly="yes"/>
                                                                        <input type="button" name="btn_get" id="btn_get" value="image upload" onclick="getUploadData('product_manage','thumb_title','abstract_img_id','abstract_img_path','image','content_type');"/>
								</td>
							  </tr>
							<tr class="datarow1">
								<td>Abstract:</td>
								<td colspan="10">
                                                        	<textarea name="product_slide_abstract" id="product_slide_abstract" cols="60" rows="2" >
		                                                        <xsl:value-of select="XML/SLIDESHOW_DETAIL/SLIDESHOW_DETAIL_DATA/PRODUCT_SLIDE_ABSTRACT"/>
                	                                        </textarea>
								</td>
							</tr>
						<!--xsl:if test="count(/XML/SLIDESHOW_DETAIL/SLIDESHOW_DETAIL_DATA)=0"-->
						<xsl:if test="count(/XML/SLIDE_UPLOAD_DETAIL/SLIDE_UPLOAD_DATA)=0">
						<tr>
							<td> Slide Image 1</td>		
							<td>title : <input type="text" name="title_1" id="title_1" value=""/>
								tags :<input type="text" name="tags_1" id="tags_1" value=""/>
								<br/>meta description :<br/>
								<textarea name="meta_description_1" id="meta_description_1" cols="60" rows="2" ></textarea>
								<br/>Media (video/image) 1:
								<input name="media_id_1" type="hidden" size="40" id="media_id_1" value=""/>
								<input name="media_upload_1_1" type="hidden" size="40" id="img_upload_id_1_1" value=""/>
								<input type="text" name="title_upload_file_1" id="title_upload_file_1" value="" />
								<input type="hidden" name="video_content_type_1" id="video_content_type_1" value=""/>
								<input type="button" name="btn_get" id="btn_get" value="media upload" onclick="getUploadData('product_manage','title_upload_file_1','media_id_1','img_upload_id_1_1','image','video_content_type_1');"/><br/>
								<span style="color:red;font-family:bold;">OR</span><br/>
								Upload Thumbnail Image 1:
								<input name="img_media_id_1" type="hidden" size="40" id="img_media_id_1" value=""/>
								<input name="img_upload_id_thm_1" type="hidden" size="40" id="img_upload_id_thm_1" value=""/>
								<input type="text" name="thumb_title_1" id="thumb_title_1" value="" /> 	
								<input type="hidden" name="content_type_1" id="content_type_1" value=""/>
								<input type="button" name="btn_get" id="btn_get" value="image upload" onclick="getUploadData('product_manage','thumb_title_1','img_media_id_1','img_upload_id_thm_1','image','content_type_1');"/>
								<input type='checkbox' name='box_1' id='box_1' value='' /> Delete
                                                                <input type='hidden' name='check_flag_1' id='check_flag_1' value=''/>
							</td>
						</tr>
						</xsl:if>
						<!--xsl:if test="count(/XML/SLIDESHOW_DETAIL/SLIDESHOW_DETAIL_DATA)>0"-->
						<xsl:if test="count(/XML/SLIDE_UPLOAD_DETAIL/SLIDE_UPLOAD_DATA)>0">
						<xsl:for-each select="/XML/SLIDE_UPLOAD_DETAIL/SLIDE_UPLOAD_DATA">
						<tr>
							<td> Slide Image <xsl:value-of select="position()"/></td>
							<td style="border:1px solid #d5d5d5;">
							title : <input type="text" name="title_{position()}" id="title_{position()}" value="{TITLE}"/>
							tags :<input type="text" name="tags_{position()}" id="tags_{position()}" value="{TAGS}"/>
							<br/>meta description :<br/>
							<textarea name="meta_description_{position()}" id="meta_description_{position()}" cols="60" rows="2" >
							<xsl:value-of select="META_DESCRIPTION"/>
							</textarea>
							<br/>
							Upload Media (upload video/audio/image) 1:	
							<input name="media_id_{position()}" type="hidden" size="40" id="media_id_{position()}" value="{MEDIA_ID}"/>
							<input name="media_upload_1_{position()}" type="hidden" size="40" id="media_upload_1_{position()}" value="{VIDEO_IMG_PATH}"/>
							<input type="text" name="title_upload_file_{position()}" id="title_upload_file_{position()}" value="{VIDEO_IMG_PATH}" />
							<input type="hidden" name="video_content_type_{position()}" id="video_content_type_{position()}" value="{CONTENT_TYPE}"/>
							<input type="button" name="btn_get" id="btn_get" value="media upload" onclick="getUploadData('product_manage','title_upload_file_{position()}','media_id_{position()}','media_upload_1_{position()}','image','video_content_type_{position()}');"/>
							<!--<input type="button" name="btn_search" id="btn_search" value="search" onclick="getUploadedDataList('product_manage','title_upload_file_{position()}','media_id_{position()}','media_upload_1_{position()}','image','main');"/>	-->
							<br/>
							<span style="color:red;font-family:bold;">OR</span><br/>
							Upload thumbnail Image <xsl:value-of select="position()"/>:
							<input name="img_media_id_{position()}" type="hidden" size="40" id="img_media_id_{position()}" value="{VIDEO_IMG_ID}"/>
							<input name="img_upload_id_thm_{position()}" type="hidden" size="40" id="img_upload_id_thm_{position()}" value="{VIDEO_IMG_PATH}"/>
							<input type="text" name="thumb_title_{position()}" id="thumb_title_{position()}" value="{VIDEO_IMG_PATH}" /> 	
							<input type="hidden" name="content_type_{position()}" id="content_type_{position()}" value="{CONTENT_TYPE}"/>
							<input type="button" name="btn_get" id="btn_get" value="image upload" onclick="getUploadData('product_manage','thumb_title_{position()}','img_media_id_{position()}','img_upload_id_thm_{position()}','image','content_type_{position()}');"/>
							<input type='checkbox' name="box_{position()}" id="box_{position()}" value='' /> Delete
                                                        <input type='hidden' name="check_flag_{position()}" id="check_flag_{position()}" value=''/>
							<!--<input type="button" name="btn_search" id="btn_search" value="search" onclick="getUploadedDataList('product_manage','thumb_title_{position()}','img_media_id_{position()}','img_upload_id_thm_{position()}','image','thumb');"/>-->
							<input name="upload_media_id_{position()}" id="upload_media_id_{position()}" value="{UPLOAD_MEDIA_ID}" type="hidden" />	
							<input type="hidden" name="slideshow_id_{position()}" id="slideshow_id_{position()}" value="{SLIDESHOW_ID}"/>
							<input type="hidden" name="product_slide_id_{position()}" id="product_slide_id_{position()}" value="{PRODUCT_SLIDE_ID}"/>
							</td>
						</tr>
						</xsl:for-each>
						</xsl:if>
						<tr>
						</tr>
						<tr id="trAddRemove">
							<td></td>
							<td colspan="0" align="right">
								<input id="add" value="Add More" onclick="addRemoveFileBrowseElements(1);" type="button" />&nbsp;
								<input id="remove" value="Remove" onclick="addRemoveFileBrowseElements(0);" type="button" />
							</td>
						</tr>
						   <tr class="datarow1">
							<td>Slide Type</td>
							<td colspan="10">
							        <select name="select_group_id" id="select_group_id">
							                <option value="0">---Select Type---</option>
							                <xsl:for-each select="/XML/TYPE_MASTER/TYPE_MASTER_DATA">
							                <xsl:choose>
							                        <xsl:when test="/XML/SLIDESHOW_DETAIL/SLIDESHOW_DETAIL_DATA/GROUP_ID=GROUP_ID">
							                                <option value="{GROUP_ID}" selected='yes'>
							                                        <xsl:value-of select="GROUP_NAME"/>
							                                </option>
							                        </xsl:when>
							                        <xsl:otherwise>
							                                <option value="{GROUP_ID}">
							                                        <xsl:value-of select="GROUP_NAME"/>
							                                </option>
							                        </xsl:otherwise>
							                </xsl:choose>
							                </xsl:for-each>
							        </select>
							</td>
						  </tr>
						<tr>
                                                        <td>Publish Time</td>
                                                        <td colspan="14">
                                                          <input type="text" name="publish_time" id="publish_time" length="15" value="{XML/SLIDESHOW_DETAIL/SLIDESHOW_DETAIL_DATA/PUBLISH_TIME}"/>(Format Required: YYYY-MM-DD HH:MM:SS)
                                                        </td>
                                                   </tr>
                                                    <tr>
                                                        <td>Status#1</td>
                                                        <td colspan="14">
															<xsl:if test="/XML/WALLCNT!=0">
																<input name="display_rows" id="display_rows" value="{/XML/WALLCNT}" type="hidden" />
															</xsl:if>
															<xsl:if test="/XML/WALLCNT=0">
																<input name="display_rows" id="display_rows" value="1" type="hidden" />
															</xsl:if>
															
                                                            <select name="status" id="status">
															<xsl:choose>
																<xsl:when test="XML/SLIDESHOW_DETAIL/SLIDESHOW_DETAIL_DATA/STATUS='Active'">
																	<option value="1" selected='yes'>Active</option>
																</xsl:when>
																<xsl:otherwise>
																	<option value="1">Active</option>
																</xsl:otherwise>
															</xsl:choose>
															<xsl:choose>
																<xsl:when test="XML/SLIDESHOW_DETAIL/SLIDESHOW_DETAIL_DATA/STATUS='InActive'">
																	<option value="0" selected='yes'>InActive</option>
																</xsl:when>
																<xsl:otherwise>
																	<option value="0">InActive</option>
																</xsl:otherwise>
															</xsl:choose>
															</select>
                                                        </td>
                                                    </tr>
													<tr>						
														<td colspan="11">
															<input type="hidden" name="selected_category_id" id="selected_category_id" value="{/XML/SELECTED_CATEGORY_ID}"/>
														</td>
													</tr>
													<tr>
													<td colspan="9">&nbsp;</td>
                                                        <td>
                                                            <div align="center">
                                                                <input type="submit" name="save" value="Save" class="formbtn"/>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div align="center">
                                                                <input type="button" name="cancel" value="Cancel" class="formbtn" onclick="javascript:this.form.reset();"/>
                                                            </div>
                                                        </td>
                                                    </tr>
													</tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </form>
                                <!--end code to add product form -->
                            </div>
                        </td>
                        <!-- main area  END -->
                    </tr>
                </table>
            </body>
	    </html>
    </xsl:template>
</xsl:stylesheet>
