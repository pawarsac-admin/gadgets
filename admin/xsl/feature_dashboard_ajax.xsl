<?xml version="1.0" ?>
<!DOCTYPE xsl:stylesheet  [
  <!ENTITY nbsp   "&#160;">
  <!ENTITY copy   "&#169;">
  <!ENTITY reg    "&#174;">
  <!ENTITY trade  "&#8482;">
  <!ENTITY mdash  "&#8212;">
  <!ENTITY ldquo  "&#8220;">
  <!ENTITY rdquo  "&#8221;">
  <!ENTITY pound  "&#163;">
  <!ENTITY yen    "&#165;">
  <!ENTITY euro   "&#8364;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html" version="4.0" encoding="UTF-8" indent="yes"/>
    <xsl:include href="inc_header.xsl" /><!-- include header-->
    <xsl:include href="inc_footer.xsl" /><!-- include footer-->
    <xsl:template match="/">
        <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title>:: Ajax - Admin Feature Dashboard Management ::</title>
                <link rel="stylesheet" type="text/css" href="{/XML/CSS_URL}main.css" />
                <script LANGUAGE="JavaScript" SRC="{XML/ADMIN_JS_URL}common.js"></script>
                <script LANGUAGE="JavaScript" SRC="{XML/ADMIN_JS_URL}category.js"></script>
                <script LANGUAGE="JavaScript" SRC="{XML/ADMIN_JS_URL}feature.js"></script>
            </head>
            <body>
	    <div id="sFeatureDetailsOverDiv">
                <table align="center" width="100%" border="0" cellpadding="2" cellspacing="2">
                    <tr>
                        <td>
                            <div align="center">
                                <table width="100%" border="1" id="feature_dashboard">
                                    <tr>
                                        <td colspan="12">
                                            <div align="center">
                                                <h3>Dashboard</h3>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="12">
                                            <div align="right"><xsl:value-of select="XML/Pages" disable-output-escaping="yes"/></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Sr.No</td>
                                        <td>Feature name</td>
                                        <td>Category Name</td>
                                        <td>Feature group</td>
                                        <td>Feature Unit</td>
                                        <td>Display on empty</td>
										<td>Status</td>
                                        <td>create date</td>
                                        <td colspan="3">&nbsp;</td>
                                        <td colspan="2">Action</td>
                                    </tr>
                                    <xsl:choose>
                                        <xsl:when test="/XML/FEATURE_MASTER/COUNT&lt;=0">
                                            <tr>
                                                <td colspan="12">
                                                    <div align="center">Zero result found.</div>
                                                </td>
                                            </tr>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:for-each select="/XML/FEATURE_MASTER/FEATURE_MASTER_DATA">
                                                <tr>
                                                    <td>
                                                        <xsl:value-of select="position()" diseable-output-esacaping="yes"/>
                                                    </td>
                                                    <td>
                                                        <xsl:value-of select="FEATURE_NAME" diseable-output-esacaping="yes"/>
                                                    </td>
                                                    <td>
                                                        <xsl:value-of select="CATEGORY_NAME" diseable-output-esacaping="yes"/>
                                                    </td>
                                                    <td>
                                                        <xsl:value-of select="FEATURE_GROUP_NAME" diseable-output-esacaping="yes"/>
                                                    </td>
                                                    <td>
                                                        <xsl:value-of select="FEATURE_UNIT" diseable-output-esacaping="yes"/>
                                                    </td>
													<td>

<xsl:choose>
<xsl:when test="SHOW_IF_EMPTY='1'">
	<img src="/images/yes.gif" alt="{SHOW_IF_EMPTY}" title="Display Empty feature"/>
</xsl:when>
<xsl:otherwise>
	<img src="/images/no.gif" alt="{SHOW_IF_EMPTY}" title="Hide Empty feature"/>
</xsl:otherwise>
</xsl:choose>


                                                    </td>
                                                    <td>
                                                        <xsl:value-of select="FEATURE_STATUS" diseable-output-esacaping="yes"/>
                                                    </td>
                                                    <td>
                                                        <xsl:value-of select="CREATE_DATE" diseable-output-esacaping="yes"/>
                                                    </td>
                                                    <td>Up</td>
                                                    <td colspan="2">Down</td>
                                                    <td>
                                                        <a href="#Update" onclick="updateFeature('{FEATURE_ID}','{JS_FEATURE_NAME}','{FEATURE_GROUP}','{JS_FEATURE_DESC}','{UNIT_ID}','{STATUS}','{MAIN_FEATURE_GROUP}',{SHOW_IF_EMPTY});">Update</a>
                                                    </td>
                                                    <td>
                                                        <a href="javascript:undefined;" onclick="deleteFeature('{FEATURE_ID}','{JS_FEATURE_NAME}');">Delete</a>
                                                    </td>
                                                </tr>
                                            </xsl:for-each>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                    <tr>
                                        <td colspan="13">
                                            <div align="right"><xsl:value-of select="XML/Pages" disable-output-escaping="yes"/></div>
                                        </td>
                                    </tr>
                                </table>
                                <table width="100%" border="0">
                                  <tr>
                                      <td colspan="6" width="100%">
                                          <h3>Add a new feature</h3>
                                      </td>
                                  </tr>
                                </table>
                                <table width="100%" border="0" id="add_feature_table">
                                    <tr>
                                        <td align="right">                                            
                                            <table width="100%" border="0">
                                                <tr>
                                                    <td>
                                                        <input type="hidden" name="startlimit" id="startlimit" value="{/XML/STARTLIMIT}"/>
                                                    </td>
                                                    <td>
                                                        <input type="hidden" name="cnt" id="cnt" value="{/XML/CNT}"/>
                                                    </td>
                                                    <td>
                                                        <input type="hidden" name="feature_id" id="feature_id"/>
                                                    </td>
                                                    <td>
                                                        <input type="hidden" name="actiontype" id="actiontype" value="insert"/>
                                                    </td>
                                                    <td>
                                                        <input type="hidden" name="featureboxcnt" id="featureboxcnt" value="1"/>
                                                    </td>
                                                    <td><input type="hidden" name="rowCount" id="rowCount" value="7"/></td>
                                                    <td></td>
                                                    <td colspan="2"></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                            </table>
                                            <table width="100%" id="Update" border="1">
						<tr>
                                                    <td>Feature Name#1</td>
                                                    <td colspan="10">
                                                        <input type="text" name="feature_name_0" id="feature_name_0" size="50"/>
                                                    </td>
                                                </tr>
						<tr>
							<td>Main Feature Group#1</td>
							<td colspan="10">
								<select name="select_main_group_0" id="select_main_group_0" onchange="getSubGroupByMainGroup('ajaxloadermaingroup','select_main_group_0','select_feature_group_0','')">
									<option value="">---Select Main Feature Group---</option>
									<xsl:for-each select="/XML/FEATURE_GROUP_MASTER/FEATURE_GROUP_MASTER_DATA">
										<option value="{GROUP_ID}">
											<xsl:value-of select="MAIN_GROUP_NAME" disable-output-escaping="yes"/>
										</option>
									</xsl:for-each>
								</select>
								<div id="ajaxloadermaingroup" style="display:none;">
									<div align="center">
										<img src="{/XML/IMAGE_URL}ajax-loader.gif"/>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<td>Feature Group#1</td>
							<td colspan="10" id="td_select_feature_group_0">
								<select name="select_feature_group_0" id="select_feature_group_id_0">
									<option value="">---Select Group---</option>
								</select>
							</td>
						</tr>							
                                                <tr>
                                                    <td>Feature Description#1</td>
                                                    <td colspan="10">
                                                        <textarea name="feature_description_0" cols="40" rows="5" id="feature_description_0"></textarea>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Feature Unit#1</td>
                                                    <td colspan="10">
                                                        <select name="feature_unit_0" id="feature_unit_0">
                                                            <option value="">---Select Unit---</option>
                                                            <xsl:for-each select="XML/FEATURE_UNIT/FEATURE_UNIT_DATA">
                                                                <option value="{UNIT_ID}">
                                                                    <xsl:value-of select="UNIT_NAME"/>
                                                                </option>
                                                            </xsl:for-each>
                                                        </select>
                                                    </td>
                                                </tr>
						<tr>
                                                     <td>Upload Feature Image:#1</td>
                                                     <td colspan="10">
                                                         <input name="uploadedfile_0" type="file" /><br />
                                                     </td>
                                                </tr>
                                                <tr>
                                                    <td>Feature Status#1</td>
                                                    <td colspan="10">
                                                        <select name="feature_status_0" id="feature_status_0">
                                                            <option value="1">Active</option>
                                                            <option value="0">InActive</option>
                                                        </select>
                                                    </td>
                                                </tr>
												<tr>
                                                    <td>Display On Empty</td>
                                                    <td colspan="10">
                                                        <input type="checkbox" name="show_if_empty_0" id="show_if_empty_0" checked="checked" value="1" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="11">
                                                        <div align="right">
                                                            <a href="javascript:void(0);" onclick="add_more_feature();" id="featurelink_0">Add More Features/</a>
							    <a href="javascript:void(0);" onclick="remove_feature_row();" id="remove_pivotlink_0">Remove Feature</a>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="9">&nbsp;</td>
                                                    <td>
                                                        <div align="center">
                                                            <input type="submit" name="save" value="Save" class="formbtn"/>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div align="center">
                                                            <input type="button" name="cancel" value="Cancel" class="formbtn" onclick="javascript:this.form.reset();"/>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                        <!-- main area  END -->
                    </tr>
                </table>
		</div>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>
