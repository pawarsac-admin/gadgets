<?xml version="1.0" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html" version="4.0" encoding="UTF-8" indent="yes"/>
    <xsl:template name="incHeader">
        <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js" type="text/javascript"></script>
	<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.15/jquery-ui.min.js" type="text/javascript"></script>
	<script>var admin_web_url = '<xsl:value-of select="/XML/ADMIN_WEB_URL" disable-output-escaping="yes"/>';</script>

	</head>
        <table width="100%" class="headerArea">
            <td align="left" valign="top"><a href="{/XML/WEB_URL}" class="logo spriteicon">buying guide</a> </td><td valign="bottom"><h3>Admin</h3></td>
            <td align="right" valign="top">
                <div align="right">
                </div>
            </td>
        </table>
    </xsl:template>
</xsl:stylesheet>
