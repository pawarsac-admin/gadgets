<?xml version="1.0" ?>
<!DOCTYPE xsl:stylesheet  [
  <!ENTITY nbsp   "&#160;">
  <!ENTITY copy   "&#169;">
  <!ENTITY reg    "&#174;">
  <!ENTITY trade  "&#8482;">
  <!ENTITY mdash  "&#8212;">
  <!ENTITY ldquo  "&#8220;">
  <!ENTITY rdquo  "&#8221;">
  <!ENTITY pound  "&#163;">
  <!ENTITY yen    "&#165;">
  <!ENTITY euro   "&#8364;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html" version="4.0" encoding="UTF-8" indent="yes"/>
    <xsl:template match="/">
        <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title>Home : Admin Menu Management</title>
                <link rel="stylesheet" type="text/css" href="{XML/WEB_URL}css/main.css" />
                <script LANGUAGE="JavaScript" SRC="{XML/ADMIN_JS_URL}header_common.js"></script>
                <script LANGUAGE="JavaScript" SRC="{XML/ADMIN_JS_URL}header_menu.js"></script>
		<script language="javascript" src="{/XML/JS_URL}jquery-1.4.2.min.js"/>
		<script>var admin_web_url = '<xsl:value-of select="/XML/ADMIN_WEB_URL"/>';</script>
            </head>
            <body id="result">
                <table align="center" width="100%" border="0" cellpadding="2" cellspacing="2">
                    <!-- call Header -->
                    <tr>
                        <!-- main area -->
                        <td>
                            <div align="center">
                                <table width="100%" border="0">
                                    <!--tr>
                                        <td align="left">
                                            <a href="{XML/ADMIN_WEB_URL}index.php">Main</a>
>  Menu Dashboard</td>
                                    </tr-->
                                </table>
                            </div>
                            <table width="100%" border="1" id="menu_dashboard">
                                <tr>
                                    <td colspan="6">
                                        <div align="center">
                                            <h3>Dashboard</h3>
                                        </div>
                                    </td>
                                </tr>
                                <xsl:if test="/XML/MSG!=''">
                                    <tr>
                                        <td colspan="6" bgcolor="#98AFC7">
                                            <div align="center">
                                                <xsl:value-of select="/XML/MSG" diseable-output-esacaping="yes"/>
                                            </div>
                                        </td>
                                    </tr>
                                </xsl:if>
                                <tr>
                                    <td colspan="6">
                                        <div align="right">pagination goes here</div>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15%">Sr no.</td>
                                    <td width="37%">Menu Name</td>
                                    <td width="12%">Status</td>
                                    <td width="12%">Create Date</td>
                                    <td colspan="2">Action</td>
                                </tr>
                                <xsl:choose>
                                    <xsl:when test="/XML/HEADER_MENU_MASTER/COUNT&lt;=0">
                                        <tr>
                                            <td colspan="6">
                                                <div align="center">Zero Result Found.</div>
                                            </td>
                                        </tr>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:for-each select="/XML/HEADER_MENU_MASTER/HEADER_MENU_MASTER_DATA">
                                            <tr>
                                                <td>
                                                    <xsl:value-of select="position()" diseable-output-esacaping="yes"/>
                                                </td>
                                                <td>
                                                    <xsl:value-of select="MENU_NAME" diseable-output-esacaping="yes"/>
                                                </td>
                                                <td>
                                                    <xsl:value-of select="MENU_STATUS" diseable-output-esacaping="yes"/>
                                                </td>
                                                <td>
                                                    <xsl:value-of select="CREATE_DATE" diseable-output-esacaping="yes"/>
                                                </td>
                                                <td width="7%">
                                                    <a href="#Update" onclick="updateMenu('{MENU_ID}','{JS_MENU_NAME}','{JS_MENU_PAGE}','{JS_TITLE}','{STATUS}','{MENU_LEVEL}');">Update</a>
                                                </td>
                                                <td width="17%">
                                                    <a href="javascript:undefined;" onclick="deleteMenu('{MENU_ID}','{JS_MENU_NAME}');">Delete</a>
                                                </td>
                                            </tr>
                                        </xsl:for-each>
                                    </xsl:otherwise>
                                </xsl:choose>
                                <tr>
                                    <td colspan="6">
                                        <div align="right">pagination goes here</div>
                                    </td>
                                </tr>
                            </table>
                            <table width="100%" border="0">
                                <tr>
                                    <td colspan="6" width="100%">
                                        <h3>Add a new header menu</h3>
                                    </td>
                                </tr>
                            </table>
                            <form action="{/XML/ADMIN_WEB_URL}header_menu.php" method="post" name="menu_action" id="menu_action" onsubmit="return validateMenu();">
                                <table width="100%" id="show_menus">
                                    <tr>
                                        <td>
                                            <div id="menuajaxloader" style="display:none;">
                                                <div align="center">
                                                    <img src="{/XML/IMAGE_URL}ajax-loader.gif"/>
                                                </div>
                                            </div>
                                            <div id="menu_ajax1" style="display:none;"></div>
                                            <xsl:if test="/XML/HEADER_MENU_MASTER/COUNT&gt;0">
                                                <div align="right">
                                                    <input type="button" name="select_menu" id="select_menu" value="Select Menu"/>
                                                </div>
                                            </xsl:if>
                                        </td>
                                    </tr>
                                </table>
                                <table width="100%" id="Update" border="1">
                                    <tr>
                                        <td width="13%">Menu name</td>
                                        <td colspan="2">
                                            <input type="text" name="menu_name" id="menu_name"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="13%">Menu Page</td>
                                        <td colspan="2">
                                            <input type="text" name="menu_page" id="menu_page"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="13%">Menu Title</td>
                                        <td colspan="2">
                                            <input type="text" name="menu_title" id="menu_title"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Status</td>
                                        <td colspan="2">
                                            <select id="menu_status" name="menu_status">
                                                <option value="1">Active</option>
                                                <option value="0">InActive</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input type="hidden" name="menu_id" id="menu_id" value=""/>
                                            <input type="hidden" name="category_id" id="category_id" value="{/XML/CATEGORY_ID}"/>
                                        </td>
                                        <td colspan="2">
                                            <input type="hidden" name="actiontype" id="actiontype" value="insert"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input type="hidden" name="startlimit" id="startlimit" value="{/XML/STARTLIMIT}"/>
                                        </td>
                                        <td colspan="2">
                                            <input type="hidden" name="cnt" id="cnt" value="{/XML/CNT}"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="26">&nbsp;</td>
                                        <td width="7%">
                                            <input type="submit" name="menusubmit" id="menusubmit" value="Add/Update"/>
                                        </td>
                                        <td width="80%">
                                            <input type="button" name="cancel" id="cancel" value="Cancel" onclick="javascript:this.form.reset();"/>
                                        </td>
                                    </tr>
                                </table>
                            </form>
                        </td>
                        <!-- main area  END -->
                    </tr>
                </table>
		<script>
	     $(document).load(function() {
		//menu_details('<xsl:value-of select="/XML/SELECTED_MENU_ID" disable-output-escaping="yes"/>','menu_ajax1','ajaxloader');
             });
		</script>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>
