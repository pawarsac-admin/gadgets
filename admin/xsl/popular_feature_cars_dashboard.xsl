<?xml version="1.0" ?>
<!DOCTYPE xsl:stylesheet  [
  <!ENTITY nbsp   "&#160;">
  <!ENTITY copy   "&#169;">
  <!ENTITY reg    "&#174;">
  <!ENTITY trade  "&#8482;">
  <!ENTITY mdash  "&#8212;">
  <!ENTITY ldquo  "&#8220;">
  <!ENTITY rdquo  "&#8221;">
  <!ENTITY pound  "&#163;">
  <!ENTITY yen    "&#165;">
  <!ENTITY euro   "&#8364;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html" version="4.0" encoding="UTF-8" indent="yes"/>
    <xsl:include href="inc_header.xsl" /><!-- include header-->
    <xsl:include href="inc_footer.xsl" /><!-- include footer-->
    <xsl:template match="/">
        <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title>:: Ajax - Admin Popular Feature Cars Management ::</title>
                <link rel="stylesheet" type="text/css" href="{/XML/CSS_URL}main.css" />		       
                <script LANGUAGE="JavaScript" SRC="{XML/ADMIN_JS_URL}common.js"></script>
                <script LANGUAGE="JavaScript" SRC="{XML/ADMIN_JS_URL}category.js"></script>
                <script language="javascript" src="{XML/ADMIN_JS_URL}popular_feature_cars.js"></script>
                <script language="javascript" src="{XML/ADMIN_JS_URL}tiny_mce/tiny_mce.js"></script>
				
                <script>
		        var admin_web_url = '<xsl:value-of select="/XML/ADMIN_WEB_URL" disable-output-escaping="yes"/>';
                </script>
	   
            </head>
            <body>
	     <div id="soVideosDiv">			
                <table align="center" width="100%" border="0" cellpadding="2" cellspacing="2">
			<tr>
				<td>View By Pivot Group :<select name="pivot_group" id="pivot_group" onchange="javascript: fillPivots('pivot_group','pivot_id','{/XML/SELECTED_PIVOT_ID}');">
					<option value="0">---Select Pivot Group---</option>
						<xsl:for-each select="/XML/PIVOT_GROUP_MASTER/PIVOT_GROUP_MASTER_DATA">
							<xsl:choose>
								<xsl:when test="/XML/SELECTED_PIVOT_GROUP=PIVOT_GROUP">
									<option value="{PIVOT_GROUP}" selected='yes'>
										<xsl:value-of select="PIVOT_GROUP_NAME"/>
									</option>
								</xsl:when>
								<xsl:otherwise>
									<option value="{PIVOT_GROUP}">
										<xsl:value-of select="PIVOT_GROUP_NAME"/>
									</option>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:for-each>
					</select>
				</td>
			</tr>
			<tr>
                             <td>View By Pivot :<select name="pivot_id" id="pivot_id" onchange="javascript: getPopularFeatureCarsDashboard('prod_article_dashboard','productajaxloader','','{/XML/STARTLIMIT}','{/XML/CNT}','','');">
                                        <option value="0">---Select Pivot---</option>
					<xsl:for-each select="/XML/PIVOT_MASTER/PIVOT_MASTER_DATA">
                                                <xsl:choose>
                                                	<xsl:when test="/XML/SELECTED_PIVOT_ID=PIVOT_ID">
                                                                <option value="{PIVOT_ID}" selected='yes'>
                                                                	<xsl:value-of select="PIVOT_NAME"/>
                                                                </option>
                                                        </xsl:when>
                                                        <xsl:otherwise>
                                                                <option value="{PIVOT_ID}">
                                                                        <xsl:value-of select="PIVOT_NAME"/>
                                                        	</option>
                                                        </xsl:otherwise>
						</xsl:choose>
                                        </xsl:for-each>
                                        </select>
                                </td>
                        </tr>
                    <tr>
                        <td>
                            <div align="center" style="border: 1px solid #d5d5d5;">
                                <table width="100%" border="0" id="product_dashboard" colspacing="0" rowspacing="0">
                                    <tr style="border: 1px solid #d5d5d5;">
                                        <td colspan="18" style="border: 1px solid #d5d5d5;">
                                            <div align="center">
                                                <h3>Dashboard</h3>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr style="border: 1px solid #d5d5d5;">
                                        <td colspan="18" style="border: 1px solid #d5d5d5;">
                                            <div align="right"><xsl:value-of select="XML/Pages" disable-output-escaping="yes"/></div>
                                        </td>
                                    </tr>
                                    <tr class="row0">
                                        <td>Sr.No</td>
                                        <td>Category Name</td>
                                        <td>Pivot Name</td>
					<td>Product Name</td>
					<td>Status</td>
                                        <td>Create date</td>
				       <td colspan="4">Action</td>
                                    </tr>
                                    <xsl:choose>
                                        <xsl:when test="/XML/POPULAR_FEATURE_CARS_MASTER/COUNT&lt;=0">
                                            <tr>
                                                <td colspan="12">
                                                    <div align="center">Zero result found.</div>
                                                </td>
                                            </tr>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:for-each select="/XML/POPULAR_FEATURE_CARS_MASTER/POPULAR_FEATURE_CARS_MASTER_DATA">
												
                                                <tr class="row1">
                                                    <td>
                                                        <xsl:value-of select="position()" diseable-output-esacaping="yes"/>
                                                    </td>
                                                    <td>
                                                        <xsl:value-of select="CATEGORY_NAME" diseable-output-esacaping="yes"/>
                                                    </td>
                                                    <td>
                                                        <xsl:value-of select="PIVOT_NAME" diseable-output-esacaping="yes"/>
                                                    </td>
                                                    <td>
                                                        <xsl:value-of select="PRODUCT_NAME" diseable-output-esacaping="yes"/>
                                                    </td>
  						    <td>
                                                        <xsl:value-of select="STATUS" diseable-output-esacaping="yes"/>
                                                    </td>
						    <td>
                                                        <xsl:value-of select="CREATE_DATE" diseable-output-esacaping="yes"/>
                                                    </td>
                                                    <td>
                                                        <a href="#Update" id="updateMe" onclick="updatePopularFeatureCars('prod_article_dashboard','productajaxloader','{POPULAR_FEATURE_ID}','{CATEGORY_ID}','{/XML/SELECTED_PIVOT_GROUP}','{/XML/SELECTED_PIVOT_ID}','{PRODUCT_ID}');">Update</a>
                                                    </td>
                                                    <td>
                                                        <a href="javascript:undefined;" onclick="deletePopularBrand('{POPULAR_FEATURE_ID}','{/XML/SELECTED_PIVOT_GROUP}','{/XML/SELECTED_PIVOT_ID}');">Delete</a>
                                                    </td>
                                                </tr>
                                            </xsl:for-each>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                    <tr style="border: 1px solid #d5d5d5;">
                                        <td colspan="18" style="border: 1px solid #d5d5d5;">
                                            <div align="right"><xsl:value-of select="XML/Pages" disable-output-escaping="yes"/></div>
                                        </td>
                                    </tr>
                                </table>
                                <!--start code to add product form -->
                                <form enctype="multipart/form-data" method="post" action="{XML/ADMIN_WEB_URL}popular_feature_cars.php" name="product_manage" id="product_manage" onsubmit="return validateProduct();">
                                    <table width="100%" border="0" id="add_product_table">                                        
                                        <tr >
                                            <td align="right">                                                
                                                <table width="100%" border="0">
                                                    <tr class="row1">
                                                        <td>
                                                            <input type="hidden" name="startlimit" id="startlimit" value="{/XML/STARTLIMIT}"/>
                                                        </td>
                                                        <td>
                                                            <input type="hidden" name="cnt" id="cnt" value="{/XML/CNT}"/>
                                                        </td>
                                                        <td>
                                                            <input type="hidden" name="actiontype" id="actiontype" value="{/XML/SELECTED_ACTION_TYPE}"/>
							    <input type="hidden" name="popular_feature_id" id="popular_feature_id" value="{/XML/POPULAR_FEATURE_ID}"/>
                                                        </td>
                                                        
                                                        <td>
							    <input type="hidden" name="selected_pivot_group" id="selected_pivot_group" value="{/XML/SELECTED_PIVOT_GROUP}"/>
							    <input type="hidden" name="selected_pivot_id" id="selected_pivot_id" value="{/XML/SELECTED_PIVOT_id}"/>
							</td>
                                                        <td colspan="2"></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                </table>
                                                <table width="100%" id="Update" border="0" class="row1" style="border:1px solid #d4d4d4;">
						<tbody id="slideshowtbody">
                                                    <tr class="datarow1">
                                                        <td>Pivot Group Name</td>
                                                        <td colspan="10">
                                                            <select name="select_pivot_group" id="select_pivot_group" onchange="javascript: fillPivots('select_pivot_group','select_pivot_id','');">
                                                                <option value="0">---Select Pivot Group---</option>
								<xsl:for-each select="/XML/PIVOT_GROUP_MASTER/PIVOT_GROUP_MASTER_DATA">
								 <xsl:choose>
									<xsl:when test="/XML/SELECTED_PIVOT_GROUP=PIVOT_GROUP">
										<option value="{PIVOT_GROUP}" selected='yes'>
											<xsl:value-of select="PIVOT_GROUP_NAME"/>
										</option>
									</xsl:when>
									<xsl:otherwise>
										<option value="{PIVOT_GROUP}">
											<xsl:value-of select="PIVOT_GROUP_NAME"/>
										</option>
									</xsl:otherwise>
								 </xsl:choose>
								</xsl:for-each>
                                                            </select>
                                                        </td>
                                                    </tr>
						    <tr class="datarow1">
                                                        <td>Pivot Name</td>
                                                        <td colspan="10">
                                                            <select name="select_pivot_id" id="select_pivot_id" onchange="javascript: fillPoductName('select_pivot_group','select_pivot_id','select_product_id');">
                                                                <option value="0">---Select Pivot---</option>
								 <xsl:for-each select="/XML/PIVOT_MASTER/PIVOT_MASTER_DATA">
                                                		 <xsl:choose>
		                                                        <xsl:when test="/XML/SELECTED_PIVOT_ID=PIVOT_ID">
                		                                                <option value="{PIVOT_ID}" selected='yes'>
                                		                                        <xsl:value-of select="PIVOT_NAME"/>
                                                		                </option>
		                                                        </xsl:when>
                		                                        <xsl:otherwise>
                                		                                <option value="{PIVOT_ID}">
                                                		                        <xsl:value-of select="PIVOT_NAME"/>
                                                                		</option>
		                                                        </xsl:otherwise>
                		                                </xsl:choose>
	                                		        </xsl:for-each>
                                                            </select>
                                                        </td>
                                                    </tr>
						    <tr class="datarow1">
                                                        <td>Product Name</td>
                                                        <td colspan="10">
                                                            <select name="select_product_id[]" id="select_product_id" multiple="multiple" SIZE='3'>
                                                                <option value="0">---Select Product---</option>
                                                                <xsl:for-each select="/XML/PRODUCT_MASTER/PRODUCT_MASTER_DATA">
										<xsl:if test="PRODUCT_NAME!=''">
                                                                 <xsl:choose>
                                                                        <xsl:when test="/XML/SELECTED_PRODUCT_ID=PRODUCT_ID">

                                                                                <option value="{PRODUCT_ID}" selected='yes'>
                                                                                        <xsl:value-of select="PRODUCT_NAME"/>
                                                                                </option>

                                                                        </xsl:when>
                                                                        <xsl:otherwise>
										
                                                                                <option value="{PRODUCT_ID}">
                                                                                        <xsl:value-of select="PRODUCT_NAME"/>
                                                                                </option>
                                                                        </xsl:otherwise>
                                                                 </xsl:choose>
										</xsl:if>
                                                                </xsl:for-each>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                <tr class="datarow1">
                                                        <td>Status</td>
                                                        <td colspan="14">
							<xsl:if test="/XML/WALLCNT!=0">
								<input name="display_rows" id="display_rows" value="{/XML/WALLCNT}" type="hidden" />
							</xsl:if>
							<xsl:if test="/XML/WALLCNT=0">
								<input name="display_rows" id="display_rows" value="1" type="hidden" />
							</xsl:if>
                                                        <select name="status" id="status">
								<xsl:choose>
									<xsl:when test="/XML/POPULAR_FEATURE_CARS_DETAIL/POPULAR_FEATURE_CARS_DETAIL_DATA/STATUS='Active'">
										<option value="1" selected='yes'>Active</option>
									</xsl:when>
									<xsl:otherwise>
										<option value="1">Active</option>
									</xsl:otherwise>
								</xsl:choose>
								<xsl:choose>
									<xsl:when test="/XML/POPULAR_FEATURE_CARS_DETAIL/POPULAR_FEATURE_CARS_DETAIL_DATA/STATUS='InActive'">
										<option value="0" selected='yes'>InActive</option>
									</xsl:when>
									<xsl:otherwise>
										<option value="0">InActive</option>
									</xsl:otherwise>
								</xsl:choose>
							</select>
                                                        </td>
                                                    </tr>
						<tr>						
							<td colspan="11">
								<input type="hidden" name="selected_category_id" id="selected_category_id" value="{/XML/SELECTED_CATEGORY_ID}"/>
							</td>
						</tr>
						<tr>
							<td colspan="9">&nbsp;</td>
                                                        <td>
                                                            <div align="center">
                                                                <input type="submit" name="save" value="Save" class="formbtn"/>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div align="center">
                                                                <input type="button" name="cancel" value="Cancel" class="formbtn" onclick="javascript:this.form.reset();"/>
                                                            </div>
                                                        </td>
                                                    </tr>
						</tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </form>
                                <!--end code to add product form -->
                            </div>
                        </td>
                        <!-- main area  END -->
                    </tr>
                </table>
		</div>
            </body>
	    </html>
    </xsl:template>
</xsl:stylesheet>
