<?xml version="1.0" ?>
<!DOCTYPE xsl:stylesheet  [
  <!ENTITY nbsp   "&#160;">
  <!ENTITY copy   "&#169;">
  <!ENTITY reg    "&#174;">
  <!ENTITY trade  "&#8482;">
  <!ENTITY mdash  "&#8212;">
  <!ENTITY ldquo  "&#8220;">
  <!ENTITY rdquo  "&#8221;">
  <!ENTITY pound  "&#163;">
  <!ENTITY yen    "&#165;">
  <!ENTITY euro   "&#8364;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html" version="4.0" encoding="UTF-8" indent="yes"/>
    <xsl:include href="inc_header.xsl" /><!-- include header-->
    <xsl:include href="inc_footer.xsl" /><!-- include footer-->
    <xsl:template match="/">
        <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title>:: Ajax - Admin  Dashboard Management ::</title>
				<link rel="stylesheet" type="text/css" href="{/XML/CSS_URL}main.css" />
		        <script>
					var admin_web_url = '<xsl:value-of select="/XML/ADMIN_WEB_URL" disable-output-escaping="yes"/>';
				</script>
	        </head>
            <body>
	    <div id="sDealerDetailsOverDiv">
                <table align="center" width="100%" border="0" cellpadding="2" cellspacing="2">
                    <tr>
                        <td>
                            <div align="center">
                                <table width="100%" border="1" id="product_dashboard">
                                    <tr>
                                        <td colspan="18">
                                            <div align="center">
                                                <h3>Dashboard</h3>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="18">
                                            <div align="right"><xsl:value-of select="XML/Pages" disable-output-escaping="yes"/></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Sr.No</td>
                                        <td>Brand Name</td>
					<td>Dealer Name</td>
					<td>City Name</td>
					<td>Location</td>
                                        <td>Status</td>
                                        <td>create date</td>
										<td colspan="4">Action</td>
                                    </tr>
                                    <xsl:choose>
                                        <xsl:when test="/XML/DEALER_MASTER/COUNT&lt;=0">
                                            <tr>
                                                <td colspan="12">
                                                    <div align="center">Zero result found.</div>
                                                </td>
                                            </tr>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:for-each select="/XML/DEALER_MASTER/DEALER_MASTER_DATA">
                                                <tr>
                                                    <td>
                                                        <xsl:value-of select="position()" diseable-output-esacaping="yes"/>
                                                    </td>
						    <td>
                                                        <xsl:value-of select="BRAND_NAME" diseable-output-esacaping="yes"/>
                                                    </td>
						    
                                                    <td>
                                                        <xsl:value-of select="DEALER_NAME" diseable-output-esacaping="yes"/>
                                                    </td>
						    <td>
                                                        <xsl:value-of select="CITY_NAME" diseable-output-esacaping="yes"/>
                                                    </td>
							 <td>
                                                        <xsl:value-of select="LOCATION" diseable-output-esacaping="yes"/>
                                                    </td>
													<td>
                                                        <xsl:value-of select="STATUS" diseable-output-esacaping="yes"/>
                                                    </td>
													<td>
                                                        <xsl:value-of select="CREATE_DATE" diseable-output-esacaping="yes"/>
                                                    </td>
													<!--	
                                                    <td>Up</td>
                                                    <td colspan="2">Down</td>
													-->
                                                    <td>
                                                        <a href="#Update" id="updateMe" onclick="updateDealer('dealer_dashboard','productajaxloader','{DEALER_ID}','{CATEGORY_ID}','{BRAND_ID}','{PRODUCT_ID}','{COUNTRY_ID}','{STATE_ID}
','{CITY_ID}','','');">Update</a>
                                                    </td>
                                                    <td>
                                                        <a href="javascript:undefined;" onclick="deleteDealer('{DEALER_ID}','{PRODUCT_ID}');">Delete</a>
                                                    </td>
                                                </tr>
                                            </xsl:for-each>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                    <tr>
                                        <td colspan="18">
                                            <div align="right"><xsl:value-of select="XML/Pages" disable-output-escaping="yes"/></div>
                                        </td>
                                    </tr>
                                </table>
                                <!--start code to add dealer form -->
                                <form enctype="multipart/form-data" method="post" action="{XML/ADMIN_WEB_URL}dealer.php" name="dealer_manage" id="dealer_manage" onsubmit="return validateDealer();">
                                    <table width="100%" border="0" id="add_product_table">                                        
                                        <tr>
                                            <td align="right">                                                
                                                <table width="100%" border="0">
                                                    <tr>
<td>
<input type="hidden" name="startlimit" id="startlimit" value="{/XML/STARTLIMIT}"/>
</td>
<td>
<input type="hidden" name="cnt" id="cnt" value="{/XML/CNT}"/>
</td>
<td>
<input type="hidden" name="product_id" id="product_id" value="{XML/DEALER_DATA/PRODUCT_ID}"/>
<input type="hidden" name="dealer_product_id" id="dealer_product_id" value="{XML/DEALER_DATA/DEALER_PRODUCT_ID}"/>
<input type="hidden" name="dealer_id" id="dealer_id" value="{XML/DEALER_DATA/DEALER_ID}"/>
</td>
<td>
<input type="hidden" name="actiontype" id="actiontype" value="insert"/>
</td>
<td></td>
<td colspan="2"></td>
<td></td>
<td></td>
                                                    </tr>
                                                </table>
                                                <table width="100%" id="Update" border="1">
						<tr>
<td>Brand Name</td>
<td colspan="10">
<select name="select_brand_id" id="select_brand_id">
<option value="">---Select Brand---</option>
<xsl:for-each select="/XML/BRAND_MASTER/BRAND_MASTER_DATA">
<xsl:if test="/XML/DEALER_DATA/BRAND_ID=BRAND_ID">
<option value="{BRAND_ID}" selected='yes'>
<xsl:value-of select="BRAND_NAME"/>
</option>
</xsl:if>
<xsl:if test="not(/XML/DEALER_DATA/BRAND_ID=BRAND_ID)">
<option value="{BRAND_ID}">
<xsl:value-of select="BRAND_NAME"/>
</option>
</xsl:if>
</xsl:for-each>
</select>
<div id="ajaxloaderbrand" style="display:none;">
<div align="center">
<img src="{/XML/IMAGE_URL}ajax-loader.gif"/>
</div>
</div>
</td>
</tr>
<tr>
<td>Country Name</td>
<td colspan="10">
<select name="select_country_id" id="select_country_id" onchange="getStateByCountry('ajaxloadercountry','0');">
<option value="">---Select Country---</option>
<xsl:for-each select="/XML/COUNTRY_MASTER/COUNTRY_MASTER_DATA">
<xsl:if test="/XML/DEALER_DATA/COUNTRY_ID=COUNTRY_ID">
<option value="{COUNTRY_ID}" selected='yes'>
<xsl:value-of select="COUNTRY_NAME"/>
</option>
</xsl:if>
<xsl:if test="not(/XML/DEALER_DATA/COUNTRY_ID=COUNTRY_ID)">
<option value="{COUNTRY_ID}">
<xsl:value-of select="COUNTRY_NAME"/>
</option>
</xsl:if>
</xsl:for-each>
</select>
<div id="ajaxloadercountry" style="display:none;">
<div align="center">
<img src="{/XML/IMAGE_URL}ajax-loader.gif"/>
</div>
</div>
</td>
</tr>
<tr>
<td>Dealer Name</td>
<td colspan="10">
<input type="text" name="dealer_name" id="dealer_name" size="40" value="{XML/DEALER_DATA/DEALER_NAME}"/>
</td>
</tr>
<tr>
<td>Dealer Address</td>
<td colspan="10">
<textarea name="dealer_address" id="dealer_address" cols="50" rows="2">
<xsl:value-of select="XML/DEALER_DATA/DEALER_ADDRESS"/>
</textarea>
</td>
</tr>
<tr>
<td>Dealer Mobile Address</td>
<td colspan="10">
<textarea name="mobile_address" id="mobile_address" cols="50" rows="2">
<xsl:value-of select="XML/DEALER_DATA/MOBILE_ADDRESS"/>
</textarea>
</td>
</tr>
<tr>
<td>Location</td>
<td colspan="10">
<input type="text" name="location" id="location" size="40" value="{XML/DEALER_DATA/LOCATION}"/>
</td>
</tr>
<tr>
<td>Pin Code</td>
<td colspan="10">
<input type="text" name="pin_code" id="pin_code" size="40" value="{XML/DEALER_DATA/PIN_CODE}"/>
</td>
</tr>
<tr>
<td>Contact No.</td>
<td colspan="10">
<input type="text" name="contact_number" id="contact_number" size="40" value="{XML/DEALER_DATA/CONTACTNO}"/>
</td>
</tr>
<tr>
<td>Fax No.</td>
<td colspan="10">
<input type="text" name="fax_number" id="fax_number" size="40" value="{XML/DEALER_DATA/FAX_NUMBER}"/>
</td>
</tr>
<tr>
<td>Email Address</td>
<td colspan="10">
<input type="text" name="email_address" id="email_address" size="40" value="{XML/DEALER_DATA/EMAIL}"/>
</td>
</tr>
<tr>
<td>Contact Person</td>
<td colspan="10">
<input type="text" name="contact_person" id="contact_person" size="40" value="{XML/DEALER_DATA/CONTACT_PERSON}"/>
</td>
</tr>
<tr>
<td>Website</td>
<td colspan="10">
<input type="text" name="website" id="website" size="40" value="{XML/DEALER_DATA/WEBSITE}"/>
</td>
</tr>
<tr>
<td>Discount Offer</td>
<td colspan="10">
<!--<input type="text" name="discount_offer" id="discount_offer" size="40" value="{XML/DEALER_DATA/DISCOUNT_OFFER}"/>-->
<textarea name="discount_offer" id="discount_offer" cols="50" rows="2">
<xsl:value-of select="XML/DEALER_DATA/DISCOUNT_OFFER"/>
</textarea>
</td>
</tr>
<tr>
<td>Status#1</td>
<td colspan="14">
<select name="dealer_status" id="dealer_status">
<xsl:choose>
<xsl:when test="XML/DEALER_DATA/STATUS='Active'">
<option value="1" selected='yes'>Active</option>
</xsl:when>
<xsl:otherwise>
<option value="1">Active</option>
</xsl:otherwise>
</xsl:choose>
<xsl:choose>
<xsl:when test="XML/DEALER_DATA/STATUS='InActive'">
<option value="0" selected='yes'>InActive</option>
</xsl:when>
<xsl:otherwise>
<option value="0">InActive</option>
</xsl:otherwise>
</xsl:choose>
</select>

</td>
</tr>
													<tr>						
                                                        <td colspan="11"><input type="hidden" name="selected_category_id" id="selected_category_id" value="{/XML/SELECTED_CATEGORY_ID}"/></td>
													</tr>
													<tr>
														<td colspan="9">&nbsp;</td>
                                                        <td>
                                                            <div align="center">
                                                                <input type="submit" name="save" value="Save" class="formbtn"/>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div align="center">
                                                                <input type="button" name="cancel" value="Cancel" class="formbtn" onclick="javascript:this.form.reset();"/>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </form>
                                <!--end code to add dealer form -->
                            </div>
                        </td>
                        <!-- main area  END -->
                    </tr>
                </table>
	      </div>
            </body>
	    </html>
    </xsl:template>
</xsl:stylesheet>
