<?xml version="1.0" ?>
<!DOCTYPE xsl:stylesheet  [
  <!ENTITY nbsp   "&#160;">
  <!ENTITY copy   "&#169;">
  <!ENTITY reg    "&#174;">
  <!ENTITY trade  "&#8482;">
  <!ENTITY mdash  "&#8212;">
  <!ENTITY ldquo  "&#8220;">
  <!ENTITY rdquo  "&#8221;">
  <!ENTITY pound  "&#163;">
  <!ENTITY yen    "&#165;">
  <!ENTITY euro   "&#8364;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html" version="4.0" encoding="UTF-8" indent="yes"/>
    <xsl:include href="inc_header.xsl" /><!-- include header-->
    <xsl:include href="inc_footer.xsl" /><!-- include footer-->
    <xsl:template match="/">
        <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title>:: Ajax - Admin Pivot Dashboard Management ::</title>
                <link rel="stylesheet" type="text/css" href="{/XML/CSS_URL}main.css" />
                <script>
		        var admin_web_url = '<xsl:value-of select="/XML/ADMIN_WEB_URL" disable-output-escaping="yes"/>';
		var pivotGroupArr = Array();
		var pivotGroupIdsArr = Array();
		<xsl:for-each select="/XML/PIVOT_GROUP/PIVOT_GROUP_DATA">
			pivotGroupIdsArr.push('<xsl:value-of select="SUB_GROUP_ID" disable-output-escaping="yes"/>');
			pivotGroupArr.push('<xsl:value-of select="SUB_GROUP_NAME" disable-output-escaping="yes"/>');
                </xsl:for-each>
                <xsl:for-each select="XML/PIVOT_UNIT/PIVOT_UNIT_DATA">
                    pivotUnitArr.push('<xsl:value-of select="UNIT_NAME" disable-output-escaping="yes"/>');
                    pivotUnitIdArr.push('<xsl:value-of select="UNIT_ID" disable-output-escaping="yes"/>');
                </xsl:for-each>
            </script>
            </head>
            <body>
	    <div id="sPivotOverDiv">
                <table align="center" width="100%" border="0" cellpadding="2" cellspacing="2">
                    <tr>
                        <td>
                            <div align="center">
                                <table width="100%" border="1" id="pivot_dashboard">
                                    <tr>
                                        <td colspan="12">
                                            <div align="center">
                                                <h3>Dashboard</h3>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="12">
                                            <div align="right"><xsl:value-of select="XML/Pages" disable-output-escaping="yes"/></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Sr.No</td>
                                        <td>Pivot name</td>
                                        <td>Category Name</td>
                                        <td>Pivot group</td>
                                        <td>Pivot Display type</td>
                                        <td>Status</td>
                                        <td>create date</td>
                                        <td colspan="3">&nbsp;</td>
                                        <td colspan="2">Action</td>
                                    </tr>
                                    <xsl:choose>
                                        <xsl:when test="/XML/PIVOT_MASTER/COUNT&lt;=0">
                                            <tr>
                                                <td colspan="12">
                                                    <div align="center">Zero result found.</div>
                                                </td>
                                            </tr>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:for-each select="/XML/PIVOT_MASTER/PIVOT_MASTER_DATA">
                                                <tr>
                                                    <td>
                                                        <xsl:value-of select="position()" diseable-output-esacaping="yes"/>
                                                    </td>
                                                    <td>
                                                        <xsl:value-of select="PIVOT_NAME" diseable-output-esacaping="yes"/>
                                                    </td>
                                                    <td>
                                                        <xsl:value-of select="CATEGORY_NAME" diseable-output-esacaping="yes"/>
                                                    </td>
                                                    <td>
                                                        <xsl:value-of select="PIVOT_GROUP_NAME" diseable-output-esacaping="yes"/>
                                                    </td>
                                                    <td>
                                                        <xsl:value-of select="PIVOT_DISPLAY_TYPE" diseable-output-esacaping="yes"/>
                                                    </td>
                                                    <td>
                                                        <xsl:value-of select="PIVOT_STATUS" diseable-output-esacaping="yes"/>
                                                    </td>
                                                    <td>
                                                        <xsl:value-of select="CREATE_DATE" diseable-output-esacaping="yes"/>
                                                    </td>
                                                    <td>Up</td>
                                                    <td colspan="2">Down</td>
                                                    <td>
                                                        <a href="#Update" onclick="updatePivot('{PIVOT_ID}','{FEATURE_ID}','{JS_PIVOT_GROUP}','{JS_PIVOT_DESC}','{PIVOT_DISPLAY_ID}','{STATUS}');">Update</a>
                                                    </td>
                                                    <td>
                                                        <a href="javascript:undefined;" onclick="deletePivot('{PIVOT_ID}','{JS_PIVOT_NAME}');">Delete</a>
                                                    </td>
                                                </tr>
                                            </xsl:for-each>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                    <tr>
                                        <td colspan="12">
                                            <div align="right"><xsl:value-of select="XML/Pages" disable-output-escaping="yes"/></div>
                                        </td>
                                    </tr>
                                </table>
                                <!--start code to add pivot form -->
                                <form method="post" enctype="multipart/form-data" action="{XML/ADMIN_WEB_URL}pivot.php" name="pivot_manage" id="pivot_manage" onsubmit="return validatePivot();">
                                    <table width="100%" border="0" id="add_pivot_table">                                        
                                        <tr>
                                            <td align="right">                                                
                                                <table width="100%" border="0">
                                                    <tr>
                                                        <td>
                                                            <input type="hidden" name="startlimit" id="startlimit" value="{/XML/STARTLIMIT}"/>
                                                        </td>
                                                        <td>
                                                            <input type="hidden" name="cnt" id="cnt" value="{/XML/CNT}"/>
                                                        </td>
                                                        <td>
                                                            <input type="hidden" name="pivot_id" id="pivot_id"/>
                                                        </td>
                                                        <td>
                                                            <input type="hidden" name="actiontype" id="actiontype" value="insert"/>
                                                        </td>
                                                        <td>
                                                            <input type="hidden" name="pivotboxcnt" id="pivotboxcnt" value="1"/>
                                                        </td>
                                                        <td>
                                                            <input type="hidden" name="rowCount" id="rowCount" value="6"/>
                                                        </td>
                                                        <td></td>
                                                        <td colspan="2"></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                </table>
                                                <table width="100%" id="Update" border="1">
                                                    <tr>
                                                        <td>Pivot Name#1</td>
                                                        <td colspan="10">
                                                            <select name="select_pivot_name_0" id="select_pivot_name_0">
                                                                <option value="">---Select Pivot---</option>
                                                            <xsl:for-each select="/XML/FEATURE_MASTER/FEATURE_MASTER_DATA">
                                                                <option value="{FEATURE_ID}">
                                                                    <xsl:value-of select="FEATURE_NAME"/>
                                                                </option>
                                                            </xsl:for-each>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Pivot Group#1</td>
                                                        <td colspan="10">
                                                            <select name="select_pivot_group_0" id="select_pivot_group_0">
                                                                <option value="">---Select Group---</option>
                                                                <xsl:for-each select="/XML/PIVOT_GROUP/PIVOT_GROUP_DATA">
                                                                    <option value="{SUB_GROUP_ID}">
                                                                        <xsl:value-of select="SUB_GROUP_NAME"/>
                                                                    </option>
                                                                </xsl:for-each>
                                                            </select>
                                                        </td>
                                                        <!--<td>&nbsp;</td>
                                                        <td>
                                                            <div align="center">OR</div>
                                                        </td>
                                                        <td colspan="7">
                                                            <input type="text" name="pivot_group_0" id="pivot_group_0" size="50"/>
                                                        </td>
							-->
                                                    </tr>
                                                    <tr>
                                                        <td>Pivot Description#1</td>
                                                        <td colspan="10">
                                                            <textarea name="pivot_description_0" cols="40" rows="5" id="pivot_description_0"></textarea>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Pivot Display Style#1</td>
                                                        <td colspan="10">
                                                            <select name="pivot_style_0" id="pivot_style_0" style="display:block;">
                                                                <option value="">---Select Style---</option>
                                                                <xsl:for-each select="/XML/PIVOT_DISPLAY_TYPES/PIVOT_DISPLAY_TYPES_DATA">
                                                                    <option value="{PIVOT_DISPLAY_ID}">
                                                                        <xsl:value-of select="PIVOT_DISPLAY_NAME"/>
                                                                    </option>
                                                                </xsl:for-each>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                        		<td>Upload Pivot Image:#1</td>
			                                        <td colspan="10">
                        			                        <input name="uploadedfile_0" type="file" /><br />
			                                        </td>
                        		            </tr>
                                                    <tr>
                                                        <td>Pivot Status#1</td>
                                                        <td colspan="10">
                                                            <select name="pivot_status_0" id="pivot_status_0">
                                                                <option value="1">Active</option>
                                                                <option value="0">InActive</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="11">
                                                            <div align="right">
                                                                <a href="javascript:void(0);" onclick="add_more_pivot();" id="pivotlink_0">Add More Pivots/</a>
                                                                <a href="javascript:void(0);" onclick="remove_pivot_row();" id="remove_pivotlink_0">Remove Pivot</a>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="8"><input type="hidden" name="selected_category_id" id="selected_category_id" value="{/XML/SELECTED_CATEGORY_ID}"/>
                                                        <input type="hidden" name="pivot_group_str" id="pivot_group_str" value="{/XML/PIVOT_GROUP_ARR_STR}"/>
                                                        <input type="hidden" name="pivot_subgroup_str" id="pivot_subgroup_str" value="{/XML/PIVOT_SUB_GROUP_ARR_STR}"/>
                                                        <input type="hidden" name="pivot_subgrp_id_str" id="pivot_subgrp_id_str" value="{/XML/PIVOT_SUB_GROUP_ID_ARR_STR}"/>
                                                        <input type="hidden" name="pivot_style_id_str" id="pivot_style_id_str" value="{/XML/PIVOT_STYLE_ID_ARR_STR}"/>
                                                        <input type="hidden" name="pivot_style_value_str" id="pivot_style_value_str" value="{/XML/PIVOT_STYLE_VALUE_ARR_STR}"/>
                                                        <input type="hidden" name="feature_name_str" id="feature_name_str" value="{/XML/FEATURE_NAME_ARR_STR}"/>
                                                        <input type="hidden" name="feature_id_str" id="feature_id_str" value="{/XML/FEATURE_IDS_ARR_STR}"/></td>
                                                        <td>
                                                            <div align="center">
                                                                <input type="submit" name="save" value="Save" class="formbtn"/>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div align="center">
                                                                <input type="button" name="cancel" value="Cancel" class="formbtn" onclick="javascript:this.form.reset();"/>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </form>
                                <!--end code to add pivot form -->
                            </div>
                        </td>
                        <!-- main area  END -->
                    </tr>
                </table>
		</div>
            </body>
	    

        </html>
    </xsl:template>
</xsl:stylesheet>
