<?xml version="1.0" ?>
<!DOCTYPE xsl:stylesheet  [
  <!ENTITY nbsp   "&#160;">
  <!ENTITY copy   "&#169;">
  <!ENTITY reg    "&#174;">
  <!ENTITY trade  "&#8482;">
  <!ENTITY mdash  "&#8212;">
  <!ENTITY ldquo  "&#8220;">
  <!ENTITY rdquo  "&#8221;">
  <!ENTITY pound  "&#163;">
  <!ENTITY yen    "&#165;">
  <!ENTITY euro   "&#8364;">
]>  
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html" version="4.0" encoding="UTF-8" indent="yes"/>
    <xsl:include href="../xsl/inc_header.xsl" /><!-- include header-->
    <xsl:include href="../xsl/inc_footer.xsl" /><!-- include footer-->
    <xsl:include href="../xsl/inc_leftnavigation.xsl" /><!-- include left navigation-->
    <xsl:include href="../xsl/inc_rightnavigation.xsl" /><!-- include right navigation-->
    <xsl:template match="/">
        <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title>: Admin User Review Approval</title>
                <link rel="stylesheet" type="text/css" href="{XML/WEB_URL}css/main.css" />
                
            </head>
            <body>
                        <form enctype="multipart/form-data" method="post" action="{XML/ADMIN_WEB_URL}user_review.php" name="user_review" id="user_review">
                        <table width="60%" border="1" id="review_details" align='center'>
                                <xsl:choose>
                                        <xsl:when test="/XML/USER_REVIEW_MASTER/COUNT&lt;=0">
                                                <tr><td>Zero result found.</td></tr>
                                        </xsl:when>
                                        <xsl:otherwise>
                                        <xsl:choose>
                                        <xsl:when test="/XML/USER_REVIEW_MASTER/USER_REVIEW_MASTER_DATA/BRAND_MODEL_NAME!=''">
                                        <tr><td colspan='2'><b><font size='5px'>
                                         <xsl:value-of select="/XML/USER_REVIEW_MASTER/USER_REVIEW_MASTER_DATA/BRAND_MODEL_NAME" disable-output-escaping="yes"/> Owner's Review
                                        </font></b></td></tr>
                                        </xsl:when>
                                        <xsl:otherwise></xsl:otherwise>
                                        </xsl:choose>
                                        <tr>
                                                <td width='20%'>Overall Rating:</td>
                                                <td>
                                                        <xsl:value-of select="/XML/OVERALL_AVG_HTML" disable-output-escaping="yes"/>
                                                </td>
                                        </tr>
                                        <xsl:choose>
                                            <xsl:when test="/XML/USER_REVIEW_MASTER/USER_REVIEW_MASTER_DATA/BRAND_MODEL_VARIANT_NAME!=''">
                                                <tr><td colspan='2'><font size='3px'>
                                              <b>My <xsl:value-of select="/XML/USER_REVIEW_MASTER/USER_REVIEW_MASTER_DATA/BRAND_MODEL_VARIANT_NAME" disable-output-escaping="yes"/></b></font>
                                                </td></tr>
                                            </xsl:when>
                                            <xsl:otherwise>
                                            </xsl:otherwise>
                                          </xsl:choose>
                                        <tr>
                                        <td>Title :</td>
                                        <td>
                                          <input type="text" name="title" id="title" value="{/XML/USER_REVIEW_MASTER/USER_REVIEW_MASTER_DATA/TITLE}" size="100"/>
                                        </td>
                                        </tr>
                                        <tr><td>
                                        <xsl:if test="/XML/USER_REVIEW_MASTER/USER_REVIEW_MASTER_DATA/USER_NAME!=''">
                                            <b>By <xsl:value-of select="/XML/USER_REVIEW_MASTER/USER_REVIEW_MASTER_DATA/USER_NAME" disable-output-escaping="yes"/></b>
                                          </xsl:if>
                                        </td><td>
                                          <xsl:if test="/XML/USER_REVIEW_MASTER/USER_REVIEW_MASTER_DATA/CREATE_DATE!=''">
                                                Posted on <xsl:value-of select="/XML/USER_REVIEW_MASTER/USER_REVIEW_MASTER_DATA/CREATE_DATE" disable-output-escaping="yes"/>
                                          </xsl:if>
                                        </td></tr>

                                        <xsl:for-each select="/XML/USER_REVIEW_ANSWER_MASTER/USER_REVIEW_ANSWER_MASTER_DATA">
                                                <tr>
                                                        <td><b><xsl:value-of select="QUENAME" disable-output-escaping="yes"/><span class="org">*</span> :</b></td>
                                                        <td><xsl:value-of select="GRADE" disable-output-escaping="yes"/></td>
                                                </tr>
                                                <xsl:for-each select="QUE_ANSWER_MASTER/QUE_ANSWER_MASTER_DATA">
                                                        <tr>
                                                                <td><xsl:value-of select="ANS" disable-output-escaping="yes"/> <span class="org">*</span> :</td>
                                                                <td><xsl:value-of select="SELECTED_ANSWER" disable-output-escaping="yes"/></td>
                                                        </tr>
                                                </xsl:for-each>
                                        </xsl:for-each>
                                        <xsl:if test="/XML/USER_REVIEW_MASTER/COUNT&gt;0">
                                        <tr><td colspan='2'><b>Car Details</b></td></tr>
                                        <tr>
                                        <td>Brand <span class="org">*</span> :</td>
                                        <td><xsl:value-of select="/XML/USER_REVIEW_MASTER/USER_REVIEW_MASTER_DATA/BRAND_NAME" disable-output-escaping="yes"/></td>
                                        </tr>
                                        <tr>
                                        <td>Model <span class="org">*</span> :</td>
                                        <td><xsl:value-of select="/XML/USER_REVIEW_MASTER/USER_REVIEW_MASTER_DATA/MODEL_NAME" disable-output-escaping="yes"/></td>
                                        </tr>
                                        <tr>
                                        <td>Variant <span class="org">*</span> :</td>
                                        <td><xsl:value-of select="/XML/USER_REVIEW_MASTER/USER_REVIEW_MASTER_DATA/VARIANT" disable-output-escaping="yes"/></td>
                                        </tr>
                                        <tr>
                                        <td>Running(KM) <span class="org">*</span> :</td>
                                        <td><xsl:value-of select="/XML/USER_REVIEW_MASTER/USER_REVIEW_MASTER_DATA/RUNNING" disable-output-escaping="yes"/></td>
                                        </tr>
                                        <tr>
                                        <td>Year of Manufacture <span class="org">*</span> :</td>
                                        <td><xsl:value-of select="/XML/USER_REVIEW_MASTER/USER_REVIEW_MASTER_DATA/YEAR_MANUFACTURE" disable-output-escaping="yes"/></td>
                                        </tr>
                                        <tr>
                                        <td>Color <span class="org">*</span> :</td>
                                        <td><xsl:value-of select="/XML/USER_REVIEW_MASTER/USER_REVIEW_MASTER_DATA/COLOR" disable-output-escaping="yes"/></td>
                                        </tr>
                                        </xsl:if>
                                        <input type="hidden" name="comment_cnt" id="comment_cnt" value="{/XML/USER_REVIEW_COMMENT_ANSWER_MASTER/COUNT}"/>
                                        <xsl:for-each select="/XML/USER_REVIEW_COMMENT_ANSWER_MASTER/USER_REVIEW_COMMENT_ANSWER_MASTER_DATA">
                                                <tr><td colspan='2'><b><xsl:value-of select="QUENAME" disable-output-escaping="yes"/></b></td></tr>
                                                <tr>
                                                        <input type="hidden" name="usr_review_ans_id_{position()}" id="usr_review_ans_id_{position()}" value="{USR_REVIEW_ANS_ID}"/>
                                                        <td colspan='2'>
                                                                <textarea name="comment_{position()}" class="w90 uniform" rows="3" cols="64"><xsl:value-of select="ANSWER" disable-output-escaping="yes"/></textarea>
                                                        </td>
                                                </tr>
                                        </xsl:for-each>
                                        </xsl:otherwise>
                                </xsl:choose>
                                <tr>
                                        <td>
                                        <b>Status : </b>
                                                <xsl:choose>
                                                        <xsl:when test="/XML/USER_REVIEW_MASTER/USER_REVIEW_MASTER_DATA/STATUS = 0">
                                                                Rejected
                                                        </xsl:when>
                                                        <xsl:otherwise>
                                                                Approved
                                                        </xsl:otherwise>
                                                </xsl:choose>
                                        </td>
                                        <td align='right'>
                                                <xsl:choose>
                                                        <xsl:when test="/XML/USER_REVIEW_MASTER/USER_REVIEW_MASTER_DATA/STATUS = 0">
                                                                <input type='submit' name='submit' value='Approve'/>
                                                        </xsl:when>
                                                        <xsl:otherwise>
                                                                <input type='submit' name='submit' value='Reject' />
                                                        </xsl:otherwise>
                                                </xsl:choose>
                                                <input type='hidden' name='user_review_id' id='user_review_id' value='{/XML/USER_REVIEW_ID}'/>
                                        </td>
                                </tr>
                        </table>
                        </form>
            </body>
            <script LANGUAGE="JavaScript" SRC="{XML/ADMIN_JS_URL}common.js"></script>
            <script LANGUAGE="JavaScript" SRC="{XML/ADMIN_JS_URL}category.js"></script>
            <script language="javascript" src="{XML/ADMIN_JS_URL}user_review.js"></script>
        </html>
    </xsl:template>
</xsl:stylesheet>
