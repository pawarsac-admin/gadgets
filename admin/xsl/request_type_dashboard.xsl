<?xml version="1.0" ?>
<!DOCTYPE xsl:stylesheet  [
  <!ENTITY nbsp   "&#160;">
  <!ENTITY copy   "&#169;">
  <!ENTITY reg    "&#174;">
  <!ENTITY trade  "&#8482;">
  <!ENTITY mdash  "&#8212;">
  <!ENTITY ldquo  "&#8220;">
  <!ENTITY rdquo  "&#8221;">
  <!ENTITY pound  "&#163;">
  <!ENTITY yen    "&#165;">
  <!ENTITY euro   "&#8364;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html" version="4.0" encoding="UTF-8" indent="yes"/>
    <xsl:include href="inc_header.xsl" /><!-- include header-->
    <xsl:include href="inc_footer.xsl" /><!-- include footer-->
    <xsl:template match="/">
        <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title>:: Ajax - Admin Request Type Dashboard Management ::</title>
                <link rel="stylesheet" type="text/css" href="{/XML/CSS_URL}main.css" />		       
                <script LANGUAGE="JavaScript" SRC="{XML/ADMIN_JS_URL}common.js"></script>
                <script LANGUAGE="JavaScript" SRC="{XML/ADMIN_JS_URL}category.js"></script>
                <script language="javascript" src="{XML/ADMIN_JS_URL}request_type.js"></script>
                 <script language="javascript" src="{XML/ADMIN_JS_URL}tiny_mce/tiny_mce.js"></script>
				
                <script>
                  
		        var admin_web_url = '<xsl:value-of select="/XML/ADMIN_WEB_URL" disable-output-escaping="yes"/>';
              </script>
	   
            </head>
            <body>
				
                <table align="center" width="100%" border="0" cellpadding="2" cellspacing="2">
                    <tr>
                        <td>
                            <div align="center" style="border: 1px solid #d5d5d5;">
                                <table width="100%" border="0" id="product_dashboard" colspacing="0" rowspacing="0">
                                    <tr style="border: 1px solid #d5d5d5;">
                                        <td colspan="18" style="border: 1px solid #d5d5d5;">
                                            <div align="center">
                                                <h3>Dashboard</h3>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr style="border: 1px solid #d5d5d5;">
                                        <td colspan="18" style="border: 1px solid #d5d5d5;">
                                            <div align="right">pagination goes here</div>
                                        </td>
                                    </tr>
                                    <tr class="row0">
                                        <td>Sr.No</td>
                                        <td>Request Type Id</td>
                                        <td>Request Type</td>
                                        <td>Status</td>
					<td>Create Date</td>
				       <td colspan="4">Action</td>
                                    </tr>
                                    <xsl:choose>
                                        <xsl:when test="/XML/REQUEST_TYPE_MASTER/COUNT&lt;=0">
                                            <tr>
                                                <td colspan="12">
                                                    <div align="center">No result found.</div>
                                                </td>
                                            </tr>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:for-each select="/XML/REQUEST_TYPE_MASTER/REQUEST_TYPE_MASTER_DATA">
												
                                                <tr class="row1">
                                                    <td>
                                                        <xsl:value-of select="position()" diseable-output-esacaping="yes"/>
                                                    </td>
                                                    <td>
                                                        <xsl:value-of select="REQUEST_TYPE_ID" diseable-output-esacaping="yes"/>
                                                    </td>
						    
                                                    <td>
                                                        <xsl:value-of select="REQUEST_TYPE" diseable-output-esacaping="yes"/>
                                                    </td>
                                                    <td>
                                                        <xsl:value-of select="STATUS" diseable-output-esacaping="yes"/>
                                                    </td>
                                                    <td>
                                                        <xsl:value-of select="CREATE_DATE" diseable-output-esacaping="yes"/>
                                                    </td>
                                                    <td>
                                                    	<a href="#Update" id="updateMe" onclick="updateRequestType('prod_article_dashboard','productajaxloader','{REQUEST_TYPE_ID}','{/XML/SELECTED_CATEGORY_ID}');">Update</a>
                                                    </td>
                                                    <td>
                                                        <a href="javascript:undefined;" onclick="deleteRequestType('{REQUEST_TYPE_ID}');">Delete</a>
                                                    </td>
                                                </tr>
                                            </xsl:for-each>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                    <tr style="border: 1px solid #d5d5d5;">
                                        <td colspan="18" style="border: 1px solid #d5d5d5;">
                                            <div align="right">pagination goes here</div>
                                        </td>
                                    </tr>
                                </table>
                                <!--start code to add product form -->
                                <form enctype="multipart/form-data" method="post" action="{XML/ADMIN_WEB_URL}request_type.php" name="product_manage" id="product_manage" onsubmit="return validateProduct();">
                                    <table width="100%" border="0" id="add_product_table">                                        
                                        <tr >
                                            <td align="right">                                                
                                                <table width="100%" border="0">
                                                    <tr class="row1">
                                                        <td>
                                                            <input type="hidden" name="startlimit" id="startlimit" value="{/XML/STARTLIMIT}"/>
                                                        </td>
                                                        <td>
                                                            <input type="hidden" name="cnt" id="cnt" value="{/XML/CNT}"/>
                                                        </td>
                                                        <td>
                                                            <input type="hidden" name="actiontype" id="actiontype" value="{/XML/SELECTED_ACTION_TYPE}"/>
							    <input type="hidden" name="request_type_id" id="request_type_id" value="{/XML/REQUEST_TYPE_ID}"/>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table width="100%" id="Update" border="0" class="row1" style="border:1px solid #d4d4d4;">
							<tbody id="slideshowtbody">
								<xsl:if test="count(/XML/REQUEST_TYPE_DETAIL/REQUEST_TYPE_DETAIL_DATA)=0">
									<tr>
										<td> Request Type 1</td>		
										<td colspan='11'>
											<textarea name="request_type_1" id="request_type_1" cols="60" rows="2" ></textarea>
										</td>
									</tr>
								</xsl:if>
								<xsl:if test="count(/XML/REQUEST_TYPE_DETAIL/REQUEST_TYPE_DETAIL_DATA)>0">
									<xsl:for-each select="/XML/REQUEST_TYPE_DETAIL/REQUEST_TYPE_DETAIL_DATA">
										<tr>
											<td> Subject <xsl:value-of select="position()"/></td>
											<td style="border:1px solid #d5d5d5;" colspan='11'>
												<textarea name="request_type_{position()}" id="request_type_{position()}" cols="60" rows="2" >
													<xsl:value-of select="REQUEST_TYPE"/>
												</textarea>
												<br/>
												
												<input type="hidden" name="request_type_id_{position()}" id="request_type_id_{position()}" value="{REQUEST_TYPE_ID}"/>
											</td>
										</tr>
									</xsl:for-each>
								</xsl:if>
								<tr id="trAddRemove">
									<td></td>
									<td colspan="0" align="right">
										<input id="add" value="Add More" onclick="addRemoveFileBrowseElements(1);" type="button" />&nbsp;
										<input id="remove" value="Remove" onclick="addRemoveFileBrowseElements(0);" type="button" />
									</td>
								</tr>
                                       				<tr>
									<td>Status#</td>
                                				        <td colspan="14">
										<xsl:if test="/XML/WALLCNT!=0">
										<input name="display_rows" id="display_rows" value="{/XML/WALLCNT}" type="hidden" />
										</xsl:if>
										<xsl:if test="/XML/WALLCNT=0">
											<input name="display_rows" id="display_rows" value="1" type="hidden" />
										</xsl:if>
                                                            			<select name="status" id="status">
										<xsl:choose>
											<xsl:when test="XML/REQUEST_TYPE_DETAIL/REQUEST_TYPE_DETAIL_DATA/STATUS='Active'">
											<option value="1" selected='yes'>Active</option>
											</xsl:when>
											<xsl:otherwise>
											<option value="1">Active</option>
											</xsl:otherwise>
										</xsl:choose>
										<xsl:choose>
											<xsl:when test="XML/REQUEST_TYPE_DETAIL/REQUEST_TYPE_DETAIL_DATA/STATUS='InActive'">
											<option value="0" selected='yes'>InActive</option>
											</xsl:when>
											<xsl:otherwise>
											<option value="0">InActive</option>
											</xsl:otherwise>
										</xsl:choose>
										</select>
									</td>
                                                		</tr>
								<tr>						
									<td colspan="11">
										<input type="hidden" name="selected_category_id" id="selected_category_id" value="{/XML/SELECTED_CATEGORY_ID}"/>
									</td>
								</tr>
								<tr>
									<td colspan="9">&nbsp;</td>
                                                        		<td>
					                        		<div align="center">
                                        					<input type="submit" name="save" value="Save" class="formbtn"/>
										</div>
                     					                </td>
		                                                        <td>
                        					                    <div align="center">
			                                                                <input type="button" name="cancel" value="Cancel" class="formbtn" onclick="javascript:this.form.reset();"/>
                                   					                    </div>
		                                                        </td>
                  					            </tr>
								</tbody>
	                                                </table>
               			                    </td>
	                                        </tr>
                      		            </table>
	                                </form>
                                <!--end code to add product form -->
                            </div>
                        </td>
                        <!-- main area  END -->
                    </tr>
                </table>
            </body>
	    </html>
    </xsl:template>
</xsl:stylesheet>
