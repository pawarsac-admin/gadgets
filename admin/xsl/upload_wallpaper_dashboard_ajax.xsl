<?xml version="1.0" ?>
<!DOCTYPE xsl:stylesheet  [
  <!ENTITY nbsp   "&#160;">
  <!ENTITY copy   "&#169;">
  <!ENTITY reg    "&#174;">
  <!ENTITY trade  "&#8482;">
  <!ENTITY mdash  "&#8212;">
  <!ENTITY ldquo  "&#8220;">
  <!ENTITY rdquo  "&#8221;">
  <!ENTITY pound  "&#163;">
  <!ENTITY yen    "&#165;">
  <!ENTITY euro   "&#8364;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html" version="4.0" encoding="UTF-8" indent="yes"/>
    <xsl:include href="inc_header.xsl" /><!-- include header-->
    <xsl:include href="inc_footer.xsl" /><!-- include footer-->
    <xsl:template match="/">
        <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title>:: Ajax - Admin Product Dashboard Management ::</title>
	            <link rel="stylesheet" type="text/css" href="{/XML/CSS_URL}main.css" />
	            <script>
		        var admin_web_url = '<xsl:value-of select="/XML/ADMIN_WEB_URL" disable-output-escaping="yes"/>';
				
				</script>
				<script language="javascript" src="{XML/ADMIN_JS_URL}upload_wallpaper.js"></script>
            </head>
            <body>
                <table align="center" width="100%" border="0" cellpadding="2" cellspacing="2">
                    <tr>
                        <td>
                            <div align="center">
								<table width="100%" border="1" id="product_dashboard">
									<tr>
										<td colspan="18">
										<div align="center">
										<h3>Dashboard</h3>
										</div>
										</td>
									</tr>
									<tr>
										<td colspan="18">
										<div align="right">pagination goes here</div>
										</td>
									</tr>
									<tr>
										<td>Sr.No</td>
										<!--<td>Article name</td>-->
										<td>Category Name</td>
										<td>Brand Name</td>
										<td>Product Name</td>
										<td>Status</td>
										<td>create date</td>
										<td colspan="4">Action</td>
									</tr>
									<xsl:choose>
									<xsl:when test="/XML/WALLPAPER_MASTER/COUNT&lt;=0">
									<tr>
										<td colspan="12">
										<div align="center">Zero result found.</div>
										</td>
									</tr>
									</xsl:when>
									<xsl:otherwise>
									<xsl:for-each select="/XML/PRODUCT_MASTER/PRODUCT_MASTER_DATA">
									<tr>
										<td>
										<xsl:value-of select="position()" diseable-output-esacaping="yes"/>
										</td>
										<td>
											<xsl:value-of select="CATEGORY_NAME" diseable-output-esacaping="yes"/>
										</td>
										<td>
											<xsl:value-of select="BRAND_NAME" diseable-output-esacaping="yes"/>
										</td>
										<td>
											<xsl:value-of select="PRODUCT_NAME" diseable-output-esacaping="yes"/>
										</td>
										<td>
											<xsl:value-of select="PRODUCT_STATUS" diseable-output-esacaping="yes"/>
										</td>
										<td>
											<xsl:value-of select="CREATE_DATE" diseable-output-esacaping="yes"/>
										</td>
										<!--	
										<td>Up</td>
										<td colspan="2">Down</td>
										-->
										<td>
											<a href="#Update" id="updateMe" onclick="updateWallPapers('prod_article_dashboard','productajaxloader','{WALLPAPER_ID}','{PRODUCT_ID}','{CATEGORY_ID}','{BRAND_ID}','','');">Update</a>
										</td>
										<td>
											<a href="javascript:undefined;" onclick="deleteWallPapers('{WALLPAPER_ID}');">Delete</a>
										</td>
									</tr>
									</xsl:for-each>
									</xsl:otherwise>
									</xsl:choose>
									<tr>
										<td colspan="18">
											<div align="right">pagination goes here</div>
										</td>
									</tr>
								</table>
                                <!--start code to add product form -->
						<form enctype="multipart/form-data" method="post" action="{XML/ADMIN_WEB_URL}upload_wallpaper.php" name="product_manage" id="product_manage" onsubmit="return validateProduct();">
							<table width="100%" border="0" id="add_product_table" >                                        
								<tr>
									<td align="right">                                                
										<table width="100%" border="0">
											<tr>
												<td>
													<input type="hidden" name="startlimit" id="startlimit" value="{/XML/STARTLIMIT}"/>
													<input type="hidden" name="cnt" id="cnt" value="{/XML/CNT}"/>
													<input type="hidden" name="product_id" id="product_id" value="{XML/WALLPAPER_DETAIL/WALLPAPER_DATA/PRODUCT_ID}"/>
													
													<input type="hidden" name="actiontype" id="actiontype" value="insert"/>
													<input type="hidden" name="featureboxcnt" id="featureboxcnt" value="{/XML/FEATURE_MASTER/COUNT}"/>
												</td>
											</tr>
										</table>	
										<table width="100%" id="Update" border="0" style="border:1px solid #d5d5d5;">
											<tbody id="slideshowtbody">
											<tr>
												<td style="border:1px solid #d5d5d5;">Brand Name</td>
												<td colspan="10" style="border:1px solid #d5d5d5;">
													<select name="select_brand_id" id="select_brand_id" onchange="getProductByBrand('ajaxloader','0');">
														<option value="">---Select Brand---</option>
														<xsl:for-each select="/XML/BRAND_MASTER/BRAND_MASTER_DATA">
															<xsl:if test="/XML/WALLPAPER_DETAIL/WALLPAPER_DATA/BRAND_ID=BRAND_ID">
																<option value="{BRAND_ID}" selected='yes'>
																	<xsl:value-of select="BRAND_NAME"/>
																</option>
															</xsl:if>
															<xsl:if test="not(/XML/WALLPAPER_DETAIL/WALLPAPER_DATA/BRAND_ID=BRAND_ID)">
																<option value="{BRAND_ID}">
																	<xsl:value-of select="BRAND_NAME"/>
																</option>
															</xsl:if>
														</xsl:for-each>
													</select>
													<div id="ajaxloader" style="display:none;">
														<div align="center">
															<img src="{/XML/IMAGE_URL}ajax-loader.gif"/>
														</div>
													</div>
												</td>
											</tr>
											<tr>
												<td style="border:1px solid #d5d5d5;">Type</td>
												<td colspan="10" style="border:1px solid #d5d5d5;">
													<select name="select_type_id" id="select_type_id">
														<xsl:if test="XML/WALLPAPER_DETAIL/WALLPAPER_DATA/TYPE='1'">
														<option value="1" selected='yes'>Wall Paper</option>
														<option value="2">Slide Show</option>
														</xsl:if>
														<xsl:if test="XML/WALLPAPER_DETAIL/WALLPAPER_DATA/TYPE='2'">
															<option value="1" >Wall Paper</option>
															<option value="2" selected='yes'>Slide Show</option>
														</xsl:if>
														<xsl:if test="not(XML/WALLPAPER_DETAIL/WALLPAPER_DATA/TYPE)">
															<option value="1">Wall Paper</option>
															<option value="2">Slide Show</option>
														</xsl:if>
													</select>
												</td>
											</tr>
											<xsl:if test="count(/XML/WALLPAPER_DETAIL/WALLPAPER_DATA)=0">
											<tr>
												<td style="border:1px solid #d5d5d5;">Upload Media:</td>
												<td colspan="10" style="border:1px solid #d5d5d5;">
													<input name="media_id_1" type="hidden" size="40" id="media_id_1" value="{XML/WALLPAPER_DATA/MEDIA_ID}"/>
													<input name="img_upload_1_1" type="hidden" size="40" id="img_upload_id_1_1" value="{XML/WALLPAPER_DATA/MEDIA_PATH}"/>
													<input type="text" name="title_upload_file_1" id="title_upload_file_1" value="{XML/WALLPAPER_DATA/MEDIA_PATH_TITLE}" />
													<input type="button" name="btn_get" id="btn_get" value="media upload" onclick="getUploadData('product_manage','title_upload_file_1','media_id_1','img_upload_id_1_1','image','main');"/>
													<input type="button" name="btn_search" id="btn_search" value="search" onclick="getUploadedDataList('product_manage','title_upload_file_1','media_id','img_upload_id_1_1','image','main');"/>	
												
												Upload Thumb:
												
													<input name="img_media_id_1" type="hidden" size="40" id="img_media_id_1" value="{XML/WALLPAPER_DATA/IMG_MEDIA_ID}"/>
													<input name="img_upload_id_thm_1" type="hidden" size="40" id="img_upload_id_thm_1" value="{XML/WALLPAPER_DATA/IMAGE_PATH}"/>
													<input type="text" name="thumb_title_1" id="thumb_title_1" value="{XML/WALLPAPER_DATA/IMAGE_PATH_TITLE}" /> 	
													<input type="button" name="btn_get" id="btn_get" value="image upload" onclick="getUploadData('product_manage','thumb_title_1','img_media_id_1','img_upload_id_thm_1','image','thumb');"/>
													<input type="button" name="btn_search" id="btn_search" value="search" onclick="getUploadedDataList('product_manage','thumb_title_1','img_media_id_1','img_upload_id_thm_1','image','thumb');"/>
															
												</td>
											</tr>
											</xsl:if>
											
											<xsl:if test="count(/XML/WALLPAPER_DETAIL/WALLPAPER_DATA)>0">
											<xsl:for-each select="/XML/WALLPAPER_DETAIL/WALLPAPER_DATA">
												<tr>
												<td style="border:1px solid #d5d5d5;">Upload Media:</td>
												<td colspan="10" style="border:1px solid #d5d5d5;">
													<input name="media_id_{position()}" type="hidden" size="40" id="media_id_{position()}" value="{MEDIA_ID}"/>
													<input name="img_upload_1_{position()}" type="hidden" size="40" id="img_upload_id_1_{position()}" value="{MEDIA_PATH}"/>
													<input type="text" name="title_upload_file_{position()}" id="title_upload_file_{position()}" value="{MEDIA_PATH}" />
													<input type="button" name="btn_get" id="btn_get" value="media upload" onclick="getUploadData('product_manage','title_upload_file_{position()}','media_id_{position()}','img_upload_id_1_{position()}','image','main');"/>
													<input type="button" name="btn_search" id="btn_search" value="search" onclick="getUploadedDataList('product_manage','title_upload_file_{position()}','media_id_{position()}','img_upload_id_1_{position()}','image','main');"/>	
												
													Upload Thumb:
												
													<input name="img_media_id_{position()}" type="hidden" size="40" id="img_media_id_{position()}" value="{IMG_MEDIA_ID}"/>
													<input name="img_upload_id_thm_{position()}" type="hidden" size="40" id="img_upload_id_thm_{position()}" value="{IMAGE_PATH}"/>
													<input type="text" name="thumb_title_{position()}" id="thumb_title_{position()}" value="{IMAGE_PATH}" /> 	
													<input type="button" name="btn_get" id="btn_get" value="image upload" onclick="getUploadData('product_manage','thumb_title_{position()}','img_media_id_{position()}','img_upload_id_thm_{position()}','image','thumb');"/>
													<input type="button" name="btn_search" id="btn_search" value="search" onclick="getUploadedDataList('product_manage','thumb_title_{position()}','img_media_id_{position()}','img_upload_id_thm_{position()}','image','thumb');"/>
													
													<input name="wallpaper_id_{position()}" id="wallpaper_id_{position()}" value="{WALLPAPER_ID}" type="hidden" />													
												</td>
											</tr>
											</xsl:for-each>
											</xsl:if>
											<tr>
												
											</tr>
											<tr id="trAddRemove">
											<td></td>
											<td colspan="0" align="right">
											<input id="add" value="Add More" onclick="addRemoveFileBrowseElements(1);" type="button" />&nbsp;
											<input id="remove" value="Remove" onclick="addRemoveFileBrowseElements(0);" type="button" />
											</td>
											</tr>
											<tr>
												<td style="border:1px solid #d5d5d5;">Status#1</td>
												<td colspan="14" style="border:1px solid #d5d5d5;">
													
													<xsl:if test="/XML/WALLCNT!=0">
													<input name="display_rows" id="display_rows" value="{/XML/WALLCNT}" type="hidden" />
													</xsl:if>
													<xsl:if test="/XML/WALLCNT=0">
													<input name="display_rows" id="display_rows" value="1" type="hidden" /> 
													</xsl:if>
													
													<select name="product_status" id="product_status">
													<xsl:if test="XML/WALLPAPER_DATA/WALLPAPER_DATA/PRODUCT_STATUS='Active'">
														<option value="1" selected='yes'>Active</option>
														<option value="0">InActive</option>
													</xsl:if>
													<xsl:if test="XML/WALLPAPER_DATA/WALLPAPER_DATA/PRODUCT_STATUS='InActive'">
														<option value="1" >Active</option>
														<option value="0" selected='yes'>InActive</option>
													</xsl:if>
													<xsl:if test="not(XML/WALLPAPER_DATA/WALLPAPER_DATA/PRODUCT_STATUS)">
														<option value="1" selected='yes'>Active</option>
														<option value="0" >InActive</option>
													</xsl:if>
												</select>
											</td>
										</tr>
										<tr>						
											<td colspan="11" style="border:1px solid #d5d5d5;"><input type="hidden" name="selected_category_id" id="selected_category_id" value="{/XML/SELECTED_CATEGORY_ID}"/></td>
										</tr>
										<tr>
											<td colspan="9" style="border:1px solid #d5d5d5;">&nbsp;</td>
											<td style="border:1px solid #d5d5d5;">
												<div align="center">
													<input type="submit" name="save" value="Save" class="formbtn"/>
												</div>
											</td>
											<td style="border:1px solid #d5d5d5;">
												<div align="center">
													<input type="button" name="cancel" value="Cancel" class="formbtn" onclick="javascript:this.form.reset();"/>
												</div>
											</td>
										</tr>
										</tbody>
									</table>
								</td>
							</tr>
						</table>
						</form>
                        <!--end code to add product form -->
                    </div>
                </td>
                <!-- main area  END -->
            </tr>
        </table>
    </body>
	</html>
    </xsl:template>
</xsl:stylesheet>