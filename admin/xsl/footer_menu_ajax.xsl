<?xml version="1.0" ?>
<!DOCTYPE xsl:stylesheet  [
  <!ENTITY nbsp   "&#160;">
  <!ENTITY copy   "&#169;">
  <!ENTITY reg    "&#174;">
  <!ENTITY trade  "&#8482;">
  <!ENTITY mdash  "&#8212;">
  <!ENTITY ldquo  "&#8220;">
  <!ENTITY rdquo  "&#8221;">
  <!ENTITY pound  "&#163;">
  <!ENTITY yen    "&#165;">
  <!ENTITY euro   "&#8364;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html" version="4.0" encoding="UTF-8" indent="yes"/>
    <xsl:include href="inc_header.xsl" /><!-- include header-->
    <xsl:include href="inc_footer.xsl" /><!-- include footer-->
    <xsl:template match="/">
        <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title>:: Ajax - Admin Menu Management ::</title>
                <link rel="stylesheet" type="text/css" href="{/XML/CSS_URL}main.css" />
                <SCRIPT LANGUAGE="JavaScript" SRC="{XML/ADMIN_JS_URL}footer_common.js"></SCRIPT>
            </head>
            <body>
                <table align="center" width="100%" border="0" cellpadding="2" cellspacing="2">
                    <tr>
                        <td>
                            <div align="center">
                                <xsl:if test="/XML/ROOT_LVL_MENU/COUNT&gt;0">
                                    <table width="100%" border="1" align='left'>
                                        <tr>
                                            <td>
                                            Your Selection :
                                                <xsl:value-of select="/XML/BREAD_CRUMB" disable-output-escaping="yes"/>
                                            </td>
                                        </tr>
                                    </table>
                                    <table width="100%" border="1" align='center'>
                                        <tr>
                                            <td>
                                                <select class='suggestClass' id='menuOptionsLevel_0' size='8' onchange="javascript:menu_level('menuOptionsLevel_0','{/XML/DIV_ID}','{/XML/AJAX_LOADER_ID}');">
                                                    <option value="">---------------- Select Menu ----------------</option>
                                                    <xsl:for-each select="/XML/ROOT_LVL_MENU/ROOT_LVL_DATA">
                                                        <xsl:choose>
                                                            <xsl:when test="SELECTED_MENU=MENU_ID">
                                                                <option value="{MENU_ID}" selected="selected">
                                                                    <xsl:value-of select="MENU_NAME" disable-output-escaping="yes"/>
                                                                </option>
                                                            </xsl:when>
                                                            <xsl:otherwise>
                                                                <option value="{MENU_ID}">
                                                                    <xsl:value-of select="MENU_NAME" disable-output-escaping="yes"/>
                                                                </option>
                                                            </xsl:otherwise>
                                                        </xsl:choose>
                                                    </xsl:for-each>
                                                </select>
                                            </td>
                                            <xsl:for-each select="/XML/MENU_LVL_SELECT/MENU_LVL_SELECT_BOX">
                                                <td>
                                                    <xsl:value-of select="." disable-output-escaping="yes"/>
                                                </td>
                                            </xsl:for-each>
                                            <!--start code to add hidden form element-->
                                            <tr>
                                                <td colspan="{/XML/TOTAL_CAT_BOX}">
                                                    <xsl:for-each select="/XML/MENU_LVL_SELECT/MENU_LVL_HIDDEN_DATA">
                                                        <xsl:value-of select="." disable-output-escaping="yes"/>
                                                    </xsl:for-each>
                                                    <!-- total menu tree fetched from database -->
                                                    <input type="hidden" name="menuboxcnt" id="menuboxcnt" value="{/XML/TOTAL_CAT_BOX}"/>
                                                    <!-- user selected menu box count -->
                                                    <input type="hidden" name="selectedboxcnt" id="selectedboxcnt" value="{/XML/SELECTED_MENU_BOX}"/>
                                                    <!-- use to get current selected menu id -->
                                                    <input type="hidden" name="selected_menu_id" id="selected_menu_id" value="{/XML/SELECTED_MENU_ID}"/>
                                                </td>
                                            </tr>
                                            <!-- start code to add hidden form element-->
                                        </tr>
                                    </table>
                                </xsl:if>
                            </div>
                        </td>
                        <!-- main area  END -->
                    </tr>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>
