<?xml version="1.0" ?>
<!DOCTYPE xsl:stylesheet  [
  <!ENTITY nbsp   "&#160;">
  <!ENTITY copy   "&#169;">
  <!ENTITY reg    "&#174;">
  <!ENTITY trade  "&#8482;">
  <!ENTITY mdash  "&#8212;">
  <!ENTITY ldquo  "&#8220;">
  <!ENTITY rdquo  "&#8221;">
  <!ENTITY pound  "&#163;">
  <!ENTITY yen    "&#165;">
  <!ENTITY euro   "&#8364;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:output method="html" version="4.0" encoding="UTF-8" indent="yes"/>
  <xsl:include href="inc_header.xsl" /><!-- include header-->
  <xsl:include href="inc_footer.xsl" /><!-- include footer-->
  <xsl:template match="/">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>:: Ajax - Admin Product Dashboard Management ::</title>
        <link rel="stylesheet" type="text/css" href="{/XML/CSS_URL}main.css" />
        <script>
		var admin_web_url = '<xsl:value-of select="/XML/ADMIN_WEB_URL" disable-output-escaping="yes"/>';
        </script>
      </head>
      <body>
        <table align="center" width="100%" border="0" cellpadding="2" cellspacing="2">
          <tr>
            <td>
              <div align="center">
				<div id="sProductDiv">
		<form enctype="multipart/form-data" method="post" action="{XML/ADMIN_WEB_URL}custom_top_products.php" name="product_manage_dashboard" id="product_manage_dashboard">
		<input type="hidden" id="grp_id" name="group_id" value="0" />
                <table width="100%" border="1" id="product_dashboard">
                  <tr>
                    <td colspan="18">
                      <div align="center">
                        <h3>Products</h3>
                      </div>
                    </td>
                  </tr>
		  <tr>
                    <td colspan="18" align="right">
                        <b>Search By:-</b>
                        <select name="select_brand" id="select_brand" onchange="getModelByBrandDashboard('','get_model_detail.php','Model','')">
                                <option value="">---All Brands---</option>
                                        <xsl:for-each select="/XML/BRAND_MASTER/BRAND_MASTER_DATA">
                                                <xsl:choose>
                                                        <xsl:when test="/XML/SELECTED_BRAND_ID=BRAND_ID">
                                                                <option value="{BRAND_ID}" selected='yes'>
                                                                        <xsl:value-of select="BRAND_NAME"/>
                                                                </option>
                                                        </xsl:when>
                                                        <xsl:otherwise>
                                                               <option value="{BRAND_ID}">
                                                                       <xsl:value-of select="BRAND_NAME"/>
                                                               </option>
                                                        </xsl:otherwise>
                                                 </xsl:choose>
                                          </xsl:for-each>
                          </select>
                          <select name="Model" id="Model" onchange="getVariantByBrandModelDashboard('','get_variants.php','Variant','')">
                                  <option value="">--All Models--</option>
                          </select>
                          <select name="Variant" id="Variant">
                                   <option value="">--All Variants--</option>
                          </select>
                          <input type="submit" value="Search" onclick="javascript:submitResearchForm();"/>
                    </td>
		  </tr>
                  <tr>
                    <td colspan="18">
                      <div align="right"><xsl:value-of select="XML/Pages" disable-output-escaping="yes"/></div>
                    </td>
                  </tr>
                  <tr>
                    <td>Sr.No</td>
					<td>Category Name</td>
                    <td>Brand Name</td>
                    <td>Product name</td>
                    <td>Variant</td>                    
                    <td>Status</td>
                    <td>create date</td>
                    <td colspan="4" align="center">Action</td>
                  </tr>
                  <xsl:choose>
                    <xsl:when test="/XML/PRODUCT_MASTER/COUNT&lt;=0">
                      <tr>
                        <td colspan="12">
                          <div align="center">Zero result found.</div>
                        </td>
                      </tr>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:for-each select="/XML/PRODUCT_MASTER/PRODUCT_MASTER_DATA">
                        <tr>
                          <td>
                            <xsl:value-of select="position()" diseable-output-esacaping="yes"/>
                          </td>
						  <td>
                            <xsl:value-of select="CATEGORY_NAME" diseable-output-esacaping="yes"/>
                          </td>
                          <td>
                            <xsl:value-of select="BRAND_NAME" diseable-output-esacaping="yes"/>
                          </td>
                          <td>
                            <xsl:value-of select="PRODUCT_NAME" diseable-output-esacaping="yes"/>
                          </td>
                          <td>
                            <xsl:value-of select="VARIANT" diseable-output-esacaping="yes"/>
                          </td>                          
                          <td>
                            <xsl:value-of select="PRODUCT_STATUS" diseable-output-esacaping="yes"/>
                          </td>
                          <td>
                            <xsl:value-of select="CREATE_DATE" diseable-output-esacaping="yes"/>
                          </td>
                          <td id="p_{position()}" colspan="2" align="center">
                            <a href="javascript:undefined;" onclick="addToListProduct('{PRODUCT_ID}','{BRAND_NAME} {JS_PRODUCT_NAME}-{VARIANT}',{position()});"><img id="img_{PRODUCT_ID}" src="{/XML/ADMIN_IMAGE_URL}add.png" border="0" alt="Add to list"/></a>
                          </td>
                        </tr>
                      </xsl:for-each>
                    </xsl:otherwise>
                  </xsl:choose>
                  <tr>
                    <td colspan="18">
                      <div align="right"><xsl:value-of select="XML/Pages" disable-output-escaping="yes"/></div>
                    </td>
                  </tr>
		  <tr>
                    <td colspan="18">
                      <input type="hidden" name="selected_category_id" id="selected_category_id" value="{/XML/SELECTED_CATEGORY_ID}"/>
                     </td>
                 </tr>
                </table>
		</form>
		</div>
               </div>
            </td>
                      <!-- main area  END -->
                    </tr>
        </table>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>