<?xml version="1.0" ?>
<!DOCTYPE xsl:stylesheet  [
  <!ENTITY nbsp   "&#160;">
  <!ENTITY copy   "&#169;">
  <!ENTITY reg    "&#174;">
  <!ENTITY trade  "&#8482;">
  <!ENTITY mdash  "&#8212;">
  <!ENTITY ldquo  "&#8220;">
  <!ENTITY rdquo  "&#8221;">
  <!ENTITY pound  "&#163;">
  <!ENTITY yen    "&#165;">
  <!ENTITY euro   "&#8364;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html" version="4.0" encoding="UTF-8" indent="yes"/>
    <xsl:include href="inc_header.xsl" /><!-- include header-->
    <xsl:include href="inc_footer.xsl" /><!-- include footer-->
    <xsl:template match="/">
        <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title>:: Ajax - Admin Feature Sub Group Management ::</title>
                <link rel="stylesheet" type="text/css" href="{/XML/CSS_URL}main.css" />
                <script LANGUAGE="JavaScript" SRC="{XML/ADMIN_JS_URL}common.js"></script>
                <script LANGUAGE="JavaScript" SRC="{XML/ADMIN_JS_URL}category.js"></script>
                <script LANGUAGE="JavaScript" SRC="{XML/ADMIN_JS_URL}feature_sub_group.js"></script>
            </head>
            <body>
					<form action="{/XML/ADMIN_WEB_URL}feature_sub_group.php" method="post" name="feature_group_action" id="feature_group_action" onsubmit="return validateFeatureGroup();">
                <table align="center" width="100%" border="0" cellpadding="2" cellspacing="2">
                    <tr>
                        <td>
                            <div align="center">
                                <table width="100%" border="1" id="feature_group_dashboard">
                                    <tr>
                                        <td colspan="7">
                                            <div align="center">
                                                <h3>Dashboard</h3>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="8">
                                            <div align="right">pagination goes here</div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="15%">Sr no.</td>
                                        <td width="37%">Feature Main Group name</td>
                                        <td width="37%">Feature Group name</td>
                                        <td width="37%">Category Name</td>
                                        <td width="12%">Status</td>
                                        <td width="12%">Create Date</td>
                                        <td colspan="2">Action</td>
                                    </tr>
                                    <xsl:choose>
                                        <xsl:when test="/XML/FEATURE_GROUP_MASTER/COUNT&lt;=0">
                                            <tr>
                                                <td colspan="8">
                                                    <div align="center">Zero result found.</div>
                                                </td>
                                            </tr>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:for-each select="/XML/FEATURE_GROUP_MASTER/FEATURE_GROUP_MASTER_DATA">
                                                <tr>
                                                    <td>
                                                        <xsl:value-of select="position()" diseable-output-esacaping="yes"/>
                                                    </td>
                                                    <td>
                                                        <xsl:value-of select="MAIN_GROUP_NAME" diseable-output-esacaping="yes"/>
                                                    </td>
                                                    <td>
                                                        <xsl:value-of select="SUB_GROUP_NAME" diseable-output-esacaping="yes"/>
                                                    </td>
                                                    <td>
                                                        <xsl:value-of select="CATEGORY_NAME" diseable-output-esacaping="yes"/>
                                                    </td>
                                                    <td>
                                                        <xsl:value-of select="SUB_GROUP_STATUS" diseable-output-esacaping="yes"/>
                                                    </td>
                                                    <td>
                                                        <xsl:value-of select="CREATE_DATE" diseable-output-esacaping="yes"/>
                                                    </td>
                                                    <td width="7%">
                                                        <a href="#Update" onclick="updateFeatureGroup('{SUB_GROUP_ID}','{JS_SUB_GROUP_NAME}','{MAIN_GROUP_ID}','{STATUS}');">Update</a>
                                                    </td>
                                                    <td width="17%">
                                                        <a href="javascript:undefined;" onclick="deleteFeatureGroup('{SUB_GROUP_ID}','{JS_SUB_GROUP_NAME}');">Delete</a>
                                                    </td>
                                                </tr>
                                            </xsl:for-each>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                    <tr>
                                        <td colspan="8">
                                            <div align="right">pagination goes here</div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                        <!-- main area  END -->
                    </tr>
                </table>
			
				<table width="100%" id="Update" border="1">
									<tr>
										<td width="13%">Select Main Group</td>
										<td>
											<select id="select_main_group" name="select_main_group">
												<option value="">---Select main group---</option>
												<xsl:for-each select="/XML/MAIN_GROUP_MASTER/MAIN_GROUP_MASTER_DATA">
													<xsl:choose>
													<xsl:when test="GROUP_ID=/XML/SELECT_MAIN_GROUP">
														<option value="{GROUP_ID}" selected="yes">
															<xsl:value-of select="MAIN_GROUP_NAME" disable-output-escaping="yes"/>
														</option>
													</xsl:when>
													<xsl:otherwise>
														<option value="{GROUP_ID}">
															<xsl:value-of select="MAIN_GROUP_NAME" disable-output-escaping="yes"/>
														</option>
													</xsl:otherwise>
													</xsl:choose>
													
													
												</xsl:for-each>
											</select>
										</td>
									</tr>
                                    <tr>
                                        <td width="13%">Feature Group Name</td>
                                        <td colspan="2">
                                            <input type="text" name="main_group_name" id="main_group_name"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Status</td>
                                        <td colspan="2">
                                            <select id="main_group_status" name="main_group_status">
                                                <option value="1">Active</option>
                                                <option value="0">InActive</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><!--<input type="hidden" name="category_id" id="category_id" value=""/>--></td>
                                        <td colspan="2">
                                            <input type="hidden" name="actiontype" id="actiontype" value="insert"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input type="hidden" name="startlimit" id="startlimit" value="{/XML/STARTLIMIT}"/>
											<input type="hidden" name="category_id" id="category_id" value="{/XML/SELECTED_CATEGORY_ID}"/>
                                        </td>
                                        <td colspan="2">
                                            <input type="hidden" name="cnt" id="cnt" value="{/XML/CNT}"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="26">
                                            <input type="hidden" name="sub_group_id" id="sub_group_id" value=""/>
											<input type="hidden" name="main_group_id" id="main_group_id" value=""/>
                                        </td>
                                        <td width="7%">
                                            <input type="submit" name="featuregroupsubmit" id="featuregroupsubmit" value="Add/Update"/>
                                        </td>
                                        <td width="80%">
                                            <input type="button" name="cancel" id="cancel" value="Cancel" onclick="javascript:this.form.reset();"/>
                                        </td>
                                    </tr>
                                </table>
								</form>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>
