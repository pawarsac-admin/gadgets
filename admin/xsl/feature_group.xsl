<?xml version="1.0" ?>
<!DOCTYPE xsl:stylesheet  [
  <!ENTITY nbsp   "&#160;">
  <!ENTITY copy   "&#169;">
  <!ENTITY reg    "&#174;">
  <!ENTITY trade  "&#8482;">
  <!ENTITY mdash  "&#8212;">
  <!ENTITY ldquo  "&#8220;">
  <!ENTITY rdquo  "&#8221;">
  <!ENTITY pound  "&#163;">
  <!ENTITY yen    "&#165;">
  <!ENTITY euro   "&#8364;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html" version="4.0" encoding="UTF-8" indent="yes"/>
    <xsl:include href="../xsl/inc_header.xsl" /><!-- include header-->
    <xsl:include href="../xsl/inc_footer.xsl" /><!-- include footer-->
    <xsl:include href="../xsl/inc_leftnavigation.xsl" /><!-- include left navigation-->
    <xsl:include href="../xsl/inc_rightnavigation.xsl" /><!-- include right navigation-->
    <xsl:template match="/">
        <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title>Mobile : Admin Feature Group Management</title>
                <link rel="stylesheet" type="text/css" href="{XML/WEB_URL}css/main.css" />
            </head>
            <body>
                <table align="center" width="100%" border="0" cellpadding="2" cellspacing="2">
                    <!-- call Header -->
                    <tr>
                        <td colspan="3">
                            <xsl:call-template name="incHeader"/>
                        </td>
                    </tr>
                    <tr>
                        <!-- call Left Navigation -->
                        <td width="10%" valign="top">
                            <xsl:call-template name="incLeftNavigation"/>
                        </td>
                        <!-- main area -->
                        <td>
                            <div align="center">
                                <table width="100%" border="0">
                                    <tr>
                                        <td>
                                            <h2>Feature Group Management</h2>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <a href="{XML/ADMIN_WEB_URL}index.php">Main</a>
>  Feature Group Dashboard</td>
                                    </tr>
                                </table>
                            </div>
                            <table width="100%" border="0">
                                <tr>
                                    <td colspan="6" width="100%">
                                        <h3>Add a new Feature Group</h3>
                                    </td>
                                </tr>
                                <xsl:if test="/XML/MSG!=''">
                                    <tr>
                                        <td colspan="6" bgcolor="#98AFC7">
                                            <div align="center">
                                                <xsl:value-of select="/XML/MSG" diseable-output-esacaping="yes"/>
                                            </div>
                                        </td>
                                    </tr>
                                </xsl:if>
                            </table>
                            <form action="{/XML/ADMIN_WEB_URL}feature_group.php" method="post" name="feature_group_action" id="feature_group_action" onsubmit="return validateFeatureGroup();">
                                <table width="100%" id="show_categorys">
                                    <tr>
                                        <td>
                                            <div id="ajaxloader" style="display:none;">
                                                <div align="center">
                                                    <img src="{/XML/IMAGE_URL}ajax-loader.gif"/>
                                                </div>
                                            </div>
                                            <div id="category_ajax" style="display:none;"></div>
                                            <div align="right">
                                                <input type="button" name="select_category" id="select_category" value="Select Category" onclick="javascript:getFeatureGroupDashboard('feature_group_dashboard','featuregroupajaxloader','','{/XML/STARTLIMIT}','{/XML/CNT}');"/>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <div id="featuregroupajaxloader" style="display:none;">
                                    <div align="center">
                                        <img src="{/XML/IMAGE_URL}ajax-loader.gif"/>
                                    </div>
                                </div>
                                <div id="feature_group_dashboard" style="display:block;">
					Please select category for feature group information.
                                </div>
                                <table width="100%" id="Update" border="1">
                                    <tr>
                                        <td width="13%">Feature Group Name</td>
                                        <td colspan="2">
                                            <input type="text" name="main_group_name" id="main_group_name"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="13%">Overview Display Group name</td>
                                        <td colspan="2">
                                            <input type="text" name="overview_display_group_name" id="overview_display_group_name"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Status</td>
                                        <td colspan="2">
                                            <select id="main_group_status" name="main_group_status">
                                                <option value="1">Active</option>
                                                <option value="0">InActive</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><!--<input type="hidden" name="category_id" id="category_id" value=""/>--></td>
                                        <td colspan="2">
                                            <input type="hidden" name="actiontype" id="actiontype" value="insert"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input type="hidden" name="startlimit" id="startlimit" value="{/XML/STARTLIMIT}"/>
                                        </td>
                                        <td colspan="2">
                                            <input type="hidden" name="cnt" id="cnt" value="{/XML/CNT}"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="26">
                                            <input type="hidden" name="main_group_id" id="main_group_id" value=""/>
                                        </td>
                                        <td width="7%">
                                            <input type="submit" name="featuregroupsubmit" id="featuregroupsubmit" value="Add/Update"/>
                                        </td>
                                        <td width="80%">
                                            <input type="button" name="cancel" id="cancel" value="Cancel" onclick="javascript:this.form.reset();"/>
                                        </td>
                                    </tr>
                                </table>
                            </form>
                        </td>
                        <!-- main area  END -->
                        <!-- call Right Navigation -->
                        <td width="15%"  valign="top">
                            <xsl:call-template name="incRightNavigation"/>
                        </td>
                    </tr>
                    <!-- call footer -->
                    <tr>
                        <td colspan="3">
                            <xsl:call-template name="incFooter"/>
                        </td>
                    </tr>
                </table>
            </body>
            <script LANGUAGE="JavaScript" SRC="{XML/ADMIN_JS_URL}common.js"></script>
            <script LANGUAGE="JavaScript" SRC="{XML/ADMIN_JS_URL}category.js"></script>
            <script LANGUAGE="JavaScript" SRC="{XML/ADMIN_JS_URL}feature_group.js"></script>
            <script>
                $(document).ready(function() {
                    category_details('<xsl:value-of select="/XML/SELECTED_CATEGORY_ID" diseable-output-escaping="yes"/>','category_ajax','ajaxloader');
                <xsl:if test="/XML/SELECTED_CATEGORY_ID!=''">
                    getFeatureGroupDashboard('feature_group_dashboard','featuregroupajaxloader','<xsl:value-of select="/XML/SELECTED_CATEGORY_ID" diseable-output-escaping="yes"/>','<xsl:value-of select="/XML/STARTLIMIT" diseable-output-escaping="yes"/>','<xsl:value-of select="/XML/CNT" diseable-output-escaping="yes"/>');
                </xsl:if>
                });
            </script>
        </html>
    </xsl:template>
</xsl:stylesheet>
