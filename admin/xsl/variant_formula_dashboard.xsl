<?xml version="1.0" ?>
<!DOCTYPE xsl:stylesheet  [
  <!ENTITY nbsp   "&#160;">
  <!ENTITY copy   "&#169;">
  <!ENTITY reg    "&#174;">
  <!ENTITY trade  "&#8482;">
  <!ENTITY mdash  "&#8212;">
  <!ENTITY ldquo  "&#8220;">
  <!ENTITY rdquo  "&#8221;">
  <!ENTITY pound  "&#163;">
  <!ENTITY yen    "&#165;">
  <!ENTITY euro   "&#8364;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html" version="4.0" encoding="UTF-8" indent="yes"/>
    <xsl:include href="inc_header.xsl" /><!-- include header-->
    <xsl:include href="inc_footer.xsl" /><!-- include footer-->
    <xsl:template match="/">
        <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title>:: Ajax - Admin  Dashboard Management ::</title>
				<link rel="stylesheet" type="text/css" href="{/XML/CSS_URL}main.css" />
		        <script>
					var admin_web_url = '<xsl:value-of select="/XML/ADMIN_WEB_URL" disable-output-escaping="yes"/>';
				</script>
	        </head>
            <body>
                <table align="center" width="100%" border="0" cellpadding="2" cellspacing="2">
                    <tr>
                        <td>
                            <div align="center">
                                <table width="100%" border="1" id="product_dashboard">
                                    <tr>
                                        <td colspan="18">
                                            <div align="center">
                                                <h3>Dashboard</h3>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="18">
                                            <div align="right">pagination goes here</div>
                                        </td>
                                    </tr>
                                    <tr>
					<td>Sr.No</td>
					<td>Category Name</td>
					<td>Variant Formula</td>
					<td>Status</td>
					<td>create date</td>
					<td colspan="4">Action</td>
                                    </tr>
                                    <xsl:choose>
                                        <xsl:when test="/XML/VARIANT_VALUE_MASTER/COUNT&lt;=0">
                                            <tr>
                                                <td colspan="12">
                                                    <div align="center">Zero result found.</div>
                                                </td>
                                            </tr>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:for-each select="/XML/VARIANT_VALUE_MASTER/VARIANT_VALUE_MASTER_DATA">
                                                <tr>
                                                    <td>
                                                        <xsl:value-of select="position()" diseable-output-esacaping="yes"/>
                                                    </td>
													<td>
                                                        <xsl:value-of select="CATEGORY_NAME" diseable-output-esacaping="yes"/>
                                                    </td>
						                                                     
													<td>
                                                        <xsl:value-of select="FORMULA" diseable-output-esacaping="yes"/>
                                                    </td>
													<td>
                                                        <xsl:value-of select="STATUS" diseable-output-esacaping="yes"/>
                                                    </td>
													<td>
                                                        <xsl:value-of select="CREATE_DATE" diseable-output-esacaping="yes"/>
                                                    </td>
													<!--	
                                                    <td>Up</td>
                                                    <td colspan="2">Down</td>
													-->
                                                    <td>
                                                        <a href="#Update" id="updateMe" onclick="updateVariantValue('variant_dashboard','variantajaxloader','{VARIANT_FORMULA_ID}','{CATEGORY_ID}','{BRAND_ID}','{PRODUCT_ID}','{COUNTRY_ID}','{STATE_ID}
','{CITY_ID}','','');">Update</a>
                                                    </td>
                                                    <td>
                                                        <a href="javascript:undefined;" onclick="deleteVariantValue('{VARIANT_FORMULA_ID}','{PRODUCT_ID}');">Delete</a>
                                                    </td>
                                                </tr>
                                            </xsl:for-each>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                    <tr>
                                        <td colspan="18">
                                            <div align="right">pagination goes here</div>
                                        </td>
                                    </tr>
                                </table>
                                <!--start code to add dealer form -->
                                <form enctype="multipart/form-data" method="post" action="{XML/ADMIN_WEB_URL}variant_formula.php" name="variant_manage" id="variant_manage" onsubmit="return validatevariant_manageVariantValue();">
                                    <table width="100%" border="0" id="add_product_table">                                        
                                        <tr>
                                            <td align="right">                                                
                                                <table width="100%" border="0">
                                                    <tr>
                                                        <td>
                                                            <input type="hidden" name="startlimit" id="startlimit" value="{/XML/STARTLIMIT}"/>
                                                        </td>
                                                        <td>
                                                            <input type="hidden" name="cnt" id="cnt" value="{/XML/CNT}"/>
                                                        </td>
                                                        <td>
										<input type="hidden" name="variant_formula_id" id="variant_formula_id" value="{XML/VARIANT_VALUE_DATA/VARIANT_FORMULA_ID}"/>
                                                        </td>
                                                        <td>
                                                            <input type="hidden" name="actiontype" id="actiontype" value="{XML/SELECTED_ACTION_TYPE}"/>
                                                        </td>
                                                        <td></td>
                                                        <td colspan="2"></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                </table>
                                                <table width="100%" id="Update" border="1">
							<tr>
								
								<td>Variant</td>
								<td colspan="18">
									<input type="hidden" name="variant_count" id="variant_count" value="{/XML/VARIANT_FORMULA_MASTER/COUNT}"/>
									<xsl:for-each select="/XML/VARIANT_FORMULA_MASTER/VARIANT_FORMULA_MASTER_DATA">
									<xsl:variable name="selectedvariantid" select="."/>
									<select name="variant_id_{position()}" id="variant_id_{position()}">
									<option value="">---Select Variant---</option>
									<xsl:for-each select="/XML/VARIANT_MASTER/VARIANT_MASTER_DATA">										
										<xsl:choose>
											<xsl:when test="VARIANT_ID=$selectedvariantid">
												<option value="{VARIANT_ID}" selected='yes'>
														<xsl:value-of select="VARIANT"/>
												</option>
											</xsl:when>
											<xsl:otherwise>
												<option value="{VARIANT_ID}">
														<xsl:value-of select="VARIANT"/>
												</option>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:for-each>
									</select>
									<xsl:if test="position()!=last()">
									<xsl:choose>
										<xsl:when test="/XML/EXISTING_FORMULA_OPERATOR/COUNT&gt;0">	
											<xsl:variable name="rootpos" select="position()"/>
											<select name="variant_operator_{position()}" id="variant_operator_{position()}">
												<xsl:for-each select="/XML/EXISTING_FORMULA_OPERATOR/EXISTING_FORMULA_OPERATOR_DATA">													
													<xsl:if test="$rootpos=position()">
															<option value="">-oprator-</option>
															<xsl:choose>
																<xsl:when test="SELECTED_OPERATOR=PLUS_OPERATOR">
																	<option value="+" selected="yes">+</option>
																</xsl:when>
																<xsl:otherwise>
																	<option value="+">+</option>
																</xsl:otherwise>
															</xsl:choose>
															<xsl:choose>
																<xsl:when test="SELECTED_OPERATOR=MINUS_OPERATOR">
																	<option value="-" selected="yes">-</option>
																</xsl:when>
																<xsl:otherwise>
																	<option value="-">-</option>
																</xsl:otherwise>
															</xsl:choose>	
															<xsl:choose>
																<xsl:when test="SELECTED_OPERATOR=MULTIPLY_OPERATOR">
																	<option value="*" selected="yes">*</option>
																</xsl:when>
																<xsl:otherwise>
																	<option value="*">*</option>
																</xsl:otherwise>
															</xsl:choose>
															<xsl:choose>
																<xsl:when test="SELECTED_OPERATOR=DIVIDE_OPERATOR">
																	<option value="/" selected="yes">/</option>
																</xsl:when>
																<xsl:otherwise>
																	<option value="/">/</option>
																</xsl:otherwise>
															</xsl:choose>	
														</xsl:if>
												</xsl:for-each>
												</select>
										</xsl:when>
										<xsl:otherwise>
												<select name="variant_operator_{position()}" id="variant_operator_{position()}">
														<option value="">-oprator-</option>
														<option value="+">+</option>
														<option value="-">-</option>
														<option value="*">*</option>
														<option value="/">/</option>
												</select>
										</xsl:otherwise>
									  </xsl:choose>
									  </xsl:if>
									</xsl:for-each>
								</td>
							</tr>	
							<tr>
								<td>Status#1</td>
								<td colspan="14">
								<select name="status" id="status">
								<xsl:choose>
									<xsl:when test="XML/VARIANT_VALUE_DATA/STATUS='Active'">
										<option value="1" selected='yes'>Active</option>
									</xsl:when>
									<xsl:otherwise>
										<option value="1">Active</option>
									</xsl:otherwise>
								</xsl:choose>
								<xsl:choose>
									<xsl:when test="XML/VARIANT_VALUE_DATA/STATUS='InActive'">
										<option value="0" selected='yes'>InActive</option>
									</xsl:when>
									<xsl:otherwise>
										<option value="0">InActive</option>
									</xsl:otherwise>
								</xsl:choose>
								
								</select>
								</td>
							</tr>
							<tr>						
								<td colspan="11"><input type="hidden" name="selected_category_id" id="selected_category_id" value="{/XML/SELECTED_CATEGORY_ID}"/></td>
							</tr>
							<tr>
								<td colspan="9">&nbsp;</td>
								<td>
								<div align="center">
								<input type="submit" name="save" value="Save" class="formbtn"/>
								</div>
								</td>
								<td>
								<div align="center">
								<input type="button" name="cancel" value="Cancel" class="formbtn" onclick="javascript:this.form.reset();"/>
								</div>
								</td>
						     </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </form>
                                <!--end code to add dealer form -->
                            </div>
                        </td>
                        <!-- main area  END -->
                    </tr>
                </table>
            </body>
	    </html>
    </xsl:template>
</xsl:stylesheet>