<?xml version="1.0" ?>
<!DOCTYPE xsl:stylesheet  [
  <!ENTITY nbsp   "&#160;">
  <!ENTITY copy   "&#169;">
  <!ENTITY reg    "&#174;">
  <!ENTITY trade  "&#8482;">
  <!ENTITY mdash  "&#8212;">
  <!ENTITY ldquo  "&#8220;">
  <!ENTITY rdquo  "&#8221;">
  <!ENTITY pound  "&#163;">
  <!ENTITY yen    "&#165;">
  <!ENTITY euro   "&#8364;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html" version="4.0" encoding="UTF-8" indent="yes"/>
    <xsl:include href="../xsl/inc_header.xsl" /><!-- include header-->
    <xsl:include href="../xsl/inc_footer.xsl" /><!-- include footer-->
    <xsl:include href="../xsl/inc_leftnavigation.xsl" /><!-- include left navigation-->
    <xsl:include href="../xsl/inc_rightnavigation.xsl" /><!-- include right navigation-->
    <xsl:template match="/">
        <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title>Mobile : Admin Custom Top Stories Management</title>
                <link rel="stylesheet" type="text/css" href="{XML/WEB_URL}css/main.css" />
		<link rel="stylesheet" href="{XML/WEB_URL}admin/css/styles.css" type="text/css" media="all" />
	    </head>
            <body>
		<style>
			#myPopUp{
				position:absolute;top:668px;left:155px;width:970px;height:550px;background:#ccc;overflow-x:hidden;overflow-y:auto;
				display:none;
			}
		</style>
		<div id="myPopUp">
			here data will appear
		</div>
                <table align="center" width="100%" border="0" cellpadding="2" cellspacing="2">
                    <!-- call Header -->
                    <tr>
                        <td colspan="3">
                            <xsl:call-template name="incHeader"/>
                        </td>
                    </tr>
                    <tr>
                        <!-- call Left Navigation -->
                        <td width="10%" valign="top">
                            <xsl:call-template name="incLeftNavigation"/>
                        </td>
                        <!-- main area -->
                        <td>
                            <div align="center">
                                <table width="100%" border="0">
                                    <tr>
                                        <td>
                                            <h2>Custom Top Stories Management</h2>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <a href="{XML/ADMIN_WEB_URL}index.php">Main</a> > Custom Top Stories
                                        </td>
                                    </tr>
                                </table>
                            </div>
			    <br/>
			    <div style="height:50px;" class="info">
				<div id="infox" class="info floatl">
					<span class="bold">Story Group : </span><select name="grouplist" id="grouplist">
					<xsl:for-each select="/XML/TOP_STORIES_GROUP/GROUP">
						<option value="{GROUP_ID}"><xsl:value-of select="TITLE" diseable-output-esacaping="yes"/></option>
					</xsl:for-each>
					</select>	
				</div>
				<div id="infoy" class="info floatr">
				<span class="bold">Create New Story Group : </span><form method="post" > Title : <input type="text" name="grouptitle"  size="30"/> No of Stories : <input type="text" name="noofstories" size="2"/>&nbsp;&nbsp;<input type="hidden" name="action"  value="createnewgroup" />&#160;<input type="submit" value=" Do It " class="groovybutton"/></form></div>
			    </div>

                            <!--start product ajax data place-->
                            
			    <pre><div id="info" class="info message bold">
			    <xsl:value-of select="/XML/MSG" diseable-output-esacaping="yes"/>
			    </div></pre>
				<form method="post">
				<div class="info">
				<ul class="ui-sortable" id="test-list">
				  
				  <xsl:for-each select="/XML/STORIES/STORY">
				  <li id="listItem_{STORY_ID}"><img src="/admin/images/arrow.png" alt="move" class="handle" height="16" width="16" />
				  <!--strong> <xsl:value-of select="position()" diseable-output-esacaping="yes"/> </strong-->
				  <input type="hidden" name="story_id_{position()}" value="{STORY_ID}" />
				  Title : <input type="text" name="product_name_{position()}" value="{TITLE}" size="50"/>
				  Link : <input type="text" name="product_link_{position()}" value="{LINK}" size="50"/>
				  </li>
				  </xsl:for-each>
				 
				</ul>
				<input type="hidden" name="group_id" value="{/XML/GROUP_ID}" />
				<input type="hidden" name="action" value="update" />
				<span style="float:right"><input type="submit" value="Save It" class="groovybutton" /></span>
				</div>
				</form>

                            <!--end product ajax data place-->
                        </td>
                        <!-- main area  END -->
                        <!-- call Right Navigation -->
                        <td width="15%"  valign="top">
                            <xsl:call-template name="incRightNavigation"/>
                        </td>
                    </tr>
                    <!-- call footer -->
                    <tr>
                        <td colspan="3">
                            <xsl:call-template name="incFooter"/>
                        </td>
                    </tr>
                </table>
            </body>
<script type="text/javascript" src="{XML/WEB_URL}admin/js/jquery-1.js"></script>
<script type="text/javascript" src="{XML/WEB_URL}admin/js/jquery-ui-personalized-1.js"></script>
<script type="text/javascript">
var group_id = '<xsl:value-of select="/XML/GROUP_ID" diseable-output-esacaping="yes"/>';
  // When the document is ready set up our sortable with it's inherant function(s)
  $(document).ready(function() {
	$("#test-list").sortable({
	handle : '.handle',
	update : function () {
		var order = $('#test-list').sortable('serialize');
		//alert(order);
		$("#info").load("/admin/custom_top_stories.php?action=rearrange&amp;group_id="+ group_id +"&amp;"+order);
		//$("#info").fadeOut("slow");
		YellowFade("#info");

	}
	});
	$('#grouplist').change(function() {
		window.location = "/admin/custom_top_stories.php?group_id=" + $("#grouplist").val();
	});
	$('#grouplist').val(group_id);

});

function YellowFade(selector){
   $(selector)
      .css('opacity', 0)
      .css('visibility','visible')
      .animate({ backgroundColor: '#00ff00', opacity: 1.0 }, 1000)
      .animate({ backgroundColor: '#00ff00', opacity: 1.0 }, 2000)
      .animate({ backgroundColor: '#ffffff', opacity: 0.0}, 750 );
}
YellowFade("#info");
</script>
        </html>
    </xsl:template>
</xsl:stylesheet>
