<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet  [
    <!ENTITY nbsp   "&#160;">
    <!ENTITY copy   "&#169;">
    <!ENTITY reg    "&#174;">
    <!ENTITY trade  "&#8482;">
    <!ENTITY mdash  "&#8212;">
    <!ENTITY ldquo  "&#8220;">
    <!ENTITY rdquo  "&#8221;">
    <!ENTITY raquo  "&#187;">
    <!ENTITY pound  "&#163;">
    <!ENTITY yen    "&#165;">
    <!ENTITY euro   "&#8364;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html" encoding="iso-8859-1" />
    <xsl:template name="incFooter">
        <br/>
        <!-- Start :All Category List-->
        <div id="all_categories_container" >
            <!-- Start :  oval_section_item -->
            <div class="section_item">
                <div class="section_item_inner">
                    <div class="section_item_title">
                        <h1>
                            <!-- DONOT USE THIS FOR HEADERS/TITLES , THIS IS JUST FOR CREATION OF THE OVAL CONTAINER -->
                        </h1>
                    </div>
                    <div class="section_item_content">
                        <div class="section_item_body">
                            <!-- Start :  section_item_body -->
                            <!-- End :  section_item_body -->
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End :  oval_section_item Important note: needs the above 4 close divs of this section-->
        </div>
        <!-- End :All Category List-->
        <div id="footer_container_outer">
            <div id="footer_container">
                <div id="footer_line1"></div>
                <div id="footer_line2">
                    <span class="fontdarkgrey fontbold">LikePort</span>
2010 - Mobile.</div>
                <span id="textdata"></span>
            </div>
            <div class="clearfix"></div>
        </div>
        <!-- Code for Search Auto Suggest -->
    </xsl:template>
</xsl:stylesheet>
