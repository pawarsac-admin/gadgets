<?xml version="1.0" ?>
<!DOCTYPE xsl:stylesheet  [
  <!ENTITY nbsp   "&#160;">
  <!ENTITY copy   "&#169;">
  <!ENTITY reg    "&#174;">
  <!ENTITY trade  "&#8482;">
  <!ENTITY mdash  "&#8212;">
  <!ENTITY ldquo  "&#8220;">
  <!ENTITY rdquo  "&#8221;">
  <!ENTITY pound  "&#163;">
  <!ENTITY yen    "&#165;">
  <!ENTITY euro   "&#8364;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html" version="4.0" encoding="UTF-8" indent="yes"/>
    <xsl:include href="../xsl/inc_header.xsl" /><!-- include header-->
    <xsl:include href="../xsl/inc_footer.xsl" /><!-- include footer-->
    <xsl:include href="../xsl/inc_leftnavigation.xsl" /><!-- include left navigation-->
    <xsl:include href="../xsl/inc_rightnavigation.xsl" /><!-- include right navigation-->
    <xsl:template match="/">
        <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title>: Admin Wallpaper Management</title>
                <link rel="stylesheet" type="text/css" href="{XML/WEB_URL}css/main.css" />
				
				<script type="text/javascript">
				function tiny(){
					tinyMCE.init({
						// General options
						mode : "textareas",
						theme : "advanced",
						plugins : "spellchecker,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",

						// Theme options
						theme_advanced_buttons1 : "bold,italic,underline,link,unlink,anchor,image",
						theme_advanced_buttons2 : "",
						theme_advanced_buttons3 : "",
						theme_advanced_buttons4 : "",
						theme_advanced_toolbar_location : "top",
						theme_advanced_toolbar_align : "left",
						theme_advanced_statusbar_location : "bottom",
						theme_advanced_resizing : true,

						// Skin options
						skin : "o2k7",
						skin_variant : "silver",

						// Example content CSS (should be your site CSS)
						content_css : "css/example.css",

						// Drop lists for link/image/media/template dialogs
						template_external_list_url : "js/template_list.js",
						external_link_list_url : "js/link_list.js",
						external_image_list_url : "js/image_list.js",
						media_external_list_url : "js/media_list.js",

						// Replace values for the template plugin
						template_replace_values : {
						username : "Some User",
						staffid : "991234"
						}
					});
				}
			</script> 
			<script language="javascript">
			<![CDATA[
				function isBlank(s){
					s = removeSpace(s);
					var len=s.length;
					var cnt;
					if(s.length==0){return true;}
					return false;
				}
				function removeSpace(s){
					return s.replace(/(^\s*)|(\s*$)/g, "");
				}

				//Function to Create/Remove one file browse element for uploading an image file
				function addRemoveFileBrowseElements(iAddOrRemove){
					//get the total number of file browse elements
					var iTotalRowsCurrent = document.getElementById('display_rows').value;
					if(iAddOrRemove==0){
						//remove one file browse element
						if(iTotalRowsCurrent>1){
							document.getElementById('slideshowtbody').removeChild(document.getElementById("tr"+iTotalRowsCurrent));
							document.getElementById('display_rows').value=iTotalRowsCurrent-1;
							//alert(document.getElementById('display_rows').value);
							if(document.getElementById('display_rows').value==0){
								//hide remove button
								document.getElementById('remove').style.visibility="hidden";
							}
						}
					}else{
						//add one file browse element 
						iTotalRowsCurrent++;
						//create a tr containing the file browse element along with desc textarea
						var oTr = document.createElement("TR");
						//append the TR to TBODY 

						document.getElementById('slideshowtbody').insertBefore(oTr, document.getElementById('trAddRemove'));
						oTr.setAttribute("id","tr"+iTotalRowsCurrent);
						var oTdUploadMedia = document.createElement("TD");
						oTr.appendChild(oTdUploadMedia);
						oTdUploadMedia.setAttribute("valign", "left");
						oTdUploadMedia.setAttribute("style", "border:1px solid #d5d5d5;");

						oTdUploadMedia.innerHTML="Upload Media"+iTotalRowsCurrent;

						var oTdUploadMediaFileBrowse = document.createElement("TD");
						oTr.appendChild(oTdUploadMediaFileBrowse);
						oTdUploadMediaFileBrowse.setAttribute("valign", "center");
						oTdUploadMediaFileBrowse.setAttribute("colspan", "10");
						oTdUploadMediaFileBrowse.setAttribute("style", "border:1px solid #d5d5d5;");
						oTdUploadMediaFileBrowse.innerHTML="<input name='media_id_"+iTotalRowsCurrent+"' type='hidden' size='40' id='media_id_"+iTotalRowsCurrent+"' value=''/><input name='img_upload_1_"+iTotalRowsCurrent+"' type='hidden' size='40' id='img_upload_id_1_"+iTotalRowsCurrent+"' value=''/><input type='text' name='title_upload_file_"+iTotalRowsCurrent+"' id='title_upload_file_"+iTotalRowsCurrent+"' value='' /><input type='button' name='btn_get' id='btn_get' value='media upload' onclick='getUploadData(\"product_manage\",\"title_upload_file_"+iTotalRowsCurrent+"\",\"media_id_"+iTotalRowsCurrent+"\",\"img_upload_id_1_"+iTotalRowsCurrent+"\",\"image\",\"main\");'/><input type='button' name='btn_search' id='btn_search' value='search' onclick='getUploadedDataList(\"product_manage\",\"title_upload_file_"+iTotalRowsCurrent+"\",\"media_id_"+iTotalRowsCurrent+"\",\"img_upload_id_1_"+iTotalRowsCurrent+"\",\"image\",\"main\");'/>Upload Thumb "+iTotalRowsCurrent+":<input name='img_media_id_"+iTotalRowsCurrent+"' type='hidden' size='40' id='img_media_id_"+iTotalRowsCurrent+"' value=''/><input name='img_upload_id_thm_"+iTotalRowsCurrent+"' type='hidden' size='40' id='img_upload_id_thm_"+iTotalRowsCurrent+"' value=''/><input type='text' name='thumb_title_"+iTotalRowsCurrent+"' id='thumb_title_"+iTotalRowsCurrent+"' value='' /><input type='button' name='btn_get' id='btn_get' value='image upload' onclick='getUploadData(\"product_manage\",\"thumb_title_"+iTotalRowsCurrent+"\",\"img_media_id_"+iTotalRowsCurrent+"\",\"img_upload_id_thm_"+iTotalRowsCurrent+"\",\"image\",\"main\");'/><input type='button' name='btn_search' id='btn_search' value='search' onclick='getUploadedDataList(\"product_manage\",\"thumb_title_"+iTotalRowsCurrent+"\",\"img_media_id_"+iTotalRowsCurrent+"\",\"img_upload_id_thm_"+iTotalRowsCurrent+"\",\"image\",\"main\");'/>";

						//increment the total rows count

						document.getElementById('display_rows').value=iTotalRowsCurrent;
						if(document.getElementById('display_rows').value>1){
							//hide remove button
							document.getElementById('remove').style.visibility="";
						}
					}
				}
			]]>
			</script>

            </head>
            <body>
			<style>
				#myPopUp{
					position:absolute;top:668px;left:155px;width:970px;height:550px;background:#ccc;overflow-x:hidden;overflow-y:auto;
					display:none;
				}
			</style>
			<div id="myPopUp">
				here data will appear
			</div>
                <table align="center" width="100%" border="0" cellpadding="2" cellspacing="2">
                    <!-- call Header -->
                    <tr>
                        <td colspan="3">
                            <xsl:call-template name="incHeader"/>
                        </td>
                    </tr>
                    <tr>
                        <!-- call Left Navigation -->
                        <td width="10%" valign="top">
                            <xsl:call-template name="incLeftNavigation"/>
                        </td>
                        <!-- main area -->
                        <td>
                            <div align="center">
                                <table width="100%" border="0">
                                    <tr>
                                        <td>
                                            <h2> Assign Wallpaper Management</h2>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <a href="{XML/ADMIN_WEB_URL}index.php">Main</a>> Assign Wallpapers 
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <table width="100%" border="0">
                                <tr>
                                    <td colspan="6" width="100%">
                                        <h3>Assign a new Wallpaper</h3>
                                    </td>
                                </tr>
                                <xsl:if test="/XML/MSG!=''">
                                    <tr>
                                        <td colspan="6" bgcolor="#98AFC7">
                                            <div align="center">
                                                <xsl:value-of select="/XML/MSG" diseable-output-esacaping="yes"/>
                                            </div>
                                        </td>
                                    </tr>
                                </xsl:if>
                            </table>
                            <!--start product ajax data place-->
                            <table>
                                <tr>
                                    <td>
                                        <div id="ajaxloader" style="display:none;">
                                            <div align="center">
                                                <img src="{/XML/IMAGE_URL}ajax-loader.gif"/>
                                            </div>
                                        </div>
                                        <div id="category_ajax" style="display:block;"></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div align="right">
                                            <input type="button" name="select_box" value="Select" id="select_box_id" class="formbtn" onclick="javascript: getWallpapersDashboard('prod_article_dashboard','productajaxloader','','{/XML/STARTLIMIT}','{/XML/CNT}');"/>

                                        </div>
                                    </td>
                                </tr>
                            </table>
                            <div id="productajaxloader" style="display:none;">
                                <div align="center">
                                    <img src="{/XML/IMAGE_URL}ajax-loader.gif"/>
                                </div>
                            </div>
                            <div id="prod_article_dashboard" style="display:block;">
                                        	Please select category for Wallpaper information.
                            </div>
                            <!--end product ajax data place-->
                        </td>
                        <!-- main area  END -->
                        <!-- call Right Navigation -->
                        <td width="15%"  valign="top">
                            <xsl:call-template name="incRightNavigation"/>
                        </td>
                    </tr>
                    <!-- call footer -->
                    <tr>
                        <td colspan="3">
                            <xsl:call-template name="incFooter"/>
                        </td>
                    </tr>
                </table>
            </body>
	    <script LANGUAGE="JavaScript" SRC="{XML/ADMIN_JS_URL}common.js"></script>
            <script LANGUAGE="JavaScript" SRC="{XML/ADMIN_JS_URL}category.js"></script>
            <script language="javascript" src="{XML/ADMIN_JS_URL}add_wallpapers.js"></script>
            <script language="javascript" src="{XML/ADMIN_JS_URL}tiny_mce/tiny_mce.js"></script>
	   
          
            <script>
            $(document).ready(function() {
                category_details('<xsl:value-of select="/XML/SELECTED_CATEGORY_ID" diseable-output-escaping="yes"/>','category_ajax','ajaxloader');
                <xsl:if test="/XML/SELECTED_CATEGORY_ID!=''">
                        getWallpapersDashboard('prod_article_dashboard','productajaxloader','<xsl:value-of select="/XML/SELECTED_CATEGORY_ID" diseable-output-escaping="yes"/>','<xsl:value-of select="/XML/STARTLIMIT" diseable-output-escaping="yes"/>','<xsl:value-of select="/XML/CNT" diseable-output-escaping="yes"/>');
                </xsl:if>
            });
				$("#updateMe").click(function(){
					alert("ok,ok,ok");
				});
            </script>
        </html>
    </xsl:template>
</xsl:stylesheet>
