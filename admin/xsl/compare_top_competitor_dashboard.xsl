<?xml version="1.0" ?>
<!DOCTYPE xsl:stylesheet  [
  <!ENTITY nbsp   "&#160;">
  <!ENTITY copy   "&#169;">
  <!ENTITY reg    "&#174;">
  <!ENTITY trade  "&#8482;">
  <!ENTITY mdash  "&#8212;">
  <!ENTITY ldquo  "&#8220;">
  <!ENTITY rdquo  "&#8221;">
  <!ENTITY pound  "&#163;">
  <!ENTITY yen    "&#165;">
  <!ENTITY euro   "&#8364;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html" version="4.0" encoding="UTF-8" indent="yes"/>
    <xsl:include href="inc_header.xsl" /><!-- include header-->
    <xsl:include href="inc_footer.xsl" /><!-- include footer-->
    <xsl:template match="/">
        <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title>:: Ajax - Admin Product Dashboard Management ::</title>
		
                <link rel="stylesheet" type="text/css" href="{/XML/CSS_URL}main.css" />
				<script language="javascript" src="{XML/ADMIN_JS_URL}compare_top_competitor.js"></script>
                <script>
		        var admin_web_url = '<xsl:value-of select="/XML/ADMIN_WEB_URL" disable-output-escaping="yes"/>';
           </script>
	   
            </head>
            <body>
				
                <table align="center" width="100%" border="0" cellpadding="2" cellspacing="2">
					
                    <tr>
                        <td>
                            <div align="center" style="border: 1px solid #d5d5d5;">
                                <table width="100%" border="0" id="product_dashboard" colspacing="0" rowspacing="0">
                                    <tr style="border: 1px solid #d5d5d5;">
                                        <td colspan="18" style="border: 1px solid #d5d5d5;">
                                            <div align="center">
                                                <h3>Dashboard</h3>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr style="border: 1px solid #d5d5d5;">
                                        <td colspan="18" style="border: 1px solid #d5d5d5;">
                                            <div align="right">pagination goes here</div>
                                        </td>
                                    </tr>
                                    <tr class="row0">
                                        <td>Sr.No</td>
                                        <td>Category Name</td>
                                        <td>Brand Name</td>
										<td>Product Name</td>
										<td>Variant</td>
										<td>Competitor Product Names</td>
										<td>Status</td>
										<td>Create date</td>
										<td colspan="4">Action</td>
                                    </tr>
                                    <xsl:choose>
                                        <xsl:when test="/XML/TOP_COMPETITOR_MASTER/COUNT&lt;=0">
                                            <tr>
                                                <td colspan="12">
                                                    <div align="center">Zero result found.</div>
                                                </td>
                                            </tr>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:for-each select="/XML/TOP_COMPETITOR_MASTER/TOP_COMPETITOR_MASTER_DATA">
												
                                                <tr class="row1">
                                                    <td>
                                                        <xsl:value-of select="position()" diseable-output-esacaping="yes"/>
                                                    </td>
                                                    <td>
                                                        <xsl:value-of select="CATEGORY_NAME" diseable-output-esacaping="yes"/>
                                                    </td>
                                                    <td>
                                                        <xsl:value-of select="BRAND_NAME" diseable-output-esacaping="yes"/>
                                                    </td>
                                                    <td>
                                                        <xsl:value-of select="PRODUCT_NAME" diseable-output-esacaping="yes"/>
                                                    </td>
													<td>
                                                        <xsl:value-of select="VARIANT" diseable-output-esacaping="yes"/>
                                                    </td>
													<td>
                                                        <xsl:value-of select="PRODUCT_NAMES" diseable-output-esacaping="yes"/><span style="padding-left:2px;"></span>
							 <xsl:value-of select="VARIANTS" diseable-output-esacaping="yes"/>
                                                    </td>
						    <td>
                                                        <xsl:value-of select="STATUS" diseable-output-esacaping="yes"/>
                                                    </td>
													<td>
                                                        <xsl:value-of select="CREATE_DATE" diseable-output-esacaping="yes"/>
                                                    </td>
													<!--	
																				<td>Up</td>
																				<td colspan="2">Down</td>
														-->
                                                    <td>
                                                        <a href="#Update" id="updateMe" onclick="updateProductCompetitor('prod_article_dashboard','productajaxloader','{COMPETITOR_PRODUCT_ID}','{PRODUCT_ID}','{CATEGORY_ID}','{BRAND_ID}','{PRODUCT_INFO_ID}','{PRODUCT_IDS}','{COMP_BRAND_ID}','{COMP_MODEL_ID}');">Update</a>
                                                    </td>
                                                    <td>
                                                        <a href="javascript:undefined;" onclick="deleteProductCompetitor('{COMPETITOR_PRODUCT_ID}');">Delete</a>
                                                    </td>
                                                </tr>
                                            </xsl:for-each>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                    <tr style="border: 1px solid #d5d5d5;">
                                        <td colspan="18" style="border: 1px solid #d5d5d5;">
                                            <div align="right">pagination goes here</div>
                                        </td>
                                    </tr>
                                </table>
                                <!--start code to add product form -->
                                <form enctype="multipart/form-data" method="post" action="{XML/ADMIN_WEB_URL}compare_top_competitor.php" name="product_manage" id="product_manage" onsubmit="return validateProduct();">
                                    <table width="100%" border="0" id="add_product_table">                                        
                                        <tr >
                                            <td align="right">                                                
                                                <table width="100%" border="0">
                                                    <tr class="row1">
                                                        <td>
                                                            <input type="hidden" name="startlimit" id="startlimit" value="{/XML/STARTLIMIT}"/>
                                                        </td>
                                                        <td>
                                                            <input type="hidden" name="cnt" id="cnt" value="{/XML/CNT}"/>
                                                        </td>
                                                        <td>
                                                            <input type="hidden" name="productid" id="productid" value="{XML/TOP_COMPETITOR_DATA/PRODUCT_ID}"/>
							<input type="hidden" name="competitor_product_id" id="competitor_product_id" value="{XML/TOP_COMPETITOR_DATA/COMPETITOR_PRODUCT_ID}"/>
							<input type="hidden" name="competitor_brand_id" id="competitor_brand_id" value="{XML/TOP_COMPETITOR_DATA/BRAND_ID_COMP}"/>
							<input type="hidden" name="competitor_productids" id="competitor_productids" value="{XML/TOP_COMPETITOR_DATA/PRODUCT_IDS}"/>
															
                                                        </td>
                                                        <td>
                                                            <input type="hidden" name="actiontype" id="actiontype" value="{/XML/SELECTED_ACTION_TYPE}"/>
                                                        </td>
                                                        <td>
                                                            <input type="hidden" name="featureboxcnt" id="featureboxcnt" value="{/XML/FEATURE_MASTER/COUNT}"/>
                                                        </td>
                                                        <td></td>
                                                        <td colspan="2"></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                </table>
                                                <table width="100%" id="Update" border="0" class="row1" style="border:1px solid #d4d4d4;">
												<tbody id="slideshowtbody">
                                                    <tr class="datarow1">
                                                        <td>Brand Name</td>
                                                        <td colspan="10">
                                                            <select name="select_brand_id" id="select_brand_id" onchange="getModelByBrand('ajaxloader','0');">
                                                                <option value="">---Select Brand---</option>
																<xsl:for-each select="/XML/BRAND_MASTER/BRAND_MASTER_DATA">
																<xsl:if test="/XML/TOP_COMPETITOR_DATA/BRAND_ID=BRAND_ID">
                                                                <option value="{BRAND_ID}" selected='yes'>
                                                                    <xsl:value-of select="BRAND_NAME"/>
                                                                </option>
																</xsl:if>
																<xsl:if test="not(/XML/TOP_COMPETITOR_DATA/BRAND_ID=BRAND_ID)">
                                                                <option value="{BRAND_ID}">
                                                                    <xsl:value-of select="BRAND_NAME"/>
                                                                </option>
																</xsl:if>
                                                            </xsl:for-each>
                                                            </select>
															<div id="ajaxloader" style="display:none;">
																<div align="center">
																	<img src="{/XML/IMAGE_URL}ajax-loader.gif"/>
																</div>
															</div>
                                                        </td>
                                                    </tr>
													<tr class="datarow1">
                                                        <td>Competitor Brand Name</td>
                                                        <td colspan="10">
                                                            <select name="select_comp_brand_id" id="select_comp_brand_id" onchange="getModelByBrandCompetitor('ajaxloader1','0');">
                                                                <option value="">---Select Brand---</option>
																<xsl:for-each select="/XML/BRAND_MASTER/BRAND_MASTER_DATA">
																<xsl:if test="/XML/TOP_COMPETITOR_DATA/BRAND_ID_COMP=BRAND_ID">
                                                                <option value="{BRAND_ID}" selected='yes'>
                                                                    <xsl:value-of select="BRAND_NAME"/>
                                                                </option>
																</xsl:if>
																<xsl:if test="not(/XML/TOP_COMPETITOR_DATA/BRAND_ID_COMP=BRAND_ID)">
                                                                <option value="{BRAND_ID}">
                                                                    <xsl:value-of select="BRAND_NAME"/>
                                                                </option>
																</xsl:if>
                                                            </xsl:for-each>
                                                            </select>
															<div id="ajaxloader1" style="display:none;">
																<div align="center">
																	<img src="{/XML/IMAGE_URL}ajax-loader.gif"/>
																</div>
															</div>
                                                        </td>
                                                    </tr>
													<tr>
                                                        <td>Status#1</td>
                                                        <td colspan="14">
															<select name="status" id="status">
															<xsl:if test="XML/TOP_COMPETITOR_DATA/STATUS='Active'">
																<option value="1" selected='yes'>Active</option>
																<option value="0">InActive</option>
															</xsl:if>
															<xsl:if test="XML/TOP_COMPETITOR_DATA/STATUS='InActive'">
																<option value="1" >Active</option>
																<option value="0" selected='yes'>InActive</option>
															</xsl:if>
															<xsl:if test="not(XML/TOP_COMPETITOR_DATA/STATUS)">
																<option value="1" selected='yes'>Active</option>
																<option value="0" >InActive</option>
															</xsl:if>
								
                                                            </select>
                                                        </td>
                                                    </tr>
													<tr>						
														<td colspan="11"><input type="hidden" name="selected_category_id" id="selected_category_id" value="{/XML/SELECTED_CATEGORY_ID}"/></td>
													</tr>
													<tr>
													<td colspan="9">&nbsp;</td>
                                                        <td>
                                                            <div align="center">
                                                                <input type="submit" name="save" value="Save" class="formbtn"/>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div align="center">
                                                                <input type="button" name="cancel" value="Cancel" class="formbtn" onclick="javascript:cleartopcompetitor();"/>
                                                            </div>
                                                        </td>
                                                    </tr>
													</tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </form>
                                <!--end code to add product form -->
                            </div>
                        </td>
                        <!-- main area  END -->
                    </tr>
                </table>
            </body>
	    </html>
    </xsl:template>
</xsl:stylesheet>
