<?xml version="1.0" ?>
<!DOCTYPE xsl:stylesheet  [
  <!ENTITY nbsp   "&#160;">
  <!ENTITY copy   "&#169;">
  <!ENTITY reg    "&#174;">
  <!ENTITY trade  "&#8482;">
  <!ENTITY mdash  "&#8212;">
  <!ENTITY ldquo  "&#8220;">
  <!ENTITY rdquo  "&#8221;">
  <!ENTITY pound  "&#163;">
  <!ENTITY yen    "&#165;">
  <!ENTITY euro   "&#8364;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html" version="4.0" encoding="UTF-8" indent="yes"/>
    <xsl:include href="inc_header.xsl" /><!-- include header-->
    <xsl:include href="inc_footer.xsl" /><!-- include footer-->
    <xsl:template match="/">
        <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title>:: Ajax - Admin Product Dashboard Management ::</title>
                <link rel="stylesheet" type="text/css" href="{/XML/CSS_URL}main.css" />
                <script>
		        var admin_web_url = '<xsl:value-of select="/XML/ADMIN_WEB_URL" disable-output-escaping="yes"/>';
           </script>
            </head>
            <body>
                <table align="center" width="100%" border="0" cellpadding="2" cellspacing="2">
                    <tr>
                        <td>
                            <div align="center">
                                <table width="100%" border="1" id="product_dashboard">
                                    <tr>
                                        <td colspan="18">
                                            <div align="center">
                                                <h3>Dashboard</h3>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="18">
                                            <div align="right">pagination goes here</div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Sr.No</td>
                                        <td>Article name</td>
                                        <td>Category Name</td>
                                        <td>Brand Name</td>
					<td>Product Name</td>
					<td>Status</td>
                                        <td>create date</td>
					<td colspan="3">&nbsp;</td>
					<td colspan="4">Action</td>
                                    </tr>
                                    <xsl:choose>
                                        <xsl:when test="/XML/ARTICLE_MASTER/COUNT&lt;=0">
                                            <tr>
                                                <td colspan="12">
                                                    <div align="center">Zero result found.</div>
                                                </td>
                                            </tr>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:for-each select="/XML/ARTICLE_MASTER/ARTICLE_MASTER_DATA">
                                                <tr>
                                                    <td>
                                                        <xsl:value-of select="position()" diseable-output-esacaping="yes"/>
                                                    </td>
                                                    <td>
                                                        <xsl:value-of select="ARTICLE_NAME" diseable-output-esacaping="yes"/>
                                                    </td>
						    
                                                    <td>
                                                        <xsl:value-of select="CATEGORY_NAME" diseable-output-esacaping="yes"/>
                                                    </td>
                                                    <td>
                                                        <xsl:value-of select="BRAND_NAME" diseable-output-esacaping="yes"/>
                                                    </td>
                                                    <td>
                                                        <xsl:value-of select="PRODUCT_NAME" diseable-output-esacaping="yes"/>
                                                    </td>
						    <td>
                                                        <xsl:value-of select="CREATE_DATE" diseable-output-esacaping="yes"/>
                                                    </td>
                                                    <td>Up</td>
                                                    <td colspan="2">Down</td>
                                                    <td>
                                                        <a href="#Update" id="updateMe" onclick="updateProductArticle('{ARTICLE_ID}','{PRODUCT_ID}','{BRAND_ID}','{CATEGORY_ID}','{STATUS}');">Update</a>
                                                    </td>
                                                    <td>
                                                        <a href="javascript:undefined;" onclick="deleteProductArticle('{ARTICLE_ID}','{JS_PRODUCT_NAME}');">Delete</a>
                                                    </td>
                                                </tr>
                                            </xsl:for-each>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                    <tr>
                                        <td colspan="18">
                                            <div align="right">pagination goes here</div>
                                        </td>
                                    </tr>
                                </table>
                                <!--start code to add product form -->
                                <form enctype="multipart/form-data" method="post" action="{XML/ADMIN_WEB_URL}article.php" name="product_manage" id="product_manage" onsubmit="return validateProduct();">
                                    <table width="100%" border="0" id="add_product_table">                                        
                                        <tr>
                                            <td align="right">                                                
                                                <table width="100%" border="0">
                                                    <tr>
                                                        <td>
                                                            <input type="hidden" name="startlimit" id="startlimit" value="{/XML/STARTLIMIT}"/>
                                                        </td>
                                                        <td>
                                                            <input type="hidden" name="cnt" id="cnt" value="{/XML/CNT}"/>
                                                        </td>
                                                        <td>
                                                            <input type="hidden" name="product_id" id="product_id"/>
							    <input type="hidden" name="article_id" id="article_id"/>
                                                        </td>
                                                        <td>
                                                            <input type="hidden" name="actiontype" id="actiontype" value="insert"/>
                                                        </td>
                                                        <td>
                                                            <input type="hidden" name="featureboxcnt" id="featureboxcnt" value="{/XML/FEATURE_MASTER/COUNT}"/>
                                                        </td>
                                                        <td></td>
                                                        <td colspan="2"></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                </table>
                                                <table width="100%" id="Update" border="1">
                                                    <tr>
                                                        <td>Brand Name</td>
                                                        <td colspan="10">
                                                            <select name="select_brand_id" id="select_brand_id" onchange="getProductByBrand();">
                                                                <option value="">---Select Brand---</option>
                                                            <xsl:for-each select="/XML/BRAND_MASTER/BRAND_MASTER_DATA">
                                                                <option value="{BRAND_ID}">
                                                                    <xsl:value-of select="BRAND_NAME"/>
                                                                </option>
                                                            </xsl:for-each>
                                                            </select>
                                                        </td>
                                                    </tr>
						    <tr>
                                                        <td>Product Name</td>
                                                        <td colspan="10">
                                                            <select name="select_product_id" id="select_product_id">
                                                                <option value="">---Select product---</option>
                                                            <xsl:for-each select="/XML/PRODUCT_MASTER/PRODUCT_MASTER_DATA">
                                                                <option value="{PRODUCT_ID}">
                                                                    <xsl:value-of select="PRODUCT_NAME"/>
                                                                </option>
                                                            </xsl:for-each>
                                                            </select>
                                                        </td>
                                                    </tr>
						    <tr>
							<td> Article Title</td>
                                                        <td colspan="10">
								<input type="text" name="article_title" id="article_title" size="50"/>
							</td>
						    </tr>
						    <tr>
							<td>Article Tags</td>
                                                        <td colspan="10">
								<input type="text" name="article_tags" id="article_tags" size="50"/>
							</td>
						    </tr>
						    <tr>
                                                	<td>Abstract</td>
                                                	<td colspan="10">
                                                    		<textarea name="article_abstract" id="article_abstract" cols="30"></textarea>
                                                	</td>
                                            	   </tr>
						    <tr>
                                                	<td>Description</td>
                                                	<td colspan="10">
                                                    		<textarea name="article_description" id="article_description" cols="30"></textarea>
                                                	</td>
                                            	   </tr>
						   <tr>
                                                	<td>Upload Media:</td>
                                                	<td colspan="10">
                                                    		<input name="img_upload_1" type="file" size="40" id="img_upload_id_1"/>
                                                	</td>
                                            	    </tr>
						    <tr>
                                                	<td>Upload Thumb:</td>
                                                	<td colspan="10">
                                                    		<input name="img_upload_thm" type="file" size="40" id="img_upload_id_thm"/>
                                                	</td>
                                            	    </tr>
                                                    <tr>
                                                        <td>Article Status#1</td>
                                                        <td colspan="14">
                                                            <select name="article_status" id="article_status">
                                                                <option value="1">Active</option>
                                                                <option value="0">InActive</option>
                                                            </select>
                                                        </td>
                                                    </tr>
						    <tr>						
                                                        <td colspan="11"><input type="hidden" name="selected_category_id" id="selected_category_id" value="{/XML/SELECTED_CATEGORY_ID}"/></td>
						     </tr>
						    <tr>
							<td colspan="9">&nbsp;</td>
                                                        <td>
                                                            <div align="center">
                                                                <input type="submit" name="save" value="Save" class="formbtn"/>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div align="center">
                                                                <input type="button" name="cancel" value="Cancel" class="formbtn" onclick="javascript:this.form.reset();"/>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </form>
                                <!--end code to add product form -->
                            </div>
                        </td>
                        <!-- main area  END -->
                    </tr>
                </table>
            </body>
	    

        </html>
    </xsl:template>
</xsl:stylesheet>
