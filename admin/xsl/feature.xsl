<?xml version="1.0" ?>
<!DOCTYPE xsl:stylesheet  [
  <!ENTITY nbsp   "&#160;">
  <!ENTITY copy   "&#169;">
  <!ENTITY reg    "&#174;">
  <!ENTITY trade  "&#8482;">
  <!ENTITY mdash  "&#8212;">
  <!ENTITY ldquo  "&#8220;">
  <!ENTITY rdquo  "&#8221;">
  <!ENTITY pound  "&#163;">
  <!ENTITY yen    "&#165;">
  <!ENTITY euro   "&#8364;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html" version="4.0" encoding="UTF-8" indent="yes"/>
    <xsl:include href="../xsl/inc_header.xsl" /><!-- include header-->
    <xsl:include href="../xsl/inc_footer.xsl" /><!-- include footer-->
    <xsl:include href="../xsl/inc_leftnavigation.xsl" /><!-- include left navigation-->
    <xsl:include href="../xsl/inc_rightnavigation.xsl" /><!-- include right navigation-->
    <xsl:template match="/">
        <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title>: Admin Add Feature Management</title>
                <link rel="stylesheet" type="text/css" href="{XML/WEB_URL}css/main.css" />
            </head>
            <body>
                <table align="center" width="100%" border="0" cellpadding="2" cellspacing="2">
                    <!-- call Header -->
                    <tr>
                        <td colspan="3">
                            <xsl:call-template name="incHeader"/>
                        </td>
                    </tr>
                    <tr>
                        <!-- call Left Navigation -->
                        <td width="10%" valign="top">
                            <xsl:call-template name="incLeftNavigation"/>
                        </td>
                        <!-- main area -->
                        <td>
                            <div align="center">
                                <table width="100%" border="0">
                                    <tr>
                                        <td>
                                            <h2>Feature Management</h2>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <a href="{XML/ADMIN_WEB_URL}index.php">Main</a>
> Add Feature
                                        </td>
                                    </tr>
                                    <xsl:if test="/XML/MSG!=''">
                                        <tr>
                                            <td colspan="6" bgcolor="#98AFC7">
                                                <div align="center">
                                                    <xsl:value-of select="/XML/MSG" diseable-output-esacaping="yes"/>
                                                </div>
                                            </td>
                                        </tr>
                                    </xsl:if>
                                </table>
                            </div>
                            
                            <form method="post" enctype="multipart/form-data" action="{XML/ADMIN_WEB_URL}feature.php" name="feature_manage" id="feature_manage" onsubmit="return validateFeature();">
                                
                                <table width="100%" border="0" id="add_feature_table">
                                    <tr>
                                        <td>
                                            <div id="ajaxloader" style="display:none;">
                                                <div align="center">
                                                    <img src="{/XML/IMAGE_URL}ajax-loader.gif"/>
                                                </div>
                                            </div>
                                            <div id="category_ajax" style="display:none;"></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div align="right">
                                                <input type="button" name="select_box" value="Select" id="select_box_id" class="formbtn" onclick="javascript:getFeatureDashboard('feature_dashboard','featureajaxloader','','{/XML/STARTLIMIT}','{/XML/CNT}');"/>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            <div id="featureajaxloader" style="display:none;">
                                                <div align="center">
                                                    <img src="{/XML/IMAGE_URL}ajax-loader.gif"/>
                                                </div>
                                            </div>
                                            <div id="feature_dashboard" style="display:block;">
                                        	Please select category for feature information.
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </form>
                        </td>
                        <!-- main area  END -->
                        <!-- call Right Navigation -->
                        <td width="15%"  valign="top">
                            <xsl:call-template name="incRightNavigation"/>
                        </td>
                    </tr>
                    <!-- call footer -->
                    <tr>
                        <td colspan="3">
                            <xsl:call-template name="incFooter"/>
                        </td>
                    </tr>
                </table>
            </body>
            <script LANGUAGE="JavaScript" SRC="{XML/ADMIN_JS_URL}common.js"></script>
            <script LANGUAGE="JavaScript" SRC="{XML/ADMIN_JS_URL}category.js"></script>
            <script language="javascript" src="{XML/ADMIN_WEB_URL}js/feature.js"></script>
            <script>
		var featureGroupArr = Array();
		var featureGroupIdsArr = Array();
		var featureUnitArr = Array();
		var featureUnitIdArr = Array();
		var featureMainGroupArr = Array();
		var featureMainGroupIdsArr = Array();
                $(document).ready(function() {
                    category_details('<xsl:value-of select="/XML/SELECTED_CATEGORY_ID" diseable-output-escaping="yes"/>','category_ajax','ajaxloader');
                    <xsl:if test="/XML/SELECTED_CATEGORY_ID!=''">
                        getFeatureDashboard('feature_dashboard','featureajaxloader','<xsl:value-of select="/XML/SELECTED_CATEGORY_ID" diseable-output-escaping="yes"/>','<xsl:value-of select="/XML/STARTLIMIT" diseable-output-escaping="yes"/>','<xsl:value-of select="/XML/CNT" diseable-output-escaping="yes"/>');
                    </xsl:if>
                });
		<xsl:for-each select="/XML/FEATURE_GROUP/FEATURE_GROUP_DATA">
			featureGroupIdsArr.push('<xsl:value-of select="SUB_GROUP_ID" disable-output-escaping="yes"/>'); 
			featureGroupArr.push('<xsl:value-of select="SUB_GROUP_NAME" disable-output-escaping="yes"/>'); 
		</xsl:for-each>
		<xsl:for-each select="/XML/FEATURE_GROUP_MASTER/FEATURE_GROUP_MASTER_DATA">
			featureMainGroupIdsArr.push('<xsl:value-of select="GROUP_ID" disable-output-escaping="yes"/>');
			featureMainGroupArr.push('<xsl:value-of select="MAIN_GROUP_NAME" disable-output-escaping="yes"/>');
                </xsl:for-each>
                <xsl:for-each select="/XML/FEATURE_UNIT/FEATURE_UNIT_DATA">
			featureUnitArr.push('<xsl:value-of select="UNIT_NAME" disable-output-escaping="yes"/>');
			featureUnitIdArr.push('<xsl:value-of select="UNIT_ID" disable-output-escaping="yes"/>');
		</xsl:for-each>
            </script>
        </html>
    </xsl:template>
</xsl:stylesheet>
