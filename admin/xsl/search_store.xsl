<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet  [
<!ENTITY nbsp   "&#160;">
<!ENTITY copy   "&#169;">
<!ENTITY reg    "&#174;">
<!ENTITY trade  "&#8482;">
<!ENTITY mdash  "&#8212;">
<!ENTITY ldquo  "&#8220;">
<!ENTITY rdquo  "&#8221;"> 
<!ENTITY pound  "&#163;">
<!ENTITY yen    "&#165;">
<!ENTITY euro   "&#8364;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="utf-8" doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"/>
<xsl:template match="/">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Upload</title>
<link href="css/admin.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript" src="js/common.js"></script>
<script>
var sform_name='<xsl:value-of select="HOME/SFORMNAME"/>'
var stitle_text='<xsl:value-of select="HOME/PARENT_TITLE"/>';
var media_hdtext='<xsl:value-of select="HOME/PARENT_UPLOADHDTXT"/>';
var hdthumbtext='<xsl:value-of select="HOME/PARENT_THUMBTXT"/>';
var stype='<xsl:value-of select="HOME/STYPE"/>';
var sParPath='<xsl:value-of select="HOME/PARENT_SPATH"/>';

function UpdateData(){
	var title = document.getElementById('media_title').value;
	var media_id = document.getElementById('media_id').value;
	//var thumb_image = document.getElementById('thumb_image').value;	
	var sPath = document.getElementById('thumb_image').value;
	window.opener.document.getElementById(stitle_text).value=title;
	window.opener.document.getElementById(media_hdtext).value=media_id;
	//window.opener.document.getElementById(hdthumbtext).value=thumb_image;
	window.opener.document.getElementById(sParPath).value=sPath;
	window.close();
}
</script>
<script type="text/javascript">
<![CDATA[
function get_radio_value(){
	if(document.frmupload.rd_image.length==undefined){
		if (document.frmupload.rd_image.checked){
			 var rad_val = document.frmupload.rd_image.value;
			 document.getElementById('media_title').value = document.getElementById('media_title_'+rad_val).value;
			 document.getElementById('media_id').value = document.getElementById('media_id_'+rad_val).value;
			 document.getElementById('thumb_image').value = document.getElementById('thumb_image_'+rad_val).value;
      		}
	}else{
		for (var i=0; i < document.frmupload.rd_image.length; i++){
	   		if (document.frmupload.rd_image[i].checked){
	      			var rad_val = document.frmupload.rd_image[i].value;
				//alert(rad_val);
				 document.getElementById('media_title').value = document.getElementById('media_title_'+rad_val).value;
				 document.getElementById('media_id').value = document.getElementById('media_id_'+rad_val).value;
				 document.getElementById('thumb_image').value = document.getElementById('thumb_image_'+rad_val).value;
	      		}
	   	}
	}
}
]]>
</script>

</head>
<body>
<!--start of main div-->
<div id="maindiv">
	<form name="frmupload" method="post" action="" enctype="multipart/form-data" onsubmit="return form_validator(document.frmupload,'search');">
		<table width="100%" height="300" border="0" align="center" cellpadding="2" cellspacing="2" bgcolor="#ececec">
			
			<tr><td style="border:1px solid #000000;"><table width="80%" border="0" align="center" cellpadding="2" cellspacing="2" bgcolor="#ececec">
							<tr><td>Search </td></tr>
							<tr>
								<td align="left" bgcolor="#ececec" class="black_plain">Name:
								<input name="search" type="text" id="search" size="20" value=""/>
								
								<div id="search_div" class="rederr"></div>
								search by:
								<select name="searchby" id="searchby">
										<option value="title">title</option>
										<option value="tags">tags</option>
									</select>
								<input type="submit" name="btn_search" id="btn_search" value="search"/>
								
								</td>
							</tr>
							<tr>
								<td align="left" bgcolor="#ececec" class="black_plain">List:</td>
							</tr>
							<tr>
							     <td> 
								  <table>
				<xsl:for-each select="HOME/UPLOADED_MEDIA/UPLOADED_MEDIA_DATA">						
					<xsl:if test="position() mod 4=0">
						 <xsl:text disable-output-escaping="yes">&lt;tr&gt;</xsl:text>
					</xsl:if>
						<td align="left" bgcolor="#ececec" class="gr">
						<input type="radio" name="rd_image[]" id="rd_image" value="{MEDIA_ID}" onclick="get_radio_value();"/>
						<xsl:value-of select="TITLE"/>
						<input name="media_id_{MEDIA_ID}" type="hidden" id="media_id_{MEDIA_ID}" size="20" value="{MEDIA_ID}"/>
						<input name="media_title_{MEDIA_ID}" type="hidden" id="media_title_{MEDIA_ID}"  value="{TITLE}"/>
						<xsl:for-each select="UPLOADED_MEDIA_IMG/UPLOADED_MEDIA_IMG_DATA">
						<xsl:if test="position()=last()">
						<input name="thumb_image_{../../MEDIA_ID}" type="hidden" id="thumb_image_{../../MEDIA_ID}"  value="{IMG_PATH}"/>
						
						<br/>
						<img src="{IMG_PATH}" width="80" height="80"/>
						</xsl:if>
						</xsl:for-each>
						</td>
					<xsl:if test="position() mod 4=0">
						 <xsl:text disable-output-escaping="yes">&lt;tr&gt;</xsl:text>
					</xsl:if>
				</xsl:for-each>
								</table>
							   </td>
						     </tr>	
						     <tr>
								<td align="right" bgcolor="#ececec" class="black_plain">
								<span class="gr">
								<input name="media_id" type="hidden" id="media_id" size="20" value=""/>
								<input name="media_title" type="hidden" id="media_title"  value=""/>
								<input name="thumb_image" type="hidden" id="thumb_image"  value=""/>
								<input type="button" name="btn_done" id="btn_done" value="Done" onclick="UpdateData();"/>
								<input type="button" name="btn_close" id="btn_close" value="close without saving" onclick="window.close();"/>
								</span></td>
							</tr>	
							
						</table></td></tr>
			
		</table>
	</form>
</div>
<!--end of main div-->
</body>
</html>
</xsl:template>
</xsl:stylesheet>
