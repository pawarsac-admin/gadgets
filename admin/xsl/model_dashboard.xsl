<?xml version="1.0" ?>
<!DOCTYPE xsl:stylesheet  [
  <!ENTITY nbsp   "&#160;">
  <!ENTITY copy   "&#169;">
  <!ENTITY reg    "&#174;">
  <!ENTITY trade  "&#8482;">
  <!ENTITY mdash  "&#8212;">
  <!ENTITY ldquo  "&#8220;">
  <!ENTITY rdquo  "&#8221;">
  <!ENTITY pound  "&#163;">
  <!ENTITY yen    "&#165;">
  <!ENTITY euro   "&#8364;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html" version="4.0" encoding="UTF-8" indent="yes"/>
    <xsl:include href="inc_header.xsl" /><!-- include header-->
    <xsl:include href="inc_footer.xsl" /><!-- include footer-->
    <xsl:template match="/">
        <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title>:: Ajax - Admin Product Dashboard Management ::</title>
		
                <link rel="stylesheet" type="text/css" href="{/XML/CSS_URL}main.css" />
		
                <script>
		        var admin_web_url = '<xsl:value-of select="/XML/ADMIN_WEB_URL" disable-output-escaping="yes"/>';
           </script>
	   
            </head>
            <body>
	     <div id="sOProduOverDiv">			
                <table align="center" width="100%" border="0" cellpadding="2" cellspacing="2">
			
                    <tr>
                        <td>
                            <div align="center" style="border: 1px solid #d5d5d5;">
                                <table width="100%" border="0" id="product_dashboard" colspacing="0" rowspacing="0">
                                    <tr style="border: 1px solid #d5d5d5;">
                                        <td colspan="18" style="border: 1px solid #d5d5d5;">
                                            <div align="center">
                                                <h3>Dashboard</h3>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr style="border: 1px solid #d5d5d5;">
                                        <td colspan="18" style="border: 1px solid #d5d5d5;">
                                            <div align="right"><xsl:value-of select="XML/Pages" disable-output-escaping="yes"/></div>
                                        </td>
                                    </tr>
                                    <tr class="row0">
                                        <td>Sr.No</td>
                                        <td>Model Title</td>
                                        <td>Category Name</td>
                                        <td>Brand Name</td>
										<td>Status</td>
									    <td>Create date</td>
										<td colspan="4">Action</td>
                                    </tr>
									
                                    <xsl:choose>
                                        <xsl:when test="count(/XML/MODEL_DETAIL/MODEL_DETAIL_DATA)&lt;=0">
                                            <tr>
                                                <td colspan="12">
                                                    <div align="center">Zero result found.</div>
                                                </td>
                                            </tr>
                                        </xsl:when>
                                        <xsl:otherwise>
										
                                            <xsl:for-each select="/XML/MODEL_DETAIL/MODEL_DETAIL_DATA">
												
                                                <tr class="row1">
                                                    <td>
                                                        <xsl:value-of select="position()" diseable-output-esacaping="yes"/>
                                                    </td>
                                                    <td>
                                                        <xsl:value-of select="PRODUCT_INFO_NAME" diseable-output-esacaping="yes"/>
                                                    </td>
						    
                                                    <td>
                                                        <xsl:value-of select="CATEGORY_NAME" diseable-output-esacaping="yes"/>
                                                    </td>
                                                    <td>
                                                        <xsl:value-of select="BRAND_NAME" diseable-output-esacaping="yes"/>
                                                    </td>
                                                   	<td>
                                                        <xsl:value-of select="STATUS" diseable-output-esacaping="yes"/>
                                                    </td>
													<td>
                                                        <xsl:value-of select="CREATE_DATE" diseable-output-esacaping="yes"/>
                                                    </td>
													<!--	
																				<td>Up</td>
																				<td colspan="2">Down</td>
														-->
                                                    <td>
                                                        <a href="#Update" id="updateMe" onclick="updateProductModel('prod_article_dashboard','productajaxloader','{PRODUCT_NAME_ID}','{CATEGORY_ID}','{BRAND_ID}','','');">Update</a>
                                                    </td>
                                                    <td>
                                                        <a href="javascript:undefined;" onclick="deleteProductModel('{PRODUCT_NAME_ID}');">Delete</a>
                                                    </td>
                                                </tr>
                                            </xsl:for-each>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                    <tr style="border: 1px solid #d5d5d5;">
                                        <td colspan="18" style="border: 1px solid #d5d5d5;">
                                            <div align="right"><xsl:value-of select="XML/Pages" disable-output-escaping="yes"/></div>
                                        </td>
                                    </tr>
                                </table>
                                <!--start code to add product form -->
                                <form enctype="multipart/form-data" method="post" action="{XML/ADMIN_WEB_URL}model.php" name="product_manage" id="product_manage" onsubmit="return validateProduct();">
                                    <table width="100%" border="0" id="add_product_table">                                        
                                        <tr >
                                            <td align="right">                                                
                                                <table width="100%" border="0">
                                                    <tr class="row1">
                                                        <td>
                                                            <input type="hidden" name="startlimit" id="startlimit" value="{/XML/STARTLIMIT}"/>
                                                        </td>
                                                        <td>
                                                            <input type="hidden" name="cnt" id="cnt" value="{/XML/CNT}"/>
                                                        </td>
                                                        <td>
                                                           
															<input type="hidden" name="product_name_id" id="product_name_id" value="{/XML/MODEL_MASTER/MODEL_MASTER_DATA/PRODUCT_NAME_ID}"/>
															
                                                        </td>
                                                        <td>
                                                            <input type="hidden" name="actiontype" id="actiontype" value="{/XML/SELECTED_ACTION_TYPE}"/>
                                                        </td>
                                                        <td>
                                                            <input type="hidden" name="featureboxcnt" id="featureboxcnt" value="{/XML/FEATURE_MASTER/COUNT}"/>
                                                        </td>
                                                        <td></td>
                                                        <td colspan="2"></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                </table>
                                                <table width="100%" id="Update" border="0" class="row1" style="border:1px solid #d4d4d4;">
												<tbody id="slideshowtbody">
                                                    <tr class="datarow1">
                                                        <td>Brand Name</td>
                                                        <td colspan="10">
                                                            <select name="select_brand_id" id="select_brand_id">
                                                                <option value="">---Select Brand---</option>
																<xsl:for-each select="/XML/BRAND_MASTER/BRAND_MASTER_DATA">
																<xsl:if test="/XML/MODEL_MASTER/MODEL_MASTER_DATA/BRAND_ID=BRAND_ID">
                                                                <option value="{BRAND_ID}" selected='yes'>
                                                                    <xsl:value-of select="BRAND_NAME"/>
                                                                </option>
																</xsl:if>
																<xsl:if test="not(/XML/MODEL_MASTER/MODEL_MASTER_DATA/BRAND_ID=BRAND_ID)">
                                                                <option value="{BRAND_ID}">
                                                                    <xsl:value-of select="BRAND_NAME"/>
                                                                </option>
																</xsl:if>
                                                            </xsl:for-each>
                                                            </select>
															<div id="ajaxloader" style="display:none;">
																<div align="center">
																	<img src="{/XML/IMAGE_URL}ajax-loader.gif"/>
																</div>
															</div>
                                                        </td>
                                                    </tr>
													
						    						<tr class="datarow1">
														<td> Model Name</td>
														<td colspan="10">
															<input type="text" name="model_title" id="model_title" size="80" value="{/XML/MODEL_MASTER/MODEL_MASTER_DATA/PRODUCT_INFO_NAME}"/>
														</td>
													</tr>
													<tr>
														<td>Model Tags</td>
														<td colspan="10">
															<input type="text" name="model_tags" id="model_tags" size="80" value="{XML/MODEL_MASTER/MODEL_MASTER_DATA/TAGS}"/>
														</td>
													</tr>
<!--<tr class="datarow1">
<td>Abstract</td>
<td colspan="10">
<textarea name="model_abstract" id="model_abstract" cols="80" rows="3" class="mceEditor">
<xsl:value-of select="XML/MODEL_MASTER/MODEL_MASTER_DATA/ABSTRACT"/>
</textarea>
</td>
</tr>-->
													<tr>
														<td>Description</td>
														<td colspan="10">
                                                    		<textarea name="model_description" id="model_description" cols="80" rows="5" class="mceEditor">
															<xsl:value-of select="XML/MODEL_MASTER/MODEL_MASTER_DATA/PRODUCT_NAME_DESC"/>
															</textarea>
														</td>
                                            	   </tr>
													<tr class="datarow1">
														<td>Upload Main Image:</td>
														<td colspan="10">
															<input name="media_id" type="hidden" size="40" id="media_id" value="{XML/MODEL_MASTER/MODEL_MASTER_DATA/MEDIA_ID}"/>
															<input name="img_upload_1" type="hidden" size="40" id="img_upload_id_1" value="{XML/MODEL_MASTER/MODEL_MASTER_DATA/VIDEO_PATH}"/>
															<input type="text" name="title_upload_file" id="title_upload_file" value="{XML/MODEL_MASTER/MODEL_MASTER_DATA/VIDEO_PATH}" readonly="yes"/>
															<input type="hidden" name="video_content_type" id="video_content_type" value="{XML/MODEL_MASTER/MODEL_MASTER_DATA/CONTENT_TYPE}"/>
															<input type="button" name="btn_get" id="btn_get" value="media upload" onclick="getUploadData('product_manage','title_upload_file','media_id','img_upload_id_1','image','video_content_type');"/>
															<input type="button" name="btn_search" id="btn_search" value="search" onclick="getUploadedDataList('product_manage','title_upload_file','media_id','img_upload_id_1','image','video_content_type');"/>	
														</td>
                                            	    </tr>
													<tr>
														<td>Upload Thumb Image:</td>
														<td colspan="10">
															<input name="img_media_id" type="hidden" size="40" id="img_media_id" value="{XML/MODEL_MASTER/MODEL_MASTER_DATA/IMG_MEDIA_ID}"/>
															<input name="img_upload_thm" type="hidden" size="40" id="img_upload_id_thm" value="{XML/MODEL_MASTER/MODEL_MASTER_DATA/IMAGE_PATH}"/>
															<input type="text" name="thumb_title" id="thumb_title" value="{XML/MODEL_MASTER/MODEL_MASTER_DATA/IMAGE_PATH}" readonly="yes"/> 	
															<input type="hidden" name="content_type" id="content_type" value="{XML/MODEL_MASTER/MODEL_MASTER_DATA/CONTENT_TYPE}"/>
															<input type="button" name="btn_get" id="btn_get" value="image upload" onclick="getUploadData('product_manage','thumb_title','img_media_id','img_upload_id_thm','image','content_type');"/>
															<input type="button" name="btn_search" id="btn_search" value="search" onclick="getUploadedDataList('product_manage','thumb_title','img_media_id','img_upload_id_thm','image','content_type');"/>			 
														</td>
                                            	    </tr>
												    <tr>
                                                        <td>Model Status#1</td>
                                                        <td colspan="14">
															<xsl:if test="/XML/WALLCNT!=0">
															<input name="display_rows" id="display_rows" value="{/XML/WALLCNT}" type="hidden" />
															</xsl:if>
															<xsl:if test="/XML/WALLCNT=0">
															<input name="display_rows" id="display_rows" value="1" type="hidden" />
															</xsl:if>
								                                                            <select name="model_status" id="model_status">
																<xsl:choose>
																	<xsl:when test="XML/MODEL_MASTER/MODEL_MASTER_DATA/STATUS='1'">
																		<option value="1" selected='yes'>Active</option>
																	</xsl:when>
																	<xsl:otherwise>
																		<option value="1">Active</option>
																	</xsl:otherwise>
																</xsl:choose>
																<xsl:choose>
																	<xsl:when test="XML/MODEL_MASTER/MODEL_MASTER_DATA/STATUS='0'">
																		<option value="0" selected='yes'>InActive</option>
																	</xsl:when>
																	<xsl:otherwise>
																		<option value="0">InActive</option>
																	</xsl:otherwise>
																</xsl:choose>															
                                                            </select>
                                                        </td>
                                                    </tr>
													<tr>						
														<td colspan="11"><input type="hidden" name="selected_category_id" id="selected_category_id" value="{/XML/SELECTED_CATEGORY_ID}"/></td>
													</tr>
													<tr>
													<td colspan="9">&nbsp;</td>
                                                        <td>
                                                            <div align="center">
                                                                <input type="submit" name="save" value="Save" class="formbtn"/>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div align="center">
                                                                <input type="button" name="cancel" value="Cancel" class="formbtn" onclick="javascript:this.form.reset();"/>
                                                            </div>
                                                        </td>
                                                    </tr>
													</tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </form>
                                <!--end code to add product form -->
                            </div>
                        </td>
                        <!-- main area  END -->
                    </tr>
                </table>
		</div>
            </body>
	    </html>
    </xsl:template>
</xsl:stylesheet>
