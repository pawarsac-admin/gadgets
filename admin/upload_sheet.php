<?php
require_once('../include/config.php');
require_once(CLASSPATH.'DbConn.php');
require_once(CLASSPATH.'brand.class.php');
require_once(CLASSPATH.'product.class.php');
require_once(CLASSPATH.'citystate.class.php');
require_once(CLASSPATH.'price.class.php');

$dbconn = new DbConn;
$brand = new BrandManagement;
$product = new ProductManagement;
$citystate = new citystate;
$price = new price;


//print_r($_REQUEST); die();
$resultCountry['0']=array("country_id"=>"1","country_name"=>"India");
$head_label_array = array("0"=>"Brand_Name",1=>"Model_Name","2"=>"Variant","3"=>"Ex-Showroom","4"=>"Registration","5"=>"Registration_Amount","6"=>"Insurance","7"=>"Insurance_Amount","8"=>"Octoroi","9"=>"Octoroi_Amount");
$sub_head_label_array = array("0"=>"city_name","1"=>"default_city");


$city_id = $_REQUEST['city_id'];
$get_city_detail = $citystate->arrGetCityStateDetails("",$city_id);
//print_r($get_city_detail);
$city_name = $get_city_detail['0']['city_name'];
//echo $city_name."VVVDV";


$category_id = $_REQUEST['catid'];
$file_name = $_REQUEST['file_name'];
$process = $_REQUEST['process'];
$category_id = (!empty($category_id)) ? $category_id : $_REQUEST['selected_category_id'];
$actiontype = $_REQUEST['actiontype'];
$message = '';
if($actiontype == 'insert' || $actiontype == 'Update'){
	if($_FILES["uploadedfile"]["type"] != "text/csv"){
		$target_path = BASEPATH."uploadsheet/";
		$name = $_FILES['uploadedfile']['name'];
		$name = strtolower(str_replace(' ', '_', trim($name)));
		$file_name = $name;
		$target_path = $target_path.$name;
		if(!empty($target_path)){unlink($target_path);}
		$message = " Upload csv file ";
		$start_process = "0";
	}else{
		if($_FILES["uploadedfile"]["name"] != ""){
			$target_path = BASEPATH."uploadsheet/";
			$name = $_FILES['uploadedfile']['name'];
			$name = strtolower(str_replace(' ', '_', trim($name)));
			$file_name = $name;
			$target_path = $target_path.$name;
			if(!empty($target_path)){unlink($target_path);}
			$sflag = rename($_FILES['uploadedfile']['tmp_name'], $target_path);
			$csv_file_data = parse_csv($target_path,$flag);
			//print "<pre>"; print_r($csv_file_data);
			$err = 0;
			if(is_array($csv_file_data)){
				$city_name =strtolower($csv_file_data[1][1]);
				$get_city_detail = $citystate->arrGetCityStateDetails($city_name);
				$heading_label = $csv_file_data[0];
				if(is_array($heading_label)){
					for($k=0;$k<count($heading_label);$k++){
						if(strlen($heading_label[$k]) > 0){
							if(!in_array($heading_label[$k],$head_label_array)){
								$alog_error[] = $heading_label[$k]; 
								$err++;
							}
						}
					}
				}
				$city_name = strtolower($csv_file_data[1][1]);

				$city_name_field = strtolower($csv_file_data[1][0]);
				$def_city_field = strtolower($csv_file_data[2][0]);
				if(!in_array($city_name_field,$sub_head_label_array)){
					$alog_error[] = $city_name_field;
					$err++;
				}
				if(!in_array($def_city_field,$sub_head_label_array)){
					$alog_error[] = $def_city_field;
					$err++;
				}

				if(empty($get_city_detail)){
					$alog_error[] = $city_name;
					$err++;
				}
				$log_error ='';
				if($err > 0){
					$log_error .= implode(",",$alog_error);
					$log_error .= "\n\rSheet heading lable or city name not match with actual heading as -\n\r";
					$log_error .= implode(",",$head_label_array); 
					$log_error .= implode(",",$sub_head_label_array); 
					
					$fwrite = file_error_log($log_error,$city_name);
					if(!empty($target_path)){unlink($target_path);}
				}	
			}
			if($sflag==1){
				if($err == 0){
						$message = "File Uploaded Successfully...";
						$start_process = "1";
				}else{
						$myFile = str_replace("","_",$city_name)."_err_log_file.xls";
						$message = "check upload sheet  <a href=".ADMIN_WEB_URL."error_log_download.php?filename=$myFile>Error Log</a>...";
						$start_process = "0";	 
				}
			}
		}
	}
}
$cntCnt = sizeof($resultCountry);
$xml .= "<COUNTRY_MASTER>";
$xml .= "<COUNT><![CDATA[$cntCnt]]></COUNT>";
for($i=0;$i<$cntCnt;$i++){
	$resultCountry[$i] = array_change_key_case($resultCountry[$i],CASE_UPPER);
	$xml .= "<COUNTRY_MASTER_DATA>";
	foreach($resultCountry[$i] as $k=>$v){
		$xml .= "<$k><![CDATA[$v]]></$k>";
	}
	$xml .= "</COUNTRY_MASTER_DATA>";
}
$xml .= "</COUNTRY_MASTER>";
$data_err = '';
$process_ins = $_REQUEST['process_ins'];
$process = $_REQUEST['process'];
$file_name = $file_name ? $file_name : $_REQUEST['file_name'] ;
//echo $process_ins."=======".$process."<br>";
if($process == "1" && $process_ins == "db_ins"){
	//echo "SASASASASASASA";
	$target_path = BASEPATH."uploadsheet/";
	$filename = $target_path.$file_name;
	$csv_file_data = parse_csv($filename,$flag);
	//print "<pre>"; print_r($csv_file_data);
	if(is_array($csv_file_data)){
		$default_city_data = $csv_file_data[2][1];
		$default_city = $default_city_data ? $default_city_data : 0;
		$city_name =strtolower($csv_file_data[1][1]);
		if(!empty($city_name)){
			$get_city_detail = $citystate->arrGetCityStateDetails($city_name);
			$city_id = $get_city_detail[0]['city_id'];
			if(!empty($city_id)){
				$state_id = $get_city_detail[0]['state_id'];
				$country_id = $get_city_detail[0]['country_id'];
				$category_id = (!empty($category_id)) ? $category_id : SITE_CATEGORY_ID;	
				for($i=3;$i<count($csv_file_data);$i++){
					$brand_name = ''; $brand_id=''; $brand_result=''; $product_result=''; $product_id=''; $product_name='';
					$v_array = array();
					if(!empty($csv_file_data[$i])){
						$brand_name = $csv_file_data[$i]['brand_name'];
						if(!empty($brand_name)){
							$product_name = $csv_file_data[$i]['variant'];
							$model_name = $csv_file_data[$i]['model_name'];
							if(!empty($category_id)){
								$brand_result = $brand->arrGetBrandDetails("",$category_id,"1","","",$brand_name);
							}
							if(!empty($brand_result)){
								$brand_id = $brand_result[0]['brand_id'];
							}
							
							if(!empty($brand_id)){
								//echo $product_name."=====product_name<br>";
								if(!empty($product_name)){
									if(!empty($category_id)){
										$product_result = $product->arrGetProductNameByName($model_name,$product_name,$category_id,$brand_id);
									}
									if(!empty($product_result)){
										$product_id = $product_result[0]['product_id'];
									}
									//echo $product_id."=====product_id<br>";
									if(!empty($product_id)){
										$ex_showroom = $csv_file_data[$i]['ex-showroom'];
										$registration = $csv_file_data[$i]['registration_amount'];
										$octroi = $csv_file_data[$i]['octoroi_amount'];
										$insurance = $csv_file_data[$i]['insurance_amount'];
										for($k=0;$k<4;$k++){
											if($k==0){
												$variant_value = $ex_showroom;
												$variant_id =1;
											}
											if($k==1){
												$variant_value = $registration;
												$variant_id =2;
											}
											if($k==2){
												$variant_value = $octroi;
												$variant_id =3;
											}
											if($k==3){
												$variant_value = $insurance;
												$variant_id =4;
											}
											$request_param ='';
											$request_param['category_id'] = $category_id;
											$request_param['brand_id'] = $brand_id;
											$request_param['product_id'] = $product_id;
											$request_param['country_id'] = $country_id;
											$request_param['state_id'] = $state_id;
											$request_param['city_id'] =$city_id ;
											$request_param['variant_value'] = $variant_value;
											$request_param['variant_id'] = $variant_id;
											$request_param['default_city'] = $default_city;
											$request_param['status'] = '1';
											if(!in_array($variant_id,$v_array)){
												$v_array[] = $variant_id;
												$data_exist = $price->arrGetVariantValueDetailTest("",$variant_id,$product_id,$category_id,$brand_id,$state_id,$city_id);
												if($data_exist>0){
													$price_variant = $data_exist[0]['price_variant'];
													if(!empty($price_variant)){ $request_param['price_variant'] = $price_variant;}
												}
												//echo $brand_name."---".$product_name."---".$brand_id."---".$product_id."---".$ex_showroom ."---".$registration."---".$insurance."---".$octroi."<br>";
												$insert_data = $price->intInsertUpdateVariantDetailTest($request_param);
											}
										}
										
									}else{
										  $log_error .= "\n\r $product_name  - sheet product name not matched with db product name \n\r";
										  $log_error .= implode(",",$alog_error);
										 // $fwrite = file_error_log($log_error,$city_name);
										  $data_err++;
										 // echo "TSTSTST<br>";
									}
								}else{
									$log_error .= "\n\r product name empty \n\r";
									$log_error .= implode(",",$alog_error);
									//$fwrite = file_error_log($log_error,$city_name);
									$data_err++;
								}	
						   }else{
								  $log_error .= "\n\r $brand_name sheet brand name not matched with db brand name \n\r";
								  $log_error .= implode(",",$alog_error);
								  //$fwrite = file_error_log($log_error,$city_name);
								  $data_err++;
						   }
						}else{
							//echo "brand empty<br>";
							$log_error .= "\n\r brand name empty \n\r";
							$log_error .= implode(",",$alog_error);
							//$fwrite = file_error_log($log_error,$city_name);
							$data_err++;
						}
					}
				}
				$messgae =  "Data Inserted Successfully....<br>";
			}else{
				  $log_error .= "\n\r sheet city name not matched with db city name \n\r";
				  $log_error .= implode(",",$alog_error);
				  //$fwrite = file_error_log($log_error,$city_name);
				  $data_err++;
			}
		}else{
		  $log_error .= "\n\r city name empty \n\r";
		  $log_error .= implode(",",$alog_error);
		  $data_err++;
		}
	}
print_r($data_err);
	if($data_err == 0){
			$message = "Data Inserted Successfully...";
			$start_process = "1";
	}else{
		   //echo $log_error."<br>";
			$fwrite = file_error_log($log_error,$city_name);
			$myFile = str_replace(" ","_",$city_name)."_err_log_file.xls";
			$message = "check upload sheet  <a href=".ADMIN_WEB_URL."error_log_download.php?filename=$myFile>Error Log</a>...";
			$start_process = "0";	 
	}
}


$limitcnt = $_REQUEST['cnt'];
$config_details = get_config_details();
$strXML = "<XML>";
$strXML .= "<MSG><![CDATA[$msg]]></MSG>";
$strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$brand_level]]></SELECTED_CATEGORY_ID>";
$strXML .= "<STARTLIMIT><![CDATA[$startlimit]]></STARTLIMIT>";
$strXML .= "<MESSAGE><![CDATA[$message]]></MESSAGE>";
$strXML .= "<START_PROCESS><![CDATA[$start_process]]></START_PROCESS>";
$strXML .= "<FILENAME><![CDATA[$file_name]]></FILENAME>";
$strXML .= "<CITY_ID><![CDATA[$city_id]]></CITY_ID>";
$strXML .= $config_details;
$strXML .= $xml;
$strXML .= "</XML>";

//header('Content-type: text/xml');echo $strXML;exit;
$doc = new DOMDocument();
$doc->loadXML($strXML);
$doc->saveXML();
$xslt = new xsltProcessor;
$xsl = DOMDocument::load('xsl/upload_sheet.xsl');
$xslt->importStylesheet($xsl);
print $xslt->transformToXML($doc);
?>
