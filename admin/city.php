<?php
	require_once('../include/config.php');
	require_once(CLASSPATH.'DbConn.php');
	require_once(CLASSPATH.'citystate.class.php');
	
	$dbconn = new DbConn;
	$oCityState = new citystate;
	
	//print "<pre>"; print_r($_REQUEST);
	$startlimit = $_REQUEST['startlimit'];
	$limitcnt = $_REQUEST['cnt'];	
	$city_id = $_REQUEST['city_id'];	
	if(!empty($city_id)){
		$insert_param['city_id'] = $city_id;
	}
	$state_id =  $_REQUEST['state_id'] ? $_REQUEST['state_id']  : $_REQUEST['select_state_id'];
	if(!empty($state_id)){
		$insert_param['state_id'] = $state_id;
	}
	$city_name = trim($_REQUEST['city_name']);
	if(!empty($city_name)){
		$insert_param['city_name'] = htmlentities($city_name,ENT_QUOTES);
	}
	$status = $_REQUEST['city_status'];
	if($status != ''){
		$insert_param['city_status'] = $status;
	}
	$actiontype = $_REQUEST['actiontype'];
	$country_id = $_REQUEST['country_id'] ? $_REQUEST['country_id'] : COUNTRYID ;
	if($actiontype == 'Delete'){
	   $result = $oCityState->boolDeleteCity($city_id);
	   $msg = 'City deleted successfully.';
	}
	if($actiontype == 'Insert'|| $actiontype== 'Update'){		
	    $result = $oCityState->intInsertUpdateCity($insert_param);
	    $msg = 'City added successfully.';
	}
	
	
	$result = $oCityState->arrGetCityDetails("",$state_id,"",$startlimit,$limitcnt);	
	$cnt = sizeof($result);
	$xml = "<CITY_MASTER>";
	$xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
	for($i=0;$i<$cnt;$i++){
		$status = $result[$i]['city_status'];
		$result[$i]['city_status'] = ($status == 1) ? 'Active' : 'InActive';
		$result[$i]['create_date'] = date('d-m-Y',strtotime($result[$i]['create_date']));
		$result[$i]['js_city_name'] = htmlentities($result[$i]['city_name'],ENT_QUOTES);
		$result[$i]['city_name'] = html_entity_decode($result[$i]['city_name'],ENT_QUOTES);
		$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
		$xml .= "<CITY_MASTER_DATA>";
		foreach($result[$i] as $k=>$v){
			$xml .= "<$k><![CDATA[$v]]></$k>";
		}
		$xml .= "</CITY_MASTER_DATA>";
	}
	$xml .= "</CITY_MASTER>";
	
	if(!empty($country_id)){
		$result = $oCityState->arrGetStateDetails("","","1",$startlimit,$limitcnt);
	}	
	$cnt = sizeof($result);
	for($i=0;$i<$cnt;$i++){
		$country_id = $result[$i]['country_id'];
		$istate_id = $result[$i]['state_id'];
		$state_name = $result[$i]['state_name'];
		if($istate_id==$state_id){
			$sStateStr.= "<OPTION value='$istate_id' selected='selected'>".$state_name."</OPTION>";
		}else{
			$sStateStr.= "<OPTION value='$istate_id'>".$state_name."</OPTION>";
		}
	}
	$sStateXml="<SSTATE><![CDATA[".$sStateStr."]]></SSTATE>";

	$config_details = get_config_details();
	$strXML = "<XML>";
	$strXML .= "<MSG><![CDATA[$msg]]></MSG>";
	$strXML .= "<STARTLIMIT><![CDATA[$startlimit]]></STARTLIMIT>";
	$strXML .= "<CNT><![CDATA[$limitcnt]]></CNT>";
	$strXML .= $config_details;
	$strXML .= $xml;
	$strXML .= $sStateXml;
	$strXML .= "</XML>";

	$strXML = mb_convert_encoding($strXML, "UTF-8");
	if($_GET['debug']==1) { header('Content-type: text/xml');echo $strXML;exit; }

	$doc = new DOMDocument();
	$doc->loadXML($strXML);
	$doc->saveXML();

	$xslt = new xsltProcessor;
	$xsl = DOMDocument::load('xsl/city.xsl');

	$xslt->importStylesheet($xsl);
	print $xslt->transformToXML($doc);
?>
