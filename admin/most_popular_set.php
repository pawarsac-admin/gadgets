<?php
require_once('../include/config.php');
require_once(CLASSPATH.'DbConn.php');
require_once(CLASSPATH.'product.class.php');

$dbconn = new DbConn;
$oProduct = new ProductManagement;

//if($_POST){ print_r($_REQUEST);}  //die();
$actiontype = $_REQUEST['actiontype'] ? $_REQUEST['actiontype'] : "Insert";
$startlimit = $_REQUEST['startlimit'];
$limitcnt = $_REQUEST['cnt'];

$compare_id=$_REQUEST['compare_id'];
if(!empty($compare_id)){
	$request_param['compare_id']=$compare_id;
}
$product_id=$_REQUEST['product_id'];
if(!empty($product_id)){
	$request_param['product_id']=$product_id;
}
$category_id=$_REQUEST['selected_category_id'];
if(!empty($category_id)){
	$request_param['category_id']=$category_id;
}
$status=$_REQUEST['status'];
if($status!=""){
	$request_param['status']=$status;
}
$media_id=$_REQUEST['media_id'];
if($media_id!=""){
	$request_param['media_id']=$media_id;
}
$compare_set_img=$_REQUEST['img_upload_1'];
if($compare_set_img!=""){
	$request_param['compare_set_img']=$compare_set_img;
}
$position=$_REQUEST['position'];
if($position!=""){
	$request_param['position']=$position;
}

if($_REQUEST['comp_product_id_1']!='' && strlen($_REQUEST['comp_product_id_1'])>0){
	$comp_set1=$_REQUEST['comp_product_id_1'];
	$comp_set2=$_REQUEST['comp_product_id_2'];
	if($comp_set1!='' && $comp_set2!=''){
		$comp_set=$comp_set1.",".$comp_set2;
	}
	$request_param['compare_set']=$comp_set;
}
if($actiontype=="Insert"){
	if($request_param!=''){
		$iResId=$oProduct->addUpdMostPopularSetDetails($request_param);
		$msg="Most Popular set detail added successfully.";
	}
}
else if($actiontype=="Update"){
	if($request_param!=''){
		$iResId=$oProduct->addUpdMostPopularSetDetails($request_param);
		$msg="Most Popular set detail updatded successfully.";
	}

}
else if($actiontype=="Delete"){
	$result = $oProduct->boolDeleteMostPopularSetDetail($compare_id);
	$msg = 'Most Popular set  Detail deleted successfully.';
}

$config_details = get_config_details();
$strXML = "<?xml version='1.0' encoding='iso-8859-1'?>";
$strXML .= "<XML>";
$strXML .= "<MSG><![CDATA[$msg]]></MSG>";
$strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$category_id]]></SELECTED_CATEGORY_ID>";
$strXML .= "<SELECTED_ACTION_TYPE><![CDATA[$actiontype]]></SELECTED_ACTION_TYPE>";
$strXML .= "<STARTLIMIT><![CDATA[$startlimit]]></STARTLIMIT>";
$strXML .= "<CNT><![CDATA[$limitcnt]]></CNT>";
$strXML .= $config_details;
$strXML .= $xml;
$strXML .= "</XML>";

$strXML = mb_convert_encoding($strXML, "UTF-8");
if($_GET['debug']==1) { header('Content-type: text/xml');echo $strXML;exit; }

$doc = new DOMDocument();
$doc->loadXML($strXML);
$doc->saveXML();

$xslt = new xsltProcessor;
$xsl = DOMDocument::load('xsl/most_popular_set.xsl');

$xslt->importStylesheet($xsl);
print $xslt->transformToXML($doc);
?>