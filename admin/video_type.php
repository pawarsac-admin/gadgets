<?php
        require_once('../include/config.php');
        require_once(CLASSPATH.'DbConn.php');
	require_once(CLASSPATH.'videos.class.php');
	
        $dbconn = new DbConn;
	$videos = new videos; 
	
	//if($_POST){ print_r($_REQUEST);} //die();
	$actiontype = $_REQUEST['actiontype'];
	$startlimit = $_REQUEST['startlimit'];
	$limitcnt = $_REQUEST['cnt'];
	$request_param=$_REQUEST;
	$category_id = $_REQUEST['selected_category_id'];
	$category_id = ($category_id != "") ? $category_id : SITE_CATEGORY_ID;
	$selected_video_type_id = $_REQUEST["select_section_id"] ? $_REQUEST['select_section_id'] : $_REQUEST['hd_view_section_id'];

	if($actiontype == 'Insert'|| $actiontype== 'Update'){
		unset($request_param);
		
		$video_sub_type_arr = Array();
		$video_sub_type_arr = $_REQUEST["select_video_sub_type_id"];
		$cnt = sizeof($video_sub_type_arr);
		for($i=0;$i<$cnt;$i++){	
			$video_sub_type_id = $video_sub_type_arr[$i];
			$hd_view_section_id = $_REQUEST["hd_view_section_id"];
			$select_video_id =  $_REQUEST["select_video_id"];
			$select_section_id = $_REQUEST["select_section_id"] ? $_REQUEST["select_section_id"] : $hd_view_section_id;
			$status = $_REQUEST["status"];

			$request_param['video_type_id'] = $select_section_id;
			$request_param['video_sub_type_id'] = $video_sub_type_id;
			$request_param['category_id'] = $category_id;
			$request_param['video_id'] = $select_video_id;
			
			$request_param['status'] = $status;
			
                        //print"<pre>";print_r($request_param);print"</pre>";
			if($actiontype == 'Insert'){
			       $result = $videos->intInsertVideoType($request_param);
                        }elseif($actiontype == 'Update'){
                        	if($i == 0){
                                        $id = $_REQUEST['hd_id'];
                                        //print"<pre>";print_r($request_param);print"</pre>";
                                        $result = $videos->boolUpdateVideoType($id,$request_param);
                                }else{
                                	$result = $videos->intInsertVideoType($request_param);
                                }
			}	
		}
		if($sresult>0){
                        if($actiontype == 'Insert'){
                                $msg = 'video type added successfully.';
                        }else{
                                $msg = 'video type updated successfully.';
                        }
                }
	}

	if($actiontype == 'Delete'){
		$id = $_REQUEST["hd_id"];
        	if($id!=''){
                	$result = $videos->booldeleteVideoType($id,"VIDEO_TYPE_GALLERY");
	                $msg = 'video type deleted successfully.';
        	}
	}

	$config_details = get_config_details();

        $strXML = "<XML>";
        $strXML .= "<MSG><![CDATA[$msg]]></MSG>";
        $strXML .= "<SELECTED_MENU_ID><![CDATA[$menu_level]]></SELECTED_MENU_ID>";
        $strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$category_id]]></SELECTED_CATEGORY_ID>";
	$strXML .= "<SELECTED_ACTION_TYPE><![CDATA[$actiontype]]></SELECTED_ACTION_TYPE>";
	$strXML .= "<VIEWSECTION><![CDATA[$selected_video_type_id]]></VIEWSECTION>";
        $strXML .= "<STARTLIMIT><![CDATA[$startlimit]]></STARTLIMIT>";
        $strXML .= "<CNT><![CDATA[$limitcnt]]></CNT>";
        $strXML .= $config_details;
        $strXML .= $xml;
        $strXML .= "</XML>";

	if($_GET['debug']==1) { header('Content-type: text/xml');echo $strXML;exit; }

        $doc = new DOMDocument();
        $doc->loadXML($strXML);
        $doc->saveXML();

        $xslt = new xsltProcessor;
        $xsl = DOMDocument::load('xsl/video_type.xsl');

        $xslt->importStylesheet($xsl);
        print $xslt->transformToXML($doc);

?>

