<?php
require_once('../include/config.php');
require_once(CLASSPATH.'DbConn.php');
require_once(CLASSPATH.'dealer.class.php');

$dbconn = new DbConn;
$oDealer = new Dealer;

$actiontype = $_REQUEST['actiontype'];
$startlimit = $_REQUEST['startlimit'];
$limitcnt = $_REQUEST['cnt'];
$category_id = $_REQUEST['selected_category_id'];
$hd_view_section_id = $_REQUEST["hd_view_section_id"];
$select_section_id = $_REQUEST["select_section_id"] ? $_REQUEST["select_section_id"] : $hd_view_section_id;

//print "<pre>"; print_r($_REQUEST); 
//die();

if($actiontype == 'Insert'|| $actiontype== 'Update'){
        if($_REQUEST['display_rows']>0){
                unset($request_param);
                $dataCount=$_REQUEST['display_rows'];
                for($i=1;$i<=$dataCount;$i++){
			unset($request_param);
                        $black_list_number = $_REQUEST['black_list_number_'.$i];
                        $black_list_number = trim($black_list_number);
                        if(!empty($black_list_number)){ $request_param['black_list_number'] = htmlentities($black_list_number,ENT_QUOTES);}
			
			$black_list_number_id = $_REQUEST['black_list_number_id_'.$i];
                        if(!empty($black_list_number_id)){ $request_param['black_list_number_id'] = $black_list_number_id;}

			$type_id = $_REQUEST['select_section_id'];
                        if(!empty($type_id)){ $request_param['type_id'] = $type_id;}

			$status = $_REQUEST['status'];
                        if($status!=''){ $request_param['status'] = $status;}
			
			$result = $oDealer->addUpdBlackListNumbersDetails($request_param);
			
			if($sresult>0 && $actiontype == 'Insert'){
		                $msg = 'Black List Numbers added successfully.';
		        }	
			if($sresult>0 && $actiontype == 'Update'){
		                $msg = 'Black List Numbers updated successfully.';
		        }	
		}
	}
}

if($actiontype == 'Delete'){
        $black_list_number_id = $_REQUEST['black_list_number_id'];
        if($black_list_number_id!=''){
                $result = $oDealer->boolDeleteBlackListNumbers($black_list_number_id);
                $msg = 'Black List Numbers deleted successfully.';
        }
}

$config_details = get_config_details();
$strXML = "<XML>";
$strXML .= "<MSG><![CDATA[$msg]]></MSG>";
$strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$category_id]]></SELECTED_CATEGORY_ID>";
$strXML .= "<STARTLIMIT><![CDATA[$startlimit]]></STARTLIMIT>";
$strXML .= "<SELECTED_ACTION_TYPE><![CDATA[$actiontype]]></SELECTED_ACTION_TYPE>";
$strXML .= "<VIEWSECTION><![CDATA[$select_section_id]]></VIEWSECTION>";
$strXML .= "<CNT><![CDATA[$limitcnt]]></CNT>";
$strXML .= $config_details;
$strXML .= $xml;
$strXML .= "</XML>";

$strXML = mb_convert_encoding($strXML, "UTF-8");
if($_GET['debug']==1) { header('Content-type: text/xml');echo $strXML;exit; }

$doc = new DOMDocument();
$doc->loadXML($strXML);
$doc->saveXML();

$xslt = new xsltProcessor;
$xsl = DOMDocument::load('xsl/black_listed_numbers.xsl');

$xslt->importStylesheet($xsl);
print $xslt->transformToXML($doc);
?>
