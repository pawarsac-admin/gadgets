<?php
	require_once('../include/config.php');
	require_once(CLASSPATH.'DbConn.php');
	require_once(CLASSPATH.'article.class.php');

	$dbconn = new DbConn;
	$oArticle = new article;

	//if($_POST){ 
		//print_r($_REQUEST); 
		//die();
	//}
	$editor_id = $_REQUEST['editor_id'];
	$publish_time = $_REQUEST['publish_time'];
	$actiontype = $_REQUEST['actiontype'];
	$startlimit = $_REQUEST['startlimit'];
	$limitcnt = $_REQUEST['cnt'];

	$iAId = $_REQUEST['article_id'];
	$iCategoryId = $_REQUEST['selected_category_id'] ? $_REQUEST['selected_category_id'] : $_REQUEST['catid'];
        $iCategoryId = ($iCategoryId != "") ? $iCategoryId : SITE_CATEGORY_ID;

	if($actiontype == 'Insert' || $actiontype== 'Update'){
		$sTitle = translatechars($_REQUEST['article_title']);	
		$sTitle = trim($sTitle);
		if(!empty($sTitle)){ $request_param['title'] = htmlentities($sTitle,ENT_QUOTES);}
		
		$sDescription = translatechars($_REQUEST['article_description']);	
		if(!empty($sDescription)){ $request_param['content'] = htmlentities($sDescription,ENT_QUOTES);}
		
		$sAbstract = $_REQUEST['article_abstract'] ? $_REQUEST['article_abstract'] : $_REQUEST['mce_article_abstract'];	
		$sAbstract = translatechars($sAbstract);

		$sAbstract = htmlentities($sAbstract,ENT_QUOTES);
		$request_param['abstract'] = addslashes($sAbstract);
		
		if(!empty($iAId)){ $request_param['article_id']=$iAId;}
		
		$sStatus=$_REQUEST['article_status'];
		if($sStatus!=''){ $request_param['status']=$sStatus;}

		/*commented by Rajesh on dated 15-01-2011.Moved content to UPLOAD_MEDIA_ARTICLE
		$sMediaId=$_REQUEST['media_id'];
		if(!empty($sMediaId)){ $request_param['media_id']=$sMediaId;}

		$sImgUpload1=$_REQUEST['img_upload_1'];
		if(!empty($sImgUpload1)){ $request_param['video_path']=$sImgUpload1;}	
		*/

		$sImgMediaId = $_REQUEST['abstract_img_id'];
		if(!empty($sImgMediaId)){ $request_param['img_media_id']=$sImgMediaId;}

		
		$sImgThmUpload = trim($_REQUEST['abstract_img_path']);
		if(!empty($sImgThmUpload)){ $request_param['image_path']=$sImgThmUpload;}		
		
		$sTags = trim($_REQUEST['article_tags']);
		if(!empty($sTags)){ $request_param['tags']=$sTags;}	

		$sArtType = $_REQUEST['select_aritcle_type_id'];
		if(!empty($sArtType)){ $request_param['article_type']=$sArtType;}		
		
		//print "<pre>"; print_r($request_param);
		$request_param['uid'] = $editor_id;
		$request_param['publish_time'] = $publish_time;
		$iAId=$oArticle->addUpdArticleDetails($request_param,"ARTICLE");
		$iAId = ($iAId!=0) ? $iAId : $_REQUEST['article_id']; 
		if($iAId!=''){
			$iPrdAId=$_REQUEST['product_article_id'];

			if(!empty($iPrdAId)){ $request_param1['product_article_id']=$iPrdAId;}
			
			if(!empty($iAId)){ $request_param1['article_id']=$iAId;}
			
			$iGroupId=$_REQUEST['select_aritcle_group_id'];
			if(!empty($iGroupId)){ $request_param1['group_id']=$iGroupId;}
			
			
			if(!empty($iCategoryId)){ $request_param1['category_id']=$iCategoryId;}
			
			$iBrandId=$_REQUEST['select_brand_id'];
			if(!empty($iBrandId)){ $request_param1['brand_id']=$iBrandId;}
			
			$iProductId=$_REQUEST['product_id'];
			if(!empty($iProductId)){ $request_param1['product_id']=$iProductId;}
			
			$iModelId=$_REQUEST['select_model_id'];
			if(!empty($iModelId)){ $request_param1['product_info_id']=$iModelId;}
		
			$iProdArtResId = $oArticle->addUpdArticleDetails($request_param1,"PRODUCT_ARTICLE");
			
			$iPrdAId = ($iPrdAId == 0) ? $iProdArtResId : $_REQUEST['product_article_id'];
			//echo "here == $iPrdAId";
			//print_r($_REQUEST);
			if($iPrdAId!=''){
				if($_REQUEST['display_rows']>0){
					unset($request_param);
					$dataCount=$_REQUEST['display_rows'];
					
					for($i=1;$i<=$dataCount;$i++){
						$upload_media_id = $_REQUEST['upload_media_id_'.$i];

												
						if(!empty($upload_media_id)){
							//echo "$upload_media_i === $upload_media_i";
							$request_param['upload_media_id'] = $upload_media_id;
						}

						$image_title = $_REQUEST['image_title_'.$i];
                        			$image_title=htmlentities($image_title,ENT_QUOTES);
						//if(!empty($image_title)){ 
							$request_param['image_title'] = $image_title;	
						//}

						$request_param['product_article_id'] = $iPrdAId;

						$media_id = $_REQUEST['media_id_'.$i];
						if(!empty($media_id)){
							$request_param['media_id'] = $media_id;	
						}

						$media_path = $_REQUEST['media_upload_1_'.$i];
						$media_path = trim($media_path);
						/*
						if(!empty($media_path)){
							$request_param['media_path'] = $media_path;
						}						
						*/
						$media_img_id = $_REQUEST['img_media_id_'.$i];
						if(!empty($media_img_id)){
							$request_param['video_img_id'] = $media_img_id;
						}

						$media_img_path = $_REQUEST['img_upload_id_thm_'.$i];
						$media_img_path = trim($media_img_path);
						if(!empty($media_img_path )){
							$request_param['video_img_path'] = $media_img_path;
						}
						$content = $_REQUEST['article_description_'.$i] ? $_REQUEST['article_description_'.$i] : $_REQUEST['mce_article_description_'.$i];
						$content = translatechars($content);
						$content = htmlentities($content,ENT_QUOTES);
						$request_param["content"] = addslashes($content);

						$content_type = !empty($_REQUEST['video_content_type_'.$i]) ? $_REQUEST['video_content_type_'.$i] : $_REQUEST['content_type_'.$i];
						
						//if(!empty($content_type)){
							$request_param['content_type'] = $content_type;
						//}
						
						
						if($content_type == 1){
							//for video
							if(!empty($media_img_path) && empty($media_path)){
								$request_param['media_path'] = $media_img_path;
								$request_param['is_media_process'] = 0;
							}

						}else if($content_type == 2){
							//for image
							if(!empty($media_path)){
								$request_param['media_path'] = $media_path;
								$request_param['video_img_path'] = $media_path;
							}
							if(!empty($media_id)){
								$request_param['video_img_id'] = $media_id;
							}
							$request_param['is_media_process'] = 1;
						}else if($content_type == 3){
							//for audio
							if(!empty($media_img_path) && empty($media_path)){
								$request_param['media_path'] = $media_img_path;
								$request_param['is_media_process'] = 0;
							}
						}
						
						$check_flag = $_REQUEST['check_flag_'.$i];
						if($check_flag == 1){
								$request_param['media_id'] = "";
								$request_param['media_path'] = "";
								$request_param['video_img_id']="";
								$request_param['video_img_path'] = "";
								$request_param['content_type'] = "";
								$request_param['is_media_process'] = "";
						}
   
						//if(!empty($media_id)){							
							$iProdArtMediaId = $oArticle->addUpdArticleDetails($request_param,"UPLOAD_MEDIA_ARTICLE");
						//}
						// print "<pre>";
	                   // print_r($request_param);
						unset($request_param);
					}
				}
			}
		}
		if($iAId==0) {
			$iAId=$iResId;
			if($actiontype == 'Insert'){
				$msg="News detail added successfully.";
			}
		}else {
			if($actiontype == 'Update'){
				$msg="News detail updated successfully.";
			}
		}
	}else if($actiontype == 'Delete'){
		$result = $oArticle->deleteArticle($iAId);
		$msg = 'Article deleted successfully.';
	}

	
	if(!empty($_REQUEST['article_id']) && $_REQUEST['act']="update"){
		$act=$_REQUEST['act'];
		$article_id=$_REQUEST['article_id'];
		$article_result = $oArticle->getArticleDetails($article_id,"","","","",$iCategoryId,"","1");
		$iCnt = sizeof($article_result);
		if(is_array($article_result)){
			$article_id = $article_result['0']['article_id'];
			$brand_id = $article_result['0']['brand_id'];
			$product_id = $article_result['0']['product_id'];
			$product_info_id = $article_result['0']['product_info_id'];
			$product_article_id = $article_result['0']['product_article_id'];
			$article_type = $article_result['0']['article_type'];
			unset($article_result);
		}
	}

	$config_details = get_config_details();
	$strXML = "<XML>";
	$strXML .= "<MSG><![CDATA[$msg]]></MSG>";
	$strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$iCategoryId]]></SELECTED_CATEGORY_ID>";
	$strXML .= "<STARTLIMIT><![CDATA[$startlimit]]></STARTLIMIT>";
	$strXML .= "<CNT><![CDATA[$limitcnt]]></CNT>";
	$strXML .= "<ACT><![CDATA[$act]]></ACT>";
	if($_REQUEST['act']="update"){ 
		$strXML .= "<ARTICLE_ID><![CDATA[$article_id]]></ARTICLE_ID>";
		$strXML .= "<BRAND_ID><![CDATA[$brand_id]]></BRAND_ID>";
		$strXML .= "<PRODUCT_ID><![CDATA[$product_id]]></PRODUCT_ID>";
		$strXML .= "<PRODUCT_INFO_ID><![CDATA[$product_info_id]]></PRODUCT_INFO_ID>";
		$strXML .= "<PRODUCT_ARTICLE_ID><![CDATA[$product_article_id]]></PRODUCT_ARTICLE_ID>";
		$strXML .= "<ARTICLE_TYPE><![CDATA[$article_type]]></ARTICLE_TYPE>";
	}
	
	$strXML .= $config_details;
	$strXML .= $xml;
	$strXML .= $ArtXMl;
	$strXML .= "</XML>";

	$strXML = mb_convert_encoding($strXML, "UTF-8");
	if($_GET['debug']==1) { header('Content-type: text/xml');echo $strXML;exit; }

	$doc = new DOMDocument();
	$doc->loadXML($strXML);
	$doc->saveXML();

	$xslt = new xsltProcessor;
	$xsl = DOMDocument::load('xsl/article.xsl');

	$xslt->importStylesheet($xsl);
	print $xslt->transformToXML($doc);
?>
