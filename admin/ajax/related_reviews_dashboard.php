<?php	
require_once('../../include/config.php');
require_once(CLASSPATH.'DbConn.php');
require_once(CLASSPATH.'category.class.php');
require_once(CLASSPATH.'product.class.php');
require_once(CLASSPATH.'brand.class.php');
require_once(CLASSPATH.'reviews.class.php');

$dbconn = new DbConn;
$category = new CategoryManagement;
$product = new ProductManagement;
$brand = new BrandManagement;
$oReviews = new reviews;


$category_id = $_REQUEST['catid'] ? $_REQUEST['catid']: $_REQUEST['selected_category_id'];

$startlimit = $_REQUEST['startlimit'];
$limitcnt = $_REQUEST['cnt'];
$r_product_id = $_REQUEST['pid'];
$r_brand_id = $_REQUEST['bid'];
$type_selecetd=$_REQUEST['article_type_id'] ? $_REQUEST['article_type_id'] :0;

$view_section_id=$_REQUEST['view_section_id'] ? $_REQUEST['view_section_id'] :"LATEST_REVIEWS";
$section_id=$_REQUEST['section_review_id'] ? $_REQUEST['section_review_id'] :"1";


$review_id =$_REQUEST['rid'] ? $_REQUEST['rid']  : $_REQUEST['review_id'];


if(!empty($category_id)){
	$aReviewsGroupDetail = $oReviews->arrGetReviewsGroupDetails("","",$category_id,"1");
	if(is_array($aReviewsGroupDetail)){
		foreach($aReviewsGroupDetail as $igKey=>$aGrpValue){
			$aReviewsGroupData[$aGrpValue['group_id']]=$aGrpValue['group_name'];
		}
	}
	$sReviewsGroupDetail=arraytoxml($aReviewsGroupDetail,"REVIEWS_GROUP_DATA");
	$sReviewsGroupDetailXML ="<REVIEWS_GROUP_MASTER>".$sReviewsGroupDetail."</REVIEWS_GROUP_MASTER>";
	/*
	$aReviewsTypeDetail = $oReviews->arrGetReviewsTypeDetails("","",$category_id,"1");
	if(is_array($aReviewsTypeDetail)){
		foreach($aReviewsTypeDetail as $itKey=>$aTypeValue){
			$aReviewsTypeData[$aTypeValue['article_type_id']]=$aTypeValue['type_name'];
		}
	}
	$sReviewsTypeDetail=arraytoxml($aReviewsTypeDetail,"REVIEWS_TYPE_DATA");
	$sReviewsTypeDetailXML ="<REVIEWS_TYPE_MASTER>".$sReviewsTypeDetail."</REVIEWS_TYPE_MASTER>";
	*/
}


//$aReviewsSectionDet=array("0"=>array("SECTION_ID"=>"LATEST_REVIEWS","SECTION_NAME"=>"Latest Reviews"),"1"=>array("SECTION_ID"=>"FEATURED_REVIEWS","SECTION_NAME"=>"Featured Reviews"),"2"=>array("SECTION_ID"=>"RELATED_REVIEWS","SECTION_NAME"=>"Related Reviews"));

$aReviewsSectionDet=array("0"=>array("SECTION_ID"=>"FEATURED_REVIEWS","SECTION_NAME"=>"Featured Reviews"));



$sReviewsSectionDetail=arraytoxml($aReviewsSectionDet,"REVIEWS_SECTION");
$sReviewsSectionDetailXML ="<REVIEWS_SECTION_MASTER>".$sReviewsSectionDetail."</REVIEWS_SECTION_MASTER>";
//echo $view_section_id."DXASDSDSDS";

	if($category_id!=''){
		if($view_section_id=="LATEST_REVIEWS"){
			$result=$oReviews->arrGetLatestReviewsDetails("","","",$category_id,"");
		}
		if($view_section_id=="FEATURED_REVIEWS"){
			$result=$oReviews->arrGetFeaturedReviewsDetails("","","",$category_id,"");
		}
		if($view_section_id=="RELATED_REVIEWS"){
			$result=$oReviews->arrGetRelatedReviewsDetails("","","",$category_id,"");
		}
	}
	//print "<pre>"; print_r($result);

	if($_REQUEST['section_review_id']!=''){	$section_id = $_REQUEST['section_review_id'];}

	if($_REQUEST['act']=='update' && !empty($section_id)){

		//echo $view_section_id."ETERE"; echo $section_id; die();
		if($view_section_id=="LATEST_REVIEWS"){
			//echo "EEEEEEE".$section_id;
			$rResult=$oReviews->arrGetLatestReviewsDetails($section_id,"","",$category_id,"");
		}
		if($view_section_id=="FEATURED_REVIEWS"){
			$rResult=$oReviews->arrGetFeaturedReviewsDetails($section_id,"","",$category_id,"");
		}
		if($view_section_id=="RELATED_REVIEWS"){
			$rResult=$oReviews->arrGetRelatedReviewsDetails($section_id,"","",$category_id,"");
		}
		//print_r($rResult);
		$xmlArt='';
		$cnt = sizeof($rResult);
		$status = $rResult[0]['status'];
		$categoryid = $rResult[0]['category_id'];
		if(!empty($categoryid)){
			$category_result = $category->arrGetCategoryDetails($categoryid);
		}
		$rResult[0]['js_title'] = $rResult[0]['title'];
		$rResult[0]['title'] = $rResult[0]['title'] ? html_entity_decode($rResult[0]['title'],ENT_QUOTES) : 'Nil';
		$rResult[0]['status'] = ($status == 1) ? 'Active' : 'InActive';
		$category_name = $category_result[0]['category_name'];
		$rResult[0]['js_category_name'] = $category_name;
		$rResult[0]['category_name'] = html_entity_decode($category_name,ENT_QUOTES);
		$rResult[0]['create_date'] = date('d-m-Y',strtotime($rResult[0]['create_date']));
		$rResult[0] = array_change_key_case($rResult[0],CASE_UPPER);
		//print "<pre>"; print_r($rResult);
		$xmlArt .= "<REVIEWS_DATA>";
		foreach($rResult[0] as $k1=>$v1){
			$xmlArt .= "<$k1><![CDATA[$v1]]></$k1>";
		}
		$xmlArt .= "</REVIEWS_DATA>";
	}


	$cnt = sizeof($result);
	if(is_array($result)){
	$xml = "<REVIEWS_MASTER>";
	$xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";	
	for($i=0;$i<$cnt;$i++){
		$status = $result[$i]['status'];
		$categoryid = $result[$i]['category_id'];
		$categoryid=1;
		if(!empty($categoryid)){
			$category_result = $category->arrGetCategoryDetails($categoryid);
		}
		$review_type = $result[$i]['review_type'];
		if(!empty($review_type)){
			if(is_array($aReviewsTypeData) && isset($aReviewsTypeData[$review_type])){
				$result[$i]['article_type_name']=$aReviewsTypeData[$review_type];
			}
		}
		$result[$i]['js_title'] = $result[$i]['title'];
		$result[$i]['title'] = $result[$i]['title'] ? html_entity_decode($result[$i]['title'],ENT_QUOTES) : 'Nil';
		$result[$i]['js_title'] = $result[$i]['title'];
		$result[$i]['title'] = $result[$i]['title'] ? html_entity_decode($result[$i]['title'],ENT_QUOTES) : 'Nil';
		$result[$i]['status'] = ($status == 1) ? 'Active' : 'InActive';
		$category_name = $category_result[0]['category_name'];
		$result[$i]['js_category_name'] = $category_name;
		$result[$i]['category_name'] = html_entity_decode($category_name,ENT_QUOTES);
		$result[$i]['create_date'] = date('d-m-Y',strtotime($result[$i]['create_date']));
		
		if(is_array($aReviewsTypeData) && isset($aReviewsTypeData[$result[$i]['review_type']])){
			$result[$i]['reviews_type_name'] =$aReviewsTypeData[$result[$i]['review_type']];	
		}
		$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
		$xml .= "<REVIEWS_MASTER_DATA>";
		foreach($result[$i] as $k=>$v){
			$xml .= "<$k><![CDATA[$v]]></$k>";
		}
		$xml .= "</REVIEWS_MASTER_DATA>";
	}
	$xml .= "</REVIEWS_MASTER>";
	}

if(!empty($category_id)){
	$result = $brand->arrGetBrandDetails("",$category_id);
}	
$cnt = sizeof($result);
$xml .= "<BRAND_MASTER>";
$xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
for($i=0;$i<$cnt;$i++){
	$status = $result[$i]['status'];
	$categoryid = $result[$i]['category_id'];
	if(!empty($categoryid)){
		$category_result = $category->$result = $category->arrGetCategoryDetails($categoryid);
	}
	$category_name = $category_result[0]['category_name'];
	$result[$i]['js_category_name'] = $category_name;
	$result[$i]['category_name'] = html_entity_decode($category_name,ENT_QUOTES);
	$result[$i]['brand_status'] = ($status == 1) ? 'Active' : 'InActive';
	$result[$i]['create_date'] = date('d-m-Y',strtotime($result[$i]['create_date']));
	$result[$i]['js_brand_name'] = $result[$i]['brand_name'];
	$result[$i]['brand_name'] = html_entity_decode($result[$i]['brand_name'],ENT_QUOTES);
	$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
	$xml .= "<BRAND_MASTER_DATA>";
	foreach($result[$i] as $k=>$v){
		$xml .= "<$k><![CDATA[$v]]></$k>";
	}
	$xml .= "</BRAND_MASTER_DATA>";
}
$xml .= "</BRAND_MASTER>";



//$result = $oReviews->getProductReviewsList("",'','REVIEWS');
$result = $oReviews->getReviewsDetails("","","","","",$category_id,"","1");
//print_r($result);
$cnt = sizeof($result);
$xmlstr .= "<SELECT_REVIEW_MASTER>";
$xmlstr .= "<COUNT><![CDATA[$cnt]]></COUNT>";
for($i=0;$i<$cnt;$i++){
	$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
	$xmlstr .= "<SELECT_REVIEW_MASTER_DATA>";
	foreach($result[$i] as $k=>$v){
		$xmlstr .= "<$k><![CDATA[$v]]></$k>";
	}
	$xmlstr .= "</SELECT_REVIEW_MASTER_DATA>";
}
$xmlstr .= "</SELECT_REVIEW_MASTER>";



$iRelUploadCnt= $iRelUploadCnt ? $iRelUploadCnt :1;
$config_details = get_config_details();

$strXML = "<XML>";
$strXML .= "<MSG><![CDATA[$msg]]></MSG>";
$strXML .= "<STARTLIMIT><![CDATA[$startlimit]]></STARTLIMIT>";
$strXML .= "<CNT><![CDATA[$limitcnt]]></CNT>";
$strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$category_id]]></SELECTED_CATEGORY_ID>"; 
$strXML .= $config_details;
$strXML .= $xml;
$strXML .= $xmlArt;
$strXML .= $xmlstr;
$strXML .= $sReviewsGroupDetailXML;
$strXML .= $sReviewsTypeDetailXML;
$strXML .= $sReviewsMediaDataDetXML;
$strXML .= $sReviewsSectionDetailXML;
$strXML .= "<WALLCNT><![CDATA[$iRelUploadCnt]]></WALLCNT>";
$strXML .= "<VIEWSECTION><![CDATA[$view_section_id]]></VIEWSECTION>";
$strXML .= "<ARTICLETYPE><![CDATA[$type_selecetd]]></ARTICLETYPE>";
$strXML .= "</XML>";

$strXML = mb_convert_encoding($strXML, "UTF-8");
if($_GET['debug']==1) { header('Content-type: text/xml');echo $strXML;exit; }

$doc = new DOMDocument();
$doc->loadXML($strXML);
$doc->saveXML();

$xslt = new xsltProcessor;
$xsl = DOMDocument::load('../xsl/related_reviews_dashboard.xsl');

$xslt->importStylesheet($xsl);
print $xslt->transformToXML($doc);
?>