<?php
require_once('../../include/config.php');
require_once(CLASSPATH.'DbConn.php');
require_once(CLASSPATH.'category.class.php');
require_once(CLASSPATH.'product.class.php');
require_once(CLASSPATH.'brand.class.php');
require_once(CLASSPATH.'feature.class.php');
require_once(CLASSPATH.'pivot.class.php');

$dbconn = new DbConn;
$category = new CategoryManagement;
$product = new ProductManagement;
$brand = new BrandManagement;
$feature = new FeatureManagement;
$pivot = new PivotManagement;

//print "<pre>"; print_r($_REQUEST);
$category_id = $_REQUEST['catid'];
$category_id = ($category_id != "") ? $category_id : SITE_CATEGORY_ID;
$startlimit = $_REQUEST['startlimit'];
$limitcnt = $_REQUEST['cnt'];
$actiontype = $_REQUEST['actiontype'] ;

$popular_feature_id =$_REQUEST['popular_feature_id'];
$selected_pivot_id = $_REQUEST['pivot_id'];
$selected_pivot_group = $_REQUEST['pivot_group'];


if($_REQUEST['act']=='fill_productname'){
        $str = "<option value='0' selected>---Select Product---</option>";
        $pivot_group_id = $_REQUEST["pivot_group"];
        $pivot_id = $_REQUEST["pivot_id"];
        $pivot_result = $pivot->arrGetPivotDetails($pivot_id,$category_id,"","",$pivot_group_id);
        $feature_id = $pivot_result[0]['feature_id'];
        $product_feature_result = $product->arrGetProductDetailsUsingFeatureid("",$feature_id);
        $cnt = sizeof($product_feature_result);
        for($i=0;$i<$cnt;$i++){
                $product_id = $product_feature_result[$i]['product_id'];
                $prod_name = "";
                if(!empty($product_id)){
                        //$product_result =$product->arrGetProductDetails($product_id,$category_id);
			$product_name = $product_feature_result[$i]['product_name'];
                        $variant_name = $product_feature_result[$i]['variant'];
                        $brand_id = $product_feature_result[$i]['brand_id'];
                        if(!empty($brand_id)){
                                $brand_result = $brand->arrGetBrandDetails($brand_id);
                                $brand_name = $brand_result[0]['brand_name'];
                                $prod_name .= $brand_name;
                        }
                        if(($variant_name != "") && ($product_name != "")){
                                $prod_name .= " ".$product_name." ".$variant_name;
                        }
                        if(!empty($prod_name)){
                                $str .= "<option value=".$product_id.">".$prod_name."</option>";
                        }
                }
        }
        echo $str;
}else{echo false;}
?>
