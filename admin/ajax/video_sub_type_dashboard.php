<?php	
require_once('../../include/config.php');
require_once(CLASSPATH.'DbConn.php');
require_once(CLASSPATH.'category.class.php');
require_once(CLASSPATH.'videos.class.php');
require_once(CLASSPATH.'pager.class.php');

$dbconn = new DbConn;
$category = new CategoryManagement;
$videoGallery = new videos();
$oPager = new Pager;

//print"<pre>";print_r($_REQUEST);print"</pre>";

$video_id = $_REQUEST['vid'];
$category_id = $_REQUEST['catid'];
$startlimit = $_REQUEST['startlimit'];
$limitcnt = $_REQUEST['cnt'];
$view_section_id = $_REQUEST['view_section_id'];
$view_section_id = ($view_section_id != "undefined") ? $view_section_id : ''; 
$type_selected=$_REQUEST['video_type_id'] ? $_REQUEST['video_type_id'] :0;
$category_id = $category_id ? $category_id : SITE_CATEGORY_ID;

$xml="";

if($_REQUEST['act']=='update' && $type_selected != 0){
	unset($result);
	$video_sub_type_id = $_REQUEST['video_sub_type_id'];
	$result = $videoGallery->arrGetVideoSubTypeDetails($video_sub_type_id,$type_selected,$category_id,"","");
	$cnt = sizeof($result);
	$xml .= "<VIDEO_DETAIL>";
	$xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
	//print"<pre>";print_r($result);print"</pre>";
	$id = $result[0]['id'];
	$video_type_id = $result[0]['video_type_id'];
	$video_sub_type_id = $result[0]['video_sub_type_id'];
	$status = $result[0]['status'];
        $result[0]['status'] = ($status == 1) ? 'Active' : 'InActive';

	if($video_sub_type_id != ""){
                $video_sub_type_result = $videoGallery->arrGetVideoSubTypeDetails($video_sub_type_id,"",$category_id);
                $video_sub_type_name = $video_sub_type_result[0]['sub_type_name'];
		if(!empty($video_sub_type_name)){
			html_entity_decode($video_sub_type_name,ENT_QUOTES,'UTF-8');
		}
        }
        $result[0]['video_sub_type_name'] = !empty($video_sub_type_name) ? $video_sub_type_name : '';
	
	$result[0] = array_change_key_case($result[0],CASE_UPPER);	
	$xml .= "<VIDEO_DETAIL_DATA>";
	foreach($result[0] as $k=>$v){
                $xml .= "<$k><![CDATA[$v]]></$k>";
        }
        $xml .= "</VIDEO_DETAIL_DATA>";

	$xml .= "</VIDEO_DETAIL>";

}

unset($result);
$result = $videoGallery->getTabDetails("",$category_id,"","","","","","");
$cnt = sizeof($result);
//print"<pre>";print_r($result);print"</pre>";
$xml .= "<VIDEO_TAB_MASTER>";
//$xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
for($i=0;$i<$cnt;$i++){
        $tab_id = $result[$i]['tab_id'];
        $tab_name = $result[$i]['tab_name'];
                $status  = $result[$i]['status'];
                if($status==1){
             $new_tab_name = strtolower($tab_name);
        if(($new_tab_name !="hindi") && ($new_tab_name != "reviews") && ($new_tab_name != "diy/maintainance") &&       ($new_tab_name != "news")){
                $result[$i] = array_change_key_case($result[$i],CASE_UPPER);
                //print "<pre>"; print_r($result[$i]);
                $xml .= "<VIDEO_TAB_MASTER_DATA>";
                foreach($result[$i] as $k=>$v){
                        $xml .= "<$k><![CDATA[$v]]></$k>";
                }
                $xml .= "</VIDEO_TAB_MASTER_DATA>";
        }
  }

}

$xml .= "</VIDEO_TAB_MASTER>";

unset($result);
if(!empty($category_id) && !empty($view_section_id) ){
	$result = $videoGallery->arrGetVideoSubTypeDetails("",$view_section_id,$category_id);
}
//print"<pre>";print_r($result);print"</pre>";
$cnt = sizeof($result);
if(!$cnt){$cnt = 0;}
$xml .= "<VIDEO_TYPE_MASTER>";
$xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
for($i=0;$i<$cnt;$i++){
        $video_sub_type_id = $result[$i]['video_sub_type_id'];
        $video_type_id = $result[$i]['video_type_id'];
        $result[$i]['category_id'] = $result[$i]['category_id'];
        $result[$i]['sub_type_name'] = $result[$i]['sub_type_name'];
        $result[$i]['video_type_id'] = $video_type_id;
        $video_sub_type_id = $result[$i]['video_sub_type_id'];
	$result[$i]['video_sub_type_id'] = $video_sub_type_id;
        $status = $result[$i]['status'];
        $result[$i]['status'] = ($status == 1) ? 'Active' : 'InActive';
        $result[$i]['create_date'] = date('d-m-Y',strtotime($result[$i]['create_date']));
        $result[$i]['update_date'] = date('d-m-Y',strtotime($result[$i]['update_date']));

	if($video_type_id != ""){
		$video_type_result = $videoGallery->arrGetTabDetails($video_type_id);
        	$video_type_name = $video_type_result[0]['tab_name'];
	}
        $result[$i]['video_type_name'] = !empty($video_type_name) ? $video_type_name : '';

	/*if($video_sub_type_id != ""){
		$video_sub_type_result = $videoGallery->arrGetVideoSubTypeDetails($video_sub_type_id,"",$category_id);
        	$video_sub_type_name = $video_sub_type_result[0]['sub_type_name'];
		if(!empty($video_sub_type_name)){
			html_entity_decode($video_sub_type_name,ENT_QUOTES,'UTF-8');
		}
	}*/
	$video_sub_type_name = $result[$i]['sub_type_name'];
	if(!empty($video_sub_type_name)){
		html_entity_decode($video_sub_type_name,ENT_QUOTES,'UTF-8');
	}
        $result[$i]['video_sub_type_name'] = !empty($video_sub_type_name) ? $video_sub_type_name : '';

        $result[$i] = array_change_key_case($result[$i],CASE_UPPER);
        //print "<pre>"; print_r($result[$i]);
        $xml .= "<VIDEO_TYPE_MASTER_DATA>";
        foreach($result[$i] as $k=>$v){
                $xml .= "<$k><![CDATA[$v]]></$k>";
        }
        $xml .= "</VIDEO_TYPE_MASTER_DATA>";
}
$xml .= "</VIDEO_TYPE_MASTER>";

$config_details = get_config_details();

$strXML = "<XML>";
$strXML .= "<MSG><![CDATA[$msg]]></MSG>";
$strXML .= "<STARTLIMIT><![CDATA[$startlimit]]></STARTLIMIT>";
$strXML .= "<CNT><![CDATA[$limitcnt]]></CNT>";
$strXML .= "<VIEW_SECTION_ID><![CDATA[$view_section_id]]></VIEW_SECTION_ID>";
$strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$category_id]]></SELECTED_CATEGORY_ID>"; 
$strXML .= $config_details;
$strXML .= $xml;
$strXML .= $nodesPaging;
$strXML .= "<WALLCNT><![CDATA[$iRelUploadCnt]]></WALLCNT>";
$strXML .= "<VIEWSECTION><![CDATA[$view_section_id]]></VIEWSECTION>";
$strXML .= "<ARTICLETYPE><![CDATA[$type_selected]]></ARTICLETYPE>";
$strXML .= "</XML>";

//$strXML = mb_convert_encoding($strXML, "UTF-8");
if($_GET['debug']==1) { header('Content-type: text/xml');echo $strXML;exit; }

$doc = new DOMDocument();
$doc->loadXML($strXML);
$doc->saveXML();

$xslt = new xsltProcessor;
$xsl = DOMDocument::load('../xsl/video_sub_type_dashboard.xsl');

$xslt->importStylesheet($xsl);
print $xslt->transformToXML($doc);
?>
