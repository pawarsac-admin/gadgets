<?php	
require_once('../../include/config.php');
require_once(CLASSPATH.'DbConn.php');
require_once(CLASSPATH.'dealer.class.php');

$dbconn = new DbConn;
$oDealer = new Dealer;

//print "<pre>"; print_r($_REQUEST);
$category_id = $_REQUEST['catid'];
$startlimit = $_REQUEST['startlimit'];
$limitcnt = $_REQUEST['cnt'];
$actiontype = $_REQUEST['actiontype'] ? $_REQUEST['actiontype'] : "Insert";

$request_type_id =$_REQUEST['request_type_id'];
$xml="";

if($_REQUEST['act']=='update' && !empty($request_type_id)){
	$result = $oDealer->arrGetRequestTypeDetails($request_type_id,"",$category_id,"");
	//print "<pre>"; print_r($result);print"</pre>";exit;
	$cnt = sizeof($result);
	$xml .= "<REQUEST_TYPE_DETAIL>";
	$xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
	for($i=0;$i<$cnt;$i++){
		$request_type_id = $result[$i]['request_type_id'];
		$request_type = $result[$i]['request_type'];
		if(!empty($request_type)){
			html_entity_decode($request_type,ENT_QUOTES);
		}
		$status = $result[$i]['status'];
		$result[$i]['status'] = ($status == 1) ? 'Active' : 'InActive';

		$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
		//print "<pre>"; print_r($result[$i]);
		$xml .= "<REQUEST_TYPE_DETAIL_DATA>";
		foreach($result[$i] as $k=>$v){
		$xml .= "<$k><![CDATA[$v]]></$k>";
		}
		$xml .= "</REQUEST_TYPE_DETAIL_DATA>";
	}
	$xml .= "</REQUEST_TYPE_DETAIL>";
}


unset($result);
if(!empty($category_id)){
	$result = $oDealer->arrGetRequestTypeDetails("","",$category_id,"");

}
$cnt = sizeof($result);
//print "<pre>"; print_r($result);
$xml .= "<REQUEST_TYPE_MASTER>";
$xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
for($i=0;$i<$cnt;$i++){
	
	$request_type_id = $result[$i]['request_type_id'];
	$request_type = $result[$i]['request_type'];
	if(!empty($request_type)){
		html_entity_decode($request_type,ENT_QUOTES);
	}
	$status = $result[$i]['status'];
	$result[$i]['status'] = ($status == 1) ? 'Active' : 'InActive';

	$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
	//print "<pre>"; print_r($result[$i]);
	$xml .= "<REQUEST_TYPE_MASTER_DATA>";
	foreach($result[$i] as $k=>$v){
		$xml .= "<$k><![CDATA[$v]]></$k>";
	}
	$xml .= "</REQUEST_TYPE_MASTER_DATA>";
}
$xml .= "</REQUEST_TYPE_MASTER>";



$iRelUploadCnt= $iRelUploadCnt ? $iRelUploadCnt :1;
$config_details = get_config_details();

$strXML = "<XML>";
$strXML .= "<MSG><![CDATA[$msg]]></MSG>";
$strXML .= "<STARTLIMIT><![CDATA[$startlimit]]></STARTLIMIT>";
$strXML .= "<CNT><![CDATA[$limitcnt]]></CNT>";
$strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$category_id]]></SELECTED_CATEGORY_ID>"; 
$strXML .= $config_details;
$strXML .= $xml;
$strXML .= "<WALLCNT><![CDATA[$iRelUploadCnt]]></WALLCNT>";
$strXML .= "<SELECTED_ACTION_TYPE><![CDATA[$actiontype]]></SELECTED_ACTION_TYPE>";
$strXML .= "</XML>";

$strXML = mb_convert_encoding($strXML, "UTF-8");
if($_GET['debug']==1) { header('Content-type: text/xml');echo $strXML;exit; }


$doc = new DOMDocument();
$doc->loadXML($strXML);
$doc->saveXML();

$xslt = new xsltProcessor;
$xsl = DOMDocument::load('../xsl/request_type_dashboard.xsl');

$xslt->importStylesheet($xsl);
print $xslt->transformToXML($doc);
?>
