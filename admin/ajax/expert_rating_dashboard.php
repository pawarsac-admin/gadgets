<?php
	require_once('../../include/config.php');
	require_once(CLASSPATH.'DbConn.php');
	require_once(CLASSPATH.'user_review.class.php');
	require_once(CLASSPATH.'brand.class.php');
	require_once(CLASSPATH.'category.class.php');
	require_once(CLASSPATH.'product.class.php');

	$dbconn = new DbConn;
	$brand = new BrandManagement;
	$category = new CategoryManagement;
	$product = new ProductManagement;
	$userreview = new USERREVIEW;

	$category_id = $_REQUEST['catid'];
	$startlimit = $_REQUEST['startlimit'];
	$limitcnt = $_REQUEST['cnt'];
	if(!empty($category_id)){
		$result = $userreview->arrGetAdminExpertGrade($category_id,'','','','',$startlimit,$limitcnt);
	}
	//print_r($result);exit;
	$cnt = sizeof($result);
	$xml = "<OVERALL_RATING_MASTER>";
	$xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";	
	for($i=0;$i<$cnt;$i++){
		unset($productnameArr);
		$status = $result[$i]['status'];
		$categoryid = $result[$i]['category_id'];
		$brand_id = $result[$i]['brand_id'];
		$model_id = $result[$i]['product_info_id'];
		$product_id = $result[$i]['product_id'];
		if(!empty($categoryid)){
			$category_result = $category->arrGetCategoryDetails($categoryid);
		}
		if(!empty($brand_id)){
			$brand_result = $brand->arrGetBrandDetails($brand_id);
			$brand_name = $brand_result[0]['brand_name'];
			$productnameArr[] = $brand_name;
		}
		if(!empty($model_id)){
			$model_result = $product->arrGetProductNameInfo($model_id);
			$model_name = $model_result[0]['product_info_name'];
			$productnameArr[] = $model_name;
		}
		if(!empty($product_id)){
			$product_result = $product->arrGetProductDetails($product_id);
			$variant = $product_result[0]['variant'];
			$productnameArr[] = $variant;
		}
		$product_detail_name = implode(" ",$productnameArr);
		$category_name = $category_result[0]['category_name'];
		$result[$i]['js_category_name'] = $category_name;
		$result[$i]['category_name'] = html_entity_decode($category_name,ENT_QUOTES,'UTF-8');
		$result[$i]['overall_rating_status'] = ($status == 1) ? 'Active' : 'InActive';
		$result[$i]['create_date'] = date('d-m-Y',strtotime($result[$i]['create_date']));
		$result[$i]['js_brand_name'] = $brand_name;
		$result[$i]['brand_name'] = html_entity_decode($brand_name,ENT_QUOTES,'UTF-8');
		$result[$i]['js_model_name'] = $model_name;
		$result[$i]['model_name'] = html_entity_decode($model_name,ENT_QUOTES,'UTF-8');
		$result[$i]['js_variant_name'] = $variant;
		$result[$i]['variant_name'] = html_entity_decode($variant,ENT_QUOTES,'UTF-8');
		$result[$i]['product_detail_name'] = $product_detail_name;
		$result[$i]['js_product_detail_name'] = html_entity_decode($product_detail_name,ENT_QUOTES,'UTF-8');
		$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
		$xml .= "<OVERALL_RATING_MASTER_DATA>";
		foreach($result[$i] as $k=>$v){
			$xml .= "<$k><![CDATA[$v]]></$k>";
		}
		$xml .= "</OVERALL_RATING_MASTER_DATA>";
	}
	$xml .= "</OVERALL_RATING_MASTER>";

	if(!empty($category_id)){
		$result = $brand->arrGetBrandDetails("",$category_id,"",$startlimit,$limitcnt);
	}

	$cnt = sizeof($result);
	$xml .= "<BRAND_MASTER>";
	$xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";	
	for($i=0;$i<$cnt;$i++){
		$status = $result[$i]['status'];
		$categoryid = $result[$i]['category_id'];
		if(!empty($categoryid)){
			$category_result = $category->arrGetCategoryDetails($categoryid);
		}
		$category_name = $category_result[0]['category_name'];
		$result[$i]['js_category_name'] = $category_name;
		$result[$i]['category_name'] = html_entity_decode($category_name,ENT_QUOTES,'UTF-8');
		$result[$i]['brand_status'] = ($status == 1) ? 'Active' : 'InActive';
		$result[$i]['create_date'] = date('d-m-Y',strtotime($result[$i]['create_date']));
		$result[$i]['js_brand_name'] = $result[$i]['brand_name'];
		$result[$i]['brand_name'] = html_entity_decode($result[$i]['brand_name'],ENT_QUOTES,'UTF-8');
		$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
		$xml .= "<BRAND_MASTER_DATA>";
		foreach($result[$i] as $k=>$v){
			$xml .= "<$k><![CDATA[$v]]></$k>";
		}
		$xml .= "</BRAND_MASTER_DATA>";
	}
	$xml .= "</BRAND_MASTER>";


	$config_details = get_config_details();
	$strXML = "<XML>";
	$strXML .= "<MSG><![CDATA[$msg]]></MSG>";
	$strXML .= "<STARTLIMIT><![CDATA[$startlimit]]></STARTLIMIT>";
	$strXML .= "<CNT><![CDATA[$limitcnt]]></CNT>";
	$strXML .= $config_details;
	$strXML .= $xml;
	$strXML .= "</XML>";
	//header('Content-type: text/xml');echo $strXML;exit;
	$doc = new DOMDocument();
	$doc->loadXML($strXML);
	$doc->saveXML();
	$xslt = new xsltProcessor;
	$xsl = DOMDocument::load('../xsl/expert_rating_dashboard.xsl');
	$xslt->importStylesheet($xsl);
	print $xslt->transformToXML($doc);
?>
