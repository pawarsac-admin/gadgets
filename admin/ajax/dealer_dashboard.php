<?php	
	require_once('../../include/config.php');
	require_once(CLASSPATH.'DbConn.php');
	require_once(CLASSPATH.'category.class.php');
	require_once(CLASSPATH.'product.class.php');
	require_once(CLASSPATH.'brand.class.php');
	require_once(CLASSPATH."citystate.class.php");
	require_once(CLASSPATH."dealer.class.php");

	$dbconn = new DbConn;
	$category = new CategoryManagement;
	$product = new ProductManagement;
	$brand = new BrandManagement;
	$oCityState = new citystate;
	$oDealer = new Dealer;

	$dealer_id = $_REQUEST['dealerid'];
	$category_id = $_REQUEST['selected_category_id'] ? $_REQUEST['selected_category_id'] : $_REQUEST['catid'];
	$startlimit = $_REQUEST['startlimit'];
	$limitcnt = $_REQUEST['cnt'];

	if($_REQUEST['act']=='update' && !empty($dealer_id)){
		$category_id = $category_id ? $category_id : $_REQUEST['catid'];
		$rResult = $oDealer->arrGetDealerDetails($dealer_id,"","","",$category_id,"","",$startlimit,$limitcnt);
		$xmlArt='';
		$cnt = sizeof($rResult);
		$status = $rResult[0]['status'];
		$category_id = $category_id ?  $category_id : $rResult[0]['category_id'];
		if(!empty($categoryid)){
			$category_result = $category->arrGetCategoryDetails($categoryid);
		}
		$brand_id = $rResult[0]['brand_id'];
		if(!empty($brand_id)){
			$brand_result = $brand->arrGetBrandDetails($brand_id);
			$brand_name = $brand_result[0]['brand_name'];
		}
		$rResult[0]['js_brand_name'] = $brand_name;
		$rResult[0]['brand_name'] = $brand_name ? html_entity_decode($brand_name,ENT_QUOTES,'UTF-8') : 'Nil';
		$product_id = $rResult[0]['product_id'];
		$rResult[0]['status'] = ($status == 1) ? 'Active' : 'InActive';
		$category_name = $category_result[0]['category_name'];
		$rResult[0]['js_category_name'] = $category_name;
		$rResult[0]['category_name'] = html_entity_decode($category_name,ENT_QUOTES,'UTF-8');
		$rResult[0]['create_date'] = date('d-m-Y',strtotime($rResult[0]['cdate']));
		$rResult[0] = array_change_key_case($rResult[0],CASE_UPPER);
		//print "<pre>"; print_r($rResult[0]);
		$xmlArt .= "<DEALER_DATA>";
		foreach($rResult[0] as $k1=>$v1){
			$xmlArt .= "<$k1><![CDATA[".html_entity_decode($v1,ENT_QUOTES,'UTF-8')."]]></$k1>";
		}
		$xmlArt .= "</DEALER_DATA>";

	}
	$result = $oDealer->arrGetDealerDetails("","","","",$category_id,"","",$startlimit,$limitcnt);
	
	$cnt = sizeof($result);
	$xml = "<DEALER_MASTER>";
	$xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";	
	for($i=0;$i<$cnt;$i++){
		$status = $result[$i]['status'];
		$category_id = $result[$i]['category_id'];
		$brand_id = $result[$i]['brand_id'];
		if(!empty($brand_id)){
			$brand_result = $brand->arrGetBrandDetails($brand_id);
			$brand_name = $brand_result[0]['brand_name'];
		}
		$result[$i]['js_brand_name'] = $brand_name;
		$result[$i]['brand_name'] = $brand_name ? html_entity_decode($brand_name,ENT_QUOTES,'UTF-8') : 'Nil';
		$product_id1 = $result[$i]['product_id'];
		/*
		if(!empty($product_id1)){
			$product_result =$product->arrGetProductDetails($product_id1,$category_id,"","1","","","",$startlimit,$limitcnt,"","");
			$product_name1 = $product_result[0]['product_name'];
		}*/
		$result[$i]['js_product_name'] =$product_name1;
		$result[$i]['product_name'] = $product_name1 ? html_entity_decode($product_name1,ENT_QUOTES,'UTF-8') : 'Nil';
		
		$city_id = $result[$i]['city_id'];
		if(!empty($city_id)){
			$city_result = $oCityState->arrGetCityDetails($city_id);
			$city_name = $city_result[0]['city_name'];
		}
		$result[$i]['city_name'] =$city_name;
		$result[$i]['create_date'] = date('d-m-Y',strtotime($result[$i]['create_date']));
		$result[$i]['status'] = ($status == 1) ? 'Active' : 'InActive';
		$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
		
		$xml .= "<DEALER_MASTER_DATA>";
		foreach($result[$i] as $k=>$v){
			$xml .= "<$k><![CDATA[".html_entity_decode($v,ENT_QUOTES,'UTF-8')."]]></$k>";
		}
		$xml .= "</DEALER_MASTER_DATA>";
	}
	$xml .= "</DEALER_MASTER>";

	if(!empty($category_id)){
		$result = $brand->arrGetBrandDetails("",$category_id);
	}	
	$cnt = sizeof($result);
	$xml .= "<BRAND_MASTER>";
	$xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
	for($i=0;$i<$cnt;$i++){
		$status = $result[$i]['status'];
		$categoryid = $result[$i]['category_id'];
		if(!empty($categoryid)){
			$category_result = $category->arrGetCategoryDetails($categoryid);
		}
		$category_name = $category_result[0]['category_name'];
		$result[$i]['js_category_name'] = $category_name;
		$result[$i]['category_name'] = html_entity_decode($category_name,ENT_QUOTES,'UTF-8');
		$result[$i]['brand_status'] = ($status == 1) ? 'Active' : 'InActive';
		$result[$i]['create_date'] = date('d-m-Y',strtotime($result[$i]['create_date']));
		$result[$i]['js_brand_name'] = $result[$i]['brand_name'];
		$result[$i]['brand_name'] = html_entity_decode($result[$i]['brand_name'],ENT_QUOTES,'UTF-8');
		$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
		$xml .= "<BRAND_MASTER_DATA>";
		foreach($result[$i] as $k=>$v){
			$xml .= "<$k><![CDATA[$v]]></$k>";
		}
		$xml .= "</BRAND_MASTER_DATA>";
	}
	$xml .= "</BRAND_MASTER>";
	$resultCountry['0']=array("country_id"=>"1","country_name"=>"India");
	$cntCnt = sizeof($resultCountry);
	$xml .= "<COUNTRY_MASTER>";
	$xml .= "<COUNT><![CDATA[$cntCnt]]></COUNT>";
	for($i=0;$i<$cntCnt;$i++){
	$resultCountry[$i] = array_change_key_case($resultCountry[$i],CASE_UPPER);
		$xml .= "<COUNTRY_MASTER_DATA>";
		foreach($resultCountry[$i] as $k=>$v){
			$xml .= "<$k><![CDATA[$v]]></$k>";
		}
		$xml .= "</COUNTRY_MASTER_DATA>";
	}
	$xml .= "</COUNTRY_MASTER>";
	
	$config_details = get_config_details();
	$strXML = "<XML>";
	$strXML .= "<MSG><![CDATA[$msg]]></MSG>";
	$strXML .= "<STARTLIMIT><![CDATA[$startlimit]]></STARTLIMIT>";
	$strXML .= "<CNT><![CDATA[$limitcnt]]></CNT>";
	$strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$category_id]]></SELECTED_CATEGORY_ID>"; 
	$strXML .= "<ACTIONTYPE><![CDATA[$actiontype]]></ACTIONTYPE>"; 
	$strXML .= $config_details;
	$strXML .= $xml;
	$strXML .= $xmlArt;
	$strXML .= "</XML>";
	
	//header('Content-type: text/xml');echo $strXML;exit;
	
	$doc = new DOMDocument();
	$doc->loadXML($strXML);
	$doc->saveXML();
	
	$xslt = new xsltProcessor;
	
	$xsl = DOMDocument::load('../xsl/dealer_dashboard.xsl');
	$xslt->importStylesheet($xsl);
	print $xslt->transformToXML($doc);
?>
