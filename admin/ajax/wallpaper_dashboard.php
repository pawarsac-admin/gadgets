<?php	
require_once('../../include/config.php');
require_once(CLASSPATH.'DbConn.php');
require_once(CLASSPATH.'category.class.php');
require_once(CLASSPATH.'product.class.php');
require_once(CLASSPATH.'brand.class.php');
require_once(CLASSPATH.'wallpaper.class.php');

$dbconn = new DbConn;
$category = new CategoryManagement;
$product = new ProductManagement;
$brand = new BrandManagement;
$oWallpapers = new Wallpapers;

//print "<pre>"; print_r($_REQUEST);
$category_id = $_REQUEST['catid'];
$startlimit = $_REQUEST['startlimit'];
$limitcnt = $_REQUEST['cnt'];
$actiontype = $_REQUEST['actiontype'] ? $_REQUEST['actiontype'] : "Insert";

$wp_id =$_REQUEST['wp_id'];
$type_selected=$_REQUEST['type_id'] ? $_REQUEST['type_id'] : $_REQUEST['wp_type_id'];

if($_REQUEST['act']=='update' && !empty($wp_id)){
	$product_wallpaper_id = $_REQUEST['product_wallpaper_id'];	
	$result = $oWallpapers->arrGetWallpapersDetails($wp_id,"","",$category_id,"","","","","create_date");
	//print "<pre>"; print_r($result);print"</pre>";
	$cnt = sizeof($result);
	$xml_wpdet .= "<WP_DETAIL>";
	$xml_wpdet .= "<COUNT><![CDATA[$cnt]]></COUNT>";
	for($i=0;$i<$cnt;$i++){
		$wp_id = $result[$i]['wallpaper_id'];
		if(!empty($wp_id)){$result[$i]['wp_id'] = $wp_id;}
		$status = $result[$i]['status'];
		$categoryid = $result[$i]['category_id'];
		if(!empty($categoryid)){
			$category_result = $category->arrGetCategoryDetails($categoryid,"","","","");
			//print "<pre>"; print_r($category_result);print"</pre>";
		}
		$category_name = $category_result[0]['category_name'];
		$result[$i]['js_category_name'] = $category_name;
		$result[$i]['category_name'] = html_entity_decode($category_name,ENT_QUOTES);
		$result[$i]['status'] = ($status == 1) ? 'Active' : 'InActive';
		$result[$i]['create_date'] = date('d-m-Y',strtotime($result[$i]['create_date']));

		$brand_id = $result[$i]['brand_id'];
		//echo "brand_id===".$brand_id;
		$category_id = 1;
		if(!empty($brand_id)){
			$brand_result = $brand->arrGetBrandDetails($brand_id,$category_id,"","","");
			//echo "sdfsd";print "<pre>"; print_r($brand_result);print"</pre>";exit;
		}
		$brand_name = $brand_result[0]['brand_name'];
		$result[$i]['js_brand_name'] = $brand_name;
		$result[$i]['brand_name'] = html_entity_decode($brand_name,ENT_QUOTES);
		if($result[$i]['product_id']!=''){
			$product_id = $result[$i]['product_id'];
			if(!empty($product_id)){
				$product_result = $product->arrGetProductDetails($product_id,$category_id,"","","","","","","");
				//echo "sdfsd";print "<pre>"; print_r($product_result);print"</pre>";exit;
			}
			$product_name = $product_result[0]['product_name'];
			$product_variant = $product_result[0]['variant'];
			$result[$i]['js_product_name'] = $product_name;
			$result[$i]['product_name'] = html_entity_decode($product_name,ENT_QUOTES);
			$result[$i]['product_variant'] = html_entity_decode($product_variant,ENT_QUOTES);
		}
		if($result[$i]['product_info_id']!=''){
			$product_info_id = $result[$i]['product_info_id'];
			if(!empty($product_info_id)){
				$product_info_result = $product->arrGetProductNameInfo($product_info_id,$category_id,"","","","","");
				$product_info_name = $product_info_result[0]['product_info_name'];
			}
			$result[$i]['js_product_name'] = $product_info_name;
			$result[$i]['product_name'] = html_entity_decode($product_name,ENT_QUOTES);
		}

		

		$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
		//print "<pre>"; print_r($result[$i]);
		$xml_wpdet .= "<WP_DETAIL_DATA>";
		foreach($result[$i] as $k=>$v){
		$xml_wpdet .= "<$k><![CDATA[$v]]></$k>";
		}
		$xml_wpdet .= "</WP_DETAIL_DATA>";
	}
	$xml_wpdet .= "</WP_DETAIL>";
}


unset($result);
if(!empty($category_id)){
	$result = $oWallpapers->arrGetWallpapersDetails("","","",$category_id,"","","","","create_date");
}
$cnt = sizeof($result);
//print "<pre>"; print_r($result);exit;
$xml_wp .= "<WP_MASTER>";
$xml_wp .= "<COUNT><![CDATA[$cnt]]></COUNT>";
for($i=0;$i<$cnt;$i++){
	$wp_id = $result[$i]['wallpaper_id'];	
	if(!empty($wp_id)){$result[$i]['wp_id'] = $wp_id;}
	$status = $result[$i]['status'];
	$categoryid = $result[$i]['category_id'];
	if(!empty($category_id)){$result[$i]['category_id'] = $categoryid;}
	if(!empty($categoryid)){
		$category_result = $category->$result = $category->arrGetCategoryDetails($categoryid,"","","","");
	}
	$category_name = $category_result[0]['category_name'];
	$result[$i]['js_category_name'] = $category_name;
	$result[$i]['category_name'] = html_entity_decode($category_name,ENT_QUOTES);
	$result[$i]['status'] = ($status == 1) ? 'Active' : 'InActive';
	$result[$i]['create_date'] = date('d-m-Y',strtotime($result[$i]['create_date']));

	$brand_id = $result[$i]['brand_id'];
	if(!empty($brand_id)){
		$brand_result = $brand->arrGetBrandDetails($brand_id,$category_id,"","","");
	}
	$brand_name = $brand_result[0]['brand_name'];
	$result[$i]['js_brand_name'] = $brand_name;
	$result[$i]['brand_name'] = html_entity_decode($brand_name,ENT_QUOTES);
	
	if($result[$i]['product_id']!='0'){
		$product_id = $result[$i]['product_id'];
		if(!empty($product_id)){
			$product_result = $product->arrGetProductDetails($product_id,$category_id,"","","","","","","","","");
		}
		$product_name = $product_result[0]['product_name'];
		$product_variant = $product_result[0]['variant'];
		$result[$i]['js_product_name'] = $product_name;
		$result[$i]['product_name'] = html_entity_decode($product_name,ENT_QUOTES);
		$result[$i]['product_variant'] = html_entity_decode($product_variant,ENT_QUOTES);
	}
	
	if($result[$i]['product_info_id']!='0'){
		$product_info_id = $result[$i]['product_info_id'];
		if(!empty($product_info_id)){
			$product_info_result = $product->arrGetProductNameInfo($product_info_id,$category_id,"","","","","");
			//print_r($product_iinfo_result);
			$product_info_name = $product_info_result[0]['product_info_name'];
		}
		$result[$i]['js_product_name'] = $product_info_name;
		$result[$i]['product_name'] = html_entity_decode($product_info_name,ENT_QUOTES);
	}
	//echo $product_info_name."====".$product_name."====".$product_variant."SAC<br>";
	$product_info_name=""; $product_variant=""; $product_name='';

	$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
	//print "<pre>"; print_r($result[$i]);
	$xml_wp .= "<WP_MASTER_DATA>";
	foreach($result[$i] as $k=>$v){
		$xml_wp .= "<$k><![CDATA[$v]]></$k>";
	}
	$xml_wp .= "</WP_MASTER_DATA>";
	unset($brand_result);
        unset($product_result);
        unset($product_info_result);
}
$xml_wp .= "</WP_MASTER>";
//echo "TEST---".$xml_wp;



unset($result);
if(!empty($category_id)){
	$result = $brand->arrGetBrandDetails("",$category_id,"","","");
}	
$cnt = sizeof($result);
$xml .= "<BRAND_MASTER>";
$xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
for($i=0;$i<$cnt;$i++){
	$status = $result[$i]['status'];
	$categoryid = $result[$i]['category_id'];
	if(!empty($categoryid)){
		$category_result = $category->arrGetCategoryDetails($categoryid,"","","","");
	}
	$category_name = $category_result[0]['category_name'];
	$result[$i]['js_category_name'] = $category_name;
	$result[$i]['category_name'] = html_entity_decode($category_name,ENT_QUOTES);
	$result[$i]['brand_status'] = ($status == 1) ? 'Active' : 'InActive';
	$result[$i]['create_date'] = date('d-m-Y',strtotime($result[$i]['create_date']));
	$result[$i]['js_brand_name'] = $result[$i]['brand_name'];
	$result[$i]['brand_name'] = html_entity_decode($result[$i]['brand_name'],ENT_QUOTES);
	$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
	$xml .= "<BRAND_MASTER_DATA>";
	foreach($result[$i] as $k=>$v){
		$xml .= "<$k><![CDATA[$v]]></$k>";
	}
	$xml .= "</BRAND_MASTER_DATA>";
}
$xml .= "</BRAND_MASTER>";


/*if(!empty($category_id)){
	//$result = $oVideos->arrGetVideoTypeDetails("","",$category_id,"1");
	$result = $oVideos->arrGetTabDetails("","2","2","");
}	
$cnt = sizeof($result);
$xml_type .= "<TYPE_MASTER>";
$xml_type .= "<COUNT><![CDATA[$cnt]]></COUNT>";
for($i=0;$i<$cnt;$i++){
	$status = $result[$i]['status'];
	//$result[$i]['create_date'] = date('d-m-Y',strtotime($result[$i]['create_date']));
	$result[$i]['tab_id'] = html_entity_decode($result[$i]['tab_id'],ENT_QUOTES);
	$result[$i]['tab_name'] = html_entity_decode($result[$i]['tab_name'],ENT_QUOTES);
	$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
	$xml_type .= "<TYPE_MASTER_DATA>";
	foreach($result[$i] as $k=>$v){
		$xml_type .= "<$k><![CDATA[$v]]></$k>";
	}
	$xml_type .= "</TYPE_MASTER_DATA>";
}
$xml_type .= "</TYPE_MASTER>";


if(!empty($category_id)){
	$result = $oVideos->arrGetVideoGroupDetails("","",$category_id,"1");
}

$cnt = sizeof($result);
$xml_type1 .= "<GTYPE_MASTER>";
$xml_type1 .= "<COUNT><![CDATA[$cnt]]></COUNT>";
for($i=0;$i<$cnt;$i++){
	$status = $result[$i]['status'];
	$result[$i]['create_date'] = date('d-m-Y',strtotime($result[$i]['create_date']));
	$result[$i]['group_name'] = html_entity_decode($result[$i]['group_name'],ENT_QUOTES);
	$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
	$xml_type1 .= "<GTYPE_MASTER_DATA>";
	foreach($result[$i] as $k=>$v){
		$xml_type1 .= "<$k><![CDATA[$v]]></$k>";
	}
	$xml_type1 .= "</GTYPE_MASTER_DATA>";
}
$xml_type1 .= "</GTYPE_MASTER>";*/


$iRelUploadCnt= $iRelUploadCnt ? $iRelUploadCnt :1;
$config_details = get_config_details();

$strXML = "<XML>";
$strXML .= "<MSG><![CDATA[$msg]]></MSG>";
$strXML .= "<STARTLIMIT><![CDATA[$startlimit]]></STARTLIMIT>";
$strXML .= "<CNT><![CDATA[$limitcnt]]></CNT>";
$strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$category_id]]></SELECTED_CATEGORY_ID>"; 
$strXML .= $config_details;
$strXML .= $xml;
$strXML .= $xml_type1;
$strXML .= $xml_type;
$strXML .= $xml_wp;
$strXML .= $xml_wpdet;
$strXML .= "<WALLCNT><![CDATA[$iRelUploadCnt]]></WALLCNT>";
$strXML .= "<SELECTED_TYPE><![CDATA[$type_selecetd]]></SELECTED_TYPE>";
$strXML .= "<WP_ID><![CDATA[$wp_id]]></WP_ID>";
$strXML .= "<SELECTED_ACTION_TYPE><![CDATA[$actiontype]]></SELECTED_ACTION_TYPE>";
$strXML .= "</XML>";

$strXML = mb_convert_encoding($strXML, "UTF-8");
if($_GET['debug']==1) { header('Content-type: text/xml');echo $strXML;exit; }


$doc = new DOMDocument();
$doc->loadXML($strXML);
$doc->saveXML();

$xslt = new xsltProcessor;
$xsl = DOMDocument::load('../xsl/wallpaper_dashboard.xsl');

$xslt->importStylesheet($xsl);
print $xslt->transformToXML($doc);
?>
