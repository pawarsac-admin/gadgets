<?php	
require_once('../../include/config.php');
require_once(CLASSPATH.'DbConn.php');
require_once(CLASSPATH.'category.class.php');
require_once(CLASSPATH.'videos.class.php');
require_once(CLASSPATH.'reviews.class.php');
require_once(CLASSPATH.'article.class.php');

$dbconn = new DbConn;
$category = new CategoryManagement;
$videoGallery = new videos();
$reviews = new reviews(); 
$article = new article();

//print"<pre>";print_r($_REQUEST);print"</pre>";

$video_id = $_REQUEST['vid'];
$category_id = $_REQUEST['catid'];
$startlimit = $_REQUEST['startlimit'];
$limitcnt = $_REQUEST['cnt'];
$r_product_id = $_REQUEST['pid'];
$r_brand_id = $_REQUEST['bid'];
$select_section_id = $_REQUEST['select_section_id'];
$type_selecetd=$_REQUEST['video_type_id'] ? $_REQUEST['video_type_id'] :0;
$category_id = $category_id ? $category_id:1;


$aVideoSectionDet=array("0"=>array("SECTION_ID"=>"VIDEOS","SECTION_NAME"=>"video"),"1"=>array("SECTION_ID"=>"REVIEWS","SECTION_NAME"=>"Reviews"),"2"=>array("SECTION_ID"=>"ARTICLES","SECTION_NAME"=>"Articles"),"3"=>array("SECTION_ID"=>"NEWS","SECTION_NAME"=>"news"));


//print "<pre>"; print_r($aArticleSectionDet);
$sVideoSectionDetail=arraytoxml($aVideoSectionDet,"VIDEO_SECTION");
$sVideoSectionDetailXML ="<VIDEO_SECTION_MASTER>".$sVideoSectionDetail."</VIDEO_SECTION_MASTER>";


if($_REQUEST['act']=='update' && !empty($video_id)){
	unset($result);
	$result=$videoGallery->getarrMostPopularVideos($video_id,$category_id,"","","","", "");

	$tbl_type = $result["0"]['tbl_type'];
                
	unset($result_list);
	if($tbl_type == 1){//Videos
        	$select_section_id = "VIDEOS";
                $result_list = $videoGallery->getVideosDetails($video_id,"","","","",$category_id,"","","","","");
        }else if($tbl_type == 2){//Reviews
        	$select_section_id = "REVIEWS";
                $result_list=$reviews->arrGetReviewsVideoDetails($video_id,"","","",$category_id,"","","","");
        }else if($tbl_type == 3){//Articles
        	$select_section_id = "ARTICLES";
                $result_list=$article->arrGetArticleVideoDetails($video_id,"","","",$category_id,"","");
        }else if($tbl_type == 4){//News
        	$select_section_id = "NEWS";
        	$result_list=$article->arrGetNewsVideoDetails($video_id,"","","",$category_id,"","");
        }
        $result_list = $videoGallery->arrGenerate($result_list,"","");

        //print "<pre>"; print_r($result_list);print"</pre>";


        $result[0]['video_id'] = $result[0]['video_id'];
        $result[0]['js_title'] = $result_list[0]['title'];
        $result[0]['title'] = $result_list[0]['title'] ? html_entity_decode($result[0]['title'],ENT_QUOTES) : 'Nil';

        $status = $result[0]['status'];
        $result[0]['status'] = ($status == 1) ? 'Active' : 'InActive';


	$result[0] = array_change_key_case($result[0],CASE_UPPER);
        //print "<pre>"; print_r($result[0]);
        $xmlVid .= "<VIDEO_DATA>";
        foreach($result[0] as $k1=>$v1){
                $xmlVid .= "<$k1><![CDATA[$v1]]></$k1>";
        }
        $xmlVid .= "</VIDEO_DATA>";
}

unset($result);
if(!empty($category_id)){
        if($select_section_id == "VIDEOS"){
		$result = $videoGallery->getVideosDetails("","","","","",$category_id,"","","","","");        
	}
        if($select_section_id == "REVIEWS"){
		
		$result=$reviews->arrGetReviewsVideoDetails("","","","",$category_id,"","1","","","");
        }
        if($select_section_id == "ARTICLES"){
		$result=$article->arrGetArticleVideoDetails("","","","",$category_id,"","");
        }
        if($select_section_id == "NEWS"){
		$result=$article->arrGetNewsVideoDetails("","","","",$category_id,"","");
	}
}
	
	$result = $videoGallery->arrGenerate($result,"","");
	//print"<pre>";print_r($result);print"</pre>";
	$cnt = sizeof($result);
	if(!$cnt){$cnt = 0;}
        $vxml = "<VIDEO_DETAILS_MASTER>";
        $vxml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
	for($i=0;$i<$cnt;$i++){
                $result[$i]['video_id'] = $result[$i]['video_id'];
                $result[$i]['title'] = $result[$i]['title'];

                $result[$i] = array_change_key_case($result[$i],CASE_UPPER);
                //print "<pre>"; print_r($result[$i]);
                $vxml .= "<VIDEO_DETAILS_MASTER_DATA>";
                foreach($result[$i] as $k=>$v){
                        $vxml .= "<$k><![CDATA[$v]]></$k>";
                }
                $vxml .= "</VIDEO_DETAILS_MASTER_DATA>";
        }
        $vxml .= "</VIDEO_DETAILS_MASTER>";



unset($result);
if(!empty($category_id)){
	$result=$videoGallery->getarrMostPopularVideos("",$category_id,"","","","", "");
}
//print"<pre>";print_r($result);print"</pre>";
$cnt = sizeof($result);
if(!$cnt){$cnt = 0;}
	$xml = "<VIDEO_MASTER>";
	$xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
	for($i=0;$i<$cnt;$i++){
		unset($result_list);	
		$result[$i]['id'] = $result[$i]['id'];
		$video_id = $result[$i]['video_id'];
		$tbl_type = $result[$i]['tbl_type'];
		
		if($tbl_type == 1){//Videos
			$tbltype = "Videos";
			$result_list = $videoGallery->getVideosDetails($video_id,"","","","",$category_id,"","","","","");
		}else if($tbl_type == 2){//Reviews
			$tbltype = "Reviews";
			$result_list=$reviews->arrGetReviewsVideoDetails($video_id,"","","",$category_id,"","","","");
		}else if($tbl_type == 3){//Articles
			$tbltype = "Articles";
			$result_list=$article->arrGetArticleVideoDetails($video_id,"","","",$category_id,"","");
		}else if($tbl_type == 4){//News
			$tbltype = "News";
			$result_list=$article->arrGetNewsVideoDetails($video_id,"","","",$category_id,"","");
		}
		$result_list = $videoGallery->arrGenerate($result_list,"","");

		$result[$i]['video_id'] = $result_list["0"]['video_id'];
                $result[$i]['title'] = $result_list["0"]['title'];
                $result[$i]['type'] = $tbltype;
                $result[$i]['tags'] = $result_list["0"]['tags'];
                $result[$i]['media_id'] = $result_list["0"]['media_id'];
                $result[$i]['media_path'] = $result_list["0"]['media_path'];
                $result[$i]['video_img_id'] = $result_list["0"]['video_img_id'];
                $result[$i]['video_img_path'] = $result_list["0"]['video_img_path'];
                $result[$i]['content_type'] = $result_list["0"]['content_type'];
                $result[$i]['is_media_process'] = $result_list["0"]['is_media_process'];

		$status = $result[$i]['status'];
        	$result[$i]['status'] = ($status == 1) ? 'Active' : 'InActive';
		$result[$i]['create_date'] = date('d-m-Y',strtotime($result[$i]['create_date']));
		$result[$i]['update_date'] = date('d-m-Y',strtotime($result[$i]['update_date']));
		$status="";

		$result[$i] = array_change_key_case($result[$i],CASE_UPPER);

	        //print "<pre>"; print_r($result[$i]);
        	$xml .= "<VIDEO_MASTER_DATA>";
	        foreach($result[$i] as $k=>$v){
        	        $xml .= "<$k><![CDATA[$v]]></$k>";
	        }
        	$xml .= "</VIDEO_MASTER_DATA>";
	}
	$xml .= "</VIDEO_MASTER>";

$config_details = get_config_details();

$strXML = "<XML>";
$strXML .= "<MSG><![CDATA[$msg]]></MSG>";
$strXML .= "<STARTLIMIT><![CDATA[$startlimit]]></STARTLIMIT>";
$strXML .= "<CNT><![CDATA[$limitcnt]]></CNT>";
$strXML .= "<SELECT_SECTION_ID><![CDATA[$select_section_id]]></SELECT_SECTION_ID>";
$strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$category_id]]></SELECTED_CATEGORY_ID>"; 
$strXML .= $config_details;
$strXML .= $sVideoSectionDetailXML;
$strXML .= $xml;
$strXML .= $vxml;
$strXML .= $xmlVid;
$strXML .= "<WALLCNT><![CDATA[$iRelUploadCnt]]></WALLCNT>";
$strXML .= "<VIEWSECTION><![CDATA[$view_section_id]]></VIEWSECTION>";
$strXML .= "<ARTICLETYPE><![CDATA[$type_selecetd]]></ARTICLETYPE>";
$strXML .= "</XML>";

//$strXML = mb_convert_encoding($strXML, "UTF-8");
if($_GET['debug']==1) { header('Content-type: text/xml');echo $strXML;exit; }

$doc = new DOMDocument();
$doc->loadXML($strXML);
$doc->saveXML();

$xslt = new xsltProcessor;
$xsl = DOMDocument::load('../xsl/most_popular_video_dashboard.xsl');

$xslt->importStylesheet($xsl);
print $xslt->transformToXML($doc);
?>
