<?php	
require_once('../../include/config.php');

require_once(CLASSPATH.'DbConn.php');
require_once(CLASSPATH.'category.class.php');
require_once(CLASSPATH.'videos.class.php');
require_once(CLASSPATH.'reviews.class.php');
require_once(CLASSPATH.'article.class.php');

$dbconn = new DbConn;
$category = new CategoryManagement;
$videoGallery = new videos();
$reviews = new reviews(); 
$article = new article();

//print"<pre>";print_r($_REQUEST);print"</pre>"; 

$item_id = $_REQUEST['itemid'];
$category_id = $_REQUEST['catid'];
$startlimit = $_REQUEST['startlimit'];
$limitcnt = $_REQUEST['cnt'];
$r_product_id = $_REQUEST['pid'];
$r_brand_id = $_REQUEST['bid'];
$select_section_id = $_REQUEST['select_section_id'];
$select_type_id = $_REQUEST['select_type_id'];
$type_selecetd=$_REQUEST['video_type_id'] ? $_REQUEST['video_type_id'] :0;
$category_id = $category_id ? $category_id : SITE_CATEGORY_ID;


$aSectionDet=array("0"=>array("SECTION_ID"=>"VIDEOS","SECTION_NAME"=>"video"),"1"=>array("SECTION_ID"=>"REVIEWS","SECTION_NAME"=>"Reviews"),"2"=>array("SECTION_ID"=>"ARTICLES","SECTION_NAME"=>"Articles"),"3"=>array("SECTION_ID"=>"NEWS","SECTION_NAME"=>"news"));

//print "<pre>"; print_r($aArticleSectionDet);
$sSectionDetail=arraytoxml($aSectionDet,"ITEM_SECTION");
$sectionxml ="<ITEM_SECTION_MASTER>".$sSectionDetail."</ITEM_SECTION_MASTER>";



if($_REQUEST['act']=='update' && !empty($item_id)){
	unset($result);
	$result=$videoGallery->getArrItemData($item_id,$category_id,"","","","", "");
	$tbl_type = $result["0"]['tbl_type'];
	unset($result_list);
	$content_type_id = $result['0']['content_type_id'];
	if($content_type_id==1){
		switch($tbl_type){
			case '1' :
				$tbltype = "Videos";
				$result_list = $videoGallery->getVideosDetails($item_id,"","","","",$category_id,"","","","","");
				break;
			case '2' :
				$tbltype = "Reviews";
				$result_list=$reviews->arrGetReviewsVideoDetails($item_id,"","","",$category_id,"","","","");
				break;
			case '3' :
				$tbltype = "Articles";
				$result_list=$article->arrGetArticleVideoDetails($item_id,"","","",$category_id,"","");
				break;
			case '4' :
				$tbltype = "News";
				$result_list=$article->arrGetNewsVideoDetails($item_id,"","","",$category_id,"","");
				break;
		}
	}else{
		switch($tbl_type){
		case '2' :
			$tbltype = "Reviews";
			$result_list=$reviews->getReviewsDetails($item_id,"","","","",$category_id);
			break;
		case '3' :
			$tbltype = "Articles";
			$result_list=$article->getArticleDetails($item_id,"","","","",$category_id);
			break;
		case '4' :
			$tbltype = "News";
			$result_list=$article->getNewsDetails($item_id,"","","","",$category_id);
			break;
		}
	}
	$result_list = $videoGallery->arrGenerateSameFormatArray($result_list,"","");
	
	//print "<pre>"; print_r($result_list);print"</pre>";
	$result[0]['item_id'] = $result[0]['item_id'];
	$result[0]['js_title'] = $result_list[0]['title'];
	$result[0]['title'] = $result_list[0]['title'] ? html_entity_decode($result[0]['title'],ENT_QUOTES) : 'Nil';
	$status = $result[0]['status'];
	$result[0]['status'] = ($status == 1) ? 'Active' : 'InActive';
	$result[0] = array_change_key_case($result[0],CASE_UPPER);
	//print "<pre>"; print_r($result[0]);
	$xmlVid .= "<ITEM_DATA>";
	foreach($result[0] as $k1=>$v1){
		$xmlVid .= "<$k1><![CDATA[$v1]]></$k1>";
	}
	$xmlVid .= "</ITEM_DATA>";
}

unset($result);
if(!empty($category_id)){
	if($select_type_id==1){
		switch($select_section_id){

			case 'VIDEOS' :
				$result = $videoGallery->getVideosDetails("","","","","",$category_id,"","","","","");
			break;
			case 'REVIEWS' :
				$result=$reviews->arrGetReviewsVideoDetails("","","","",$category_id,"","","","");
			break;
			case 'ARTICLES' :
				$result=$article->arrGetArticleVideoDetails("","","","",$category_id,"","");
			break;
			case 'NEWS' :
				$result=$article->arrGetNewsVideoDetails("","","","",$category_id,"","");
			break;
		}
    }else{
		///echo $select_section_id."---".$select_type_id;
		switch($select_section_id){
			case 'REVIEWS' :
				$result=$reviews->getReviewsDetails("","","","","",$category_id);
				break;
			case 'ARTICLES' :
				$result=$article->getArticleDetails("","","","","",$category_id);
				break;
			case 'NEWS' :
				$result=$article->getNewsDetails("","","","","",$category_id); 
				break;
		}
	}
}
$result = $videoGallery->arrGenerateSameFormatArray($result,$select_section_id,$select_type_id);
//print"<pre>";print_r($result);print"</pre>";
$cnt = sizeof($result);
if(!$cnt){$cnt = 0;}
$vxml = "<ITEM_DETAILS_MASTER>";
$vxml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
for($i=0;$i<$cnt;$i++){
	$result[$i]['item_id'] = $result[$i]['item_id'];
	$result[$i]['title'] = $result[$i]['title'];
	$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
	//print "<pre>"; print_r($result[$i]);
	$vxml .= "<ITEM_DETAILS_MASTER_DATA>";
	foreach($result[$i] as $k=>$v){
	$vxml .= "<$k><![CDATA[$v]]></$k>";
	}
	$vxml .= "</ITEM_DETAILS_MASTER_DATA>";
}
$vxml .= "</ITEM_DETAILS_MASTER>";



unset($result);
if(!empty($category_id)){
	$result=$videoGallery->getArrItemData("",$category_id,"","","","", "");
}
//print"<pre>";print_r($result);print"</pre>";
$cnt = sizeof($result);
if(!$cnt){$cnt = 0;}
$xml = "<ITEM_MASTER>";
$xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
for($i=0;$i<$cnt;$i++){
	unset($result_list);	
	$result[$i]['id'] = $result[$i]['id'];
	$item_id = $result[$i]['item_id'];
	$tbl_type = $result[$i]['tbl_type_id'];
	$content_type_id = $result[$i]['content_type_id'];
	if($content_type_id==1){
		switch($tbl_type){
			case '1' :
				$tbltype = "Videos";
				$result_list = $videoGallery->getVideosDetails($item_id,"","","","",$category_id,"","","","","");
				break;
			case '2' :
				$tbltype = "Reviews";
				$result_list=$reviews->arrGetReviewsVideoDetails($item_id,"","","",$category_id,"","","","");
				break;
			case '3' :
				$tbltype = "Articles";
				$result_list=$article->arrGetArticleVideoDetails($item_id,"","","",$category_id,"","");
				break;
			case '4' :
				$tbltype = "News";
				$result_list=$article->arrGetNewsVideoDetails($item_id,"","","",$category_id,"","");
				break;
		}
	}else{
		switch($tbl_type){
		case '2' :
			$tbltype = "Reviews";
			$result_list=$reviews->getReviewsDetails($item_id,"","","","",$category_id);
			break;
		case '3' :
			$tbltype = "Articles";
			$result_list=$article->getArticleDetails($item_id,"","","","",$category_id);
			break;
		case '4' :
			$tbltype = "News";
			$result_list=$article->getNewsDetails($item_id,"","","","",$category_id);
			break;
		}
	}

	$result_list = $videoGallery->arrGenerateSameFormatArray($result_list,"","");
	
	//echo $content_type_id;
	$result[$i]['item_id'] = $item_id;
	$result[$i]['title'] = $result_list["0"]['title'];
	$result[$i]['content_type_data'] = ($content_type_id == 1) ? 'Video' : 'Text' ;
	$result[$i]['section_type'] = $tbltype;
	$result[$i]['tags'] = $result_list["0"]['tags'];
	$result[$i]['media_id'] = $result_list["0"]['media_id'];
	$result[$i]['media_path'] = $result_list["0"]['media_path'];
	$result[$i]['video_img_id'] = $result_list["0"]['video_img_id'];
	$result[$i]['video_img_path'] = $result_list["0"]['video_img_path'];
	$result[$i]['content_type'] = $result_list["0"]['content_type'];
	$result[$i]['is_media_process'] = $result_list["0"]['is_media_process'];
	$status = $result[$i]['status'];
	$result[$i]['status'] = ($status == 1) ? 'Active' : 'InActive';
	$result[$i]['create_date'] = date('d-m-Y',strtotime($result[$i]['create_date']));
	$result[$i]['update_date'] = date('d-m-Y',strtotime($result[$i]['update_date']));
	$status="";
	$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
	//print "<pre>"; print_r($result[$i]);
	
	$xml .= "<ITEM_MASTER_DATA>";
	foreach($result[$i] as $k=>$v){
		$xml .= "<$k><![CDATA[$v]]></$k>";
	}
	$xml .= "</ITEM_MASTER_DATA>";
}
$xml .= "</ITEM_MASTER>";

$config_details = get_config_details();

$strXML = "<XML>";
$strXML .= "<MSG><![CDATA[$msg]]></MSG>";
$strXML .= "<STARTLIMIT><![CDATA[$startlimit]]></STARTLIMIT>";
$strXML .= "<CNT><![CDATA[$limitcnt]]></CNT>";
$strXML .= "<SELECT_SECTION_ID><![CDATA[$select_section_id]]></SELECT_SECTION_ID>";
$strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$category_id]]></SELECTED_CATEGORY_ID>"; 
$strXML .= $config_details;
$strXML .= $sectionxml;
$strXML .= $typexml;
$strXML .= $xml;
$strXML .= $vxml;
$strXML .= $xmlVid;
$strXML .= "<WALLCNT><![CDATA[$iRelUploadCnt]]></WALLCNT>";
$strXML .= "<VIEWSECTION><![CDATA[$view_section_id]]></VIEWSECTION>";
$strXML .= "<CONTENT_TYPE_ID><![CDATA[$select_type_id]]></CONTENT_TYPE_ID>";
$strXML .= "</XML>";

//$strXML = mb_convert_encoding($strXML, "UTF-8");
if($_GET['debug']==1) { header('Content-type: text/xml');echo $strXML;exit; }

$doc = new DOMDocument();
$doc->loadXML($strXML);
$doc->saveXML();

$xslt = new xsltProcessor;
$xsl = DOMDocument::load('../xsl/homepage_item_dashboard.xsl');

$xslt->importStylesheet($xsl);
print $xslt->transformToXML($doc);
?>
