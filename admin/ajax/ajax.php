<?php	
require_once('../../include/config.php');
require_once(CLASSPATH.'DbConn.php');
require_once(CLASSPATH.'topstories.class.php');
require_once(CLASSPATH.'product.class.php');

$dbconn			= new DbConn;
$oTopStories	= new TopStories;
$oProduct		= new ProductManagement;

$group_type		= 2;
$action			= $_REQUEST['action'];
$group_id		= ($_REQUEST['group_id'])?		trim($_REQUEST['group_id'])		: 0;
$product_id		= ($_REQUEST['product_id'])?	trim($_REQUEST['product_id'])	: 0;
$story_id		= ($_REQUEST['story_id'])?		trim($_REQUEST['story_id'])		: 0;
$product_name	= ($_REQUEST['product_name'])?	trim($_REQUEST['product_name'])	: '';
$product_name	=  trim($product_name,"-");

if($action){
	switch($action){
		case 'addtolist':
			$op		= $oTopStories->createGroupRecord( $group_id, $product_id, $product_name, $grouptitle );
			$msg	= $op;
			break;
		case 'removefromlist':
			$op	= $oTopStories->removeGroupRecord( $story_id );
			$msg	= $op;
			break;
		default	:
			$msg	= 'nnn';
			break;
	}

	echo $msg;
	die;
}
?>