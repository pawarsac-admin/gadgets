<?php	
require_once('../../include/config.php');
require_once(CLASSPATH.'DbConn.php');
require_once(CLASSPATH.'dealer.class.php');

$dbconn = new DbConn;
$oDealer = new Dealer;

//print "<pre>"; print_r($_REQUEST);
$category_id = $_REQUEST['catid'];
$startlimit = $_REQUEST['startlimit'];
$limitcnt = $_REQUEST['cnt'];
$actiontype = $_REQUEST['actiontype'];
$view_section_id = $_REQUEST['view_section_id'];

$black_list_number_id =$_REQUEST['black_list_number_id'];
$xml="";

$aSectionDet=array("0"=>array("SECTION_ID"=>"LANDLINE","SECTION_NAME"=>"Landline"),"1"=>array("SECTION_ID"=>"MOBILE","SECTION_NAME"=>"Mobile"));
$sSectionDetail=arraytoxml($aSectionDet,"BLACK_LIST_NUMBER_SECTION");
$sSectionDetailXML ="<BLACK_LIST_NUMBER_SECTION_MASTER>".$sSectionDetail."</BLACK_LIST_NUMBER_SECTION_MASTER>";

if($actiontype =='update' && !empty($black_list_number_id) && !empty($view_section_id)){
	$type_id="";
        if($view_section_id=="LANDLINE"){
		$type_id="1";
        }
        if($view_section_id=="MOBILE"){
		$type_id="2";
        }
	$result = $oDealer->arrGetBlackListNumbers($black_list_number_id,"",$type_id,$category_id,"","","","");
	//print "<pre>"; print_r($result);print"</pre>";exit;
	$cnt = sizeof($result);
	$xml .= "<BLACK_LIST_NUMBER_DETAIL>";
	$xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
	for($i=0;$i<$cnt;$i++){
		$black_list_number_id = $result[$i]['black_list_number_id'];
		$black_list_number = $result[$i]['black_list_number'];
		$status = $result[$i]['status'];
		$result[$i]['status'] = ($status == 1) ? 'Active' : 'InActive';
		$type_id = $result[$i]['type_id'];
		$result[$i]['black_list_number_type'] = ($type_id == 1) ? 'Landline' : 'Mobile';

		$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
		//print "<pre>"; print_r($result[$i]);
		$xml .= "<BLACK_LIST_NUMBER_DETAIL_DATA>";
		foreach($result[$i] as $k=>$v){
		$xml .= "<$k><![CDATA[$v]]></$k>";
		}
		$xml .= "</BLACK_LIST_NUMBER_DETAIL_DATA>";
	}
	$xml .= "</BLACK_LIST_NUMBER>";
}


unset($result);
if(!empty($category_id)){
	if($view_section_id=="LANDLINE"){
		$result = $oDealer->arrGetBlackListNumbers("","","1",$category_id,"");
        }
        if($view_section_id=="MOBILE"){
		$result = $oDealer->arrGetBlackListNumbers("","","2",$category_id,"");
        }

}
$cnt = sizeof($result);
//print "<pre>"; print_r($result);
$xml .= "<BLACK_LIST_NUMBER_MASTER>";
$xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
for($i=0;$i<$cnt;$i++){
	
	$black_list_number_id = $result[$i]['black_list_number_id'];
	$black_list_number = $result[$i]['black_list_number'];

	$status = $result[$i]['status'];
	$result[$i]['status'] = ($status == 1) ? 'Active' : 'InActive';

	$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
	//print "<pre>"; print_r($result[$i]);
	$xml .= "<BLACK_LIST_NUMBER_MASTER_DATA>";
	foreach($result[$i] as $k=>$v){
		$xml .= "<$k><![CDATA[$v]]></$k>";
	}
	$xml .= "</BLACK_LIST_NUMBER_MASTER_DATA>";
}
$xml .= "</BLACK_LIST_NUMBER_MASTER>";



$iRelUploadCnt= $iRelUploadCnt ? $iRelUploadCnt :1;
$config_details = get_config_details();

$strXML = "<XML>";
$strXML .= "<MSG><![CDATA[$msg]]></MSG>";
$strXML .= "<STARTLIMIT><![CDATA[$startlimit]]></STARTLIMIT>";
$strXML .= "<CNT><![CDATA[$limitcnt]]></CNT>";
$strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$category_id]]></SELECTED_CATEGORY_ID>"; 
$strXML .= $config_details;
$strXML .= $sSectionDetailXML;
$strXML .= $xml;
$strXML .= "<WALLCNT><![CDATA[$iRelUploadCnt]]></WALLCNT>";
$strXML .= "<SELECTED_ACTION_TYPE><![CDATA[$actiontype]]></SELECTED_ACTION_TYPE>";
$strXML .= "<VIEW_SECTION_ID><![CDATA[$view_section_id]]></VIEW_SECTION_ID>";
$strXML .= "<VIEWSECTION><![CDATA[$view_section_id]]></VIEWSECTION>";
$strXML .= "</XML>";

$strXML = mb_convert_encoding($strXML, "UTF-8");
if($_GET['debug']==1) { header('Content-type: text/xml');echo $strXML;exit; }


$doc = new DOMDocument();
$doc->loadXML($strXML);
$doc->saveXML();

$xslt = new xsltProcessor;
$xsl = DOMDocument::load('../xsl/black_listed_numbers_dashboard.xsl');

$xslt->importStylesheet($xsl);
print $xslt->transformToXML($doc);
?>
