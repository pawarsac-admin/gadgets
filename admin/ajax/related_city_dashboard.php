<?php	
require_once('../../include/config.php');
require_once(CLASSPATH.'DbConn.php');
require_once(CLASSPATH.'category.class.php');
//require_once(CLASSPATH.'product.class.php');
//require_once(CLASSPATH.'brand.class.php');
require_once(CLASSPATH.'citystate.class.php');
require_once(CLASSPATH.'pager.class.php');

$dbconn = new DbConn;
$category = new CategoryManagement;
//$product = new ProductManagement;
//$brand = new BrandManagement;
$oCityState = new citystate;
$oPager = new Pager;

//print "<pre>"; print_r($_REQUEST);
$category_id = $_REQUEST['catid'];
$category_id = ($category_id != "") ? $category_id : SITE_CATEGORY_ID;
$startlimit = $_REQUEST['startlimit'];
$limitcnt = $_REQUEST['cnt'];
$actiontype = $_REQUEST['actiontype'] ;

$related_id =$_REQUEST['related_id'] ? $_REQUEST['related_id'] : '';
$selected_city_id = $_REQUEST['selected_city_id'] ? $_REQUEST['selected_city_id'] : '';

if($_REQUEST['act']=='update' && !empty($related_id)){
	
	$result = $oCityState->arrGetRelatedCityDetails($related_id,"","",$category_id,"","","","");
	//print "<pre>"; print_r($result);print"</pre>";//exit;
	$cnt = sizeof($result);
	$xml .= "<RELATED_CITY_DETAIL>";
	$xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
	for($i=0;$i<$cnt;$i++){
		$status = $result[$i]['status'];
		$categoryid = $result[$i]['category_id'];
		if(!empty($categoryid)){
			$category_result = $category->arrGetCategoryDetails($categoryid);
		}
		$category_name = $category_result[0]['category_name'];
		$result[$i]['js_category_name'] = $category_name;
		$result[$i]['category_name'] = html_entity_decode($category_name,ENT_QUOTES);
		$result[$i]['status'] = ($status == 1) ? 'Active' : 'InActive';
		$result[$i]['create_date'] = date('d-m-Y',strtotime($result[$i]['create_date']));

		$city_id = $result[$i]['city_id'];
		unset($city_result);
	        if(!empty($city_id)){
                	$city_result = $oCityState->arrGetCityDetails($city_id);
        	}
	        $city_name = $city_result[0]['city_name'];
        	$result[$i]['js_city_name'] = $city_name;
	        $result[$i]['city_name'] = html_entity_decode($city_name,ENT_QUOTES);
        	$city_name = "";

		unset($city_result);
        	$related_city_id = $result[$i]['related_city_id'];
	        if(!empty($related_city_id)){
        	        $city_result = $oCityState->arrGetCityDetails($related_city_id);
	        }
        	$related_city_name = $city_result[0]['city_name'];
	        $result[$i]['js_related_city_name'] = $related_city_name;
        	$result[$i]['related_city_name'] = html_entity_decode($related_city_name,ENT_QUOTES);
	        $related_city_name = "";

		$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
		//print "<pre>"; print_r($result[$i]);
		$xml .= "<RELATED_CITY_DETAIL_DATA>";
		foreach($result[$i] as $k=>$v){
		$xml .= "<$k><![CDATA[$v]]></$k>";
		}
		$xml .= "</RELATED_CITY_DETAIL_DATA>";
	}
	$xml .= "</RELATED_CITY_DETAIL>";
}


unset($result);
$cnt="";
if(!empty($category_id) && !empty($selected_city_id)){
	 $result = $oCityState->arrGetRelatedCityDetails("",$selected_city_id,"",$category_id,"","","","");
}
$cnt = sizeof($result);
//print "<pre>"; print_r($result);print "</pre>";
$xml .= "<RELATED_CITY_MASTER>";
$xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
for($i=0;$i<$cnt;$i++){
	$status = $result[$i]['status'];
	$categoryid = $result[$i]['category_id'];
	if(!empty($categoryid)){
		$category_result = $category->arrGetCategoryDetails($categoryid);
	}
	$category_name = $category_result[0]['category_name'];
	$result[$i]['js_category_name'] = $category_name;
	$result[$i]['category_name'] = html_entity_decode($category_name,ENT_QUOTES);
	$result[$i]['status'] = ($status == 1) ? 'Active' : 'InActive';
	$result[$i]['create_date'] = date('d-m-Y',strtotime($result[$i]['create_date']));

	$city_id = $result[$i]['city_id'];
        unset($city_result);
        if(!empty($city_id)){
                $city_result = $oCityState->arrGetCityDetails($city_id);
        }
        $city_name = $city_result[0]['city_name'];
        $result[$i]['js_city_name'] = $city_name;
        $result[$i]['city_name'] = html_entity_decode($city_name,ENT_QUOTES);
        $city_name = "";

        unset($city_result);
        $related_city_id = $result[$i]['related_city_id'];
        if(!empty($related_city_id)){
                $city_result = $oCityState->arrGetCityDetails($related_city_id);
        }
        $related_city_name = $city_result[0]['city_name'];
        $result[$i]['js_related_city_name'] = $related_city_name;
        $result[$i]['related_city_name'] = html_entity_decode($related_city_name,ENT_QUOTES);
        $related_city_name = "";

	$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
	//print "<pre>"; print_r($result[$i]);print "</pre>";
	$xml .= "<RELATED_CITY_MASTER_DATA>";
	foreach($result[$i] as $k=>$v){
		$xml .= "<$k><![CDATA[$v]]></$k>";
	}
	$xml .= "</RELATED_CITY_MASTER_DATA>";
}
$xml .= "</RELATED_CITY_MASTER>";



unset($result);
if(!empty($category_id)){
	$result = $oCityState->arrGetCityDetails();
}	
$cnt = sizeof($result);
$xml .= "<CITY_MASTER>";
$xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
for($i=0;$i<$cnt;$i++){
	$status = $result[$i]['status'];
	$categoryid = $result[$i]['category_id'];
	if(!empty($categoryid)){
		$category_result = $category->arrGetCategoryDetails($categoryid);
	}
	$category_name = $category_result[0]['category_name'];
	$result[$i]['js_category_name'] = $category_name;
	$result[$i]['category_name'] = html_entity_decode($category_name,ENT_QUOTES);
	$result[$i]['city_status'] = ($status == 1) ? 'Active' : 'InActive';
	$result[$i]['create_date'] = date('d-m-Y',strtotime($result[$i]['create_date']));
	$result[$i]['js_city_name'] = $result[$i]['city_name'];
	$result[$i]['city_name'] = html_entity_decode($result[$i]['city_name'],ENT_QUOTES);
	$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
	$xml .= "<CITY_MASTER_DATA>";
	foreach($result[$i] as $k=>$v){
		$xml .= "<$k><![CDATA[$v]]></$k>";
	}
	$xml .= "</CITY_MASTER_DATA>";
}
$xml .= "</CITY_MASTER>";


$iRelUploadCnt= $iRelUploadCnt ? $iRelUploadCnt :1;
$config_details = get_config_details();

$strXML = "<XML>";
$strXML .= "<MSG><![CDATA[$msg]]></MSG>";
$strXML .= "<STARTLIMIT><![CDATA[$startlimit]]></STARTLIMIT>";
$strXML .= "<CNT><![CDATA[$limitcnt]]></CNT>";
$strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$category_id]]></SELECTED_CATEGORY_ID>"; 
$strXML .= $config_details;
$strXML .= $xml;
$strXML .= "<WALLCNT><![CDATA[$iRelUploadCnt]]></WALLCNT>";
$strXML .= "<SELECTED_CITY_ID><![CDATA[$selected_city_id]]></SELECTED_CITY_ID>";
$strXML .= "<SELECTED_ACTION_TYPE><![CDATA[$actiontype]]></SELECTED_ACTION_TYPE>";
$strXML .= "</XML>";

$strXML = mb_convert_encoding($strXML, "UTF-8");
if($_GET['debug']==1) { header('Content-type: text/xml');echo $strXML;exit; }


$doc = new DOMDocument();
$doc->loadXML($strXML);
$doc->saveXML();

$xslt = new xsltProcessor;
$xsl = DOMDocument::load('../xsl/related_city_dashboard.xsl');

$xslt->importStylesheet($xsl);
print $xslt->transformToXML($doc);
?>
