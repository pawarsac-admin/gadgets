<?php	
require_once('../../include/config.php');
require_once(CLASSPATH.'DbConn.php');
require_once(CLASSPATH.'category.class.php');
require_once(CLASSPATH.'article.class.php');
require_once(CLASSPATH.'pager.class.php');

$dbconn = new DbConn;
$category = new CategoryManagement;
$article = new article();
$oPager = new Pager;

//print"<pre>";print_r($_REQUEST);print"</pre>";

$category_id = $_REQUEST['catid'];
$startlimit = $_REQUEST['startlimit'];
$limitcnt = $_REQUEST['cnt'];
$view_section_id = $_REQUEST['view_section_id'];
$view_section_id = !empty($view_section_id) ? $view_section_id : ''; 
$type_selected=$_REQUEST['article_type_id'] ? $_REQUEST['article_type_id'] :0;
$category_id = $category_id ? $category_id : SITE_CATEGORY_ID;

$xml="";

if($_REQUEST['act']=='update' && $type_selected != 0){
	unset($result);
	$article_sub_type_id = $_REQUEST['article_sub_type_id'];
	$result = $article->arrGetArticleSubTypeDetails($article_sub_type_id,$type_selected,$category_id);
	//print"<pre>";print_r($result);
	$cnt = sizeof($result);
	$xml .= "<ARTICLE_DETAIL>";
	$xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
	//print"<pre>";print_r($result);print"</pre>";
	$id = $result[0]['id'];
	$article_type_id = $result[0]['article_type_id'];
	$article_sub_type_id = $result[0]['article_sub_type_id'];
	$status = $result[0]['status'];
        $result[0]['status'] = ($status == 1) ? 'Active' : 'InActive';

	$article_sub_type_name = $result[0]['sub_type_name'];
	$article_sub_type_name = html_entity_decode($article_sub_type_name,ENT_QUOTES,'UTF-8');
        $result[0]['article_sub_type_name'] = !empty($article_sub_type_name) ? $article_sub_type_name : '';
	
	$result[0] = array_change_key_case($result[0],CASE_UPPER);	
	$xml .= "<ARTICLE_DETAIL_DATA>";
	foreach($result[0] as $k=>$v){
                $xml .= "<$k><![CDATA[$v]]></$k>";
        }
        $xml .= "</ARTICLE_DETAIL_DATA>";

	$xml .= "</ARTICLE_DETAIL>";

}

unset($result);
$result = $article->arrGetArticleTypeDetails($category_id);
$cnt = sizeof($result);
//print"<pre>";print_r($result);print"</pre>";
$xml .= "<ARTICLE_TYPE_MASTER>";
$xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
for($i=0;$i<$cnt;$i++){
        $article_type_id = $result[$i]['article_type_id'];
        $type_name = $result[$i]['type_name'];
        $result[$i] = array_change_key_case($result[$i],CASE_UPPER);
        //print "<pre>"; print_r($result[$i]);
        $xml .= "<ARTICLE_TYPE_MASTER_DATA>";
        foreach($result[$i] as $k=>$v){
                $xml .= "<$k><![CDATA[$v]]></$k>";
        }
        $xml .= "</ARTICLE_TYPE_MASTER_DATA>";
}
$xml .= "</ARTICLE_TYPE_MASTER>";

unset($result);
if(!empty($category_id) && !empty($view_section_id) ){
	$result = $article->arrGetArticleSubTypeDetails("",$view_section_id,$category_id,"","");
}
//print"<pre>";print_r($result);print"</pre>";
$cnt = sizeof($result);
if(!$cnt){$cnt = 0;}
$xml .= "<ARTICLE_SUB_TYPE_MASTER>";
$xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
for($i=0;$i<$cnt;$i++){
        $article_sub_type_id = $result[$i]['article_sub_type_id'];
        $article_type_id = $result[$i]['article_type_id'];
        $result[$i]['category_id'] = $result[$i]['category_id'];
        $result[$i]['sub_type_name'] = $result[$i]['sub_type_name'];
        $result[$i]['article_type_id'] = $article_type_id;
        $article_sub_type_id = $result[$i]['article_sub_type_id'];
	$result[$i]['article_sub_type_id'] = $article_sub_type_id;
        $status = $result[$i]['status'];
        $result[$i]['status'] = ($status == 1) ? 'Active' : 'InActive';
        $result[$i]['create_date'] = date('d-m-Y',strtotime($result[$i]['create_date']));
        $result[$i]['update_date'] = date('d-m-Y',strtotime($result[$i]['update_date']));

	if($article_type_id != ""){
		$article_type_result = $article->arrGetArticleTypeDetails($category_id,$article_type_id);
        	$article_type_name = $article_type_result[0]['type_name'];
	}
        $result[$i]['article_type_name'] = !empty($article_type_name) ? $article_type_name : '';

	/*if($article_sub_type_id != ""){
		$article_sub_type_result = $article->arrGetArticleSubTypeDetails($article_sub_type_id,"",$category_id,"","");
        	$article_sub_type_name = $article_sub_type_result[0]['sub_type_name'];
		if(!empty($article_sub_type_name)){
			$article_sub_type_name = html_entity_decode($article_sub_type_name,ENT_QUOTES,'UTF-8');
		}
	}*/
    //    $result[$i]['article_sub_type_name'] = !empty($article_sub_type_name) ? $article_sub_type_name : '';

        $result[$i] = array_change_key_case($result[$i],CASE_UPPER);
        //print "<pre>"; print_r($result[$i]);
        $xml .= "<ARTICLE_SUB_TYPE_MASTER_DATA>";
        foreach($result[$i] as $k=>$v){
                $xml .= "<$k><![CDATA[$v]]></$k>";
        }
        $xml .= "</ARTICLE_SUB_TYPE_MASTER_DATA>";
}
$xml .= "</ARTICLE_SUB_TYPE_MASTER>";

$config_details = get_config_details();

$strXML = "<XML>";
$strXML .= "<MSG><![CDATA[$msg]]></MSG>";
$strXML .= "<STARTLIMIT><![CDATA[$startlimit]]></STARTLIMIT>";
$strXML .= "<CNT><![CDATA[$limitcnt]]></CNT>";
$strXML .= "<VIEW_SECTION_ID><![CDATA[$view_section_id]]></VIEW_SECTION_ID>";
$strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$category_id]]></SELECTED_CATEGORY_ID>"; 
$strXML .= $config_details;
$strXML .= $xml;
$strXML .= $nodesPaging;
$strXML .= "<WALLCNT><![CDATA[$iRelUploadCnt]]></WALLCNT>";
$strXML .= "<VIEWSECTION><![CDATA[$view_section_id]]></VIEWSECTION>";
$strXML .= "<ARTICLETYPE><![CDATA[$type_selected]]></ARTICLETYPE>";
$strXML .= "</XML>";

//$strXML = mb_convert_encoding($strXML, "UTF-8");
if($_GET['debug']==1) { header('Content-type: text/xml');echo $strXML;exit; }

$doc = new DOMDocument();
$doc->loadXML($strXML);
$doc->saveXML();

$xslt = new xsltProcessor;
$xsl = DOMDocument::load('../xsl/article_sub_type_dashboard.xsl');

$xslt->importStylesheet($xsl);
print $xslt->transformToXML($doc);
?>
