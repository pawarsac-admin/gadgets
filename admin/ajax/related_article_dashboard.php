<?php	
require_once('../../include/config.php');
require_once(CLASSPATH.'DbConn.php');
require_once(CLASSPATH.'category.class.php');
require_once(CLASSPATH.'product.class.php');
require_once(CLASSPATH.'brand.class.php');
require_once(CLASSPATH.'article.class.php');

$dbconn = new DbConn;
$category = new CategoryManagement;
$product = new ProductManagement;
$brand = new BrandManagement;
$oArticle = new article;

$article_id = $_REQUEST['aid'];
$category_id = $_REQUEST['catid'];
$startlimit = $_REQUEST['startlimit'];
$limitcnt = $_REQUEST['cnt'];
$r_product_id = $_REQUEST['pid'];
$r_brand_id = $_REQUEST['bid'];
$type_selecetd=$_REQUEST['article_type_id'] ? $_REQUEST['article_type_id'] :0;
$category_id = $category_id ? $category_id:1;
$view_section_id=$_REQUEST['view_section_id'] ? $_REQUEST['view_section_id'] :"LATEST_ARTICLE";
$section_id=$_REQUEST['section_article_id'] ? $_REQUEST['section_article_id'] :"1";


if(!empty($category_id)){
	$aArticleGroupDetail = $oArticle->arrGetArticleGroupDetails($category_id);
	if(is_array($aArticleGroupDetail)){
		foreach($aArticleGroupDetail as $igKey=>$aGrpValue){
			$aArticleGroupData[$aGrpValue['group_id']]=$aGrpValue['group_name'];
		}
	}
	$sArticleGroupDetail=arraytoxml($aArticleGroupDetail,"ARTICLE_GROUP_DATA");
	$sArticleGroupDetailXML ="<ARTICLE_GROUP_MASTER>".$sArticleGroupDetail."</ARTICLE_GROUP_MASTER>";

	$aArticleTypeDetail = $oArticle->arrGetArticleTypeDetails($category_id);
	if(is_array($aArticleTypeDetail)){
		foreach($aArticleTypeDetail as $itKey=>$aTypeValue){
			$aArticleTypeData[$aTypeValue['article_type_id']]=$aTypeValue['type_name'];
		}
	}
	$sArticleTypeDetail=arraytoxml($aArticleTypeDetail,"ARTICLE_TYPE_DATA");
	$sArticleTypeDetailXML ="<ARTICLE_TYPE_MASTER>".$sArticleTypeDetail."</ARTICLE_TYPE_MASTER>";
}

if(!empty($category_id)){

	$aParameters=array('category_id'=>$category_id);
	if($view_section_id=="LATEST_ARTICLE"){
		$result=$oArticle->arrGetLatestArticleDetails("","",$category_id,"","");
	}
	if($view_section_id=="FEATURED_ARTICLE"){
		$result=$oArticle->arrGetFeaturedArticleDetails("","",$category_id,"","");
	}
	if($view_section_id=="RELATED_ARTICLE"){
		$result=$oArticle->arrGetRelatedArticleDetails("","",$category_id,"","");
	}
}
//print_r($aArticleGroupData);print_r($aArticleTypeData);
if($_REQUEST['section_article_id']!=''){
	$section_id = $_REQUEST['section_article_id'];
}
if($_REQUEST['act']=='Delete' && !empty($section_id)){
	$dresult = $oArticle->deleteArticle($article_id,'ARTICLE');
}



if($_REQUEST['act']=='update' && !empty($section_id)){
	//echo "SEC---".$section_id;
	if($view_section_id=="LATEST_ARTICLE"){
		$rResult=$oArticle->arrGetLatestArticleDetails($section_id,"",$category_id,"","");
	}
	if($view_section_id=="FEATURED_ARTICLE"){
		$rResult=$oArticle->arrGetFeaturedArticleDetails($section_id,"",$category_id,"","");
	}
	if($view_section_id=="RELATED_ARTICLE"){
		$rResult=$oArticle->arrGetRelatedArticleDetails($section_id,"",$category_id,"","");
	}
	
	$xmlArt='';
	$cnt = sizeof($rResult);
	//print "<pre>"; print_r($rResult);
	$status = $rResult[0]['status'];
	$categoryid = $rResult[0]['category_id'];
	if(!empty($categoryid)){
		$category_result = $category->arrGetCategoryDetails($categoryid);
	}

	$rResult[0]['js_title'] = $rResult[0]['title'];
	$rResult[0]['title'] = $rResult[0]['title'] ? html_entity_decode($rResult[0]['title'],ENT_QUOTES) : 'Nil';

	$rResult[0]['status'] = ($status == 1) ? 'Active' : 'InActive';
	$category_name = $category_result[0]['category_name'];
	$rResult[0]['js_category_name'] = $category_name;
	$rResult[0]['category_name'] = html_entity_decode($category_name,ENT_QUOTES);
	$rResult[0]['create_date'] = date('d-m-Y',strtotime($rResult[0]['create_date']));
	$rResult[0] = array_change_key_case($rResult[0],CASE_UPPER);
	$status='';
	//print "<pre>"; print_r($rResult[0]);
	$xmlArt .= "<ARTICLE_DATA>";
	foreach($rResult[0] as $k1=>$v1){
		$xmlArt .= "<$k1><![CDATA[$v1]]></$k1>";
	}
	$xmlArt .= "</ARTICLE_DATA>";
}

//print "<pre>"; print_r($result);
$cnt = sizeof($result);
if(is_array($result)){
$xml = "<ARTICLE_MASTER>";
$xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";	
for($i=0;$i<$cnt;$i++){
	$status = $result[$i]['status'];
	$categoryid = $result[$i]['category_id'];
	$categoryid=1;
	if(!empty($categoryid)){
		$category_result = $category->arrGetCategoryDetails($categoryid);
	}
	$article_type = $result[$i]['article_type'];
	if(!empty($article_type)){
		if(is_array($aArticleTypeData) && isset($aArticleTypeData[$article_type])){
			$result[$i]['article_type_name']=$aArticleTypeData[$article_type];
		}
	}
	$result[$i]['js_title'] = $result[$i]['title'];
	$result[$i]['title'] = $result[$i]['title'] ? html_entity_decode($result[$i]['title'],ENT_QUOTES) : 'Nil';
	
	$result[$i]['js_title'] = $result[$i]['title'];
	$result[$i]['title'] = $result[$i]['title'] ? html_entity_decode($result[$i]['title'],ENT_QUOTES) : 'Nil';
	$result[$i]['status'] = ($status == 1) ? 'Active' : 'InActive';
	$category_name = $category_result[0]['category_name'];
	$result[$i]['js_category_name'] = $category_name;
	$result[$i]['category_name'] = html_entity_decode($category_name,ENT_QUOTES);
	$result[$i]['create_date'] = date('d-m-Y',strtotime($result[$i]['create_date']));
	$status='';
	
	if(is_array($aArticleTypeData) && isset($aArticleTypeData[$result[$i]['article_type']])){
		$result[$i]['article_type_name'] =$aArticleTypeData[$result[$i]['article_type']];	
	}
	$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
	//print "<pre>"; print_r($result[$i]);
	$xml .= "<ARTICLE_MASTER_DATA>";
	foreach($result[$i] as $k=>$v){
		$xml .= "<$k><![CDATA[$v]]></$k>";
	}
	$xml .= "</ARTICLE_MASTER_DATA>";
}
$xml .= "</ARTICLE_MASTER>";
}
if(!empty($category_id)){
	$result = $brand->arrGetBrandDetails("",$category_id);
}	
$cnt = sizeof($result);
$xml .= "<BRAND_MASTER>";
$xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
for($i=0;$i<$cnt;$i++){
	$status = $result[$i]['status'];
	$categoryid = $result[$i]['category_id'];
	if(!empty($categoryid)){
		$category_result = $category->$result = $category->arrGetCategoryDetails($categoryid);
	}
	$category_name = $category_result[0]['category_name'];
	$result[$i]['js_category_name'] = $category_name;
	$result[$i]['category_name'] = html_entity_decode($category_name,ENT_QUOTES);
	$result[$i]['brand_status'] = ($status == 1) ? 'Active' : 'InActive';
	$result[$i]['create_date'] = date('d-m-Y',strtotime($result[$i]['cdate']));
	$result[$i]['js_brand_name'] = $result[$i]['brand_name'];
	$result[$i]['brand_name'] = html_entity_decode($result[$i]['brand_name'],ENT_QUOTES);
	$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
	//print "<pre>"; print_r($result[$i]);
	$xml .= "<BRAND_MASTER_DATA>";
	foreach($result[$i] as $k=>$v){
		$xml .= "<$k><![CDATA[$v]]></$k>";
	}
	$xml .= "</BRAND_MASTER_DATA>";
}
$xml .= "</BRAND_MASTER>";

//$aArticleSection=array("1"=>"LATEST_ARTICLE","2"=>"FEATURED_ARTICLE","3"=>"RELATED_ARTICLE");

$aArticleSectionDet=array("0"=>array("SECTION_ID"=>"LATEST_ARTICLE","SECTION_NAME"=>"Latest Article"),"1"=>array("SECTION_ID"=>"FEATURED_ARTICLE","SECTION_NAME"=>"Featured Article"),"2"=>array("SECTION_ID"=>"RELATED_ARTICLE","SECTION_NAME"=>"Related Article"));


//print "<pre>"; print_r($aArticleSectionDet);
$sArticleSectionDetail=arraytoxml($aArticleSectionDet,"ARTICLE_SECTION");
$sArticleSectionDetailXML ="<ARTICLE_SECTION_MASTER>".$sArticleSectionDetail."</ARTICLE_SECTION_MASTER>";

$iRelUploadCnt= $iRelUploadCnt ? $iRelUploadCnt :1;
$config_details = get_config_details();

$strXML = "<XML>";
$strXML .= "<MSG><![CDATA[$msg]]></MSG>";
$strXML .= "<STARTLIMIT><![CDATA[$startlimit]]></STARTLIMIT>";
$strXML .= "<CNT><![CDATA[$limitcnt]]></CNT>";
$strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$category_id]]></SELECTED_CATEGORY_ID>"; 
$strXML .= $config_details;
$strXML .= $xml;
$strXML .= $xmlArt;
$strXML .= $sArticleGroupDetailXML;
$strXML .= $sArticleTypeDetailXML;
$strXML .= $sArticleMediaDataDetXML;
$strXML .= $sArticleSectionDetailXML;
$strXML .= "<WALLCNT><![CDATA[$iRelUploadCnt]]></WALLCNT>";
$strXML .= "<VIEWSECTION><![CDATA[$view_section_id]]></VIEWSECTION>";
$strXML .= "<ARTICLETYPE><![CDATA[$type_selecetd]]></ARTICLETYPE>";
$strXML .= "</XML>";

$strXML = mb_convert_encoding($strXML, "UTF-8");
if($_GET['debug']==1) { header('Content-type: text/xml');echo $strXML;exit; }

$doc = new DOMDocument();
$doc->loadXML($strXML);
$doc->saveXML();

$xslt = new xsltProcessor;
$xsl = DOMDocument::load('../xsl/related_article_dashboard.xsl');

$xslt->importStylesheet($xsl);
print $xslt->transformToXML($doc);
?>
