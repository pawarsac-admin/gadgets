<?php	
require_once('../../include/config.php');
require_once(CLASSPATH.'DbConn.php');
require_once(CLASSPATH.'category.class.php');
require_once(CLASSPATH.'product.class.php');
require_once(CLASSPATH.'brand.class.php');
require_once(CLASSPATH.'feature.class.php');
require_once(CLASSPATH.'pager.class.php');
require_once(CLASSPATH.'pivot.class.php');

$dbconn = new DbConn;
$category = new CategoryManagement;
$product = new ProductManagement;
$brand = new BrandManagement;
$feature = new FeatureManagement;
$pivot = new PivotManagement;
$oPager = new Pager;

//print "<pre>"; print_r($_REQUEST);
$category_id = $_REQUEST['catid'];
$category_id = ($category_id != "") ? $category_id : SITE_CATEGORY_ID;
$startlimit = $_REQUEST['startlimit'];
$limitcnt = $_REQUEST['cnt'];
$actiontype = $_REQUEST['actiontype'] ;

$popular_feature_id =$_REQUEST['popular_feature_id'];
$selected_pivot_id = $_REQUEST['pivot_id'];
$selected_pivot_group = $_REQUEST['pivot_group'];
$selected_product_id = $_REQUEST['product_id'];

if($_REQUEST['act']=='update' && !empty($popular_feature_id)){

	$result = $feature->arrGetPopularFeatureCarDetails($popular_feature_id , $selected_pivot_group, $selected_pivot_id, "", "", "", $category_id,"","","","");
	//print "<pre>"; print_r($result);print"</pre>";//exit;
	$cnt = sizeof($result);
	$xml .= "<POPULAR_FEATURE_CARS_DETAIL>";
	$xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
	for($i=0;$i<$cnt;$i++){	
		$popular_feature_id = $result[$i]['popular_feature_id'];
        	$pivot_group = $result[$i]['pivot_group'];      
	        $pivot_id = $result[$i]['pivot_id'];    
        	$brand_id = $result[$i]['brand_id'];
	        $model_id = $result[$i]['model_id'];
        	$product_id = $result[$i]['product_id'];
  	        $status = $result[$i]['status'];
	        $categoryid = $result[$i]['category_id'];
        	if(!empty($categoryid)){
                	$category_result = $category->arrGetCategoryDetails($categoryid);
	        }
        	$category_name = $category_result[0]['category_name'];
	        $result[$i]['js_category_name'] = $category_name;
        	$result[$i]['category_name'] = html_entity_decode($category_name,ENT_QUOTES);
	        $result[$i]['status'] = ($status == 1) ? 'Active' : 'InActive';
        	$result[$i]['create_date'] = date('d-m-Y',strtotime($result[$i]['create_date']));

		if(!empty($pivot_id)){
        	        $pivot_result = $pivot->arrGetPivotDetails($pivot_id,$category_id,"","",$pivot_group);
                	$pivot_id = $result[0]['pivot_id'];
	                $feature_id = $result[0]['feature_id'];
        	        $feature_result = $feature->arrGetFeatureDetails($feature_id,$category_id);
                	$feature_name = $feature_result[0]['feature_name'];
	        }
        	$result[$i]['pivot_name'] = $feature_name ? $feature_name : '';

	        $prod_name = "";
        	if(!empty($product_id)){
                	$product_result =$product->arrGetProductDetails($product_id,$category_id);
	                $product_name = $product_result[0]['product_name'];
        	        $variant_name = $product_result[0]['variant'];
                	$brand_id = $product_result[0]['brand_id'];
	                if(!empty($brand_id)){
        	                $brand_result = $brand->arrGetBrandDetails($brand_id);
                	        $brand_name = $brand_result[0]['brand_name'];
                        	$prod_name .= $brand_name;
	                }
        	        if(($variant_name != "") && ($product_name != "")){
                	        $prod_name .= " ".$product_name." ".$variant_name;
	                }
        	}
	        $result[$i]['product_name'] = (!empty($prod_name)) ? $prod_name : '';
	
		$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
		//print "<pre>"; print_r($result[$i]);
		$xml .= "<POPULAR_FEATURE_CARS_DETAIL_DATA>";
		foreach($result[$i] as $k=>$v){
			$xml .= "<$k><![CDATA[$v]]></$k>";
		}
		$xml .= "</POPULAR_FEATURE_CARS_DETAIL_DATA>";
	}

	$xml .= "</POPULAR_FEATURE_CARS_DETAIL>";
}


unset($result);
if(!empty($category_id) && !empty($selected_pivot_group) && !empty($selected_pivot_id)){
	 $result = $feature->arrGetPopularFeatureCarDetails("", $selected_pivot_group, $selected_pivot_id, "", "", "", $category_id,"","","","");
}
//print "<pre>"; print_r($result);
$cnt = sizeof($result);
$xml .= "<POPULAR_FEATURE_CARS_MASTER>";
$xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
for($i=0;$i<$cnt;$i++){
	$popular_feature_id = $result[$i]['popular_feature_id'];	
	$pivot_group = $result[$i]['pivot_group'];	
	$pivot_id = $result[$i]['pivot_id'];	
	$brand_id = $result[$i]['brand_id'];	
	$model_id = $result[$i]['model_id'];	
	$product_id = $result[$i]['product_id'];	
	$status = $result[$i]['status'];
	$categoryid = $result[$i]['category_id'];
	if(!empty($categoryid)){
		$category_result = $category->arrGetCategoryDetails($categoryid);
	}
	$category_name = $category_result[0]['category_name'];
	$result[$i]['js_category_name'] = $category_name;
	$result[$i]['category_name'] = html_entity_decode($category_name,ENT_QUOTES);
	$result[$i]['status'] = ($status == 1) ? 'Active' : 'InActive';
	$result[$i]['create_date'] = date('d-m-Y',strtotime($result[$i]['create_date']));

	if(!empty($pivot_id)){
		unset($pivot_result);
		$pivot_result = $pivot->arrGetPivotDetails($pivot_id,$category_id,"","",$pivot_group);
		$pivot_id = $pivot_result[0]['pivot_id'];
                $feature_id = $pivot_result[0]['feature_id'];
		unset($feature_result);
                $feature_result = $feature->arrGetFeatureDetails($feature_id,$category_id);
                $feature_name = $feature_result[0]['feature_name'];
	}
	$result[$i]['pivot_name'] = $feature_name ? $feature_name : '';

	$prod_name = "";
        if(!empty($product_id)){
        	$product_result =$product->arrGetProductDetails($product_id,$category_id);
                $product_name = $product_result[0]['product_name'];
                $variant_name = $product_result[0]['variant'];
                $brand_id = $product_result[0]['brand_id'];
                if(!empty($brand_id)){
                	$brand_result = $brand->arrGetBrandDetails($brand_id);
                        $brand_name = $brand_result[0]['brand_name'];
                        $prod_name .= $brand_name;
		}
                if(($variant_name != "") && ($product_name != "")){
                	$prod_name .= " ".$product_name." ".$variant_name;
                }
	}
	$result[$i]['product_name'] = (!empty($prod_name)) ? $prod_name : '';

	$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
	//print "<pre>"; print_r($result[$i]);
	$xml .= "<POPULAR_FEATURE_CARS_MASTER_DATA>";
	foreach($result[$i] as $k=>$v){
		$xml .= "<$k><![CDATA[$v]]></$k>";
	}
	$xml .= "</POPULAR_FEATURE_CARS_MASTER_DATA>";
}
$xml .= "</POPULAR_FEATURE_CARS_MASTER>";

unset($result);
if(!empty($category_id)){
        $result = $pivot->arrPivotSubGroupDetails("",$category_id);
}
	$cnt = sizeof($result);
	$xml .= "<PIVOT_GROUP_MASTER>";
        $xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
        for($i=0;$i<$cnt;$i++){
		$pivot_group = $result[$i]['sub_group_id'];
		$pivot_group_name = $result[$i]['sub_group_name'];
                $result[$i]['pivot_group'] = $pivot_group;
                $result[$i]['pivot_group_name'] = html_entity_decode($pivot_group_name,ENT_QUOTES);

                $result[$i] = array_change_key_case($result[$i],CASE_UPPER);
                $xml .= "<PIVOT_GROUP_MASTER_DATA>";
                foreach($result[$i] as $k=>$v){
                        $xml .= "<$k><![CDATA[$v]]></$k>";
                }
                $xml .= "</PIVOT_GROUP_MASTER_DATA>";
	}
	$xml .= "</PIVOT_GROUP_MASTER>";


        unset($result);
	if(!empty($selected_pivot_group)){
	        $result = $pivot->arrGetPivotDetails("",$category_id,"","",$selected_pivot_group);
	}
       	$cnt = sizeof($result);
	$xml .= "<PIVOT_MASTER>";
        $xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
       	for($i=0;$i<$cnt;$i++){
               	$pivot_id = $result[$i]['pivot_id'];
                $feature_id = $result[$i]['feature_id'];
       	        $feature_result = $feature->arrGetFeatureDetails($feature_id,$category_id);
       	        $feature_name = $feature_result[0]['feature_name'];
		$feature_result[$i]['pivot_id'] = $pivot_id;	
		$feature_result[$i]['pivot_name'] = $feature_name;	

		$feature_result[$i] = array_change_key_case($feature_result[$i],CASE_UPPER);
                $xml .= "<PIVOT_MASTER_DATA>";
                foreach($feature_result[$i] as $k=>$v){
                        $xml .= "<$k><![CDATA[$v]]></$k>";
                }
                $xml .= "</PIVOT_MASTER_DATA>";
       	}
	$xml .= "</PIVOT_MASTER>";

	unset($pivot_result);
	unset($product_feature_result);
        if(!empty($selected_pivot_group) && !empty($selected_pivot_id)){
        	$pivot_result = $pivot->arrGetPivotDetails($selected_pivot_id,$category_id,"","",$selected_pivot_group);
      		$feature_id = $pivot_result[0]['feature_id'];
	       	$product_feature_result = $product->arrGetProductDetailsUsingFeatureid("",$feature_id);
	}
       	$cnt = sizeof($product_feature_result);
	$xml .= "<PRODUCT_MASTER>";
        $xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
       	for($i=0;$i<$cnt;$i++){
               	$product_id = $product_feature_result[$i]['product_id'];
                $prod_name = "";
			
                if(!empty($product_id)){
       	                //$product_result =$product->arrGetProductDetails($product_id,$category_id);
               	        $product_name = $product_feature_result[$i]['product_name'];
                       	$variant_name = $product_feature_result[$i]['variant'];
                        $brand_id = $product_feature_result[$i]['brand_id'];
       	                if(!empty($brand_id)){
               	                $brand_result = $brand->arrGetBrandDetails($brand_id);
                       	        $brand_name = $brand_result[0]['brand_name'];
                               	$prod_name .= $brand_name;
                        }
       	                if(($variant_name != "") && ($product_name != "")){
               	                $prod_name .= " ".$product_name." ".$variant_name;
                       	}
                }
		$product_feature_result[$i]['product_id'] = (!empty($product_id)) ? $product_id : '';
                $product_feature_result[$i]['product_name'] = (!empty($prod_name)) ? $prod_name : '';
               	$product_feature_result[$i] = array_change_key_case($product_feature_result[$i],CASE_UPPER);
      	        $xml .= "<PRODUCT_MASTER_DATA>";
                foreach($product_feature_result[$i] as $k=>$v){
               	        $xml .= "<$k><![CDATA[$v]]></$k>";
       	        }
                $xml .= "</PRODUCT_MASTER_DATA>";
	}
	$xml .= "</PRODUCT_MASTER>";

$iRelUploadCnt= $iRelUploadCnt ? $iRelUploadCnt :1;
$config_details = get_config_details();

$strXML = "<XML>";
$strXML .= "<MSG><![CDATA[$msg]]></MSG>";
$strXML .= "<STARTLIMIT><![CDATA[$startlimit]]></STARTLIMIT>";
$strXML .= "<CNT><![CDATA[$limitcnt]]></CNT>";
$strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$category_id]]></SELECTED_CATEGORY_ID>"; 
$strXML .= $config_details;
$strXML .= $xml;
$strXML .= "<WALLCNT><![CDATA[$iRelUploadCnt]]></WALLCNT>";
$strXML .= "<SELECTED_PIVOT_GROUP><![CDATA[$selected_pivot_group]]></SELECTED_PIVOT_GROUP>";
$strXML .= "<SELECTED_PIVOT_ID><![CDATA[$selected_pivot_id]]></SELECTED_PIVOT_ID>";
$strXML .= "<SELECTED_PRODUCT_ID><![CDATA[$selected_product_id]]></SELECTED_PRODUCT_ID>";
$strXML .= "<SELECTED_ACTION_TYPE><![CDATA[$actiontype]]></SELECTED_ACTION_TYPE>";
$strXML .= "</XML>";

$strXML = mb_convert_encoding($strXML, "UTF-8");
if($_GET['debug']==1) { header('Content-type: text/xml');echo $strXML;exit; }


$doc = new DOMDocument();
$doc->loadXML($strXML);
$doc->saveXML();

$xslt = new xsltProcessor;
$xsl = DOMDocument::load('../xsl/popular_feature_cars_dashboard.xsl');

$xslt->importStylesheet($xsl);
print $xslt->transformToXML($doc);
?>
