<?php
	require_once('../../include/config.php');
	require_once(CLASSPATH.'DbConn.php');
	require_once(CLASSPATH.'citystate.class.php');
	$dbconn = new DbConn;
	$citystate = new citystate;
	$state_id = $_REQUEST['state_id'];
	$result = $citystate->arrGetCityDetails($state_id);
	$cnt = sizeof($result);
	$html .= '<select name="city_id" id="city_id"><option value="">---Select City---</option>';
	for($i=0;$i<$cnt;$i++){
		$city_id = $result[$i]['city_id'];
		$city_name = html_entity_decode($result[$i]['city_name'],ENT_QUOTES);
		$html .= "<option value='$city_id'>$city_name</option>";
	}
	$html .= "</select>";
	echo $html;	
?>
