<?php	
require_once('../../include/config.php');
require_once(CLASSPATH.'DbConn.php');
require_once(CLASSPATH.'category.class.php');
require_once(CLASSPATH.'product.class.php');
require_once(CLASSPATH.'brand.class.php');
require_once(CLASSPATH.'article.class.php');
require_once(CLASSPATH.'pager.class.php');

$dbconn = new DbConn;
$category = new CategoryManagement;
$product = new ProductManagement;
$brand = new BrandManagement;
$oArticle = new article;
$oPager = new Pager;

$category_id = $_REQUEST['catid'];
$startlimit = $_REQUEST['startlimit'];
$limitcnt = $_REQUEST['cnt'];
$r_product_id = $_REQUEST['pid'];
$r_brand_id = $_REQUEST['bid'];
$type_selecetd=$_REQUEST['article_type_id'] ? $_REQUEST['article_type_id'] :0;

if(!empty($category_id)){
	$aArticleGroupDetail = $oArticle->arrGetArticleGroupDetails($category_id);
	if(is_array($aArticleGroupDetail)){
		foreach($aArticleGroupDetail as $igKey=>$aGrpValue){
			$aArticleGroupData[$aGrpValue['group_id']]=$aGrpValue['group_name'];
		}
	}
	$sArticleGroupDetail=arraytoxml($aArticleGroupDetail,"ARTICLE_GROUP_DATA");
	$sArticleGroupDetailXML ="<ARTICLE_GROUP_MASTER>".$sArticleGroupDetail."</ARTICLE_GROUP_MASTER>";

	$aArticleTypeDetail = $oArticle->arrGetArticleTypeDetails($category_id);
	if(is_array($aArticleTypeDetail)){
		foreach($aArticleTypeDetail as $itKey=>$aTypeValue){
			$aArticleTypeData[$aTypeValue['article_type_id']]=$aTypeValue['type_name'];
		}
	}
	$sArticleTypeDetail=arraytoxml($aArticleTypeDetail,"ARTICLE_TYPE_DATA");
	$sArticleTypeDetailXML ="<ARTICLE_TYPE_MASTER>".$sArticleTypeDetail."</ARTICLE_TYPE_MASTER>";
}

if($type_selecetd!=0){$type_id=$type_selecetd;}else{$type_id="";}
$iArticleItemCount = $oArticle->getArticleCount("","",$type_id,"","",$category_id,"","","");
if($iArticleItemCount!=0){
	$page = $_REQUEST['page'] ? $_REQUEST['page'] : 1;
	$perpage=10;
	$start  = $oPager->findStart($perpage);
	$recordcount=$iArticleItemCount;
	$sExtraParam="ajax/article_ajax_list.php,DivArticle,$category_id";
	$jsparams=$start.",".$perpage.",".$sExtraParam;
	$pages= $oPager->findPages($recordcount,$perpage);
	if($pages > 1 ){
		$pagelist= $oPager->jsPageNumNextPrev($page,$pages,"sArticlePagination",$jsparams,"text");
		$nodesPaging .= "<Pages><![CDATA[".$pagelist."]]></Pages>";
		$nodesPaging .= "<Page><![CDATA[".$page."]]></Page>";
		$nodesPaging .= "<Perpage><![CDATA[".$perpage."]]></Perpage>";
	}
	$orderby=" order by A.create_date desc";
	$result = $oArticle->getArticleDetails("","",$type_id,"","",$category_id,"","",$start,$perpage,$orderby);
}
//print "<pre>"; print_r($result);
//echo "<br>=========================================<br>";
$cnt = sizeof($result);
$xml = "<ARTICLE_MASTER>";
$xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";	
for($i=0;$i<$cnt;$i++){
	$status = $result[$i]['status'];
	$categoryid = $result[$i]['category_id'];
	if(!empty($categoryid)){
		$category_result = $category->arrGetCategoryDetails($categoryid);
	}
	$brand_id = $result[$i]['brand_id'];
	if(!empty($brand_id)){
		$brand_result = $brand->arrGetBrandDetails($brand_id);
		$brand_name = $brand_result[0]['brand_name'];
	}
	$result[$i]['js_brand_name'] = $brand_name;
	$result[$i]['brand_name'] = $brand_name ? html_entity_decode($brand_name,ENT_QUOTES) : 'Nil';
	
	$product_info_id1 = $result[$i]['product_info_id'];
	if(!empty($product_info_id1) && $product_info_id1!=0){
		$product_info_result = $product->arrGetProductNameInfo($product_info_id1,$category_id,"","","1");
		$product_info_name = $product_info_result[0]['product_info_name'];
		$result[$i]['product_name'] = $product_info_name ? html_entity_decode($product_info_name,ENT_QUOTES) : 'Nil';
	}
	$product_info_name='';
	$product_id1 = $result[$i]['product_id'];
	if(!empty($product_id1) && $product_id1!=0){
		//echo "ENTERE====START";
		$product_result =$product->arrGetProductDetails($product_id1,$category_id,"","1","","","","","");
		//print "<pre>"; print_r($product_result);
		$product_name1 = $product_result[0]['product_name'];
		$product_variant1 = $product_result[0]['variant'];
		$result[$i]['js_product_name'] =$product_name1;
		$result[$i]['product_name'] = $product_name1 ? html_entity_decode($product_name1,ENT_QUOTES) : 'Nil';
		$result[$i]['js_variant'] =$product_variant1;
		$result[$i]['variant'] = $product_variant1 ? html_entity_decode($product_variant1,ENT_QUOTES) : 'Nil';
		//echo "ENTERE====END";
	}
	
	$result[$i]['js_title'] = $result[$i]['title'];
	$result[$i]['title'] = $result[$i]['title'] ? html_entity_decode($result[$i]['title'],ENT_QUOTES) : '';
	$result[$i]['js_abstract'] = $result[$i]['abstract'];
	$result[$i]['abstract'] = $result[$i]['abstract'] ? html_entity_decode($result[$i]['abstract'],ENT_QUOTES) : '';
	$result[$i]['js_title'] = $result[$i]['title'];
	$result[$i]['title'] = $result[$i]['title'] ? html_entity_decode($result[$i]['title'],ENT_QUOTES) : '';
	$result[$i]['article_status'] = ($status == 1) ? 'Active' : 'InActive';
	$category_name = $category_result[0]['category_name'];
	$result[$i]['js_category_name'] = $category_name;
	$result[$i]['category_name'] = html_entity_decode($category_name,ENT_QUOTES);
	$result[$i]['create_date'] = date('d-m-Y',strtotime($result[$i]['create_date']));
	
	if(is_array($aArticleTypeData) && isset($aArticleTypeData[$result[$i]['article_type']])){
		$result[$i]['article_type_name'] =$aArticleTypeData[$result[$i]['article_type']];	
	}
	if(is_array($aArticleGroupData) && isset($aArticleGroupData[$result[$i]['group_id']])){
		$result[$i]['article_group_name'] =$aArticleGroupData[$result[$i]['group_id']];	
	}
	
	$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
	//print "<pre>"; print_r($result[$i]);
	$xml .= "<ARTICLE_MASTER_DATA>";
	foreach($result[$i] as $k=>$v){
		$xml .= "<$k><![CDATA[$v]]></$k>";
	}
	$xml .= "</ARTICLE_MASTER_DATA>";
	$brand_name="";	$product_variant1=''; $product_name1=''; $product_info_name='';
        unset($brand_result);
        unset($product_result);
        unset($product_info_result);
}
$xml .= "</ARTICLE_MASTER>";

$iRelUploadCnt= $iRelUploadCnt ? $iRelUploadCnt :1;
$config_details = get_config_details();
$strXML = "<XML>";
$strXML .= "<MSG><![CDATA[$msg]]></MSG>";
$strXML .= "<STARTLIMIT><![CDATA[$startlimit]]></STARTLIMIT>";
$strXML .= "<CNT><![CDATA[$limitcnt]]></CNT>";
$strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$category_id]]></SELECTED_CATEGORY_ID>"; 
$strXML .= $config_details;
$strXML .= $xml;
$strXML .= $xmlArt;
$strXML .= $sArticleGroupDetailXML;
$strXML .= $sArticleTypeDetailXML;
$strXML .= $sArticleMediaDataDetXML;
$strXML .= $nodesPaging;
$strXML .= "<WALLCNT><![CDATA[$iRelUploadCnt]]></WALLCNT>";
$strXML .= "<ARTICLETYPE><![CDATA[$type_selecetd]]></ARTICLETYPE>";
$strXML .= "</XML>";

$strXML = mb_convert_encoding($strXML, "UTF-8");
if($_GET['debug']==1) { header('Content-type: text/xml');echo $strXML;exit; }


$doc = new DOMDocument();
$doc->loadXML($strXML);
$doc->saveXML();

$xslt = new xsltProcessor;
$xsl = DOMDocument::load('../xsl/article_ajax_list.xsl');

$xslt->importStylesheet($xsl);
print $xslt->transformToXML($doc);
?>
