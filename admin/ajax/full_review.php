<?php
		require_once('../../include/config.php');
		require_once(CLASSPATH.'DbConn.php');
		require_once(CLASSPATH.'user_review.class.php');
		require_once(CLASSPATH.'brand.class.php');
		require_once(CLASSPATH.'category.class.php');
		require_once(CLASSPATH.'product.class.php');

		$dbconn = new DbConn;
		$userreview = new USERREVIEW;
		$brand = new BrandManagement;
		$category = new CategoryManagement;
		$product = new ProductManagement;

		$category_id = $_REQUEST['catid'] ? $_REQUEST['catid'] : SITE_CATEGORY_ID;		
		$totalquestion = $_REQUEST['totalquestion'];
		$uname = trim($_REQUEST['uname']);
		$brand_id = $_REQUEST['brand_id'];
		$model_id = $_REQUEST['product_info_id'];
		$product_id = $_REQUEST['product_id'];
		$user_review_id = $_REQUEST['user_review_id'];
		$startlmit = $_REQUEST['startlimit'];
		$cnt = $_REQUEST['cnt'];

		unset($result);
		$result = $userreview->arrGetUserReviewDetails($user_review_id,"","","","",$brand_id,$category_id,$model_id,$product_id,"",$startlimit,$cnt);
		//print"<pre>";print_r($result);print"</pre>";die();

		$cnt = sizeof($result);
		
		$xml = "<USER_REVIEW_MASTER>";
		$xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
		for($i=0;$i<$cnt;$i++){
			$brand_id = $result[$i]['brand_id'];
			$model_id = $result[$i]['product_info_id'];
			$product_id = $result[$i]['product_id'];
			$category_id = $result[$i]['category_id'];
			$create_date = $result[$i]['create_date'];
			$result[$i]['create_date'] = date('d/m/Y',strtotime($create_date));
			$user_name = $result[$i]['user_name'];
                        $title = $result[$i]['title'];
                        $location = $result[$i]['location'];
                        if($title != ""){
                                $title = html_entity_decode($title,ENT_QUOTES);
                                $result[$i]['title'] = $title;
                        }
                        if($user_name != ""){
                                $user_name = html_entity_decode($user_name,ENT_QUOTES);
                                $result[$i]['user_name'] = $user_name;
                        }
			if($location != ""){
                                $location = html_entity_decode($location,ENT_QUOTES);
                                $result[$i]['location'] = $user_name;
                        }
			if(!empty($brand_id)){
				unset($brand_result);
				$brand_name="";
				$brand_result = $brand->arrGetBrandDetails($brand_id,$category_id);
				$brand_name = $brand_result[0]['brand_name'];				
				$modelArr[] = $brand_name;
			}
			$result[$i]['brand_name'] = $brand_name;
			if(!empty($model_id)){
				unset($product_result);
				$model_name="";
				$product_result = $product->arrGetProductNameInfo($model_id);
				$model_name = $product_result[0]['product_info_name'];
				$modelArr[] = $model_name;
			}			
			$result[$i]['model_name'] = $model_name;
			$result[$i]['brand_model_name'] = implode(" ",$modelArr);
			if(!empty($product_id)){
				unset($product_result);
				$variant="";
				$product_result = $product->arrGetProductDetails($product_id);
				$variant = $product_result[0]['variant'];
				$modelArr[] = $variant_name;
			}
			$result[$i]['brand_model_variant_name'] = implode(" ",$modelArr);
			unset($modelArr);

			$result[$i]['variant'] = $variant;
			$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
			$xml .= "<USER_REVIEW_MASTER_DATA>";
			foreach($result[$i] as $k=>$v){
				$xml .= "<$k><![CDATA[$v]]></$k>";
			}
			$xml .= "</USER_REVIEW_MASTER_DATA>";

		}
		$xml .= "</USER_REVIEW_MASTER>";
		
		unset($result);
		$result = $userreview->arrGetUserQnA('','',$user_review_id,"1");
		
		$cnt = sizeof($result);
		
		$xml .= "<USER_REVIEW_ANSWER_MASTER>";
		$xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
		for($i=0;$i<$cnt;$i++){
			unset($que_result);

			$que_id = $result[$i]['que_id'];
			$que_result = $userreview->arrGetQuestions($que_id);
			
			$result[$i]['quename'] = $que_result[0]['quename'];
			
			$answer = $result[$i]['answer'];
			$ansArr = explode(",",$answer);
			$gradeCnt = $result[$i]['grade'];
			$html = "";
			for($grade=1;$grade<=5;$grade++){
				if($grade <= $gradeCnt){
					$html .= '<img class="starOn" />';
				}else{
					$html .= '<img class="starOff" />';
				}
			}
			$result[$i]['grade'] = $html;
			
			unset($ans_result);
			$ans_result = $userreview->arrGetQueAnswer("",$que_id);
			
			$anscnt = sizeof($ans_result);		

			$xml .= "<USER_REVIEW_ANSWER_MASTER_DATA>";

			$xml .= "<QUE_ANSWER_MASTER>";
			$xml .= "<COUNT><![CDATA[$anscnt]]></COUNT>";
			
			if($anscnt > 0){
				for($ans=0;$ans<$anscnt;$ans++){
					$ans_id = trim($ans_result[$ans]['ans_id']);					
					$html = "";
					$ansCnt = $ansArr[$ans];
					for($grade=1;$grade<=5;$grade++){
						if($grade <= $ansCnt){
							$html .= '<img class="vsblStr mlr1"/>';
						}else{
							$html .= '<img class="dsblStr mlr1"/>';
						}
					}
					$ans_result[$ans]['selected_answer'] = $html;

					$ans_result[$ans] = array_change_key_case($ans_result[$ans],CASE_UPPER);
					$xml .= "<QUE_ANSWER_MASTER_DATA>";
					foreach($ans_result[$ans] as $key=>$val){
						$xml .= "<$key><![CDATA[$val]]></$key>";
					}
					$xml .= "</QUE_ANSWER_MASTER_DATA>";
				}
			}
			$xml .= "</QUE_ANSWER_MASTER>";

			$result[$i] = array_change_key_case($result[$i],CASE_UPPER);			
			foreach($result[$i] as $k=>$v){
				$xml .= "<$k><![CDATA[$v]]></$k>";
			}
			$xml .= "</USER_REVIEW_ANSWER_MASTER_DATA>";
		}
		$xml .= "</USER_REVIEW_ANSWER_MASTER>";
		
		unset($result);
		$result = $userreview->arrGetUserQnA('','',$user_review_id,"0","1");

		$cnt = sizeof($result);
		$xml .= "<USER_REVIEW_COMMENT_ANSWER_MASTER>";
		$xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
		for($i=0;$i<$cnt;$i++){
			$que_id = $result[$i]['que_id'];
			$que_result = $userreview->arrGetQuestions($que_id);			
			$result[$i]['quename'] = $que_result[0]['quename'];

			$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
			$xml .= "<USER_REVIEW_COMMENT_ANSWER_MASTER_DATA>";
			foreach($result[$i] as $k=>$v){
				$xml .= "<$k><![CDATA[".html_entity_decode($v,ENT_QUOTES)."]]></$k>";
			}
			$xml .= "</USER_REVIEW_COMMENT_ANSWER_MASTER_DATA>";
		}
		$xml .= "</USER_REVIEW_COMMENT_ANSWER_MASTER>";
		
		//used to check admin rating.
		unset($result);
		$result = $userreview->arrGetOverallGrade($category_id,$brand_id,$product_id,$model_id,'',$user_review_id);

		$overallcnt = $result[0]['totaloverallcnt'];
		$overallavg = round($result[0]['overallavg']);
		
		$html = "";
		for($grade=1;$grade<=5;$grade++){
			if($grade <= $overallavg){
				$html .= '<img class="vsblStr mlr1"/>';
			}else{
				$html .= '<img class="dsblStr mlr1"/>';
			}
		}
		$xml .= "<OVERALL_AVG_HTML><![CDATA[$html]]></OVERALL_AVG_HTML>";
		$xml .= "<OVERALL_AVG_CNT><![CDATA[$overallavg]]></OVERALL_AVG_CNT>";
		$xml .= "<OVERALL_TOTAL_CNT><![CDATA[$overallcnt]]></OVERALL_TOTAL_CNT>";
		
		$config_details = get_config_details();

        $strXML .= "<XML>";
        $strXML .= $config_details;
	$strXML .= "<MSG><![CDATA[$msg]]></MSG>";
        $strXML .= "<SEO_URL><![CDATA[$seo_url]]></SEO_URL>";
        $strXML .= "<SEO_WEB_URL><![CDATA[".SEO_WEB_URL."]]></SEO_WEB_URL>";
        $strXML .= "<SEO_TITLE><![CDATA[$seo_title]]></SEO_TITLE>";
        $strXML .= "<SEO_TAGS><![CDATA[$seo_keywords]]></SEO_TAGS>";
        $strXML .= "<SEO_DESC><![CDATA[$seo_desc]]></SEO_DESC>";
        $strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$category_id]]></SELECTED_CATEGORY_ID>";        
	$strXML.= $xml;
        $strXML .= "<USER_REVIEW_ID><![CDATA[$user_review_id]]></USER_REVIEW_ID>";        
	$strXML.= "</XML>";

	$strXML = mb_convert_encoding($strXML, "UTF-8");
	if($_REQUEST['debug']==1) { header('Content-type: text/xml');echo $strXML;exit; }

	$doc = new DOMDocument();
	$doc->loadXML($strXML);
	$doc->saveXML();

	$xslt = new xsltProcessor;
	$xsl = DOMDocument::load('../xsl/user_full_review.xsl');

	$xslt->importStylesheet($xsl);
	print $xslt->transformToXML($doc);
?>
