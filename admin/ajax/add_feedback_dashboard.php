<?php	
require_once('../../include/config.php');
require_once(CLASSPATH.'DbConn.php');
require_once(CLASSPATH.'user_feedback.class.php');

$dbconn = new DbConn;
$oFeedbackuser = new FEEDBACKUSER;

//print "<pre>"; print_r($_REQUEST);
$category_id = $_REQUEST['catid'];
$startlimit = $_REQUEST['startlimit'];
$limitcnt = $_REQUEST['cnt'];
$actiontype = $_REQUEST['actiontype'] ? $_REQUEST['actiontype'] : "Insert";

$feedback_id =$_REQUEST['feedback_id'];

if($_REQUEST['act']=='update' && !empty($feedback_id)){
	$result = $oFeedbackuser->arrGetFeedbackDetails($feedback_id,"","","","","create_date");
	//print "<pre>"; print_r($result);print"</pre>";exit;
	$cnt = sizeof($result);
	$xml .= "<FEEDBACK_DETAIL>";
	$xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
	for($i=0;$i<$cnt;$i++){
		$feedback_id = $result[$i]['feedback_id'];
		$subject = $result[$i]['subject'];
		$res = $oFeedbackuser->arrGetFeedbackSubjectDetails($subject,"","","","");
		$result[$i]['subject']=$res[0]['subject'];
		$status = $result[$i]['status'];
		$result[$i]['status'] = ($status == 1) ? 'Active' : 'InActive';

		$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
		//print "<pre>"; print_r($result[$i]);
		$xml .= "<FEEDBACK_DETAIL_DATA>";
		foreach($result[$i] as $k=>$v){
		$xml .= "<$k><![CDATA[$v]]></$k>";
		}
		$xml .= "</FEEDBACK_DETAIL_DATA>";
	}
	$xml .= "</FEEDBACK_DETAIL>";
}


unset($result);
if(!empty($category_id)){
	$result = $oFeedbackuser->arrGetFeedbackDetails("","","","","","create_date");

}
$cnt = sizeof($result);
//print "<pre>"; print_r($result);
$xml_feedback .= "<FEEDBACK_MASTER>";
$xml_feedback .= "<COUNT><![CDATA[$cnt]]></COUNT>";
for($i=0;$i<$cnt;$i++){
	
	$feedback_id = $result[$i]['feedback_id'];

	$subject = $result[$i]['subject'];
	$res = $oFeedbackuser->arrGetFeedbackSubjectDetails($subject,"","","","");
	//print "<pre>"; print_r($res);
	$sub = $res[0]['subject'];
	$sub = substr($sub, 0, 20);
	$result[$i]['subject']=$sub;

	$feedback = $result[$i]['feedback'];
	$result[$i]['feedback'] = substr($feedback, 0, 20);

	$email_id = $result[$i]['email_id'];
	$status = $result[$i]['status'];
	$result[$i]['status'] = ($status == 1) ? 'Active' : 'InActive';

	$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
	//print "<pre>"; print_r($result[$i]);
	$xml_feedback .= "<FEEDBACK_MASTER_DATA>";
	foreach($result[$i] as $k=>$v){
		$xml_feedback .= "<$k><![CDATA[$v]]></$k>";
	}
	$xml_feedback .= "</FEEDBACK_MASTER_DATA>";
}
$xml_feedback .= "</FEEDBACK_MASTER>";



$iRelUploadCnt= $iRelUploadCnt ? $iRelUploadCnt :1;
$config_details = get_config_details();

$strXML = "<XML>";
$strXML .= "<MSG><![CDATA[$msg]]></MSG>";
$strXML .= "<STARTLIMIT><![CDATA[$startlimit]]></STARTLIMIT>";
$strXML .= "<CNT><![CDATA[$limitcnt]]></CNT>";
$strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$category_id]]></SELECTED_CATEGORY_ID>"; 
$strXML .= $config_details;
$strXML .= $xml;
$strXML .= $xml_feedback;
$strXML .= "<WALLCNT><![CDATA[$iRelUploadCnt]]></WALLCNT>";
$strXML .= "<SELECTED_TYPE><![CDATA[$type_selecetd]]></SELECTED_TYPE>";
$strXML .= "<SELECTED_ACTION_TYPE><![CDATA[$actiontype]]></SELECTED_ACTION_TYPE>";
$strXML .= "<FEEDBACK_ID><![CDATA[$feedback_id]]></FEEDBACK_ID>";
$strXML .= "</XML>";

$strXML = mb_convert_encoding($strXML, "UTF-8");
if($_GET['debug']==1) { header('Content-type: text/xml');echo $strXML;exit; }


$doc = new DOMDocument();
$doc->loadXML($strXML);
$doc->saveXML();

$xslt = new xsltProcessor;
$xsl = DOMDocument::load('../xsl/add_feedback_dashboard.xsl');

$xslt->importStylesheet($xsl);
print $xslt->transformToXML($doc);
?>
