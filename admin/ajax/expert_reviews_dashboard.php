<?php	
require_once('../../include/config.php');
require_once(CLASSPATH.'DbConn.php');
require_once(CLASSPATH.'category.class.php');
require_once(CLASSPATH.'product.class.php');
require_once(CLASSPATH.'brand.class.php');
require_once(CLASSPATH.'reviews.class.php');

$dbconn = new DbConn;
$category = new CategoryManagement;
$product = new ProductManagement;
$brand = new BrandManagement;
$oReviews = new reviews;

$category_id = $_REQUEST['catid'];
$startlimit = $_REQUEST['startlimit'];
$limitcnt = $_REQUEST['cnt'];
$r_product_id = $_REQUEST['pid'];
$r_brand_id = $_REQUEST['bid'];

$review_id =$_REQUEST['rid'] ? $_REQUEST['rid']  : $_REQUEST['review_id'];
$type_selecetd=$_REQUEST['article_type_id'] ? $_REQUEST['article_type_id'] :0;

if(!empty($category_id)){
	$aReviewsGroupDetail = $oReviews->arrGetReviewsGroupDetails("","",$category_ids="","1");
	if(is_array($aReviewsGroupDetail)){
		foreach($aReviewsGroupDetail as $igKey=>$aGrpValue){
			$aReviewsGroupData[$aGrpValue['group_id']]=$aGrpValue['group_name'];
		}
	}
	$sReviewsGroupDetail=arraytoxml($aReviewsGroupDetail,"REVIEW_GROUP_DATA");
	$sReviewsGroupDetailXML ="<REVIEW_GROUP_MASTER>".$sReviewsGroupDetail."</REVIEW_GROUP_MASTER>";

	$aReviewsTypeDetail = $oReviews->arrGetReviewsTypeDetails("","",$category_id,"1");
	if(is_array($aReviewsTypeDetail)){
		foreach($aReviewsTypeDetail as $itKey=>$aTypeValue){
			$aReviewsTypeData[$aTypeValue['article_type_id']]=$aTypeValue['type_name'];
		}
	}
	$sReviewsTypeDetail=arraytoxml($aReviewsTypeDetail,"REVIEW_TYPE_DATA");
	$sReviewsTypeDetailXML ="<REVIEW_TYPE_MASTER>".$sReviewsTypeDetail."</REVIEW_TYPE_MASTER>";
}

if(!empty($category_id)){
	$aParameters=array('category_id'=>$category_id);
	$result = $oReviews->getExpertReviewsDetails("","",$type_selecetd,"","",$category_id,"","",$startlimit,$cnt);
}

if($_REQUEST['act']=='Delete' && !empty($review_id)){
	$dresult = $oReviews->booldeleteExpertReviews($review_id,'EXPERT_REVIEWS');
}
if($_REQUEST['act']=='update' && !empty($review_id)){
	$rResult = $oReviews->getExpertReviewsDetails($review_id,"","","","",$category_id,"","",$startlimit,$cnt);
	$xmlArt='';
	//print "<pre>"; print_r($rResult);
	$cnt = sizeof($rResult);
	$status = $rResult[0]['status'];
	$categoryid = $rResult[0]['category_id'];
	if(!empty($categoryid)){
		$category_result = $category->arrGetCategoryDetails($categoryid);
	}
	$category_name = $category_result[0]['category_name'];
	$rResult[0]['js_category_name'] = $category_name;
	$rResult[0]['category_name'] = html_entity_decode($category_name,ENT_QUOTES);
	
	$brand_id = $rResult[0]['brand_id'];
	if(!empty($brand_id)){
		$brand_result = $brand->arrGetBrandDetails($brand_id);
		$brand_name = $brand_result[0]['brand_name'];
	}
	$rResult[0]['js_brand_name'] = $brand_name;
	$rResult[0]['brand_name'] = $brand_name ? html_entity_decode($brand_name,ENT_QUOTES) : 'Nil';
	$product_id = $rResult[0]['product_id'];
	if(!empty($product_id)){
		$product_result =$product->arrGetProductDetails($product_id,$category_id,"","","","","",$startlimit,$limitcnt);
		$product_name = $product_result[0]['product_name'];
	}
	$rResult[0]['js_product_name'] =$product_name;
	$rResult[0]['product_name'] = $product_name ? html_entity_decode($product_name,ENT_QUOTES) : 'Nil';
	$product_review_id = $rResult[0]['product_review_id'];
	if(!empty($product_review_id)){
		$review_media_result = $oReviews->arrGetUploadMediaReviewsDetails($product_review_id);
		$sReviewsMediaDataDet=arraytoxml($review_media_result,"MEDIA_UPLOAD_DATA");
		$sReviewsMediaDataDetXML ="<MEDIA_UPLOAD_DETAIL>".$sReviewsMediaDataDet."</MEDIA_UPLOAD_DETAIL>";
	}
	$rResult[0]['js_title'] = $rResult[0]['title'];
	$rResult[0]['title'] = $rResult[0]['title'] ? html_entity_decode($rResult[0]['title'],ENT_QUOTES) : 'Nil';
	$rResult[0]['js_abstract'] = $rResult[0]['abstract'];
	$rResult[0]['abstract'] = $rResult[0]['abstract'] ? html_entity_decode($rResult[0]['abstract'],ENT_QUOTES) : 'Nil';
	$rResult[0]['js_content'] = $rResult[0]['content'];
	$rResult[0]['content'] = $rResult[0]['content'] ? html_entity_decode($rResult[0]['content'],ENT_QUOTES) : 'Nil';
	$rResult[0]['js_tags'] = $rResult[0]['tags'];
	$rResult[0]['tags'] = $rResult[0]['tags'] ? html_entity_decode($rResult[0]['tags'],ENT_QUOTES) : 'Nil';
	$rResult[0]['js_image_path'] = $rResult[0]['image_path'];
	$rResult[0]['image_path'] = $rResult[0]['image_path'];
	$rResult[0]['js_video_path'] = $rResult[0]['video_path'];
	$rResult[0]['video_path'] = $rResult[0]['video_path'];
	if($rResult[0]['video_path']!=''){
		$sMainImagePath=getImageDetails($rResult[0]['video_path'],SERVICEID,$action='api');
		//$sMainImage = $sMainImagePath['main_image'];
		$rResult[0]['video_path_title'] = $sMainImagePath['title'];
	}
	if($rResult[0]['image_path']!=''){
		$sMainThmImagePath=getImageDetails($rResult[0]['image_path'],SERVICEID,$action='api');
		//$sMainThmImage = $sMainThmImagePath['main_image'];
		$rResult[0]['image_path_title'] = $sMainThmImagePath['title'];
	}
	$rResult[0]['review_status'] = ($status == 1) ? 'Active' : 'InActive';
	$rResult[0]['create_date'] = date('d-m-Y',strtotime($rResult[0]['create_date']));
	$rResult[0] = array_change_key_case($rResult[0],CASE_UPPER);
	
	$xmlArt .= "<REVIEW_DATA>";
	foreach($rResult[0] as $k1=>$v1){
		$xmlArt .= "<$k1><![CDATA[$v1]]></$k1>";
	}
	$xmlArt .= "</REVIEW_DATA>";
}


$cnt = sizeof($result);
$xml = "<REVIEW_MASTER>";
$xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";	
for($i=0;$i<$cnt;$i++){
	$status = $result[$i]['status'];
	$categoryid = $result[$i]['category_id'];
	if(!empty($categoryid)){
		$category_result = $category->arrGetCategoryDetails($categoryid);
	}
	$brand_id = $result[$i]['brand_id'];
	if(!empty($brand_id)){
		$brand_result = $brand->arrGetBrandDetails($brand_id);
		$brand_name = $brand_result[0]['brand_name'];
	}
	$result[$i]['js_brand_name'] = $brand_name;
	$result[$i]['brand_name'] = $brand_name ? html_entity_decode($brand_name,ENT_QUOTES) : 'Nil';
	$brand_name="";
	$product_info_id1 = $result[$i]['product_info_id'];
	if(!empty($product_info_id1) && $product_info_id1!=0){
		$product_info_result = $product->arrGetProductNameInfo($product_info_id1,$category_id,"","","1");
		$product_info_name = $product_info_result[0]['product_info_name'];
		$result[$i]['product_name'] = $product_info_name ? html_entity_decode($product_info_name,ENT_QUOTES) : 'Nil';
	}
	$product_info_name='';
	$product_id1 = $result[$i]['product_id'];
	if(!empty($product_id1) && $product_id1!=0){
		$product_result =$product->arrGetProductDetails($product_id1,$category_id,"","1","","","",$startlimit,$limitcnt);
		$product_name1 = $product_result[0]['product_name'];
		$product_variant1 = $product_result[0]['variant'];
		$result[$i]['js_product_name'] =$product_name1;
		$result[$i]['product_name'] = $product_name1 ? html_entity_decode($product_name1,ENT_QUOTES) : 'Nil';
		$result[$i]['js_variant'] =$product_variant1;
		$result[$i]['variant'] = $product_variant1 ? html_entity_decode($product_variant1,ENT_QUOTES) : 'Nil';
	}
	$product_variant1='';
	$result[$i]['js_title'] = $result[$i]['title'];
	$result[$i]['title'] = $result[$i]['title'] ? html_entity_decode($result[$i]['title'],ENT_QUOTES) : 'Nil';
	$result[$i]['js_abstract'] = $result[$i]['abstract'];
	$result[$i]['abstract'] = $result[$i]['abstract'] ? html_entity_decode($result[$i]['abstract'],ENT_QUOTES) : 'Nil';
	$result[$i]['js_title'] = $result[$i]['title'];
	$result[$i]['title'] = $result[$i]['title'] ? html_entity_decode($result[$i]['title'],ENT_QUOTES) : 'Nil';
	$result[$i]['review_status'] = ($status == 1) ? 'Active' : 'InActive';
	$category_name = $category_result[0]['category_name'];
	$result[$i]['js_category_name'] = $category_name;
	$result[$i]['category_name'] = html_entity_decode($category_name,ENT_QUOTES);
	$result[$i]['create_date'] = date('d-m-Y',strtotime($result[$i]['create_date']));
	
	if(is_array($aReviewsTypeData) && isset($aReviewsTypeData[$result[$i]['review_type']])){
		$result[$i]['review_type_name'] =$aReviewsTypeData[$result[$i]['review_type']];	
	}
	if(is_array($aReviewsGroupData) && isset($aReviewsGroupData[$result[$i]['group_id']])){
		$result[$i]['review_group_name'] =$aReviewsGroupData[$result[$i]['group_id']];	
	}
	$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
	$xml .= "<REVIEW_MASTER_DATA>";
	foreach($result[$i] as $k=>$v){
		$xml .= "<$k><![CDATA[$v]]></$k>";
	}
	$xml .= "</REVIEW_MASTER_DATA>";
	$brand_name="";$product_name1="";$product_info_name="";$product_variant1="";
        unset($brand_result);
        unset($product_result);
        unset($product_info_result);
}
$xml .= "</REVIEW_MASTER>";

if(!empty($category_id)){
	$result = $brand->arrGetBrandDetails("",$category_id);
}	
$cnt = sizeof($result);
$xml .= "<BRAND_MASTER>";
$xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
for($i=0;$i<$cnt;$i++){
	$status = $result[$i]['status'];
	$categoryid = $result[$i]['category_id'];
	if(!empty($categoryid)){
		$category_result = $category->$result = $category->arrGetCategoryDetails($categoryid);
	}
	$category_name = $category_result[0]['category_name'];
	$result[$i]['js_category_name'] = $category_name;
	$result[$i]['category_name'] = html_entity_decode($category_name,ENT_QUOTES);
	$result[$i]['brand_status'] = ($status == 1) ? 'Active' : 'InActive';
	$result[$i]['create_date'] = date('d-m-Y',strtotime($result[$i]['create_date']));
	$result[$i]['js_brand_name'] = $result[$i]['brand_name'];
	$result[$i]['brand_name'] = html_entity_decode($result[$i]['brand_name'],ENT_QUOTES);
	$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
	$xml .= "<BRAND_MASTER_DATA>";
	foreach($result[$i] as $k=>$v){
		$xml .= "<$k><![CDATA[$v]]></$k>";
	}
	$xml .= "</BRAND_MASTER_DATA>";
}
$xml .= "</BRAND_MASTER>";

$iRelUploadCnt= $iRelUploadCnt ? $iRelUploadCnt :1;

$config_details = get_config_details();

$strXML = "<XML>";
$strXML .= "<MSG><![CDATA[$msg]]></MSG>";
$strXML .= "<STARTLIMIT><![CDATA[$startlimit]]></STARTLIMIT>";
$strXML .= "<CNT><![CDATA[$limitcnt]]></CNT>";
$strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$category_id]]></SELECTED_CATEGORY_ID>"; 
$strXML .= $config_details;
$strXML .= $xml;
$strXML .= $xmlArt;
$strXML .= $sReviewsGroupDetailXML;
$strXML .= $sReviewsTypeDetailXML;
$strXML .= $sReviewsMediaDataDetXML;
$strXML .= "<WALLCNT><![CDATA[$iRelUploadCnt]]></WALLCNT>";
$strXML .= "<REVIEWTYPE><![CDATA[$type_selecetd]]></REVIEWTYPE>";
$strXML .= "</XML>";

$strXML = mb_convert_encoding(trim($strXML), "UTF-8");
if($_GET['debug']==1) { header('Content-type: text/xml');echo $strXML;exit; }

$doc = new DOMDocument();
$doc->loadXML($strXML);
$doc->saveXML();

$xslt = new xsltProcessor;
$xsl = DOMDocument::load('../xsl/expert_reviews_dashboard.xsl');

$xslt->importStylesheet($xsl);
print $xslt->transformToXML($doc);
?>
