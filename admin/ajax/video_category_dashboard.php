<?php	
require_once('../../include/config.php');
require_once(CLASSPATH.'DbConn.php');
require_once(CLASSPATH.'category.class.php');
require_once(CLASSPATH.'videos.class.php');
require_once(CLASSPATH.'pager.class.php');

$dbconn = new DbConn;
$category = new CategoryManagement;
$videoGallery = new videos();
$oPager = new Pager;

//print"<pre>";print_r($_REQUEST);print"</pre>";

$video_id = $_REQUEST['vid'];
$category_id = $_REQUEST['catid'];
$startlimit = $_REQUEST['startlimit'];
$limitcnt = $_REQUEST['cnt'];
$category_id = $category_id ? $category_id : SITE_CATEGORY_ID;

$xml="";

if($_REQUEST['act']=='update'){
	unset($result);
	$tab_id = $_REQUEST['id'];
	$result = $videoGallery->getTabDetails($tab_id,$category_id,"","","","","","");
	$cnt = sizeof($result);
	$xml .= "<VIDEO_TAB_DETAIL>";
	$xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
	//print"<pre>";print_r($result);print"</pre>";
	$tab_id = $result[0]['tab_d'];
        $tab_name = $result[0]['tab_name'];
	if(!empty($tab_name)){
		$tab_name = html_entity_decode($tab_name,ENT_QUOTES,'UTF-8');
	}
	$result[0]['tab_name'] = $tab_name;
        $order_tab = $result[0]['order_tab'];
	$status = $result[0]['status'];
        $result[0]['status'] = ($status == 1) ? 'Active' : 'InActive';
	
	$result[0] = array_change_key_case($result[0],CASE_UPPER);	
	$xml .= "<VIDEO_TAB_DETAIL_DATA>";
	foreach($result[0] as $k=>$v){
                $xml .= "<$k><![CDATA[$v]]></$k>";
        }
        $xml .= "</VIDEO_TAB_DETAIL_DATA>";

	$xml .= "</VIDEO_TAB_DETAIL>";

}

unset($result);
if(!empty($category_id)){
	$result = $videoGallery->getTabDetails("",$category_id,"","","","","","");
}
//print"<pre>";print_r($result);print"</pre>";
$cnt = sizeof($result);
$order_cnt=0;
if(!$cnt){$cnt = 0;}
$order_cnt=$cnt+1;
$xml .= "<VIDEO_TAB_MASTER>";
for($ocnt=1;$ocnt<=$order_cnt;$ocnt++){
        $xml .= "<ORDERING>";
        $xml .= "<ORDERING_DATA>".$ocnt."</ORDERING_DATA>";
        $xml .= "</ORDERING>";
}
$xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
for($i=0;$i<$cnt;$i++){
        $result[$i]['tab_id'] = $result[$i]['tab_id'];
        $result[$i]['category_id'] = $result[$i]['category_id'];
	$tab_name = $result[$i]['tab_name'];
        if(!empty($tab_name)){
                $tab_name = html_entity_decode($tab_name,ENT_QUOTES,'UTF-8');
        }
        $result[$i]['tab_name'] = $tab_name;
        $result[$i]['order_tab'] = $result[$i]['order_tab'];
        $status = $result[$i]['status']; 
        $result[$i]['status'] = ($status == 1) ? 'Active' : 'InActive';
        $result[$i]['create_date'] = date('d-m-Y',strtotime($result[$i]['create_date']));
        $result[$i]['update_date'] = date('d-m-Y',strtotime($result[$i]['update_date']));

        $result[$i] = array_change_key_case($result[$i],CASE_UPPER);
        //print "<pre>"; print_r($result[$i]);
        $xml .= "<VIDEO_TAB_MASTER_DATA>";
        foreach($result[$i] as $k=>$v){
                $xml .= "<$k><![CDATA[$v]]></$k>";
        }
        $xml .= "</VIDEO_TAB_MASTER_DATA>";
}
$xml .= "</VIDEO_TAB_MASTER>";

$config_details = get_config_details();

$strXML = "<XML>";
$strXML .= "<MSG><![CDATA[$msg]]></MSG>";
$strXML .= "<STARTLIMIT><![CDATA[$startlimit]]></STARTLIMIT>";
$strXML .= "<CNT><![CDATA[$limitcnt]]></CNT>";
$strXML .= "<VIEW_SECTION_ID><![CDATA[$view_section_id]]></VIEW_SECTION_ID>";
$strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$category_id]]></SELECTED_CATEGORY_ID>"; 
$strXML .= $config_details;
$strXML .= $xml;
$strXML .= $nodesPaging;
$strXML .= "<WALLCNT><![CDATA[$iRelUploadCnt]]></WALLCNT>";
$strXML .= "<VIEWSECTION><![CDATA[$view_section_id]]></VIEWSECTION>";
$strXML .= "<ARTICLETYPE><![CDATA[$type_selected]]></ARTICLETYPE>";
$strXML .= "</XML>";

//$strXML = mb_convert_encoding($strXML, "UTF-8");
if($_GET['debug']==1) { header('Content-type: text/xml');echo $strXML;exit; }

$doc = new DOMDocument();
$doc->loadXML($strXML);
$doc->saveXML();

$xslt = new xsltProcessor;
$xsl = DOMDocument::load('../xsl/video_category_dashboard.xsl');

$xslt->importStylesheet($xsl);
print $xslt->transformToXML($doc);
?>
