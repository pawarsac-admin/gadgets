<?php	
	error_reporting(E_ALL);
	require_once('../../include/config.php');
	require_once(CLASSPATH.'DbConn.php');
	require_once(CLASSPATH.'pivot.class.php');
	require_once(CLASSPATH.'feature.class.php');
	require_once(CLASSPATH.'category.class.php');
	require_once(CLASSPATH.'product.class.php');
	require_once(CLASSPATH.'brand.class.php');
	require_once(CLASSPATH.'citystate.class.php');
	
	$dbconn = new DbConn;
	$pivot = new PivotManagement;
	$feature = new FeatureManagement;
	$category = new CategoryManagement;
	$product = new ProductManagement;
	$brand = new BrandManagement;
	$citystate = new citystate;

	$category_id = $_REQUEST['catid'];
	$product_id = $_REQUEST['product_id'];
	$brand_id = $_REQUEST['brand_id'];
	$startlimit = $_REQUEST['startlimit'];
	$limitcnt = $_REQUEST['cnt'];
	
	if(!empty($category_id)){
		$result = $product->arrGetProductDetails($product_id,$category_id,$brand_id,"",$startlimit,$limitcnt);

	}

	//print_r($result);exit;

	$cnt = sizeof($result);
	$xml = "<PRODUCT_MASTER>";
	$xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";	
	for($i=0;$i<$cnt;$i++){
		$status = $result[$i]['status'];
		$categoryid = $result[$i]['category_id'];
		if(!empty($categoryid)){
			$category_result = $category->arrGetCategoryDetails($categoryid);
		}
		$brand_id = $result[$i]['brand_id'];
		if(!empty($brand_id)){
			$brand_result = $brand->arrGetBrandDetails($brand_id);
			$brand_name = $brand_result[0]['brand_name'];
		}
		$result[$i]['js_brand_name'] = $brand_name;
		$result[$i]['brand_name'] = $brand_name ? html_entity_decode($brand_name,ENT_QUOTES) : 'Nil';

		$result[$i]['js_product_name'] = $result[$i]['product_name'];
		$result[$i]['product_name'] = $result[$i]['product_name'] ? html_entity_decode($result[$i]['product_name'],ENT_QUOTES) : 'Nil';
	
		$result[$i]['js_product_mrp'] = $result[$i]['product_mrp'];	
		$result[$i]['product_mrp'] = $result[$i]['product_mrp'] ? html_entity_decode($result[$i]['product_mrp'],ENT_QUOTES) : 'Nil';

		$result[$i]['is_product_upcoming'] = $result[$i]['is_upcoming'] ? 'Yes' : 'No';
		$result[$i]['is_product_latest'] = $result[$i]['is_latest'] ? 'Yes' : 'No';
		
		$result[$i]['product_status'] = ($status == 1) ? 'Active' : 'InActive';
		
		$category_name = $category_result[0]['category_name'];
		$result[$i]['js_category_name'] = $category_name;
		$result[$i]['category_name'] = html_entity_decode($category_name,ENT_QUOTES);
		$result[$i]['create_date'] = date('d-m-Y',strtotime($result[$i]['create_date']));
		$result[$i]['js_feature_name'] = $result[$i]['feature_name'];

		$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
		$xml .= "<PRODUCT_MASTER_DATA>";
		foreach($result[$i] as $k=>$v){
			$xml .= "<$k><![CDATA[$v]]></$k>";
		}
		$xml .= "</PRODUCT_MASTER_DATA>";
	}
	$xml .= "</PRODUCT_MASTER>";

	if(!empty($category_id)){
		$result = $brand->arrGetBrandDetails("",$category_id);
	}	
	$cnt = sizeof($result);
        $xml .= "<BRAND_MASTER>";
        $xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
        for($i=0;$i<$cnt;$i++){
                $status = $result[$i]['status'];
                $categoryid = $result[$i]['category_id'];
                if(!empty($categoryid)){
                        $category_result = $category->$result = $category->arrGetCategoryDetails($categoryid);
                }
                $category_name = $category_result[0]['category_name'];
                $result[$i]['js_category_name'] = $category_name;
                $result[$i]['category_name'] = html_entity_decode($category_name,ENT_QUOTES);
                $result[$i]['brand_status'] = ($status == 1) ? 'Active' : 'InActive';
                $result[$i]['create_date'] = date('d-m-Y',strtotime($result[$i]['create_date']));
                $result[$i]['js_brand_name'] = $result[$i]['brand_name'];
                $result[$i]['brand_name'] = html_entity_decode($result[$i]['brand_name'],ENT_QUOTES);
                $result[$i] = array_change_key_case($result[$i],CASE_UPPER);
                $xml .= "<BRAND_MASTER_DATA>";
                foreach($result[$i] as $k=>$v){
                        $xml .= "<$k><![CDATA[$v]]></$k>";
                }
                $xml .= "</BRAND_MASTER_DATA>";
        }
        $xml .= "</BRAND_MASTER>";
	if(!empty($category_id)){
		$result = $feature->arrGetFeatureDetails("",$category_id,"",$startlimit,$limitcnt);
	}
	//print_r($result);exit;
	$cnt = sizeof($result);
	$xml .= "<FEATURE_MASTER>";
	$xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";	
	for($i=0;$i<$cnt;$i++){
		$status = $result[$i]['status'];
		$categoryid = $result[$i]['category_id'];
		$feature_id = $result[$i]['feature_id'];
		if(!empty($categoryid)){
			$category_result = $category->arrGetCategoryDetails($categoryid);
		}
		$unit_id = $result[$i]['unit_id'];
		if(!empty($unit_id)){
			$unit_result = $feature->arrFeatureUnitDetails($unit_id,$categoryid);
			$feature_unit = $unit_result[0]['unit_name'];
		}
		if(!empty($feature_id)){
			$pivot_result = $pivot->arrGetPivotDetails("",$categoryid,$feature_id,"");
			echo $product_id." & ".$feature_id."<br/>";
			$product_result = $product->arrGetProductFeatureDetails("",$feature_id,$product_id);
			echo "<br/><pre>";
			print_r($product_result);
			echo "<pre>";
			
		}
		$result[$i]['product_feature_id'] = $product_result[0]['feature_id'];
		$result[$i]['feature_value'] = $product_result[0]['feature_value'];
		$result[$i]['pivot_feature_id'] = $pivot_result[0]['feature_id'];
		$result[$i]['js_feature_name'] = $result[$i]['feature_name'];
		$result[$i]['js_feature_group'] = $result[$i]['feature_group'];
		$result[$i]['js_feature_desc'] = $result[$i]['feature_description'];
		$result[$i]['js_feature_unit'] = $feature_unit;
		$result[$i]['feature_status'] = ($status == 1) ? 'Active' : 'InActive';
		$result[$i]['feature_unit'] = $feature_unit ? html_entity_decode($feature_unit,ENT_QUOTES) : 'Nil';
		$result[$i]['feature_group'] = $result[$i]['feature_group'] ? html_entity_decode($result[$i]['feature_group'],ENT_QUOTES) : 'Nil';
		$result[$i]['feature_desc'] = $result[$i]['feature_desc'] ? html_entity_decode($result[$i]['feature_desc'],ENT_QUOTES) : 'Nil';
		$category_name = $category_result[0]['category_name'];
		$result[$i]['js_category_name'] = $category_name;
		$result[$i]['category_name'] = html_entity_decode($category_name,ENT_QUOTES);
		$result[$i]['create_date'] = date('d-m-Y',strtotime($result[$i]['create_date']));
		$result[$i]['js_feature_name'] = $result[$i]['feature_name'];
		$result[$i]['feature_name'] = $result[$i]['feature_name'] ? html_entity_decode($result[$i]['feature_name'],ENT_QUOTES) : 'Nil';
		$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
		$xml .= "<FEATURE_MASTER_DATA>";
		foreach($result[$i] as $k=>$v){
			$xml .= "<$k><![CDATA[$v]]></$k>";
		}
		$xml .= "</FEATURE_MASTER_DATA>";
	}
	$xml .= "</FEATURE_MASTER>";	

	$result = $citystate->arrGetStateDetails();
	$cnt = sizeof($result);
        $xml .= "<STATE_MASTER>";
	for($i=0;$i<$cnt;$i++){
		$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
                $xml .= "<STATE_MASTER_DATA>";
                foreach($result[$i] as $k=>$v){
                        $xml .= "<$k><![CDATA[$v]]></$k>";
                }
                $xml .= "</STATE_MASTER_DATA>";
        }
        $xml .= "</STATE_MASTER>";
	
	$result = $citystate->arrGetCityDetails($state_id);
	$cnt = sizeof($result);
	    $xml .= "<CITY_MASTER>";
	for($i=0;$i<$cnt;$i++){
		$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
                $xml .= "<CITY_MASTER_DATA>";
                foreach($result[$i] as $k=>$v){
                        $xml .= "<$k><![CDATA[$v]]></$k>";
                }
                $xml .= "</CITY_MASTER_DATA>";
        }
        $xml .= "</CITY_MASTER>";
	$config_details = get_config_details();
	$strXML = "<XML>";
	$strXML .= "<MSG><![CDATA[$msg]]></MSG>";
	$strXML .= "<STARTLIMIT><![CDATA[$startlimit]]></STARTLIMIT>";
	$strXML .= "<CNT><![CDATA[$limitcnt]]></CNT>";
	$strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$category_id]]></SELECTED_CATEGORY_ID>"; 
	$strXML .= $config_details;
	$strXML .= $xml;
	$strXML .= "</XML>";
	//header('Content-type: text/xml');echo $strXML;exit;
	$doc = new DOMDocument();
	$doc->loadXML($strXML);
	$doc->saveXML();
	$xslt = new xsltProcessor;
	$xsl = DOMDocument::load('../xsl/product_update_ajax.xsl');
	$xslt->importStylesheet($xsl);
	print $xslt->transformToXML($doc);
?>
