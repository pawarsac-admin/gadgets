<?php	
require_once('../../include/config.php');
require_once(CLASSPATH.'DbConn.php');
require_once(CLASSPATH.'pivot.class.php');
require_once(CLASSPATH.'feature.class.php');
require_once(CLASSPATH.'category.class.php');
require_once(CLASSPATH.'product.class.php');
require_once(CLASSPATH.'brand.class.php');
require_once(CLASSPATH.'pager.class.php');
require_once(CLASSPATH.'price.class.php');

$dbconn = new DbConn;
$category = new CategoryManagement;
$product = new ProductManagement;
$brand = new BrandManagement;
$oPager = new Pager;
$price = new price;

//print "<pre>"; print_r($_REQUEST);

$category_id = $_REQUEST['catid'];
$startlimit = $_REQUEST['startlimit'];
$limitcnt = $_REQUEST['cnt'];
$iCmptId=$_REQUEST['oncars_compare_id'];
$actiontype = $_REQUEST['actiontype'];

if(!empty($category_id)){
	$result = $product->arrGetOncarsCompareSetDetails("",$category_id);
	$resultCount=count($result);

	if($resultCount!=0){
		$page = $_REQUEST['page'] ? $_REQUEST['page'] : 1;
		$perpage=20;
		$start  = $oPager->findStart($perpage);
		$recordcount=$resultCount;
		$sExtraParam="ajax/oncars_comparison_dashboard.php,sMostpopularDiv,$category_id";
		$jsparams=$start.",".$perpage.",".$sExtraParam;
		$pages= $oPager->findPages($recordcount,$perpage);
		if($pages > 1 ){
			$pagelist= $oPager->jsPageNumNextPrev($page,$pages,"sMostpopularPagination",$jsparams,"text");
			$nodesPaging .= "<Pages><![CDATA[".$pagelist."]]></Pages>";
			$nodesPaging .= "<Page><![CDATA[".$page."]]></Page>";
			$nodesPaging .= "<Perpage><![CDATA[".$perpage."]]></Perpage>";
		}
	}
	//print "<pre>"; print_r($result);
}
$arr_cnt="0";
if($_REQUEST['act']=='update' && !empty($iCmptId)){
	$rResult = $product->arrGetOncarsCompareSetDetails($iCmptId,$category_id,"","","");
	//print "<pre>"; print_r($rResult);
	$xmlArt='';
	$cnt = sizeof($rResult);
	$xmlArt .= "<ONCARS_COMPARE_SET>";
	$status = $rResult[0]['status'];

	$title = $rResult[$i]["title"];
        if(!empty($title)){
                $title = html_entity_decode($title,ENT_QUOTES,'UTF-8');
                $title = removeSlashes($title);
        }
        $rResult[$i]["title"] = $title;

        $description = $rResult[$i]["description"];
        if(!empty($description)){
                $description = html_entity_decode($description,ENT_QUOTES,'UTF-8');
                $description = removeSlashes($description);
        }
        $rResult[$i]["description"] = $description;
	
	$categoryid = $rResult[0]['category_id'];
	if(!empty($categoryid)){
		$category_result = $category->arrGetCategoryDetails($categoryid);
	}
	$product_ids = $rResult[0]['compare_set'];	
	if(!empty($product_ids)){
		$compare_set_arr = explode(",",$product_ids);
		$arr_cnt = sizeof($compare_set_arr);
        	$xmlArt .= "<COUNT><![CDATA[$arr_cnt]]></COUNT>";
		for($i=0;$i<$arr_cnt;$i++){
			$xmlArt .= "<ONCARS_COMPARE_SET_NAMES>";
			$product_id = $compare_set_arr[$i];
			unset($product_result);
			$product_result =$product->arrGetProductDetails($product_id,$category_id);
			$brand_id = $product_result[0]["brand_id"];
			$product_name = $product_result[0]['product_name'];
			$prod_info_result = $product->arrGetProductNameInfo("",$category_id,"",$product_name);
			$model_id = $prod_info_result[0]['product_name_id'];
			$xmlArt .= "<SELECTED_BRAND_ID><![CDATA[$brand_id]]></SELECTED_BRAND_ID>";
			$xmlArt .= "<SELECTED_MODEL_ID><![CDATA[$model_id]]></SELECTED_MODEL_ID>";
			$xmlArt .= "<SELECTED_VARIANT_ID><![CDATA[$product_id]]></SELECTED_VARIANT_ID>";

			unset($result);
			$result = $brand->arrGetBrandDetails("",$category_id);
			$brand_cnt = sizeof($result);
			$xmlArt .= "<ONCARS_BRAND_MASTER>";
			for($n=0;$n<$brand_cnt;$n++){
		        	$result[$n] = array_change_key_case($result[$n],CASE_UPPER);
			        $xmlArt .= "<ONCARS_BRAND_MASTER_DATA>";
			        foreach($result[$n] as $k=>$v){
			                $xmlArt .= "<$k><![CDATA[$v]]></$k>";
			        }
			        $xmlArt .= "</ONCARS_BRAND_MASTER_DATA>";
			}
			$xmlArt .= "</ONCARS_BRAND_MASTER>";
			
			$prod_info = $product->arrGetProductNameInfo("",$category_id,$brand_id);
			$model_count = sizeof($prod_info);
			$xmlArt .= "<ONCARS_MODEL_MASTER>";
			for($j=0;$j<$model_count;$j++){
				$xmlArt .= "<ONCARS_MODEL_MASTER_DATA>";
				$prod_info[$j] = array_change_key_case($prod_info[$j],CASE_UPPER);
			        foreach($prod_info[$j] as $k1=>$v1){
                			$xmlArt .= "<$k1><![CDATA[$v1]]></$k1>";
        			}
			        $xmlArt .= "</ONCARS_MODEL_MASTER_DATA>";
			}
			$xmlArt .= "</ONCARS_MODEL_MASTER>";
			$aProductDetail=$product->arrGetProductByName($product_name);
			$variant_cnt = sizeof($aProductDetail);
			$xmlArt .= "<ONCARS_VARIANT_MASTER>";
			for($v=0;$v<$variant_cnt;$v++){
				$productid = $aProductDetail[$v]["product_id"];
				$sName="";
				$prod_price_res = $price->arrGetPriceDetails("",$productid,$category_id,"","","","","","","1");
				if((sizeof($prod_price_res)) > 0){
                                	$xmlArt .= "<ONCARS_VARIANT_MASTER_DATA>";
					$sName=$aProductDetail[$v]['variant'];
					$xmlArt .= "<PRODUCT_ID>".$productid."</PRODUCT_ID>";
					$xmlArt .= "<VARIANT>".$sName."</VARIANT>";
                                	$xmlArt .= "</ONCARS_VARIANT_MASTER_DATA>";
				}
                        }
			$xmlArt .= "</ONCARS_VARIANT_MASTER>";
			$xmlArt .= "</ONCARS_COMPARE_SET_NAMES>";
		}
	}
	$rResult[0]['js_compare_set_img'] = $rResult[0]['compare_set_img_path'];
	$rResult[0]['compare_set_img'] = $rResult[0]['compare_set_img_path'];
	$rResult[0]['compare_set_img_title'] = $rResult[0]['compare_set_img_path'];
	$rResult[0]['compare_set_img_id'] = $rResult[0]['compare_set_img_id'];
	
	$rResult[0]['status'] = ($status == 1) ? 'Active' : 'InActive';
	$category_name = $category_result[0]['category_name'];
	$rResult[0]['js_category_name'] = $category_name;
	$rResult[0]['category_name'] = html_entity_decode($category_name,ENT_QUOTES,'UTF-8');
	$rResult[0]['create_date'] = date('d-m-Y',strtotime($rResult[0]['create_date']));
	
	$rResult[0] = array_change_key_case($rResult[0],CASE_UPPER);
	//print "<pre>"; print_r($rResult[0]);
	$xmlArt .= "<ONCARS_COMPARE_SET_DATA>";
	foreach($rResult[0] as $k1=>$v1){
		$xmlArt .= "<$k1><![CDATA[$v1]]></$k1>";
	}
	$xmlArt .= "</ONCARS_COMPARE_SET_DATA>";
	$xmlArt .= "</ONCARS_COMPARE_SET>";
}

unset($result);
if(!empty($category_id)){
	$result = $product->arrGetOncarsCompareSetDetails("",$category_id,"","","");
}
//print "<pre>"; print_r($result);
$cnt = sizeof($result);
if(!$cnt){$cnt = 0;}
$position_cnt = $cnt+1;
$xml = "<ONCARS_COMPARE_SET_MASTER>";
$xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";	
for($pocnt=1;$pocnt<=$position_cnt;$pocnt++){
	$xml .= "<COMPARISON_POSITION>";
        $xml .= "<COMPARISON_POSITION_DATA>".$pocnt."</COMPARISON_POSITION_DATA>";
        $xml .= "</COMPARISON_POSITION>";
}
for($i=0;$i<$cnt;$i++){
	$status = $result[$i]['status'];
	$categoryid = $result[$i]['category_id'];
	if(!empty($categoryid)){
		$category_result = $category->arrGetCategoryDetails($categoryid);
	}
	$title = $result[$i]["title"];
        if(!empty($title)){
  		$title = html_entity_decode($title,ENT_QUOTES,'UTF-8');
                $title = removeSlashes($title);
        }
        $result[$i]["title"] = $title;

        $description = $result[$i]["description"];
        if(!empty($description)){
                $description = html_entity_decode($description,ENT_QUOTES,'UTF-8');
                $description = removeSlashes($description);
        }
        $result[$i]["description"] = $description;
	$product_ids = $result[$i]['compare_set'];
	if(!empty($product_ids) && $product_ids!=0){
		$product_ids_arr = explode(",",$product_ids);
		$productName1 = "";
		for($j=0;$j<sizeof($product_ids_arr);$j++){
			$product_id = $product_ids_arr[$j];
			$product_result =$product->arrGetProductDetails($product_id,$category_id,"","","","","","","","");
			//print "<pre>"; print_r($product_result);print"</pre>";
			if(is_array($product_result)){
				$pos=$j+1;
				$sProductName=$product_result[0]['product_name'];
				$iProductId=$product_result[0]['product_id'];
				$iBrandId=$product_result[0]['brand_id'];
				unset($brand_result);
				$brand_result = $brand->arrGetBrandDetails($iBrandId,$category_id);
				$brand_name = $brand_result[0]['brand_name'];
				$sVariant=$product_result[0]['variant'];
				$productName=$brand_name." " .$sProductName." ".$sVariant;
				$brand_name="";
				$productName1 .=$productName.",";
				$product_model =$product->arrGetProductNameInfo("",$category_id,"",$sProductName,"");
				$product_name_id = $product_model[0]['product_name_id'];
				$result[$i]['comp_model_id_'.$pos] = $product_name_id;
				$result[$i]['comp_brand_id_'.$pos] = $iBrandId;
				$result[$i]['comp_product_id_1'.$pos] = $iProductId;
			}
		}
		$product_name1=substr($productName1 , 0 , -1);
		$result[$i]['js_product_names'] =$product_name1;
		$result[$i]['product_names'] = $product_name1 ? html_entity_decode($product_name1,ENT_QUOTES,'UTF-8') : 'Nil';
	}
	$result[$i]['status'] = ($status == 1) ? 'Active' : 'InActive';
	$category_name = $category_result[0]['category_name'];
	$result[$i]['js_category_name'] = $category_name;
	$result[$i]['category_name'] = html_entity_decode($category_name,ENT_QUOTES,'UTF-8');
	$result[$i]['create_date'] = date('d-m-Y',strtotime($result[$i]['create_date']));
	$result[$i]['js_feature_name'] = $result[$i]['feature_name'];
	$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
	//print "<pre>"; print_r($result[$i]);
	$xml .= "<ONCARS_COMPARE_SET_MASTER_DATA>";
	foreach($result[$i] as $k=>$v){
		$xml .= "<$k><![CDATA[$v]]></$k>";
	}
	$xml .= "</ONCARS_COMPARE_SET_MASTER_DATA>";
}
$xml .= "</ONCARS_COMPARE_SET_MASTER>";




if(!empty($category_id)){
	$result = $brand->arrGetBrandDetails("",$category_id);
}	
$cnt = sizeof($result);
$xml .= "<BRAND_MASTER>";
$xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
for($i=0;$i<$cnt;$i++){
	$status = $result[$i]['status'];
	$categoryid = $result[$i]['category_id'];
	if(!empty($categoryid)){
		$category_result = $category->$result = $category->arrGetCategoryDetails($categoryid);
	}
	$category_name = $category_result[0]['category_name'];
	$result[$i]['js_category_name'] = $category_name;
	$result[$i]['category_name'] = html_entity_decode($category_name,ENT_QUOTES,'UTF-8');
	$result[$i]['brand_status'] = ($status == 1) ? 'Active' : 'InActive';
	$result[$i]['create_date'] = date('d-m-Y',strtotime($result[$i]['create_date']));
	$result[$i]['js_brand_name'] = $result[$i]['brand_name'];
	$result[$i]['brand_name'] = html_entity_decode($result[$i]['brand_name'],ENT_QUOTES,'UTF-8');
	$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
	$xml .= "<BRAND_MASTER_DATA>";
	foreach($result[$i] as $k=>$v){
		$xml .= "<$k><![CDATA[$v]]></$k>";
	}
	$xml .= "</BRAND_MASTER_DATA>";
}
$xml .= "</BRAND_MASTER>";

$iRelUploadCnt = $arr_cnt;
$iRelUploadCnt= $iRelUploadCnt ? $iRelUploadCnt :1;
$config_details = get_config_details();
$strXML = "<?xml version='1.0' encoding='iso-8859-1'?>";
$strXML .= "<XML>";
$strXML .= "<MSG><![CDATA[$msg]]></MSG>";
$strXML .= "<STARTLIMIT><![CDATA[$startlimit]]></STARTLIMIT>";
$strXML .= "<CNT><![CDATA[$limitcnt]]></CNT>";
$strXML .= "<WALLCNT><![CDATA[$iRelUploadCnt]]></WALLCNT>";
$strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$category_id]]></SELECTED_CATEGORY_ID>";
$strXML .= "<SELECTED_ACTION_TYPE><![CDATA[$actiontype]]></SELECTED_ACTION_TYPE>"; 
$strXML  .= "<DISP_ROW_COUNT><![CDATA[$arr_cnt]]></DISP_ROW_COUNT>";
$strXML .= $config_details;
$strXML .= $xml;
$strXML .= $nodesPaging;
$strXML .= $xmlArt;
$strXML .= "</XML>";
$strXML = mb_convert_encoding($strXML, "UTF-8");
//header('Content-type: text/xml');echo $strXML;exit;
if($_GET['debug']==1) { header('Content-type: text/xml');echo $strXML;exit; }
$doc = new DOMDocument();
$doc->loadXML($strXML);
$doc->saveXML();

$xslt = new xsltProcessor;
$xsl = DOMDocument::load('../xsl/oncars_comparison_dashboard.xsl');

$xslt->importStylesheet($xsl);
print $xslt->transformToXML($doc);
?>
