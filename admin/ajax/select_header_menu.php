<?php
	require_once('../../include/config.php');
	require_once(CLASSPATH.'DbConn.php');
	require_once(CLASSPATH.'menu.class.php');

	$dbconn = new DbConn;
	$menu = new MenuManagement;

	//echo "menu_id==".$_REQUEST['menuid'];
	$menu_level = $_REQUEST['menuid'];
	$cat_id = $_REQUEST['catid'];
	if(!empty($menu_level)){
		$breadcrumbresult = $menu->arrGetHeaderMenuBreadCrumb($menu_level);
		//print_r($breadcrumbresult);exit;
		$breadcrumbresultcnt = sizeof($breadcrumbresult);
		$breadCrumb = implode(BREAD_CRUMB_STR,$breadcrumbresult);
	}
	$xml = "<BREAD_CRUMB><![CDATA[$breadCrumb]]></BREAD_CRUMB>";

	$divid = $_REQUEST['divid'];
	$ajaxloaderid = $_REQUEST['ajaxloaderid'];
	$totalmenuboxcnt = 1;
	$selectedboxcnt = 0;

	if($breadcrumbresult > 1 && !empty($menu_level)){
		$xml .= "<MENU_LVL_SELECT>";
		foreach($breadcrumbresult as $menu_id => $menu_name){

			$result = $menu->arrGetHeaderMenuDetails("",$menu_id);
			$cnt = sizeof($result);
			if($cnt > 0){
				$html = "<select class='suggestClass' id='menuOptionsLevel_".$totalmenuboxcnt."' size='8' onchange=\"javascript:menu_level('menuOptionsLevel_".$totalmenuboxcnt."','$divid','$ajaxloaderid');\">";
				$html .= "<option value=''>---------------- Select Menu ----------------</option>";
				for($i=0;$i<$cnt;$i++){
					$menuid = $result[$i]['menu_id'];
					$menuname = $result[$i]['menu_name'];
					if(array_key_exists($menuid,$breadcrumbresult)){
						$html .= "<option value='$menuid' selected='selected'>$menuname</option>";
						$selectedboxcnt++;
					}else{
						$html .= "<option value='$menuid'>$menuname</option>";
					}
				 }
				$html .= "</select>";
				$xml .= "<MENU_LVL_SELECT_BOX><![CDATA[$html]]></MENU_LVL_SELECT_BOX>";
				$last_lvl_menuid = end(array_flip($breadcrumbresult));
				if($last_lvl_menuid == $menu_id){
					$lastlevelhtml .= "<!--start below code is used to set last menu id value and menu name as a hidden field for use in form-->";
					$lastlevelhtml .= "<input type='hidden' name='last_lvl_menuid' id='last_lvl_menuid' value='$menuid'/>";
					$lastlevelhtml .="<input type='hidden' name='last_lvl_menu_name' id='last_lvl_menu_name' value='$menuname'/>";
					$lastlevelhtml .= "<!--start below code is used to set last menu id value and menu name as a hidden field for use in form-->";
				}
				$totalmenuboxcnt++;
			}else{
				$lastlevelhtml .= "<!--start below code is used to set last menu id value and menu name as a hidden field for use in form-->";
				$lastlevelhtml .= "<input type='hidden' name='last_lvl_menuid' id='last_lvl_menuid' value='$menu_id'/>";
				$lastlevelhtml .="<input type='hidden' name='last_lvl_menu_name' id='last_lvl_menu_name' value='$menu_name'/>";
				$lastlevelhtml .= "<!--start below code is used to set last menu id value and menu name as a hidden field for use in form-->";
			}
			$hiddenhtml .= "<!--start below code is used to set menu id value and menu name as a hidden field for use in form(for {$totalmenuboxcnt}st select box)-->";
			$hiddenhtml .= "<input type='hidden' name='menuId_{$totalmenuboxcnt}' id='menuId_{$totalmenuboxcnt}' value='$menu_id'/>";
			$hiddenhtml .="<input type='hidden' name='menuValue_{$totalmenuboxcnt}' id='menuValue_{$totalmenuboxcnt}' value='$menu_name'/>";
			$hiddenhtml .= "<!--end above code is used to set menu id value and menu name as a hidden field for use in form(for {$totalmenuboxcnt}st select box)-->";


			$hiddenhtml .=	$lastlevelhtml;
		}
		$xml .= "<MENU_LVL_HIDDEN_DATA><![CDATA[$hiddenhtml]]></MENU_LVL_HIDDEN_DATA>";
		$xml .= "</MENU_LVL_SELECT>";
	}

	$result = $menu->arrGetHeaderMenuDetails("","0",$cat_id);
	//print"<pre>";print_r($result);print"</pre>";exit;
	$cnt = sizeof($result);
	$xml .= "<ROOT_LVL_MENU>";
	$xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
	for($i=0;$i<$cnt;$i++){
		$menu_id = $result[$i]['menu_id'];
		$xml .= "<ROOT_LVL_DATA>";
		//if($breadcrumbresultcnt > 1 && !empty($menu_level)){
			if(array_key_exists($menu_id,$breadcrumbresult) && !empty($menu_level)){
				$selectedboxcnt++;
				$xml .= "<SELECTED_MENU><![CDATA[$menu_id]]></SELECTED_MENU>";
			}
		//}
		$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
		foreach($result[$i] as $k=>$v){
			$xml .="<$k><![CDATA[$v]]></$k>";
		}
		$xml .= "</ROOT_LVL_DATA>";
	}
	$xml .= "</ROOT_LVL_MENU>";

	$config_details = get_config_details();
	$strXML = "<XML>";
	$strXML .= $config_details;
	$strXML .= $xml;
	$strXML .= "<DIV_ID><![CDATA[$divid]]></DIV_ID>";
	$strXML .= "<AJAX_LOADER_ID><![CDATA[$ajaxloaderid]]></AJAX_LOADER_ID>";
	$strXML .= "<TOTAL_MENU_BOX><![CDATA[$totalmenuboxcnt]]></TOTAL_MENU_BOX>";
	$strXML .= "<SELECTED_MENU_BOX><![CDATA[$selectedboxcnt]]></SELECTED_MENU_BOX>";
	$strXML .= "<SELECTED_MENU_ID><![CDATA[$menu_level]]></SELECTED_MENU_ID>";
	$strXML .= "</XML>";
	//header('Content-type: text/xml');echo $strXML;exit;
	$doc = new DOMDocument();
	$doc->loadXML($strXML);
	$doc->saveXML();
	$xslt = new xsltProcessor;
	$xsl = DOMDocument::load('../xsl/header_menu_ajax.xsl');
	$xslt->importStylesheet($xsl);
	print $xslt->transformToXML($doc);
?>
