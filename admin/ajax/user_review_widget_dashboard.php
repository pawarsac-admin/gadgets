<?php	
require_once('../../include/config.php');
require_once(CLASSPATH.'DbConn.php');
require_once(CLASSPATH.'category.class.php');
require_once(CLASSPATH.'product.class.php');
require_once(CLASSPATH.'brand.class.php');
require_once(CLASSPATH.'user_review.class.php');

$dbconn = new DbConn;
$category = new CategoryManagement;
$product = new ProductManagement;
$brand = new BrandManagement;
$userreview = new USERREVIEW;

//print "<pre>"; print_r($_REQUEST);print"</pre>";exit;
$category_id = $_REQUEST['catid'];
$startlimit = $_REQUEST['startlimit'];
$limitcnt = $_REQUEST['cnt'];
$actiontype = $_REQUEST['actiontype'] ? $_REQUEST['actiontype'] : "Insert";
$id = $_REQUEST['id'];
$selected_id = $id;

if($_REQUEST['act']=='update' && !empty($id)){
	$result = $userreview->arrGetUserReviewWidget($id,$brand_id,$product_info_id,$category_id,"");
	//print "<pre>"; print_r($result);exit;
	$cnt = sizeof($result);
	$xml .= "<USER_REVIEW_WIDGET_DETAIL>";
	$xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
	for($i=0;$i<$cnt;$i++){
		$status = $result[$i]['status'];
		$categoryid = $result[$i]['category_id'];
		if(!empty($categoryid)){
			$category_result = $category->arrGetCategoryDetails($categoryid,"","","","");
		}
		$category_name = $category_result[0]['category_name'];
		$result[$i]['js_category_name'] = $category_name;
		$result[$i]['category_name'] = html_entity_decode($category_name,ENT_QUOTES);
		$result[$i]['status'] = ($status == 1) ? 'Active' : 'InActive';
		$result[$i]['create_date'] = date('d-m-Y',strtotime($result[$i]['create_date']));

		$brand_id = $result[$i]['brand_id'];
		$result[$i]['brand_id'] = $brand_id;
		if(!empty($brand_id)){
			$brand_result = $brand->arrGetBrandDetails($brand_id,$category_id,"","","");
		}
		$brand_name = $brand_result[0]['brand_name'];
		$result[$i]['js_brand_name'] = $brand_name;
		$result[$i]['brand_name'] = html_entity_decode($brand_name,ENT_QUOTES);
		if($result[$i]['product_info_id']!=''){
			$product_info_id = $result[$i]['product_info_id'];
			$result[$i]['product_info_id'] = $product_info_id;
			if(!empty($product_info_id)){
				$product_info_result = $product->arrGetProductNameInfo($product_info_id,$category_id,"","","","","");
				$product_info_name = $product_info_result[0]['product_info_name'];
			}
			$result[$i]['js_product_name'] = $product_info_name;
			$result[$i]['product_name'] = html_entity_decode($product_info_name,ENT_QUOTES);
		}
		$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
		//print "<pre>"; print_r($result[$i]);
		$xml .= "<USER_REVIEW_WIDGET_DETAIL_DATA>";
		foreach($result[$i] as $k=>$v){
			$xml .= "<$k><![CDATA[$v]]></$k>";
		}
		$xml .= "</USER_REVIEW_WIDGET_DETAIL_DATA>";
	}
	$xml .= "</USER_REVIEW_WIDGET_DETAIL>";
	unset($brand_result);
	unset($product_info_result);
}


unset($result);
$product_info_name=""; $product_variant=""; $product_name="";
if(!empty($category_id)){
	$result = $userreview->arrGetUserReviewWidget("","","",$category_id,"");
}
//print"<pre>";print_r($result);print"</pre>";exit;
$cnt = sizeof($result);
$xml .= "<USER_REVIEW_WIDGET_MASTER>";
$xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
for($i=0;$i<$cnt;$i++){

	$id = $result[$i]['id'];
	$status = $result[$i]['status'];
	$categoryid = $result[$i]['category_id'];
	$brand_id = $result[$i]['brand_id'];
	//$product_id = $result[$i]['product_id'];
	$product_info_id = $result[$i]['product_info_id'];
	$position = $result[$i]['position'];

	if(!empty($categoryid)){
		$category_result = $category->arrGetCategoryDetails($categoryid);
	}
	$category_name = $category_result[0]['category_name'];
	$result[$i]['js_category_name'] = $category_name;
	$result[$i]['category_name'] = html_entity_decode($category_name,ENT_QUOTES);
	$result[$i]['status'] = ($status == 1) ? 'Active' : 'InActive';
	$result[$i]['create_date'] = date('d-m-Y',strtotime($result[$i]['create_date']));


	$brand_id = $result[$i]['brand_id'];
	$result[$i]['brand_id'] = $brand_id;
	if(!empty($brand_id)){
		unset($brand_result);
		$brand_result = $brand->arrGetBrandDetails($brand_id,$category_id);
	}
	$brand_name = $brand_result[0]['brand_name'];
	$result[$i]['js_brand_name'] = $brand_name;
	$result[$i]['brand_name'] = html_entity_decode($brand_name,ENT_QUOTES);

	if($result[$i]['product_info_id']!=''){
		$product_info_id = $result[$i]['product_info_id'];
		$result[$i]['product_info_id'] = $product_info_id;
		if(!empty($product_info_id)){
			unset($product_info_result);
			$product_info_result = $product->arrGetProductNameInfo($product_info_id,$category_id);
			$product_info_name = $product_info_result[0]['product_info_name'];
		}
		$result[$i]['js_product_name'] = $product_info_name;
		$result[$i]['product_name'] = html_entity_decode($product_info_name,ENT_QUOTES);
	}

	$product_name=''; $product_info_name='';

	$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
	//print "<pre>"; print_r($result[$i]);
	$xml.= "<USER_REVIEW_WIDGET_MASTER_DATA>";
	foreach($result[$i] as $k=>$v){
		$xml.= "<$k><![CDATA[$v]]></$k>";
	}
	$xml.= "</USER_REVIEW_WIDGET_MASTER_DATA>";
	$product_info_name=""; $product_name="";
	unset($brand_result);
	unset($product_info_result);
}
$xml.= "</USER_REVIEW_WIDGET_MASTER>";
//echo "TEST---".$xml_wallpaper;

unset($result);
if(!empty($category_id)){
        $result = $brand->arrGetBrandDetails("",$category_id);
}
$cnt = sizeof($result);
$xml .= "";
$xml .= "<BRAND_MASTER>";
$xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
for($i=0;$i<$cnt;$i++){
        $status = $result[$i]['status'];
        $categoryid = $result[$i]['category_id'];
        if(!empty($categoryid)){
                //$category_result = $category->$result = $category->arrGetCategoryDetails($categoryid);
		$category_result = $category->arrGetCategoryDetails($categoryid,"","","","");
        }
        $category_name = $category_result[0]['category_name'];
        $result[$i]['js_category_name'] = $category_name;
        $result[$i]['category_name'] = html_entity_decode($category_name,ENT_QUOTES);
        $result[$i]['brand_status'] = ($status == 1) ? 'Active' : 'InActive';
        $result[$i]['create_date'] = date('d-m-Y',strtotime($result[$i]['create_date']));
        $result[$i]['js_brand_name'] = $result[$i]['brand_name'];
        $result[$i]['brand_name'] = html_entity_decode($result[$i]['brand_name'],ENT_QUOTES);
        $result[$i] = array_change_key_case($result[$i],CASE_UPPER);
	//print "<pre>"; print_r($result[$i]);
        $xml .= "<BRAND_MASTER_DATA>";
        foreach($result[$i] as $k=>$v){
                $xml .= "<$k><![CDATA[$v]]></$k>";
        }
        $xml .= "</BRAND_MASTER_DATA>";
}
$xml .= "</BRAND_MASTER>";



$iRelUploadCnt= $iRelUploadCnt ? $iRelUploadCnt :0;
$config_details = get_config_details();

$strXML = "<XML>";
$strXML .= "<MSG><![CDATA[$msg]]></MSG>";
$strXML .= "<STARTLIMIT><![CDATA[$startlimit]]></STARTLIMIT>";
$strXML .= "<CNT><![CDATA[$limitcnt]]></CNT>";
$strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$category_id]]></SELECTED_CATEGORY_ID>"; 
$strXML .= $config_details;
$strXML .= $xml;
$strXML .= "<WALLCNT><![CDATA[$iRelUploadCnt]]></WALLCNT>";
$strXML .= "<SELECTED_TYPE><![CDATA[$type_selecetd]]></SELECTED_TYPE>";
$strXML .= "<SELECTED_ACTION_TYPE><![CDATA[$actiontype]]></SELECTED_ACTION_TYPE>";
$strXML .= "<ID><![CDATA[$selected_id]]></ID>";
$strXML .= "</XML>";

$strXML = mb_convert_encoding($strXML, "UTF-8");
if($_GET['debug']==1) { header('Content-type: text/xml');echo $strXML;exit; }

$doc = new DOMDocument();
$doc->loadXML($strXML);
$doc->saveXML();

$xslt = new xsltProcessor;
$xsl = DOMDocument::load('../xsl/user_review_widget_dashboard.xsl');

$xslt->importStylesheet($xsl);
print $xslt->transformToXML($doc);
?>
