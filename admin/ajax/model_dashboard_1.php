<?php	
require_once('../../include/config.php');
require_once(CLASSPATH.'DbConn.php');
require_once(CLASSPATH.'category.class.php');
require_once(CLASSPATH.'product.class.php');
require_once(CLASSPATH.'brand.class.php');

$dbconn = new DbConn;
$category = new CategoryManagement;
$oProduct = new ProductManagement;
$oBrand = new BrandManagement;

$product_name_id = $_REQUEST['product_name_id'];
$category_id = $_REQUEST['catid'];
$startlimit = $_REQUEST['startlimit'];
$limitcnt = $_REQUEST['cnt'];
$r_product_id = $_REQUEST['pid'];
$r_brand_id = $_REQUEST['bid'];

if(!empty($category_id)){
	$aBrandDetail=$oBrand->arrGetBrandDetails("",$category_id);
	$sBrandDataDet=arraytoxml($aBrandDetail,"BRAND_MASTER_DATA");
	$sBrandDataDetXML ="<BRAND_MASTER>".$sBrandDataDet."</BRAND_MASTER>";
	if(is_array($aBrandDetail)){
		foreach($aBrandDetail as $ibKey=>$aBrandData){
			$aBrandDetailName[$aBrandData['brand_id']][]=$aBrandData['brand_name'];
		}
	}
	$aProductInfoResultList = $oProduct->arrGetProductNameInfo("",$category_id,"","","1",$startlimit,$cnt);
	//print "<pre>"; print_r($aProductInfoResultList);
	if(is_array($aProductInfoResultList)){
		foreach($aProductInfoResultList as $iKey=>$aProductInfoData){
			$aProductInfoList[$iKey]=$aProductInfoData;
			if(is_array($aBrandDetailName) && isset($aBrandDetailName[$aProductInfoData['brand_id']])){
					$sModelBrandName=$aBrandDetailName[$aProductInfoData['brand_id']][0];
			}
			$aProductInfoList[$iKey]['brand_name']=$sModelBrandName;
		}
		$cnt = sizeof($aProductInfoList);
		$sModelDataDetXML .= "<MODEL_DETAIL>";
		$sModelDataDetXML .= "<COUNT><![CDATA[$cnt]]></COUNT>";
		for($i=0;$i<$cnt;$i++){
			$aProductInfoList[$i] = array_change_key_case($aProductInfoList[$i],CASE_UPPER);
			$sModelDataDetXML .= "<MODEL_DETAIL_DATA>";
			foreach($aProductInfoList[$i] as $k=>$v){
					$sModelDataDetXML .= "<".trim($k)."><![CDATA[".trim($v)."]]></".trim($k).">";
			}
			$sModelDataDetXML .= "</MODEL_DETAIL_DATA>";
		}
		$sModelDataDetXML .= "</MODEL_DETAIL>";
		//print_r($aProductInfoList);exit;
		//$sModelDataDet = arraytoxml($aProductInfoList,"MODEL_DETAIL_DATA");
		//$sModelDataDetXML = "<MODEL_DETAIL>".$sModelDataDet."</MODEL_DETAIL>";
	}
}
//echo "DTATA---".$sModelDataDetXML;die();
if($_REQUEST['act']=='Delete' && !empty($product_name_id)){
	$dresult = $oProduct->deleteProductInfo($product_name_id,'PRODUCT_NAME_INFO');
}
if($_REQUEST['act']=='update' && !empty($product_name_id)){
	$aProductInfoResultDetail = $oProduct->arrGetProductNameInfo($product_name_id,$category_id,"","","1",$startlimit,$cnt);
	//print "<pre>"; print_r($result);
	if(is_array($aProductInfoResultDetail)){
		foreach($aProductInfoResultDetail as $iKey=>$aProductInfoValue){
			$aProductInfoDetail[$iKey]=$aProductInfoValue;
			if(is_array($aBrandDetailName) && isset($aBrandDetailName[$aProductInfoData['brand_id']])){
					$sModelBrandName=$aBrandDetailName[$aProductInfoData['brand_id']][0];
			}
			$aProductInfoDetail[$iKey]['brand_name']=$sModelBrandName;
			if($aProductInfoData['video_path']!=''){
				$sMainImagePath=getImageDetails($aProductInfoData['video_path'],SERVICEID,$action='api');
				//$sMainImage = $sMainImagePath['main_image'];
				$aProductInfoDetail[$iKey]['video_path_title'] = $sMainImagePath['title'];
			}
			if($aProductInfoData['image_path']!=''){
				$sMainThmImagePath=getImageDetails($aProductInfoData['image_path'],SERVICEID,$action='api');
				//$sMainThmImage = $sMainThmImagePath['main_image'];
				$aProductInfoDetail[$iKey]['image_path_title'] = $sMainThmImagePath['title'];
			}
		}
		
		$sModelValueData=arraytoxml($aProductInfoDetail,"MODEL_MASTER_DATA");
		$sModelValueDataXML ="<MODEL_MASTER>".$sModelValueData."</MODEL_MASTER>";
	}
}

$config_details = get_config_details();

$strXML = "<XML>";
$strXML .= "<MSG><![CDATA[$msg]]></MSG>";
$strXML .= "<STARTLIMIT><![CDATA[$startlimit]]></STARTLIMIT>";
$strXML .= "<CNT><![CDATA[$limitcnt]]></CNT>";
$strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$category_id]]></SELECTED_CATEGORY_ID>"; 
$strXML .= $config_details;
$strXML .= $sModelDataDetXML;
$strXML .= $sModelValueDataXML;
$strXML .= "<WALLCNT><![CDATA[$iRelUploadCnt]]></WALLCNT>";
$strXML .= "</XML>";
$strXML = mb_convert_encoding($strXML, "UTF-8");
//header('Content-type: text/xml');echo $strXML;exit;
$doc = new DOMDocument();
$doc->loadXML($strXML);
$doc->saveXML();

$xslt = new xsltProcessor;
$xsl = DOMDocument::load('../xsl/model_dashboard.xsl');

$xslt->importStylesheet($xsl);
print $xslt->transformToXML($doc);
?>