<?php	
require_once("../../include/config.php");
require_once(CLASSPATH.'DbConn.php');
require_once(CLASSPATH."category.class.php");
require_once(CLASSPATH."brand.class.php");
require_once(CLASSPATH."citystate.class.php");
require_once(CLASSPATH."dealer.class.php");

$dbconn 	= new DbConn;
$oCategory	= new CategoryManagement;
$oBrand		= new BrandManagement;
$oCityState = new citystate;
$oDealer	= new Dealer;

$premium_dealer_id = $_REQUEST['premium_dealer_id'];
$dealer_id = $_REQUEST['dealer_id'];
$startlimit = $_REQUEST['startlimit'];
$limitcnt = $_REQUEST['cnt'];
$category_id = $_REQUEST['selected_category_id'] ? $_REQUEST['selected_category_id'] : $_REQUEST['catid'];

$xml="";

if($_REQUEST['act']=='update'){
	$rResult=$oDealer->arrGetPremiumDealerDetails($premium_dealer_id,"","","","",$category_id,"","","","","");
	$cnt = sizeof($rResult);
	$status = $rResult[0]['status'];
	$rResult[0]['premium_dealer_id'] = $rResult[0]['premium_dealer_id'];
	$rResult[0]['js_title'] = $rResult[0]['title'];
	$rResult[0]['title'] = $rResult[0]['title'] ? html_entity_decode($rResult[0]['title'],ENT_QUOTES) : 'Nil';
	$rResult[0]['status'] = ($status == 1) ? 'Active' : 'InActive';
	$rResult[0] = array_change_key_case($rResult[0],CASE_UPPER);
	$xml .= "<DEALER_DATA>";
	foreach($rResult[0] as $k1=>$v1){
		$xml .= "<$k1><![CDATA[$v1]]></$k1>";
	}
	$xml .= "</DEALER_DATA>";
}

if(!empty($category_id)){
	$result=$oDealer->arrGetPremiumDealerDetails("","","","","",$category_id,"","","","","");
}
	//print "<pre>"; print_r($result);
	$cnt = sizeof($result);
	$xml .= "<DEALER_MASTER>";
	$xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
	for($i=0;$i<$cnt;$i++){
		$result[$i]['premium_dealer_id'] = $result[$i]['premium_dealer_id'];
		$result[$i]['title'] = $result[$i]['title'];
		$result[$i]['category_id'] = $result[$i]['category_id'];
		$result[$i]['brand_id'] = $result[$i]['brand_id'];
		$brand_id = $result[$i]['brand_id'];
		if(!empty($brand_id)){
			$brand_result = $oBrand->arrGetBrandDetails($brand_id);
			$brand_name = $brand_result[0]['brand_name'];
		}
		$result[$i]['js_brand_name'] = $brand_name;
		$result[$i]['brand_name'] = $brand_name ? html_entity_decode($brand_name,ENT_QUOTES) : 'Nil';
		$result[$i]['product_id'] = $result[$i]['product_id'];
		$city_id = $result[$i]['city_id'];
		if(!empty($city_id)){
			$city_result = $oCityState->arrGetCityDetails($city_id);
			$city_name = $city_result[0]['city_name'];
		}
		$result[$i]['city_name'] =$city_name;
		$status = $result[$i]['status'];
		$result[$i]['status'] = ($status == 1) ? 'Active' : 'InActive';
		$result[$i]['create_date'] = date('d-m-Y',strtotime($result[$i]['create_date']));
		$result[$i]['update_date'] = date('d-m-Y',strtotime($result[$i]['update_date']));
		$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
		//print "<pre>"; print_r($result[$i]);
		$xml .= "<DEALER_MASTER_DATA>";
		foreach($result[$i] as $k=>$v){
				$xml .= "<$k><![CDATA[$v]]></$k>";
		}
		$xml .= "</DEALER_MASTER_DATA>";
	}
	$xml .= "</DEALER_MASTER>";

unset($result);
if(!empty($category_id)){
	$result=$oDealer->arrGetDealerDetails("","","","",$category_id,"","",$startlimit,$limitcnt);
}
	//print_r($result);
	$cnt = sizeof($result);
	$xml .= "<DEALER_DETAIL>";
	$xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
	for($i=0;$i<$cnt;$i++){
		$result[$i]['title'] = $result[$i]['title'];
		$result[$i]['category_id'] = $result[$i]['category_id'];
		$result[$i]['brand_id'] = $result[$i]['brand_id'];
		$brand_id = $result[$i]['brand_id'];
		if(!empty($brand_id)){
			$brand_result = $oBrand->arrGetBrandDetails($brand_id);
			$brand_name = $brand_result[0]['brand_name'];
		}
		$result[$i]['js_brand_name'] = $brand_name;
		$result[$i]['brand_name'] = $brand_name ? html_entity_decode($brand_name,ENT_QUOTES) : 'Nil';
		$result[$i]['product_id'] = $result[$i]['product_id'];
		$city_id = $result[$i]['city_id'];
		if(!empty($city_id)){
			$city_result = $oCityState->arrGetCityDetails($city_id);
			$city_name = $city_result[0]['city_name'];
		}
		$result[$i]['city_name'] =$city_name;
		$status = $result[$i]['status'];
		$result[$i]['status'] = ($status == 1) ? 'Active' : 'InActive';
		$result[$i]['create_date'] = date('d-m-Y',strtotime($result[$i]['create_date']));
		$result[$i]['update_date'] = date('d-m-Y',strtotime($result[$i]['update_date']));
		$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
		//print "<pre>"; print_r($result[$i]);
		$xml .= "<DEALER_DETAIL_DATA>";
		foreach($result[$i] as $k=>$v){
				$xml .= "<$k><![CDATA[$v]]></$k>";
		}
		$xml .= "</DEALER_DETAIL_DATA>";
	}
	$xml .= "</DEALER_DETAIL>";



$config_details = get_config_details();

$strXML = "<XML>";
$strXML .= "<MSG><![CDATA[$msg]]></MSG>";
$strXML .= "<STARTLIMIT><![CDATA[$startlimit]]></STARTLIMIT>";
$strXML .= "<CNT><![CDATA[$limitcnt]]></CNT>";
$strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$category_id]]></SELECTED_CATEGORY_ID>"; 
$strXML .= $config_details;
$strXML .= $xml;
$strXML .= "<WALLCNT><![CDATA[$iRelUploadCnt]]></WALLCNT>";
$strXML .= "</XML>";

$strXML = mb_convert_encoding($strXML, "UTF-8");
if($_GET['debug']==1) { header('Content-type: text/xml');echo $strXML;exit; }

$doc = new DOMDocument();
$doc->loadXML($strXML);
$doc->saveXML();

$xslt = new xsltProcessor;
$xsl = DOMDocument::load('../xsl/premium_dealer_dashboard.xsl');

$xslt->importStylesheet($xsl);
print $xslt->transformToXML($doc);
?>
