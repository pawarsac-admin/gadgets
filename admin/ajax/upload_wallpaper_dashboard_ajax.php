<?php	
	require_once('../../include/config.php');
	require_once(CLASSPATH.'DbConn.php');
	require_once(CLASSPATH.'pivot.class.php');
	require_once(CLASSPATH.'feature.class.php');
	require_once(CLASSPATH.'category.class.php');
	require_once(CLASSPATH.'product.class.php');
	require_once(CLASSPATH.'brand.class.php');
	require_once(CLASSPATH.'wallpaper.class.php');
	
	
	$dbconn = new DbConn;
	$pivot = new PivotManagement;
	$feature = new FeatureManagement;
	$category = new CategoryManagement;
	$product = new ProductManagement;
	$brand = new BrandManagement;
	$oWallpaper=new Wallpapers;


	$wallpaper_id = $_REQUEST['lpid'];
	$category_id = $_REQUEST['catid'];
	$startlimit = $_REQUEST['startlimit'];
	$limitcnt = $_REQUEST['cnt'];
	$r_product_id = $_REQUEST['pid'];
	$r_brand_id = $_REQUEST['bid'];

	//print_r($_REQUEST);
	
	


	if($_REQUEST['wallpaper_id']!=''){
		$wallpaper_id = $_REQUEST['wallpaper_id'];
	}
	if($_REQUEST['act']=='Delete' && !empty($wallpaper_id)){
			$dresult = $product->boolDeleteWallpapers($wallpaper_id);

	}
	
	if($_REQUEST['act']=='update' && !empty($r_product_id)){
		$rResult =$oWallpaper->arrGetWallpapersDetails("",$r_product_id,"","","","",$startlimit,$limitcnt);
		//print "<pre>"; print_r($rResult);
		
		$xmlArt='';
		$xmlArt .= "<WALLPAPER_DETAIL>";
		$iWallPapercnt = sizeof($rResult);
		
		if(is_array($rResult)){
			foreach($rResult as $iKey=>$aResultData){
				$status = $aResultData['status'];
				$categoryid = $aResultData['category_id'];
				$wallpaper_id = $aResultData['wallpaper_id'];
				//echo "wallpaper_id ---".$wallpaper_id ;
				if(!empty($wallpaper_id)){
					$aSlideShowResult = $oWallpaper->arrGetSlideShowDetails($wallpaper_id);
					$aSlideShowResult[0]['wallpaper_id'];
					$rResult[0]['type']=($aSlideShowResult[0]['wallpaper_id']!='') ? '2' : '1';
				}
				if(!empty($categoryid)){
					$category_result = $category->arrGetCategoryDetails($categoryid);
				}
				$brand_id = $aResultData['brand_id'];
				if(!empty($brand_id)){
					$brand_result = $brand->arrGetBrandDetails($brand_id);
					$brand_name = $brand_result[0]['brand_name'];
				}
			
				$product_id = $aResultData['product_id'];
				$aResultData['product_status'] = ($status == 1) ? 'Active' : 'InActive';
				$category_name = $category_result[0]['category_name'];
				$aResultData['js_category_name'] = $category_name;
				$aResultData['category_name'] = html_entity_decode($category_name,ENT_QUOTES);
				$aResultData['create_date'] = date('d-m-Y',strtotime($rResult[0]['cdate']));
				$aResultData['js_feature_name'] = $result[0]['feature_name'];
				
				$iMainMediaId=$aResultData["img_media_id"];
				$iThmMediaId=$aResultData["media_id"];	
				/*
				if($iMainMediaId!=''){
					$sMainImagePath=getImageDetails($iMainMediaId,SERVICEID,$action='api');
					$sMainImage = $sMainImagePath['main_image'];
					$sTitleImage = $sMainImagePath['title'];
					$aResultData['media_path_title'] = $sTitleImage;
				}
				if($iThmMediaId!=''){
					$sThmImagePath=getImageDetails($iThmMediaId,SERVICEID,$action='api');
					$sMainImage = $sThmImagePath['main_image'];
					$sTitleImage = $sThmImagePath['title'];
					$aResultData['image_path_title'] = $sTitleImage;
				}
				*/
				//$aResultData['media_path_title'] = $sTitleImage;
				//$aResultData['image_path_title'] = $sTitleImage;
				$aResultData = array_change_key_case($aResultData,CASE_UPPER);

				$xmlArt .= "<WALLPAPER_DATA>";
				//print "<pre>"; print_r($aResultData);
				foreach($aResultData as $k1=>$v1){
					$xmlArt .= "<$k1><![CDATA[$v1]]></$k1>";
				}
				$xmlArt .= "</WALLPAPER_DATA>";
			}
			$xmlArt .= "</WALLPAPER_DETAIL>";
		}

	}else{ $iWallPapercnt =0; }
	
	$xmlArt;
	if(!empty($category_id)){
		$aParameters=array('category_id'=>$category_id);
		$result = $oWallpaper->arrGetWallpapersDetails("","","0",$category_id,"","",$startlimit,$limitcnt);
	}
	if(is_array($result)){
		foreach($result as $iKey=>$aValueDet){
			$aWallpaperDet[$aValueDet['product_id']][]=$aValueDet;
		}
	}
	$iPcnt=0;
	//print_r($aWallpaperDet);
	if(is_array($aWallpaperDet)){
		foreach($aWallpaperDet as $ipKey=>$aValueProductWise){
			if(is_array($aValueProductWise)){
				foreach($aValueProductWise as $ipdKey=>$aWallpaerValueProdDet){
					$aProductWiseData[$iPcnt]['product_id']=$aWallpaerValueProdDet['product_id'];
					$aProductWiseData[$iPcnt]['category_id']=$aWallpaerValueProdDet['category_id'];
					$aProductWiseData[$iPcnt]['brand_id']=$aWallpaerValueProdDet['brand_id'];
				}
			}
			$iPcnt++;
		}
	}
	$result='';
	//print "<pre>"; print_r($aProductWiseData);
	$result=$aProductWiseData;
	$cnt = sizeof($result);
	$xml = "<PRODUCT_MASTER>";
	$xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";	
	for($i=0;$i<$cnt;$i++){
		$status = $result[$i]['status'];
		$categoryid = $result[$i]['category_id'];
		if(!empty($categoryid)){
			$category_result = $category->arrGetCategoryDetails($categoryid);
		}
		$brand_id = $result[$i]['brand_id'];
		if(!empty($brand_id)){
			$brand_result = $brand->arrGetBrandDetails($brand_id);
			$brand_name = $brand_result[0]['brand_name'];
		}
		$result[$i]['js_brand_name'] = $brand_name;
		$result[$i]['brand_name'] = $brand_name ? html_entity_decode($brand_name,ENT_QUOTES) : 'Nil';
		$product_id1 = $result[$i]['product_id'];
		if(!empty($product_id1)){
			$product_result =$product->arrGetProductDetails($product_id1,$category_id,"","1","","","",$startlimit,$limitcnt);
			$product_name1 = $product_result[0]['product_name'];
		}
		$result[$i]['js_product_name'] =$product_name1;
		$result[$i]['product_name'] = $product_name1 ? html_entity_decode($product_name1,ENT_QUOTES) : 'Nil';
		$result[$i]['product_status'] = ($status == 1) ? 'Active' : 'InActive';
		$category_name = $category_result[0]['category_name'];
		$result[$i]['js_category_name'] = $category_name;
		$result[$i]['category_name'] = html_entity_decode($category_name,ENT_QUOTES);
		$result[$i]['create_date'] = date('d-m-Y',strtotime($result[$i]['create_date']));
		$result[$i]['js_feature_name'] = $result[$i]['feature_name'];
		$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
		$xml .= "<PRODUCT_MASTER_DATA>";
		foreach($result[$i] as $k=>$v){
			$xml .= "<$k><![CDATA[$v]]></$k>";
		}
		$xml .= "</PRODUCT_MASTER_DATA>";
	}
	$xml .= "</PRODUCT_MASTER>";



	if(!empty($category_id)){
		$result = $brand->arrGetBrandDetails("",$category_id);
	}	
	$cnt = sizeof($result);
        $xml .= "<BRAND_MASTER>";
        $xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
        for($i=0;$i<$cnt;$i++){
                $status = $result[$i]['status'];
                $categoryid = $result[$i]['category_id'];
                if(!empty($categoryid)){
                        $category_result = $category->$result = $category->arrGetCategoryDetails($categoryid);
                }
                $category_name = $category_result[0]['category_name'];
                $result[$i]['js_category_name'] = $category_name;
                $result[$i]['category_name'] = html_entity_decode($category_name,ENT_QUOTES);
                $result[$i]['brand_status'] = ($status == 1) ? 'Active' : 'InActive';
                $result[$i]['create_date'] = date('d-m-Y',strtotime($result[$i]['create_date']));
                $result[$i]['js_brand_name'] = $result[$i]['brand_name'];
                $result[$i]['brand_name'] = html_entity_decode($result[$i]['brand_name'],ENT_QUOTES);
                $result[$i] = array_change_key_case($result[$i],CASE_UPPER);
                $xml .= "<BRAND_MASTER_DATA>";
                foreach($result[$i] as $k=>$v){
                        $xml .= "<$k><![CDATA[$v]]></$k>";
                }
                $xml .= "</BRAND_MASTER_DATA>";
        }
        $xml .= "</BRAND_MASTER>";
	
	

	
	
	$config_details = get_config_details();
	$strXML = "<XML>";
	$strXML .= "<MSG><![CDATA[$msg]]></MSG>";
	$strXML .= "<STARTLIMIT><![CDATA[$startlimit]]></STARTLIMIT>";
	$strXML .= "<CNT><![CDATA[$limitcnt]]></CNT>";
	$strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$category_id]]></SELECTED_CATEGORY_ID>"; 
	$strXML .= $config_details;
	$strXML .= $xml;
	$strXML .= $xmlArt;
	$strXML .= "<WALLCNT><![CDATA[$iWallPapercnt]]></WALLCNT>";
	
	$strXML .= "</XML>";
	//header('Content-type: text/xml');echo $strXML;exit;
	$doc = new DOMDocument();
	$doc->loadXML($strXML);
	$doc->saveXML();
	$xslt = new xsltProcessor;
	$xsl = DOMDocument::load('../xsl/upload_wallpaper_dashboard_ajax.xsl');
	$xslt->importStylesheet($xsl);
	print $xslt->transformToXML($doc);
?>
