<?php
require_once('../../include/config.php');
require_once(CLASSPATH.'DbConn.php');
require_once(CLASSPATH.'category.class.php');
require_once(CLASSPATH.'product.class.php');
require_once(CLASSPATH.'brand.class.php');
require_once(CLASSPATH.'feature.class.php');
require_once(CLASSPATH.'pivot.class.php');

$dbconn = new DbConn;
$category = new CategoryManagement;
$product = new ProductManagement;
$brand = new BrandManagement;
$feature = new FeatureManagement;
$pivot = new PivotManagement;

//print "<pre>"; print_r($_REQUEST);
$category_id = $_REQUEST['catid'];
$category_id = ($category_id != "") ? $category_id : SITE_CATEGORY_ID;
$startlimit = $_REQUEST['startlimit'];
$limitcnt = $_REQUEST['cnt'];
$actiontype = $_REQUEST['actiontype'] ;

$popular_feature_id =$_REQUEST['popular_feature_id'];
$selected_pivot_id = $_REQUEST['pivot_id'];
$selected_pivot_group = $_REQUEST['pivot_group'];

if($_REQUEST['act']=='fill_pivots'){
        $str = "<option value='0' selected>---Select Pivot---</option>";
        $pivot_group_id = $_REQUEST["pivot_group"];
        unset($result);
        $result = $pivot->arrGetPivotDetails("",$category_id,"","",$pivot_group_id);
        $cnt = sizeof($result);
        //print "<pre>"; print_r($result);
        for($i=0;$i<$cnt;$i++){
                $pivot_id = $result[$i]['pivot_id'];
                $feature_id = $result[$i]['feature_id'];
                $feature_result = $feature->arrGetFeatureDetails($feature_id,$category_id);
                $fcnt = sizeof($feature_result);
                for($j=0;$j<$fcnt;$j++){
                        $feature_name = $feature_result[$j]['feature_name'];
                        $str .="<option value='".$pivot_id."'>".$feature_name."</option>";
                }
        }
        echo $str;
}else{echo false;}
?>
