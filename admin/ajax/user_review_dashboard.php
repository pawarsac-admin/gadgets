<?php
	require_once('../../include/config.php');
	require_once(CLASSPATH.'DbConn.php');
	require_once(CLASSPATH.'user_review.class.php');
	require_once(CLASSPATH.'brand.class.php');
	require_once(CLASSPATH.'category.class.php');
	require_once(CLASSPATH.'product.class.php');

	$dbconn = new DbConn;
	$userreview = new USERREVIEW;
	$brand = new BrandManagement;
	$category = new CategoryManagement;
	$product = new ProductManagement;

	$category_id = $_REQUEST['catid'] ? $_REQUEST['catid'] : SITE_CATEGORY_ID;		

	unset($result);
	$result = $userreview->arrGetUserReviewDetails("","","","","","",$category_id,"","","",$startlimit,$cnt);

	$cnt = sizeof($result);
		
	$xml = "<USER_REVIEW_MASTER>";
	$xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
	for($i=0;$i<$cnt;$i++){
		$brand_id = $result[$i]['brand_id'];
		$model_id = $result[$i]['product_info_id'];
		$product_id = $result[$i]['product_id'];
		$category_id = $result[$i]['category_id'];
		$create_date = $result[$i]['create_date'];
		$status = $result[$i]['status'];
		$result[$i]['status'] = ($status == 1) ? 'Approved' : 'Rejected';
		$result[$i]['create_date'] = date('d/m/Y',strtotime($create_date));
		if(!empty($brand_id)){
			unset($brand_result);
			$brand_name="";
			$brand_result = $brand->arrGetBrandDetails($brand_id,$category_id);
			$brand_name = $brand_result[0]['brand_name'];				
			$modelArr[] = $brand_name;
		}
		$result[$i]['brand_name'] = $brand_name;
		if(!empty($model_id)){
			unset($product_result);
			$model_name="";
			$product_result = $product->arrGetProductNameInfo($model_id);
			$model_name = $product_result[0]['product_info_name'];
			$modelArr[] = $model_name;
		}			
		$result[$i]['model_name'] = $model_name;
		$result[$i]['brand_model_name'] = implode(" ",$modelArr);
		if(!empty($product_id)){
			unset($product_result);
			$variant="";
			$product_result = $product->arrGetProductDetails($product_id);
			$variant = $product_result[0]['variant'];
			$modelArr[] = $variant_name;
		}
		$result[$i]['brand_model_variant_name'] = implode(" ",$modelArr);
		unset($modelArr);

		$result[$i]['variant'] = $variant;
		$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
		$xml .= "<USER_REVIEW_MASTER_DATA>";
		foreach($result[$i] as $k=>$v){
			$xml .= "<$k><![CDATA[$v]]></$k>";
		}
		$xml .= "</USER_REVIEW_MASTER_DATA>";
	}
	$xml .= "</USER_REVIEW_MASTER>";
		
		
	$config_details = get_config_details();

        $strXML .= "<XML>";
        $strXML .= $config_details;
	$strXML .= "<MSG><![CDATA[$msg]]></MSG>";
        $strXML .= "<SEO_URL><![CDATA[$seo_url]]></SEO_URL>";
        $strXML .= "<SEO_WEB_URL><![CDATA[".SEO_WEB_URL."]]></SEO_WEB_URL>";
        $strXML .= "<SEO_TITLE><![CDATA[$seo_title]]></SEO_TITLE>";
        $strXML .= "<SEO_TAGS><![CDATA[$seo_keywords]]></SEO_TAGS>";
        $strXML .= "<SEO_DESC><![CDATA[$seo_desc]]></SEO_DESC>";
        $strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$category_id]]></SELECTED_CATEGORY_ID>";        
	$strXML.= $xml;
	$strXML.= "</XML>";

	$strXML = mb_convert_encoding($strXML, "UTF-8");
	if($_REQUEST['debug']==1) { header('Content-type: text/xml');echo $strXML;exit; }

	$doc = new DOMDocument();
	$doc->loadXML($strXML);
	$doc->saveXML();

	$xslt = new xsltProcessor;
	$xsl = DOMDocument::load('../xsl/user_review_dashboard.xsl');

	$xslt->importStylesheet($xsl);
	print $xslt->transformToXML($doc);
?>
