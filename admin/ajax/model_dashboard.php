<?php	
require_once('../../include/config.php');
require_once(CLASSPATH.'DbConn.php');
require_once(CLASSPATH.'category.class.php');
require_once(CLASSPATH.'product.class.php');
require_once(CLASSPATH.'brand.class.php');
require_once(CLASSPATH.'pager.class.php');

$dbconn = new DbConn;
$category = new CategoryManagement;
$oProduct = new ProductManagement;
$oBrand = new BrandManagement;
$oPager = new Pager;

$product_name_id = $_REQUEST['product_name_id'];
$category_id = $_REQUEST['catid'];
$startlimit = $_REQUEST['startlimit'];
$limitcnt = $_REQUEST['cnt'];
$actiontype = $_REQUEST['actiontype'] ? $_REQUEST['actiontype'] : 'Insert';
if(!empty($category_id)){
	$aBrandDetail=$oBrand->arrGetBrandDetails("",$category_id);
	$sBrandDataDet=arraytoxml($aBrandDetail,"BRAND_MASTER_DATA");
	$sBrandDataDetXML ="<BRAND_MASTER>".$sBrandDataDet."</BRAND_MASTER>";
	if(is_array($aBrandDetail)){
		foreach($aBrandDetail as $ibKey=>$aBrandData){
			$aBrandDetailName[$aBrandData['brand_id']][]=$aBrandData['brand_name'];
		}
	}
	

	$oProductcnt = $oProduct->arrGetProductNameInfo("",$category_id,$brand_id,"","","","");
        $iGetoProduCount=count($oProductcnt);

	if($iGetoProduCount!=0){
	$page = $_REQUEST['page'] ? $_REQUEST['page'] : 1;
	$perpage=20;
	$start  = $oPager->findStart($perpage);
	$recordcount=$iGetoProduCount;
	$sExtraParam="ajax/model_dashboard.php,sOProduOverDiv,$category_id";
	$jsparams=$start.",".$perpage.",".$sExtraParam;
	$pages= $oPager->findPages($recordcount,$perpage);
	if($pages > 1 ){
		$pagelist= $oPager->jsPageNumNextPrev($page,$pages,"sOProduOverPagination",$jsparams,"text");
		$nodesPaging .= "<Pages><![CDATA[".$pagelist."]]></Pages>";
		$nodesPaging .= "<Page><![CDATA[".$page."]]></Page>";
		$nodesPaging .= "<Perpage><![CDATA[".$perpage."]]></Perpage>";
	}
	 //$orderby=" order by create_date desc";
	 $aProductInfoResultList = $oProduct->arrGetProductNameInfo("",$category_id,$brand_id,"","",$start,$perpage);
	  #print "<pre>";
	  #print_r($aProductInfoResultList);
   }
	
	if(is_array($aProductInfoResultList)){
		foreach($aProductInfoResultList as $iKey=>$aProductInfoData){
			$aProductInfoList[$iKey]=$aProductInfoData;
			$status = $aProductInfoData['status'];
			$aProductInfoList[$iKey]['status'] = ($status == 1) ? 'Active' : 'InActive';
			if(is_array($aBrandDetailName) && isset($aBrandDetailName[$aProductInfoData['brand_id']])){
					$sModelBrandName=$aBrandDetailName[$aProductInfoData['brand_id']][0];
			}
			$aProductInfoList[$iKey]['brand_name']=$sModelBrandName;
			$categoryid = $aProductInfoData['category_id'];
			if(!empty($categoryid)){
				$category_result = $category->arrGetCategoryDetails($categoryid);
			}
			$aProductInfoList[$iKey]['category_name']=$category_result[0]['category_name'];
		}
		$sModelDataDet = arraytoxml($aProductInfoList,"MODEL_DETAIL_DATA");
		$sModelDataDetXML = "<MODEL_DETAIL>".$sModelDataDet."</MODEL_DETAIL>";
	}
}
//echo "DTATA---".$sModelDataDetXML;die();
if($_REQUEST['act']=='Delete' && !empty($product_name_id)){
	$dresult = $oProduct->deleteProductInfo($product_name_id,'PRODUCT_NAME_INFO');
}
if($_REQUEST['act']=='update' && !empty($product_name_id)){
	$aProductInfoResultDetail = $oProduct->arrGetProductNameInfo($product_name_id,$category_id,"","","",$startlimit,$cnt);
	//print "<pre>"; print_r($aProductInfoResultDetail);
	if(is_array($aProductInfoResultDetail)){
		foreach($aProductInfoResultDetail as $iKey=>$aProductInfoValue){
			$aProductInfoDetail[$iKey]=$aProductInfoValue;
			if(is_array($aBrandDetailName) && isset($aBrandDetailName[$aProductInfoData['brand_id']])){
					$sModelBrandName=$aBrandDetailName[$aProductInfoData['brand_id']][0];
			}
			$aProductInfoDetail[$iKey]['brand_name']=$sModelBrandName;
			if($aProductInfoData['media_id']!=''){
				//$sMainImagePath=getImageDetails($aProductInfoData['media_id'],SERVICEID,$action='api');
				//$sMainImage = $sMainImagePath['main_image'];
				$aProductInfoDetail[$iKey]['video_path_title'] = $sMainImagePath['title'];
			}
			if($aProductInfoData['img_media_id']!=''){
				//$sMainThmImagePath=getImageDetails($aProductInfoData['img_media_id'],SERVICEID,$action='api');
				//$sMainThmImage = $sMainThmImagePath['main_image'];
				$aProductInfoDetail[$iKey]['image_path_title'] = $sMainThmImagePath['title'];
			}
		}
		$sModelValueData=arraytoxml($aProductInfoDetail,"MODEL_MASTER_DATA");
		$sModelValueDataXML ="<MODEL_MASTER>".$sModelValueData."</MODEL_MASTER>";
	}
}

$config_details = get_config_details();

$strXML = "<XML>";
$strXML .= "<MSG><![CDATA[$msg]]></MSG>";
$strXML .= "<STARTLIMIT><![CDATA[$startlimit]]></STARTLIMIT>";
$strXML .= "<CNT><![CDATA[$limitcnt]]></CNT>";
$strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$category_id]]></SELECTED_CATEGORY_ID>"; 
$strXML .= "<SELECTED_ACTION_TYPE><![CDATA[$actiontype]]></SELECTED_ACTION_TYPE>"; 
$strXML .= $config_details;
$strXML .= $sModelDataDetXML;
$strXML .= $sModelValueDataXML;
$strXML .= $sBrandDataDetXML;
$strXML .= $nodesPaging;
$strXML .= "<WALLCNT><![CDATA[$iRelUploadCnt]]></WALLCNT>";
$strXML .= "</XML>";
$strXML = mb_convert_encoding($strXML, "UTF-8");
//header('Content-type: text/xml');echo $strXML;exit;
$doc = new DOMDocument();
$doc->loadXML($strXML);
$doc->saveXML();

$xslt = new xsltProcessor;
$xsl = DOMDocument::load('../xsl/model_dashboard.xsl');

$xslt->importStylesheet($xsl);
print $xslt->transformToXML($doc);
?>
