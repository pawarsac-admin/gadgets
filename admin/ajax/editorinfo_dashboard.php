<?php	
require_once('../../include/config.php');
require_once(CLASSPATH.'DbConn.php');
require_once(CLASSPATH.'user.class.php');

$dbconn = new DbConn;
$obj_user = new user;


//print "<pre>"; print_r($_REQUEST);print"</pre>";exit;
$category_id = $_REQUEST['catid'];
$startlimit = $_REQUEST['startlimit'];
$limitcnt = $_REQUEST['cnt'];
$editor_id =$_REQUEST['editor_id'];
$actiontype = $_REQUEST['act'];

if($_REQUEST['act']=='update' && !empty($editor_id)){
	$result = $obj_user->arrGetEditorInfoDetails($editor_id,$category_id,"","","","","","","");
	//print "<pre>"; print_r($result);exit;
	$cnt = sizeof($result);
	$xml .= "<EDITOR_INFO_DETAIL>";
	$xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
	for($i=0;$i<$cnt;$i++){
		$status = $result[$i]['status'];
		$categoryid = $result[$i]['category_id'];
		$result[$i]['status'] = ($status == 1) ? 'Active' : 'InActive';
		$result[$i]['create_date'] = date('d-m-Y',strtotime($result[$i]['create_date']));
		$editor_name = $result[$i]['editor_name'];
		if($editor_name != ""){$result[$i]['editor_name'] = html_entity_decode($editor_name,ENT_QUOTES);}
		$designation = $result[$i]['designation'];
		if($designation != ""){$result[$i]['designation'] = html_entity_decode($designation,ENT_QUOTES);}
		$short_desc = $result[$i]['short_desc'];
		if($short_desc != ""){$result[$i]['short_desc'] = html_entity_decode($short_desc,ENT_QUOTES);}
		$long_desc = $result[$i]['long_desc'];
		if($long_desc != ""){
			$result[$i]['long_desc'] = html_entity_decode($long_desc,ENT_QUOTES);
		}
			

		$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
		//print "<pre>"; print_r($result[$i]);
		$xml .= "<EDITOR_INFO_DETAIL_DATA>";
		foreach($result[$i] as $k=>$v){
		$xml .= "<$k><![CDATA[$v]]></$k>";
		}
		$xml .= "</EDITOR_INFO_DETAIL_DATA>";
	}
	$xml .= "</EDITOR_INFO_DETAIL>";
}


unset($result);
if(!empty($category_id)){
	$result = $obj_user->arrGetEditorInfoDetails("",$category_id,"","","","","","","");

}
//print"<pre>";print_r($result);print"</pre>";exit;
$cnt = sizeof($result);
$xml .= "<EDITOR_INFO_MASTER>";
$xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
for($i=0;$i<$cnt;$i++){

	$status = $result[$i]['status'];
	$categoryid = $result[$i]['category_id'];
	$editor_id = $result[$i]['editor_id'];

	$result[$i]['status'] = ($status == 1) ? 'Active' : 'InActive';
	$result[$i]['create_date'] = date('d-m-Y',strtotime($result[$i]['create_date']));

	$editor_name = $result[$i]['editor_name'];
        if($editor_name != ""){$result[$i]['editor_name'] = html_entity_decode($editor_name,ENT_QUOTES);}
	$designation = $result[$i]['designation'];
        if($designation != ""){$result[$i]['designation'] = html_entity_decode($designation,ENT_QUOTES);}
        $short_desc = $result[$i]['short_desc'];
        if($short_desc != ""){$result[$i]['short_desc'] = html_entity_decode($short_desc,ENT_QUOTES);}
        $long_desc = $result[$i]['long_desc'];
        if($long_desc != ""){
		if(strlen($long_desc)>100){ $long_desc = getCompactString($long_desc, 95).' ...'; }
		$result[$i]['long_desc'] = html_entity_decode($long_desc,ENT_QUOTES);
	}

	$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
	//print "<pre>"; print_r($result[$i]);
	$xml .= "<EDITOR_INFO_MASTER_DATA>";
	foreach($result[$i] as $k=>$v){
		$xml .= "<$k><![CDATA[$v]]></$k>";
	}
	$xml .= "</EDITOR_INFO_MASTER_DATA>";
}
$xml .= "</EDITOR_INFO_MASTER>";
//echo "TEST---".$xml_wallpaper;

$config_details = get_config_details();

$strXML = "<XML>";
$strXML .= "<MSG><![CDATA[$msg]]></MSG>";
$strXML .= "<STARTLIMIT><![CDATA[$startlimit]]></STARTLIMIT>";
$strXML .= "<CNT><![CDATA[$limitcnt]]></CNT>";
$strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$category_id]]></SELECTED_CATEGORY_ID>"; 
$strXML .= $config_details;
$strXML .= $xml;
$strXML .= "<WALLCNT><![CDATA[$iRelUploadCnt]]></WALLCNT>";
$strXML .= "<SELECTED_TYPE><![CDATA[$type_selecetd]]></SELECTED_TYPE>";
$strXML .= "<SELECTED_ACTION_TYPE><![CDATA[$actiontype]]></SELECTED_ACTION_TYPE>";
$strXML .= "</XML>";

$strXML = mb_convert_encoding($strXML, "UTF-8");
if($_GET['debug']==1) { header('Content-type: text/xml');echo $strXML;exit; }


$doc = new DOMDocument();
$doc->loadXML($strXML);
$doc->saveXML();

$xslt = new xsltProcessor;
$xsl = DOMDocument::load('../xsl/editorinfo_dashboard.xsl');

$xslt->importStylesheet($xsl);
print $xslt->transformToXML($doc);
?>
