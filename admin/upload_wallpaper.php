<?php
	require_once('../include/config.php');
	//require_once(CLASSPATH.'category.class.php');
	require_once(CLASSPATH.'DbConn.php');
	require_once(CLASSPATH.'product.class.php');
	require_once(CLASSPATH.'wallpaper.class.php');
	$dbconn = new DbConn;
	
	
	$oProduct = new ProductManagement;
	$oWallpaper=new Wallpapers;
	
	$actiontype = $_REQUEST['actiontype'];
	$startlimit = $_REQUEST['startlimit'];
        $limitcnt = $_REQUEST['cnt'];
	$request_param=$_REQUEST;


	//if($_POST){ print_r($_REQUEST);  } //die();

	$wallpaper_id=$_REQUEST['wallpaper_id'];
	$product_id=$_REQUEST['product_id'];
	if(strlen($_POST['product_id'])>0 || strlen($_REQUEST['select_brand_id'])>0 || strlen($_REQUEST['selected_category_id'])>0){
	$_REQUEST['display_rows']=$_REQUEST['display_rows']?$_REQUEST['display_rows']:1;
		if($_REQUEST['display_rows']>0){
			$dataCount=$_REQUEST['display_rows'];
			for($i=1;$i<=$dataCount;$i++){
				
				$wallpaper_id=$_REQUEST['wallpaper_id_'.$i];
				$media_path = trim($_REQUEST['img_upload_1_'.$i]);
				$image_path = trim($_REQUEST['img_upload_id_thm_'.$i]);
				$aParameters = array("category_id"=>$_REQUEST['selected_category_id'],"brand_id"=>$_REQUEST['select_brand_id'],"product_id"=>$_REQUEST['product_id'],"wallpaper_position"=>$_REQUEST['wallpaper_position'],"status"=>$_REQUEST['product_status'],"media_id"=>$_REQUEST['media_id_'.$i],"media_path"=>$media_path,"img_media_id"=>$_REQUEST['img_media_id_'.$i],"image_path"=>$image_path );

				$wallpaper_id==0 ? $aParameters['create_date']=date("Y-m-d H:i:s") : $aParameters['update_date']=date("Y-m-d H:i:s"); 
				if($wallpaper_id>0) $aParameters['wallpaper_id']=$wallpaper_id;
				$iResId=$oWallpaper->intInsertWallpapers($aParameters);
				$wallpaper_id=$iResId;
				//print_r($aParameters);
				if($wallpaper_id!='' && $_REQUEST['select_type_id']=='2'){ //2 for slide show
					$mediapath = trim($_REQUEST['img_upload_1_'.$i]);
					$imagepath = trim($_REQUEST['img_upload_id_thm_'.$i]);
					$aSlideParameters=array("wallpaper_id"=>$wallpaper_id,"category_id"=>$_REQUEST['selected_category_id'],"brand_id"=>$_REQUEST['select_brand_id'],"product_id"=>$_REQUEST['product_id'],"slideshow_position"=>$_REQUEST['slideshow_position'],"status"=>$_REQUEST['product_status'],"media_id"=>$_REQUEST['media_id_'.$i],"media_path"=>$mediapath,"img_media_id"=>$_REQUEST['img_media_id_'.$i],"image_path"=>$imagepath );
					$iSlideResId=$oWallpaper->intInsertSlideShow($aSlideParameters);
				}
			}
		}
		
		
		if($wallpaper_id==0) {
			$wallpaper_id=$iResId;
			$msg="Wallpaper detail added successfully.";
		}else {
			$msg="Wallpaper detail updated successfully.";
		}
	
	}

	if($actiontype == 'Delete'){
	   $result = $oWallpaper->boolDeleteWallpaper($wallpaper_id);
	   $msg = 'Wallpaper deleted successfully.';
	}
	
$startlimit = $_REQUEST['startlimit'];
$limitcnt = $_REQUEST['cnt'];
$config_details = get_config_details();

$strXML = "<XML>";
$strXML .= "<MSG><![CDATA[$msg]]></MSG>";
$strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$brand_level]]></SELECTED_CATEGORY_ID>";
$strXML .= "<STARTLIMIT><![CDATA[$startlimit]]></STARTLIMIT>";
$strXML .= "<CNT><![CDATA[$limitcnt]]></CNT>";
$strXML .= $config_details;
$strXML .= $xml;
$strXML .= "</XML>";

//header('Content-type: text/xml');echo $strXML;exit;
$doc = new DOMDocument();
$doc->loadXML($strXML);
$doc->saveXML();

$xslt = new xsltProcessor;
$xsl = DOMDocument::load('xsl/upload_wallpaper.xsl');

$xslt->importStylesheet($xsl);
print $xslt->transformToXML($doc);
?>
