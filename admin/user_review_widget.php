<?php
require_once('../include/config.php');
require_once(CLASSPATH.'DbConn.php');
require_once(CLASSPATH.'user_review.class.php');

$dbconn = new DbConn;
$userreview = new USERREVIEW;

$actiontype = ($_REQUEST['actiontype'] != "") ? $_REQUEST['actiontype'] : "Insert";
$startlimit = $_REQUEST['startlimit'];
$limitcnt = $_REQUEST['cnt'];
$category_id = $_REQUEST['selected_category_id'];

//print "<pre>"; print_r($_REQUEST); //die();
if($actiontype == 'Insert'|| $actiontype == 'Update'){
	$id = $_REQUEST['id'];
	if(!empty($id)){$request_param['id'] = $id;}

	$category_id = $_REQUEST['selected_category_id'];
	if($category_id!=''){ $request_param['category_id'] = $category_id;}
	
	$brand_id = $_REQUEST['select_brand_id'];
	if(!empty($brand_id)){$request_param['brand_id'] = $brand_id;}
	
	$product_info_id = $_REQUEST['select_model_id'];
	if(!empty($product_info_id)){$request_param['product_info_id'] = $product_info_id;}

	$status = $_REQUEST['status'];
        if($status!=''){ $request_param['status'] = $status;}

	if($actiontype == 'Insert' && $product_info_id!=''){
		$id=$userreview->intInsertUpdateUserReviewWidget($request_param,"USER_REVIEW_WIDGET");
		if($id>0){
			$msg = 'user review widget added successfully.';
		}
	}elseif($actiontype == 'Update' && $id!=''){
		$id=$userreview->intInsertUpdateUserReviewWidget($request_param,"USER_REVIEW_WIDGET");
		if($id>0){
			$msg = 'user review widget updated successfully.';
		}
	}
}

if($actiontype == 'Delete' && $_REQUEST['id']!=''){
	$id = $_REQUEST['id'];
	$result = $userreview->boolDeleteUserReviewWidget($id);
	$msg = 'user review widget deleted successfully.';
}
$config_details = get_config_details();
$strXML = "<XML>";
$strXML .= "<MSG><![CDATA[$msg]]></MSG>";
$strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$category_id]]></SELECTED_CATEGORY_ID>";
$strXML .= "<STARTLIMIT><![CDATA[$startlimit]]></STARTLIMIT>";
$strXML .= "<SELECTED_ACTION_TYPE><![CDATA[$actiontype]]></SELECTED_ACTION_TYPE>";
$strXML .= "<CNT><![CDATA[$limitcnt]]></CNT>";
$strXML .= $config_details;
$strXML .= $xml;
$strXML .= "</XML>";

$strXML = mb_convert_encoding($strXML, "UTF-8");
if($_GET['debug']==1) { header('Content-type: text/xml');echo $strXML;exit; }

$doc = new DOMDocument();
$doc->loadXML($strXML);
$doc->saveXML();

$xslt = new xsltProcessor;
$xsl = DOMDocument::load('xsl/user_review_widget.xsl');

$xslt->importStylesheet($xsl);
print $xslt->transformToXML($doc);
?>
