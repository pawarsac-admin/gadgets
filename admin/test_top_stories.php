<?php
require_once('../include/config.php');
require_once(CLASSPATH.'DbConn.php');
require_once(CLASSPATH.'TopStories.Class.php');


$dbconn		 = new DbConn;
$oTopStories = new TopStories;

$group_id	= ($_REQUEST['group_id'])? $_REQUEST['group_id'] : 1;


$storyXML		= $oTopStories->getStoriesByGroupXML( $group_id, $dbconn);
$strXML = "<XML>";
$strXML .= "<GROUP_ID><![CDATA[$group_id]]></GROUP_ID>";
$strXML .= $storyXML;
$strXML .= "</XML>";

$strXML = mb_convert_encoding($strXML, "UTF-8");
header('Content-type: text/xml');echo $strXML;
?>