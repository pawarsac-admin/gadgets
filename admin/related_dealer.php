<?php
require_once("../include/config.php");
require_once(CLASSPATH.'DbConn.php');
require_once(CLASSPATH."category.class.php");
require_once(CLASSPATH."brand.class.php");
require_once(CLASSPATH."citystate.class.php");
require_once(CLASSPATH."dealer.class.php");

$dbconn 	= new DbConn;
$oCategory	= new CategoryManagement;
$oBrand		= new BrandManagement;
$oCityState = new citystate;
$oDealer	= new Dealer;

//if($_POST){ print_r($_REQUEST);} ///die();

$actiontype = $_REQUEST['actiontype'] ? $_REQUEST['actiontype'] : "Insert";
$startlimit = $_REQUEST['startlimit'];
$limitcnt = $_REQUEST['cnt'];
$category_id = $_REQUEST['selected_category_id'] ? $_REQUEST['selected_category_id'] : $_REQUEST['catid'];
$hd_view_section_id = $_REQUEST["hd_view_section_id"];
$select_section_id = $_REQUEST["select_section_id"] ? $_REQUEST["select_section_id"] : $hd_view_section_id;

if($actiontype == 'Insert'|| $actiontype== 'Update'){
	unset($request_param);

	$status = trim($_REQUEST['status']);
	if($status!=''){ $request_param['status']=$status;}

	$dealer_id = $_REQUEST['select_dealer_id'] ? $_REQUEST['select_dealer_id'] : $_REQUEST['dealer_id'] ;
	if(!empty($dealer_id)){ $request_param['dealer_id']=$dealer_id;}

	//print "<pre>"; print_r($request_param);
	if($select_section_id == "FEATURED_DEALER" || $select_section_id == "PREMIUM_DEALER"){
		if(!empty($category_id)){ $request_param['category_id']=$category_id;}
        }
	if($select_section_id == "FEATURED_DEALER"){
		$table_name="FEATURED_DEALER";
	}else if($select_section_id == "PREMIUM_DEALER"){
		$table_name="PREMIUM_DEALER";
	}
	
	if($dealer_id!=''){
		$result = $oDealer->addUpdRelatedDealerDetails($request_param,$table_name);
	}
	if($actiontype == 'Insert'){
		if($sresult>0){$msg = 'Related Dealer added successfully.';}
	}elseif($actiontype == 'Update'){
		if($sresult>0){$msg = 'Related Dealer updated successfully.';}
	}
}

if($actiontype == 'Delete'){
	$dealer_id = $_REQUEST["dealer_id"] ? $_REQUEST["dealer_id"] : $_REQUEST["hd_dealer_id"];
	$hd_view_section_id = $_REQUEST["hd_view_section_id"];
	if($hd_view_section_id == "FEATURED_DEALER"){
                $table_name="FEATURED_DEALER";
        }else if($hd_view_section_id == "PREMIUM_DEALER"){
                $table_name="PREMIUM_DEALER";
        }
	if($dealer_id!=''){
		$result = $oDealer->boolDeleteRelatedDealer($dealer_id,$table_name);
		$msg = 'Related Dealer deleted successfully.';
	}
}

$config_details = get_config_details();

$strXML = "<XML>";
$strXML .= "<MSG><![CDATA[$msg]]></MSG>";
$strXML .= "<SELECTED_MENU_ID><![CDATA[$menu_level]]></SELECTED_MENU_ID>";
$strXML .= "<SELECTED_CATEGORY_ID><![CDATA[$category_id]]></SELECTED_CATEGORY_ID>";
$strXML .= "<SELECTED_ACTION_TYPE><![CDATA[$actiontype]]></SELECTED_ACTION_TYPE>";
$strXML .= "<VIEWSECTION><![CDATA[$select_section_id]]></VIEWSECTION>";
$strXML .= "<STARTLIMIT><![CDATA[$startlimit]]></STARTLIMIT>";
$strXML .= "<CNT><![CDATA[$limitcnt]]></CNT>";
$strXML .= $config_details;
$strXML .= $xml;
$strXML .= "</XML>";

if($_GET['debug']==1) { header('Content-type: text/xml');echo $strXML;exit; }

$doc = new DOMDocument();
$doc->loadXML($strXML);
$doc->saveXML();

$xslt = new xsltProcessor;
$xsl = DOMDocument::load('xsl/related_dealer.xsl');

$xslt->importStylesheet($xsl);
print $xslt->transformToXML($doc);
?>
