<?php
/*
Plugin Name: Sharebar
Description: Adds a dynamic bar with sharing icons (Facebook, Twitter, etc.) that changes based on browser size and page location.
Version: 1.0.0
Author: Sachin Pawar
Date: 11-07-2011
use: sharebar( $display_flag  , $orientaion , $size , $class , $width , $height , $user_specified_share_array );
e.g.

1> Display Single icon with size specified
//sharebar_button('plus1', 'small' );

2> Display icons with custome parameter
//the_sharebar();
//the_sharebar( 'horizontal' , 'big' , 'cls' , 95 , 95 );
//the_sharebar( 'horizontal' , 'small' , 'cls' , 95 , 95 , array('plus1','digg'));
//the_sharebar( 'horizontal' , 'big' , 'cls' , 95 , 95 , array('plus1','digg'));

//$op	= get_the_sharebar();
//$op	= get_the_sharebar( 'horizontal' , 'big' , 'cls' , 95 , 95 );
//$op	= get_the_sharebar( 'horizontal' , 'small' , 'cls' , 95 , 95 , array('plus1','digg'));
//$op	= get_the_sharebar( 'vertical' , 'big' , 'cls' , 95 , 95 , array('plus1','digg'));

Default icons : plus1, twitter, facebook, sharethis, buzz, digg, reddit, email 
Default orientaion : Horizontal
Default size : big
Default H/W	: 100%	
*/

$page_URI = (!empty($_SERVER['HTTPS'])) ? "https://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'] : "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];

$share = array(0=>"plus1", 1=>"twitter", 2=>"facebook", 3=>"sharethis", 4=>"buzz", 5=>"digg", 6=>"reddit", 7=>"email" );

$plus1['big'] = '<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script><g:plusone size="tall"></g:plusone>';

$plus1['small'] = '<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script><g:plusone></g:plusone>';

$digg['big'] = '<script type="text/javascript">(function() { var s = document.createElement(\'SCRIPT\'), s1 = document.getElementsByTagName(\'SCRIPT\')[0]; s.type = \'text/javascript\'; s.async = true; s.src = \'http://widgets.digg.com/buttons.js\'; s1.parentNode.insertBefore(s, s1); })(); </script><a class="DiggThisButton DiggMedium"></a>';

$digg['small'] = '<script type="text/javascript">(function() { var s = document.createElement(\'SCRIPT\'), s1 = document.getElementsByTagName(\'SCRIPT\')[0]; s.type = \'text/javascript\'; s.async = true; s.src = \'http://widgets.digg.com/buttons.js\'; s1.parentNode.insertBefore(s, s1); })(); </script><a class="DiggThisButton DiggCompact"></a>';

$twitter['big'] = '<a href="http://twitter.com/share" class="twitter-share-button" data-count="vertical" data-via="[twitter]">Tweet</a><script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>';

$twitter['small'] = '<a href="http://twitter.com/share" class="twitter-share-button" data-count="horizontal" data-via="[twitter]">Tweet</a><script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>';

$facebook['big'] = '<iframe src="http://www.facebook.com/plugins/like.php?href=' . $page_URI . '&layout=box_count&show_faces=false&width=60&action=like&colorscheme=light&height=45" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:45px; height:60px;" allowTransparency="true"></iframe>';

$facebook['small'] = '<iframe src="http://www.facebook.com/plugins/like.php?href=' . $page_URI . '&layout=button_count&show_faces=false&width=85&action=like&colorscheme=light&height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:85px; height:21px;" allowTransparency="true"></iframe>';

$sharethis['big'] = '<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script><span class="st_facebook_vcount" displayText="Share"></span><!--span class="st_email" displayText="Email"></span><span class="st_sharethis" displayText="Share"></span-->';

$sharethis['small'] = '<span class="st_facebook_hcount" displayText="Share"></span><span class="st_email" displayText="Email"></span><span class="st_sharethis" displayText="Share"></span>';

$buzz['big'] = '<a title="Post to Google Buzz" class="google-buzz-button" href="http://www.google.com/buzz/post" data-button-style="normal-count"></a><script type="text/javascript" src="http://www.google.com/buzz/api/button.js"></script>';

$buzz['small'] = '<a title="Post to Google Buzz" class="google-buzz-button" href="http://www.google.com/buzz/post" data-button-style="small-count"></a><script type="text/javascript" src="http://www.google.com/buzz/api/button.js"></script>';

$reddit['big']	= '<script type="text/javascript" src="http://reddit.com/static/button/button2.js"></script>';

$reddit['small'] = '<script type="text/javascript" src="http://reddit.com/static/button/button1.js"></script>';

$email['big']	= '<a href="mailto:?subject=' . $page_URI . '" class="sharebar-button email">Email</a>';

$email['small']	= '<a href="mailto:?subject=' . $page_URI . '" class="sharebar-button email">Email</a>';

function sharebar( $orientation = 'horizontal', $size = 'big' , $class_name = '' , $width= '100%' , $height = '100%' , $custome_share = array() ){
		sharebar_init();
		global $share, $plus1, $digg, $twitter,$facebook,$sharethis,$buzz,$reddit,$email;

		if(!empty($custome_share)) $share = $custome_share ; 

		$id_value	= ($orientation == 'vertical')? 'sharebar' : 'sharebarx';
		
		$str = '<div id="sharebar" class="'.$class_name.'" width="'. $width .'" height="'. $height .'">';
		$str .= '<ul id="'.$id_value.'">';
		foreach( $share as $result ){ 
			$str .= '<li>'.${$result}[$size].'</li>'; 
		}
		$str .= '</ul>';
		$str .= '</div>';
		return $str;
}

function sharebar_button($name, $size = 'big'){
	global $share, $plus1, $digg, $twitter,$facebook,$sharethis,$buzz,$reddit,$email;
	if($size == 'big') echo ${$name}['big']; else echo ${$name}['small'];
}

function sharebar_init(){
	echo '<link rel="stylesheet" href="/plugins/sharebar/css/sharebar.css" type="text/css" />';
}

function the_sharebar( $orientation = 'horizontal', $size = 'big' , $class_name = '' , $width= '100%' , $height = '100%' , $custome_share = array() ){
	echo get_the_sharebar( $orientation , $size , $class_name , $width  , $height , $custome_share );
}

function get_the_sharebar( $orientation = 'horizontal', $size = 'big' , $class_name = '' , $width= '100%' , $height = '100%' , $custome_share = array() ){
	return sharebar( $orientation , $size , $class_name , $width  , $height , $custome_share );
}

//sharebar_button('facebook', 'small' );

//the_sharebar();
//the_sharebar( 'horizontal' , 'big' , 'cls' , 95 , 95 );
//the_sharebar( 'horizontal' , 'small' , 'cls' , 95 , 95 , array('plus1','digg'));
//the_sharebar( 'horizontal' , 'big' , 'cls' , 95 , 95 , array('plus1','digg'));

//$op	= get_the_sharebar();
//$op	= get_the_sharebar( 'horizontal' , 'small' , 'cls' , 95 , 95 );
//$op	= get_the_sharebar( 'vertical' , 'small' , 'cls' , 95 , 95 , array('plus1','digg'));
//$op	= get_the_sharebar( 'vertical' , 'big' , 'cls' , 95 , 95 , array('plus1','digg'));
//echo $op;

?>