<?php
    //ini_set("display_errors",1);
	require_once('./include/config.php');
	require_once(CLASSPATH.'DbConn.php');
	require_once(CLASSPATH.'report.class.php');
	require_once(CLASSPATH.'brand.class.php');
	require_once(CLASSPATH.'product.class.php');
	require_once(CLASSPATH.'reviews.class.php');
	require_once(CLASSPATH.'videos.class.php');
	require_once(CLASSPATH.'article.class.php');

	$dbconn = new DbConn;
	$report = new report;
	$oBrand = new BrandManagement;
	$oProduct = new ProductManagement;
	$reviews = new reviews();
	$videoGallery = new videos();
	$article = new article();
	
	$cagetory_id = $_REQUEST['category_id'] ? $_REQUEST['category_id'] : SITE_CATEGORY_ID;
	
	$typeid = $_REQUEST['typeid'];
	$startlimit = $_REQUEST['startlimit'];
	$cnt = $_REQUEST['cnt'];
	$startdate = $_REQUEST['startdate'];
	$enddate = $_REQUEST['endddate'];
	$vd_type=$_REQUEST['vd_type'];

	switch($typeid){
		case '1':
			//article goes here.
			$type = 'OCA';
			$result = $report->arrSolarArticleDetails($cagetory_id,$startlimit,$cnt,$startdate,$enddate);
			break;
		case '2':
			//news
			$type = 'OCN';
			$result = $report->arrSolarNewsDetails($category_id,$startlimit,$cnt,$startdate,$enddate);
			break;
		case '3':
			//reviews
			$type = 'OCR';
			$result = $report->arrSolarReviewsDetails($category_id,$startlimit,$cnt,$startdate,$enddate);
			break;
		case '4':
			//video
			$type = 'OCV';
			if($vd_type==''){
				$result_list1 = $report->arrSolarVideoDetails($category_id,$startlimit,$cnt,$startdate,$enddate);
			   $result_list2 = $reviews->arrGetReviewsVideoDetails("","","","",$category_id,"","1",$startlimit,$cnt,"");
			   $result_review = $videoGallery->arrGenerate($result_list2,"","","2");
			   $result_list3 = $article->arrGetArticleVideoDetails("","","","",$category_id,"","1","3",$startlimit,$cnt);
				$result_article = $videoGallery->arrGenerate($result_list3,"","","6");
				$result_list4 = $article->arrGetNewsVideoDetails("","","","",$category_id,"","1","",$startlimit,$cnt,"");
				$result_news = $videoGallery->arrGenerate($result_list4,"","","9");
				$result=array_merge($result_list1,$result_review,$result_article,$result_news);
			}
            // =====================================================
            if($vd_type=='1'){ // all video 
			 $result_list1 = $report->arrSolarVideoDetails($category_id,$startlimit,$cnt,$startdate,$enddate);
			 $result=$result_list1;
            }else if($vd_type=="2"){ // Review video 
			 $result_list2 = $reviews->arrGetReviewsVideoDetails("","","","",$category_id,"","1",$startlimit,$cnt,"");
			 $result_review = $videoGallery->arrGenerate($result_list2,"","","2");
			 $result=$result_review;
			}else if($vd_type=="3"){ // article video 
 			  $result_list3 = $article->arrGetArticleVideoDetails("","","","",$category_id,"","1","3",$startlimit,$cnt);
			  $result_article = $videoGallery->arrGenerate($result_list3,"","","6");
			  $result=$result_article;
			}else if($vd_type=="4"){  // News Video 
			  $result_list4 = $article->arrGetNewsVideoDetails("","","","",$category_id,"","1","",$startlimit,$cnt,"");
			  $result_news = $videoGallery->arrGenerate($result_list4,"","","9");
			  $result=$result_news;
			}
		    

            //print "<pre>"; print_r($result_news);
            //exit;
			break;
		case '5':
			//cars
			$type = 'OCC';
			$result = $report->arrSolarCarDetails($category_id,$startlimit,$cnt,$startdate,$enddate);
			break;
		default:
			//cars
			$type = 'OCC';
			$result = $report->arrSolarCarDetails($category_id,$startlimit,$cnt,$startdate,$enddate);
			break;	
	}
	function genxml($result,$type){
 		global $oBrand,$oProduct;
    	$cnt = sizeof($result);
		//print "<pre>";print_r($result);
		$xml = "<add>";
		 for($i=0;$i<$cnt;$i++){ 
			 $result[$i]['create_date'] = gmdate('Y-m-d\TG:i:s\Z',strtotime($result[$i]['create_date']));
			 $creatdate=$result[$i]['create_date'];
           if($type=="OCA" || $type=="OCN"){	// Article News
             $id=$type.$result[$i]['article_id'];
			// $author=html_entity_decode($result[$i]['author'],ENT_QUOTES,'UTF-8');
			 $tag=html_entity_decode($result[$i]['tags'],ENT_QUOTES,'UTF-8');
			 $author=$result[$i]['editor_name'];
			 if($tag==''){
			   $tag="Oncars";
			 }
			 $title=html_entity_decode($result[$i]['title'],ENT_QUOTES,'UTF-8');	
			 $short_desc=$result[$i]['short_desc']; 
			 if($short_desc){
			   $description=html_entity_decode($result[$i]['short_desc'],ENT_QUOTES,'UTF-8');
			   $description=strip_tags($description);
			 }else{
				   $description=html_entity_decode($result[$i]['abstract'],ENT_QUOTES,'UTF-8');
				   $description=strip_tags($description);
			 }
			 if($type=="OCA"){
			 $seoTitleArr="";
			   unset($seoTitleArr);
			   $seoTitleArr[] = SEO_WEB_URL;
			   $seoTitleArr[] = SEO_AUTO_ARTICLE_DETAIL;
		       $seoTitleArr[] = $title;
		       $seoTitleArr[] = $result[$i]['article_id'];
		       $seo_url = implode("/",$seoTitleArr);
			 }else {

				 $seoTitleArr="";
			     unset($seoTitleArr);
			     $seoTitleArr[] = SEO_WEB_URL;
			     $seoTitleArr[] = SEO_AUTO_NEWS_DETAIL;
		         $seoTitleArr[] = $title;
		         $seoTitleArr[] = $result[$i]['article_id'];
		         $seo_url = implode("/",$seoTitleArr);
			   
			 }
		  }else if($type=="OCR"){ // Reviews
            $id=$type.$result[$i]['review_id'];
		    $tag=html_entity_decode($result[$i]['tags'],ENT_QUOTES,'UTF-8');
			$author=$result[$i]['editor_name'];
			if($tag==''){
			   $tag="Oncars";
			 }
			$title=html_entity_decode($result[$i]['title'],ENT_QUOTES,'UTF-8');	
			 $short_desc=$result[$i]['short_desc']; 
			 if($short_desc){
			   $description=html_entity_decode($result[$i]['short_desc'],ENT_QUOTES,'UTF-8');
			   $description=strip_tags($description);
			 }else{
				   $description=html_entity_decode($result[$i]['abstract'],ENT_QUOTES,'UTF-8');
				   $description=strip_tags($description);
			 }
			$product_id = $result[$i]['product_id'];
			$product_info_id = $result[$i]['product_info_id'];
			$review_id = $result[$i]['review_id'];
			$review_group_id = $result[$i]['group_id'];
            $brand_id=$result[$i]['brand_id'];

			$aBrandDetail=$oBrand->arrGetBrandDetails($brand_id,$category_id);			
		    $brand_name=$aBrandDetail[0]['brand_name'];

			$aProductDetail=$oProduct->arrGetProductDetails($product_id,$category_id,"","1");
			$variant=$aProductDetail[0]['variant'];

			$productNameInfo = $oProduct->arrGetProductNameInfo($product_info_id,$category_id,"","",1,"","");
			//print "<pre>";print_r($productNameInfo);
			$model=$productNameInfo[0]['product_info_name'];
			
			$brand_name = html_entity_decode($brand_name,ENT_QUOTES,'UTF-8');
			$brand_name = removeSlashes($brand_name);

			$brand_name = seo_title_replace($brand_name);
			
			$model = html_entity_decode($model,ENT_QUOTES,'UTF-8');
			$model = removeSlashes($model);
			$model = seo_title_replace($model);
			$variant = html_entity_decode($variant,ENT_QUOTES,'UTF-8');
			$variant = removeSlashes($variant);
			$variant = seo_title_replace($variant);
			if($product_id!='0'){
			unset($seoTitleArr);
			$seoTitleArr[] = SEO_WEB_URL;
			$seoTitleArr[] = $brand_name."-cars";
			$seoTitleArr[] = $brand_name."-".$model;
			$seoTitleArr[] = $variant;
			$seoTitleArr[] = "reviews-ratings";
			$seoTitleArr[] = "on-cars-reviews";
			$seoTitleArr[] = $product_id;
			$seoTitleArr[] = $review_group_id;
			$seoTitleArr[] = $review_id;
			$seo_url = implode("/",$seoTitleArr);
		}
		if($product_id=='0'){
			unset($seoTitleArr);
			$seoTitleArr[] = SEO_WEB_URL;
			$seoTitleArr[] = $brand_name."-cars";
			$seoTitleArr[] = $brand_name."-".$model;
			//$seoTitleArr[] = $model;
			$seoTitleArr[] = "reviews-ratings";
			$seoTitleArr[] = "on-cars-Model-reviews";
			$seoTitleArr[] = $model;
			$seoTitleArr[] = $product_info_id;
			$seoTitleArr[] = $review_group_id;
			$seoTitleArr[] = $review_id;
			$seo_url = implode("/",$seoTitleArr);
			
		}
		    
	 }else if($type=="OCV"){

            $id=$type.$result[$i]['video_id'];
		    $tag=html_entity_decode($result[$i]['tags'],ENT_QUOTES,'UTF-8');
			$author="Oncars";
			if($tag==''){
			   $tag="Oncars";
			 }
			$title=html_entity_decode($result[$i]['title'],ENT_QUOTES,'UTF-8');	
			$description=html_entity_decode($result[$i]['meta_description'],ENT_QUOTES,'UTF-8');
            if($result[$i]['type_id']){
             $tab_selected=$result[$i]['type_id'];
            }else{
			  $tab_selected=$result[$i]['tbl_type'];
			}

		 $seoTitleArr="";
         unset($seoTitleArr);
		 $seoTitleArr[] = SEO_WEB_URL;
			if($tab_selected == "2"){
			$seoTitleArr[] = SEO_CAR_VIDEOS_REVIEW; //Car-Video-Reviews
			}elseif($tab_selected == "3"){
			$seoTitleArr[] = SEO_CAR_VIDEOS_AUTO_PORN; //Auto Porn
			}elseif($tab_selected == "4"){
			$seoTitleArr[] = SEO_CAR_VIDEOS_INTERNATIONAL; //Car-Video-International
			}elseif($tab_selected == "5"){
			$seoTitleArr[] = SEO_CAR_VIDEOS_OTHERS; //Car-Video-Others
			}elseif($tab_selected == "6"){
			$seoTitleArr[] = SEO_CAR_VIDEOS_MAINTAINANCE; //Car-Video-DIY
			}elseif($tab_selected == "7"){
			$seoTitleArr[] =SEO_CAR_VIDEOS_FIRST_DRIVE; //Car-Video-First-Drive
			}elseif($tab_selected == "8"){
			$seoTitleArr[] = SEO_CAR_VIDEOS_QUICK_TEST; //Car-Quick -Test 
			}elseif($tab_selected == "9"){
			$seoTitleArr[] = SEO_CAR_VIDEOS_NEWS; //Car-Video-News
			 }

			$stitle = html_entity_decode($title,ENT_QUOTES,'UTF-8');
			$stitle = removeSlashes($stitle);
			//$stitle = seo_title_replace($stitle);
			$seoTitleArr[] = str_replace(array(" ","/"),"-",$stitle);


			$seoTitleArr[] = $result[$i]['video_id'];
			$seo_play_url = implode("/",$seoTitleArr);
			$seo_url=$seo_play_url;

	   }

			 $xml .="<doc>";
			 $xml .="<field name='id'>".$id."</field>";
			 $xml .="<field name='title'><![CDATA[$title]]></field>";
			 $xml .="<field name='author'><![CDATA[$author]]></field>";
			 $xml .="<field name='tag'><![CDATA[$tag]]></field>";
			 $xml .="<field name='description'><![CDATA[$description]]></field>";
			 $xml .="<field name='URL'><![CDATA[$seo_url]]></field>";
			 $xml .="<field name='date'><![CDATA[$creatdate]]></field>";
			 $xml .="</doc>";



		 }
		$xml .= "</add>";
		return $xml;
	}
	//print "<pre>";print_r($result);
	$xml = genxml($result,$type);
	header('Content-type: text/xml');
	echo $xml;
	exit;
	
?>
